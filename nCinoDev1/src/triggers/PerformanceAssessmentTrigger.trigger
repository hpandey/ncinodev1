trigger PerformanceAssessmentTrigger on Performance_Assessment__c (before update) {
    PerformanceAssessmentTriggerHandler handler = new PerformanceAssessmentTriggerHandler();
    if (Trigger_Setting__c.getValues('Performance_Assessment__c ')==null || Trigger_Setting__c.getValues('Performance_Assessment__c ').isActive__c==true) {
        if(trigger.isBefore){
            handler.UpdateRecordOwner(Trigger.new);
        }
    }
}