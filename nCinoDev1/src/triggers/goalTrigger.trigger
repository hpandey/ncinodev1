trigger goalTrigger on Goal__c (before insert, before update) {
    goalTriggerHandler handler = new goalTriggerHandler();
    if (Trigger_Setting__c.getValues('Goal__c ')==null || Trigger_Setting__c.getValues('Goal__c ').isActive__c==true) {
        if(trigger.isBefore){
            handler.updateGoalOwner(Trigger.new);
        }
    }
    for (Goal__c g : Trigger.new){
        System.debug('Goal ' + g.goalid__c + ' Owner is ' + g.ownerid);
    }
}