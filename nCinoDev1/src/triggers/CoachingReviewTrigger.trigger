trigger CoachingReviewTrigger on Coaching_Review__c (after insert, after update) {
    CoachingReviewTriggerHandler handler = new CoachingReviewTriggerHandler();
    if (Trigger_Setting__c.getValues('Coaching_Review__c')==null || Trigger_Setting__c.getValues('Coaching_Review__c').isActive__c==true) {
        if(trigger.isAfter){
            handler.shareReview(Trigger.new);
//            if (trigger.isInsert && CoachingReviewTriggerHandler.firstRun) {
//                handler.populatePriorReview(Trigger.new);
//                CoachingReviewTriggerHandler.firstRun = false;
//            }
        }
    }
}