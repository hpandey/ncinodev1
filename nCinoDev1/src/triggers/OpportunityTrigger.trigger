trigger OpportunityTrigger on Opportunity (before insert, after insert,before update,after update) {

  OpportunityTriggerHandler handler = new OpportunityTriggerHandler();
   // case# 1584...won't fire for lookup...Ephrem 10/2014 
   if ((Trigger_Setting__c.getValues('Opportunity')==null && createLookbackCTRL.isLookBack==false)|| (Trigger_Setting__c.getValues('Opportunity').isActive__c==true && createLookbackCTRL.isLookBack==false)) {
    if(Trigger.isInsert){
        if(Trigger.isAfter){
            handler.OnAfterInsert(Trigger.new);
        } else {
            handler.OnBeforeInsert(Trigger.new);
        }
    }
    //added handler for Closed Won and RM Credit Only logic... Ephrem 03/13/2014 
    if(Trigger.isUpdate){  
        if(Trigger.isAfter){        
            handler.OnUpdate(Trigger.new);
            deleteDuplicatePipeline.ProcessDeletion();     //added for case 1853 - Ephrem Tekle - 10-2015    
        }       
    }       
  }
}