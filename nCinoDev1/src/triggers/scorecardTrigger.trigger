trigger scorecardTrigger on Scorecard__c (after insert, after update) {
    scorecardHandler handler = new scorecardHandler();
    if (Trigger_Setting__c.getValues('Scorecard__c')==null || Trigger_Setting__c.getValues('Scorecard__c').isActive__c==true) {
        if(trigger.isAfter){
            handler.shareScorecard(Trigger.new);
        }
    }
}