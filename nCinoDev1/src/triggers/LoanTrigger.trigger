/************************************************************************************
 Trigger Name        : LoanTrigger
 Version             : 1.0
 Created Date        : 16th Feb 2016
 Function            : Trigger for Loan Object.
 Author              : Pranil Thubrikar
 Modification Log    :
* Developer                         Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar                 2/16/2016                Original Version
*************************************************************************************/

trigger LoanTrigger on LLC_BI__Loan__c (Before Insert, After Insert, Before Update, After Update, Before Delete, After Delete) {

    Trigger_Setting__c triggerSetting  = Trigger_Setting__c.getValues(environmentVariables.loanAPI);
    
    if(triggerSetting == null || triggerSetting.isActive__c){
        LoanTriggerHandler loanTriggerHandlerObj = new LoanTriggerHandler();
        TriggerDispatcher.Run(loanTriggerHandlerObj);
    }
    
}