trigger RelationshipTrigger on Relationship__c (before update, after insert) {
/*
    Descrption - 
    Call Sharing code when inserted
*/
  if (Trigger_Setting__c.getValues('Relationship__c')==null || 
      Trigger_Setting__c.getValues('Relationship__c').isActive__c==true) { 
    AllRelationshipTriggerHandler handler = new AllRelationshipTriggerHandler();
    String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;
    if (usrProfileName != 'Regions Migration') {
        //DO NOTHING
    }else if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterNightlyRelationshipInsert(Trigger.new);
    }else if(Trigger.isUpdate && Trigger.isBefore){
        List<Relationship__c> updateRecords = new List<Relationship__c>();
        for( Relationship__c obj : Trigger.new )
        {
            if( Trigger.oldMap.get( obj.Id ).OwnerId != obj.OwnerId ){
                updateRecords.add(obj);
            }
        }
        if(updateRecords.size() > 0){
            handler.OnBeforeNightlyRelationshipUpdate(updateRecords);
        }
    }
  }
}