trigger ParticipantTrigger on Participant__c (before Insert, before Update, after Insert, after Update, after Delete) {
/*
    Descrption - 
    Ensure Participant is unique on Call Plan
    Update the Call Plan with number of Participants (required since lookup)
*/
 if (Trigger_Setting__c.getValues('Participant')==null || 
     Trigger_Setting__c.getValues('Participant').isActive__c==true) {
    boolean okToAdd = true;
    
    Set <String> Participants = new Set<String>();
    
    if(Trigger.isBefore){       
        // loop through existing participants
        // removed the filter- and name__c <> null- from the select query and added a logic in the for loop to exclude null values... Ephrem 04-18-2014
        List <Participant__c> parts = [SELECT name__c from Participant__c where call_plan__c = :Trigger.new[0].Call_Plan__c]; 
        for (Participant__c part : parts) {
            if(part.name__c != null)
            {
                if (Participants.contains (part.name__c)) {
                } else {
                   Participants.add(part.name__c);
                }
             }   
        }
        for(Participant__c x : Trigger.New){
          // loop through new participants
          if ( ( Trigger.isInsert) || (Trigger.oldMap.get( x.Id ).name__c != x.name__c ) ) {
            if (Participants.contains (x.name__c)) {
                okToAdd = false;
            } else {
                Participants.add(x.name__c);
            }
          }
          // I-51732 Participant can be non-SF user, if so, then do not adjust owner
          if (x.Name__c != null) x.OwnerId = x.Name__c;                                
        }
        // ensure that the Participant is only added to call plan once
        if (!okToAdd){
            Trigger.New[0].addError('Participant is already on the Call Plan');
        }
        
    }
    
    if(Trigger.isAfter){
        Set<id> callplanIds = new Set<id>();
        List<call_plan__c> callplansToUpdate = new List<call_plan__c>();
  
        if (Trigger.isUpdate || Trigger.isDelete) {
            for (Participant__c item : Trigger.old)
                callplanIds.add(item.Call_Plan__c);
        } else {
            for (Participant__c item : Trigger.new)
            callplanIds.add(item.Call_Plan__c);
        }
 
        // get a map of the call_plans with the number of items
        Map<id,Call_Plan__c> callplanMap = new Map<id,Call_Plan__c>([select id, participants__c from Call_Plan__c where id IN :callplanIds]);
 
        // query the call_plans and the related participant items and add the size of the participant items to the call plan's participants__c
        for (Call_Plan__c cp : [select Id, Name, participants__c,(select id from Participants__r) from Call_Plan__c where Id IN :callplanIds]) {
            callplanMap.get(cp.Id).participants__c = cp.Participants__r.size();
            // add the value/participant in the map to a list so we can update it
            callplansToUpdate.add(callplanMap.get(cp.Id));
        }
        //-----------------------------------------------------------------------
        //Goals participant insert - Jeff Hulslander 10/5/13
        //-----------------------------------------------------------------------
        if(trigger.isInsert){
            List <Id> lstUsers = new List<Id>();
            List <Id> lstCallPlan = new List<Id>();
            for(Participant__c prt : trigger.new){
                lstUsers.add(prt.Name__c);
                lstCallPlan.add(prt.Call_Plan__c);
            }
            Map<ID,Call_Plan__c> cpMap = new Map<ID,Call_Plan__c>(); 
            for(Call_Plan__c cp : [SELECT id, Completed_Date__c From Call_Plan__c WHERE ID IN :lstCallPlan] ) {           
                cpMap.put(cp.Id, cp);         
            }
            List<Goal__c> lstGoals = new List<Goal__c>([select id,Time_Period__c,User__c,Start_Date__c,End_Date__c from Goal__c where User__c in :lstUsers]);
            List<Call_Goal__c> newCallGoals = new List<Call_Goal__c>();
            for(Participant__c p : trigger.new){
                Call_Plan__c call = cpMap.get(p.Call_Plan__c);
                for(Goal__c g : lstGoals){
                    //For each triggered Participant, find goals that match this user and time period
                    if(p.Name__c == g.User__c 
                    && call.Completed_Date__c >= g.Start_Date__c 
                    && call.Completed_Date__c <= g.End_Date__c){
                        newCallGoals.add(new Call_Goal__c(
                            Goal__c = g.Id,
                            Call_Plan__c = p.Call_Plan__c
                            )
                        );   
                    }
                }
            }
            insert newCallGoals;
        }
        //-----------------------------------------------------------------------
        //Goals participant delete - Jeff Hulslander 10/5/13
        //-----------------------------------------------------------------------
        if(trigger.isDelete){
            List <Id> lstCPs = new List<Id>();
            for(Participant__c prt : trigger.old){
                lstCPs.add(prt.Call_Plan__c);
            }
            List<Call_Goal__c> lstCallGoals = new List<Call_Goal__c>([select id,Goal__r.User__c,Call_Plan__c from Call_Goal__c where Call_Plan__c in :lstCPs]);
            List<Call_Goal__c> callGoalsToDelete = new List<Call_Goal__c>();
            for(Participant__c prt : trigger.old){
                for(Call_Goal__c glp : lstCallGoals){
                    if(prt.Name__c == glp.Goal__r.User__c && prt.Call_Plan__c == glp.Call_Plan__c){
                        callGoalsToDelete.add(glp);
                    }
                }
            }
            delete callGoalsToDelete;
        }
        //-----------------------------------------------------------------------
        //Sharing participant to Call Plan after insert - Jeff Hulslander 8/15/13
        //-----------------------------------------------------------------------
        if(trigger.isInsert){
            // Create a new list of sharing objects for Job
            List<Call_Plan__Share> callPlanShrs  = new List<Call_Plan__Share>();
            
            // Declare variable for participant sharing
            Call_Plan__Share prtShr;
            
            for(Participant__c prt : trigger.new){
                if (prt.Name__c!=NULL){
                    // Instantiate the sharing object
                    prtShr = new Call_Plan__Share();
                    
                    // Set the ID of record being shared
                    prtShr.ParentId = prt.Call_Plan__c;
                    
                    // Set the ID of user or group being granted access
                    prtShr.UserOrGroupId = prt.Name__c;
                    
                    // Set the access level
                    prtShr.AccessLevel = 'read';
                    
                    // Set the Apex sharing reason for call plan participant
                    prtShr.RowCause = Schema.Call_Plan__Share.RowCause.Call_Plan_Participant__c;
                    
                    // Add objects to list for insert
                    callPlanShrs.add(prtShr);
                }
            }
            
            // Insert sharing records and capture save result 
            // The false parameter allows for partial processing if multiple records are passed 
            // into the operation 
            if(!callPlanShrs.isEmpty()){
                Database.SaveResult[] lsr = Database.insert(callPlanShrs,false);
            
                // Create counter
                Integer i=0;
                
                // Process the save results
                for(Database.SaveResult sr : lsr){
                    if(!sr.isSuccess()){
                        // Get the first save result error
                        Database.Error err = sr.getErrors()[0];
                        
                        // Check if the error is related to a trivial access level
                        // Access levels equal or more permissive than the object's default 
                        // access level are not allowed. 
                        // These sharing records are not required and thus an insert exception is 
                        // acceptable. 
                        if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                                                       &&  err.getMessage().contains('AccessLevel'))){
                            // Throw an error when the error is not related to trivial access level.
                            trigger.newMap.get(callPlanShrs[i].ParentId).
                              addError(
                               'Unable to grant sharing access due to following exception: '
                               + err.getMessage());
                        }
                    }
                    i++;
                }
            }
        }
        //------------------------------------------------------------------------------
        //Revoke sharing participant to Call Plan after delete - Jeff Hulslander 8/15/13
        //------------------------------------------------------------------------------
        if(trigger.isDelete){
            List<Call_Plan__Share> sharesToDelete = new List<Call_Plan__Share>();
            for(Participant__c prt : trigger.old){
                if (prt.Name__c!=NULL){
                    LIST<Call_Plan__Share> delShare = [SELECT Id FROM Call_Plan__Share 
                                                    WHERE ParentId = :prt.Call_Plan__c
                                                    AND UserOrGroupId = :prt.Name__c
                                                    AND RowCause = :Schema.Call_Plan__Share.RowCause.Call_Plan_Participant__c];
                    sharesToDelete.addAll(delShare);
                }
            }
            if(!sharesToDelete.isEmpty()){
                Database.Delete(sharesToDelete, false);
            }
        }
        update callplansToUpdate;
    } 
 }   
}