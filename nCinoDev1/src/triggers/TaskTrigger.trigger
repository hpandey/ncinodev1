trigger TaskTrigger on Task (before insert,before update,after update, after insert) {
    TaskTriggerHandler handler = new TaskTriggerHandler();
    if (Trigger_Setting__c.getValues('Task')==null || Trigger_Setting__c.getValues('Task').isActive__c==true) {
        if(Trigger.isBefore){
            handler.BeforeSaveREB(Trigger.new);
            if(Trigger.isInsert){
                handler.beforeInsert(Trigger.new);
            }
        }
        if(Trigger.isAfter){
           if(Trigger.isUpdate){         
                handler.afterUpdate(Trigger.OldMap,Trigger.new);
           }    
           if(Trigger.isInsert){       //handler for email notification...     
                handler.emailNotification(Trigger.new);                
           }
        }
     }
}