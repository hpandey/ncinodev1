trigger ownerCopyD on Designee__c (before Insert, before Update) {
/*
    Descrption - 
    Copy owner to custom field - NOT REQUIRED SUMMER13
*/ 
if (Trigger_Setting__c.getValues('Designee__c')==null || 
      Trigger_Setting__c.getValues('Designee__c').isActive__c==true) {
    // handle arbitrary number of opps
    for(Designee__c x : Trigger.New){

        // check that owner is a user (not a queue)
        if( ((String)x.OwnerId).substring(0,3) == '005' ){
            x.Owner_Copy__c = x.OwnerId;
        }
        else{
            // in case of Queue we clear out our copy field
            x.Owner_Copy__c = null;
        }
    }
  }  
}