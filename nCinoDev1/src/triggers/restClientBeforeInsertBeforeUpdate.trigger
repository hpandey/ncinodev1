trigger restClientBeforeInsertBeforeUpdate on Restricted_Client_Information__c (after insert , before insert , before update) {   
/*
    Descrption - 
    Ensure only 1 Restricted Client per Client
    Configure Calculation of Next Conduct Review Date
*/
 if (Trigger_Setting__c.getValues('Restricted_Client_Information__c')==null || 
      Trigger_Setting__c.getValues('Restricted_Client_Information__c').isActive__c==true){       

    set<ID> accIDs = new set<ID>();
    map<string,integer> rest_count = new map<string,integer>();
    
    
    // check in before insert if more than one Restrictedr Client Information not to be created.  
    if(trigger.isInsert && trigger.isAfter)
    {
         for(Restricted_Client_Information__c oclientInfo : trigger.new)
         {               
            accIDs.add(oclientInfo.Client__c);  // get the account Ids 
         } 
         
        // aggregate SOQL to count number of childs per account.                                               
        AggregateResult[] groupedResults  = [SELECT count(ID) cnt ,Client__c FROM Restricted_Client_Information__c WHERE Client__c IN: accIDs  GROUP BY Client__c];
            for (AggregateResult ar : groupedResults)  
            {                           
                rest_count.put(''+ar.get('Client__c'),integer.valueof(ar.get('cnt')));
            }
        // put error message if more than one client info exists    
         for(Restricted_Client_Information__c oclientError : trigger.new)
         {      
                if(rest_count.get(oclientError.Client__c) > 1 && !TEST.isRunningTest() )        
                {
                    oclientError.addError('More than 1 restricted client information exists');
                }
                                    
         } 
    }
                                                          
    Boolean isUpdate = false;
    if(trigger.isInsert && trigger.isBefore)
    {       
        isUpdate = true; // to check whether current execution is insert or update call
                
    }
    
    // below logic to be executed in both insert and update call by using isUpdate  flag
    
    if( (trigger.isBefore && trigger.isUpdate) || isUpdate == true )
    {
    Map<Id, String> clientRiskMap = new Map<Id, String>();
    for(Restricted_Client_Information__c clientInfo : trigger.new)
    {
        // if any of the 3 custom fields are modifed then below logic executes
        if(!trigger.isInsert) // if not an insert call! trigger.oldmap can not be used in insert call.
                    if( trigger.oldMap.get(clientInfo.ID).Risk_Rating_Override__c != trigger.newMap.get(clientInfo.ID).Risk_Rating_Override__c
                            || trigger.oldMap.get(clientInfo.ID).Risk_Rating_Level__c != trigger.newMap.get(clientInfo.ID).Risk_Rating_Level__c
                            || trigger.oldMap.get(clientInfo.ID).Monitoring_For_Suspicious_Activity__c != trigger.newMap.get(clientInfo.ID).Monitoring_For_Suspicious_Activity__c
                            || trigger.oldMap.get(clientInfo.ID).Date_Last_Review_Approved__c != trigger.newMap.get(clientInfo.ID).Date_Last_Review_Approved__c 
                            || trigger.oldMap.get(clientInfo.ID).Date_Approved__c != trigger.newMap.get(clientInfo.ID).Date_Approved__c 
                            || trigger.oldMap.get(clientInfo.ID).Existing_Client__c != trigger.newMap.get(clientInfo.ID).Existing_Client__c )                            
                                                       
                                                       
                            {
                                isUpdate = true;                                    
                            }                
        // if date approved is not null and any of the 3 feilds modifed (isUpdate)   
        integer nrdYear = 0;
       string clientRisk = '';
        
       if(clientInfo.Risk_Rating_Level__c != null) // 1st priority
        {
                clientRisk = clientInfo.Risk_Rating_Level__c;
                if(clientInfo.Risk_Rating_Level__c == 'High')
                    nrdYear = 1;
                if(clientInfo.Risk_Rating_Level__c == 'Moderate')
                    nrdYear = 2;
                if(clientInfo.Risk_Rating_Level__c == 'Low')
                    nrdYear = 3;
        }
        // calcualte years based on risk rating override
        if(clientInfo.Risk_Rating_Override__c != null) // 2nd priority
        {
                clientRisk = clientInfo.Risk_Rating_Override__c;
                if(clientInfo.Risk_Rating_Override__c == 'High')
                    nrdYear = 1;
                if(clientInfo.Risk_Rating_Override__c == 'Moderate')
                    nrdYear = 2;
                if(clientInfo.Risk_Rating_Override__c == 'Low')
                    nrdYear = 3;
        } 
        clientRiskMap.put(clientInfo.Client__c,clientRisk);
                                 
        if((clientInfo.Date_Approved__c != null || clientInfo.Date_Last_Review_Approved__c != null ) && isUpdate == true)                                 
        {            
                    if(trigger.oldMap != null) // on update only
                    {
                         
                        // if conduct review is created/modified         
                        if(clientInfo.Date_Last_Review_Approved__c != null) 
                        //&& trigger.oldMap.get(clientInfo.ID).Date_Last_Review_Approved__c != trigger.newMap.get(clientInfo.ID).Date_Last_Review_Approved__c)
                        {         
                          clientInfo.Next_Review_Date__c = clientInfo.Date_Last_Review_Approved__c.addYears(nrdYear);
                        }    
                        // if risk assesment review is created/modified 
                        else if(clientInfo.Date_Approved__c != null)
                        // && trigger.oldMap.get(clientInfo.ID).Date_Approved__c != trigger.newMap.get(clientInfo.ID).Date_Approved__c)
                        {             
                          clientInfo.Next_Review_Date__c = clientInfo.Date_Approved__c.addYears(nrdYear);
                        }    
                    } 
                    else
                    {
                        // if conduct review is created/modified         
                        if(clientInfo.Date_Last_Review_Approved__c != null )                          
                            clientInfo.Next_Review_Date__c = clientInfo.Date_Last_Review_Approved__c.addYears(nrdYear);
                        // if risk assesment review is created/modified 
                        else if(clientInfo.Date_Approved__c != null )                           
                            clientInfo.Next_Review_Date__c = clientInfo.Date_Approved__c.addYears(nrdYear);
                    }                                                          
        }
        
        // when last review date is null AND bank relation begin date is null AND date approved EXISTS
       // if(clientInfo.Date_Last_Review_Approved__c == null && clientInfo.Bank_Relationship_Begin_Date__c == null && clientInfo.Date_Approved__c != null)       
        if(clientInfo.Date_Last_Review_Approved__c == null && clientInfo.Existing_Client__c == 'No' && clientInfo.Date_Approved__c != null)       
        {
            clientInfo.Next_Review_Date__c  = clientInfo.Date_Approved__c.addYears(1);                           
        }
        
        // if monitoring is true and any of the 3 feilds modifed (isUpdate)        
        if(clientInfo.Monitoring_For_Suspicious_Activity__c  && isUpdate == true) // if true
        {
                if(clientInfo.Date_Approved__c != null)
                {
                   clientInfo.Next_Review_Date__c =    clientInfo.Date_Approved__c.addMonths(6);
                }
                if(clientInfo.Date_Last_Review_Approved__c != null )
                {
                    clientInfo.Next_Review_Date__c =    clientInfo.Date_Last_Review_Approved__c.addMonths(6);
                }
                
                           
        }
        // If the Next review date is in the past or within the next 30 days, move it out 1 month, so it isn't already overdue.
        if(clientInfo.Next_Review_Date__c != null && ( (clientInfo.Next_Review_Date__c <= system.today()) || (clientInfo.Next_Review_Date__c > system.today() &&  clientInfo.Next_Review_Date__c <= system.today().addDays(30))  ))
        {
            clientInfo.Next_Review_Date__c = system.today().addmonths(1);            
        }
                                  
    } // end for loop
    
    //Update Risk Rating on Client Object (Added 9/17 by Jeff Hulslander - T-186777)
    List<Account> accList = [Select Id,Name,Client_Risk_Rating__c from Account where Id in :clientRiskMap.keySet()];
    for(Account acc : accList){
        //Update field value
        acc.Client_Risk_Rating__c = clientRiskMap.get(acc.Id);
    }//End for
    update accList;
    }

 }
}