trigger callPlanTMcopy on Participant__c (before insert, before Update, after insert, after Update, after delete) {
        
    if (Trigger_Setting__c.getValues('Participant__c')==null || 
        Trigger_Setting__c.getValues('Participant__c').isActive__c==true){                 
        boolean okToAdd = true;
        string ErrMsg;
        string tmCCC;        //Treasury Management Commercial Card Consultant user
        string tmCCCarea;
        string tmCCCregion;
        string tmSC;         //Treasury Management Sales Consultant
        string tmSCarea;
        string tmSCregion;
        string tmOfficer;    //Treasury Management Officer
        string tmOfficerArea;
        string tmOfficerRegion;
        string tmSA;    //Treasury Management Sales Associate ... case 1755 Ephrem 03/2015
        string tmSAarea;
        string tmSAregion;
        string cp;          
        if((Trigger.isBefore)||(Trigger.isInsert) || (Trigger.isUpdate)){
            for (participant__c ptm : [SELECT call_plan__c, name__c, name__r.id, name__r.lob__c, name__r.c_i_type__c, name__r.Name, name__r.Area__c, name__r.Region__c from Participant__c WHERE call_plan__c=:Trigger.new[0].call_plan__c]){
                if(ptm.call_plan__c != null){
                    if(ptm.name__r.c_i_type__c == 'Commercial Card Sales Consultant'){
                        tmCCC = ptm.name__r.Name; tmCCCarea = ptm.name__r.Area__c; tmCCCregion = ptm.name__r.Region__c;
                    }
                    if(ptm.name__r.c_i_type__c == 'Treasury Management Sales Consultant'){
                        tmSC = ptm.name__r.Name; tmSCarea = ptm.name__r.Area__c; tmSCregion = ptm.name__r.Region__c;
                    }
                    
                    if(ptm.name__r.c_i_type__c == 'Treasury Management Officer'){
                        tmOfficer = ptm.name__r.Name; tmOfficerArea = ptm.name__r.Area__c; tmOfficerRegion = ptm.name__r.Region__c;
                    }
                    if(ptm.name__r.c_i_type__c == 'Treasury Management Sales Associate'){
                        tmSA = ptm.name__r.Name; tmSAarea = ptm.name__r.Area__c; tmSAregion = ptm.name__r.Region__c;
                    }
                }
                else{
                    ErrMSG = 'Please update the Call Plan record before adding a participant. If you encounter additional issues, please contact OneView Support.';
                    okToAdd = false;
                    break;
                }
            }
            if (!okToAdd){
                Trigger.New[0].addError(ErrMSG);
            }
            else if ((tmCCC != '') || (tmSC != '') || (tmOfficer != '') || (tmSA !='')){
                //Update Call Plan record with TM users names
               Call_Plan__c thisCP = [SELECT id, Commercial_Card_Sales_Consultant__c, Commercial_Card_Sales_Consultant_Area__c, Commercial_Card_Sales_Consultant_Region__c, 
                                      Treasury_Management_Officer__c, Treasury_Management_Officer_Area__c, Treasury_Management_Officer_Region__c, 
                                      Treasury_Management_Sales_Consultant__c, Treasury_Management_Sales_Consultant_Are__c, Treasury_Management_Sales_Consultant_Reg__c,
                                      Treasury_Management_Sales_Associate__c, Treasury_Management_Sales_Associate_Area__c, Treasury_Management_Sales_Associate_Reg__c
                                      from Call_Plan__c where id =:Trigger.new[0].call_plan__c];                
                if(thisCP.id != null){
                    if (tmCCC != ''){thisCP.Commercial_Card_Sales_Consultant__c = tmCCC;}
                    if (tmCCCarea != ''){thisCP.Commercial_Card_Sales_Consultant_Area__c = tmCCCarea;}
                    if (tmCCCregion != ''){thisCP.Commercial_Card_Sales_Consultant_Region__c = tmCCCregion;}        
                    if (tmSC != ''){thisCP.Treasury_Management_Sales_Consultant__c = tmSC;}
                    if (tmSCarea != ''){thisCP.Treasury_Management_Sales_Consultant_Are__c = tmSCarea;}
                    if (tmSCregion != ''){thisCP.Treasury_Management_Sales_Consultant_Reg__c = tmSCregion;}        
                    if (tmOfficer != ''){thisCP.Treasury_Management_Officer__c = tmOfficer;}
                    if (tmOfficerArea != ''){thisCP.Treasury_Management_Officer_Area__c = tmOfficerArea;}
                    if (tmOfficerRegion != ''){thisCP.Treasury_Management_Officer_Region__c = tmOfficerRegion;}
                    if (tmSA != ''){thisCP.Treasury_Management_Sales_Associate__c = tmSA;}
                    if (tmSAarea != ''){thisCP.Treasury_Management_Sales_Associate_Area__c = tmSAarea;}
                    if (tmSAregion != ''){thisCP.Treasury_Management_Sales_Associate_Reg__c = tmSAregion;}
                    try{
                        update thisCP;
                    }catch (DmlException e){
                        Trigger.New[0].addError('Please update the Call Plan record before adding a participant. If you encounter additional issues, please contact OneView Support.');
                    }
                }
           }
        }else if(Trigger.isDelete){
            //Remove TM User from Call Plan record
            for(participant__c x : Trigger.old){
                Call_Plan__c thisCP = [SELECT id, Commercial_Card_Sales_Consultant__c, Commercial_Card_Sales_Consultant_Area__c, Commercial_Card_Sales_Consultant_Region__c, 
                                        Treasury_Management_Officer__c, Treasury_Management_Officer_Area__c, Treasury_Management_Officer_Region__c, 
                                        Treasury_Management_Sales_Consultant__c, Treasury_Management_Sales_Consultant_Are__c, Treasury_Management_Sales_Consultant_Reg__c,
                                        Treasury_Management_Sales_Associate__c, Treasury_Management_Sales_Associate_Area__c, Treasury_Management_Sales_Associate_Reg__c
                                        from Call_Plan__c where id = :Trigger.old[0].Call_Plan__c];
                if(thisCP.id != null){
                    thisCP.Commercial_Card_Sales_Consultant__c = '';
                    thisCP.Commercial_Card_Sales_Consultant_Area__c = '';
                    thisCP.Commercial_Card_Sales_Consultant_Region__c = '';            
                    thisCP.Treasury_Management_Sales_Consultant__c = '';
                    thisCP.Commercial_Card_Sales_Consultant_Area__c = '';
                    thisCP.Treasury_Management_Sales_Consultant_Reg__c = '';           
                    thisCP.Treasury_Management_Officer__c = '';
                    thisCP.Treasury_Management_Officer_Area__c = '';
                    thisCP.Treasury_Management_Officer_Region__c = '';
                    thisCP.Treasury_Management_Sales_Associate__c = '';
                    thisCP.Treasury_Management_Sales_Associate_Area__c = '';
                    thisCP.Treasury_Management_Sales_Associate_Reg__c = '';                                              
                    try{
                        update thisCP;
                    }catch (DmlException e){
                        Trigger.old[0].addError('There was an issue removing the Treasury Management participant data from the Call Plan. Please contact OneView Support.');
                    }
                }else{}
            }
        }
    }
}