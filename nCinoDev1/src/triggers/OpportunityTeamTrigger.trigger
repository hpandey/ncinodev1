trigger OpportunityTeamTrigger on OpportunityTeamMember (after delete, after insert, after update) {
    OpportunityTeamTriggerHandler handler = new OpportunityTeamTriggerHandler();
    if (Trigger_Setting__c.getValues('OpportunityTeamMember')==null || Trigger_Setting__c.getValues('OpportunityTeamMember').isActive__c==true) {
        if(Trigger.isInsert){
            handler.AfterInsert(Trigger.new);
            handler.RevenueSegment(trigger.new);
            handler.CoreSegment(trigger.new);
        }else if(Trigger.isDelete){
            handler.AfterDelete(Trigger.old);
            handler.RevenueSegment(trigger.old);
            handler.CoreSegment(trigger.old);            
        }
	        
    }
}