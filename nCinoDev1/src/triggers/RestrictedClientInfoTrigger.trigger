/************************************************************************************
 Apex Class Name     : AccountTrigger
 Version             : 1.0
 Created Date        : 26th Jan 2016
 Function            : Actual Trigger Code for Restricted_Client_Information__c Object
 Author              : Ansuman Patnaik (apatnaik@deloitte.com),Jag Valaiyapathy (jvalaiyapathy@deloitte.com)
 Credits             : Chris Aldridge (Original Author)
 Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------------                 
* Jag Valaiyapathy          1/26/2016                Original Version
*************************************************************************************/

trigger RestrictedClientInfoTrigger on Restricted_Client_Information__c(before insert, before update, before delete,
                                  after insert, after update, after delete,
                                  after undelete){

    TriggerDispatcher.Run(new RestrictedClientInfoTriggerHandler());
}