trigger teamMemberCopy on OpportunityTeamMember (before Insert, before Update, after delete) {
    /*
    Descrption - 
    Copy owner to custom field - NOT REQUIRED SUMMER13
    Ensure WM only can add more than 1 team member per LOB
    */
    if (Trigger_Setting__c.getValues('OpportunityTeamMember')==null || 
      Trigger_Setting__c.getValues('OpportunityTeamMember').isActive__c==true) {   
        if((Trigger.isInsert) || (Trigger.isUpdate) ){            
            boolean okToAdd = true;
            string ErrMsg;
            string TMCCSCUser;
            string TMCCSCarea;
            string TMCCSCregion;
            string TMSSUser;
            string TMSSArea;
            string TMSSRegion;
            string TMOUser;
            string TMOUserArea;
            string TMOUserRegion; 
            string TMassociate;
            string TMassociateArea;
            string TMassociateRegion; 
            string SBAUser;                     //Dale Blocker 05-15-2014
            string SBAUserArea;                 //Dale Blocker 05-15-2014
            string SBAUserRegion;               //Dale Blocker 05-15-2014
            integer BCBCnt = 0;
            String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;
            List <ID> userIds = new List<String>();
            // handle arbitrary number of recs
            system.debug(trigger.new);
            for(OpportunityTeamMember x : Trigger.New){
                x.Team_Member_Copy__c = x.UserId;
                x.Opportunity_Copy__c = x.OpportunityId;
                userIds.add(x.UserId);
            }
            Set <String> lobs = new Set<String>();
            Set <String> TMlobs = new Set<String>();  
            Set <String> SBAlobs = new Set<String>();          //Dale Blocker 05-15-2014
            // loop through existing team members
            List <OpportunityTeamMember> otms = [SELECT lob__c, user.id, user.c_i_type__c, user.Name, user.Area__c, user.Region__c from opportunityteammember where opportunityid = :Trigger.new[0].OpportunityId];
            system.debug(otms);
            for (OpportunityTeamMember otm : otms) {
                // S-111255 allow more than 1 WM team member
                // validate that there are no other team members with same LOB
                // Allow only 1 of each TM user Business group type
                // Allow only 1 of each C&I user Business group type...Ephrem 03-2014
                // Added trigger.isInsert to prevent lob edit error during update operation... Ephrem 04-30-2014
                // Added Portfolio Management Group and Funeral Trust business groups... Ephrem 08-2014
                // Added Endowments & Foundations business group...Ephrem 11-2014
                if ((otm.lob__c == 'Corporate Trust')||(otm.lob__c == 'Institutional Trust')||(otm.lob__c == 'NRRE')||(otm.lob__c == 'Private Wealth Management')
                    ||(otm.lob__c == 'Portfolio Management Group') || (otm.lob__c == 'Funeral Trust') || (otm.LOB__c == 'Endowments & Foundations')){
                        continue;
                }else if (otm.lob__c == 'Business Community Banking'){
                    if ((TMlobs.contains (otm.user.c_i_type__c))&&(trigger.isInsert)&&(usrProfileName != 'System Administrator')){
                        ErrMSG = 'Only one Business Banker can be added to the Pipeline team';
                        okToAdd = false; 
                        break;                              
                    } else {
                        SBAlobs.add(otm.user.c_i_type__c);
                        BCBCnt = BCBCnt + 1; 
                        if(otm.user.c_i_type__c == 'SBA'){
                            SBAUser=otm.user.Name; SBAUserarea=otm.user.Area__c; SBAUserregion=otm.user.Region__c;
                        }
                        continue;
                    }
                }else{
                    if ((otm.user.c_i_type__c == 'Commercial Card Sales Consultant')||(otm.user.c_i_type__c == 'Treasury Management Sales Consultant')||(otm.user.c_i_type__c == 'Treasury Management Officer')
                        || (otm.user.c_i_type__c == 'Treasury Management Sales Associate')|| (otm.user.c_i_type__c == 'CMM')||(otm.user.c_i_type__c == 'Corporate')||(otm.user.c_i_type__c == 'GIB')){
                        if ((TMlobs.contains (otm.user.c_i_type__c))&&(trigger.isInsert)&&(usrProfileName != 'System Administrator')){
                            ErrMSG = 'Only 1 '+otm.user.c_i_type__c+' team member can be added';
                            okToAdd = false; 
                            break;                              
                        } else {
                            TMlobs.add(otm.user.c_i_type__c);
                            //added area and region for the TM users...Ephrem
                            if(otm.user.c_i_type__c == 'Commercial Card Sales Consultant'){
                                TMCCSCUser=otm.user.Name; TMCCSCarea=otm.user.Area__c; TMCCSCregion=otm.user.Region__c;
                            }
                            if(otm.user.c_i_type__c == 'Treasury Management Sales Consultant'){
                                TMSSUser=otm.user.Name; TMSSArea=otm.user.Area__c; TMSSRegion=otm.user.Region__c;
                            }
                            if(otm.user.c_i_type__c == 'Treasury Management Officer'){
                                TMOUser=otm.user.Name; TMOUserArea=otm.user.Area__c; TMOUserRegion=otm.user.Region__c;
                            }
                            if(otm.user.c_i_type__c == 'Treasury Management Sales Associate'){
                                 TMassociate=otm.user.Name; TMassociateArea=otm.user.Area__c;TMassociateRegion = otm.user.Region__c;
                            }                 
                        }            
                        continue;
                    }
                    if (lobs.contains (otm.lob__c)&&(trigger.isInsert)&& (usrProfileName != 'System Administrator')) {
                        ErrMSG = 'Only 1 '+otm.lob__c+' team member can be added';
                        okToAdd = false;
                        break; 
                    }else {
                        lobs.add(otm.lob__c);
                    }                        
                } 
            } 
            
            // loop through new team members
            if(trigger.isInsert){
                List <User> users = [select u.id, u.lob__c, u.c_i_type__c, u.name, u.area__c, u.region__c from User u where u.id IN :userIds];
                for (User u : users){
                    // S-111255 allow more than 1 WM team member...
                    if ((u.lob__c == 'Corporate Trust')||(u.lob__c == 'Institutional Trust')||(u.lob__c == 'NRRE')||(u.lob__c == 'Private Wealth Management') || (u.lob__c == 'Portfolio Management Group') || (u.lob__c == 'Funeral Trust') 
                    	|| (u.lob__c == 'Endowments & Foundations')){
                            continue;
                    }else if (u.lob__c == 'Business Community Banking'){                                        
                        if (BCBCnt == 2) {
                            ErrMSG = 'Only 2 '+u.lob__c+' team members can be added';
                            okToAdd = false; 
                            break;                              
                        }else if((u.c_i_type__c != 'SBA')&&(!SBAlobs.contains ('SBA'))&&(BCBCnt > 0)){
                                ErrMSG = 'Only one Business Banker can be added to the Pipeline team';
                                okToAdd = false; 
                                break;
                        }else if((u.c_i_type__c == 'SBA')&&(SBAlobs.contains ('Business Banking SBA'))){
                                ErrMSG = 'Only one Business Banker can be added to the Pipeline team';
                                okToAdd = false; 
                                break;
                        } else {
                            SBAlobs.add(u.c_i_type__c);
                            if(u.c_i_type__c == 'SBA'){
                                SBAUser=u.Name; SBAUserarea=u.Area__c; SBAUserregion=u.Region__c;                          
                            }
                        }            
                        continue;
                    }else{
                        if ((u.c_i_type__c == 'Commercial Card Sales Consultant')|| (u.c_i_type__c == 'Treasury Management Sales Consultant')||(u.c_i_type__c == 'Treasury Management Officer')
                            || (u.c_i_type__c == 'Treasury Management Sales Associate')|| (u.c_i_type__c == 'CMM')||(u.c_i_type__c == 'Corporate')||(u.c_i_type__c == 'GIB')){
                            if ((TMlobs.contains (u.c_i_type__c))&&(trigger.isInsert)&& (usrProfileName != 'System Administrator')){
                                ErrMSG = 'Only 1 '+u.c_i_type__c+' team member can be added';
                                okToAdd = false; 
                                break;
                            } else {
                                TMlobs.add(u.c_i_type__c);
                                if(u.c_i_type__c == 'Commercial Card Sales Consultant'){
                                    TMCCSCUser=u.Name; TMCCSCarea=u.Area__c; TMCCSCregion=u.Region__c;                          
                                }
                                if(u.c_i_type__c == 'Treasury Management Sales Consultant'){
                                    TMSSUser=u.Name; TMSSArea=u.Area__c; TMSSRegion=u.Region__c;                            
                                }
                                if(u.c_i_type__c == 'Treasury Management Officer'){
                                    TMOUser=u.Name; TMOUserArea=u.Area__c; TMOUserRegion=u.Region__c;
                                }
                                if(u.c_i_type__c == 'Treasury Management Sales Associate'){
                                    TMassociate = u.Name; TMassociateArea = u.Area__c; TMassociateRegion = u.Region__c;
                                }                    
                            }            
                            continue;
                        }
                        if (lobs.contains (u.lob__c)&&(trigger.isInsert)&&(usrProfileName != 'System Administrator')) {
                            ErrMSG = 'Only 1 '+u.lob__c+' team member can be added';                
                            okToAdd = false;
                            break;
                        } else {
                            lobs.add(u.lob__c);
                        }            
                    }
                }
            }
            if (!okToAdd){
                Trigger.New[0].addError(ErrMSG);
             } else {
                //Update Opportunity record with TM users names
                if ((TMCCSCUser != '') || (TMSSUser != '') || (TMOUser != '') || (SBAUser != '')){
                    Opportunity thisOpp = [SELECT id, Treasury_Management_Sales_Consultant__c, Treasury_Management_Sales_Consultant_Are__c, Treasury_Management_Sales_Consultant_Reg__c, 
                                           Commercial_Card_Sales_Consultant__c, Commercial_Card_Consultant_Area__c, Commercial_Card_Consultant_Region__c, 
                                           Treasury_Management_Officer__c,  Treasury_Management_Officer_Area__c, Treasury_Management_Officer_Region__c, Treasury_Management_Sales_Associate__c, 
                                           Treasury_Management_Sales_Associate_Area__c, Treasury_Management_Sales_Associate_Reg__c, Business_Banking_SBA__c, Business_Banking_SBA_Area__c,
                                           Business_Banking_SBA_Region__c
                                           FROM opportunity where id = :Trigger.new[0].OpportunityId];
                    if(thisOpp.id != null){ 
                        if (TMCCSCUser != ''){thisOpp.Commercial_Card_Sales_Consultant__c = TMCCSCUser;}
                        if (TMCCSCarea != ''){thisOpp.Commercial_Card_Consultant_Area__c = TMCCSCarea;}
                        if( TMCCSCregion != ''){thisOpp.Commercial_Card_Consultant_Region__c = TMCCSCregion;} 
                        if (TMSSUser != ''){thisOpp.Treasury_Management_Sales_Consultant__c = TMSSUser;} 
                        if (TMSSArea != ''){thisOpp.Treasury_Management_Sales_Consultant_Are__c = TMSSArea;} 
                        if( TMSSRegion != ''){thisOpp.Treasury_Management_Sales_Consultant_Reg__c = TMSSRegion;}
                        if (TMOUser != ''){thisOpp.Treasury_Management_Officer__c = TMOUser;}
                        if (TMOUserArea != ''){thisOpp.Treasury_Management_Officer_Area__c = TMOUserArea;}
                        if (TMOUserRegion != ''){thisOpp.Treasury_Management_Officer_Region__c = TMOUserRegion;}
                        if (TMassociate != ''){thisOpp.Treasury_Management_Sales_Associate__c = TMassociate;}
                        if (TMassociateArea != ''){thisOpp.Treasury_Management_Sales_Associate_Area__c = TMassociateArea;}
                        if (TMassociateRegion != ''){thisOpp.Treasury_Management_Sales_Associate_Reg__c = TMassociateRegion;}                             
                        //Dale Blocker 05-15-2014
                        if (SBAUser != ''){thisOpp.Business_Banking_SBA__c = SBAUser;}
                        if (SBAUserArea != ''){thisOpp.Business_Banking_SBA_Area__c = SBAUserArea;}
                        if (SBAUserRegion != ''){thisOpp.Business_Banking_SBA_Region__c = SBAUserRegion;}                             
                        //Dale Blocker 05-15-2014
                        try {
                            if(trigger.isBefore){
                                update thisOpp;
                            }
                        }catch (DmlException e) {
                            Trigger.New[0].addError('There was an issue updating the Pipeline with Team Members');
                            System.debug('System-generated error message: '+e.getMessage());
                        }
                    }         
                }            
            }       
        }else if(Trigger.isDelete){
            //Remove TM User from Opportunity record
            for(OpportunityTeamMember x : Trigger.old){
                if(x.LOB__c == 'Treasury Management'){
                    Opportunity thisOpp = [SELECT id, Treasury_Management_Officer__c, Treasury_Management_Officer_Area__c, Treasury_Management_Officer_Region__c, 
                                          Commercial_Card_Sales_Consultant__c, Commercial_Card_Consultant_Area__c, Commercial_Card_Consultant_Region__c, 
                                          Treasury_Management_Sales_Consultant__c, Treasury_Management_Sales_Consultant_Are__c, Treasury_Management_Sales_Consultant_Reg__c,
                                          Treasury_Management_Sales_Associate__c, Treasury_Management_Sales_Associate_Area__c, Treasury_Management_Sales_Associate_Reg__c
                                          FROM opportunity WHERE id = :Trigger.old[0].OpportunityId];                    
                    if(x.Business_Group_Type__c == 'Commercial Card Sales Consultant'){
                        thisOpp.Commercial_Card_Sales_Consultant__c = '';
                        thisOpp.Commercial_Card_Consultant_Area__c = '';
                        thisOpp.Commercial_Card_Consultant_Region__c = '';                
                    }
                    if(x.Business_Group_Type__c == 'Treasury Management Sales Consultant'){
                        thisOpp.Treasury_Management_Sales_Consultant__c = '';
                        thisOpp.Treasury_Management_Sales_Consultant_Are__c = '';
                        thisOpp.Treasury_Management_Sales_Consultant_Reg__c = '';                     
                    }
                    if(x.Business_Group_Type__c == 'Treasury Management Officer'){
                        thisOpp.Treasury_Management_Officer__c = '';
                        thisOpp.Treasury_Management_Officer_Area__c = '';
                        thisOpp.Treasury_Management_Officer_Region__c = '';
                    }
                    if(x.Business_Group_Type__c == 'Treasury Management Sales Associate')
                    {
                        thisOpp.Treasury_Management_Sales_Associate__c = '';
                        thisOpp.Treasury_Management_Sales_Associate_Area__c = '';
                        thisOpp.Treasury_Management_Sales_Associate_Reg__c = '';
                    }try {
                        update thisOpp;
                    } catch (DmlException e) {
                             Trigger.old[0].addError('There was an issue removing the Treasury Management Team Member from the Pipeline.');
                    }                    
                }else if(x.LOB__c == 'Business Community Banking'){
                    //Dale Blocker 05-15-2014
                    Opportunity thisOpp = [SELECT id, Business_Banking_SBA__c, Business_Banking_SBA_Area__c, Business_Banking_SBA_Region__c
                                          FROM opportunity WHERE id = :Trigger.old[0].OpportunityId];                    
                    if(x.Business_Group_Type__c == 'Business Banking SBA'){
                        thisOpp.Business_Banking_SBA__c = '';
                        thisOpp.Business_Banking_SBA_Area__c = '';
                        thisOpp.Business_Banking_SBA_Region__c = '';                
                    }try {
                        update thisOpp;
                    } catch (DmlException e) {
                             Trigger.old[0].addError('There was an issue removing the Business Community Banking Team Member from the Pipeline.');
                    }     
                    //Dale Blocker 05-15-2014
                }                
            }
        }
    }
 }