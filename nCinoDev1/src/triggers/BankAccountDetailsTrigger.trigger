trigger BankAccountDetailsTrigger on Bank_Account_Details__c (after insert,before insert,before update) {
/*
    Descrption - 
    Call Sharing code when inserted during Nightly migration
    If closed , determine if Bank account officer is existing OneView user.
        if not, use client owner.
        this is used in closure notification workflow
*/
if (Trigger_Setting__c.getValues('Bank_Account_Details__c')==null || 
    Trigger_Setting__c.getValues('Bank_Account_Details__c').isActive__c==true) {     

    
    if(trigger.isAfter && trigger.isInsert) {   
        AllRelationshipTriggerHandler handler = new AllRelationshipTriggerHandler();
        String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;
        if (usrProfileName != 'Regions Migration') {    
            //DO NOTHING
        }else if(Trigger.isInsert && Trigger.isAfter){
            handler.OnAfterNightlyBankAccountDetailsInsert(Trigger.new);
        }
    }

    // executes on both before insert and before update
    if(trigger.isBefore){ 
        map<String,String> accOfficeCodeMap = new map<String,String>();
        map<String,String> associateMap = new map<String,String>();
        map<String,String> userEmail= new map<String,String>();
    
        list<Bank_Account_Details__c> bankacclist = new list<Bank_Account_Details__c>();

        for(user associate : [select id,email,Associate_ID__c from user Where  isActive = true]){
            if(associate.email != null){
                associateMap.put(string.valueof(associate.Associate_ID__c ),string.valueof(associate.email));                           
                userEmail.put(string.valueof(associate.Id).substring(0,15),string.valueof(associate.email));
            }   
        }

            for(Bank_Account_Details__c bac : trigger.new ){                               
                if(bac.Closed_Indicator__c == 'Yes' ){
                    if( bac.Account_Officer_Code__c != null  && associateMap.containsKey(string.valueof(bac.Account_Officer_Code__c))) {                           
                        bac.Associate_Email__c = associateMap.get(string.valueof(bac.Account_Officer_Code__c));
                       
                    } else {                           
                          if( bac.Client_Owner_Id__c != null && userEmail.containsKey(bac.Client_Owner_Id__c)) {
                             bac.Associate_Email__c = userEmail.get(bac.Client_Owner_Id__c);                                                                                  
                          }
                    } 
                 }
             }  
    }
} 
}