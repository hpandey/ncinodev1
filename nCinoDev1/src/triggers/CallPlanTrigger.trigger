//Modified for //Story S-270660, Task T-335392 - Rita W  11/19/14

trigger CallPlanTrigger on Call_Plan__c (before delete, after insert) {
/*
    Descrption - 
    Call Sharing code when inserted
    Validate that only Client Owner, client team or admins can create call plan
    
    2013-08-23 PMartin - only check access on non-COI clients
*/

CallPlanEmailParticipantsTriggerHandler EmailHandler = new CallPlanEmailParticipantsTriggerHandler();    
    
if(Trigger.isAfter){
   if (Trigger_Setting__c.getValues('Call_Plan__c')==null || 
      Trigger_Setting__c.getValues('Call_Plan__c').isActive__c==true) { 
    
    AllRelationshipTriggerHandler handler = new AllRelationshipTriggerHandler();
    String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;
    if (usrProfileName == 'Regions Migration' || usrProfileName=='System Administrator') {
        //DO NOTHING
    }else if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterCallPlanInsert(Trigger.new);
        for (Call_Plan__c cp : Trigger.new) {
           String clientRecType = [Select recordtype.name from account where id = :cp.client__c].recordtype.name;
           If (clientRecType == 'COI' || clientRecType == 'Internal COI') {
               Continue;
           }
           List<AccountShare> acs = [Select Id from AccountShare 
                                where RowCause in ('Team','Owner') 
                                and AccountId = :cp.Client__c 
                                and UserorGroupId = :Userinfo.getUserId() ];
                                
            if (acs.size()==0){
                cp.addError('Only the Client Owner, Team Members or Admin can add Call Plan');
            }
        } 
        //Dale Blocker - Case 1383 6/6/14
        //this next section it specifically for send account team emails on emerging risk.
        //the code below has nothing to do with the code above.
        List<Call_Plan__c> callplan = new List<Call_Plan__c>();
        for( Call_Plan__c obj : Trigger.new )
        {
            callplan.add(obj);
        }
        if(callplan.size() > 0){
            if(callplan[0].Status__c == 'Completed' && callplan[0].Changes_Business_or_Financial_Profile__c == 'Yes'){
                //CallPlanEmailAccountTeamTriggerHandler EmailHandler = new CallPlanEmailAccountTeamTriggerHandler();
                //EmailHandler.SendAccountTeamEmails(callplan); 
                
                //Story S-271020  Task # T-335124 Send emails to Participants__c if Notification flag = true
                //and send emails to additional email recipients on the Call Plan record. (RW 12/2/2014)
                //EmailHandler.SendParticipantEmails(callplan);
            }
        }      
    }else if(Trigger.isUpdate){
        List<Call_Plan__c> callplan = new List<Call_Plan__c>();
        for( Call_Plan__c obj : Trigger.new )
        {
            callplan.add(obj);
        }
        if(callplan.size() > 0){
            if(callplan[0].Status__c == 'Completed' && callplan[0].Changes_Business_or_Financial_Profile__c == 'Yes'){
                //CallPlanEmailAccountTeamTriggerHandler EmailHandler = new CallPlanEmailAccountTeamTriggerHandler();
                //EmailHandler.SendAccountTeamEmails(callplan);
                
                //Story S-271020  Task # T-335124 Send emails to Participants__c if Notification flag = true
                //and send emails to additional email recipients on the Call Plan record. (RW 12/2/2014)
                //EmailHandler.SendParticipantEmails(callplan);
            }
        }      
    }
   }
}
if(Trigger.isBefore){
    for (Call_Plan__c p : Trigger.old) {
        // loop through existing participants
        List <Participant__c> parts = [SELECT Id from Participant__c where Call_Plan__c = :Trigger.old[0].Id];
        Integer partsSize = parts.size();
        If (partsSize > 0){
            p.addError('Please delete participants before deleting Call Plan.');
        }
    }
}

/*
//Story S-270660, Task T-335392 - Rita W  11/19/14   
if(Trigger.isAfter) {
    if (Trigger.isInsert) {
        set<Id> ObjIdsToFollow = new set<Id>();
        
        try {
            for(Call_Plan__c cp : Trigger.New){ 
                ObjIdsToFollow.add(cp.Id); 
            } 
            if(ObjIdsToFollow!= null && ObjIdsToFollow.size()>0) { 
                entitySubscriptionHandler.followRecords(ObjIdsToFollow,UserInfo.getUserId()); 
            }
        }           
            catch(exception ex){
                system.debug('Exception occur->'+ex);
            }           
        
        }
    }    
*/    
    
}