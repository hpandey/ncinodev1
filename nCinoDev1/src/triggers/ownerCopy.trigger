trigger ownerCopy on Account (before Insert, before Update, after Insert, after Update) {
/*
    Descrption - 
    Call Sharing code when inserted during Nightly migration
    Copy owner to custom field - NOT REQUIRED SUMMER13
*/
 if (Trigger_Setting__c.getValues('Account')==null || 
      Trigger_Setting__c.getValues('Account').isActive__c==true){       
    AllRelationshipTriggerHandler handler1 = new AllRelationshipTriggerHandler();
    //YourTriggerHandler handler2 = new YourTriggerHandler();
    String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;
    if (usrProfileName != 'Regions Migration') {
        //DO NOTHING
    }else if(Trigger.isInsert && Trigger.isAfter){
        handler1.OnAfterNightlyClientInsert(Trigger.new);
    }else if(Trigger.isUpdate && Trigger.isBefore){
        List<Account> updateRecords = new List<Account>();
        for( Account obj : Trigger.new )
        {
            if( Trigger.oldMap.get( obj.Id ).OwnerId != obj.OwnerId ){
                updateRecords.add(obj);
            }
        }
        if(updateRecords.size() > 0){
            handler1.OnBeforeNightlyClientUpdate(updateRecords);
        }
    }
    
    if (usrProfileName == 'System Administrator' || usrProfileName == 'Regions Migration') {
		 if(Trigger.isAfter && Trigger.isUpdate){      
    	    List<Account> lstAccnts = new List<Account>();
	        for(Account otm : Trigger.New){
	            lstAccnts.add(otm);
	        }    	        	
        	List<Contact> UpdateContacts = new List<Contact>([SELECT Relationship__c, AccountId FROM Contact where AccountId in :lstAccnts]); 
			for(Contact cnt : updateContacts){
            	for (Account accnt : lstAccnts){
            		If (cnt.AccountId == accnt.Id){
            			cnt.Relationship__c = accnt.Relationship__c;
            		}
            	}
	        }
	        update UpdateContacts;    	        	
        }
	 }   
    // handle arbitrary number of opps
    /*
    if(Trigger.isBefore){
        for(Account x : Trigger.New){
    
            // check that owner is a user (not a queue)
            if( ((String)x.OwnerId).substring(0,3) == '005' ){
                x.Owner_Copy__c = x.OwnerId;
            }
            else{
                // in case of Queue we clear out our copy field
                x.Owner_Copy__c = null;
            }
        }
    }
    */
  }
}