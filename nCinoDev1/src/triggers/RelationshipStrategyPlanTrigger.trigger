trigger RelationshipStrategyPlanTrigger on Relationship_Strategy_Plan__c (after insert) {
/*
    Descrption - 
    Call Sharing code when inserted
    Validate that only Client Owner, client team or admins can create RSP
*/
  if (Trigger_Setting__c.getValues('Relationship_Strategy_Plan__c')==null || 
      Trigger_Setting__c.getValues('Relationship_Strategy_Plan__c').isActive__c==true) { 
    AllRelationshipTriggerHandler handler = new AllRelationshipTriggerHandler();
    String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;
    if (usrProfileName == 'Regions Migration' || usrProfileName=='System Administrator') {
        //DO NOTHING
    }else if(Trigger.isInsert && Trigger.isAfter){
       handler.OnAfterRSPInsert(Trigger.new);
      
       List<Id> relationShipId = new List<Id>();
       for (Relationship_Strategy_Plan__c r : Trigger.new) {
           relationShipId.add(r.Relationship__c);
       }
    
       Map<Id, Relationship__c> relationShipMap = new Map<Id, Relationship__c> 
                                                  ([Select Id, OwnerId from RelationShip__c where Id in :relationShipId]);
       
       //modified code... validate if the current user is Rel. manager or client team member...Ephrem 05-14-2014

       Set<Id> acctTeamSet = new Set<Id>();  
       Map<Id, Set<Id>> accountTeamMap = new Map<Id, Set<Id>>();
       
       for (Account acc:[Select Relationship__c, (Select UserId From AccountTeamMembers) From Account where Relationship__c in :relationShipId]) {
          for (AccountTeamMember a : acc.AccountTeamMembers) {
               acctTeamSet.add(a.UserId);
          }
       }
       //Case 1466 - Allow only one TM Strategy Plan per Relationship.  
       //Code changes marked by comment lines... Ephrem - 05-2015
       //*************************
       RecordType RSPrecordType = [select id from recordtype where Name='TM Strategy Plan' limit 1];        
       //*************************
                                                         
       for (Relationship_Strategy_Plan__c r : Trigger.new) {            
            if(UserInfo.getUserId() != relationShipMap.get(r.Relationship__c).OwnerId){
                if(!acctTeamSet.contains(UserInfo.getUserId())){
                       r.addError('Only the Relationship Owner, Related Client Team Members, or Admins can add Relationship Strategy Plan');
               }
            }else{}
       //************************Case 1466...
           Integer tmsp = [SELECT count() from Relationship_Strategy_Plan__c where recordtypeid=: RSPrecordType.id and relationship__c=:r.relationship__c]; 
                               
           if(tmsp > 1){
            r.addError('Only one TM Strategy Plan can be created per Relationship. Please edit the existing TM Strategy Plan.');
           }else{}
        //************************              
       }
       
       /*
       //T-181938 Jeff Hulslander
       Map<Id, Set<Id>> accountTeamMap = new Map<Id, Set<Id>>();
       for (Account acc: 
                   [Select Relationship__c, (Select UserId From AccountTeamMembers) From Account where Relationship__c in :relationShipId]) {
           Set<Id> acctTeamSet = new Set<Id>();
           for (AccountTeamMember a : acc.AccountTeamMembers) {
               acctTeamSet.add(a.UserId);
           }
           accountTeamMap.put(acc.Relationship__c, acctTeamSet);
       }
       //End Changes 
             
       for (Relationship_Strategy_Plan__c r : Trigger.new) {
           if (relationShipMap.get(r.Relationship__c).OwnerId != r.OwnerId){
               //T-181938 Jeff Hulslander
               Set<Id> acctTeamSet = new Set<Id>();
               acctTeamSet = accountTeamMap.get(r.Relationship__c);
               if (!acctTeamSet.contains(r.OwnerId)){
                   r.addError('Only the Relationship Owner, Related Client Team Members, or Admins can add Relationship Strategy Plan');
               }
               //End Changes
           }
       }*/
    }
  }
}