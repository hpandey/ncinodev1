trigger AttachmentsTrigger on Attachment (before insert) {
	set<String> setExtNotAllowed = new set<String> {'exe','dll', 'bat', 'cmd', 'msi'};
	for (Attachment attachment :Trigger.new)
	{
		String strFilename = attachment.Name.toLowerCase();
		List<String> parts = strFilename.splitByCharacterType();
		if(setExtNotAllowed.Contains(parts[parts.size()-1]))
		{
			attachment.addError('Executable files cannot be attached.');
		}
	}
}