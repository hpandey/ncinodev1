trigger updateRestrictedclientInfo  on Risk_Assessment_Review__c (after insert,after update) {
/*
    Descrption - 
    Update the Date Last Approved on the Risk Assessment
*/

    Id RT_ID = Schema.SObjectType.Risk_Assessment_Review__c.getRecordTypeInfosByName().get('Risk Assessment').getRecordTypeId();
    Id RT_ConductReview = Schema.SObjectType.Risk_Assessment_Review__c.getRecordTypeInfosByName().get('Conduct Review').getRecordTypeId(); 
    Map<Id,Decimal> riskMap = new Map<Id,Decimal>();
    Map<Id,Date> riskMapCR = new Map<Id,Date>();    
    
    for(Risk_Assessment_Review__c riskassesment : trigger.new) {
    if(riskassesment.recordTypeID == RT_ID)
        riskMap.put(riskassesment.Restricted_Client_Information__c,riskassesment.Risk_Rating__c);
    if(riskassesment.recordTypeID == RT_ConductReview)
        riskMapCR.put(riskassesment.Restricted_Client_Information__c,riskassesment.Date_Review_Approved__c);         
    }

if(riskMapCR != null) {

    List<Restricted_Client_Information__c> resClientinfosCR = new List<Restricted_Client_Information__c>();
    
    for(Restricted_Client_Information__c resClientCR : [SELECT ID,Date_Last_Review_Approved__c FROM  Restricted_Client_Information__c WHERE ID  IN: riskMapCR.keyset()])
    {
        Restricted_Client_Information__c oresc = new Restricted_Client_Information__c(Id=resClientCR.Id);
        oresc.Date_Last_Review_Approved__c  = riskMapCR.get(resClientCR.ID);
        
        resClientinfosCR.add(oresc);    
    }
       update resClientinfosCR;         
}


}