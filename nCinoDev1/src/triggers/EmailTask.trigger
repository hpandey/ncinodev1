trigger EmailTask on Task (after insert, after update) {

    for (Task t: Trigger.new)
    {
        if(t.Status == 'Completed' && t.OwnerId != t.CreatedById && t.Recieve_Email_when_Completed__c == True){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            String emailAddr = [select Email from User where Id = :t.CreatedById].Email;
            String ownerName = [select Name from User where Id = :t.OwnerId].Name;

            String[] toAddresses = new String[] {emailAddr};
            mail.setToAddresses(toAddresses);

            mail.setSubject('Task Completed');

            mail.setPlainTextBody(
                '<b>Subject: </b>' + t.Subject + '\n' + 
                '<b>By: </b>' + ownerName + '\n' +
                '<b>Due Date: </b>' + t.ActivityDate + '\n' +
                '<b>Priority: </b>' + t.Priority + '\n' +
                '<b>Status: </b>' + t.Status + '\n' + '\n' +
                '<b>Comments: </b>' + '\n' +
                t.Description
            
            );
            mail.setHtmlBody(
                '<b>Subject: </b>' + t.Subject + '<br/>' + 
                '<b>By: </b>' + ownerName + '<br/>' +
                '<b>Due Date: </b>' + t.ActivityDate + '<br/>' +
                '<b>Priority: </b>' + t.Priority + '<br/>' +
                '<b>Status: </b>' + t.Status + '<br/>' + '<br/>' +
                '<b>Comments: </b>' + '<br/>' +
                t.Description
            );

            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}