trigger PDMtrigger on PDM__c (before insert, after insert) {

    /*
        Description - 
        Call Sharing code when inserted
        Validate that only Client Owner, client team or admins can create PDM
    */
    if(Trigger.isAfter){
      if (Trigger_Setting__c.getValues('PDM__c')==null || 
          Trigger_Setting__c.getValues('PDM__c').isActive__c==true) {   
        AllRelationshipTriggerHandler handler = new AllRelationshipTriggerHandler();
        String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;
        if (usrProfileName == 'Regions Migration' || usrProfileName=='System Administrator'  ) {
            //DO NOTHING
        }else if(Trigger.isInsert && Trigger.isAfter){
           handler.OnAfterPDMInsert(Trigger.new);           
                       
           List<Id> relationShipId = new List<Id>();
           for (PDM__c p : Trigger.new) {
               relationShipId.add(p.Relationship__c);
           }
        
           Map<Id, Relationship__c> relationShipMap = new Map<Id, Relationship__c> 
                                                      ([Select Id, OwnerId from RelationShip__c where Id in :relationShipId ]);
           
           /****************Case 1788...only client owner, client team or admins can create PDM...
           ***see the new logic after the commented out code block...added by Ephrem Tekle 04-2015********

           for (PDM__c p : Trigger.new) {
            if (relationShipMap.get(p.Relationship__c).OwnerId != 
            p.OwnerId)
                p.addError('Only the Relationship Owner or Admin can add PDM');
           }
           ***************************************************************************/
           
           Set<Id> acctTeamSet = new Set<Id>();  
              
           for (Account acc:[Select Relationship__c, (Select UserId From AccountTeamMembers) From Account where Relationship__c in :relationShipId]) {
               for (AccountTeamMember a : acc.AccountTeamMembers) {
                   acctTeamSet.add(a.UserId);
               }
           }
            
           for (PDM__c p : Trigger.new) {
               if(UserInfo.getUserId() != relationShipMap.get(p.Relationship__c).OwnerId){
                   if(!acctTeamSet.contains(UserInfo.getUserId())){
                       p.addError('Only the Relationship Owner, Related Client Team Members, or Admins can add PDM');
                   }
               }else{}              
           }
        }
      }
    }
    //Added 9/4 by Jeff H (T-175121) - Validate only one PDM record per relationship
    if(Trigger.isBefore){
        for (PDM__c p : Trigger.new) {
            // loop through existing participants
            List <PDM__c> pdms = [SELECT Id from PDM__c where Relationship__c= :Trigger.new[0].Relationship__c];
            Integer pdmsSize = pdms.size();
            If (pdmsSize > 0){
                p.addError('Only one PDM record may be created per relationship record.');
            }
        }
    }
}