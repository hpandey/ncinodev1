trigger filterChatterFeedItem on FeedItem (before insert, before update) 
{
    set<String> setExtNotAllowed = new set<String> {'exe', 'dll', 'bat', 'cmd', 'msi'};	
	for (FeedItem FI : Trigger.new) {
		system.debug(FI);	
		If(FI.ContentFileName != null) {	
		    String strFilename = FI.ContentFileName.toLowerCase();
	    	List<String> parts = strFilename.splitByCharacterType();
	        if (setExtNotAllowed.Contains(parts[parts.size()-1])) {
	            FI.addError('Executables are not files are allowed.');
	        }
	        if (FI.ContentData.size() > 26214400) {
	        	FI.addError('Files cannot be greater than 25mb.' + FI.ContentData);
	        }	
		}	
	}
    String[] filterFields = new String[]{'Body'};

    redacted.filterObject(trigger.new,filterFields);   
}