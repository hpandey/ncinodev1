/************************************************************************************
 Trigger Name        : AsynchronousQueueTrigger
 Version             : 1.0
 Created Date        : 29th Feb 2016
 Function            : Trigger for Asynchronous Process Log Object.
 Author              : Sumit Sagar
 Modification Log    :
* Developer                         Date                   Description
* ----------------------------------------------------------------------------                 
* Sumit Sagar                     2/29/2016                Original Version
*************************************************************************************/
trigger AsynchronousQueueTrigger on Asynchronous_Queue__c (Before Insert, After Insert , After Update ) {
    Trigger_Setting__c triggerSetting  = Trigger_Setting__c.getValues(environmentVariables.AsyncProcessLogAPI);
    
    if(triggerSetting == null || triggerSetting.isActive__c){
        AsynchronousQueueTriggerHandler AsynchronousProcessLogTriggerHandlerObj = new AsynchronousQueueTriggerHandler();
        TriggerDispatcher.Run(AsynchronousProcessLogTriggerHandlerObj);
    }

}