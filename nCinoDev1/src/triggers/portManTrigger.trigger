trigger portManTrigger on Portfolio_Management__c (after insert, after update) {
    portManTriggerHandler handler = new portManTriggerHandler();
    if (Trigger_Setting__c.getValues('Portfolio_Management__c')==null || Trigger_Setting__c.getValues('Portfolio_Management__c').isActive__c==true) {
        if(trigger.isAfter){
            handler.sharePortfolio(Trigger.new);
        }
    }
}