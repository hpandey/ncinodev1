//Edited to deploy under the oneview admin 

trigger ContactTrigger on Contact (before insert, before update) {
/*
    Descrption - 
    Validate that only Client Owner, client team or admins can create contacts
*/
  if (Trigger_Setting__c.getValues('Contact')==null || 
      Trigger_Setting__c.getValues('Contact').isActive__c==true) {
    ContactTriggerHandler handler = new ContactTriggerHandler();

    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    if(Trigger.isUpdate && Trigger.isBefore){
        handler.OnBeforeUpdate(Trigger.new);
    }    
  }
}