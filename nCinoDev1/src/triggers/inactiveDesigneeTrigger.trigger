//------------------------------------------------------------------------------------------------------------------------------------------
//Case 2266 - remove a user from the Designee object when moved to an Inactive status.
//The trigger is calling the handler class that utilizes the @future method ...
//The trigger fires whenever any user's status is modified to an inactive user...
//It checks if an inactive user is in a Designee object and if found, it will be removed from Designee object
//Ephrem Tekle - 08-2015
//------------------------------------------------------------------------------------------------------------------------------------------

trigger inactiveDesigneeTrigger on User (after update) {
    if ((Trigger_Setting__c.getValues('User')==null)|| (Trigger_Setting__c.getValues('User').isActive__c==true)) {
        for(User u: Trigger.new){
            if(u.isActive == false){
                deleteInactiveDesigneeHandler.processInactiveDesignee();
            }else{}
        }
    }
}