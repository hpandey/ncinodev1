/************************************************************************************
 Trigger Name        : AccountTrigger
 Version             : 1.0
 Created Date        : 18th March 2016
 Function            : Trigger for Loan Object.
 Author              : Deloitte Consulting
 Modification Log    :
* Developer                         Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar                 3/18/2016                Original Version
*************************************************************************************/

trigger AccountTrigger on Account (Before Insert, After Insert, Before Update, After Update, Before Delete, After Delete) {

    Trigger_Setting__c triggerSetting  = Trigger_Setting__c.getValues(environmentVariables.accountAPI);
    
    if(triggerSetting == null || triggerSetting.isActive__c){
        AccountTriggerHandler accTriggerHandlerObj = new AccountTriggerHandler();
        TriggerDispatcher.Run(accTriggerHandlerObj);
    }
    
}