/************************************************************************************
 Trigger Name        : EntityInvolvementTrigger
 Version             : 1.0
 Created Date        : 1st Mar 2016
 Function            : Trigger for Entity Involvement Object.
 Author              : Deloitte Consulting
 Modification Log    :
* Developer                         Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar                 2/16/2016                Original Version
*************************************************************************************/

trigger EntityInvolvementTrigger on LLC_BI__Legal_Entities__c (Before Insert, After Insert, Before Update, After Update, Before Delete, After Delete, After Undelete) {

    Trigger_Setting__c triggerSetting  = Trigger_Setting__c.getValues(environmentVariables.entityInvolvementAPI);
    
    if(triggerSetting == null || triggerSetting.isActive__c){
        EntityInvolvementTriggerHandler EntityInvolvementTriggerHandlerObj = new EntityInvolvementTriggerHandler();
        TriggerDispatcher.Run(EntityInvolvementTriggerHandlerObj);
    }
}