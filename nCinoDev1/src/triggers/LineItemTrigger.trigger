trigger LineItemTrigger on OpportunityLineItem (After Insert, After Update) {
/*
    Descrption - 
    Validate only multiple line items for TM
    Copy product group upto pipeline - PMartin, comment out for now
*/
  if (Trigger_Setting__c.getValues('OpportunityLineItem')==null || 
      Trigger_Setting__c.getValues('OpportunityLineItem').isActive__c==true) {
    Map<ID, LIST<String>> opptProductGroup = new Map<ID,List<String>>();   
    
    User usr = [select u.Profile.Name, u.LOB__c from User u where u.id = :Userinfo.getUserId()];
        
    if (usr.Profile.Name=='System Administrator') {
            // DO Nothing
    }
    if (usr.LOB__c=='Treasury Management') {
      boolean notTM = false;
      boolean notSingle = false;
      Integer count;   
	  for (OpportunityLineItem optyLineItem : Trigger.new){
	      count = 0;
	      for (OpportunityLineItem oli : [Select product_group__c from OpportunityLineItem where OpportunityId=:optyLineItem.OpportunityId]) {
			count = count + 1;         
			if (count > 1) notSingle = true;
			if (oli.Product_Group__c != 'Treasury Management') notTM = true;
			if (notSingle && notTM){
			   optyLineItem.adderror('Can only add Multiple Line Items for Treasury Management Products');
			}
	      }
	   }  
	}  
    
	//Dale Blocker - Case 1400 6/24/14
	//copying the FeeType field from the pipelineproduct object to the pipeline object for reporting
	for (OpportunityLineItem optyLineItem : Trigger.new){                
		List<String> product_info = new List<String>();
		product_info.add(optyLineItem.Fee_Type__c);
		opptProductGroup.put(optyLineItem.opportunityId, product_info); 
	}     
	List <Opportunity> opts = [Select id, Fee_Type__c from Opportunity where id in :opptProductGroup.keySet()];
	List<String> product_info2 = new List<String>();
	for (Opportunity o : opts) {
		product_info2 = opptProductGroup.get(o.id);
	    o.Fee_Type__c = product_info2.get(0);
	}
	update opts;
    

//Copied and modified for a different purpose
//Original code from Peter 
   /* 
   for (OpportunityLineItem optyLineItem : Trigger.new){                
        List<String> product_info = new List<String>();
        product_info.add(optyLineItem.Product_group__c);
        product_info.add(optyLineItem.Product_Family__c);
        product_info.add(optyLineItem.Product_Name__c);
        opptProductGroup.put(optyLineItem.opportunityId, product_info); 
        
        }     
   List <Opportunity> opts = [Select id, Product_Group__c, Product_Family__c from Opportunity where id in :opptProductGroup.keySet()];
   List<String> product_info2 = new List<String>();
   for (Opportunity o : opts) {
       product_info2 = opptProductGroup.get(o.id);
       o.Product_Group__c = product_info2.get(0);
       o.Product_Family__c = product_info2.get(1);
       o.Product__c = product_info2.get(2);
   }
   update opts;
   */ 
   }   
}