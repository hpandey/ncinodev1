trigger LoanDocumentTrigger on LLC_BI__LLC_LoanDocument__c (Before Insert, After Insert, Before Update, After Update, Before Delete, After Delete, After Undelete) {
    
    Trigger_Setting__c triggerSetting  = Trigger_Setting__c.getValues(environmentVariables.loanDocumentAPI);
    
    if(triggerSetting == null || triggerSetting.isActive__c){
        LoanDocumentTriggerHandler LoanDocumentTriggerHandlerObj = new LoanDocumentTriggerHandler();
        TriggerDispatcher.Run(LoanDocumentTriggerHandlerObj);
    }
}