<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Acct_Closure_Notification</fullName>
        <description>Acct Closure Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Associate_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Admin_Only/Acct_Closure_Notification</template>
    </alerts>
    <alerts>
        <fullName>X120_Day_Renewal_Email</fullName>
        <description>120 Day Renewal Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X120_Day_Renewal_Emails/X120_Day_Renewal</template>
    </alerts>
    <rules>
        <fullName>120 day renewal</fullName>
        <active>true</active>
        <description>120 days before maturity/financial review, do something</description>
        <formula>AND( 
NOT( Client__r.Relationship__r.Transfer_to_Branch__c = TRUE), 
UPPER(Account_Type__c) = &apos;LOAN&apos;, 
ISPICKVAL(Owner:User.LOB__c, &quot;Business Community Banking&quot;), 
ISPICKVAL(Closed_Indicator__c, &quot;NO&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X120_Day_Renewal_Email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Bank_Account_up_for_Renewal</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Bank_Account_Details__c.Earliest_Review_Date__c</offsetFromField>
            <timeLength>-120</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Acct Closure Notification</fullName>
        <actions>
            <name>Acct_Closure_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notifies Account Officer (Code) if OneView user else Client Owner</description>
        <formula>( ISNEW() &amp;&amp;  NOT( ISBLANK( Associate_Email__c ) ) &amp;&amp; (Associate_Email__c &lt;&gt; &apos;oneviewadmin@regions.com&apos;) )  || ( ISCHANGED(Associate_Email__c )  &amp;&amp; (Associate_Email__c &lt;&gt; &apos;oneviewadmin@regions.com&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Bank_Account_up_for_Renewal</fullName>
        <assignedToType>owner</assignedToType>
        <description>The following credit is scheduled to mature or have a financial review
within the next 120 days.  Please acknowledge receipt of this task within 7
days in order to move this credit to your active renewal pipeline.</description>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Bank Account up for Renewal</subject>
    </tasks>
</Workflow>
