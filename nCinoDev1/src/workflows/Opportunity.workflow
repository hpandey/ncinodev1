<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CT_Urgent_New_Business_Approval</fullName>
        <description>CT Urgent New Business Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>beverly.miles@regions.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dian.wilson@regions.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Local_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Corporate_Trust_Local_Manager_New_Business_Approval_Urgent</template>
    </alerts>
    <alerts>
        <fullName>Corporate_Trust_New_Business_Approval</fullName>
        <description>Corporate Trust New Business Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>CT_AAC</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Corporate_Trust_New_Business_Approval</template>
    </alerts>
    <alerts>
        <fullName>Corporate_Trust_New_Business_Approval_CT_AAC</fullName>
        <description>Corporate Trust New Business Approval CT AAC</description>
        <protected>false</protected>
        <recipients>
            <recipient>CT_AAC</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>amy.benefield@regions.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Corporate_Trust_New_Business_Approval</template>
    </alerts>
    <alerts>
        <fullName>Corporate_Trust_New_Business_Approval_CT_AAC_1</fullName>
        <description>Corporate Trust New Business Approval CT AAC</description>
        <protected>false</protected>
        <recipients>
            <recipient>CT_AAC</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>beverly.miles@regions.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dian.wilson@regions.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Corporate_Trust_New_Business_Approval</template>
    </alerts>
    <alerts>
        <fullName>Corporate_Trust_New_Business_Approved</fullName>
        <description>Corporate Trust New Business Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Corporate_Trust_New_Business_Approved</template>
    </alerts>
    <alerts>
        <fullName>Corporate_Trust_New_Business_Rejected</fullName>
        <description>Corporate Trust New Business Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Corporate_Trust_New_Business_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Corporate_Trust_Urgent_New_Business</fullName>
        <description>Corporate Trust Urgent New Business</description>
        <protected>false</protected>
        <recipients>
            <recipient>amy.benefield@regions.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Corporate_Trust_New_Business_Approval_Urgent</template>
    </alerts>
    <alerts>
        <fullName>Incentive_Approved_Email</fullName>
        <description>Incentive Approved Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Incentive_Approved</template>
    </alerts>
    <alerts>
        <fullName>Incentive_Rejection_Email</fullName>
        <description>Incentive Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Business_Development_Officer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Incentive_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Institutional_Approved_Notification</fullName>
        <description>Institutional Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Institutional_Trust_New_Business_Approved</template>
    </alerts>
    <alerts>
        <fullName>Institutional_Rejection_Notification</fullName>
        <description>Institutional Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Institutional_Trust_New_Business_Rejected</template>
    </alerts>
    <alerts>
        <fullName>LLC_BI__Close_Date_Moved</fullName>
        <ccEmails>Demo@Bankr.com</ccEmails>
        <description>Close Date Moved</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>LLC_BI__Bankr_Templates/LLC_BI__Workflow_Alert_Close_Date_Accelerated</template>
    </alerts>
    <alerts>
        <fullName>LLC_BI__Guarantee_Fee_Not_Paid</fullName>
        <ccEmails>Demo@Bankr.com</ccEmails>
        <description>Guarantee Fee Not Paid</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>LLC_BI__Bankr_Templates/LLC_BI__Guarantee_Fee_Not_Paid</template>
    </alerts>
    <alerts>
        <fullName>LLC_BI__Loan_Withdrawn_Declined</fullName>
        <ccEmails>Demo@Bankr.com</ccEmails>
        <description>Loan Withdrawn / Declined</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>LLC_BI__Bankr_Templates/LLC_BI__Loan_Withdrawn_Declined</template>
    </alerts>
    <alerts>
        <fullName>LLC_BI__Order_New_Appraisal_Alert</fullName>
        <ccEmails>Demo@Bankr.com</ccEmails>
        <description>Order New Appraisal Alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>LLC_BI__Bankr_Templates/LLC_BI__Order_Appraisal</template>
    </alerts>
    <alerts>
        <fullName>LLC_BI__Stage_Change_to_Compliance</fullName>
        <ccEmails>Demo@Bankr.com</ccEmails>
        <description>Stage Change to Compliance</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>LLC_BI__Bankr_Templates/LLC_BI__Stage_Change_Alert</template>
    </alerts>
    <alerts>
        <fullName>NRRE_Approval_Notification</fullName>
        <description>NRRE Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Local_Trust_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/NRRE_New_Business_Approval</template>
    </alerts>
    <alerts>
        <fullName>NRRE_Approved_Notification</fullName>
        <description>NRRE Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/NRRE_New_Business_Approved</template>
    </alerts>
    <alerts>
        <fullName>NRRE_Rejection_Notification</fullName>
        <description>NRRE Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/NRRE_New_Business_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Verify_Exception_to_incentive</fullName>
        <description>Verify Exception to incentive payment</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_BDO_LOB_President__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Regional_Trust_LOB_President__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Approval_Process_Emails/Incentive_Rejected_excpeiont</template>
    </alerts>
    <alerts>
        <fullName>X30_Day_Renewal_Email</fullName>
        <description>30 Day Renewal Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X120_Day_Renewal_Emails/X30_Day_Renewal</template>
    </alerts>
    <alerts>
        <fullName>X60_Day_Renewal_Email</fullName>
        <description>60 Day Renewal Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>X120_Day_Renewal_Emails/X60_Day_Renewal</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_By_LOB_President</fullName>
        <field>Approval_Status__c</field>
        <formula>&quot;Approved By LOB President&quot;</formula>
        <name>Approved By LOB President</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Audit_Banker_Complete_Date</fullName>
        <field>Audit_Banker_Edits_Completed_Date__c</field>
        <formula>Today()</formula>
        <name>Audit  Banker Complete Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Audit_Banker_Edits_Complete_FALSE</fullName>
        <description>Changes the Audit Banker Edits to False if Pipeline is reopened.</description>
        <field>Audit_Banker_Edits_Completed__c</field>
        <literalValue>0</literalValue>
        <name>Audit Banker Edits Complete FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Audit_Banker_edits_Completed</fullName>
        <field>Audit_Banker_Edits_Completed__c</field>
        <literalValue>1</literalValue>
        <name>Audit Banker edits Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Audit_Reopen_Date</fullName>
        <field>Audit_Reopened_Date__c</field>
        <formula>Today()</formula>
        <name>Audit Reopen Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Audit_Reopened_FALSE</fullName>
        <description>Changes the Audit Reopen Box to False</description>
        <field>Audit_Reopened__c</field>
        <literalValue>0</literalValue>
        <name>Audit Reopened FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BB_Renewal_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BCB_Renewal_Closed_Pipeline</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>BB Renewal Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BDO_Processed</fullName>
        <field>BDO_Form_Processed_Date__c</field>
        <formula>TODAY()</formula>
        <name>BDO Processed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CT_Approval_submitted</fullName>
        <description>mark when new business approval has been submitted</description>
        <field>CT_Approval_Submitted__c</field>
        <formula>TODAY()</formula>
        <name>CT Approval submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CT_Approved_by_Committee</fullName>
        <description>mark when new business has been approved by committee</description>
        <field>CT_Approved_by_Committee__c</field>
        <formula>TODAY()</formula>
        <name>CT Approved by Committee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Record_Type_to_REB_Open_Pipeline</fullName>
        <field>RecordTypeId</field>
        <lookupValue>REB_Open_Pipeline</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type to REB Open Pipeline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_needs_to_be_merged</fullName>
        <description>Updates Client field Needs to be Merged to Checked</description>
        <field>Needs_to_be_Merged__c</field>
        <literalValue>1</literalValue>
        <name>Client needs to be merged</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_BCB_Pipeline</fullName>
        <description>change the BCB pipeline record type</description>
        <field>RecordTypeId</field>
        <lookupValue>BCB_Closed_Pipeline</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Close BCB Pipeline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Corp_Trust_Pipeline</fullName>
        <description>Change record type to Closed Corp Trust</description>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Corporate_Trust</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Close Corp Trust Pipeline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Inst_Trust_Pipeline</fullName>
        <description>Change record type for Closed Inst Trust Pipeline</description>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Institutional_Trust</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Close Inst Trust Pipeline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_PWM_Other_Pipeline</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Private_Wealth_Other_Products</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Close PWM Other Pipeline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_PWM_Pipeline</fullName>
        <description>Change record type for Closed Won PWM pipeline</description>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Private_Wealth_Trusts_Investments</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Close PWM Pipeline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_RBC_Pipeline</fullName>
        <description>Used with the RBC record type - when pipeline closes updated to RBC closed record type</description>
        <field>RecordTypeId</field>
        <lookupValue>RBC_Closed_Pipeline</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Close RBC Pipeline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_REB_Pipeline</fullName>
        <description>Change the REB pipeline record type</description>
        <field>RecordTypeId</field>
        <lookupValue>REB_Closed_Pipeline</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Close REB Pipeline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed_PWM_Loan</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Private_Wealth_Loans_Deposits</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Closed PWM Loan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Corporate_Trust_Approved_Record_type</fullName>
        <description>Update the Record type to Corporate Trust Approved</description>
        <field>RecordTypeId</field>
        <lookupValue>Corporate_Trust_Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Corporate Trust Approved Record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Corporate_Trust_New_Business_Approved</fullName>
        <description>Update the Stage to Approved by CT AAC</description>
        <field>StageName</field>
        <literalValue>Approved by Committee</literalValue>
        <name>Corporate Trust New Business Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Corporate_Trust_Open_Record_type</fullName>
        <description>Revert pipeline back to open</description>
        <field>RecordTypeId</field>
        <lookupValue>Corporate_Trust</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Corporate Trust Open Record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Form_Processed</fullName>
        <field>Form_Processed__c</field>
        <formula>TODAY()</formula>
        <name>Form Processed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Approval_Date_Blank</fullName>
        <description>When incentive rejected, reset the incentive approval date</description>
        <field>Incentive_Approval_Date__c</field>
        <name>Incentive Approval Date Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Approval_Rejected</fullName>
        <field>Incentive_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Incentive Approval Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Approval_Status_Approved</fullName>
        <description>Update the Incentive Status to Approved</description>
        <field>Incentive_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Incentive Approval - Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Approval_Status_Pending</fullName>
        <description>Once the incentive approval has been initiated, update the Incentive Status to Pending</description>
        <field>Incentive_Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Incentive Approval - Status Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Next_Approver_Initial</fullName>
        <description>Update Next approver, either local Trust Mgr or BDO</description>
        <field>Next_Incentive_Approver__c</field>
        <formula>IF( ISBLANK( Business_Development_Officer__c ) ,  Local_Trust_Manager__r.Full_Name__c ,  Business_Development_Officer__r.Full_Name__c )</formula>
        <name>Incentive Next Approver Initial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Next_Approver_Level2</fullName>
        <description>Update next approver to Incentive Admin Level2</description>
        <field>Next_Incentive_Approver__c</field>
        <formula>&quot;Incentive Admin Level2&quot;</formula>
        <name>Incentive Next Approver Level2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Next_Approver_Step1</fullName>
        <description>Update Next Approver</description>
        <field>Next_Incentive_Approver__c</field>
        <formula>Local_Trust_Manager__r.Full_Name__c</formula>
        <name>Incentive Next Approver Step1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Next_Approver_Step2</fullName>
        <description>Update next approver, either local BDO mgr or regional Trust mgr</description>
        <field>Next_Incentive_Approver__c</field>
        <formula>IF( ISBLANK( Business_Development_Officer__c ) ,  Regional_Trust_Manager__r.Full_Name__c  ,  Local_BDO_Manager__r.Full_Name__c  )</formula>
        <name>Incentive Next Approver Step2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Next_Approver_Step3</fullName>
        <description>Update Next Approver to Regional Bus. Group Manager</description>
        <field>Next_Incentive_Approver__c</field>
        <formula>Regional_Trust_Manager__r.Full_Name__c</formula>
        <name>Incentive Next Approver Step3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Next_Approver_Step4</fullName>
        <description>Update Next approver, either Bus. Group Head (Strategist) or Bus. Group Head</description>
        <field>Next_Incentive_Approver__c</field>
        <formula>IF( ISBLANK( Business_Development_Officer__c ) ,  Regional_Trust_LOB_President__r.Full_Name__c ,  Regional_BDO_Manager__r.Full_Name__c )</formula>
        <name>Incentive Next Approver Step4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Next_Approver_Step5</fullName>
        <description>Update next approver to Bus. Group Head</description>
        <field>Next_Incentive_Approver__c</field>
        <formula>Regional_Trust_LOB_President__r.Full_Name__c</formula>
        <name>Incentive Next Approver Step5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Next_Approver_Step6</fullName>
        <description>Update next approver to either Strategist Manager or Incentive Admin1</description>
        <field>Next_Incentive_Approver__c</field>
        <formula>IF( ISBLANK( Business_Development_Officer__c ) , &apos;Incentive Admin level1&apos; ,  Regional_BDO_LOB_President__r.Full_Name__c  )</formula>
        <name>Incentive Next Approver Step6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Next_Approver_Step7</fullName>
        <description>Update next approver to Incentive Admin Level1</description>
        <field>Next_Incentive_Approver__c</field>
        <formula>&quot;Incentive Admin Level1&quot;</formula>
        <name>Incentive Next Approver Step7</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Next_Approver_blank</fullName>
        <description>Update the next approver to blank because of rejection</description>
        <field>Next_Incentive_Approver__c</field>
        <name>Incentive Next Approver blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Record_Type</fullName>
        <description>When Incentive approved, change record type to Incentive</description>
        <field>RecordTypeId</field>
        <lookupValue>incentive</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Incentive Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Status_Admin1</fullName>
        <description>Update status to reflect Level1 approval</description>
        <field>Incentive_Status__c</field>
        <literalValue>Admin1 Approved</literalValue>
        <name>Incentive Status Admin1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Status_Step1</fullName>
        <description>Update Incentive Status after BDO approves</description>
        <field>Incentive_Status__c</field>
        <literalValue>Strategist Approved</literalValue>
        <name>Incentive Status Step1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Status_Step2</fullName>
        <description>Update status after Local Bus. Group Manager approves</description>
        <field>Incentive_Status__c</field>
        <literalValue>Local Bus. Group Manager Approved</literalValue>
        <name>Incentive Status Step2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Status_Step3</fullName>
        <description>Update status based on Regional Bus. Group Manager Approval (Strategist)</description>
        <field>Incentive_Status__c</field>
        <literalValue>Regional Bus. Group Manager Approved (Strategist)</literalValue>
        <name>Incentive Status Step3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Status_Step4</fullName>
        <description>Update status based on Regional Bus. Group Manager approval</description>
        <field>Incentive_Status__c</field>
        <literalValue>Regional Bus. Group Manager Approved</literalValue>
        <name>Incentive Status Step4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incentive_Status_Step5</fullName>
        <description>Update status to reflect Bus. Group Head Approval (Strategist)</description>
        <field>Incentive_Status__c</field>
        <literalValue>Bus. Group Head Approved (Strategist)</literalValue>
        <name>Incentive Status Step5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Institutional_Trust_Approved_Record_type</fullName>
        <description>Update the record type to Institutional Trust Approved</description>
        <field>RecordTypeId</field>
        <lookupValue>Institutional_Trust_Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Institutional Trust Approved Record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Institutional_Trust_Open_record_type</fullName>
        <description>Revert pipeline back to open</description>
        <field>RecordTypeId</field>
        <lookupValue>Institutional_Trust</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Institutional Trust Open record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LLC_BI__Update_Start_Date_of_Current_Stage</fullName>
        <field>LLC_BI__Start_Date_at_Current_Stage__c</field>
        <formula>TODAY()</formula>
        <name>Update Start Date of Current Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LOB_Pres_Approved</fullName>
        <field>Incentive_Status__c</field>
        <literalValue>Bus. Group Head Approved</literalValue>
        <name>Bus. Group Head Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Probablity</fullName>
        <field>Last_Probablity__c</field>
        <formula>PRIORVALUE( Probability )</formula>
        <name>Last Probablity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Sales_Stage</fullName>
        <field>Last_Sales_Stage__c</field>
        <formula>TEXT (PRIORVALUE( StageName ))</formula>
        <name>Last Sales Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPPTY_Record_Type_Update_on_Reopen</fullName>
        <description>Updates the Record Type back to an Open Record Type</description>
        <field>RecordTypeId</field>
        <lookupValue>Open_Pipeline</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>OPPTY Record Type Update on Reopen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Original_Close_Date</fullName>
        <field>Original_Close_Date__c</field>
        <formula>BLANKVALUE(Original_Close_Date__c, CloseDate)</formula>
        <name>Original Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PWM_Closed_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Private_Wealth_Trusts_Investments</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PWM Closed Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PWM_Loans_Deposits_Record_type</fullName>
        <description>Updates record type to Closed PWN Loans &amp; Deposits Pipeline Layout</description>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Private_Wealth_Loans_Deposits</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PWM Loans &amp; Deposits Record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pre_July1_Close_Date</fullName>
        <description>Set close date to Jan 1 2013 for Pre July1 Incentives</description>
        <field>CloseDate</field>
        <formula>DATE(2013,01,01)</formula>
        <name>Pre July1 Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>REB_Open_Pipeline_Sale_Stage</fullName>
        <description>Updated once the record changes from REB Forecast Pipeline to REB Open Pipeline</description>
        <field>StageName</field>
        <literalValue>Qualification</literalValue>
        <name>REB Open Pipeline Sale Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RM_TA_Processed</fullName>
        <field>RM_TA_Form_Processed_Date__c</field>
        <formula>TODAY()</formula>
        <name>RM/TA Processed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BCB_Open_Pipeline</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Close_Date</fullName>
        <description>Set Close date to current date when Stage = Closed Won</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Set Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Incentive_Approval_Date</fullName>
        <field>Incentive_Approval_Date__c</field>
        <formula>today()</formula>
        <name>Set Incentive Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Change</fullName>
        <field>StageName</field>
        <literalValue>Accepted</literalValue>
        <name>Stage Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TM_Closed_Pipeline</fullName>
        <field>RecordTypeId</field>
        <lookupValue>TM_Closed_Pipeline</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>TM Closed Pipeline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TM_Open_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>TM_Open_Pipeline</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>TM Open Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Record_Type_Update_On</fullName>
        <description>Update Opportunity Record type</description>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Pipeline</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>OPPTY Record Type Update on Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Approved</fullName>
        <field>StageName</field>
        <literalValue>Approved by Committee</literalValue>
        <name>Update Stage to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Approved_for_Incentive</fullName>
        <description>Used In NRRE Approval Process</description>
        <field>StageName</field>
        <literalValue>Approved for Incentive</literalValue>
        <name>Update Stage to Approved for Incentive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>30 days remain on 120 renewal</fullName>
        <active>true</active>
        <description>30 days before closing and probability is less than 75%</description>
        <formula>AND($RecordType.Name = &apos;BCB Renewal Pipeline&apos;,(Probability)&lt;0.75,(Probability)&gt;0.00)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X30_Day_Renewal_Email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Renewal_Closes_in_30_Days</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>60 days remain on 120 renewal</fullName>
        <active>true</active>
        <description>60 days before closing and probability is less than 50%</description>
        <formula>AND($RecordType.Name = &apos;BCB Renewal Pipeline&apos;,(Probability)&lt;0.50,(Probability)&gt;0.00)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X60_Day_Renewal_Email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Renewal_Closes_in_60_Days</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Add Relationship to the Opportunity upon creation</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Audit - Banker Complete Edits</fullName>
        <actions>
            <name>Audit_Banker_Complete_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Audit_Banker_edits_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Audit_Reopened_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to Update Audit fields so Auditor is aware that updates have been made.</description>
        <formula>AND( ISCHANGED(StageName) &amp;&amp;  ISPICKVAL(StageName,&quot;Closed Won&quot;), Audit_Reopened__c = TRUE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BB - Audit Open</fullName>
        <actions>
            <name>Audit_Banker_Edits_Complete_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Audit_Reopen_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Original_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Stage_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to reopen the Record Type</description>
        <formula>AND( $User.ProfileId = &apos;00eG00000014KcI&apos; || $User.ProfileId = &apos;00eG0000001GiVu&apos;, RecordTypeId  = &apos;012G00000017XBb&apos;, ISCHANGED( Audit_Reopened__c ) &amp;&amp; Audit_Reopened__c = TRUE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CT New Business unapproved</fullName>
        <actions>
            <name>Corporate_Trust_Open_Record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Corporate Trust,Closed Corporate Trust,Corporate Trust Approved</value>
        </criteriaItems>
        <description>When Trust advisor wants to change new business details to resubmit for approval, stage should be changed to Accepted, then record type reverts to open.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close BB Renewal Pipeline</fullName>
        <actions>
            <name>BB_Renewal_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>BCB Renewal Pipeline</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close BCB Pipeline</fullName>
        <actions>
            <name>Close_BCB_Pipeline</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>BCB Open Pipeline</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>When BCB Pipeline close - won, change record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close Corp Trust Pipeline</fullName>
        <actions>
            <name>Close_Corp_Trust_Pipeline</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Corporate Trust,Corporate Trust Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>When Corp Trust Pipeline close - won, change record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close Inst Trust Pipeline</fullName>
        <actions>
            <name>Close_Inst_Trust_Pipeline</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Institutional Trust,Institutional Trust Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>When Inst Trust Pipeline close - won, change record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close PWM Loan Pipeline</fullName>
        <actions>
            <name>Closed_PWM_Loan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Private Wealth Loans &amp; Deposits</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>When PWM Pipeline close - won, change record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close PWM Other Pipeline</fullName>
        <actions>
            <name>Close_PWM_Other_Pipeline</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Private Wealth Other Products Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>When PWM Pipeline close - won, change record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close PWM Pipeline</fullName>
        <actions>
            <name>Close_PWM_Pipeline</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Private Wealth Trusts &amp; Investments</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>When PWM Pipeline close - won, change record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close RBC Pipeline</fullName>
        <actions>
            <name>Close_RBC_Pipeline</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>RBC Open Pipeline</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <description>Used with RBC record type - when closed updates record type to closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close REB Pipeline</fullName>
        <actions>
            <name>Close_REB_Pipeline</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>REB Open Pipeline</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>When REB Pipeline close - won, change record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close TM Pipeline</fullName>
        <actions>
            <name>TM_Closed_Pipeline</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>TM Open Pipeline</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <description>When TM Pipeline close - won, change record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Won Pipeline - Merge Client</fullName>
        <actions>
            <name>Client_needs_to_be_merged</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Party_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Used to mark Client field Needs to be merged as Checked</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IT New Business unapproved</fullName>
        <actions>
            <name>Institutional_Trust_Open_record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Institutional Trust,Closed Institutional Trust,Institutional Trust Approved</value>
        </criteriaItems>
        <description>When Trust advisor wants to change new business details to resubmit for approval, stage should be changed to Accepted, then record type reverts to open.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Incentive BDO Form Processed</fullName>
        <actions>
            <name>BDO_Processed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the BDO Form Processed to today when incentive status changed to BDO Processed</description>
        <formula>AND( ISCHANGED( Incentive_Status__c ) , ISPICKVAL(Incentive_Status__c, &apos;BDO Processed&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Incentive Form Processed</fullName>
        <actions>
            <name>Form_Processed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the Form Processed to today when incentive status changed to Form Processed</description>
        <formula>AND( ISCHANGED( Incentive_Status__c ) , ISPICKVAL(Incentive_Status__c, &apos;Form Processed&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Incentive RMTA Form Processed</fullName>
        <actions>
            <name>RM_TA_Processed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the RM/TA Form Processed to today when incentive status changed to RM/TA Processed</description>
        <formula>AND( ISCHANGED( Incentive_Status__c ) , ISPICKVAL(Incentive_Status__c, &apos;RM/TA Processed&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LLC_BI__Update Start Date of Current Stage</fullName>
        <actions>
            <name>LLC_BI__Update_Start_Date_of_Current_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(StageName)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Last Sales Stage</fullName>
        <actions>
            <name>Last_Probablity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Last_Sales_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Adds the last stage the pipeline was in prior to be moving to closed lost/won on the opportunity for reporting purposes.</description>
        <formula>AND( ISCHANGED(StageName),   Last_Sales_Stage__c = &apos;&apos;, OR( ISPICKVAL(StageName, &apos;Closed Lost&apos;), ISPICKVAL(StageName, &apos;Closed Won&apos;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPPTY Close</fullName>
        <actions>
            <name>Set_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Opportunity_Record_Type_Update_On</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Changes made when a pipeline is closed won or lost</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPPTY Close - Close Date</fullName>
        <actions>
            <name>Set_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Changes made when a pipeline is closed won or lost</description>
        <formula>AND(  OR( ISPICKVAL(StageName, &apos;Closed Won&apos;) ,  ISPICKVAL(StageName, &apos;Closed Lost&apos;)) ,  OR( $Profile.Name &lt;&gt; &apos;System Administrator&apos;,  $Profile.Name &lt;&gt; &apos;Regions Power User&apos;),  ISCHANGED(StageName),   NOT( Pre_July1_Incenitve__c ),NOT ( lookback__c    ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPPTY Close - Record Type</fullName>
        <actions>
            <name>Update_Opportunity_Record_Type_Update_On</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Open Pipeline</value>
        </criteriaItems>
        <description>Changes made when a pipeline is closed won or lost</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPPTY Reopen</fullName>
        <actions>
            <name>OPPTY_Record_Type_Update_on_Reopen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Qualification,Needs Analysis,Prospecting,Accepted,Negotiation/Review,Proposal/Price Quote,Id. Decision Makers,Lead Generation,Perception Analysis,Value Proposition,Proposed/Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Closed Opportunity</value>
        </criteriaItems>
        <description>When a pipeline is changed back from Closed</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PWM Loans %26 Deposit Pipeline Close - Record Type</fullName>
        <actions>
            <name>PWM_Loans_Deposits_Record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost,Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Private Wealth Loans &amp; Deposits</value>
        </criteriaItems>
        <description>Changes made when a pipeline is closed won or lost</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pre July1 Incentive</fullName>
        <actions>
            <name>Pre_July1_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Pre_July1_Incenitve__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>backdate close date to Jan 1 2013</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>REB Identified Forecast Pipeline</fullName>
        <actions>
            <name>Change_Record_Type_to_REB_Open_Pipeline</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>REB_Open_Pipeline_Sale_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Forecast Pipeline</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Identified</value>
        </criteriaItems>
        <description>When REB Pipeline changes to Identified, change record</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TM - Audit Open for BB</fullName>
        <actions>
            <name>Audit_Banker_Edits_Complete_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Audit_Reopen_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Original_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Stage_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TM_Open_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to reopen the Record Type</description>
        <formula>AND( $User.ProfileId = &apos;00eG00000014KcI&apos; || $User.ProfileId = &apos;00eG0000001GiVu&apos;, RecordTypeId  = &apos;012G0000001NLaP&apos;, ISCHANGED( Audit_Reopened__c ) &amp;&amp; Audit_Reopened__c = TRUE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TM Pipeline Onboarding to set tasks</fullName>
        <actions>
            <name>TM_Onboarding_Annual_Review_w_TMO</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>TM_Onboarding_Quality_Call_1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>TM_Onboarding_Quality_Call_2</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>TM_Onboarding_Quality_Call_3</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>TM_Onboarding_Send_What_to_Expect_Email_to_Client</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product_Family__c</field>
            <operation>equals</operation>
            <value>Treasury Management</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Service_Type__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Closed Won pipeline (which type of product) set tasks for TMA and TMO for onboarding process</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TM Pipeline Onboarding to set tasks for Positive Pay Only</fullName>
        <actions>
            <name>TM_Onboarding_Quality_Call_1_PP</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>TM_Onboarding_Quality_Call_2_PP</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>TM_Onboarding_Quality_Call_3_PP</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product_Family__c</field>
            <operation>equals</operation>
            <value>Treasury Management</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Service_Type__c</field>
            <operation>equals</operation>
            <value>Yes - Positive Pay Only</value>
        </criteriaItems>
        <description>Closed Won pipeline (which type of product) set tasks for TMA and TMO for onboarding process</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TM Pipeline Onboarding to set tasks for Quick Deposit Only</fullName>
        <actions>
            <name>TM_Onboarding_Annual_Review_w_TMO</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>TM_Onboarding_Quality_Call_1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>TM_Onboarding_Quality_Call_2</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>TM_Onboarding_Quality_Call_3</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Product_Family__c</field>
            <operation>equals</operation>
            <value>Treasury Management</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Service_Type__c</field>
            <operation>equals</operation>
            <value>Yes-Quick Deposit Only</value>
        </criteriaItems>
        <description>Closed Won pipeline (which type of product) set tasks for TMA and TMO for onboarding process</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Renewal_Closes_in_30_Days</fullName>
        <assignedToType>owner</assignedToType>
        <description>The following credit is scheduled to mature within the next 30 days.  You
are receiving this task because your pipeline does not reflect that it is at
75% or greater in the pipeline process.</description>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Renewal Closes in 30 Days</subject>
    </tasks>
    <tasks>
        <fullName>Renewal_Closes_in_60_Days</fullName>
        <assignedToType>owner</assignedToType>
        <description>The following credit is scheduled to mature within the next 60 days.  You
are receiving this task because your pipeline does not reflect that it is at
50% or greater in the pipeline process.</description>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Renewal Closes in 60 Days</subject>
    </tasks>
    <tasks>
        <fullName>TM_Onboarding_Annual_Review_w_TMO</fullName>
        <assignedTo>Treasury Management Officer</assignedTo>
        <assignedToType>opportunityTeam</assignedToType>
        <description>Using the Year Review Guidelines, review the pipeline and ensure all tasks have been completed and the client is satisfied.</description>
        <dueDateOffset>365</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>TM Onboarding Annual Review w TMO</subject>
    </tasks>
    <tasks>
        <fullName>TM_Onboarding_Quality_Call_1</fullName>
        <assignedToType>owner</assignedToType>
        <description>#1  Reminders:
 - Did you send the What to Expect email?
 - Has the client received all necessary information/equipment for the service?
 - Can the client successfully log into the service?
 - Is the client interested in training?</description>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>TM Onboarding Quality Call #1</subject>
    </tasks>
    <tasks>
        <fullName>TM_Onboarding_Quality_Call_1_PP</fullName>
        <assignedToType>owner</assignedToType>
        <description>Send What to Expect email - 

This will let the client know that the implementation process has begun and a reminder of next steps.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>TM Onboarding Send &quot;What to Expect&quot; Email to Client PP</subject>
    </tasks>
    <tasks>
        <fullName>TM_Onboarding_Quality_Call_2</fullName>
        <assignedToType>owner</assignedToType>
        <description>#2 Reminders:
 
Did you check pricing accuracy?
Any issues indicated in the HEAT Record?
Have you checked their login activity report?
Confirm that training has been completed.
Are they comfortable utilizing the service?
Is the client satisfied?</description>
        <dueDateOffset>60</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>TM Onboarding Quality Call #2</subject>
    </tasks>
    <tasks>
        <fullName>TM_Onboarding_Quality_Call_2_PP</fullName>
        <assignedToType>owner</assignedToType>
        <description>- Check SIMS for status of setup
- Ensure the setup process is well underway and running smoothly. 
  Review SIMS for notes
- If appropriate, contact client to:
    Confirm receipt of TMPI email
    Answer questions if needed
    Encourage training</description>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>TM Onboarding Quality Call #1 PP</subject>
    </tasks>
    <tasks>
        <fullName>TM_Onboarding_Quality_Call_3</fullName>
        <assignedToType>owner</assignedToType>
        <description>#3 Reminders

 - Did you see any issues on their Heat Record?
 - What is their login activity?
 - Does the client require any further training?
 - Is the client satisfied with the service?
 - Is there an opportunity to expand their service?</description>
        <dueDateOffset>180</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>TM Onboarding Quality Call #3</subject>
    </tasks>
    <tasks>
        <fullName>TM_Onboarding_Quality_Call_3_PP</fullName>
        <assignedToType>owner</assignedToType>
        <description>- Email or call the client to make sure they are comfortable with the service

- Validate the account is billing correctly</description>
        <dueDateOffset>45</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>TM Onboarding Quality Call #2 PP</subject>
    </tasks>
    <tasks>
        <fullName>TM_Onboarding_Send_What_to_Expect_Email_to_Client</fullName>
        <assignedToType>owner</assignedToType>
        <description>Using the &quot;Send an Email&quot; button on the pipeline record, send the product specific  &quot;What to Expect&quot; email template to the contact. Ensure the email address for the contact has been added to OneView.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>TM Onboarding Send &quot;What to Expect&quot; Email to Client</subject>
    </tasks>
</Workflow>
