<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Client_Profile_Approval</fullName>
        <description>Client Profile Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Client_Profile_Approved</template>
    </alerts>
    <alerts>
        <fullName>Client_Profile_Rejected</fullName>
        <description>Client Profile Rejected</description>
        <protected>false</protected>
        <recipients>
            <recipient>Energy_RM</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Client_Profile_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Profile_Approval_Notification</fullName>
        <description>Profile Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Client_Profile_Approved</template>
    </alerts>
    <alerts>
        <fullName>Profile_Rejection_Notification</fullName>
        <description>Profile Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Client_Profile_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Regions360_Updated</fullName>
        <field>Regions360_Last_Updated__c</field>
        <formula>Bank_Relationship_Begin_Date__c</formula>
        <name>Regions360 Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Risk_Rating_Low</fullName>
        <field>Risk_Rating_Override__c</field>
        <literalValue>Low</literalValue>
        <name>Risk Rating Low</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Save_Date_Approved</fullName>
        <field>Date_Approved__c</field>
        <formula>TODAY()</formula>
        <name>Save Date Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Save_Date_Submitted</fullName>
        <description>updates RCI &quot;Date Submitted&quot;</description>
        <field>Date_Submitted__c</field>
        <formula>TODAY()</formula>
        <name>Save Date Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Temp Regions 360 Date Copy</fullName>
        <actions>
            <name>Regions360_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Restricted_Client_Information__c.Bank_Relationship_Begin_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
