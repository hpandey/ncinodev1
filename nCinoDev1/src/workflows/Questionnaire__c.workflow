<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Level_Parent_Question_Level_1</fullName>
        <field>Level__c</field>
        <formula>Parent_Question__r.Level__c+1</formula>
        <name>Set Level = Parent Question Level + 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Parent Question Updated</fullName>
        <actions>
            <name>Set_Level_Parent_Question_Level_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Parent_Question__c !=null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
