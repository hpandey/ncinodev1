<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>email_admin</fullName>
        <description>email admin</description>
        <protected>false</protected>
        <recipients>
            <recipient>jennifer.lariviere@regions.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>phillip.landers@regions.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Admin_Only/Integration_Error_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Occurrence_Date_Time</fullName>
        <field>Last_Occurrence__c</field>
        <formula>NOW()</formula>
        <name>Update Occurrence Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Email Admins On Exception</fullName>
        <actions>
            <name>email_admin</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>IntegrationError__c.Error_Type__c</field>
            <operation>equals</operation>
            <value>Exception</value>
        </criteriaItems>
        <criteriaItems>
            <field>IntegrationError__c.Error_Type__c</field>
            <operation>equals</operation>
            <value>Timeout</value>
        </criteriaItems>
        <description>AccountBalance Exception workflow to email administrators. This workflow can be activated or deactivated as needed.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Error Last Occurrence Date%2FTime</fullName>
        <actions>
            <name>Update_Occurrence_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Integration</value>
        </criteriaItems>
        <description>Set&apos;s the Error Occurrence Data/Time when the Records in Touched by a User with a Profile Name than Contains &quot;Integration&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
