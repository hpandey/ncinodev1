<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Coaching_Area_Complete</fullName>
        <description>set coaching area complete if results filled</description>
        <field>Completed__c</field>
        <literalValue>1</literalValue>
        <name>Coaching Area Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>InComplete_Coaching_Area</fullName>
        <description>uncheck the complete flag if results are deleted</description>
        <field>Completed__c</field>
        <literalValue>0</literalValue>
        <name>InComplete Coaching Area</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Complete Coaching Area</fullName>
        <actions>
            <name>Coaching_Area_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Coaching_Area__c.Results__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>If Results filled, set Completed flag</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>InComplete Coaching Area</fullName>
        <actions>
            <name>InComplete_Coaching_Area</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Coaching_Area__c.Results__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If Results blank, uncheck Completed flag</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
