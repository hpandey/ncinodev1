<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Draft_Portfolio_Management</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Draft_Portfolio_Management</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Draft Portfolio Management</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Portfolio_Management</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Final_Portfolio_Management</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Final Portfolio Management</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Draft Portfolio Management</fullName>
        <actions>
            <name>Draft_Portfolio_Management</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Portfolio_Management__c.Status__c</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <description>Changes Record Type of record to Draft</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Final Portfolio Management</fullName>
        <actions>
            <name>Final_Portfolio_Management</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Portfolio_Management__c.Status__c</field>
            <operation>equals</operation>
            <value>Final</value>
        </criteriaItems>
        <description>Changes Record Type of record to Final</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
