<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Author_of_Article_Rejection</fullName>
        <description>Notify Author of Article Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Knowledge_Approval_Process_EMails/How_To_Article_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Notify_Knowledge_Author_of_Pending_New_Article</fullName>
        <description>Notify Knowledge Author of Pending New Article</description>
        <protected>false</protected>
        <recipients>
            <recipient>Knowledge_Approvers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Knowledge_Approval_Process_EMails/How_To_Article_Approval_Request</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Pending_Review_False</fullName>
        <field>Pending_Review__c</field>
        <literalValue>0</literalValue>
        <name>Set Pending Review False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish_How_To</fullName>
        <action>Publish</action>
        <label>Publish How To</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
</Workflow>
