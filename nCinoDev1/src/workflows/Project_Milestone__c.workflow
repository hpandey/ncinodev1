<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Analysis_End_Date</fullName>
        <description>Updates End Date plus 14 days</description>
        <field>End_Date__c</field>
        <formula>Today()+ 14</formula>
        <name>Analysis End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Analysis_Start_Date</fullName>
        <description>Updates Start Date</description>
        <field>Start_Date__c</field>
        <formula>Today()</formula>
        <name>Analysis Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_to_In_Approval</fullName>
        <field>Status</field>
        <literalValue>In Approval</literalValue>
        <name>Case Status to In Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_to_In_Development</fullName>
        <field>Status</field>
        <literalValue>In Development</literalValue>
        <name>Case Status to In Development</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_to_In_Testing</fullName>
        <field>Status</field>
        <literalValue>In Testing</literalValue>
        <name>Case Status to In Testing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Development_End_Date</fullName>
        <description>Sets Development end date to today + 70 days</description>
        <field>End_Date__c</field>
        <formula>Today()+ 70</formula>
        <name>Development - End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Development_Start_Date</fullName>
        <description>14 Days after Project Begins</description>
        <field>Start_Date__c</field>
        <formula>Today()+ 14</formula>
        <name>Development - Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>High_Level_Status_Yellow</fullName>
        <field>High_Level_Statu__c</field>
        <literalValue>Yellow</literalValue>
        <name>High Level Status Yellow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PM_Deployment_End_Date</fullName>
        <description>updates project milestone &quot;Deployment&quot; end date using today&apos;s date + 100</description>
        <field>End_Date__c</field>
        <formula>Today () + 100</formula>
        <name>PM Deployment End  Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PM_Deployment_Start_Date</fullName>
        <description>Updates project milestone start date from today + 98 days</description>
        <field>Start_Date__c</field>
        <formula>Today() + 98</formula>
        <name>PM Deployment Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PM_Testing_End_Date</fullName>
        <field>End_Date__c</field>
        <formula>Today() + 84</formula>
        <name>PM - Testing End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PM_Testing_Start_Date</fullName>
        <field>Start_Date__c</field>
        <formula>Today()+ 70</formula>
        <name>PM Testing Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PM_Training_End_Date</fullName>
        <field>End_Date__c</field>
        <formula>Today() + 98</formula>
        <name>PM Training End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PM_Training_Start_Date</fullName>
        <field>Start_Date__c</field>
        <formula>Today() + 84</formula>
        <name>PM - Training Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Proposed_Release_Date</fullName>
        <description>Updates case &quot;Requested Due Date&quot; to reflect today + 100 days</description>
        <field>Requested_Due_Delivery_Date__c</field>
        <formula>Today () + 100</formula>
        <name>Proposed Release Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Release_Date</fullName>
        <description>Updates the Case Release Date to match the End Date from Deployment</description>
        <field>Release_Date__c</field>
        <formula>End_Date__c</formula>
        <name>Release Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Technology_End_Date</fullName>
        <description>End Date for Technology&apos;s work</description>
        <field>End_Date__c</field>
        <formula>Today()+ 70</formula>
        <name>Technology - End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Technology_Start_Date</fullName>
        <description>Date technology starts working on project</description>
        <field>Start_Date__c</field>
        <formula>Today()+ 14</formula>
        <name>Technology - Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PM - Analysis Begin and End Date</fullName>
        <actions>
            <name>Analysis_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Analysis_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_Milestone__c.Name</field>
            <operation>equals</operation>
            <value>Analysis</value>
        </criteriaItems>
        <description>Sets project milestone (Analysis) start and end dates</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PM - Analysis Closed</fullName>
        <actions>
            <name>Case_Status_to_In_Development</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Moves Case &quot;Status&quot; to &quot;In Development&quot; when project milestone &quot;Analysis&quot; is closed.</description>
        <formula>AND(
 Name = &quot;Analysis&quot;,
ISCHANGED( Status__c ) &amp;&amp;  ISPICKVAL(Status__c , &quot;Closed&quot;) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PM - Testing Closed</fullName>
        <actions>
            <name>Case_Status_to_In_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Moves to In Approval Status</description>
        <formula>AND(
 Name = &quot;Testing&quot;,
ISCHANGED( Status__c ) &amp;&amp;  ISPICKVAL(Status__c , &quot;Closed&quot;) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PM Deployment End and Start Date</fullName>
        <actions>
            <name>PM_Deployment_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Deployment_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Proposed_Release_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_Milestone__c.Name</field>
            <operation>equals</operation>
            <value>Deployment</value>
        </criteriaItems>
        <description>Updated Project Milestone deployment start and end dates.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PM Development Begin and End Date</fullName>
        <actions>
            <name>Development_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Development_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_Milestone__c.Name</field>
            <operation>equals</operation>
            <value>Development</value>
        </criteriaItems>
        <description>Sets project milestone Development&apos;s start and end dates</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PM Development Closed</fullName>
        <actions>
            <name>Case_Status_to_In_Testing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Moves Case Status to &quot;In Testing&quot; when project milestone Development is closed.</description>
        <formula>AND(
 Name = &quot;Development&quot;,
ISCHANGED( Status__c ) &amp;&amp;  ISPICKVAL(Status__c , &quot;Closed&quot;) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PM Past Due End Date</fullName>
        <actions>
            <name>High_Level_Status_Yellow</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Project_Milestone__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Blocked,Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Project_Milestone__c.End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>PM Release Date</fullName>
        <actions>
            <name>Release_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  
Name = &quot;Deployment&quot;,
 ISCHANGED( End_Date__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PM Technology Begin and End Date</fullName>
        <actions>
            <name>Technology_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Technology_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_Milestone__c.Name</field>
            <operation>equals</operation>
            <value>Technology</value>
        </criteriaItems>
        <description>Sets project milestone Technology start and end dates - work in the same time frame as development</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PM Testing Begin and End Date</fullName>
        <actions>
            <name>PM_Testing_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Testing_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_Milestone__c.Name</field>
            <operation>equals</operation>
            <value>Testing</value>
        </criteriaItems>
        <description>Updates project milestone testing start and end dates.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PM Training Start %26 End Date</fullName>
        <actions>
            <name>PM_Training_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PM_Training_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Project_Milestone__c.Name</field>
            <operation>equals</operation>
            <value>Training</value>
        </criteriaItems>
        <description>Sets project management training start and end dates</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
