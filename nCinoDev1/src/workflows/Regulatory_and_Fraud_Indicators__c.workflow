<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Request_forRegional_President_Approval_Rejection</fullName>
        <description>Request forRegional President Approval/Rejection</description>
        <protected>false</protected>
        <recipients>
            <field>Regional_President__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Prescreen_Approval</template>
    </alerts>
    <alerts>
        <fullName>Request_for_Business_Banking_approval_Rejection</fullName>
        <description>Request for Business Banking approval/Rejection</description>
        <protected>false</protected>
        <recipients>
            <field>Head_Of_Business_Banking__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Prescreen_Approval</template>
    </alerts>
    <alerts>
        <fullName>Request_for_Credit_Executive_Approval_Rejection</fullName>
        <description>Request for Credit Executive Approval/Rejection</description>
        <protected>false</protected>
        <recipients>
            <field>Business_Banking_Credit_Executive__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Prescreen_Approval</template>
    </alerts>
    <fieldUpdates>
        <fullName>Prescreen_Result</fullName>
        <description>The Prescreen Result should be updated to passed if Reason is Another Business Location is in the Market</description>
        <field>Result__c</field>
        <literalValue>Passed</literalValue>
        <name>Prescreen Result</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Result_First_Approval</fullName>
        <description>If 1st approvers approves, the record goes to 2nd approver and status remains failed</description>
        <field>Result__c</field>
        <literalValue>First Approval</literalValue>
        <name>Set Result First Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Result_changed_to_Passed</fullName>
        <description>Result to be changed to Passed once approval from all 3 approvers has been received</description>
        <field>Result__c</field>
        <literalValue>Passed</literalValue>
        <name>Result changed to Passed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitted_for_Approval</fullName>
        <description>Set the flag, once the record is submitted for approval</description>
        <field>Submitted_for_approval__c</field>
        <literalValue>1</literalValue>
        <name>Set Submitted for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Rejected</fullName>
        <field>Result__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_to_Second_Approval</fullName>
        <field>Result__c</field>
        <literalValue>Second Approval</literalValue>
        <name>Updated to Second Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Lock Record if Reason is Another Business Location</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Regulatory_and_Fraud_Indicators__c.Override_Reason__c</field>
            <operation>equals</operation>
            <value>Another Business Location is in the Market</value>
        </criteriaItems>
        <criteriaItems>
            <field>Regulatory_and_Fraud_Indicators__c.Override_Result__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Lock Record if Reason is Another Business Location is in the Market</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Prescreen Result passed when Override is checked</fullName>
        <actions>
            <name>Prescreen_Result</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Regulatory_and_Fraud_Indicators__c.Override_Reason__c</field>
            <operation>equals</operation>
            <value>Another Business Location is in the Market</value>
        </criteriaItems>
        <criteriaItems>
            <field>Regulatory_and_Fraud_Indicators__c.Override_Reason__c</field>
            <operation>equals</operation>
            <value>Vermont Approval</value>
        </criteriaItems>
        <description>update the Prescreen  Result to passed when the override checkbox is checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
