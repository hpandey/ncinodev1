<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Approvers_of_New_Article_Review_Request</fullName>
        <description>Notify Approvers of New Article Review Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>Knowledge_Approvers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Knowledge_Approval_Process_EMails/Escalation_Contact_Article_Approval_Request</template>
    </alerts>
    <alerts>
        <fullName>Notify_Author_of_Article_Rejection</fullName>
        <description>Notify Author of Article Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Knowledge_Approval_Process_EMails/Escalation_Contacts_Article_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Notify_Knowledge_Approver_of_Pending_New_Article</fullName>
        <description>Notify Knowledge Approver of Pending New Article</description>
        <protected>false</protected>
        <recipients>
            <recipient>Knowledge_Approvers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Knowledge_Approval_Process_EMails/Escalation_Contact_Article_Approval_Request</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Pending_Review_False</fullName>
        <description>Set Pending Review status to False</description>
        <field>Pending_Review__c</field>
        <literalValue>0</literalValue>
        <name>Set Pending Review False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish_Article</fullName>
        <action>Publish</action>
        <description>After approval article will automatically be placed in a Published state.</description>
        <label>Publish Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>Publish_Escalation_Contact</fullName>
        <action>Publish</action>
        <label>Publish Escalation Contact</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>Publish_Escalation_Contacts_Article</fullName>
        <action>PublishAsNew</action>
        <label>Publish Escalation Contacts Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
</Workflow>
