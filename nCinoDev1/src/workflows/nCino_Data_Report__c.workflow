<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_the_Date_Validated</fullName>
        <description>Updates the Date Validated field on the Data Report object after being validated to insert into nCino.</description>
        <field>Date_Validated__c</field>
        <formula>TODAY()</formula>
        <name>Update the Date Validated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Valided Report</fullName>
        <actions>
            <name>Update_the_Date_Validated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>nCino_Data_Report__c.Ok_to_Insert__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow updates a report record after a user validates the associated extract record.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
