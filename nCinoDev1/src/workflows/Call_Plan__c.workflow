<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Add_Call_Participants_to_Completed_Call_Plan_when_Joint_Call_is_Yes</fullName>
        <ccEmails>jennifer.lariviere@regions.com</ccEmails>
        <ccEmails>mike.long@regions.com</ccEmails>
        <description>Add Call Participants to Completed Call Plan when Joint Call is Yes</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Admin_Only/Add_Participants_to_Completed_Call_Plan</template>
    </alerts>
    <fieldUpdates>
        <fullName>Check_Box_Not_Recorded</fullName>
        <field>Emerging_Risk_Follow_up_Recorded__c</field>
        <literalValue>0</literalValue>
        <name>Check Box - Not Recorded</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Emerging_Risk_Follow_up_Noted</fullName>
        <field>Emerging_Risk_Follow_up_Recorded__c</field>
        <literalValue>1</literalValue>
        <name>Emerging Risk Follow-up Recorded</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>No_Documented_Emerging_Risk</fullName>
        <field>No_Documented_Emerging_Risk__c</field>
        <formula>1</formula>
        <name>No Documented Emerging Risk</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Completed Joint Call Participants</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Call_Plan__c.Joint_Call2__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Call_Plan__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Call_Plan__c.Participants__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>You must add Participants to a Completed Call Plan when Joint Call equals Yes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Add_Call_Participants_to_Completed_Call_Plan_when_Joint_Call_is_Yes</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Add_Call_Plan_Participants</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Emerging Risk Follow-up</fullName>
        <actions>
            <name>No_Documented_Emerging_Risk</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Call_Plan__c.Risk__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Call_Plan__c.Changes_Business_or_Financial_Profile__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>If No Follow up identified on Call Plan count.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Emerging Risk Follow-up Not Recorded</fullName>
        <actions>
            <name>Check_Box_Not_Recorded</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Call_Plan__c.Risk__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Call_Plan__c.Changes_Business_or_Financial_Profile__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>If No Follow up identified on Call Plan count.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Emerging Risk Follow-up Recorded</fullName>
        <actions>
            <name>Emerging_Risk_Follow_up_Noted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Call_Plan__c.Risk__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Call_Plan__c.Changes_Business_or_Financial_Profile__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>If No Follow up identified on Call Plan count.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Add_Call_Plan_Participants</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please verify that you have added Participants to the Call Plan with Joint Call marked &quot;Yes&quot;.</description>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Add Call Plan Participants</subject>
    </tasks>
</Workflow>
