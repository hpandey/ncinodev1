<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Draft_Scorecard</fullName>
        <description>Changes record type to draft</description>
        <field>RecordTypeId</field>
        <lookupValue>Draft_Scorecard</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Draft Scorecard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Scorecard</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Final_Scorecard</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Final Scorecard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Scorecard to Draft - Read%2FWrite</fullName>
        <actions>
            <name>Draft_Scorecard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Scorecard__c.Scorecard_Status__c</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <description>When Status is draft changes record type and page layout to Scorecard Layout</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Scorecard to Read Only record</fullName>
        <actions>
            <name>Final_Scorecard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Scorecard__c.Scorecard_Status__c</field>
            <operation>equals</operation>
            <value>Final</value>
        </criteriaItems>
        <description>When Status is Final changes record type and page layout to Read Only</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
