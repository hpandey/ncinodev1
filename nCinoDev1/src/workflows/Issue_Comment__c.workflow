<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Issue_Owner_that_a_Comment_has_been_made</fullName>
        <description>Notify Issue Owner that a Comment has been made</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>nCino_Project_Mgmt/PM02_Issue_New_Project_Issue_Comment</template>
    </alerts>
    <fieldUpdates>
        <fullName>Issue_Comment_Issue_Resolved_Closed</fullName>
        <field>Issue_Status__c</field>
        <literalValue>Closed - Resolved</literalValue>
        <name>Issue Comment - Issue Resolved Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Issue__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Issue_Ready_for_Retest</fullName>
        <field>Issue_Status__c</field>
        <literalValue>Ready for Retest</literalValue>
        <name>Issue - Ready for Retest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Issue__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Issue Comment%3A Issue Resolved</fullName>
        <actions>
            <name>Issue_Comment_Issue_Resolved_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Issue_Comment__c.Issue_Resolved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow rule sets the Issue Status to Resolved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Issue Comment%3A Ready for Retest</fullName>
        <actions>
            <name>Issue_Ready_for_Retest</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Issue_Comment__c.Ready_for_Retest__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow rule sets the Issue Status to &apos;Ready for Retest&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
