<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Coaching_Status_Review_Send_to_Coachee</fullName>
        <description>Coaching Status Review Send to Coachee</description>
        <protected>false</protected>
        <recipients>
            <field>Relationship_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Coaching/Coaching_Status_Review_to_Coachee</template>
    </alerts>
    <rules>
        <fullName>Coaching Review</fullName>
        <actions>
            <name>Coaching_Reminder</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Coaching_Review__c.Reminder_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>create task when reminder set</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Coaching Review Notification to Coachee</fullName>
        <actions>
            <name>Coaching_Status_Review_Send_to_Coachee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Coaching_Review__c.Status__c</field>
            <operation>equals</operation>
            <value>Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Coaching_Review__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>PWM Coaching Record Type</value>
        </criteriaItems>
        <description>This rule is used with Coaching Review to send out email notification to alert person being coached that the status of the review is &quot;Review&quot; and is viewable now.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Coaching_Reminder</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Coaching_Review__c.Reminder_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Coaching Reminder</subject>
    </tasks>
</Workflow>
