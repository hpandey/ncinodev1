<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>COI_Contact</fullName>
        <field>COI__c</field>
        <formula>IF( Account.RecordType.DeveloperName = &apos;COI&apos;, &apos;COI&apos;, &apos;&apos;)</formula>
        <name>COI Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COI_Owner</fullName>
        <description>Make COI contacts owned by Admin</description>
        <field>OwnerId</field>
        <lookupValue>oneviewadmin@regions.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>COI Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>COI_Record_Type</fullName>
        <description>Set the record type for COI contacts</description>
        <field>RecordTypeId</field>
        <lookupValue>COI_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>COI Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Client_Contact_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Client_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Client Contact Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LLC_BI__Set_Contact_Business_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>LLC_BI__Business_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Contact Business Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LLC_BI__Set_Contact_Individual_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>LLC_BI__Individual_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Contact Individual Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Prospect_Contact_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Prospect_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Prospect Contact Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>COI Contact</fullName>
        <actions>
            <name>COI_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>COI_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>COI_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>COI</value>
        </criteriaItems>
        <description>Set COI indicator on Contact</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Client Contact</fullName>
        <actions>
            <name>Client_Contact_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client</value>
        </criteriaItems>
        <description>Set Client Contact Record Type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LLC_BI__Contact Set Business Record Type</fullName>
        <actions>
            <name>LLC_BI__Set_Contact_Business_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.LLC_BI__Primary_Contact__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>sets Record Type to Business if Contact is not marked as primary</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LLC_BI__Contact Set Individual Record Type</fullName>
        <actions>
            <name>LLC_BI__Set_Contact_Individual_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.LLC_BI__Primary_Contact__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>sets Record Type to Individual if Contact is marked as primary</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Prospect Contact</fullName>
        <actions>
            <name>Prospect_Contact_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Prospect</value>
        </criteriaItems>
        <description>Set Prospect Contact Record Type</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>sample</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Contact.IsEmailBounced</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
