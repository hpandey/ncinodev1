<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Comml_Lost_Relationship_is_valid_and_approved_by_your_Manager</fullName>
        <description>Comml Lost Relationship is valid and approved by your Manager</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Comml_Lost_Relationship_Approved</template>
    </alerts>
    <alerts>
        <fullName>Once_Manager_declines_rejects_lost_relationship_this_email_is_used_to_notify_the</fullName>
        <description>Once Manager declines/rejects lost relationship, this email is used to notify the Relationship Owner of the declined/rejected status.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Comml_Lost_Relationship_Declined</template>
    </alerts>
    <fieldUpdates>
        <fullName>BB_Relationship_Status_Inactive</fullName>
        <field>Status__c</field>
        <literalValue>Inactive</literalValue>
        <name>BB Relationship Status Inactive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comml_Lost_Relationship_Uncheck_Reject</fullName>
        <description>Unchecks the Comml Lost Relationship field if during the approval process the manager rejects the approval.</description>
        <field>Comml_Lost_Relationship__c</field>
        <literalValue>0</literalValue>
        <name>Comml Lost Relationship Uncheck - Reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lost_Relationship_Date_Remove</fullName>
        <description>If rejected, removes the date from the field</description>
        <field>Relationship_Lost_Date__c</field>
        <name>Lost Relationship Date - Remove</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lost_Top_REA_Uncheck</fullName>
        <description>Unchecks the &quot;Lost Top REA&quot; when a lost relationship approval request is rejected.</description>
        <field>Lost_Top_REA__c</field>
        <literalValue>0</literalValue>
        <name>Lost Top REA Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_Lost_Trailing_Revenue</fullName>
        <description>Clear Lost Trailing Revenue field</description>
        <field>Lost_Total_Trailing_Revenue__c</field>
        <name>Remove Lost Trailing Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BB Inactive Relationship</fullName>
        <actions>
            <name>BB_Relationship_Status_Inactive</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>AND( 
ISCHANGED(Purged_Indicator__c ), 
ISPICKVAL( Status__c , &quot;Active&quot;) 
)</description>
        <formula>AND(   ISCHANGED(Purged_Indicator__c ),  Purged_Indicator__c = &quot;Y&quot;,  NOT(ISPICKVAL( Status__c , &quot;Inactive&quot;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
