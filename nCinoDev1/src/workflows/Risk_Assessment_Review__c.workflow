<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Conduct_Review_Rejected</fullName>
        <description>Conduct Review Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Conduct_Review_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Notify_of_Approval</fullName>
        <description>Notify of Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Conduct_Review_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Date_Last_Review_Update</fullName>
        <field>Date_Last_Review_Approved__c</field>
        <formula>TODAY()</formula>
        <name>Date Last Review Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Restricted_Client_Information__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Review_Approved_update</fullName>
        <field>Date_Last_Review_Approved__c</field>
        <formula>TODAY()</formula>
        <name>Last Review Approved update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Restricted_Client_Information__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Save_Date_Approved</fullName>
        <field>Date_Review_Approved__c</field>
        <formula>Today()</formula>
        <name>Save Date Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Save_Date_Submitted</fullName>
        <field>Date_Review_Submitted__c</field>
        <formula>TODAY()</formula>
        <name>Save Date Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
