<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_to_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Final_Performance_Assessment</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change to Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_to_Draft</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Draft_Performance_Assessment</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Allow Underwriter to see Assessment in Read Only</fullName>
        <actions>
            <name>Change_to_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Performance_Assessment__c.Assessment_Status__c</field>
            <operation>equals</operation>
            <value>Final</value>
        </criteriaItems>
        <description>After manager reviews and finalizes assessment change record type and owner to underwriter - read only page layout</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Status back to Draft Assessment</fullName>
        <actions>
            <name>Record_Type_to_Draft</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Performance_Assessment__c.Assessment_Status__c</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <description>If manager changes status to Draft - changes record type back to Draft</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
