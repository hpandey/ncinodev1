<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Issue_New_Comment_Notification</fullName>
        <description>Issue - New Comment Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>nCino_Project_Mgmt/PM02_Issue_New_Project_Issue_Comment</template>
    </alerts>
    <alerts>
        <fullName>Issue_New_Issue_Created_and_Assigned</fullName>
        <description>Issue - New Issue Created and Assigned</description>
        <protected>false</protected>
        <recipients>
            <recipient>jenwerner@deloitte.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>nCino_Project_Mgmt/PM01_Issue_New_Project_Issue</template>
    </alerts>
    <rules>
        <fullName>Issue  - New Issue Assigned to You</fullName>
        <actions>
            <name>Issue_New_Issue_Created_and_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow notifies the owner of an internal project issue that an issue has been assigned to them.</description>
        <formula>AND(  ISCHANGED( OwnerId ), LastModifiedById &lt;&gt; OwnerId  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Issue  - New Issue Assignment</fullName>
        <actions>
            <name>Issue_New_Issue_Created_and_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow notifies the Assigned to User  that an internal project issue has been assigned to them.</description>
        <formula>AND(  ISCHANGED(  Assigned_To__c  ), LastModifiedById &lt;&gt; OwnerId  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Issue - New Comment Notification</fullName>
        <actions>
            <name>Issue_New_Comment_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an Email Alert to the Issue Owner &amp; Creator when a new comment is added</description>
        <formula>AND(  ISCHANGED(Comment_Count__c ),  Comment_Count__c &gt; 0  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
