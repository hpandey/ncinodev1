<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Appraisal_Order_Approved</fullName>
        <description>Appraisal Order - Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PSO_Email_Templates/ET04_Appraisal_Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Appraisal_Order_Rejected</fullName>
        <description>Appraisal Order - Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PSO_Email_Templates/ET05_Appraisal_Order_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Appraisal_Set_Status_to_Ordered</fullName>
        <field>Appraisal_Status__c</field>
        <literalValue>Ordered / In Progress</literalValue>
        <name>Appraisal - Set Status to Ordered</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Appraisal_Set_Status_to_Recalled</fullName>
        <field>Appraisal_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Appraisal - Set Status to Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Appraisal_Set_Status_to_Rejected</fullName>
        <field>Appraisal_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Appraisal - Set Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Appraisal_Set_Status_to_Submitted</fullName>
        <field>Appraisal_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Appraisal - Set Status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
