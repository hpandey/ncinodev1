<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_email_notification_for_Technology_Assist</fullName>
        <description>Case email notification for Technology Assist</description>
        <protected>false</protected>
        <recipients>
            <recipient>chris.wilder@regions.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dale.blocker@regions.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ephrem.tekle@regions.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jennifer.dierksheide@regions.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jennifer.human@regions.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jennifer.lariviere@regions.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jill.gordon@regions.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Emails_Internal/Case_Technology_Assist_Notification</template>
    </alerts>
    <alerts>
        <fullName>Closed_Case_Notification</fullName>
        <description>Closed Case Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Emails_Internal/Case_Closed_Requester_Notification</template>
    </alerts>
    <alerts>
        <fullName>Committee_Approval_Steve</fullName>
        <description>Committee Approval - Steve</description>
        <protected>false</protected>
        <recipients>
            <recipient>jill.gordon@regions.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>steve.rueve@regions.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Admin_Case_to_Committee</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification</fullName>
        <description>Email Notification for Project Management</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Emails_Internal/Case_PM_Assist_Notification</template>
    </alerts>
    <alerts>
        <fullName>Enhancement_Request_Case_notification_to_BG_Requester</fullName>
        <description>Enhancement Request Case notification to BG Requester</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Emails_Internal/Case_Requester_Notification</template>
    </alerts>
    <alerts>
        <fullName>Exception_Case_Approval_Goes_to_Steve_for_final_approval</fullName>
        <description>Exception Case Approval - Goes to Steve for final approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>jill.gordon@regions.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>steve.rueve@regions.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Process_Emails/Admin_Case_Needs_Exception_Approval</template>
    </alerts>
    <fieldUpdates>
        <fullName>Admin_Case_Set_Status_to_Approved</fullName>
        <description>Once admin case is approved by Manager, the case status is set to Approved.</description>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Admin Case Set Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Admin_Case_Set_Status_to_Rejected</fullName>
        <description>When an admin case approval is rejected, the status will field will update to &quot;Approval Rejected&quot;.</description>
        <field>Status</field>
        <literalValue>Approval Rejected</literalValue>
        <name>Admin Case Set Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Business_Group_Priority</fullName>
        <field>Status</field>
        <literalValue>Business Group Prioritization</literalValue>
        <name>Business Group Priority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Actual_Hours</fullName>
        <field>Actual_Hours__c</field>
        <formula>PM_Actual_Hours__c</formula>
        <name>Update Actual Hours</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Estimated_Hours</fullName>
        <field>Estimate_Hours__c</field>
        <formula>PM_Estimated_Hours__c</formula>
        <name>Update Estimated Hours</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Withdrawn_or_Declined</fullName>
        <field>Status</field>
        <literalValue>Withdrawn/Declined</literalValue>
        <name>Withdrawn or Declined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Case IT Assignment Email Alert</fullName>
        <actions>
            <name>Case_email_notification_for_Technology_Assist</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Technology_Assist__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow creates an email alert (replaced the task we used to use)  for IT to be alerted to new case with technology assist checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Enhancement Request Case</fullName>
        <actions>
            <name>Enhancement_Request_Case_notification_to_BG_Requester</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Enhancement Request</value>
        </criteriaItems>
        <description>When new Enhancement Request Case is created to provide email to BG requestor that a case has been created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Enhancement Request Case Closed</fullName>
        <actions>
            <name>Closed_Case_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Used for closed enhancement cases to notify the BG requester.</description>
        <formula>AND(
 ISPICKVAL( Origin , &quot;Enhancement Request&quot;), 
 ISCHANGED(Status) &amp;&amp; ISPICKVAL( Status, &quot;Closed&quot;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PM Case Hours</fullName>
        <actions>
            <name>Update_Actual_Hours</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Estimated_Hours</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( 
 ISCHANGED( PM_Actual_Hours__c ),
 ISCHANGED( PM_Estimated_Hours__c )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PM Needed</fullName>
        <actions>
            <name>Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Project_Management_Needed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Used to notify the PM of a new case and also used to create the Project Milestones once checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Case_Approval_Rejected</fullName>
        <assignedToType>owner</assignedToType>
        <description>Your case approval was rejected, please refer to case comments added by Manager and resubmit your case once issues are addressed.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Case Approval Rejected</subject>
    </tasks>
    <tasks>
        <fullName>Case_Approved_Please_Update_Status</fullName>
        <assignedToType>owner</assignedToType>
        <description>Your Case has been approved and can now move forward in the process.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Case Approved - Proceed</subject>
    </tasks>
    <tasks>
        <fullName>Committee_Approved_Proceed</fullName>
        <assignedToType>owner</assignedToType>
        <description>Your Case has been approved by the Committee and can now move forward in the process.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Committee Approved - Proceed</subject>
    </tasks>
    <tasks>
        <fullName>Exception_Requested_by_BG_Approved_Please_proceed</fullName>
        <assignedToType>owner</assignedToType>
        <description>Your request for an Exception has been approved and is ready for you to proceed.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Exception Requested by BG Approved - Please proceed</subject>
    </tasks>
    <tasks>
        <fullName>IT_Case_Assignment</fullName>
        <assignedTo>chris.wilder@regions.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A case in OneView has been marked as needing technology assistance. Follow steps below:

1: Open case - assign the appropriate resource in the &quot;Technology Assigned&quot; field in Case Team section and &quot;Save&quot; case.

2: Open Task - set Status to &quot;Completed&quot;.</description>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>IT Case Assignment</subject>
    </tasks>
</Workflow>
