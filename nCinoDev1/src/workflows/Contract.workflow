<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>TM_RFP_Award_Date_Notification_to_contract_owner</fullName>
        <description>TM RFP Award Date Notification to contract owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>jill.gordon@regions.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TM_RFP_Emails/TM_RFP_Award_Date_Notice</template>
    </alerts>
    <rules>
        <fullName>TM RFP Award Date Notify</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract.RecordTypeId</field>
            <operation>equals</operation>
            <value>RFP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.Bid_Decision__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract.RFP_Award_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sends an email notification to the contract owner to alert them to the fact that an RFP Award date is approaching to check on it.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Contract.RFP_Award_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
