<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Config_Task_Percent_Complete_to_100</fullName>
        <field>Percent_Complete__c</field>
        <literalValue>100 %</literalValue>
        <name>Config Task - Percent Complete to 100</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Config_Task_Set_Date_Complete_to_Today</fullName>
        <field>Date_Completed__c</field>
        <formula>Now()</formula>
        <name>Config Task - Set Date Complete to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Config_Task_Set_Status_to_Complete</fullName>
        <description>Automatically update the Config Status to Complete</description>
        <field>Config_Status__c</field>
        <literalValue>Complete</literalValue>
        <name>Config Task - Set Status to Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Config Task Complete</fullName>
        <actions>
            <name>Config_Task_Percent_Complete_to_100</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Config_Task_Set_Date_Complete_to_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Config_Task_Set_Status_to_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Automatically updates the Date Completed field to today, sets the Percent Complete field to 100% and sets the Config Status to Complete</description>
        <formula>Task_Complete__c = True</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
