<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CBSA</fullName>
        <field>CBSA__c</field>
        <formula>IF(
TEXT(Property_Location_MSA__c) = &quot;Other (Tertiary Market)&quot;,  City__c &amp; &quot;, &quot; &amp; TEXT(State__c) , 
Text(Property_Location_MSA__c))</formula>
        <name>CBSA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comml_Card_Scorecard_Value_for_Rollup</fullName>
        <description>To populate reporting field on the product level for rollup to Opportunity</description>
        <field>Comml_Card_Scorecard_Value_for_Rollup__c</field>
        <formula>Comml_Card_Scorecard_Value__c</formula>
        <name>Comml Card Scorecard Value for Rollup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deposit_Spread_Total_calculation</fullName>
        <field>Deposit_Spread_Revenue_Total__c</field>
        <formula>Deposit_Spread_Revenue_int__c +  Deposit_Spread_Revenue_nonint__c</formula>
        <name>Deposit Spread Total calculation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Facility_Type</fullName>
        <field>Facility_Type__c</field>
        <formula>TEXT(Facility_Type__c)</formula>
        <name>Facility Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FinServ_or_Bank_Update_to_Pipeline</fullName>
        <description>Updates FinServ or Bank field from the product to the pipeline for reporting purposes.</description>
        <field>FinServ_or_Bank__c</field>
        <formula>TEXT (FinServ_or_Bank__c)</formula>
        <name>FinServ or Bank Update to Pipeline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Formal_Commit_Field_Update_from_Product</fullName>
        <description>Updates the pipeline field &quot;Formal Commitment Offered?&quot; so it can be reported on.</description>
        <field>Formal_Commitment_offered__c</field>
        <formula>TEXT( Formal_Commitment_Offered__c )</formula>
        <name>Formal Commit Field Update from Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Funding_Source</fullName>
        <field>Funding_Source__c</field>
        <formula>Funding_Source__c</formula>
        <name>Funding Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Index</fullName>
        <field>Index__c</field>
        <formula>TEXT(Pricing__c)</formula>
        <name>Index</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Bank</fullName>
        <field>Lead_Bank__c</field>
        <formula>Text(Lead_Bank__c)</formula>
        <name>Lead Bank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Leveraged_Loan_Roll_up</fullName>
        <field>Leveraged_Loan_Pipeline__c</field>
        <formula>TEXT(Leveraged_Loan__c)</formula>
        <name>Leveraged Loan Roll-up</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Location_State</fullName>
        <field>Location_State__c</field>
        <formula>TEXT(State__c)</formula>
        <name>Location State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lost_Commitment</fullName>
        <description>Updates pipeline Lost Commitment field from the product</description>
        <field>Lost_Commitment__c</field>
        <formula>Lost_Commitment__c</formula>
        <name>Lost Commitment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Multi_Bank_Facility</fullName>
        <field>Multi_Bank_Facility__c</field>
        <formula>TEXT(Multi_Bank_Facility__c)</formula>
        <name>Multi-Bank Facility</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_or_Renewal</fullName>
        <field>New_or_Renewal__c</field>
        <formula>TEXT(New_or_Renewal__c)</formula>
        <name>New or Renewal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Original_Comml_Card_Scorecard_Val</fullName>
        <description>Update the Original Comml Card Scorecard Val</description>
        <field>Original_Comml_Card_Scorecard_Value__c</field>
        <formula>Comml_Card_Scorecard_Value__c</formula>
        <name>Original Comml Card Scorecard Val</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Original_Deposit_Spread_Rev</fullName>
        <description>Update the Original Dep Spread Rev</description>
        <field>Original_Deposit_Spread_Revenue__c</field>
        <formula>Deposit_Spread_Revenue_Total__c</formula>
        <name>Original Deposit Spread Rev</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Original_Fee</fullName>
        <description>Update Original Fee from Fee</description>
        <field>Original_Fees__c</field>
        <formula>Fees__c</formula>
        <name>Original Fee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Original_Liquidity_Manager_Balance</fullName>
        <description>Updates Original Liquidity manager Balance field</description>
        <field>Original_Liquidity_Manager_Balance__c</field>
        <formula>Liquidity_Manager_Balance__c</formula>
        <name>Original Liquidity Manager Balance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Original_Revenue_from_Projected_Revenue</fullName>
        <description>Populate Original Revenue from Projected Revenue</description>
        <field>Original_Projected_Rev__c</field>
        <formula>Proj_Rev__c</formula>
        <name>Original Revenue from Projected Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Family</fullName>
        <field>Product_Family__c</field>
        <formula>Product_Family__c</formula>
        <name>Product Family</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Family_Line_Item_Update</fullName>
        <description>Used to populate Product Family for Roll-up (Do NOT DELETE)</description>
        <field>Product_Family_DELETE__c</field>
        <formula>Product_Family__c</formula>
        <name>Product Family - Line Item Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Group</fullName>
        <field>Product_Group__c</field>
        <formula>Product_Group__c</formula>
        <name>Product Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Group_DEL</fullName>
        <field>Product_Group_DELETE__c</field>
        <formula>TEXT( Product2.Product_Group__c )</formula>
        <name>Product Group DEL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Name</fullName>
        <field>Product__c</field>
        <formula>Product_Name__c</formula>
        <name>Product Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Utilization</fullName>
        <description>Update Pipeline item with product utilization</description>
        <field>Product_Utilization__c</field>
        <formula>Product2.Utilization__c</formula>
        <name>Product Utilization</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Projected_Revenue_Calculation</fullName>
        <field>Proj_Rev__c</field>
        <formula>CASE( 
TEXT( PricebookEntry.Product2.Pricing_Formula__c ) , 
&apos;CI Credit Spread&apos;, Credit_Spread_Revenue__c + BLANKVALUE(Fees__c,0) + BLANKVALUE(Unused_Fee__c,0) + BLANKVALUE(Other_Fees__c,0), 
&apos;CI Deposit Spread non interest&apos;, Deposit_Spread_Revenue_nonint__c + BLANKVALUE(Fees__c,0), 
&apos;CI Deposit Spread interest&apos;, Deposit_Spread_Revenue_int__c + BLANKVALUE(Fees__c,0), 
&apos;PWM Deposit&apos;, BLANKVALUE(Projected_Deposit_Revenue__c,0) + BLANKVALUE(Fee_One_Time__c,0) + BLANKVALUE(Fee_Recurring__c,0), 
&apos;PWM Credit&apos;, BLANKVALUE(Fee_One_Time__c,0) + BLANKVALUE( Loan_Spread_Revenue__c ,0), 
&apos;TM&apos;,BLANKVALUE(PxV__c,0) + BLANKVALUE(Setup__c,0), 
&apos;Commercial Card&apos;, BLANKVALUE(PxV__c,0) + BLANKVALUE(Setup__c,0) + BLANKVALUE(Interchange__c , 0) + BLANKVALUE(Comml_Card_Scorecard_Value__c , 0), 
&apos;REB Investments&apos;, BLANKVALUE(Transaction_Size__c,0) * (Yield__c - 0.02), 
&apos;Secondary Sales&apos;, BLANKVALUE(REB_Revenue__c,0), 
&apos;Liquidity&apos;, BLANKVALUE(Liquidity_Manager_Fee__c,0), 
&apos;REB Credit&apos;, Credit_Spread_Revenue__c + BLANKVALUE(Fees__c,0) + BLANKVALUE(Unused_Fee__c,0) + BLANKVALUE(Other_Fees__c,0), 
Estimated_Revenue__c + BLANKVALUE(Fees__c,0) + BLANKVALUE(Fee_One_Time__c,0) + BLANKVALUE(Fee_Recurring__c,0) + BLANKVALUE(Fee_Acceptance__c,0) + BLANKVALUE( Fee_Investment_Management__c , 0) 
)</formula>
        <name>Projected Revenue Calculation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Property_Type</fullName>
        <field>Property_Type__c</field>
        <formula>TEXT(Property_Type__c)</formula>
        <name>Property Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Purpose_of_Financing_Roll_Up</fullName>
        <field>Purpose_of_Financing_Roll_Up__c</field>
        <formula>TEXT(Purpose_of_Financing__c)</formula>
        <name>Purpose of Financing Roll Up</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SNC_Bank_Position_Update</fullName>
        <description>Updates SNC Bank Position on the pipeline from the product for reporting purposes for RBC</description>
        <field>SNC_Bank_Position__c</field>
        <formula>TEXT(SNC_Bank_Position__c )</formula>
        <name>SNC Bank Position Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SNC_Lead_Bank_Update</fullName>
        <description>Updates the SNC Lead Bank pipeline field with the pipeline product field for RBC reporting purposes</description>
        <field>SNC_Lead_Bank__c</field>
        <formula>TEXT(Lead_Bank__c )</formula>
        <name>SNC Lead Bank Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SNC_Number_of_Banks_Update</fullName>
        <description>Updates the SNC Number of Banks on the pipeline from the product for RBC reporting purposes</description>
        <field>SNC_Number_of_Banks__c</field>
        <formula>TEXT(SNC_Number_of_banks__c)</formula>
        <name>SNC Number of Banks Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SNC_Total_Facility_Size</fullName>
        <description>Updates the SNC Total Facility Size from the product to the pipeline for RBC reporting purposes</description>
        <field>SNC_Total_Facility_Size__c</field>
        <formula>TEXT(Total_Facility_Size__c )</formula>
        <name>SNC Total Facility Size</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Shared_National_Credit_Update</fullName>
        <description>Pulls in the Shared National Credit value from the product to the pipeline for reporting purposes for RBC</description>
        <field>Shared_National_Credit__c</field>
        <formula>TEXT(Shared_National_Credit__c )</formula>
        <name>Shared National Credit Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Term</fullName>
        <field>Term__c</field>
        <formula>Text(Term_in_Months_full__c)</formula>
        <name>Term</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Term_Lending_Program</fullName>
        <field>Term_Lending_Program__c</field>
        <formula>TEXT(Term_Lending_Program__c)</formula>
        <name>Term Lending Program</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Total_Facility_Amount</fullName>
        <field>Total_Facility_Amount__c</field>
        <formula>Total_Facility_Size__c</formula>
        <name>Total Facility Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unit_Price_from_Projected_Revenue</fullName>
        <description>Fill Unit Price from Projected Revenue</description>
        <field>UnitPrice</field>
        <formula>Proj_Rev__c</formula>
        <name>Unit Price from Projected Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Workflow_Seq</fullName>
        <description>Update the workflow seq to ensure workflows proceed in correct order</description>
        <field>Workflow_Seq__c</field>
        <formula>CASE( BLANKVALUE(Workflow_Seq__c,1) , 1, 2, 2, 3, 3, 4, 4, 1, 1)</formula>
        <name>Workflow Seq</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>testing</fullName>
        <field>Description</field>
        <formula>CASE( TEXT( Product2.Product_Group__c ) , 
&apos;Loan&apos;, &apos;loan credit + fees&apos;, 
&apos;Lease&apos;, &apos;lease spread + fees&apos;,
&apos;other&apos;)</formula>
        <name>testing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Comml Card Scorecard Value for Rollup</fullName>
        <actions>
            <name>Comml_Card_Scorecard_Value_for_Rollup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rule to update reporting field from formula field</description>
        <formula>AND( Product_Name__c = &quot;Commercial Card&quot;, 
OR(
ISNEW (),
ISCHANGED( Comml_Card_Scorecard_Value__c ))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FinServ or Bank Roll Up to Pipeline</fullName>
        <actions>
            <name>Facility_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FinServ_or_Bank_Update_to_Pipeline</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Group__c</field>
            <operation>equals</operation>
            <value>Credit Facility</value>
        </criteriaItems>
        <description>Populates the FinServ or Bank field on the pipeline from the product</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Formal Commit Field Update to Pipeline</fullName>
        <actions>
            <name>Formal_Commit_Field_Update_from_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Group__c</field>
            <operation>equals</operation>
            <value>Credit Facility</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Leveraged Loan Roll-up</fullName>
        <actions>
            <name>Leveraged_Loan_Roll_up</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Family</field>
            <operation>equals</operation>
            <value>Leasing,Commercial Loan</value>
        </criteriaItems>
        <description>Used to populated Leveraged Loat at the pipeline for reporting purposes</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lost Business</fullName>
        <actions>
            <name>Lost_Commitment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Name__c</field>
            <operation>equals</operation>
            <value>Lost Existing Client Loan</value>
        </criteriaItems>
        <description>Updates the Lost Commitment field on the pipeline from the product</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Line item calculations</fullName>
        <actions>
            <name>Deposit_Spread_Total_calculation</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Projected_Revenue_Calculation</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Workflow_Seq</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  NOT( ISBLANK( CreatedById ) ) ,  BLANKVALUE( Workflow_Seq__c , 1) = 2 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Product Fields</fullName>
        <actions>
            <name>Product_Family</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Product_Family_Line_Item_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Product_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Product_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Product_Utilization</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Workflow_Seq</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>set product fields on Pipeline item record so that it can be used as filter in rollup summary</description>
        <formula>AND(  NOT( ISBLANK( Product_Family__c ) ) ,  BLANKVALUE( Workflow_Seq__c , 1) = 1 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Projected Revenue Calculation</fullName>
        <actions>
            <name>Projected_Revenue_Calculation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OpportunityLineItem.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Projected Revenue to UnitPrice</fullName>
        <actions>
            <name>Unit_Price_from_Projected_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Workflow_Seq</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Fill UnitPrice from Projected Revenue</description>
        <formula>AND(  NOT( ISBLANK( CreatedById ) ) ,  BLANKVALUE( Workflow_Seq__c , 1) = 3 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Purpose of Financing Roll-up</fullName>
        <actions>
            <name>Purpose_of_Financing_Roll_Up</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Family</field>
            <operation>contains</operation>
            <value>Loan</value>
        </criteriaItems>
        <description>Used to populated Purpose of Financing at the pipeline for reporting purposes</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>REB Loan 6</fullName>
        <actions>
            <name>Term_Lending_Program</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update REB Loan 6</description>
        <formula>ISPICKVAL( Product2.Family ,&quot;REB Loan 6&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>REB Products</fullName>
        <actions>
            <name>CBSA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Funding_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Index</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Lead_Bank</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Location_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Multi_Bank_Facility</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>New_or_Renewal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Property_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Term</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Total_Facility_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update REB Opportunities with REB Product Information</description>
        <formula>CASE(Product2.Product_Group__c, 
&quot;Capital Markets&quot;,&quot;TRUE&quot;, 
&quot;Credit Facility&quot;,&quot;TRUE&quot;, 
&quot;Depository Products&quot;,&quot;TRUE&quot;, 
&quot;Investments&quot;, &quot;TRUE&quot;, 
&quot;False&quot;)=&quot;TRUE&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SNC Roll-ups</fullName>
        <actions>
            <name>SNC_Bank_Position_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SNC_Lead_Bank_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SNC_Number_of_Banks_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SNC_Total_Facility_Size</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Shared_National_Credit_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Family</field>
            <operation>equals</operation>
            <value>Leasing,Commercial Loan,Loan,REB Loan 1,REB Loan 2,REB Loan 3,REB Loan 4,REB Loan 5,REB Loan 6</value>
        </criteriaItems>
        <description>Used to populate all the SNC - Sharing National Credit fields at the pipeline for reporting purposes</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Original Revenue</fullName>
        <actions>
            <name>Original_Comml_Card_Scorecard_Val</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Original_Deposit_Spread_Rev</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Original_Fee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Original_Liquidity_Manager_Balance</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Original_Revenue_from_Projected_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Workflow_Seq</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Original Revenue field added for TM - populated from Projected Revenue on create/update. Locked after Close</description>
        <formula>AND(  NOT( ISPICKVAL( Opportunity.StageName , &apos;Closed Won&apos;) ) ,  NOT( ISPICKVAL( Opportunity.StageName , &apos;Closed Lost&apos;) ) ,BLANKVALUE( Workflow_Seq__c , 1) = 4   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Revenue on Pipeline Admin</fullName>
        <actions>
            <name>Workflow_Seq</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When admins chg pipeline line item, update the workflow seq. This ensures proper updates to the pipeline from the product.</description>
        <formula>AND( OR(( ISPICKVAL( Opportunity.StageName , &apos;Closed Won&apos;) ) , ( ISPICKVAL( Opportunity.StageName , &apos;Closed Lost&apos;) )) ,($Profile.Name = &apos;System Administrator&apos;) || ($Profile.Name = &quot;PWM Power User&quot;) || ($Profile.Name = &quot;CT Power User&quot;) || ($Profile.Name = &quot;Incentive Administrator&quot;) || ($Profile.Name = &quot;OneView Audit Profile&quot;) || ($Profile.Name = &quot;TM Auditor&quot;),BLANKVALUE( Workflow_Seq__c , 1) = 4 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
