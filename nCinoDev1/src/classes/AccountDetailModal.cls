public class AccountDetailModal {     
    public boolean displayPopup {get; set;}     
    public string transDate {get; set;}
    public decimal accountNum {get; set;}
    public string CustomerBankNumber {get; set;}
    public decimal balance {get; set;}
    public string sBalance {get; set;}
    public string sBalanceStyle {get; set;}
    public string RejectReasonCode {get; set;}
    public string ApplicationCode {get; set;}
    public string AcceptRejectCode {get; set;}
    public Bank_Account_Details__c account { get; set; }
    public datetime currentDate {get; set;}
    public string errorMessage {get; set;}
    public string errorStyle {get; set;}
    public AccountBalanceArrays.ArrayOfKeyValueOfstringstring res {get; set;}
    public string MessageForError {get; set;}
    public string MessageForRejection {get; set;}
    public string MessageForTimeout {get; set;}
    public void closePopup() {        
        displayPopup = false;    
    }   
    public static String doFormatting(Decimal val, String osep, String nsep)
    {
        String s, tmp; Integer i = 6;
        Integer minChar = 0;
        s = val.setScale(2).toPlainString().replace(osep, nsep);
        if (val < 0){
            minChar = 1;
        }else{
            minChar = 0;
        }
        while((s.length() - minChar) > i)
        {
            tmp = s.substring(0, s.length() - i) + osep + s.substring(s.length() - i);
            s = tmp;
            i += 4;
        }
        return s;
    }  
    public void showPopup() {        
        displayPopup = true;   
        String RegionsURL = RegionsConnectivity__c.getInstance('AccountBalance_WS').RegionsURL__c;
        String UserID = RegionsConnectivity__c.getInstance('AccountBalance_WS').UserID__c;
        String Password = RegionsConnectivity__c.getInstance('AccountBalance_WS').Password__c;
        String ApplicationCode = String.valueOf(account.Source_System_Code__c);
        String OperatorID = RegionsConnectivity__c.getInstance('AccountBalance_WS').OperatorID__c;
        String CustomerBankNumber = String.valueOf(account.Bank_Number__c);
        String AccountNumber = String.valueOf(account.Source_Account_Number__c);
        MessageForError = RegionsConnectivity__c.getInstance('AccountBalance_WS').MessageForError__c;
        MessageForRejection = RegionsConnectivity__c.getInstance('AccountBalance_WS').MessageForRejection__c;
        Decimal Timeout = RegionsConnectivity__c.getInstance('AccountBalance_WS').Timeout__c;
        MessageForTimeout = RegionsConnectivity__c.getInstance('AccountBalance_WS').MessageForTimeoutError__c;
        
        AccountBalance.BasicHttpBinding_ISalesForceAccountProfile AB= new AccountBalance.BasicHttpBinding_ISalesForceAccountProfile();
        AB.timeout_x = Timeout.intValue();
        //System.debug('Your Account Info: ' + AccountNumber);
        try{
            res = AB.FetchSalesForceAccountProfile(UserID,Password,AccountNumber,ApplicationCode,OperatorID,CustomerBankNumber);
            //System.debug('Your Response: ' + res);
            //System.debug("Your Response: ' + res.KeyValueOfstringstring.size()); 
            if (res.KeyValueOfstringstring.size() < 4){
                errorMessage = MessageForError;
                errorStyle = 'display:none;';
            }else{
                for (Integer i = 0; i < res.KeyValueOfstringstring.size(); i++){
                    if (res.KeyValueOfstringstring[i].Key == 'AcceptRejectCode'){
                        AcceptRejectCode = res.KeyValueOfstringstring[i].Value;
                    }else if(res.KeyValueOfstringstring[i].Key == 'AccountBalance'){
                        sBalance = res.KeyValueOfstringstring[i].Value;
                    }else if(res.KeyValueOfstringstring[i].Key == 'RejectReasonCode'){
                        RejectReasonCode = res.KeyValueOfstringstring[i].Value;
                    }else if(res.KeyValueOfstringstring[i].Key == 'ApplicationCode'){
                        ApplicationCode = res.KeyValueOfstringstring[i].Value;
                    }else if(res.KeyValueOfstringstring[i].Key == 'AccountNumber'){
                        accountNum = decimal.valueOf(res.KeyValueOfstringstring[i].Value);
                    }
                }
                if (AcceptRejectCode == 'R'){
                        
                    errorMessage = 'The web service rejected your request with rejection reason of (' + RejectReasonCode + '), with the supplied account number of (' + AccountNumber + '). ' + MessageForRejection;
                    errorStyle = 'display:none;';
                }else{
                    //String numBalTest = 
                    sBalance = sBalance.replace(' ','');
                    if (string.isNotEmpty(sBalance) && sBalance.replace('-','').isNumeric()){
                        balance = decimal.valueOf(sBalance) / 100;
                        String fb = balance.format();
                        //System.debug('Your Balance: ' + balance);
                        sBalance = String.valueOf(doFormatting(balance, ',', '.'));
                    }else {
                        //System.debug('isNumeric Fail: ' + sBalance);
                        sBalance = '0';
                    }
                    
                    if (sBalance.startsWith('-')){
                        sBalance = sBalance.replace('-','-($');
                        sBalance = sBalance + ')';
                        sBalanceStyle = 'color: #ff0000;';
                    }else{
                        sBalance = '$' + sBalance;
                    }
                    errorMessage = '';
                }
            }
        }catch(CalloutException e){
            //Need to fire a workflow to email folks here!
            System.debug('Username/Password Error!!: ' + e);
            IntegrationError__c errorRecord = new IntegrationError__c();
            if (e.getMessage().contains('Read timed out')){
                errorMessage = MessageForTimeout;
                errorRecord.Error__c = 'Failed to connect to the Get Activities web service due to a long running web service call.';
                    errorRecord.Error_Description__c = 'The user was unable to connect to the web service within the timeout set in the system. ' + e;
                    errorRecord.Error_Type__c = 'Timeout';
            }else{
                errorMessage = 'You have encountered a login error. ' + MessageForError;
                errorRecord.Error__c = 'Failed to connect to the web service.';
                errorRecord.Error_Description__c = 'The user was unable to connect to the web service. ' + e;
                errorRecord.Error_Type__c = 'Exception';
            }
            errorRecord.Identifier_1__c = AccountNumber;
            errorRecord.Identifier_2__c = ApplicationCode;
            errorRecord.Integration__c = 'AccountBalance';
            errorRecord.Last_Occurrence__c = datetime.now();
            errorRecord.Status__c = 'New';
            errorRecord.Status_Notes__c = 'New & Still Open';
            errorRecord.UserEncounteredError__c = UserInfo.getUserID();
            try {
                insert errorRecord; // inserts the new record into the database
            } catch (DMLException f) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating new error log'));
            }
            errorStyle = 'display:none;';
        }catch(Exception e){
            System.debug('Error!!: ' + e);
            IntegrationError__c errorRecord = new IntegrationError__c();

            errorRecord.Error__c = 'Unknown Error';
            errorRecord.Error_Description__c = 'Unknown error has occurred.' + e;
            errorRecord.Error_Type__c = 'Exception';
            errorRecord.Identifier_1__c = AccountNumber;
            errorRecord.Identifier_2__c = ApplicationCode;
            errorRecord.Integration__c = 'AccountBalance';
            errorRecord.Last_Occurrence__c = datetime.now();
            errorRecord.Status__c = 'New';
            errorRecord.Status_Notes__c = 'New & Still Open';
            errorRecord.UserEncounteredError__c = UserInfo.getUserId();
            try {
                insert errorRecord; // inserts the new record into the database
            } catch (DMLException f) {
              ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating new error log'));
            }
            //new integration error
            errorMessage = 'A runtime error has occurred. ' + MessageForError;
            errorStyle = 'display:none;';
        }    
    }
    public AccountDetailModal(ApexPages.StandardController controller) {
      account = (Bank_Account_Details__c) controller.getRecord();
    }
}