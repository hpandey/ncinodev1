/**********************************************************************************************
 * Name         : RuleCriteriaWrapper
 * Author       : Deloitte Consulting
 * Description  : Class used for to provide efficient sorting of Apex SelectOption instances. 
 *********************************************************************************************/
 
global class SelectOptionSorter {
    
    /***
    * Method name    : FieldToSort(Child Class)
    * Description    : Sort field to use in SelectOption i.e. Label or Value
    * Return Type    : None
    * Parameter      : None
    */
    global enum FieldToSort {
        Label, Value
    }
    
    /***
    * Method name    : doSort
    * Description    : Sort select option list
    * Return Type    : Void
    * Parameter      : List<Selectoption> opts, FieldToSort sortField
    */
    global static void doSort(List<Selectoption> options, FieldToSort sortField) {
        
        Map<String, Selectoption> mapSelectOption = new Map<String, Selectoption>();
        // intSuffix to avoid duplicate values like same labels or values are in inbound list 
        Integer intSuffix = 1;
        for (Selectoption opt : options) {
            if (sortField == FieldToSort.Label) {
                mapSelectOption.put(    // Done this cryptic to save scriptlines, if this loop executes 10000 times
                                // it would every script statement would add 1, so 3 would lead to 30000.
                             (opt.getLabel().capitalize() + intSuffix++), // Key using Label + intSuffix Counter  
                             opt);   
            } else {
                mapSelectOption.put(    
                             (opt.getValue().capitalize() + intSuffix++), // Key using Label + intSuffix Counter  
                             opt);   
            }
        }
        
        List<String> listSortKeys = new List<String>();
        listSortKeys.addAll(mapSelectOption.keySet());
        listSortKeys.sort();
        // clear the original collection to rebuilt it
        options.clear();
        
        for (String key : listSortKeys) {
            options.add(mapSelectOption.get(key));
        }
    }
}