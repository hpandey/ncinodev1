global class batchCallGoals implements Database.Batchable<sObject>
{
    global batchCallGoals(){
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id, name from Call_Goal__c where createddate = this_year';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Call_Goal__c> scope)
    {
        //If the job starts to fail due to large size of records, Truncate the custom object via API, 
        //then run the batch job
        //modified for the purpose of checking record size...case 1808... Ephrem Tekle 04-2015
        List<Call_Goal__c> cgList = new List<Call_Goal__c>();
        for(Call_Goal__c cg : scope){
            cgList.add(cg);
        }
        if(!cgList.isEmpty() && cgList.size() > 0){
            Database.delete(cgLIst,false);
            DataBase.emptyRecycleBin(cgLIst);
        }       
    }   
    global void finish(Database.BatchableContext BC)
    {
        batchCallGoalsUpdate controller = new batchCallGoalsUpdate();
        database.executebatch(controller, 100);
    }
}