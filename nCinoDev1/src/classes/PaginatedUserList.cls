public with sharing class PaginatedUserList {
      private List<User> selectList=new List<User>();
      private final Integer SUB_LIST_SIZE=1000;
      
      //Returns total numbers of items.
      
      public PaginatedUserList() {
      }
      
      public PaginatedUserList(List<User> lst) {
          selectList = lst;
      }
     
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : getSize (Integer)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public Integer getSize(){
            return selectList.size();
      }
      
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : add (void)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public void add(User so){
          selectList.add(so);
      }
      
      
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get (User)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public User get(Integer index){
            if(index>-1 && index<getSize()){
                  return selectList.get(index);
            }else{
                  return null;
            }    
      }
      
      
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : remove (Integer)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public void remove(Integer index){
            if(index>-1 && index<getSize()){
                  selectList.remove(index);
            }    
      }


    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : clear (void)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public void clear(){
            selectList.clear();
      }


    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : getSelectList (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User> getSelectList(){
            return selectList;
      }


    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : getSubList (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      //Gets Select Options for the given start and end index both are inclusive.
      public List<User> getSubList(Integer startIndex,Integer endIndex){
            List<User> subList=new List<User>();
            if(startIndex>-1 && startIndex<selectList.size()&& endIndex>-1 && endIndex<selectList.size()&&startIndex<=endIndex){
                  for(Integer i=startIndex;i<=endIndex;i++){
                        subList.add(get(i));
                  }
            }
            return subList;
      }
      
      //Gets Nth sub list by dividing the main list in sublists of SUB_LIST_SIZE(1000 elements).
      
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : getNthSubList (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User> getNthSubList(Integer index){
            Integer subListCount=(Integer)(selectList.size()/SUB_LIST_SIZE);
            if(Math.mod(selectList.size(),SUB_LIST_SIZE)>0) subListCount++;
            if(index>=0 && index<=subListCount){
                  Integer startIndex=index*SUB_LIST_SIZE;
                  Integer endIndex=index*SUB_LIST_SIZE+(SUB_LIST_SIZE-1);
                  if(endIndex>=selectList.size())endIndex=selectList.size()-1;
                  return getSubList(startIndex,endIndex);
            }else{
                  return new List<User>();
            }
      }
      //Gets sublist for given index.


    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get0 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get0(){
            return getNthSubList(0);
      }
      
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get1 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get1(){
            return getNthSubList(1);
      }
      
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get2 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get2(){
            return getNthSubList(2);
      }
      
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get3 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get3(){
            return getNthSubList(3);
      }
      
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get4 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get4(){
            return getNthSubList(4);
      }
      
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get5 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get5(){
            return getNthSubList(5);
      }
      
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get6 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get6(){
            return getNthSubList(6);
      }
      
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get7 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get7(){
            return getNthSubList(7);
      }
      
      
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get8 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get8(){
            return getNthSubList(8);
      }
      
      
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get9 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get9(){
            return getNthSubList(9);
      }
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get9 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get10(){
            return getNthSubList(10);
      }
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get9 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get11(){
            return getNthSubList(11);
      }
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get9 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get12(){
            return getNthSubList(12);
      }
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get9 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get13(){
            return getNthSubList(13);
      }
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get9 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get14(){
            return getNthSubList(14);
      }
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get9 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get15(){
            return getNthSubList(15);
      }
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get9 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get16(){
            return getNthSubList(16);
      }
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get9 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get17(){
            return getNthSubList(17);
      }
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get9 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get18(){
            return getNthSubList(18);
      }
    /*
     * @author : Shaver, Matthew
     * @version : 1.0
     * @return : get9 (List<User>)
     *
     * Modification Log :
     * ----------------------------------------------------------------------------
     * Ver Date Modified By Description
     * ----------------------------------------------------------------------------
     * 0.00 September 9, 2014 Matthew Shaver Initial Draft
     **/
      public List<User>get19(){
            return getNthSubList(19);
      }
}