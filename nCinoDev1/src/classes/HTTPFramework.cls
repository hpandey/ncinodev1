/************************************************************************************
 Apex Class Name     : HTTP Framework
 Version             : 1.0
 Created Date        : 13th Jan 2016
 Function            : Framework used to inititate Web-Service Callouts
 Author              : Jag Valaiyapathy (jvalaiyapathy@deloitte.com)
 Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Jag Valaiyapathy          1/13/2016                Original Version
*************************************************************************************/
public class HTTPFramework {

    private HTTP http {get; set;}
    private HTTPRequest httpRequest {get; set;}
    private HTTPResponse httpResponse {get; set;}
    @testVisible private boolean disableLogging {get; set;}
    private LogFramework lf {get; set;}
    
    //Constructor
    public HTTPFramework(){
        http = new HTTP();
        httpRequest = new HTTPRequest();
        httpResponse = new HTTPResponse();
        lf = LogFramework.getLogger();
        disableLogging = FALSE;
        
    }
    
    /* One to one wrapper for Salesforce Standard System.HTTP Classes */

    //Set EndPoint
    public void setEndpoint(String endPoint){
        httpRequest.setEndpoint(endPoint);
    }
    
    //Set Method
    public void setMethod(String method){
        httpRequest.setMethod(method);
    }
    
    //Set Timeout
    public void setTimeout(Integer timeout){
        httpRequest.setTimeout(timeout);
    }
    
    //Setting single Headers
    public void setHeader(String key, String value){
        httpRequest.setHeader(key, value);
    }
    
    public HttpRequest getRequest(){
        return httpRequest;
    }
    //Setting multiple Headers - Accepts Map    
    public void setMultipleHeaders(Map<String,String> headerMap){
        for(String key: headerMap.keySet()){
            httpRequest.setHeader(key, headerMap.get(key));
        }
    }
    
    //Set Body
    public void setBody(String body){
        httpRequest.setBody(body);
    }
    
    //Set Compression
    public void setCompression(boolean compressed){
        httpRequest.setCompressed(compressed);
    }
    
    //Set Client Certificate Name
    public void setClientCertificateName(String certDevName){
        httpRequest.setClientCertificateName(certDevName);
    }
    
    public HTTPResponse getResponse(){
        return httpResponse;
    }
    
    //Invoking Webservice callouts
    public httpResponse performCallout(){
        try{
            httpResponse = http.send(httpRequest);
            webServiceLogging();
        }catch(CalloutException ex){
            lf.createExceptionLogs(ex);
            throw ex;
        }
        return httpResponse;
    }
    
    public void disableLogging(){
        disableLogging = true;
    }

    private void webServiceLogging(){
        if(!disableLogging){
            lf.createWorkLogs(httpRequest);
            lf.createWorkLogs(httpResponse);
        }
    }
    
}