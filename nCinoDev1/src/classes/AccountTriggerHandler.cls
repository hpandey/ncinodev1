/************************************************************************************
 Apex Class Name     : AccountTriggerHandler
 Version             : 1.0
 Created Date        : 18th March 2016
 Function            : Handler Class for Account Trigger. This Class will not have any logic and will call methods in Helper Class.
 Author              : Deloitte Consulting
 Modification Log    :
* Developer                             Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar         2/16/2016                Original Version
*************************************************************************************/

public Class AccountTriggerHandler implements ITriggerHandler {
 
    //Create Object for Service Class to call appropriate methods.
    AccountService accServiceObj = new AccountService();

    //Handler Method for Before Insert.
    public void BeforeInsert(List<sObject> newItems) {
    }
    
    //Handler Method for Before Update.
    public void BeforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
    }
    
    //Handler Method for Before Delete.    
    public void BeforeDelete(Map<Id, sObject> oldItems) {
    }
    
    //Handler method for After Insert.
    public void AfterInsert(Map<Id, sObject> newItems) {
    }

    //Handler method for After Update.    
    public void AfterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems){
        
        List<Account> listFilteredAcc = new List<Account>();
        Map<String, List<Regulatory_and_Fraud_Indicators__c>> mapResultDML = new Map<String, List<Regulatory_and_Fraud_Indicators__c>>();
        List<Regulatory_and_Fraud_Indicators__c> listResultDelete = new List<Regulatory_and_Fraud_Indicators__c>();
        List<Regulatory_and_Fraud_Indicators__c> listResultInsert = new List<Regulatory_and_Fraud_Indicators__c>();

        for(Account acc : (List<Account>)newItems.values()){
            if(acc.BillingState != ((Account)oldItems.get(acc.Id)).BillingState){
                listFilteredAcc.add(acc);
            }
        }
        
        if(!listFilteredAcc.isEmpty()){
            mapResultDML = accServiceObj.handleAccountAddressChange(listFilteredAcc);
            //Create Object of the Entity Service Class
            if(mapResultDML != null && !mapResultDML.isEmpty()){
                if(mapResultDML.get('Insert') != null && !mapResultDML.get('Insert').isEmpty()){
                    listResultInsert = mapResultDML.get('Insert');
                    List<Database.SaveResult> listSaveResult = Database.Insert(listResultInsert,false);
                    if(!listSaveResult.isEmpty()){
                        //Add Error on Entity Involvement Records if Prescreening Result records are not created.
                        for(Integer index = 0; index < listSaveResult.size(); index++){
                            if(!listSaveResult[index].isSuccess()){
                                if(listResultInsert[index].Loan__c != null){
                                    newItems.get(listResultInsert[index].Customer__c).addError(Label.Prescreening_Insert_Error+ ' : '+UtilityClass.getErrorMessages(listSaveResult[index].getErrors()));
                                }
                            }
                        }
                        utilityClass.processSaveResult(listSaveResult);
                    }
                }
                if(mapResultDML.get('Delete') != null && !mapResultDML.get('Delete').isEmpty()){
                    listResultDelete = mapResultDML.get('Delete');
                    List<Database.DeleteResult> listDeleteResult = Database.Delete(listResultDelete,false);
                    if(!listDeleteResult.isEmpty()){
                        //Add Error on Entity Involvement Records if Prescreening Result records are not created.
                        for(Integer index = 0; index < listResultDelete.size(); index++){
                            if(!listDeleteResult[index].isSuccess()){
                                if(listResultDelete[index].Customer__c != null){
                                    oldItems.get(listResultDelete[index].Customer__c).addError(Label.Prescreening_Insert_Error+ ' : '+UtilityClass.getErrorMessages(listDeleteResult[index].getErrors()));
                                }
                            }
                        }
                        utilityClass.processDeleteResult(listDeleteResult);
                    }
                }
            }
        }
    }

    //Handler method for After Delete    
    public void AfterDelete(Map<Id, sObject> oldItems) {
    }

    //Handler method for After Undelete.
    public void AfterUndelete(Map<Id, sObject> oldItems) {
    }
}