//Edited to deploy under the oneview admin 

public with sharing class ContactTriggerHandler {
    public boolean okToAdd { get; set; }
    public ContactTriggerHandler(){
        //Handler
    }
    public void OnBeforeInsert(List<Contact> insertRecords){
        okToAdd = false;
        // EXECUTE BEFORE INSERT LOGIC
        //List<AccountShare> AccountShares = [SELECT AccountAccessLevel, ContactAccessLevel, RowCause, UserOrGroupId FROM AccountShare where AccountId = :insertRecords[0].AccountId];
        List<AccountShare> AccountShares = [SELECT AccountAccessLevel, ContactAccessLevel, RowCause, UserOrGroupId FROM AccountShare where AccountId = :insertRecords[0].AccountId];
        List<Account> currentAccount = [SELECT Relationship__c FROM Account where Account.Id = :insertRecords[0].AccountId];
        Id relationshipId = currentAccount[0].Relationship__c;
        Id currentuser = UserInfo.getUserID();
       // if user is sysadmin, allow pipeline entry
       // 2013-08-23 PMartin - only check access on non-COI clients
        String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;
        String clientRecType = [Select recordtype.name from account where id = :insertRecords[0].AccountId].recordtype.name;
        if (usrProfileName == 'System Administrator' || clientRecType == 'COI' || clientRecType == 'Internal COI') {
            okToAdd = true;
        } else {
             for (Integer i = 0; i <AccountShares.size(); i++){
                if (AccountShares[i].UserOrGroupId == currentuser){
                //User can add the contact
                okToAdd = true;
                }
            }
        }
        if (!okToAdd){
            insertRecords[0].addError('You are not part of the Client team. please contact the RM');
            //The user isn't in the GroupId or UserId for this Account
            //contact.addError('You are not part of the account team. please contact the RM');
        }else{
            if (insertRecords[0].Relationship__c == null){
                insertRecords[0].Relationship__c = relationshipId;
            }
        }
    }
    public void OnBeforeUpdate(List<Contact> updateRecords){
        String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;
        if (usrProfileName == 'System Administrator' || usrProfileName == 'Regions Migration') {    	
	        List <Id> lstAccnts = new List<Id>();
	        for(Contact otm : updateRecords){
	            lstAccnts.add(otm.AccountId);
	        }    	        	
        	List<Account> currentAccounts = new List<Account>([SELECT Relationship__c FROM Account where Account.Id in :lstAccnts]); 
			for(Contact otm : updateRecords){
            	for (Account atm : currentAccounts){
            		If (otm.AccountId == atm.Id){
            			otm.Relationship__c = atm.Relationship__c;
            		}
            		
            	}
	        }    	        	
        }
    }    
}