@isTest(SeeAllData=true)
private class TestOpportunityTrigger {

  static testMethod void unitTestAdmin() {
    
    Account acc = new Account(Name='Test Account');
    insert acc;
  
    Opportunity opty = new Opportunity(Name='Test Opty', closedate=date.today(), stagename='Prospecting', accountid=acc.id);
    insert opty;   
    
     Product2 prod = new Product2(Name = 'Test Product', Family='Capital Markets');
     insert prod;
        
        Pricebook2 pbook = [Select Id,Name from pricebook2 where isStandard=true];     
          system.debug('****prod2****'+prod.Id);
             
    //Pricebook2 pbook = [Select Id,Name from pricebook2 where isStandard=true];
    PriceBookEntry pbe = new PriceBookEntry ( Pricebook2Id=pbook.id, Product2Id=prod.id,UnitPrice=20, IsActive=true);
            insert pbe;   
    
                                                      
    Opportunity opty2 = new Opportunity(Name='Test Opty2',  Lookback_Pipeline__c = opty.ID, closedate=date.today(), stagename='Prospecting', accountid=acc.id);
    insert opty2;
     OpportunityLineItem litm = new OpportunityLineItem(PricebookEntryId=pbe.Id,
                                    OpportunityId=opty2.id, Estimated_Revenue__c=20,
                                              Quantity=1, unitPrice=1);
                                                  
    opty2.Lookback__c =true;
    update opty2;  
    
  }
  
  //Start Testing for OpportunityTeamMember trigger/handler
   static testMethod void unitTestTeam() {  
    Account acc = new Account(Name='Test Account');
    insert acc;   	  
   	Opportunity opty = new Opportunity(Name='Test Opty', closedate=date.today(), stagename='Prospecting', accountid=acc.id);
    insert opty;      
    // code to test oppteamMember code                 
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      
    User standardUser = new User(Alias = 'a2087', Email='standarduser0@dummy.com', 
      EmailEncodingKey='UTF-8', LastName='Testing0', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduser0@dummy.com',
      C_I_Type__c = 'CMM',
      LOB__c = 'Commercial and Industrial (C&I)'      
      );
      insert standardUser;
      date myDate = date.today();
        Goal__c testGoal1 = new Goal__c();
        testGoal1.User__c = standardUser.Id;
        testGoal1.Call_Total_Goal__c = 1;
        testGoal1.Time_Period__c = 'Monthly';
        testGoal1.Start_Date__c = myDate.addMonths(-1);
        testGoal1.End_Date__c = myDate.addMonths(1);
        insert testGoal1;
    OpportunityTeamMember NewOtm = new OpportunityTeamMember(UserId=standardUser.Id, TeamMemberRole='Area President', OpportunityId=opty.id, Deal_Credit__c=100.0);
    insert NewOtm;
    
    User standardUser1 = new User(Alias = 'b2087', Email='standarduser1@dummy.com', 
      EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@dummy.com',
      C_I_Type__c = 'GIB',
      LOB__c = 'Commercial and Industrial (C&I)',
      Specialized_Industries__c = true
      );
    insert standardUser1;

    OpportunityTeamMember NewOtm1 = new OpportunityTeamMember(UserId=standardUser1.Id, TeamMemberRole='Area President', OpportunityId=opty.id, Deal_Credit__c=100.0);
    insert NewOtm1;
    
    
   }
   static testMethod void unitTestTeam2() {  
    Account acc = new Account(Name='Test Account');
    insert acc;   	  
  
   	Opportunity opty2 = new Opportunity(Name='Test Opty', closedate=date.today(), stagename='Prospecting', accountid=acc.id);
    insert opty2;        
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];        
    User standardUser2 = new User(Alias = 'c2087', Email='standarduser2@dummy.com', 
      EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduser2@dummy.com',
      LOB__c = 'Regions Business Capital', 
      Specialized_Industries__c = true     
      );
      insert standardUser2;
    OpportunityTeamMember NewOtm2 = new OpportunityTeamMember(UserId=standardUser2.Id, TeamMemberRole='Area President', OpportunityId=opty2.id, Deal_Credit__c=100.0);
    insert NewOtm2;
    
    User standardUser3 = new User(Alias = 'd2087', Email='standarduser3@dummy.com', 
      EmailEncodingKey='UTF-8', LastName='Testing3', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduser3@dummy.com',
      C_I_Type__c = 'Corporate',
      LOB__c = 'Commercial and Industrial (C&I)'
      );
    insert standardUser3;

    OpportunityTeamMember NewOtm3 = new OpportunityTeamMember(UserId=standardUser3.Id, TeamMemberRole='Area President', OpportunityId=opty2.id, Deal_Credit__c=100.0);
    insert NewOtm3;
    
    delete NewOtm3;
    
   }
   static testMethod void unitTestTeam3() {  
    Account acc = new Account(Name='Test Account');
    insert acc;   	  
   	Opportunity opty = new Opportunity(Name='Test Opty', closedate=date.today(), stagename='Prospecting', accountid=acc.id);
    insert opty;      
    // code to test oppteamMember code                 
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      
    User standardUser = new User(Alias = 'a2087', Email='standarduser0@dummy.com', 
      EmailEncodingKey='UTF-8', LastName='Testing0', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduser0@dummy.com',
      C_I_Type__c = 'Affordable Housing',
      LOB__c = 'Real Estate Banking'      
      );
      insert standardUser;
    OpportunityTeamMember NewOtm = new OpportunityTeamMember(UserId=standardUser.Id, TeamMemberRole='Area President', OpportunityId=opty.id, Deal_Credit__c=100.0);
    insert NewOtm;
    
    User standardUser1 = new User(Alias = 'b2087', Email='standarduser1@dummy.com', 
      EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@dummy.com',
      C_I_Type__c = 'Home Builder Finance',
      LOB__c = 'Real Estate Banking'
      );
    insert standardUser1;

    OpportunityTeamMember NewOtm1 = new OpportunityTeamMember(UserId=standardUser1.Id, TeamMemberRole='Area President', OpportunityId=opty.id, Deal_Credit__c=100.0);
    insert NewOtm1;
    
    
   }

  static testMethod void unitTestTeam4() {  
    Account acc = new Account(Name='Test Account');
    insert acc;   	  
   	Opportunity opty = new Opportunity(Name='Test Opty', closedate=date.today(), stagename='Prospecting', accountid=acc.id);
    insert opty;      
    // code to test oppteamMember code                 
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      
    User standardUser = new User(Alias = 'a2087', Email='standarduser0@dummy.com', 
      EmailEncodingKey='UTF-8', LastName='Testing0', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduser0@dummy.com',
      LOB__c = 'Energy & Natural Resources',
      Specialized_Industries__c = true      
      );
      insert standardUser;
    OpportunityTeamMember NewOtm = new OpportunityTeamMember(UserId=standardUser.Id, TeamMemberRole='Area President', OpportunityId=opty.id, Deal_Credit__c=100.0);
    insert NewOtm;
    
    User standardUser1 = new User(Alias = 'b2087', Email='standarduser1@dummy.com', 
      EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@dummy.com',
      LOB__c = 'Healthcare'
      );
    insert standardUser1;

    OpportunityTeamMember NewOtm1 = new OpportunityTeamMember(UserId=standardUser1.Id, TeamMemberRole='Area President', OpportunityId=opty.id, Deal_Credit__c=100.0);
    insert NewOtm1;
    
    
   }
   //Start Testing for OpportunityTeamMember trigger/handler
  
  static testMethod void unitTestStanUser() {
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      
    User standardUser = new User(Alias = 'd2087', Email='standarduser@dummy.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@dummy.com');
    
    System.runAs(standardUser){
    Account acc = new Account(Name='Test Account');
    insert acc;
  
    Opportunity opty = new Opportunity(Name='Test Opty', closedate=date.today(), stagename='Prospecting', accountid=acc.id);
    insert opty;   
    }
  
  }
}