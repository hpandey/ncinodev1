/**************************************************************************************
* Name            : TestDeploymentArtifactsV2Controller
* Date            : 15 Feb,2011
* Description     : For code coverage of DeploymentArtifactsV2Controller
* Author          : Urminder Vohra
* Modified        
 
  02/19/2011	Urminder : changed the types of all lists and updated the test class. 
  04/06/2011	Urminder :added new method createData() to updated the maps and list of controller
				          and method createdDateTest() to compare created Date of SObject and its Fields
  04/27/2011    Urminder :to get coverage of soapMeatadata added its classes object in createData()
***************************************************************************************/
@isTest
private class TestDeploymentArtifactsV2Controller {
    
    public static List<DeploymentArtifactsV2Controller.WrapperApexClass> classList;
    public static List<DeploymentArtifactsV2Controller.WrapperApexTrigger> triggerList;
    public static List<DeploymentArtifactsV2Controller.WrapperApexPage> pageList;
    public static List<DeploymentArtifactsV2Controller.WrapperScontrol> scontrolList;
    public static List<DeploymentArtifactsV2Controller.WrapperReport> reportList;
    public static List<DeploymentArtifactsV2Controller.WrapperProfile> profileList;
    public static List<DeploymentArtifactsV2Controller.WrapperWeblink> weblinkList;
    public static List<DeploymentArtifactsV2Controller.WrapperStaticResource> staticResourceList;
    public static List<DeploymentArtifactsV2Controller.WrapperRecordType> recordTypeList;
    public static List<DeploymentArtifactsV2Controller.WrapperDashbord> dashboardList;
    public static List<DeploymentArtifactsV2Controller.WrapperApexComponent> componentList;
    public static String XMLString;
    
    public static void methods(DeploymentArtifactsV2Controller dac) {
    	classList  = dac.getnewClasses();
	 	
	 	pageList = dac.getnewPages();
	 	
	 	triggerList = dac.getnewTriggers(); 
	 	
	 	scontrolList = dac.getnewScontrols();
	 	
	 	reportList = dac.getnewReports();
	 	
	 	profileList = dac.getnewProfiles();
	 	
	 	weblinkList = dac.getnewWeblinks();
	 	
	 	staticResourceList = dac.getnewStaticResources();
	 	
	 	recordTypeList = dac.getnewRecordTypes();
	 	
	 	dashboardList = dac.getnewDashboards();
	 	
	 	componentList = dac.getnewComponents();
    }
    /**
     * method to compare lastModifiedDate of SObject and its Fields
     */ 
    static testMethod void lastModifiedDateTest() {
    	//test.startTest();
    	String datenow = String.valueOf(date.today());
    	String[] dates = datenow.split('-');
    	datenow = dates[1] + '/' + dates[2] + '/' + dates[0];
    	ApexPages.currentPage().getParameters().put('searchDate',datenow);
    	ApexPages.currentPage().getParameters().put('dtClause','LastModifiedDate');
    	DeploymentArtifactsV2Controller dac = new DeploymentArtifactsV2Controller();
    	
	 	methods(dac);
	 	XMLString = '<?xml version="1.0" encoding="UTF-8"?>'+
					'\n<Package xmlns="http://soap.sforce.com/2006/04/metadata">' +
					'\n <version>20.0</version>'+
					'\n </Package>';
	 	dac.fetchXML();
	 	
	 	//changing the search date to Jan 1,2010.
	 	dac.searchdatetime = Date.newInstance(2010, 1, 1);
	 	//selecting all data to show as xml 
	 	classList  = dac.getnewClasses();
	 
	 	for(DeploymentArtifactsV2Controller.WrapperApexClass cls : classList) {
	 		cls.isSelected = true;
	 	}
	 	
	 	pageList = dac.getnewPages();
	 	for(DeploymentArtifactsV2Controller.WrapperApexPage pg : pageList) {
	 		pg.isSelected = true;
	 	}
	 	
	 	triggerList = dac.getnewTriggers();
	 	for(DeploymentArtifactsV2Controller.WrapperApexTrigger pg : triggerList) {
	 		pg.isSelected = true;
	 	}
	 	scontrolList = dac.getnewScontrols();
	 	for(DeploymentArtifactsV2Controller.WrapperScontrol pg : scontrolList) {
	 		pg.isSelected = true;
	 	}
	 	reportList = dac.getnewReports();
	 	for(DeploymentArtifactsV2Controller.WrapperReport pg : reportList) {
	 		pg.isSelected = true;
	 	}
	 	profileList = dac.getnewProfiles();
	 	for(DeploymentArtifactsV2Controller.Wrapperprofile pg : profileList) {
	 		pg.isSelected = true;
	 	}
	 	weblinkList = dac.getnewWeblinks();
	 	for(DeploymentArtifactsV2Controller.WrapperWeblink pg : weblinkList) {
	 		pg.isSelected = true;
	 	}
	 	staticResourceList = dac.getnewStaticResources();
	 	for(DeploymentArtifactsV2Controller.WrapperStaticResource pg : staticResourceList) {
	 		pg.isSelected = true;
	 	}
	 	recordTypeList = dac.getnewRecordTypes();
	 	for(DeploymentArtifactsV2Controller.WrapperRecordType pg : recordTypeList) {
	 		pg.isSelected = true;
	 	}
	 	dashboardList = dac.getnewDashboards();
	 	for(DeploymentArtifactsV2Controller.WrapperDashbord pg : dashboardList) {
	 		pg.isSelected = true;
	 	}
	 	componentList = dac.getnewComponents();
	 	for(DeploymentArtifactsV2Controller.WrapperApexComponent pg : componentList) {
	 		pg.isSelected = true;
	 	}
	 	dac.dateClause = 'LastModifiedDate';
	 	dac.fetchXML();
	 	createData(dac);
	 	dac.showArtifacts();
	 	dac.getitems();
	 	dac.DeploymentArtifactsV2Excel();
	 	
	 	dac.getcustomFields('Object1__c',23);
	 	dac.getcustomFields('StdObject',32);
	 	
	 	
	 	//test.stopTest();
    }
    /**
     * method to compare created Date of SObject and its Fields
     */
     static testMethod void createdDateTest() {
     	String datenow = String.valueOf(date.today());
    	String[] dates = datenow.split('-');
    	datenow = dates[1] + '/' + dates[2] + '/' + dates[0];
    	ApexPages.currentPage().getParameters().put('searchDate',datenow);
    	
    	
     	DeploymentArtifactsV2Controller controller = new DeploymentArtifactsV2Controller();
     	
     	methods(controller);
     	createData(controller);
     	controller.fetchXML();
     	controller.getcustomFields('Object1__c',23);
	 	controller.getcustomFields('StdObject',32);
     }
    /**
     * in this method we create update the list and map used in DeploymentArtifactsV2Controller 
     */
     public static void createData(DeploymentArtifactsV2Controller controller) {
     	String object1 = 'Object1__c';//Custom Object
     	String object2 = 'StdObject';//Standard Object
     	String datenow = String.valueOf(date.today());
    	String[] dates = datenow.split('-');
    	datenow = dates[1] + '/' + dates[2] + '/' + dates[0];
    	
    	List<DeploymentArtifactsV2Controller.Wrapper> listWrap = new List<DeploymentArtifactsV2Controller.Wrapper>();
    	DeploymentArtifactsV2Controller.Wrapper wrap = new DeploymentArtifactsV2Controller.Wrapper('TestWrapperName');
    	wrap.lastModifiedDate = date.parse(datenow);
    	wrap.createdDate = date.parse(datenow);
    	wrap.isSelected = true;
    	listWrap.add(wrap);
    	DeploymentArtifactsV2Controller.Wrapper wrap2 = new DeploymentArtifactsV2Controller.Wrapper('TestWrapperName2',datetime.now(),datetime.now());
    	wrap2.lastModifiedDate = date.parse(datenow);
    	wrap2.createdDate = date.parse(datenow);
    	wrap2.isSelected = true;
    	listWrap.add(wrap2);
    	
    	
    	list<String> listField = new list<String>();
     	listField.add('CustomField1__c');
     	listField.add('CustomField2__c');
     	//updating list and maps for Custom Object and its fields
     	List<DeploymentArtifactsV2Controller.Wrapper> listWrap2 = new List<DeploymentArtifactsV2Controller.Wrapper>();
     	DeploymentArtifactsV2Controller.Wrapper obj = new DeploymentArtifactsV2Controller.Wrapper(Object1);
     	
     	obj.lastModifiedDate = date.parse(datenow);
     	obj.createdDate = date.parse(datenow);
     	obj.listCstmFlds = listField;
     	obj.listLayouts = listWrap;
     	obj.wrapList = listWrap;
     	obj.isSelected = true;
     	controller.mapSObjects.put(Object1,obj);
     	controller.customFieldMap.put(Object1,listWrap);
     	controller.mapLayouts.put(Object1,listWrap);
     	listWrap2.add(obj);
     	controller.listCstmObj = listWrap2;
     	//updating list and maps for Standard Object and its Custom fields
     	DeploymentArtifactsV2Controller.Wrapper obj2 = new DeploymentArtifactsV2Controller.Wrapper(object2);
     	obj2.lastModifiedDate = date.parse(datenow).addDays(-3);
     	obj2.createdDate = date.parse(datenow).addDays(-5);
     	obj2.listCstmFlds = listField;
     	obj2.isSelected = true;
     	obj2.wrapList = listWrap;
     	listWrap2.add(obj2);
     	controller.listStandardObj = listWrap2;
     	controller.listCustomTab = listWrap2;
     	controller.listCustomLabel = listWrap2;
     	controller.listAnalyticSnapshot = listWrap2;
     	controller.listReportType = listWrap2;
     	controller.listWorkflow = listWrap2; 
     	controller.mapSObjects.put(object2,obj2);
     	controller.customFieldMap.put(object2,listWrap);
     	//controller.mapLayouts.put(object2,listWrap);
     	
     	//intilise the instatnces of the classes in soapMetadata
     	 soapMetadata.listMetadataResponse_element resp = new soapMetadata.listMetadataResponse_element();
     	 soapMetadata.listMetadata_element elm = new soapMetadata.listMetadata_element();
	     soapMetadata.LogInfo log = new soapMetadata.LogInfo();
	     soapMetadata.CallOptions_element calloption = new soapMetadata.CallOptions_element();
	     soapMetadata.FileProperties fp = new soapMetadata.FileProperties();
	     soapMetadata.DebuggingInfo_element di = new soapMetadata.DebuggingInfo_element();
	     soapMetadata.DebuggingHeader_element dh = new soapMetadata.DebuggingHeader_element(); 
     }
}