/**=====================================================================
 * Appirio, Inc
 * Name: REB_EmergingRiskNotifications
 * Description: Story S-271020  Task # T-335124 Send emails to ER Recipients
 *              
 * Created Date: 12/5/2014
 * Created By: Rita Washington (Appirio)
 =====================================================================*/

public class REB_EmergingRiskNotifications {

    public void SendERnotifications(Call_Plan__c callplan) {
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        String CPWebAddress;
        List<String> lstEmails = new List<String>();
        String clientName;
        List<String> emailAddress = new List<String>();
        
      //Create Emails for all Recipients
        CPWebAddress = URL.getSalesforceBaseUrl().toExternalForm()+'/'+ callplan.Id;
        
        List<Risk_Notification_Recipient__c> lstRnR = [Select r.Recipient__r.Email, r.Recipient__c, r.Name, r.Id, r.Call_Plan__c, r.Email_Sent__c 
                                                       From Risk_Notification_Recipient__c r
                                                       Where Call_Plan__c =: callplan.Id AND Email_Sent__c = false];            
            if(lstRnR.size() > 0) {
                Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();   
                CPWebAddress = URL.getSalesforceBaseUrl().toExternalForm()+'/'+ callplan.Id;
                clientName = callplan.Client__r.Name;                         	
                for (Risk_Notification_Recipient__c rn : lstRnR) {
                    //Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
                    //CPWebAddress = URL.getSalesforceBaseUrl().toExternalForm()+'/'+ callplan.Id;
                    //clientName = callplan.Client__r.Name;
                    System.Debug('EMAIL: '+ rn.Recipient__r.Email);
                    lstEmails.add(rn.Recipient__r.Email);
                    //singleMail.setToAddresses(lstEmails); 
                    //constructRemainingMsg(singleMail, clientName, CPWebAddress);
                    //emailList.add(singleMail);
                }
                singleMail.setToAddresses(lstEmails); 
                constructRemainingMsg(singleMail, clientName, CPWebAddress);
                emailList.add(singleMail);
                sendEmails(emailList, lstRnR); 
            }
         //sendEmails(emailList, lstRnR);
    }
    
    private void constructRemainingMsg(Messaging.SingleEmailMessage sem, String clientName, String CPWebAddress) {
            sem.setSaveAsActivity(false);
            sem.setSubject('Emerging Risk Factor Identified - Please Review - '+ clientName);
            sem.setPlainTextBody('As a member of the client team and/or a member of either the first or second lines of defense, ' +
                                 'you are receiving this email as a notification that an emerging risk has been identified on a Call Plan. ' +
                                 'Please click on the link below to view the emerging risk factor information on the Call Plan. ' +
                                 'Reminder – Call Plans may contain sensitive information and in cases of emerging risks, the information ' +
                                 'should be held strictly confidential. Should you have any questions about the emerging risks, please contact ' +
                                 'the associate that entered the information in the Call Plan.'+'\n\n\n'+ CPWebAddress +'\n\n'+
                                 'OneView Administrator');
    }
    
    private void sendEmails(List<Messaging.SingleEmailMessage> emailList, List<Risk_Notification_Recipient__c> lstRnR) {
        String EmailFailure;
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
       
        if(emails.size() < 100) {
            try {
                Messaging.sendEmail(emailList);
                for(Risk_Notification_Recipient__c r : lstRnR) { r.Email_Sent__c = true; }
            }
            catch(System.EmailException emlEx) {
                EmailFailure = 'Email Failed: ' + emlEx;
                for(Risk_Notification_Recipient__c r : lstRnR) { r.Email_Sent__c = false; }
            }
        }
         update lstRnR;
    }

}