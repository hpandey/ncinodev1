@isTest
public class EmailTaskTriggerTestClass{

    static testMethod void validateEmailTaskTriggerTestClass() {
      
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(
            Alias = 'standt',
            Email='ncinouser@ncino.com', 
            EmailEncodingKey='UTF-8',
            LastName='Testing',
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US',
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles',
            UserName='standarduser121243548654321@testorg.com');
            insert u;

        Task t = new Task(OwnerId = u.Id, Subject = 'Test', Priority = 'Normal', Status = 'Not Started', Recieve_Email_when_Completed__c = True); 
        //Insert Task
        insert t;
        User myUser = [SELECT Id, Alias, LastName FROM User WHERE Id = : t.OwnerId];
        
        System.runAs(myUser){ 
            t.Status = 'Completed';
            update(t);
        }
    }
}