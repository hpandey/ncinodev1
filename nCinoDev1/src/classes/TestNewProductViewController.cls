@isTest
private class TestNewProductViewController {
    
    @isTest public static void testGetInvolvedLoans() {
        Account testAccount = createAccount();
        LLC_BI__Loan__c testLoan = createLoan();
        LLC_BI__CFG_ConfigValue__c testCFG1 = createCFGValue();
        LLC_BI__CFG_ConfigValue__c testCFG2 = createCFGValue1();
        LLC_BI__CFG_ConfigValue__c testCFG3 = createCFGValue2();

        ApexPages.StandardController stdController = new ApexPages.StandardController(testAccount);
        NewProductViewController pv = new NewProductViewController(stdController);

        Test.startTest();

        LLC_BI__Legal_Entities__c testEntityNoLoan = new LLC_BI__Legal_Entities__c(
            Name = 'EntityWithLoan',
            LLC_BI__Account__c = testAccount.Id,
            LLC_BI__Loan__C = testLoan.Id);
        insert testEntityNoLoan;

        pv.getNewLoanLink();
        pv.getNewDepositLink();
        pv.getNewEntityInvolvementLink();

        Test.stopTest();
    }
    
    @isTest
    public static void testGetInvolvedDeposits() {
        Account testAccount = createAccount();
        LLC_BI__Deposit__c testDeposit = createDeposit();
        LLC_BI__CFG_ConfigValue__c testCFG1 = createCFGValue();
        LLC_BI__CFG_ConfigValue__c testCFG2 = createCFGValue1();
        LLC_BI__CFG_ConfigValue__c testCFG3 = createCFGValue2();

        ApexPages.StandardController stdController = new ApexPages.StandardController(testAccount);
        NewProductViewController pv = new NewProductViewController(stdController);

        Test.startTest();

        LLC_BI__Legal_Entities__c testEntityNoDeposit = new LLC_BI__Legal_Entities__c(
            Name = 'EntityNoDeposit',
            LLC_BI__Account__c = testAccount.Id,
            LLC_BI__Deposit__c = testDeposit.Id);
        insert testEntityNoDeposit;

        Test.stopTest();
    }

    private static Account createAccount(){
        Account testAccount = new Account(
            Name = 'TestAccount');
        insert testAccount;
        return testAccount;
    }
    private static LLC_BI__Loan__c createLoan(){
        LLC_BI__Loan__c testLoan = new LLC_BI__Loan__c(
            Name = 'TestLoan');
        insert testLoan;
        return testLoan;
    }
    private static LLC_BI__Deposit__c createDeposit(){
        LLC_BI__Deposit__c testDeposit = new LLC_BI__Deposit__c(
            Name = 'TestDeposit');
        insert testDeposit;
        return testDeposit;
    }

    private static LLC_BI__CFG_ConfigValue__c createCFGValue() {
        LLC_BI__CFG_ConfigValue__c testCFG = new LLC_BI__CFG_ConfigValue__c(
            LLC_BI__Category__c = 'Products & Services',
            LLC_BI__fieldValue__c = 'true',
            LLC_BI__Key__c = 'Show_New_Loan_Button');
        insert testCFG;
        return testCFG;
    }
    
    private static LLC_BI__CFG_ConfigValue__c createCFGValue1() {
        LLC_BI__CFG_ConfigValue__c testCFG = new LLC_BI__CFG_ConfigValue__c(
            LLC_BI__Category__c = 'Products & Services',
            LLC_BI__fieldValue__c = 'true',
            LLC_BI__Key__c = 'Show_New_Deposit_Button');
        insert testCFG;
        return testCFG;
    }

    private static LLC_BI__CFG_ConfigValue__c createCFGValue2() {
        LLC_BI__CFG_ConfigValue__c testCFG = new LLC_BI__CFG_ConfigValue__c(
            LLC_BI__Category__c = 'Products & Services',
            LLC_BI__fieldValue__c = 'true',
            LLC_BI__Key__c = 'Show_New_Entity_Button');
        insert testCFG;
        return testCFG;
    }
}