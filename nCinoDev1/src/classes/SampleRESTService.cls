@RestResource(urlMapping='/SampleRESTService/*')
global with sharing class SampleRESTService {

	@Httpget	
	global static void getAccountByName() {

	    String searchStr = RestContext.request.params.containsKey('search')?RestContext.request.params.get('search'):'';
	    searchStr = (searchStr!=null && !String.isBlank(searchStr))?searchStr='%' + searchStr + '%':searchStr;
	    list<Account> listsObjectAccounts = [Select Id, Name FROM Account where Name LIKE :searchStr limit 10];
	    system.debug('###' + JSON.serializePretty(listsObjectAccounts));
	    RestContext.response.responseBody =  Blob.valueOf(JSON.serialize(listsObjectAccounts));
     
  	}
}