@isTest
public class TestCoachingReviewTriggerHandler {

    static testMethod void shareCoachingReview() {
    
        Profile p = [select id from profile where name='Regions Standard User'];
        User uRM1 = new User(alias = 'uRM123', email= 'uRM1234@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id, country='United States',
          timezonesidkey='America/Los_Angeles', username= 'urTest123@testorg.com');
        Insert uRM1;
    
        Coaching_Review__c cr1 = new Coaching_Review__c();
        cr1.Coaching_Date__c = date.today();
        cr1.Reminder_Date__c = date.today(); 
        cr1.Status__c = 'Draft';
        cr1.Relationship_Manager__c = uRM1.id;
        Insert cr1;
        
        System.runAs(uRM1){
            System.AssertEquals([Select id from Coaching_Review__c].size(),0);
        }
        
        cr1.Status__c = 'Review';
        Update cr1;
        
        System.runAs(uRM1){
            System.AssertEquals([Select id from Coaching_Review__c].size(),1);
        }
        
    }



}