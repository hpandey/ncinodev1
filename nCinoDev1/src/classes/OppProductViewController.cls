public with sharing class OppProductViewController {

	private final Opportunity opp;
	public Opportunity acctUsed {get;set;}
    public List<LLC_BI__Legal_Entities__c> loans {get;set;}
    public List<LLC_BI__Legal_Entities__c> deposits {get;set;}

	public OppProductViewController(ApexPages.StandardController controller) {
        this.opp = (Opportunity)controller.getRecord();

        this.acctUsed = [SELECT AccountId FROM Opportunity WHERE Id = :opp.Id LIMIT 1];

        this.loans = getLoanProducts();

        this.deposits = getDepositProducts();
    }

    public List<Schema.FieldSetMember> getLoanFields() {
        return SObjectType.LLC_BI__Legal_Entities__c.FieldSets.LoanProducts.getFields();
    }

    public List<Schema.FieldSetMember> getDepositFields() {
        return SObjectType.LLC_BI__Legal_Entities__c.FieldSets.DepositProducts.getFields();
    }

    private List<LLC_BI__Legal_Entities__c> getLoanProducts() {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getLoanFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id FROM LLC_BI__Legal_Entities__c WHERE LLC_BI__Account__c=\'' + acctUsed.AccountId + '\' AND LLC_BI__Loan__c != null AND LLC_BI__Deposit__c = null ORDER BY LLC_BI__Borrower_Type__c ASC';
        return Database.query(query);
    }

    private List<LLC_BI__Legal_Entities__c> getDepositProducts() {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getDepositFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id FROM LLC_BI__Legal_Entities__c WHERE LLC_BI__Account__c=\'' + acctUsed.AccountId + '\' AND LLC_BI__Loan__c = null AND LLC_BI__Deposit__c != null ORDER BY LLC_BI__Relationship_Type__c ASC';
        return Database.query(query);
    }
}