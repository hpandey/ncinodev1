@isTest
global class WebServiceMockImpl implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       AccountBalance.FetchSalesForceAccountProfileResponse_element respElement = 
           new AccountBalance.FetchSalesForceAccountProfileResponse_element();
           
      AccountBalanceArrays.KeyValueOfstringstring_element[] keyValue= new AccountBalanceArrays.KeyValueOfstringstring_element[5];
      //{new AccountBalanceArrays.KeyValueOfstringstring_element()};
      keyValue[0]=new AccountBalanceArrays.KeyValueOfstringstring_element();
      keyValue[0].Key='AcceptRejectCode';
      keyValue[0].Value='A';
      keyValue[1]=new AccountBalanceArrays.KeyValueOfstringstring_element();
      keyValue[1].Key='RejectReasonCode' ;
      keyValue[1].Value='' ;
      keyValue[2]=new AccountBalanceArrays.KeyValueOfstringstring_element();
      keyValue[2].Key='ApplicationCode' ;
      keyValue[2].Value='DA' ;
      keyValue[3]=new AccountBalanceArrays.KeyValueOfstringstring_element();
      keyValue[3].Key='AccountNumber' ;
      keyValue[3].Value='000000000003207242' ;
      keyValue[4]=new AccountBalanceArrays.KeyValueOfstringstring_element();
      keyValue[4].Key='BalanceAvailable' ;
      keyValue[4].Value='0000000000000' ;
            
      AccountBalanceArrays.ArrayOfKeyValueOfstringstring keyValueString= new AccountBalanceArrays.ArrayOfKeyValueOfstringstring();
      keyValueString.KeyValueOfstringstring=keyValue;
       
      respElement.FetchSalesForceAccountProfileResult = keyValueString;
       
       response.put('response_x', respElement); 
   }
}