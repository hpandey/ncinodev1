@isTest(SeeAllData=true) 
public class TestAccountDetailModal{

/*static RecordType DepositType;
static RegionsConnectivity__c c;
static Account testAccount;
static Bank_Account_Details__c testBankAccount;

static void testData(){
  DepositType=[select Id from RecordType where Name='Deposit' and SobjectType='Bank_Account_Details__c'];

//Define Custom Setting
 c = new RegionsConnectivity__c();
    c.Name = 'AccountBalance_WS';
    c.RegionsURL__c='https://sfdc.regionstest.com/SalesForce/SalesForceAccountProfile.svc';
    c.UserID__c='SalesForce001';
    c.Password__c='D0g5h1tTac0';
    c.ApplicationCode__c='DA';
    c.OperatorID__c='MQCSMDN';
    c.MessageForError__c='Please contact customer service at (888) 888-8888.';
    c.MessageForRejection__c='Please contact customer service at (888) 888-8888.';
    insert c;
     
    testAccount=new Account(Name='Test Client',Type='Other', Segment__c='');
    insert testAccount;

    testBankAccount= new Bank_Account_Details__c(RecordTypeId=DepositType.Id,Client__c=testAccount.Id, 
    Name='Test Bank Account');
    insert testBankAccount;
}*/

static testMethod void testConnectionSuccess(){
Bank_Account_Details__c testBankAccount=[Select Id,Source_Account_Number__c,Bank_Number__c, Source_System_Code__c From Bank_Account_Details__c LIMIT 1];
ApexPages.StandardController sc = new ApexPages.StandardController(testBankAccount);

// create an instance of the controller

AccountDetailModal PageCon = new AccountDetailModal(sc);

//Test converage for the visualforce page
PageReference pageRef = Page.BankAccountDetail;
        pageRef.getParameters().put('id', String.valueOf(testBankAccount.Id));
        //Test.setCurrentPage(pageRef);

Test.setCurrentPageReference(pageRef);
Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
PageCon.showPopup();
PageCon.closePopup();

}

static testMethod void testCalloutException(){
//RecordType DepositType=[select Id from RecordType where Name='Deposit' and SobjectType='Bank_Account_Details__c'];

//Define Custom Setting
/*RegionsConnectivity__c c = new RegionsConnectivity__c();
    c.Name = 'AccountBalance_WS';
    c.RegionsURL__c='https://sfdc.regionstest.com/SalesForce/SalesForceAccountProfile.svc';
    c.UserID__c='SalesForce001';
    c.Password__c='D0g5h1tTac0';
    c.ApplicationCode__c='DA';
    c.OperatorID__c='MQCSMDN';
    c.MessageForError__c='Please contact customer service at (888) 888-8888.';
    c.MessageForRejection__c='Please contact customer service at (888) 888-8888.';
    insert c;*/

Account testAccount=new Account(Name='Test Client',Type='Other', Segment__c='');
insert testAccount;

Bank_Account_Details__c testBankAccount= new Bank_Account_Details__c(Client__c=testAccount.Id, Name='Test Bank Account', Bank_Number__c='Test Bk #');
insert testBankAccount;

ApexPages.StandardController sc = new ApexPages.StandardController(testBankAccount);

// create an instance of the controller

AccountDetailModal PageCon = new AccountDetailModal(sc);

//Test converage for the visualforce page
PageReference pageRef = Page.BankAccountDetail;
pageRef.getParameters().put('id', String.valueOf(testBankAccount.Id));

Test.setCurrentPageReference(pageRef);
Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
PageCon.showPopup();

}

static testMethod void testGenericException(){
//RecordType DepositType=[select Id from RecordType where Name='Deposit' and SobjectType='Bank_Account_Details__c'];

Account testAccount=new Account(Name='Test Client',Type='Other', Segment__c='');
insert testAccount;

Bank_Account_Details__c testBankAccount= new Bank_Account_Details__c(Client__c=testAccount.Id, Name='Test Bank Account', Bank_Number__c='Test BK #' );
insert testBankAccount;

ApexPages.StandardController sc = new ApexPages.StandardController(testBankAccount);

// create an instance of the controller

AccountDetailModal PageCon = new AccountDetailModal(sc);

//Test converage for the visualforce page
PageReference pageRef = Page.BankAccountDetail;
pageRef.getParameters().put('id', String.valueOf(testBankAccount.Id));

Test.setCurrentPageReference(pageRef);

PageCon.showPopup();
}

static testMethod void testDoFormatting(){
AccountDetailModal.doFormatting(-1234567,'T','t');
}

}