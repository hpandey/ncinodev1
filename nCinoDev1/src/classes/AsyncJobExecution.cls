/**********************************************************************************************
 * Name         : AsyncJobExecution
 * Author       : Deloitte Consulting
 * Description  : Class which contains logic of Apex Queueable jobs.
 *********************************************************************************************/


public class AsyncJobExecution implements Queueable, Database.AllowsCallouts {
    
    Set<Id> setRecordIDs;
    Asynchronous_Queue__c asyncQueueRecord;
    Type objType;

    /***
    * Method name    : AsyncJobExecution
    * Description    : Constructor
    * Return Type    : None
    * Parameter      : Asynchronous_Queue__c asyncQueueRecord
    */

    public AsyncJobExecution(Asynchronous_Queue__c asyncQueueRecord) {
        this.asyncQueueRecord = asyncQueueRecord;
        
        if(String.isBlank(asyncQueueRecord.Functional_Class__c)) {

            AsyncJobExecutionException objException = new AsyncJobExecutionException('Functional Class for Asychronous Queue' 
                                                                                        +' Record cannot be null or blank');

            throw objException;
        }

        else {

            List<ApexClass> list_ApexClass = [SELECT Id FROM ApexClass WHERE Name = :asyncQueueRecord.Functional_Class__c];
            
            if(!list_ApexClass.isEmpty()) {
            
                this.objType = Type.forName(asyncQueueRecord.Functional_Class__c);
            }
            
            else {
                AsyncJobExecutionException objException = new AsyncJobExecutionException('Invalid Functional Class');
            
                throw objException;
            }
        }
    }
    
    /***
    * Method name    : execute
    * Description    : Execute method have logic of queuebale apex.
    * Return Type    : void
    * Parameter      : QueueableContext context
    */

    public void execute(QueueableContext context) {
        
        try{
            if(asyncQueueRecord.isCallout__c) { 
                OutboundIntegrationInterface objOutboundIntg = (OutboundIntegrationInterface)objType.newInstance();
                objOutboundIntg.executeIntegration(asyncQueueRecord);
            }
            else {

                // implement non callout framework
            }
        }
        
        catch(Exception ex) {

            Asynchronous_Queue__c asyncQueueRecord_toUpdate = new Asynchronous_Queue__c(id = asyncQueueRecord.id,
                                                                        Error_Type__c = 'System Exception : '+ ex.getTypeName(),
                                                                        Error_Message__c = ex.getMessage(),
                                                                        Request_Status__c = Label.Request_Type_Completed_with_Errors
                                                                        );

            update asyncQueueRecord_toUpdate;

            LogFramework.getLogger().createExceptionLogs(ex);
        }

        finally {

            LogFramework.getLogger().commitLogs();
        }

    }

    public class AsyncJobExecutionException extends Exception { }
    
}