public with sharing class AcctNoteAttachment {
	// old stuff
    public String ObjectID {get; set;}
    public String ObjectURL {get; set;}
    public String ObjectName {get; set;}
    public Integer ObjectCount {get; set;}
    public Boolean DelItemType {get; set;}
    public String DelItemID {get; set;}
    public Boolean rend{get;set;}
    public Boolean Disable {get; set;}
    public String ObjColor {get; set;}
	private List<NoteAndAttachMent> Items;
    private final ApexPages.StandardController c;
    	
    //offset paging studd 
    public Integer CountTotalRecords{get;set;} 
    public String QueryString {get;set;} 
    public Integer OffsetSize = 0; 
    private Integer QueryLimit = 5; 
    public Boolean returnItems {get; set;}
    public Boolean returnVA {get; set;}



    
    public AcctNoteAttachment(ApexPages.StandardController c){
    	system.debug(UserInfo.getUserId());
        this.c = c;
        ObjectID = c.getId();
        string objtype = c.getRecord().getSObjectType().getDescribe().getName();

        Account count = [Select o.Name, o.OwnerID, o.Id, (Select Id  From NotesAndAttachments ) From Account o where o.id=:c.getid() limit 1];
        CountTotalRecords = count.NotesAndAttachments.size();
                
        ObjectName = count.name;
        ObjectURL = '/apex/UpLoadAttAcct';
        //coding for sharing/team member access
        //rend = true;
        Disable = True;
        ObjColor = 'gray';
        returnItems = False;
        returnVA = false;
        string AcctAccess;
        String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;

       	//String AcctAccess = [Select AccountAccessLevel From AccountShare where  AccountId = :ObjectID and UserOrGroupId = :Userinfo.getUserId()].AccountAccessLevel;
       	List <AccountShare> Access = [Select AccountAccessLevel From AccountShare where  AccountId = :ObjectID and UserOrGroupId = :Userinfo.getUserId() limit 1];
       	if (Access.size() > 0){
       		 AcctAccess = Access[0].AccountAccessLevel;
       	}
       	system.debug(AcctAccess);
       	Set<id> UpUsers =  RoleUtils.getRoleManagerUsers(count.OwnerID);
       	
       	if (AcctAccess == 'All' || AcctAccess == 'Edit' || usrProfileName == 'System Administrator' || UpUsers.Contains(Userinfo.getUserId()) == True){
       		Disable = False;
       		ObjColor = 'black';
       		returnItems = true; 
       		returnVA = True;           			
       	} 
        
    }
   
       
    
    public List<NoteAndAttachMent> getItems(){ 
    	
             if (returnItems == false) { 
             	return null; 
             } else {
				Account items = [Select o.Name, o.Id, (Select Id, IsNote, Title, ParentId, LastModifiedDate, Createdbyid  From NotesAndAttachments order by LastModifiedDate desc limit :QueryLimit offset :OffsetSize ) From Account o where o.id=:c.getid() limit 1];
				if (items.NotesAndAttachments.size() < 1) {
					returnItems = false;
					returnVA = false;						
					return null; 
				}             	
            	return items.NotesAndAttachments;
           	} 
    	
    }
    
    public Boolean getDisablePrevious(){ 
        if(OffsetSize>0){ 
            return false; 
        } 
        else return true; 
    } 
  
    public Boolean getDisableNext() { 
    	system.debug(countTotalRecords);
        if (OffsetSize + QueryLimit < countTotalRecords){ 
            return false; 
        } 
        else return true; 
    } 
  
    public PageReference Next() { 
        OffsetSize += QueryLimit; 
        return null; 
    } 
  
    public PageReference Previous() { 
        OffsetSize -= QueryLimit; 
        return null; 
    } 
    
    public PageReference DeleteNote()
    {     
        If (DelItemType){     
            Note del = new Note(ID=DelItemId);
            delete del;
        } else {
            Attachment del = new Attachment(ID=DelItemId);
            delete del;         
        }
        return null;
    }
       
    

}