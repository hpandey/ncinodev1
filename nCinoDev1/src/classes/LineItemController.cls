global class LineItemController {   
    Public OpportunityLineItem oppLineItem {get;set;}
    Public string errorMsg{get;set;}
    Public string exMsg{get;set;}
    Public Boolean isException{get;set;}
    String retUrl;
    Id lineItemId;
        
    public LineItemController(ApexPages.StandardController controller) {
        lineItemId = controller.getId();
        isException=false;
        exMsg='';
    }
    
    //Method to display dynamic content on screen
    public Component.Apex.PageBlockSection getSection(){
            Map<string, Schema.SObjectField> FieldMap;
            FieldMap = Schema.SObjectType.OpportunityLineItem.fields.getMap();
            Set<string> FieldSet = FieldMap.keySet();
            List<string> FieldList = new List<string>();
            FieldList.addAll(FieldSet);
            FieldList.sort();
            boolean addHelp = true;
            // update to add any new fields to be shown
            String qryOppLineItem = new selectall('OpportunityLineItem').soql + ' where Id= :lineItemId LIMIT 1';
            oppLineItem = Database.query(qryOppLineItem);
            OpportunityLineItem itemFamily = [select PriceBookEntry.product2.family from OpportunityLineItem where Id=:lineItemId LIMIT 1];
            
            retUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + oppLineItem.OpportunityId;
            
            List<Product_Layout__c> productLayoutList = 
                    [select Id, Name, Field__c, Sequence__c, Display__c, Custom_Label_Text__c, URL__C  
                    from Product_Layout__c where Name= :itemFamily.PriceBookEntry.Product2.Family and Sequence__c>0
                    order by Sequence__c ASC];
            
            Component.Apex.PageBlock pb= new Component.Apex.PageBlock();
            pb.id='pb2';
             
            
            
            //pb.childComponents.add(pbs);
            
            Component.Apex.outputPanel div = new Component.Apex.outputPanel();
            div.styleClass='ui-widget-content';
            
            Component.Apex.outputPanel span = new Component.Apex.outputPanel();
            span.styleClass='ui-icon ui-icon-cart';
            span.style='display:inline-block;';
            
            div.childComponents.add(span);
            
            Component.Apex.PageBlockSection pbs = new Component.Apex.PageBlockSection(columns = 2);
            pbs.id='pbs1';
            pbs.title=oppLineItem.Product_Name__c;
            pbs.collapsible=false;
            
            for(Product_Layout__c productLayout : productLayoutList ){
            	  addHelp = True;            	
                  Component.Apex.PageBlockSectionItem pbsi = new Component.Apex.PageBlockSectionItem();
                  Schema.DescribeFieldResult field = (FieldMap.get(productLayout.Field__c)).getDescribe();
                  
                  if (field.isAccessible() && productLayout.Display__c=='Output'){                     
                      Component.Apex.OutputText lblText = new Component.Apex.OutputText();
                      if (productLayout.Custom_Label_Text__c==NULL){
                          lblText.value = field.getLabel();
                      }else{
                          lblText.value = productLayout.Custom_Label_Text__c;
                      }
                  
                      Component.Apex.OutputLabel lblValue = new Component.Apex.OutputLabel();
                      lblValue.style = 'font-weight:Bold;color:black;';
                      if (field.getType().name()=='Currency') {
                        List<String> args = new String[]{'0','number','###,###,##0.00'};
                        Decimal d = (Decimal) oppLineItem.get(productLayout.Field__c);
                        if (d==null)
                            lblValue.value = oppLineItem.get(productLayout.Field__c);
                        else
                            lblValue.value =  '$' + String.format(d.format(), args);
                      }
                      else {
                        lblValue.value = oppLineItem.get(productLayout.Field__c);
                      }
                      pbsi.childComponents.add(lblText);
                      pbsi.childComponents.add(lblValue);                       
                  }
                  else if (field.isAccessible() && field.isUpdateable() && productLayout.Display__c=='Input'){
                      Component.Apex.InputField input = new Component.Apex.InputField();  
                      input.expressions.value = '{!oppLineItem.'+productLayout.Field__c +'}';  
                      input.id = field.getName();  
                      
                      Component.Apex.OutputLabel inputLabel = new Component.Apex.OutputLabel();  
                      if (productLayout.Custom_Label_Text__c==NULL){
                          inputLabel.value = field.getLabel();
                      }else{
                          inputLabel.value = productLayout.Custom_Label_Text__c;
                      }
                      inputLabel.for = field.getName();  
                      
                      pbsi.childComponents.add(inputLabel);
                      pbsi.childComponents.add(input);                       
                  }  
                  else if(field.isAccessible() && productLayout.Display__c=='Link'){
                  	  addHelp = false;
                  	  Component.Apex.OutputLink Link = new Component.Apex.OutputLink();
                      Link.value = 'javascript:void(0)';
                      system.debug('url name ** '+productLayout.URL__c);
                      Link.onclick = 'window.open("'+productLayout.URL__c+'","","resizable=yes","width=500,height=500")';
                      
                      Link.title = 'Click Here';
                      
                      Component.Apex.outputText txt = new Component.Apex.outputText();
        			  txt.value = 'Click Here';
        			  link.childComponents.add(txt);

                      Component.Apex.OutputLabel LinkLabel = new Component.Apex.OutputLabel();  
                      if (productLayout.Custom_Label_Text__c==NULL){
                          LinkLabel.value = field.getLabel();
                      }else{
                          LinkLabel.value = productLayout.Custom_Label_Text__c;
                      } 
                      LinkLabel.for = field.getName();
                  
                      pbsi.childComponents.add(LinkLabel);
                      pbsi.childComponents.add(Link);                         	
                  } 
              if(addHelp == True){       
              	pbsi.helpText = field.getInlineHelpText();
              }	            
              pbs.childComponents.add(pbsi);
              div.childComponents.add(pbs);
                } 
                pb.childComponents.add(div);            
            return pbs;
    }
        
    //Method to save line item and return to parent screen
    public Pagereference saveProducts(){        
        exMsg='';
        List<Database.upsertResult> results = Database.upsert(new List <OpportunityLineItem> {oppLineItem}, false);
                    
        for (Database.upsertResult result : results){
            if (result.isSuccess()==false){
                 isException=true;
                 for (Database.error errorResult : result.getErrors()){                              
                     exMsg= exMsg + errorResult.getMessage() + '<br/>';
                 }
                 return null;
            }
            else 
              return new Pagereference(retUrl);
        }
        
        /* try {
            upsert oppLineItem;
            return new Pagereference(retUrl);   
        } catch(DMLException e){
            System.debug('DML upsert Error: ' + e);
            for (Integer i = 0; i < e.getNumDml(); i++) {
                errorMsg = e.getDmlMessage(i)+'<br/>';
                System.debug(e.getDmlMessage(i)); 
            }
            return null;
        }*/
        return null;
    }
    
    //Method to return to parent screen
    public Pagereference cancel(){  
        return new Pagereference(retUrl);
    }
    
    //Method to save and refresh the current edit screen
    public void quickSave(){  
       exMsg='';
        List<Database.upsertResult> results = Database.upsert(new List <OpportunityLineItem> {oppLineItem}, false);
                    
        for (Database.upsertResult result : results){
            if (result.isSuccess()==false){
                 isException=true;
                 for (Database.error errorResult : result.getErrors()){                              
                     exMsg= exMsg + errorResult.getMessage() + '<br/>';
                 }
            }
        }
       
       /* try {
            upsert oppLineItem;  
        } catch(DMLException e){
            System.debug('DML upsert Error: ' + e);
            for (Integer i = 0; i < e.getNumDml(); i++) {
                errorMsg = e.getDmlMessage(i)+'<br/>';
                System.debug(e.getDmlMessage(i)); 
            }
        } */
    }
    
         
}