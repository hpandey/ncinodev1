// 
// (c) 2014 Appirio, Inc.
//
// Call Plan Controller that handles adding, notifying, and removing participants
//
// 19 Nov 2014    Matthew Shaver       Original
// 9  Dec 2014    Matthew Shaver       First version completed and sent to TEST
//
global class CallPlanController {

    public String saveText {
        get {
            if (String.isBlank(ApexPages.currentPage().getParameters().get('cid'))) {
                return 'Save and Add Participants';
            }
            return 'Save';
        }
    }
    
    // Select Options of the User Roles picklist values under the User object
    public List<SelectOption> userRoles {
        get {
            return buildSelectOptions(User.Team_Role__c.getDescribe());
        }
    }
    // Select Options of the Roles picklist values under the Contact object
    public List<SelectOption> contactRoles {
        get {
            return buildSelectOptions(Contact.Role__c.getDescribe());
        }
    }
    // Select Options of the Call Plan Types picklist values under the Call Plan object
    public List<SelectOption> callPlanTypeOptions {
        get {
            return buildSelectOptions(Call_Plan__c.Call_Plan_Type__c.getDescribe());
        }
    }
    
    // Select Options of the Meeting Category picklist values under the Call Plan object
    public List<SelectOption> callPlanCategoryOptions {
        get {
            return buildSelectOptions(Call_Plan__c.Meeting_Category__c.getDescribe());
        }
    }
    
    // Select Options of the Status picklist values under the Call Plan object
    public List<SelectOption> callPlanStatusOptions {
        get {
            return buildSelectOptions(Call_Plan__c.Status__c.getDescribe());
        }
    }
    
    // Select Options of the Primary Risk picklist values under the Call Plan object
    public List<SelectOption> callPlanPrimaryRiskOptions {
        get {
            return buildSelectOptions(Call_Plan__c.Primary_Emerging_Risk__c.getDescribe());
        }
    }
    
    // Select Options of the Secondary Risk picklist values under the Call Plan object
    public List<SelectOption> callPlanSecondaryRiskOptions {
        get {
            return buildSelectOptions(Call_Plan__c.Secondary_E__c.getDescribe());
        }
    }
    
    // Select Options of the Joint Call picklist values under the Call Plan object
    public List<SelectOption> callPlanJointCallOptions {
        get {
            return buildSelectOptions(Call_Plan__c.Joint_Call2__c.getDescribe());
        }
    }
    
    // Select Options of the Emerging Risk Identified picklist values under the Call Plan object
    public List<SelectOption> callPlanEmergingRiskIdentifiedOptions {
        get {
            return buildSelectOptions(Call_Plan__c.Changes_Business_or_Financial_Profile__c.getDescribe());
        }
    }
    
    
    // List of all accounts (limted to 10,000 due to set pagination limit)
    //public PaginatedUserList UserItems {
    //    get {
    //        return new PaginatedUserList([SELECT id, Name, Email FROM User WHERE IsActive = true ORDER BY Name ASC]);
    //    }
    //}
    
    // The record type that is used for task creation
    public String taskRecordType { 
        get {
            return [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard' LIMIT 1].Id;
        }
    }
    
    // Determine if the current user is an admin
    public Boolean isAdmin { 
        get {
            return [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name == 'System Administrator';
        }
    }
    
    // The URL to the current salesforce environment for links in the UI
    public String SFUrl {
        get {
            return URL.getSalesforceBaseUrl().toExternalForm();
        }
    }
    
    public String PrevUrl {
        get {
            return ApexPages.currentPage().getParameters().get('retURL');
        }
    }
    
    // Accessible properties through the UI
    public String mainCallPlanId { get; set; }
    public Call_Plan__c mainCallPlan { get; set; }
    public String callPlanName { get; set; }
    public String callPlanOwner { get; set; }
    public String callPlanOwnerId { get; set; }
    public String callPlanEmergingRisk { get; set; }
    public String callPlanCompletedDate { get; set; }
    public String callPlanTargetDate { get; set; }
    
    public String accountId { get; set; }
    public String accountName { get; set; }
    
    public String mainContactId { get; set; }
    public String contactName { get; set; }
    public String contactRole { get; set; }
    
    // Determines if the view should show editable controls initially but disabled Participants and Activities
    public Boolean newCallPlan { get; set; }
    // Determines if the view should show editable controls, fill in valuse, and activate Participants and Activities
    public Boolean editCallPlan { get; set; }
    // Boolean set if the Status__c column is set to "Completed" for the loaded Call Plan
    public Boolean completedCallPlan { get; set; }
    
    public Boolean validUser { get; set; }
    
    // List of users that are going to be notified of an emerging risk
    public List<UserUI> additionalNotifyUsers { get; set; }
    // List of activities that are linked to the loaded call plan
    public List<Task> taskItems { get; set; }
    // URL link back the client or contact depending on how the user arrived to the Call Plan page
    public String referrerUrl { get; set; }
    
    // Controller is split into two types to allow for test classes
    // This controller is called from the visual force page REG_Call_Plan
    public CallPlanController() {
        this(ApexPages.currentPage().getParameters().get('cid'), 
            ApexPages.currentPage().getParameters().get('clientid'),
            ApexPages.currentPage().getParameters().get('contactid'),
            ApexPages.currentPage().getParameters().get('mode'));
    }
    
    public CallPlanController(String callPlanId, String clientId, String contactId, String mode) {
        
        // If this this is a new call plan
        if (String.isBlank(callPlanId)) {
            callPlanOwner = UserInfo.getName();
            newCallPlan = true;
            Boolean checkUser = false;
            // Came through the Client Object
            if (!String.isBlank(clientId)) {
                Account act = [SELECT Id,Name FROM Account WHERE Id = :clientId];
                accountId = act.Id;
                accountName = act.Name;
                referrerUrl = accountId;
                checkUser = true;
            } // Came through the Contact Object 
            else if (!String.isBlank(contactId)) {
                Contact con = [Select Id,Name,AccountId,Account.Name,Role__c FROM Contact WHERE Id = :contactId];
                accountId = con.AccountId;
                accountName = con.Account.Name;
                mainContactId = con.Id;
                contactName = con.Name;
                contactRole = con.role__c;
                referrerUrl = mainContactId;
                checkUser = true;
            }
            
            if (checkUser) {
                validUser = validUser();
            } else {
                validUser = true;
            }
        } else {
            List<Call_Plan__c> callPlans = [SELECT Call_Plan_Type__c,Changes_Business_or_Financial_Profile__c,Client__c,Client__r.Name,Contact__c,Contact__r.Name,
                                            Joint_Call2__c,Name,Owner_Full_Name__c,Status__c,Target_Date__c, Meeting_Category__c, Completed_Date__c,
                                            Objectives__c, Primary_Emerging_Risk__c, Secondary_E__c, Describe_Emerging_Risk__c, Risk__c,
                                            Other_Risk_Factors_Discussed__c, Call_Result__c, Next_Steps__c, Treasury_Management_Officer__c,
                                            Treasury_Management_Officer_Area__c, Treasury_Management_Officer_Region__c, Treasury_Management_Sales_Consultant__c,
                                            Commercial_Card_Sales_Consultant__c, OwnerId, Participants__c
                                            FROM Call_Plan__c WHERE Id = :callPlanId LIMIT 1];
            if (callPlans.size() > 0) {
                mainCallPlan = callPlans[0];
                mainCallPlanId = callPlanId;
                accountId = mainCallPlan.Client__c;
                accountName = mainCallPlan.Client__r.Name;
                mainContactId = mainCallPlan.Contact__c;
                contactName = mainCallPlan.Contact__r.Name;                
                callPlanOwner = mainCallPlan.Owner_Full_Name__c;
                callPlanOwnerId = mainCallPlan.OwnerId;
                callPlanName = mainCallPlan.Name;
                completedCallPlan = mainCallPlan.Status__c == 'Completed';
                callPlanTargetDate = (mainCallPlan.Target_Date__c == null?'':mainCallPlan.Target_Date__c.format());
                callPlanCompletedDate = (mainCallPlan.Completed_Date__c == null?'':mainCallPlan.Completed_Date__c.format());
                
                // Default value of emerging risk is "No" if nothing is selected
                callPlanEmergingRisk = (String.isBlank(mainCallPlan.Changes_Business_or_Financial_Profile__c)?'No':mainCallPlan.Changes_Business_or_Financial_Profile__c);
                
                // Users that are selected to be notified of an emerging risk
                additionalNotifyUsers = new List<UserUI>();
                for (Risk_Notification_Recipient__c notify : [SELECT Id,Recipient__r.Name,Recipient__r.Email,Recipient__c,Email_Sent__c FROM Risk_Notification_Recipient__c WHERE Call_Plan__c = :callPlanId]) {
                    UserUI user = new UserUI();
                    user.id = notify.Id;
                    user.name = notify.Recipient__r.Name;
                    user.email = notify.Recipient__r.Email;
                    user.userid = notify.Recipient__c;
                    user.emailSent = notify.Email_Sent__c;
                    additionalNotifyUsers.add(user);
                }
                
                // All task items related to the Call Plan
                taskItems = [SELECT Status, Subject, WhatId, What.Name, WhoId, Who.Name, ActivityDate, Priority, Id FROM Task WHERE WhatId = :callPlanId ORDER BY ActivityDate DESC NULLS LAST];
                
                // Hack maybe? All functionality built to handle showing editable content on "newCallPlan"
                // The editCallPlan just overrides disabling the Participants and Activites section
                if (mode == 'edit') {
                    editCallPlan = true;
                    newCallPlan = true;
                }
                validUser = validUser();
            }
        }
    }
    private Boolean validUser() {
        String usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;
        if (usrProfileName == 'Regions Migration' || usrProfileName=='System Administrator') {
            return true;
        } else {
            String clientRecType = [Select recordtype.name from account where id = :accountId].recordtype.name;
            If (clientRecType == 'COI' || clientRecType == 'Internal COI') {
                return true;
            } else {
                List<AccountShare> acs = [Select Id from AccountShare 
                where RowCause in ('Team','Owner') 
                and AccountId = :accountId
                and UserorGroupId = :Userinfo.getUserId()];
                
                List<AccountTeamMember> managers = [SELECT UserId, User.ManagerId FROM AccountTeamMember 
                                            WHERE AccountId = :accountId AND User.ManagerId = :UserInfo.getUserId()];
                
                List<Account> ownerManager = [SELECT Owner.ManagerId FROM Account WHERE Id = :accountId AND Owner.ManagerId = :UserInfo.getUserId()];
                
                if (acs.size() == 0 && managers.size() == 0 && ownerManager.size() == 0) {
                    newCallPlan = false;
                    editCallPlan = false;
                    return false;
                }
            }
        }
        return true;
    }
    //  Return all current team members for the loaded Call Plan
    //  @param               None
    //  @return              List of team members that is consumed by the UI
    public List<TeamMemberUI> getClientTeamMembers() {
        return getClientTeamMembers(mainCallPlanId, accountId);
    }
    
    //  Return all current contacts for the loaded Call Plan
    //  @param               None
    //  @return              List of contacts that is consumed by the UI
    public List<ContactUI> getClientContacts() {
        return getClientContacts(mainCallPlanId, accountId);
    }
    
    //  Return all participants for the loaded Call Plan
    //  @param               None
    //  @return              List of participants that is consumed by the UI
    public List<ParticipantUI> getParticipants() {
        // The type of the participant is dependent on which value is filled in on the Participant__c object
        // If Associate__c is filled in then the participant is not a OneView user or Contact
        // If Contact__c is filled in then the participant is a Contact
        // If Name__c is filled in then the participant is a OneView User
        if (String.IsBlank(mainCallPlanId)) {
            return new List<ParticipantUI>();
        }
        List<Participant__c> participants = [SELECT Id, Associate__c,Contact__r.Name,Name__r.Name,Name__r.Email,Role__c,Send_Notification__c
                                            FROM Participant__c WHERE Call_Plan__c = :mainCallPlanId];
        List<ParticipantUI> response = new List<ParticipantUI>();
        for (Participant__c part : participants) {
            String name = part.Associate__c;
            String userid = '';
            String email = '';
            List<SelectOption> roles = null;
            
            // Repeated if checks allow for overriding the value
            if (String.IsBlank(name)) {
                name = part.Contact__r.Name;
                userid = part.Contact__c;
                roles = contactRoles;
            }
            if (String.IsBlank(name)) {
                name = part.Name__r.Name;
                // Since only OneView users can be added to the emerging risk notifications
                // an email must be collected
                email = (String.IsBlank(part.Name__r.Email)?'':part.Name__r.Email);
                userid = part.Name__c;
                roles = userRoles;
            }
            if (roles == null) {
                roles = userRoles;
            }
            ParticipantUI partUI = new ParticipantUI();
            partUI.id = part.Id;
            partUI.name = name;
            partUI.email = email;
            partUI.role = part.Role__c;
            partUI.userid = userid;
            partUI.roleOptions = roles;
            partUI.notify = part.Send_Notification__c;
            
            response.add(partUI);
        }
        return response;
    }
    
    //  Builds a list of select options based on a given field's picklist values
    //  @param               None
    //  @return              List of select options
    private List<SelectOption> buildSelectOptions(Schema.DescribeFieldResult fieldDesc) {
        List<SelectOption> statusOptions = new List<SelectOption>();
        statusOptions.add(new SelectOption('', ''));
        for (Schema.Picklistentry picklistEntry : fieldDesc.getPicklistValues())
        {
            statusOptions.add(new SelectOption(pickListEntry.getValue(), pickListEntry.getLabel()));
        }
        return statusOptions;
    }
    
    //  Retrieves all client team members associated with the client
    //  @param callPlanId    The Call Plan Id
    //  @param clientId      The Client(Account) Id
    //  @return              List of team members
    @RemoteAction
    global static List<TeamMemberUI> getClientTeamMembers(String callPlanId, String clientId) {
        Map<String, TeamMemberUI> memberMap = new Map<String, TeamMemberUI>();
        
        // The call plan is needed in order to automatically add the Owner to the Team Member list
        List<Account> accounts = [SELECT OwnerId, Owner.Name, Owner.Team_Role__c,Owner.Email FROM Account WHERE Id = :clientId];
        
        TeamMemberUI owner = new TeamMemberUI();
        if (accounts.size() > 0) {
            Account acc = accounts[0];
            owner.userid = acc.ownerid;
            owner.name = acc.Owner.Name;
            owner.role = acc.Owner.Team_Role__c;
            owner.email = acc.Owner.Email;
            
            memberMap.put(owner.userid, owner);
        }
        
        /* for (AccountTeamMember member : [SELECT id, UserId, User.Name, User.Email, User.Team_Role__c FROM AccountTeamMember WHERE AccountId = :clientId]) {
            if (member.UserId != owner.userid) {
                TeamMemberUI mem = new TeamMemberUI();
                mem.userid = member.UserId;
                mem.name = member.User.Name;
                mem.email = member.User.Email;
                mem.role = member.User.Team_Role__c;
                memberMap.put(mem.userid, mem);
            }
        } */
        // Changed code to remove soql query from the for loop. CJW 2/8/15
        AccountTeamMember[] ClientMember = [SELECT id, UserId, User.Name, User.Email, User.Team_Role__c FROM AccountTeamMember WHERE AccountId = :clientId];
        for (AccountTeamMember member: ClientMember) {
            if (member.UserId != owner.userid) {
                TeamMemberUI mem = new TeamMemberUI();
                mem.userid = member.UserId;
                mem.name = member.User.Name;
                mem.email = member.User.Email;
                mem.role = member.User.Team_Role__c;
                memberMap.put(mem.userid, mem);
            }
        }

        
        
        // If the member is already a participant, check the Participant checkbox
        
        system.debug(callPlanID);
        //added if not null for new callplans
        if (CallPlanId != null) {
            Participant__c[] existingParts = [SELECT Id, Name__c FROM Participant__c WHERE Call_Plan__c = :callPlanId];
            for (Participant__c part : existingParts) {
                TeamMemberUI mem = memberMap.get(part.Name__c);
                if (mem != null) {
                    mem.partcheck = true;
                }
            }
        
        
            // Similar if they are already in the risk notification list
            Risk_Notification_Recipient__c[] notifications = [SELECT Id,Recipient__c FROM Risk_Notification_Recipient__c WHERE Call_Plan__c = :callPlanId];
            for (Risk_Notification_Recipient__c risk : notifications) {
                TeamMemberUI mem = memberMap.get(risk.Recipient__c);
                if (mem != null) {
                    mem.riskcheck = true;
                }
            }
        }
        
        List<TeamMemberUI> members = memberMap.values();
        members.sort();
        return members;
    }
    
    //  Retrieves all client contacts associated with the client
    //  @param callPlanId    The Call Plan Id
    //  @param clientId      The Client(Account) Id
    //  @return              List of contacts
    @RemoteAction
    global static List<ContactUI> getClientContacts(String callPlanId, String clientId) {
        List<Contact> contacts = [SELECT id, name, Role__c FROM Contact WHERE AccountId = :clientId];
        List<Call_Plan__c> cps = [SELECT id, Contact__C FROM Call_Plan__c WHERE Id = :callPlanId LIMIT 1];
        Call_Plan__c cp = null;
        if (cps.size() > 0) {
            cp = cps[0];
        }
        
        List<ContactUI> response = new List<ContactUI>();
        for (Contact con : contacts) {
            ContactUI conUI = new ContactUI();
            conUI.id = con.id;
            conUI.name = con.name;
            conUI.role = con.Role__c;
            
            // If already a participant
            if ([SELECT COUNT() FROM Participant__c WHERE Contact__c = :con.id AND Call_Plan__c = :callPlanId] > 0) {
                conUI.partcheck = true;
            } else if (cp != null && cp.Contact__c == con.id) {
                conUI.partcheck = true;
            }
            response.add(conUI);
        }
        
        return response;
    }
    
    
    //  Sends the email notifications to all Risk Notification members
    //  @param callPlanId    The Call Plan Id
    //  @return              A boolean representing if the notification was successful.
    @RemoteAction
    global static Boolean sendNotifications(String callPlanId) {
        List<Call_Plan__c> cps = [SELECT Id, Client__c, Client__r.Name FROM Call_Plan__c WHERE Id = :callPlanId];
        if (cps.size() > 0) {
            REB_EmergingRiskNotifications ern = new REB_EmergingRiskNotifications ();
            try {
                ern.SendERnotifications(cps[0]);
                return true;
            } catch(Exception ex) {
                // Add exception logging
            }
        }
        return false;
    }
    
    //  Removes a specific participant from a call plan
    //  @param id            The participant id
    //  @param userid        The user id (if they are a OneView user)
    //  @return              A boolean representing if the deletion was successful
    @RemoteAction
    global static Boolean removeParticipant(String id, String userId) {
        Participant__c[] participants = [SELECT Id, Call_Plan__c FROM Participant__c WHERE Id = :id];
        if (participants.size() > 0) {
            Participant__c part = participants[0];
            Risk_Notification_Recipient__c[] notifications = [SELECT Id FROM Risk_Notification_Recipient__c WHERE Recipient__c = :userId AND Call_Plan__c = :part.Call_Plan__c];
            try {
                delete participants;
                delete notifications;
                return true;
            } catch (Exception ex) {
                // Add exception logging
            }
        }
        return false;
    }
    
    //  Removes a specific notification user
    //  @param id            The notification id
    //  @return A boolean representing if the deletion was successful
    @RemoteAction
    global static Boolean removeNotify(String id) {
        Risk_Notification_Recipient__c[] notifications = [SELECT Id FROM Risk_Notification_Recipient__c WHERE Id= :id];
        try {
            delete notifications;
            return true;
        } catch (Exception ex) {
                // Add exception logging
        }
        return false;
    }
    
    //  Adds a set of participants
    //  @param List<ParticipantUI> List of participants
    //  @return                    An updated list of participants with ids
    @RemoteAction
    global static List<ParticipantUI> addParticipants(List<ParticipantUI> parts) {
        for (ParticipantUI partUI : parts) {
            
            Participant__c part = new Participant__c();
            String recordTypeName = 'Non_OneView_Participant';
            if (partUI.type == 'Contact') {
                recordTypeName = 'Contact_Participant';
                part.Contact__c = partUI.userid;
            } else if (partUI.type == 'OneView') {
                recordTypeName = 'OneView_Participant';
                part.Name__c = partUI.userid;
                partUI.email = [SELECT Email FROM User WHERE id = :partUI.userid][0].Email;
            } else {
                recordTypeName = 'Non_OneView_Participant';
                part.Associate__c = partUI.name;
            }
            
            RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = :recordTypeName LIMIT 1];
            part.RecordTypeId = rt.Id;
            part.Business_Segment_Introduced__c = Boolean.valueOf(partUI.businessSegment);
            part.Call_Plan__c = partUI.callPlanId;
            part.Role__c = partUI.role;
            
            // Inserting in loop so that duplicates are skipped
            // We do not want to update existing values so upsert is invalid
            try {
                insert part;
                partUI.id = part.id;
            } catch(Exception ex) {
                // Add exception logging
            }
        }
                
        return parts;
    }
    
    //  Adds a notifications for a list of participants
    //  @param List<ParticipantUI> List of participants
    //  @return                    An updated list of participants with ids
    @RemoteAction
    global static List<ParticipantUI> addNotify(List<ParticipantUI> parts) {
        for (ParticipantUI partUI : parts) {
            if (Boolean.valueOf(partUI.notify)) {
            
                // Would have preferred to do an upsert on the Risk_Notification_Recipient
                // however this would have to be a compound external id key match
                if ([SELECT COUNT() FROM Risk_Notification_Recipient__c WHERE Call_Plan__c = :partUI.callPlanId AND Recipient__c = :partUI.userid] == 0) {
                    Risk_Notification_Recipient__c notify = new Risk_Notification_Recipient__c();
                    notify.Recipient__c = partUI.userid;
                    notify.Call_Plan__c = partUI.callPlanId;
                    
                    try {
                        insert notify;
                        partUI.id = notify.id;
                    } catch(Exception ex) { 
                        // Add exception logging
                    }
                }
            }
        }
        return parts;
    }
    
    //  Update the role values for a set of participants
    //  @param List<ParticipantUI> List of participants
    //  @return                    A boolean representing if the update was successful
    @RemoteAction
    global static boolean updateRoles(List<ParticipantUI> parts) {
        List <Participant__c> participants = new List<Participant__c>();
        for (ParticipantUI partUI : parts) {
            Participant__c part = new Participant__c();
            part.Id = partUI.id;
            part.Role__c = partUI.role;
            participants.add(part);
        }
        upsert participants;
        return true;
    }
    
    //  Create or save a call plan based on all of the values passed in
    //  @param CallPlanUI    Call plan from the UI that has all the updated values
    //  @return              Call Plan object with the updated ID and Name
    @RemoteAction
    global static Call_Plan__c savePlan(CallPlanUI cp) {
        Call_Plan__c callPlan = new Call_Plan__c();
        if (!String.IsBlank(cp.Id)) {
            callPlan.Id = cp.Id;
        }
        callPlan.Call_Plan_Type__c = cp.Call_Plan_Type;
        callPlan.Changes_Business_or_Financial_Profile__c = cp.Emerging_Risk;
        callPlan.Client__c = cp.Client;
        callPlan.Contact__c = cp.Contact;
        callPlan.Joint_Call2__c = cp.Joint_Call2;
        callPlan.Status__c = cp.Status;
        callPlan.Target_Date__c = (String.IsBlank(cp.Target_Date)?null:Date.parse(cp.Target_Date));
        callPlan.Meeting_Category__c = cp.Meeting_Category;
        callPlan.Completed_Date__c = (String.IsBlank(cp.Completed_Date)?null:Date.parse(cp.Completed_Date));
        callPlan.Objectives__c = cp.Objectives;
        callPlan.Primary_Emerging_Risk__c = cp.Primary_Emerging_Risk;
        callPlan.Secondary_E__c = cp.Secondary_E;
        callPlan.Describe_Emerging_Risk__c = cp.Describe_Emerging_Risk;
        callPlan.Risk__c = cp.Risk;
        callPlan.Other_Risk_Factors_Discussed__c = cp.Other_Risk_Factors_Discussed;
        callPlan.Call_Result__c = cp.Call_Result;
        callPlan.Next_Steps__c = cp.Next_Steps;
        Database.UpsertResult updateResult = Database.upsert(callPlan, true);
        
        // Can't return updateResult since the computed name is needed
        return [SELECT Id,Name FROM Call_Plan__c WHERE Id = :updateResult.Id];
    }
    
    
    //  Save users that need to be notified of an emerging risk
    //  @param String        Call Plan Id
    //  @param List<UserUI>  List of users to be notified
    //  @return              List of Upsert result that has the updated ids
    @RemoteAction
    global static List<Database.UpsertResult> saveNotify(String callPlanId, List<UserUI> users) {
        List<Risk_Notification_Recipient__c> notifications = new List<Risk_Notification_Recipient__c>();
        for (UserUI user : users) {
            Risk_Notification_Recipient__c notify = new Risk_Notification_Recipient__c();
            if (!String.IsBlank(user.id)) {
                notify.id = user.id;
            }
            notify.Call_Plan__c = callPlanId;
            notify.Recipient__c = user.userid;
            notifications.add(notify);
        }
        return Database.upsert(notifications, false);
    }
    
    @RemoteAction
    global static List<Account> searchClients(String search) {
        String searchString = '%' + search + '%';
        return [SELECT id, name FROM Account WHERE name LIKE :searchString LIMIT 10];
    }

    @RemoteAction
    global static List<Contact> searchContacts(String search) {
        String searchString = '%' + search + '%';
        return [SELECT id, name, role__c FROM Contact WHERE name LIKE :searchString LIMIT 10];
    }

    @RemoteAction
    global static List<User> searchUsers(String search) {
        String searchString = '%' + search + '%';
        return [SELECT id, name, email FROM User WHERE name LIKE :searchString LIMIT 10];
    }
        
    @RemoteAction
    global static List<Contact> getContacts(String accountId) {
        return [SELECT Id, Name, Role__c FROM Contact WHERE AccountId = :accountId LIMIT 1000];
    }
    
    // SF Object: User
    // Used for JSON transfer from the Controller to the UI
    global class UserUI {
        public String name { get; set; }
        public String email { get; set; }
        public Boolean emailSent { get; set; }
        public String userid { get; set; }
        public String id { get; set ; }
    }
    
    // SF Object: AccountTeamMember
    // Used for JSON transfer from the Controller to the UI
    global class TeamMemberUI implements Comparable {
        public String userid { get; set; }
        public String name { get; set; }
        public String role { get; set; }
        public String email { get; set; }
        public Boolean partcheck { get; set; }
        public Boolean riskcheck { get; set; }

        global Integer compareTo(Object compareTo) {
            TeamMemberUI compareToTeamMember = (TeamMemberUI)compareTo;
                        
            if(!String.isBlank(name))
                return name.compareTo(compareToTeamMember.name);
            else
                return 1;       
        }
    }
    
    
    // SF Object: Contact
    // Used for JSON transfer from the Controller to the UI
    global class ContactUI {
        public String name { get; set; }
        public String id { get; set; }
        public Boolean partcheck { get; set; }
        public String role { get; set; }
    }
    
    // SF Object: Participant__c
    // Used for JSON transfer from the Controller to the UI
    global class ParticipantUI {
        public String id { get; set; }
        public String name { get; set; }
        public String email { get; set; }
        public String role { get; set; }
        public List<SelectOption> roleOptions { get; set; }
        public String type { get; set; }
        public String userid { get; set; }
        public boolean notify { get; set; }
        public boolean businessSegment { get; set; }
        public String callPlanId { get; set; }
    }
    
    // SF Object: Call_Plan__c
    // Used for JSON transfer from the Controller to the UI
    global class CallPlanUI {
        public String Id { get; set; }
        public String Call_Plan_Type { get; set; }
        public String Emerging_Risk { get; set; }
        public String Client { get; set; }
        public String Contact { get; set; }
        public String Joint_Call2 { get; set; }
        public String Owner_Full_Name { get; set; }
        public String Status { get; set; }
        public String Target_Date { get; set; }
        public String Meeting_Category { get; set; }
        public String Completed_Date { get; set; }
        public String Objectives { get; set; }
        public String Primary_Emerging_Risk { get; set; } 
        public String Secondary_E { get; set; }
        public String Describe_Emerging_Risk { get; set; } 
        public String Risk { get; set; }
        public String Other_Risk_Factors_Discussed { get; set; } 
        public String Call_Result { get; set; }
        public String Next_Steps { get; set; }
        public List<UserUI> Additional_Notify_Users { get; set; }
    }
}