/******************************************************************************************************************
* Name            :  RuleExecutionEngine
* Author          :  Deloitte Consulting
* Description     :  This class supports evaluation of rules and execution of actions
*******************************************************************************************************************/

public with sharing class RuleExecutionEngine {
    
    /**** Class properties  ****/
    // stores API name of sobject that the instance of rule engine supports 
    private String strsObjectName;
    // stores detailed schema description of sobject that the instance 
    // of rule engine needs to support
    private Schema.DescribeSObjectResult sObjectDescribe;
    // stores map of API name of fields as key and field result as value 
    // of all fields associated to sobject under execution
    private Map<String,Schema.DescribeFieldResult> mapFieldName_FieldResult;
    // stores list of sobject records for which rules need to be executed
    private List<sObject> listsObjectRecords;
    // map of field type to equivalent apex data type 
    private map<Id,sObject> mapRecords_toUpdate = new map<Id,sobject>();
    private map<String,sObject> mapRecords_toInsert = new map<String,sobject>();
    private Map<Schema.DisplayType,String> mapFieldDisplayType_DataType = new Map<Schema.DisplayType,String>{
                                                                Schema.DisplayType.Date => EnvironmentVariables.DATATYPE_DATE,
                                                                Schema.DisplayType.DateTime => EnvironmentVariables.DATATYPE_DATETIME,
                                                                Schema.DisplayType.Integer => EnvironmentVariables.DATATYPE_INTEGER,
                                                                Schema.DisplayType.Picklist => EnvironmentVariables.DATATYPE_STRING,
                                                                Schema.DisplayType.TextArea => EnvironmentVariables.DATATYPE_MULTIPICKLIST,
                                                                Schema.DisplayType.String => EnvironmentVariables.DATATYPE_STRING,
                                                                Schema.DisplayType.Boolean => EnvironmentVariables.DATATYPE_BOOLEAN,
                                                                Schema.DisplayType.Currency => EnvironmentVariables.DATATYPE_DOUBLE,
                                                                Schema.DisplayType.Double => EnvironmentVariables.DATATYPE_DOUBLE,
                                                                Schema.DisplayType.EMAIL => EnvironmentVariables.DATATYPE_EMAIL,
                                                                Schema.DisplayType.Phone => EnvironmentVariables.DATATYPE_PHONE,
                                                                Schema.DisplayType.URL => EnvironmentVariables.DATATYPE_STRING
                                                                };
    // map of apex data type to field on rule criterion object that is used to store the data                                                               
    private Map<String,String> mapDataType_RCFieldName = new Map<String,String>{
                                                                EnvironmentVariables.DATATYPE_DATE => 'Date_Value__c',
                                                                EnvironmentVariables.DATATYPE_INTEGER => 'Number__c',
                                                                EnvironmentVariables.DATATYPE_STRING => 'Text_Value__c',
                                                                EnvironmentVariables.DATATYPE_DOUBLE => 'Currency_Value__c',
                                                                EnvironmentVariables.DATATYPE_BOOLEAN => 'Boolean_Value__c',
                                                                EnvironmentVariables.DATATYPE_PICKLIST =>'PICKLIST',
                                                                EnvironmentVariables.DATATYPE_DATETIME => 'Date_Value__c',
                                                                EnvironmentVariables.DATATYPE_TEXTAREA => 'Text_Value__c',
                                                                EnvironmentVariables.DATATYPE_MULTIPICKLIST => 'MULTIPICKLIST',
                                                                EnvironmentVariables.DATATYPE_CURRENCY => 'Number_Value__c',
                                                                EnvironmentVariables.DATATYPE_PERCENT => 'Percent_Value__c',
                                                                EnvironmentVariables.DATATYPE_EMAIL =>  'Email_Value__c',
                                                                EnvironmentVariables.DATATYPE_PHONE => 'Phone_Value__c'
                                                                };
    // insatnce of ruleservice class that supports retreiving rules,
    // associated criteria and fields to query
    private RuleService objRuleService; 

    /**** End Class Properties ****/

    // constructor
    public RuleExecutionEngine(String strsObjectName, String strRuleType,List<sObject> listsObjectRecords) {
        this.listsObjectRecords = listsObjectRecords; // assign the list of sobject
        this.strsObjectName = strsObjectName; // assign the object name

        this.mapFieldName_FieldResult = new Map<String,Schema.DescribeFieldResult>();
        getSobjectData(strsObjectName); // get field names and its additional information
        
        this.objRuleService = new RuleService(strsObjectName,strRuleType,new Set<String>()); // call the RuleService class to fire the querys

        querySobjectRecords(); // query the necessary sobject records
    }
    
    /*Method Name    :  getSobjectData
     *Description    :  Gets all the fields based on sobject name 
     *Parameters     :  String
     *Returns        :  void
    */
    public void getSobjectData(String sobjectName){
        Map<String, Schema.SObjectType> mapGlobalDescribe = Schema.getGlobalDescribe();

        if(!mapGlobalDescribe.containskey(sobjectName)) {
            throw new RuleExecutionEngineException('Invalid sobject --> ' + strsObjectName);
        }//end if

        this.sObjectDescribe = mapGlobalDescribe.get(sobjectName).getDescribe(); // get describe based on object name
        
        for(Schema.sObjectField fieldType : sObjectDescribe.Fields.getMap().values()){ // iterate to get the field type 
            Schema.DescribeFieldResult fieldResult = fieldType.getDescribe();
            this.mapFieldName_FieldResult.put(fieldResult.getName().toLowerCase(),fieldResult);
        }//end for
    }//end method

    /*Method Name    :  querySobjectRecords
     *Description    :  Quries sobject record to get all fields required 
                        to evalute the rules 
     *Parameters     :  --none--
     *Returns        :  void
    */
    private void querySobjectRecords() {
        String strQuery = 'SELECT id,{0} FROM {1} WHERE Id IN :setIds';
        Set<id> setIds = new Map<Id,sObject>(listsObjectRecords).keySet(); // add ids to the set
        List<String> listFieldsToQuery = new List<String>();
        listFieldsToQuery.addAll(objRuleService.getsetFieldsToQuery()); // get the field names
        String fields = String.join(listFieldsToQuery,',');
        strQuery = String.format(strQuery, new LIST<String> {fields,strsObjectName});
        try {
            listsObjectRecords = database.query(strQuery); // query the sobject
        }
        catch(QueryException ex) {
            utilityClass.throwExceptions(ex); 
        }//end try catch
    }//end method

    /*Method Name    :  getRuleCriteriaResults
     *Description    :  returns result of each rule criteria for each
                        sobject in a map
                        [   {RuleCriterion1.id , [{sobject1.id,result},{sobject2.id,result} ... ] }
                            {RuleCriterion2.id , [{sobject1.id,result},{sobject2.id,result} ... ] }
                            ...
                        ]
     *Parameters     :  --none--
     *Returns        :  Map<Id,Map<Id,boolean>>
    */
    private Map<Id,Map<Id,boolean>> getRuleCriteriaResults(){
        Map<Id,Map<Id,boolean>> mapRuleCriterionId_mapsObjectId_Result = new Map<Id,Map<Id,boolean>>();
        for( Rule_Criterion__c ruleCriterion_Iterator : objRuleService.getlistAllRuleCriteria()) { // iterate over the rule criteria
            Map<Id,boolean> mapsObjectId_Result = new Map<Id,boolean>();
            for( sObject sobj_Iterator : listsObjectRecords) { // iterate over the list of sobjects
                mapsObjectId_Result.put(sobj_Iterator.id,doComparison(sobj_Iterator,ruleCriterion_Iterator)); // create a map of rule criteria ID and its result as boolean
            }//end for
            mapRuleCriterionId_mapsObjectId_Result.put(ruleCriterion_Iterator.id,mapsObjectId_Result);
        }//end for
        return mapRuleCriterionId_mapsObjectId_Result;
    }//end method


    /*Method Name    :  doComparison
     *Description    :  validates the rule criterion condition with sobject data
                        and return the result of comparison
     *Parameters     :  sObject, Rule_Criterion__c
     *Returns        :  boolean
    */
    private boolean doComparison(sObject sObjRecord, Rule_Criterion__c objCriterion) {

        boolean boolResult = false; 
        String strOperator ='';
        if(sObjRecord.getSObjectType() != sObjectDescribe.getSObjectType()) {
            throw new RuleExecutionEngineException('Invalid sobject record passed. This rule execution is ' 
                                                        +'configured for --> ' + strsObjectName);
        }//end if

        if(objCriterion.Field_Name__c!=null && !String.isBlank(objCriterion.Field_Name__c)) { // check if the field name in Rule Criteira object is not blank and null
            
            if(mapFieldName_FieldResult.containskey(objCriterion.Field_Name__c.toLowerCase())) { // check if the field is added in the map

                //get the field type
                String strRCFieldType = mapFieldDisplayType_DataType
                                            .get(mapFieldName_FieldResult
                                                .get(objCriterion.Field_Name__c.toLowerCase())
                                        .getType());

                if(objCriterion.Operator__c!=null && !String.isBlank(objCriterion.Operator__c)) { // check if the operator is not null and blank
                    strOperator = objCriterion.Operator__c.toLowerCase();   
                }
                else {
                    throw new RuleExecutionEngineException('Invalid operator on Rule Criterion record --> ' 
                                                        +objCriterion.name+', associated to rule --> '
                                                        +objCriterion.Rule__r.Name);
                }//end if else
                
                Object DataToCompare = objCriterion.get(mapDataType_RCFieldName.get(strRCFieldType)); // get the field where the comparision value is added, based on field type
                
                String strRCDataValue = String.valueOf(DataToCompare); // convert the value to string
                String strRecordDataValue = '';
                if(sObjRecord.get(objCriterion.Field_Name__c)!=null) { // check if the field name is not null
                    strRecordDataValue = String.valueOf(sObjRecord.get(objCriterion.Field_Name__c)); // get the value from the sobject field

                }//end if
                boolResult = getComparisonResult(strRecordDataValue,strRCDataValue,strRCFieldType,strOperator);
                
            }//end if
            // logic for lookup field comparison to be implimented later
            else if(objCriterion.Field_Name__c.toLowerCase().contains('__r')){

            }//end else if
        }//end if

        return boolResult;
    }//end method

    /*Method Name    :  getComparisonResult
     *Description    :  compares values based on operator and data type, and 
                        results the result of comparison
     *Parameters     :  String, String, String, String
     *Returns        :  boolean
    */
    private boolean getComparisonResult(String strRecordDataValue, String strRCDataValue, String strRCFieldType, String strOperator){
        boolean boolResult = false;
        Object valueToCompare;
        Object valueToCompareAgainst;

        Set<String> setComparisonDataTypes = new Set<String>{
                                                    EnvironmentVariables.DATATYPE_DATE,
                                                    EnvironmentVariables.DATATYPE_DATETIME,
                                                    EnvironmentVariables.DATATYPE_INTEGER,
                                                    EnvironmentVariables.DATATYPE_DOUBLE
                                                    };
        if(strRCFieldType == EnvironmentVariables.DATATYPE_DATE){ // comparision for date fields
            valueToCompare = Date.valueOf(strRecordDataValue); // convert to date
            valueToCompareAgainst = Date.valueOf(strRCDataValue); // convert to date
        }
        else if(strRCFieldType == EnvironmentVariables.DATATYPE_DATETIME) { // comparision for date time fields
            valueToCompare = DateTime.valueOf(strRecordDataValue); // convert to datetime
            valueToCompareAgainst = DateTime.valueOf(strRCDataValue);// convert to datetime
        }
        else if(strRCFieldType == EnvironmentVariables.DATATYPE_INTEGER) { // comparision for integer fields
            valueToCompare = Integer.valueOf(strRecordDataValue); // convert to integer
            valueToCompareAgainst = Integer.valueOf(strRCDataValue); // convert to integer
        }
        else if(strRCFieldType == EnvironmentVariables.DATATYPE_DOUBLE) { // comparision for currency fields
            valueToCompare = Double.valueOf(strRecordDataValue); // convert to double
            valueToCompareAgainst = Double.valueOf(strRCDataValue); // convert to double
        }else if (strRCFieldType == EnvironmentVariables.DATATYPE_BOOLEAN){ // comparision for boolean fields
            valueToCompare = Boolean.valueOf(strRecordDataValue); // convert to boolean
            valueToCompareAgainst = Boolean.valueOf(strRCDataValue); // convert to boolean    
        }
        else {
            valueToCompare = strRecordDataValue;
            valueToCompareAgainst = strRCDataValue;
        }

        if(strOperator == EnvironmentVariables.OPERTATOR_EQUALSTO) { // for Equals To operator
            boolResult = valueToCompare == valueToCompareAgainst?true:false;
        }
        else if(strOperator == EnvironmentVariables.OPERTATOR_NOTEQUALTO) {// for Not Equals To operator
            boolResult = valueToCompare != valueToCompareAgainst?true:false;
        }
        else if(setComparisonDataTypes.contains(strRCFieldType)) {
            // for greater than operator
            if(strOperator == EnvironmentVariables.OPERTATOR_GREATERTHAN) {
                if(strRCFieldType == EnvironmentVariables.DATATYPE_DATE)
                    boolResult = (Date)valueToCompare > (Date)valueToCompareAgainst?true:false; 
                if(strRCFieldType == EnvironmentVariables.DATATYPE_DATETIME)
                    boolResult = (DateTime)valueToCompare > (DateTime)valueToCompareAgainst?true:false; 
                if(strRCFieldType == EnvironmentVariables.DATATYPE_INTEGER)
                    boolResult = (Integer)valueToCompare > (Integer)valueToCompareAgainst?true:false;
                if(strRCFieldType == EnvironmentVariables.DATATYPE_DOUBLE)
                    boolResult = (Double)valueToCompare > (Double)valueToCompareAgainst?true:false; 
            }
            // for greater than equal operator
            else if(strOperator == EnvironmentVariables.OPERTATOR_GREATERTHANOREQUALTO) {
                if(strRCFieldType == EnvironmentVariables.DATATYPE_DATE)
                    boolResult = (Date)valueToCompare >= (Date)valueToCompareAgainst?true:false;    
                if(strRCFieldType == EnvironmentVariables.DATATYPE_DATETIME)
                    boolResult = (DateTime)valueToCompare >= (DateTime)valueToCompareAgainst?true:false;    
                if(strRCFieldType == EnvironmentVariables.DATATYPE_INTEGER) 
                    boolResult = (Integer)valueToCompare >= (Integer)valueToCompareAgainst?true:false;
                if(strRCFieldType == EnvironmentVariables.DATATYPE_DOUBLE)  
                    boolResult = (Double)valueToCompare >= (Double)valueToCompareAgainst?true:false;    
            }
            // for less than equal to operator
            else if(strOperator == EnvironmentVariables.OPERTATOR_LESSTHANOREQUALTO) {
                if(strRCFieldType == EnvironmentVariables.DATATYPE_DATE)
                    boolResult = (Date)valueToCompare <= (Date)valueToCompareAgainst?true:false;    
                if(strRCFieldType == EnvironmentVariables.DATATYPE_DATETIME)
                    boolResult = (DateTime)valueToCompare <= (DateTime)valueToCompareAgainst?true:false;    
                if(strRCFieldType == EnvironmentVariables.DATATYPE_INTEGER)
                    boolResult = (Integer)valueToCompare <= (Integer)valueToCompareAgainst?true:false;
                if(strRCFieldType == EnvironmentVariables.DATATYPE_DOUBLE)
                    boolResult = (Double)valueToCompare <= (Double)valueToCompareAgainst?true:false;    
            }
            // for less than operator
            else if(strOperator == EnvironmentVariables.OPERTATOR_LESSTHAN) {
                if(strRCFieldType == EnvironmentVariables.DATATYPE_DATE)
                    boolResult = (Date)valueToCompare < (Date)valueToCompareAgainst?true:false; 
                if(strRCFieldType == EnvironmentVariables.DATATYPE_DATETIME)
                    boolResult = (DateTime)valueToCompare < (DateTime)valueToCompareAgainst?true:false; 
                if(strRCFieldType == EnvironmentVariables.DATATYPE_INTEGER)
                    boolResult = (Integer)valueToCompare < (Integer)valueToCompareAgainst?true:false;
                if(strRCFieldType == EnvironmentVariables.DATATYPE_DOUBLE)
                    boolResult = (Double)valueToCompare < (Double)valueToCompareAgainst?true:false; 
            }
        }
        else if(strRCFieldType == EnvironmentVariables.DATATYPE_STRING) {
            // for contains operator 
            if(strOperator == EnvironmentVariables.OPERTATOR_CONTAINS) { 
                boolResult = strRecordDataValue.contains(strRCDataValue)?true:false;
            }
            else if(strOperator == EnvironmentVariables.OPERTATOR_DOESNOTCONTAIN) {
                boolResult = (!strRecordDataValue.contains(strRCDataValue))?true:false;
            } 
        }
        return boolResult;
    }

    /*Method Name    :  executeRules
     *Description    :  iterates for all rules for each sobject record and 
                        evaluates the criteria logic passing results prepared from
                        getRuleCriteriaResults. Method prepares the results in a map
                        [   {rule1.id, [{sobject1.id, result}, {sobject2.id, result} ... ] } 
                            {rule2.id, [{sobject1.id, result}, {sobject2.id, result} ... ] } 
                            ...
                        ]
     *Parameters     :  --none--
     *Returns        :  void
    */
    public void executeRules(){

        Map<Id,Map<Id,boolean>> mapRuleCriterionId_mapsObjectId_Result = getRuleCriteriaResults();
        Map<Id,Map<Id,boolean>> mapRuleId_mapsObjectId_Result = new Map<Id,Map<Id,boolean>>();
        
        ExpressionEvaluator evaluator = new ExpressionEvaluator();
        for(Rule__c ruleIterator : objRuleService.getlistActiveRulesWithCriteria()) { 

            list<Rule_Criterion__c> ruleCriteria = ruleIterator.Rule_Criteria__r;
            Map<Id,Boolean> mapRuleResultPersObject = new Map<Id,Boolean>();

            for(sObject sobject_Iterator : listsObjectRecords) {
                Map<String,Boolean> mapCriteriaResult = new Map<String,Boolean>();
                for(Rule_Criterion__c rcIterator : ruleCriteria) {
                    mapCriteriaResult.put(String.valueOf(rcIterator.Criteria_Sequence__c), 
                                            mapRuleCriterionId_mapsObjectId_Result.get(rcIterator.id)
                                                .get(sobject_Iterator.id)); 
                }
                
                evaluator = new ExpressionEvaluator();
                Boolean boolResult = evaluator.evaluationExpression(ruleIterator.Criteria_Logic__c, mapCriteriaResult); 
                mapRuleResultPersObject.put(sobject_Iterator.id,boolResult);

            }

            mapRuleId_mapsObjectId_Result.put(ruleIterator.id,mapRuleResultPersObject);
        }

        system.debug('####'+mapRuleId_mapsObjectId_Result);
        Set<Id> setRuleIds =  mapRuleId_mapsObjectId_Result.keySet(); // set to store all the RuleIDs
        List<Rule_Action__c> lstActions =  objRuleService.queryRuleActions(mapRuleId_mapsObjectId_Result,setRuleIds);//query to get all actions
        getRuleActions(lstActions,setRuleIds,mapRuleId_mapsObjectId_Result); // based on the query response generate rule actions
    }//end method
    
    /*Method Name    :  getRuleActions
     *Description    :  This method is used to get the rule actions and process the data as needed
     *Parameters     :  List<Rule_Action__c> lstActions, Set<Id> setRuleIds, Map<Id,Map<Id,boolean>> mapRuleId_mapsObjectId_Result
     *Returns        :  void
    */
    public void getRuleActions(List<Rule_Action__c> lstActions, Set<Id> setRuleIds, Map<Id,Map<Id,boolean>> mapRuleId_mapsObjectId_Result){
        System.debug('inside getRuleActions');
        map <Id,List<Rule_Action__c>> mapRuleActions = new map <Id,List<Rule_Action__c>>(); 
        map <Id,List<Record_data__c>> mapRuleData = new map <Id,List<Record_data__c>>(); 
        map <Id,List<Rule_Action__c>> mapSobjectId_Actions = new map<Id,List<Rule_Action__c>>();
        
        List<sObject> lstSobject_ToUpdate = new List<sObject>(); 
        
        //create maps to link the Rule ID with its Rule action & Rule Action ID with its Rule data
        if(lstActions!=null && !lstActions.IsEmpty()) // null check
        {
            for(Rule_Action__c iterator_Action : lstActions) //Go through the list of Actions
            {
                List<Rule_Action__c> lstRuleAction = new List<Rule_Action__c>();
                if(mapRuleActions.containskey(iterator_Action.Rule__r.Id)){ // create a map of Rule ID and list of Rule Actions
                    lstRuleAction=mapRuleActions.get(iterator_Action.Rule__r.Id);
                    lstRuleAction.add(iterator_Action);
                    mapRuleActions.put(iterator_Action.Rule__r.Id,lstRuleAction); 
                }else{
                    lstRuleAction.add(iterator_Action);
                    mapRuleActions.put(iterator_Action.Rule__r.Id,lstRuleAction);   
                }//end if else
                for(Record_data__c iterator_RecorData:iterator_Action.Records_data__r){ //iterate over the child records
                    List<Record_data__c> lstRecordData = new  List<Record_data__c>(); // Create a map of Rule Action ID and list of Rule Data
                    if(mapRuleData.containskey(iterator_Action.Id)){
                        lstRecordData=mapRuleData.get(iterator_Action.Id);
                        lstRecordData.add(iterator_RecorData);
                        mapRuleData.put(iterator_Action.Id,lstRecordData); 
                    }else{
                        lstRecordData.add(iterator_RecorData);
                        mapRuleData.put(iterator_Action.Id,lstRecordData);   
                    }//end if else 
                }//end for  
            }//end for
        }//end if 
       
       //create a map to link the sObject Id with the actions to be excecuted on it  
       for(Id ruleId : setRuleIds){ // go through the list of Rule IDs
            Set<Id> sobjectId = mapRuleId_mapsObjectId_Result.get(ruleId).keySet();  // get the Record Id 
            map<Id,Boolean> mapResult = new map<Id,Boolean>();
            List<Rule_Action__c> lstActionList = mapRuleActions.get(ruleId); //get the list of rules
            mapResult = mapRuleId_mapsObjectId_Result.get(ruleId); // get the results map
            for(Id iterator:sobjectId){
               if(mapResult.get(iterator) == True) // execute the rules only when for the records that resulted in true
                    mapSobjectId_Actions.put(iterator,lstActionList); // create a map of the Sobject ID and its RuleAction 
            }//end for
        }//end for 
        
        for(Id sobject_Iterator: mapSobjectId_Actions.keyset()){//Iterate over the rule action records and decide if update/insert should be done
            for(Rule_Action__c iterator_Action : mapSobjectId_Actions.get(sobject_Iterator)){
                if(iterator_Action.Action_Type__c=='Update Current Record'){
                    getRecordsToUpdate(sobject_Iterator,mapRuleData.get(iterator_Action.Id));    
                }else if (iterator_Action.Action_Type__c=='Create Child Record'){
                    getRecordsToInsert(sobject_Iterator,mapRuleData.get(iterator_Action.Id)); 
                }
            }       
        }//end for 
        performActions(); // finally perform insert/udpate actions        
    }//end method
    
    /*Method Name    :  getRecordsToUpdate
     *Description    :  This method is used to get a map of records that needs to be updated as part of Action
     *Parameters     :  Id recordId,List<Record_data__c> lstRecordData
     *Returns        :  void
    */
    public void getRecordsToUpdate(Id recordId,List<Record_data__c> lstRecordData ){
        
        List<sObject> lstRecordsToUpdate = new List<sObject>();
        
        for(Record_data__c iterator_RecordData:lstRecordData)
        {
            String strFieldType = mapFieldDisplayType_DataType
                                            .get(mapFieldName_FieldResult
                                                .get(iterator_RecordData.Field_Name__c.toLowerCase())
                                        .getType());
            System.debug('type is '+ strFieldType); 
            sObject sobj = Schema.getGlobalDescribe().get(strsObjectName).newSObject() ;
            if(strFieldType == EnvironmentVariables.DATATYPE_DATE || strFieldType == EnvironmentVariables.DATATYPE_DATETIME)
            { 
                 sobj.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Date_Value__c);
            }
            else if (strFieldType== EnvironmentVariables.DATATYPE_INTEGER)
            {
                sobj.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Number_Value__c);
            } 
            else if (strFieldType== EnvironmentVariables.DATATYPE_STRING)
            {
                sobj.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Text_Value__c);
            } 
            else if (strFieldType== EnvironmentVariables.DATATYPE_BOOLEAN)
            {
                System.debug('is boolean'+iterator_RecordData.Boolean_Value__c);
                sobj.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Boolean_Value__c);
            } 
            else if (strFieldType== EnvironmentVariables.DATATYPE_DOUBLE)
            {
                sobj.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Currency_Value__c);
            }                                                  
            if(!mapRecords_toUpdate.containskey(recordId)){
                sobj.put('Id',recordId);
                //sobj.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Text_Value__c);
                mapRecords_toUpdate.put(recordId,sobj);
            }else{
                sObject temp = mapRecords_toUpdate.get(recordId);
                temp.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Text_Value__c);  
                mapRecords_toUpdate.put(recordId,temp); 
            }//end if else
        }//end for
        System.debug('UpdateAction on '+recordId);  
        System.debug('Record data Is '+mapRecords_toUpdate);
    }//end method
    
    /*Method Name    :  getRecordsToInsert
     *Description    :  This method is used to get a map of records that needs to be inserted as part of Action
     *Parameters     :  Id recordId,List<Record_data__c> lstRecordData
     *Returns        :  void
    */
    public void getRecordsToInsert(Id recordId,List<Record_data__c> lstRecordData ){
        List<sObject> lstRecordsToInsert = new List<sObject>();
        for(Record_data__c iterator_RecordData:lstRecordData)
        {
            String objectName = iterator_RecordData.Rule_Action__r.Object_Name__c; 
            sObject sobj = Schema.getGlobalDescribe().get(objectName).newSObject() ;
            
            getSobjectData(objectName);
            System.debug('##### '+objectName);
            String strFieldType = mapFieldDisplayType_DataType
                                            .get(mapFieldName_FieldResult
                                                .get(iterator_RecordData.Field_Name__c.toLowerCase())
                                        .getType());
            if(strFieldType == EnvironmentVariables.DATATYPE_DATE || strFieldType == EnvironmentVariables.DATATYPE_DATETIME)
            { 
                 sobj.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Date_Value__c);
            }
            else if (strFieldType== EnvironmentVariables.DATATYPE_INTEGER)
            {
                sobj.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Number_Value__c);
            } 
            else if (strFieldType== EnvironmentVariables.DATATYPE_STRING)
            {
                sobj.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Text_Value__c);
            } 
            else if (strFieldType== EnvironmentVariables.DATATYPE_BOOLEAN)
            {
                System.debug('is boolean'+iterator_RecordData.Boolean_Value__c);
                sobj.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Boolean_Value__c);
            } 
            else if (strFieldType== EnvironmentVariables.DATATYPE_DOUBLE)
            {
                sobj.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Currency_Value__c);
            }                 
            if(!mapRecords_toInsert.containskey(objectName)){
                sobj.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Text_Value__c);
                mapRecords_toInsert.put(objectName,sobj);
            }else{
                sObject temp = mapRecords_toInsert.get(objectName);
                temp.put(iterator_RecordData.Field_Name__c,iterator_RecordData.Text_Value__c);  
                mapRecords_toInsert.put(objectName,temp); 
            }//end if else   
        }//end for 
        System.debug('CreateAction on '+recordId);  
        System.debug('Record data Is '+mapRecords_toInsert);      
    }//end method
    
    /*Method Name    :  performActions
     *Description    :  This method is used to update/insert records
     *Parameters     :  Id recordId,List<Record_data__c> lstRecordData
     *Returns        :  void
    */
    public void performActions(){
        try{
            if(mapRecords_toUpdate!=null){
                List<sobject> lstupdateRecords = mapRecords_toUpdate.values();
                if(lstupdateRecords!=null && lstupdateRecords.size()>0)
                    update lstupdateRecords;
            }//end if
            if(mapRecords_toInsert!=null){
                List<sobject> lstInsertRecords = mapRecords_toInsert.values();
                if(lstInsertRecords!=null && lstInsertRecords.size()>0)
                    insert lstInsertRecords;
            }//end if
        }catch(DMLException e){
            System.debug('DML exception is '+e.getMessage());    
        }//end try catch
    }//end method
    
    // exception class for rule execution engine
    public class RuleExecutionEngineException extends Exception { }
}