@isTest
private class TestSalesExecutiveBatch{
	static testmethod void testBatchClass(){
		Test.startTest();
		Database.QueryLocator QL;
		Database.BatchableContext BC;
		
		Profile p = [select id from profile where name='Regions Standard User'];
		User testUser1 = new User(alias = 'user1', email= 'user1@testorg.com',
              emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
              localesidkey='en_US', profileid = p.Id, country='United States',
              timezonesidkey='America/Chicago', username= 'scooby1@testorg.com', C_I_Type__c = 'Corporate');
        insert testUser1;      
        
        User testUser2 = new User(alias = 'user2', email= 'user2@testorg.com',
              emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
              localesidkey='en_US', profileid = p.Id, country='United States',
              timezonesidkey='America/Chicago', username= 'scooby2@testorg.com', C_I_Type__c = 'Corporate', Sales_Executive__c = testUser1.Id);
        insert testUser2;  
        
        User testUser3 = new User(alias = 'user3', email= 'user3@testorg.com',
              emailencodingkey='UTF-8', lastname='Testing3', languagelocalekey='en_US',
              localesidkey='en_US', profileid = p.Id, country='United States',
              timezonesidkey='America/Chicago', username= 'scooby3@testorg.com', C_I_Type__c = 'Corporate', Sales_Executive__c = testUser2.Id);
        		insert testUser3; 
        
        Account acc = new Account(Name='Scooby Account', OwnerId=testUser2.Id);
    	insert acc;
    	
    	Account acc2 = new Account(Name='Scooby Doo', OwnerId=testUser3.Id);
    	insert acc2;  
    	
    	AccountShare a1 = New AccountShare(UserOrGroupId = testUser1.Id, AccountId=acc.Id, AccountAccessLevel = 'Edit',
                           ContactAccessLevel = 'Edit', CaseAccessLevel = 'Edit', OpportunityAccessLevel = 'Edit');
    	AccountShare a2 = New AccountShare(UserOrGroupId = testUser2.Id, AccountId=acc.Id, AccountAccessLevel = 'Edit',
                           ContactAccessLevel = 'Edit', CaseAccessLevel = 'Edit', OpportunityAccessLevel = 'Edit');
        AccountShare a3 = New AccountShare(UserOrGroupId = testUser2.Sales_Executive__c, AccountId=acc.Id, AccountAccessLevel = 'Edit',
                           ContactAccessLevel = 'Edit', CaseAccessLevel = 'Edit', OpportunityAccessLevel = 'Edit');
                           
        AccountTeamMember at1 = New AccountTeamMember(UserId = testUser1.Id, AccountID = acc.Id, TeamMemberRole = 'Team');
    	
    	AccountTeamMember at2 = New AccountTeamMember(UserId = testUser2.Id, AccountID = acc.Id, TeamMemberRole = 'Owner');
                          
        AccountTeamMember at3 = New AccountTeamMember(UserId = testUser2.Sales_Executive__c, AccountID = acc.Id, 
                          TeamMemberRole = 'Team');                  
    	      
		List <account> AcctList = [Select Id, OwnerId, Owner.C_I_Type__c, Owner.Sales_Executive__c From Account where Owner.C_I_Type__c = 'Corporate' and Owner.Sales_Executive__c <> null limit 1];
   		SalesExecutiveClientTeamBatch sb = new SalesExecutiveClientTeamBatch();		
		try{
    		QL = sb.start(BC);	
	    	sb.execute(BC,AcctList);
	    	sb.finish(BC);
    	}catch(exception e){
    		System.debug('System-generated error message: '+e.getMessage());
    	}
	    Test.stopTest();	
    }    
}