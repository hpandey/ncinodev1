global class batchPipelineGoalScheduler implements Schedulable{
   global void execute(SchedulableContext sc) {
      batchPipelineGoals bPG = new batchPipelineGoals(); 
      database.executebatch(bPG);
   }
}