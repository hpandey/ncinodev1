global class clientTeamRequest
{
    webservice static Boolean sendEmail(Id accountId, String emailAdd)
    {
        Boolean userNotified = true;
        
        try
        {
            // Pick a dummy Contact
            Contact c = [select id, Email from Contact where email <> null limit 1];
            
            // Construct the list of emails we want to send
            List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();
            
            Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
            msg.setTemplateId( [select id from EmailTemplate Where Name = 'Client Request Access Email'].id );
            msg.setWhatId(accountId);
            msg.setTargetObjectId(c.id);
            msg.setToAddresses(new List<String>{emailAdd});
            
            lstMsgs.add(msg);
            
            // Send the emails in a transaction, then roll it back
            Savepoint sp = Database.setSavepoint();
            Messaging.sendEmail(lstMsgs);
            Database.rollback(sp);
            
            // For each SingleEmailMessage that was just populated by the sendEmail() method, copy its
            // contents to a new SingleEmailMessage. Then send those new messages. 
            List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>(); 
            for (Messaging.SingleEmailMessage email : lstMsgs) { 
                Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage(); 
                emailToSend.setToAddresses(email.getToAddresses()); 
                emailToSend.setPlainTextBody(email.getPlainTextBody()); 
                emailToSend.setHTMLBody(email.getHTMLBody()); 
                emailToSend.setSubject(email.getSubject());
                lstMsgsToSend.add(emailToSend); 
            }
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(lstMsgsToSend);
            
            for(Messaging.SendEmailResult res : results){
                    if(res.isSuccess() == false){
                            userNotified = false;
                    }
            }
        } catch(Exception e) {
            userNotified = false;
        }
        return userNotified;
    }
}