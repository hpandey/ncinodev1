global class OppApprovalLookBack {


    WebService static String ApproveLookBack(id oppID) {     
    	string ErrMsg;
    	Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
		req1.setComments('Submitting request for approval.');
		req1.setObjectId(oppID);
		try {
			Approval.ProcessResult result = Approval.process(req1);
		} catch (System.DmlException e) {
		       for (Integer i = 0; i < e.getNumDml(); i++) {
                            if( ErrMsg == null ||  ErrMsg == ''){
                                     ErrMsg =  e.getDmlMessage(i);
                            }else{
                                    ErrMsg = ErrMsg + '\n' +   e.getDmlMessage(i) ;
                            }
                            		         System.debug(e.getDmlMessage(i)); 
       	
				}
				 System.debug(ErrMsg); 
				 return ErrMsg;
		}
    	return null;
    }	
    	
}