/******************************************************************************************************************
* Name            :             Decision_Matrix
* Author          :             Deloitte Consulting
* Description     :             This class is used to execute rules and perform actions based on the results
*******************************************************************************************************************/
public with sharing class Decision_Matrix{

    public List<Rule_Criterion__c> lstRules;
    private String strCriteriaLogic;
    

    /*Method Name    :        getActiveRules
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to get all the rule criteria 
     *Parameters     :        String,String
     *Returns        :        Void
    */
    public void getActiveRules(String ruleType , String objectName, Set<ID>lstSobjectIds){
        String strFieldNames='';
        lstRules = [SELECT Id,Currency_Value__c,Date_Value__c,Field_Name__c,Number__c,
                                           Operator__c,Text_Value__c,Rule__c,Rule__r.Active__c,Rule__r.Criteria_Logic__c,
                                           Rule__r.Object_Name__c,Rule__r.Rule_Type__c,Field_Type__c,Name
                                           FROM Rule_Criterion__c
                                           WHERE Rule__r.Active__c = true AND Rule__r.Rule_Type__c=:ruleType 
                                           AND Rule__r.Object_Name__c=:objectName]; 
        if(!lstRules.isEmpty())
            strCriteriaLogic = lstRules[0].Rule__r.Criteria_Logic__c; // this will be further used to evaluate the records
        for(Rule_Criterion__c criteria:lstRules)
        {
            if(strFieldNames=='')
            {
                strFieldNames = criteria.Field_Name__c;     
            }else
                strFieldNames += ','+criteria.Field_Name__c; // Add all the field names in a string which will further be querried.   
        }//end for 
        
        getObjectRecords(objectName,lstSobjectIds,strFieldNames);                                    
    }//end method
    
    /*Method Name    :        getObjectRecords
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to get necessary fields from the object  
     *Parameters     :        String,List<Id>
     *Returns        :        Void
    */
    public void getObjectRecords(String objectName, Set<ID>lstSobjectIds, String fieldNames){
        String queryStr;
        List<sObject> lstRecords = new List<Sobject>();
        System.debug('lstSobjectIds conatins '+fieldNames);
        if(!lstSobjectIds.isEmpty() && fieldNames!='')
        {
             queryStr = 'SELECT Id,'+fieldNames+' FROM '+objectName +' where ID IN ';
             queryStr +=':lstSobjectIds';
             lstRecords = database.query(queryStr);  
        }//end if 
        System.debug('@@@@@@@@@@ '+lstRecords ); 
        evaluateRules(lstRecords ,objectName); 
   }//end method
    
    /*Method Name    :        evaluateRules
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to evaluate the active rules to check its outcome 
     *Parameters     :        List<Rule_Criterion__c>
     *Returns        :        Void
    */ 
    public map<String,map<String,Boolean>> evaluateRules(List<sObject> lstRecords, String objectName ){
        map<String,map<String,Boolean>> mapResults = new map<String,map<String,Boolean>>();// This map will store the results in a map for each record
        for(Rule_Criterion__c rule :lstRules)
        {
           for(Sobject sobj:lstRecords)
           {
               if(rule.Operator__c =='Equal to' && rule.Field_Type__c=='Number') //execute Number & equal to conditions
               {
                   if(sobj.get(rule.Field_Name__c) == rule.Number__c)
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),true,mapResults,rule.Name);
                   }else
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),false,mapResults,rule.Name);  
                   }//end else   
               }
               
               else if(rule.Operator__c =='Equal to' && rule.Field_Type__c=='Text') //execute equal & Text conditions
               {
                   if(sobj.get(rule.Field_Name__c) == rule.Text_Value__c)
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),true,mapResults,rule.Name);
                   }else
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),false,mapResults,rule.Name);  
                   }//end else   
               }//end if   
               
               else if(rule.Operator__c =='Equal to' && rule.Field_Type__c=='Date') //execute equal & Date conditions
               {
                   if(sobj.get(rule.Field_Name__c) == rule.Date_Value__c)
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),true,mapResults,rule.Name);
                   }else
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),false,mapResults,rule.Name);  
                   }//end else   
               }//end if 
               
               else if(rule.Operator__c =='Equal to' && rule.Field_Type__c=='Currency') //execute equal & Currency conditions
               {
                   if(sobj.get(rule.Field_Name__c) == rule.Currency_Value__c)
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),true,mapResults,rule.Name);
                   }else
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),false,mapResults,rule.Name);  
                   }//end else   
               }//end if 
               
               else if(rule.Operator__c =='Equal to' && rule.Field_Type__c=='Number') //execute Greater than = & Number conditions
               {
                   if(Integer.valueof(sobj.get(rule.Field_Name__c)) >= rule.Number__c)
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),true,mapResults,rule.Name);
                   }else
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),false,mapResults,rule.Name);  
                   }//end else   
               }//end if 
               
               else if(rule.Operator__c =='Equal to' && rule.Field_Type__c=='Date') //execute Greater than = & Date conditions
               {
                   if(Date.valueof(sobj.get(rule.Field_Name__c)) >= rule.Date_Value__c)
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),true,mapResults,rule.Name);
                   }else
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),false,mapResults,rule.Name);  
                   }//end else   
               }//end if
               
               else if(rule.Operator__c =='Equal to' && rule.Field_Type__c=='Currency') //execute Greater than = & Currency conditions
               {
                   if(Double.valueof(sobj.get(rule.Field_Name__c)) >= rule.Currency_Value__c)
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),true,mapResults,rule.Name);
                   }else
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),false,mapResults,rule.Name);  
                   }//end else   
               }//end if         
               
               else if(rule.Operator__c =='Equal to' && rule.Field_Type__c=='Number') //execute Less than = & Number conditions
               {
                   if(Integer.valueof(sobj.get(rule.Field_Name__c)) <= rule.Number__c)
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),true,mapResults,rule.Name);
                   }else
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),false,mapResults,rule.Name);  
                   }//end else   
               }//end if 
               
               else if(rule.Operator__c =='Equal to' && rule.Field_Type__c=='Date') //execute Less than = & Date conditions
               {
                   if(Date.valueof(sobj.get(rule.Field_Name__c)) <= rule.Date_Value__c)
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),true,mapResults,rule.Name);
                   }else
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),false,mapResults,rule.Name);  
                   }//end else   
               }//end if
               
               else if(rule.Operator__c =='Equal to' && rule.Field_Type__c=='Currency') //execute less than = & Currency conditions
               {
                   if(Double.valueof(sobj.get(rule.Field_Name__c)) <= rule.Currency_Value__c)
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),true,mapResults,rule.Name);
                   }else
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),false,mapResults,rule.Name);  
                   }//end else   
               }//end if
               
               else if(rule.Operator__c =='Equal to' && rule.Field_Type__c=='Text') //execute contains & text conditions
               {
                   if(String.valueof(sobj.get(rule.Field_Name__c)).contains(rule.Text_Value__c))
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),true,mapResults,rule.Name);
                   }else
                   {
                       mapResults=generateMap(String.valueof(sobj.get('Id')),false,mapResults,rule.Name);  
                   }//end else   
               }//end if
               
           }//end for  
        }//end for
        System.debug('@@@@@@@@@@ '+mapResults);
        return mapResults;
    }//end method
    
   public map<String,map<String,Boolean>> generateMap(String recordId, Boolean result,map<String,map<String,Boolean>>mpAllResults,String ruleName ){
      map<String,Boolean> mpIndividualResults = new map<String,Boolean>(); 
      if(mpAllResults!=null)
      {
          if(mpAllResults.containskey(recordId)) 
          {
              mpIndividualResults = mpAllResults.get(recordId);
              mpIndividualResults.put(ruleName,result); 
              mpAllResults.put(recordId,mpIndividualResults);     
          }else if(!mpAllResults.containskey(recordId)){
              mpIndividualResults.put(ruleName,result);
              mpAllResults.put(recordId,mpIndividualResults);
          }//end if
      }//end if
      return mpAllResults;    
   }//end method
      
}//end class