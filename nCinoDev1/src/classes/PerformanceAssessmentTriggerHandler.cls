public with sharing class PerformanceAssessmentTriggerHandler {
    public PerformanceAssessmentTriggerHandler(){
        //Handler
    }
    public void UpdateRecordOwner(List<Performance_Assessment__c> newRecords){
        //replaces the owner of the record with the credit underwriter or the creator... Ephrem 06-2014
        for(Performance_Assessment__c thisPA : newRecords){
            if (thisPA.Assessment_Status__c == 'Final'){
                thisPA.OwnerId = thisPA.Credit_Underwriter__c;
            }else if(thisPA.Assessment_Status__c == 'Draft'){
                thisPA.OwnerId = thisPA.CreatedById;
            }else {}
        }
    }
}