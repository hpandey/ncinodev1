global class deleteParticipantBatch implements Database.batchable<sObject>, Database.Stateful {
	global final String Query;
		
	global deleteParticipantBatch(){
       Query = 'Select ID From Participant__c where Flag_for_Deletion__c = True';
    } 		

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Participant__c> ParticipantList){
    	List<Participant__c> PList = new List<Participant__c>();
		 for (Participant__c p : ParticipantList){ 
			PList.add(p);		 	
		 }
		Delete PList;		    	
    }    
    
 	global void finish(Database.BatchableContext BC){
      AsyncApexJob aaj = [select Id, ApexClassId, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors, 
        CompletedDate, MethodName, ExtendedStatus 
        from AsyncApexJob where Id=:BC.getJobId()];
        
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  

      if (aaj.NumberofErrors>0){
        
        //Send Mail on failure if specified in the custom setting.
        string toaddress = 'chris.wilder@regions.com';
        mail.setToAddresses(new String[] { 'chris.wilder@regions.com' });
           mail.setSubject('FAILED : deleteParticipantBatch Job Run');  
           mail.setPlainTextBody('The batch Apex job processed ' + aaj.TotalJobItems +  
          ' batches with '+ aaj.NumberOfErrors + ' failures. ExtendedStatus: ' + aaj.ExtendedStatus);  
       
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
      }
 		
 	}    
}