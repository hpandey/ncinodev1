global class batchPipelineGoalsUpdate implements Database.Batchable<sObject>
{
    List<Goal__c> goals {get; set;}
    global batchPipelineGoalsUpdate(){
        // 20140116 - pmartin - only link open pipeline to current and past goals
        // issue affecting TM where openpipelines linked to future months and then dup when pipeline closed
        goals = new List<Goal__c>([SELECT Id, User__c, Start_Date__c, End_Date__c, Time_Period__c from Goal__c where Is_Pipeline_Goal__c=TRUE and start_date__c < TODAY]);
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id,Opportunity.CloseDate,UserId, Deal_Credit__c, Opportunity.IsClosed ' +
                       'FROM OpportunityTeamMember ' +
                       'WHERE Opportunity.CloseDate <> null and UserId <> null and Opportunity.Probability > 49.0';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<OpportunityTeamMember> scope)
    {
        List<Pipeline_Goal__c> newPipelineGoals = New List<Pipeline_Goal__c>();
        System.debug('Processing ' + scope.size() + ' OpportunityTeamMembers against ' + goals.size() + ' Pipeline goals records.');
        for(OpportunityTeamMember otm : scope)
        {
            for(Goal__c g : goals)
            {
                //For each triggered Opportunity Team Member, find goals that match this user and time period
                // 20140116 - pmartin - only link open pipeline to current and past goals
                // issue affecting TM where openpipelines linked to future months and then dup when pipeline closed
                if(otm.UserId == g.User__c 
                && (
                (!otm.Opportunity.IsClosed & (g.End_Date__c >= date.today()))
                || (otm.Opportunity.CloseDate >= g.Start_Date__c && otm.Opportunity.CloseDate <= g.End_Date__c))){
                    newPipelineGoals.add(new Pipeline_Goal__c(
                        Goal__c = g.Id,
                        Pipeline__c = otm.OpportunityId,
                        Deal_Credit__c = otm.Deal_Credit__c
                        )
                    );   
                }
            }
        }
        insert newPipelineGoals;
    }   
    global void finish(Database.BatchableContext BC)
    {
    }
}