/**=====================================================================
 * Appirio, Inc
 * Name: CallPlanEmailParticipantsTriggerHandler
 * Description: Story S-271020  Task # T-335124 Send emails to Participants__c if Notification flag = true
 * 				and send emails to additional email recipients on the Call Plan record.
 * Created Date: 12/2/2014
 * Created By: Rita Washington (Appirio)
 =====================================================================*/
public class CallPlanEmailParticipantsTriggerHandler {
    public Map<Participant__c, Messaging.SingleEmailMessage> EmailParticipantMap = new Map<Participant__c, Messaging.SingleEmailMessage>();
    
    public void SendParticipantEmails(List<Call_Plan__c> callplan){
    	List<Participant__c> part = new List<Participant__c>();
        
        //Get Call Plan Participants      
        for (Participant__c p : [Select p.Name__c, p.Call_Plan__r.Id, p.Call_Plan__r.Status__c, 
                                 		p.Call_Plan__r.Changes_Business_or_Financial_Profile__c, 
                                 		p.Call_Plan__c, p.Call_Plan__r.Client__r.Name, 
                                 		p.Call_Plan__r.Client__r.Owner_ID__c,
                                 		p.Email_Sent__c
                                 From Participant__c p 
                                 Where p.Call_Plan__r.Id in : callplan
                                 And p.Send_Notification__c = true 
                                 And p.Call_Plan__r.Status__c = 'Completed' 
                                 And p.Call_Plan__r.Changes_Business_or_Financial_Profile__c = 'Yes'
                                 And p.Email_Sent__c = false]) {
                                 part.add(p) ;                         
        }
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

        emails = CreateEmailMsgs(part,callplan);
        sendEmails();
	}

    private List<Messaging.SingleEmailMessage> CreateEmailMsgs(List<Participant__c> pts, List<Call_Plan__c> callplan) {
		List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        String CPWebAddress;
        List<String> addlEmails;
        String emailAddresses;
        
        //Create Emails for Call Plan Participants
        for (Participant__c p : pts) {
            Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
            
        	CPWebAddress = URL.getSalesforceBaseUrl().toExternalForm()+'/'+ p.Call_Plan__r.Id;
            string clientName = p.Call_Plan__r.Client__r.Name;
            singleMail.setTargetObjectId(p.Name__c); 
            
			constructRemainingMsg(singleMail, clientName, CPWebAddress);
            EmailParticipantMap.put(p, singleMail);
           
            emails.add(singleMail);            
        }
        //Create Emails for Additional Recipients
        for(Call_Plan__c cp : callplan) {
            if(cp.Additional_Email_Recipients__c != null) {
                Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
                emailAddresses = String.valueOf(cp.Additional_Email_Recipients__c);
                
                addlEmails = emailAddresses.split(',');
                singleMail.setToAddresses(addlEmails);                
                
                CPWebAddress = URL.getSalesforceBaseUrl().toExternalForm()+'/'+ cp.Id;
            	String clientName = cp.Name;
                
                constructRemainingMsg(singleMail, clientName, CPWebAddress);
            	emails.add(singleMail);     
            }
        }
        return emails;
    }
    
    private void constructRemainingMsg(Messaging.SingleEmailMessage sem, string clientName, string CPWebAddress) {
            sem.setSaveAsActivity(false);
            sem.setSubject('Emerging Risk Factor Identified - Please Review - '+ clientName);
            sem.setPlainTextBody('As a member of the client team for '+ clientName +', you are receiving this email '+
                                        'as a notification that an emerging risk factor has been identified on a call plan. Please '+
                                        'click on the link below to view the emerging risk factor information on the Call Plan.' + 
                                        '\n\n'+'Reminder - call plans may contain sensitive information and in cases of emerging ' +
                                        'risks, the information should be held strictly confidential. Should you have any questions ' +
                                        'about the emerging risks, please contact the associate that entered the information in the ' +
                                        'call plan.'+'\n\n'+'This emerging risk identification email can also be forwarded to other ' +
                                        'members of either the First or Second Lines of Defense as needed.'+'\n\n\n'+ CPWebAddress +'\n\n'+
                                        'OneView Administrator');
    }
    
    private void sendEmails() {
        String EmailFailure;
		List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        List<Participant__c> p = new List<Participant__c>();
        
        p.addAll(EmailParticipantMap.keyset());
        if(emails.size() < 100) {
            try {
                emails = EmailParticipantMap.values();
                Messaging.sendEmail(emails);
                for(Participant__c pts : p) { pts.Email_Sent__c = true; }
            }
            catch(System.EmailException emlEx) {
                EmailFailure = 'Email Failed: ' + emlEx;
                for(Participant__c pts : p) { pts.Email_Sent__c = false; }
            }
        }
         update p;
    }
}