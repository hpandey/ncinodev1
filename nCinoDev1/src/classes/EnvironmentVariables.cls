/************************************************************************************
 Apex Class Name     : environmentVariables
 Version             : 1.0
 Created Date        : 19th Feb 2016
 Function            : Class to store static Final values of variables used in environment.
 Author              : Sumit Sagar/Pranil Thubrikar 
 Modification Log    :
* Developer                             Date                   Description
* ----------------------------------------------------------------------------                 
* Sumit Sagar/Pranil Thubrikar         2/19/2016                Original Version
*************************************************************************************/

public Class EnvironmentVariables{

    public Static Final String loanAPI = 'LLC_BI__Loan__c';
    public Static Final String entityInvolvementAPI = 'LLC_BI__Legal_Entities__c';
    public Static Final String loanDocumentAPI = 'LLC_BI__LLC_LoanDocument__c';
    public Static Final String accountAPI = 'Account';
    public Static Final String AsyncProcessLogAPI = 'Asynchronous_Process_Log__c';
    public Static Final String SPACE = ' ';
    public Static Final String NULL_Value = '';
    public Static Final String COMMA_WITH_SPACE = ' , ';
    public Static Final String COMMA = ',';
    public Static Final String SELECT_CONSTANT = 'select';
    public Static Final String FROM_CONSTANT = 'from';
    public Static Final String WHERE_CONSTANT = 'where';
    public Static Final String ID = 'id';
    public Static Final String EQUAL = ' = ';
    public Static Final String EQUALWITHCOLON = ' =: ';
    public Static Final Integer ZERO = 0;
    public Static Final Integer QueueJobTransactionLimit = 50;
    public Static Final String RECORD_ID = 'idRecord';
    
    public Static Final Integer STATUS_200 = 200;
    public Static Final Integer STATUS_201 = 200;

    public Static Final String DATATYPE_DATE                ='Date';
    public Static Final String DATATYPE_DATETIME            ='DateTime'; 
    public Static Final String DATATYPE_INTEGER             ='Integer';
    public Static Final String DATATYPE_STRING              ='String';
    public Static Final String DATATYPE_BOOLEAN             ='Boolean';
    public Static Final String DATATYPE_DOUBLE              ='Double';
    public Static Final String DATATYPE_PICKLIST            ='Picklist';
    public Static Final String DATATYPE_TEXTAREA            ='Textarea';
    public Static Final String DATATYPE_MULTIPICKLIST       ='Multipicklist';
    public Static Final String DATATYPE_PERCENT             ='Percent';
    public Static Final String DATATYPE_EMAIL               ='Email';
    public Static Final String DATATYPE_PHONE               ='Phone';
    public Static Final String DATATYPE_CURRENCY            ='Currency';

    public Static Final String OPERTATOR_EQUALSTO               = 'Equal To';
    public Static Final String OPERTATOR_NOTEQUALTO             = 'Not Equal To';
    public Static Final String OPERTATOR_GREATERTHAN            = 'Greater Than';
    public Static Final String OPERTATOR_GREATERTHANOREQUALTO   = 'Greater Than Or Equal To';
    public Static Final String OPERTATOR_LESSTHAN               = 'Less Than';
    public Static Final String OPERTATOR_LESSTHANOREQUALTO      = 'Less Than Or Equal To';
    public Static Final String OPERTATOR_CONTAINS               = 'Contains';
    public Static Final String OPERTATOR_DOESNOTCONTAIN         = 'Does Not Contain';

    
}