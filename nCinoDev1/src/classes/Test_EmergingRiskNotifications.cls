/**=====================================================================
 * Appirio, Inc
 * Name: Test_EmergingRiskNotifications
 * Description: Story S-271020  Task # T-335124 Test class
 * for REB_EmergingRiskNotifications
 *              
 * Created Date: 12/6/2014
 * Created By: Rita Washington (Appirio)
 =====================================================================*/
@isTest
public class Test_EmergingRiskNotifications {
    public static Call_Plan__c cp = new Call_Plan__c();
    
    static testmethod void erNotifyTest() {
        createTestData();
        REB_EmergingRiskNotifications eRN = new REB_EmergingRiskNotifications();
        eRN.SendERnotifications(cp);
        Risk_Notification_Recipient__c r = getRiskNotification();
        system.assert(r.Email_Sent__c = true);
    }
    
    private static void createTestData() {
        User u = createUser(true);
        
        Account acct = new Account(name='Acme');
        insert acct;
        
        Contact cont = new Contact(FirstName = 'John', LastName = 'Smith', Phone = '111-111-1111', AccountId = acct.Id);
        insert cont;
        
        cp.Client__c = acct.Id;
        cp.Contact__c = cont.Id;
        cp.Call_Plan_Type__c = 'Face to Face';
        cp.Status__c = 'Planning';
        cp.Joint_Call2__c = 'No';
        cp.Changes_Business_or_Financial_Profile__c = 'Yes';
        insert cp;
        
        Participant__c p = new Participant__c(Call_Plan__c = cp.Id, Role__c = 'Area VP');
        insert p;
        
        Risk_Notification_Recipient__c r = new Risk_Notification_Recipient__c(Call_Plan__c = cp.Id,
                                                                              Recipient__c = u.Id,
                                                                              Email_Sent__c = false);
        insert r;
    }
    
    public static User createUser(boolean isInsert)
    {
        List<Profile> profileList = [select id from Profile where Name='System Administrator' limit 1];
        Id profileId;
        User usr;
        if(profileList.get(0) != null)
        {
            profileId = profileList.get(0).Id;    
        }
        
        usr= new User();
        usr.Email              = 'test'+ Integer.valueOf(Math.random()) +'@example.com';
        usr.Username           = 'test' + Integer.valueOf(Math.random()) + '@testuser1.com';
        usr.LastName           = 'test' ;
        usr.Alias              = 'test' ;
        usr.ProfileId          = profileId ;
        usr.LanguageLocaleKey  = 'en_US';
        usr.LocaleSidKey       = 'en_US';
        usr.TimeZoneSidKey     = 'America/Chicago';
        usr.EmailEncodingKey   = 'UTF-8';
        if(isInsert) insert usr;
        return usr ;
    }    
    
    private static Risk_Notification_Recipient__c getRiskNotification(){
        
        Risk_Notification_Recipient__c rn = [Select r.Id, r.Email_Sent__c, r.Call_Plan__c From Risk_Notification_Recipient__c r
                                             Where r.Call_Plan__c =: cp.Id];
        return rn;
        
    }
        
        

}