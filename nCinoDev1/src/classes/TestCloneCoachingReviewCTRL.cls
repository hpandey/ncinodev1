@isTest
public class TestCloneCoachingReviewCTRL {

    static testMethod void cloneReview() {
    
        Profile p = [select id from profile where name='Regions Standard User'];
        User u2 = new User(alias = 'zRM123', email= 'zRM1234@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id, country='United States',
          timezonesidkey='America/Los_Angeles', username= 'zrTest123@testorg.com');
        Insert u2;
    
        Coaching_Review__c cr = new Coaching_Review__c();
        cr.Coaching_Date__c = date.today();
        cr.Reminder_Date__c = date.today(); 
        cr.Relationship_Manager__c = u2.id;
        Insert cr;
        
        Coaching_Area__c ca1 = new Coaching_Area__c();
        ca1.Area_of_Development__c = 'Sales';
        ca1.Focus_Area__c = 'Call Planning';
        ca1.Comments__c = 'Coaching area 1 sales';
        ca1.Coaching_Review__c = cr.id;
        insert ca1;   
    
        Coaching_Area__c ca2 = new Coaching_Area__c();
        ca2.Area_of_Development__c = 'Leadership';
        ca2.Focus_Area__c = 'Communication';
        ca2.Comments__c = 'Coaching area 2 leadership';
        ca2.Results__c = 'Coaching area 2 results';
        ca2.Coaching_Review__c = cr.id;
        insert ca2;   
        
        PageReference clonePage = Page.cloneCoachingReview;
        Test.setCurrentPageReference(clonePage); 
        clonePage.getParameters().put('id', cr.id);
                                                    
        ApexPages.standardController controller = new ApexPages.standardController(cr);
        cloneCoachingReviewCTRL cloneCTRL = new cloneCoachingReviewCTRL(controller); 
        cloneCTRL.cloneReview();

        Coaching_Review__c cr_clone = [Select id, Coaching_Date__c, Reminder_Date__c
                                         From Coaching_Review__c
                                         Where Relationship_Manager__c = :u2.id
                                           and id <> :cr.id];
        System.AssertEquals(cr_clone.Coaching_Date__c, cr.Coaching_Date__c.addMonths(1));
        System.AssertEquals(cr_clone.Reminder_Date__c, cr.Reminder_Date__c.addMonths(1));
        
        List <Coaching_Area__c> clone_areas = [Select id, completed__c from coaching_area__c 
                                                where coaching_review__c = :cr_clone.id];
        System.AssertEquals(clone_areas.size(),1);
    
    
    
    
    }



}