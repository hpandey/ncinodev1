/************************************************************************************
 Apex Class Name     : RestrictedClientInfoTriggerHandler
 Version             : 1.0
 Created Date        : 26th Jan 2016
 Function            : Actual Trigger Code for Restricted_Client_Information__c Object
 Author              : Ansuman Patnaik (apatnaik@deloitte.com),Jag Valaiyapathy (jvalaiyapathy@deloitte.com)
 Credits             : Chris Aldridge (Original Author)
 Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------------                 
* Jag Valaiyapathy          1/26/2016                Original Version
*************************************************************************************/

public class RestrictedClientInfoTriggerHandler implements ITriggerHandler {

    
    public void BeforeInsert(List<sObject> newItems){

    }   

    public void BeforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems){

    }

    public void BeforeDelete(Map<Id, sObject> oldItems){

    }

    public void AfterInsert(Map<Id, sObject> newItems){
        
        map<String,Id> mapAccntIdRCIId = new map<String,Id>();
        List<Account> lstUpdatedAccounts = new List<Account>();
        Integer QUERY_ROW_LIMIT = Limits.getDMLRows();
        /*
           Collect Clientids from the newly inserted Restricted Client Infomation rows
        */
        for(Sobject RCI:newItems.values())
        {
            mapAccntIDRCIId.put((String)RCI.get('Client__c'),(ID)RCI.get('id'));
        }
        //Query Client records using the clientid list above
        map<Id,Account> mapAccntIDAccnt = new map<Id,Account>([SELECT 
                            Restricted_Client_Information__c, 
                            Name 
                        FROM 
                            Account
                        WHERE
                            id IN :mapAccntIDRCIId.keySet()     
                        LIMIT :QUERY_ROW_LIMIT]);
        /*
            Iterate through the Accounts queried above and update the Restricted Client Information look up field
        */                  
        for(ID acctId:mapAccntIdRCIId.keySet()){
            Account tempAccnt = (Account)mapAccntIDAccnt.get(acctid);
            tempAccnt.Restricted_Client_Information__c = mapAccntIDRCIId.get(acctid);
            lstUpdatedAccounts.add(tempAccnt);
        }

        update lstUpdatedAccounts;
    }

    public void AfterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems){

    }

    public void AfterDelete(Map<Id, sObject> oldItems){

    }

    public void AfterUndelete(Map<Id, sObject> oldItems){

    }

}