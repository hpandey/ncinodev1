/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testPortManTrigger {

    static testMethod void myUnitTest() {
        test.startTest();
        
        UserRole r = [SELECT Id,Name from UserRole where Name like '%Credit%' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        User usrOwner = new User(Alias = 'userown', Email='userown@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='TestingA', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, LOB__c='Treasury Management',
        TimeZoneSidKey='America/Los_Angeles', UserName='userown@testorg.com');
        insert usrOwner;
        
        User usrCU = new User(Alias = 'usercu', Email='usercu@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='TestingA', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, LOB__c='Treasury Management',
        TimeZoneSidKey='America/Los_Angeles', UserName='usercu@testorg.com',
        UserRole = r, ManagerId = usrOwner.Id);
        insert usrCU;
        
        Portfolio_Management__c testPM = new Portfolio_Management__c();
        testPM.OwnerId = usrOwner.Id;
        testPM.Credit_Underwriter__c = usrCU.Id;
        testPM.Status__c = 'Draft';
        insert testPM;
        
        List<Portfolio_Management__Share> lstShares = new List<Portfolio_Management__Share>([Select Id from Portfolio_Management__Share where ParentId=:testPM.Id]);
        system.assertEquals(1,lstShares.size());
        
        testPM.Status__c = 'Final';
        update testPM;
        lstShares = [Select Id from Portfolio_Management__Share where ParentId=:testPM.Id];
        system.assertEquals(2,lstShares.size());
        
        testPM.Status__c = 'Draft';
        update testPM;
        lstShares = [Select Id from Portfolio_Management__Share where ParentId=:testPM.Id];
        system.assertEquals(1,lstShares.size());
        
        test.stopTest();
    }
}