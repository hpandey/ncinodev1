public with sharing class CoachingReviewExtension{
    private final Coaching_Review__c cr;
    public User owner {get;set;}
    public List<Coaching_Area__c> cas {get;set;}
    
    public CoachingReviewExtension(ApexPages.StandardController controller) {
        this.cr = (Coaching_Review__c)controller.getRecord();
        owner = [SELECT Name,LOB__c,C_I_Type__c,Region__c,Area__c 
                    FROM User 
                    WHERE ID=:cr.Relationship_Manager__c LIMIT 1];
        cas = [SELECT Id,Name,Area_of_Development__c, Focus_Area__c, Comments__c, Next_Steps__c, Results__c 
               FROM Coaching_Area__c 
               WHERE Coaching_Review__c=:cr.Id
               ORDER BY Area_of_Development__c];
    }
}