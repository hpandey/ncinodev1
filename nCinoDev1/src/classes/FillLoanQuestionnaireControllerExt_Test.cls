/******************************************************************************************************************
* Name            :             FillLoanQuestionnaireControllerExt_Test
* Author          :             Deloitte Consulting
* Description     :             This is a test class created for FillLoanQuestionnaireControllerExt
*******************************************************************************************************************/

@isTest(SeeAllData=false) // See all data is set to false
public class FillLoanQuestionnaireControllerExt_Test{

    @testSetup
    static void createData(){
        LLC_BI__Loan__c loan = TestDataFactory.createLoanWithoutID();//create Loan Record
        loan.LLC_BI__CRA_Type_Code__c ='Small business'; // set the business value
        if(loan!=null)
            insert loan;//insert loan
        System.assert(loan!=null); //Checking if loan is inserted successfully
        TestDataFactory.createQuestionnaireRecords(); //create Questionnaire Records
    }//end method
    
     /*Method Name    :       fillQuestionnaire_SaveNewQuestions
     *Author          :       Deloitte Consulting
     *Test Case       :       Test method for Saving new Questionnaires
    */
    private testMethod static void fillQuestionnaire_SaveNewQuestions(){
        test.startTest();
        //get the loan record
        LLC_BI__Loan__c loan = TestDataFactory.getLoanRecords('Small business'); // Insert Loan records
        System.assert(loan!=null); //Checking if loan record exists
        List<Questionnaire__c> question = TestDataFactory.getQuestionnaireRecords('Small business'); // Insert Questionnaire records
        System.assert(question.size()>0); //Checking if questionnaire records exists
         
        ApexPages.Standardcontroller loanQuestion = new ApexPages.Standardcontroller(loan);
        ApexPages.currentPage().getParameters().put('id', loan.id); // Pass Loan ID
        ApexPages.currentPage().getParameters().put('type', loan.LLC_BI__CRA_Type_Code__c ); // Pass loan type
        
        //Check to see if the parameters are set
        System.assert(ApexPages.currentPage().getParameters().get('id')!=null); 
        System.assert(ApexPages.currentPage().getParameters().get('type')!=null); 
        
        FillLoanQuestionnaireControllerExt ext = new FillLoanQuestionnaireControllerExt();
        Integer beforeSize = ext.lstQuestionnaireWrapper.size(); // get the list size before answering parent question
        //setting the answer for a parent question so that child Question is added to the list
        for(FillLoanQuestionnaireControllerExt.QuestionnaireWrapper q:ext.lstQuestionnaireWrapper){
            if(q.qn.id==question[8].Parent_Question__c){
                q.Answer ='Fair';
            }//end if
        }//end for
        ext.parentQuestionID = question[8].Parent_Question__c; // pass the parent record id 
        
        System.assert(ext.parentQuestionID!=null); //Checking if the parent Id assigned is not null
        
        ext.buildDependentQuestionnaireMap(); //Once the parent answer is set check for child questions and add to the list
        System.assertEquals(ext.lstQuestionnaireWrapper.size(),beforeSize+2); // Checking if 2 child questions are added to the list
        ext.cancel();
        ext.save(); // Save the Questionnaire records
        
        System.assert(ext.save()!=null); //Checking if the save method executed successfully without any exceptions
        
        test.stopTest();
    }//end method
    
    /*Method Name    :        fillQuestionnaire_ReloadLoanQuestions
     *Author          :       Deloitte Consulting
     *Test Case       :       Test method for Reloading Questions
    */
    private testMethod static void fillQuestionnaire_ReloadLoanQuestions(){
        test.startTest();
        LLC_BI__Loan__c loan = TestDataFactory.getLoanRecords('Small business'); // Insert Loan records
        System.assert(loan!=null); //Checking if loan record exists
        List<Questionnaire__c> question = TestDataFactory.getQuestionnaireRecords('Small business'); // Insert Questionnaire records
        System.assert(question.size()>0); //Checking if questionnaire records exists
        
        List<Loan_Questionnaire__c> lstLoanQuestion = TestDataFactory.createLoanQuestionnaireRecords(question,loan); // Insert Loan Questionnaire records
        
        System.assert(lstLoanQuestion.size()>0); //Checking if loanquestionnaire records exists
        
        ApexPages.Standardcontroller loanQuestion = new ApexPages.Standardcontroller(loan);
        ApexPages.currentPage().getParameters().put('id', loan.id); // Pass Loan Id
        ApexPages.currentPage().getParameters().put('type', loan.LLC_BI__CRA_Type_Code__c ); // Pass Loan type
        
        //Check to see if the parameters are set
        System.assert(ApexPages.currentPage().getParameters().get('id')!=null); 
        System.assert(ApexPages.currentPage().getParameters().get('type')!=null); 
        
        FillLoanQuestionnaireControllerExt ext = new FillLoanQuestionnaireControllerExt();
        for(Questionnaire__c q:question){ // Set the Required values to true
            q.Required__c=true;
        }//end for
        if(question!=null && question.size()>0)
            update question;
        ext.save();// Save Questionniare
        
        System.assert(ext.save()!=null); //Checking if the save method executed successfully without any exceptions
        
        ext.deleteOldRecords(); // reload questions
        // Set the answers to null and call the save method so that required field validation is called.
        for(FillLoanQuestionnaireControllerExt.QuestionnaireWrapper q:ext.lstQuestionnaireWrapper){
            q.Answer =null;
            q.Answers =null;
        }//end for
        //update question;
        ext.save();
        System.assert(ext.save()==null); //Checking if the save method executed with an error
        
        test.stopTest();
    }//end method
    
    /*Method Name    :        fillQuestionnaire_UpateLoanQuestionnaire
     *Author          :       Deloitte Consulting
     *Test Case       :       Test method for Updating Loan Questionnaire
    */
    private testMethod static void fillQuestionnaire_UpateLoanQuestionnaire()
    {
        test.startTest();
        LLC_BI__Loan__c loan = TestDataFactory.getLoanRecords('Small business'); // Insert Loan record
        System.assert(loan!=null); //Checking if loan record exists
        List<Questionnaire__c> question = TestDataFactory.getQuestionnaireRecords('Small business'); //get Questionnaire records
        System.assert(question.size()>0); //Checking if questionnaire records exists
        List<Loan_Questionnaire__c> lstLoanQuestion = TestDataFactory.createLoanQuestionnaireRecords(question,loan); // create Loan questionnaire records
        System.assert(lstLoanQuestion.size()>0); //Checking if loanquestionnaire records exists
        
        ApexPages.Standardcontroller loanQuestion = new ApexPages.Standardcontroller(loan);
        ApexPages.currentPage().getParameters().put('id', loan.id);
        ApexPages.currentPage().getParameters().put('type', loan.LLC_BI__CRA_Type_Code__c );
        
        //Check to see if the parameters are set
        System.assert(ApexPages.currentPage().getParameters().get('id')!=null); 
        System.assert(ApexPages.currentPage().getParameters().get('type')!=null);
        
        FillLoanQuestionnaireControllerExt ext = new FillLoanQuestionnaireControllerExt();
        
        //iterate over the wrapper and update the answers
        for(FillLoanQuestionnaireControllerExt.QuestionnaireWrapper q:ext.lstQuestionnaireWrapper){
            // Update the answer
            q.Answer ='Test'; 
            List<String> lstanswers = new List<String>();
            lstanswers.add('Test1');
            lstanswers.add('Test2');
            q.Answers = lstanswers;
        }//end for
        ext.save(); // Save the questions
        System.assert(ext.save()!=null); //Checking if the save method executed with an error
        
        test.stopTest();
    }//end method
}//end class