global class docManRemoter{

    public String accountName { get; set; }
    public static Account account { get; set; }
    
    public docManRemoter(ApexPages.StandardController controller){}
    
    @RemoteAction
    global static Boolean testRemote(String fileName, String docStoreId) {
        system.debug('##### fileName : '+fileName);
        system.debug('##### docStoreId : '+docStoreId);
        system.debug('##### fileName_2 : '+ fileName.substringAfterLast('.'));
        if(fileName.substringAfterLast('.') == 'txt'){
            return true;
        }
        else{
            return false;
        }
    }
    
    @RemoteAction
    global static String testRemote2(String docId) {
        String strParentId;
        system.debug('##### docId : '+docId);
        if(docId != null && docId != ''){
            strParentId = [SELECT Id, parentId FROM Attachment WHERE ID = :docId].parentId;
            if(strParentId != null){
                system.debug('##### parentId : '+strParentId);
                return strParentId;
            }
        }
        return null;
    }
}