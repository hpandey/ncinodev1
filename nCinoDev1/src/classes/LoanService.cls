/************************************************************************************
 Apex Class Name     : LoanService
 Version             : 1.0
 Created Date        : 16th Feb 2016
 Function            : Helper Class for LoanTriggerHandler. This Class will have all the logic.
 Author              : Deloitte Consulting
 Modification Log    :
* Developer                         Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar                 2/16/2016                Original Version
*************************************************************************************/

public Class LoanService {
    
    /************************************************************************
    Method Name: getRelatedOpportunity
    Author Name: Pranil Thubrikar
    Description: Method to get Opportunities related to Loans.
    Parameters: List<LLC_BI__Loan__c> listLoan
    Returns: List<Opportunity>
    *************************************************************************/
    private List<Opportunity> getRelatedOpportunity(List<LLC_BI__Loan__c> listLoan) {
        
        Set<Id> setOpptyIds = new Set<Id>();
        List<Opportunity> listRelatedOpportunity = new List<Opportunity>();
        
        if(!listLoan.isEmpty()){
            for(LLC_BI__Loan__c loan : listLoan){
                if(loan.Opportunity__c != null){
                    setOpptyIds.add(loan.Opportunity__c);
                }
            }
        }
        
        if(!setOpptyIds.isEmpty()){
            try{
                listRelatedOpportunity = [SELECT Id, (SELECT Id 
                                                      FROM OpportunityLineItems 
                                                      LIMIT 1)
                                          FROM Opportunity
                                          WHERE Id IN :setOpptyIds];
            }
            catch(QueryException e){
                UtilityClass.throwExceptions(e);
                throw new LoanServiceException(Label.Loan_Oppty_Sync_Error + ' : '+e.getMessage());
            }
        }
        return listRelatedOpportunity;
    }
    
    /************************************************************************
    Method Name: getLoanToPipelineFieldMapRecs
    Author Name: Pranil Thubrikar
    Description: Method to get Field Map records for Loan to Pipeline Sync.
    Parameters: 
    Returns: List<LLC_BI__Field_Map__c>
    *************************************************************************/
    public List<LLC_BI__Field_Map__c> getLoanToPipelineFieldMapRecs() {
        List<LLC_BI__Field_Map__c> listFieldMap = new List<LLC_BI__Field_Map__c>();
        try{
            listFieldMap = [SELECT Id, Name, LLC_BI__Orig_Obj_Field__c, LLC_BI__Target_Obj_Field__c 
                            FROM LLC_BI__Field_Map__c 
                            WHERE Name = :Label.LoanToPipelineSync OR Name = :Label.LoanToProductSync];
        }
        catch(QueryException e){
            UtilityClass.throwExceptions(e);
            throw new LoanServiceException(Label.Loan_Oppty_Sync_Error + ' : '+e.getMessage());
        }
        return listFieldMap;
    }
    
    /************************************************************************
    Method Name: loanToPiplineSync
    Author Name: Sumit Sagar/Pranil Thubrikar
    Description: Method to sync Loans data in Pipeline when Loan is updated.
    Parameters: List<LLC_BI__Loan__c> listLoan
    Returns: void
    *************************************************************************/
    public List<sObject> loanToPiplineSync(List<LLC_BI__Loan__c> listLoan) {
        
        List<LLC_BI__Field_Map__c> listFieldMap = new List<LLC_BI__Field_Map__c>(); 
        Map<Id, Opportunity> mapRelatedOpportunity;
        Map<Id, Opportunity> mapOpportunityWithProducts;
        List<sObject> listRecsToBeUpdated = new List<sObject>();
        List<Opportunity> listRelatedOppty = new List<Opportunity>();
        
        //Get the Field Map records.
        listFieldMap = getLoanToPipelineFieldMapRecs();
        
        if(listFieldMap != null){
            if(!listLoan.isEmpty() && !listFieldMap.isEmpty()){
                //Get Related Opportunities.
                listRelatedOppty = getRelatedOpportunity(listLoan);
                mapRelatedOpportunity = new Map <Id, Opportunity>(listRelatedOppty);
                if(!mapRelatedOpportunity.isEmpty()){
                    for(LLC_BI__Loan__c new_loan : listLoan){
                        try{
                            if(new_loan.Opportunity__c != null){
                                Opportunity opp = mapRelatedOpportunity.get(new_loan.Opportunity__c);
                                OpportunityLineItem productRec;
                                if(opp.OpportunityLineItems[0] != null){
                                    productRec = opp.OpportunityLineItems[0];
                                }
                                //update the Opportunity and Product fields.
                                for(LLC_BI__Field_Map__c fieldMapRec : listFieldMap){
                                    if(fieldMapRec.Name == Label.LoanToPipelineSync){
                                        opp.put(fieldMapRec.LLC_BI__Target_Obj_Field__c, new_loan.get(fieldMapRec.LLC_BI__Orig_Obj_Field__c));
                                    }
                                    else if(fieldMapRec.Name == Label.LoanToProductSync){
                                        productRec.put(fieldMapRec.LLC_BI__Target_Obj_Field__c, new_loan.get(fieldMapRec.LLC_BI__Orig_Obj_Field__c));
                                    }
                                }
                                if(opp.OpportunityLineItems[0] != null){
                                    listRecsToBeUpdated.add(productRec);
                                }
                                listRecsToBeUpdated.add(opp);
                            }
                        }
                        catch(SObjectException e){
                            UtilityClass.throwExceptions(e);
                            new_loan.addError(Label.Loan_Oppty_Sync_Error + ' : '+e.getMessage());
                        }
                    }
                    if(!listRecsToBeUpdated.isEmpty()){
                        //Sort sObject List.
                        listRecsToBeUpdated.sort();
                    }
                }
            }
        }
        return listRecsToBeUpdated;
    }
    
    /************************************************************************
    Method Name: evaluatePrescreenRules
    Author Name: Pranil Thubrikar
    Description: Method to create Prescreening Result Record for the respective Entity Involvement records related to Loan.
    Parameters: List<LLC_BI__Loan__c> listLoan
    Returns: List <Regulatory_and_Fraud_Indicators__c>
    *************************************************************************/
    public List <Regulatory_and_Fraud_Indicators__c> evaluatePrescreenRules(List<LLC_BI__Loan__c> listLoan){
        
        List<LLC_BI__Legal_Entities__c> listRelatedEntities = new List<LLC_BI__Legal_Entities__c>();
        List<LLC_BI__Legal_Entities__c> listFilteredEntities = new List<LLC_BI__Legal_Entities__c>();
        List<Regulatory_and_Fraud_Indicators__c> listResultInsert = new List<Regulatory_and_Fraud_Indicators__c>();
        List<Regulatory_and_Fraud_Indicators__c> listFinalResultInsert = new List<Regulatory_and_Fraud_Indicators__c>();
        
        if(!listLoan.isEmpty()){
            //Get related Entity Involvement Records.
            listRelatedEntities = getRelatedEntityInvolvementDetails(listLoan);
            if(!listRelatedEntities.isEmpty()){
                EntityInvolvementService entityInvolvementServiceObj = new EntityInvolvementService();
                //Filter the Entity records for whom Prescreening Result is already present.
                //listFilteredEntities = entityInvolvementServiceObj.filterRelatedEntities(listRelatedEntities);
                //if(!listFilteredEntities.isEmpty())
                if(!listRelatedEntities.isEmpty()){
                    //Create Prescreening Records.
                    listResultInsert = entityInvolvementServiceObj.evaluatePrescreenRules(listRelatedEntities);
                    if(listResultInsert != null && !listResultInsert.isEmpty()){
                        for(Regulatory_and_Fraud_Indicators__c prescreenRec : listResultInsert){
                            if((prescreenRec.Flag_Type__c == Label.Vermont || prescreenRec.Flag_Type__c == Label.Out_of_Market) && prescreenRec.Result__c == Label.Failed){
                                listFinalResultInsert.add(prescreenRec);
                            }
                        }
                    }
                }
            }
        }
        //Returning the list in Handler Class.
        return listFinalResultInsert;
    }
    
    
    /************************************************************************
    Method Name: getRelatedEntityInvolvementDetails
    Author Name: Pranil Thubrikar
    Description: Method to  Entity Involvement records details related to Loan.
    Parameters: List<LLC_BI__Loan__c> listLoan
    Returns: List<LLC_BI__Legal_Entities__c>
    *************************************************************************/
    public List<LLC_BI__Legal_Entities__c> getRelatedEntityInvolvementDetails(List<LLC_BI__Loan__c> listLoan){
        
        Set<Id> setLoanIds = new Set<Id>();
        Set<Id> setEntityId = new Set<Id>();
        List<LLC_BI__Legal_Entities__c> listRelatedEntities = new List<LLC_BI__Legal_Entities__c>();
        
        if(!listLoan.isEmpty()){
            setLoanIds = new map<id,LLC_BI__Loan__c>(listLoan).keySet();
        }

        if(!setLoanIds.isEmpty()){
            try{
                //Get related Entity Involvement records.
                if(setEntityId != null){
                    listRelatedEntities = [SELECT Id, LLC_BI__Account__c, LLC_BI__Loan__c
                                           FROM LLC_BI__Legal_Entities__c 
                                           WHERE LLC_BI__Loan__c IN :setLoanIds];
                }
            }
            catch(QueryException e){
                utilityClass.throwExceptions(e);
                throw new LoanServiceException(Label.Prescreening_Insert_Error + ' : '+e.getMessage());
            }
        }
        return listRelatedEntities;
    }
    
    
    /************************************************************************
    Method Name: getPrescreeningResultsToDelete
    Author Name: Pranil Thubrikar
    Description: Method which returns Prescreening Records to Delete after Account in updated.
    Parameters: List <Regulatory_and_Fraud_Indicators__c>listOldResult, List <Regulatory_and_Fraud_Indicators__c>listNewResult
    Returns: List <Regulatory_and_Fraud_Indicators__c>
    *************************************************************************/
    public List <Regulatory_and_Fraud_Indicators__c> getPrescreeningResultsToDelete(List <Regulatory_and_Fraud_Indicators__c>listOldResult){
        
        List <Regulatory_and_Fraud_Indicators__c> listResultToDelete = new List <Regulatory_and_Fraud_Indicators__c>();
//Code Review Comment - Move 'Rejected' to Custom Label       
        if(listOldResult != null && !listOldResult.isEmpty()){
            for(Regulatory_and_Fraud_Indicators__c oldResult : listOldResult){
                if(oldResult.Result__c != Label.Passed && oldResult.Result__c !=Label.Rejected){
                    listResultToDelete.add(oldResult);
                } 
            }
        }
        return listResultToDelete;
    }
    
    /************************************************************************
    Method Name: getPrescreeningResultsToInsert
    Author Name: Pranil Thubrikar
    Description: Method which returns Prescreening Records to Insert after Account address is changed.
    Parameters: List <Regulatory_and_Fraud_Indicators__c>listOldResult, List <Regulatory_and_Fraud_Indicators__c>listNewResult
    Returns: List <Regulatory_and_Fraud_Indicators__c>
    *************************************************************************/
    public List <Regulatory_and_Fraud_Indicators__c> getPrescreeningResultsToInsert(List <Regulatory_and_Fraud_Indicators__c>listNewResult){
    //Code Review Comment - added comment to EntityInvolvementService.getRelatedPrescreeningResults() method to return a map. so instead of lstOldResult the
    //arguement to this method would be a map. this way we wont have to have for inside for      
        List <Regulatory_and_Fraud_Indicators__c> listResultToInsert = new List <Regulatory_and_Fraud_Indicators__c>();
        if(listNewResult != null && !listNewResult.isEmpty()){
            for(Regulatory_and_Fraud_Indicators__c newResult : listNewResult){
                if(newResult.Result__c == Label.Failed){
                    listResultToInsert.add(newResult);
                }
            }
        }
        return listResultToInsert;
    }
    
    /************************************************************************
    Method Name: updatePrescreeningRuleLastRunTime
    Author Name: Pranil Thubrikar
    Description: Method to update Loan field with the datetime at which the Prescreening Rule ran.
    Parameters: List <Id>listLoanId
    Returns: Void
    *************************************************************************/

    public void updatePrescreeningRuleLastRunTime(List<Id>listLoanId){
        
        Set<Id> setLoanIds = new Set<Id>();
        List<LLC_BI__Loan__c> listLoan = new List<LLC_BI__Loan__c>();
        
        if(listLoanId != null && !listLoanId.isEmpty() ){
            setLoanIds.addAll(listLoanId);
            if(!setLoanIds.isEmpty()){
                for(String loanId : setLoanIds){
                    LLC_BI__Loan__c loanRec = new LLC_BI__Loan__c(Id = loanId);
                    loanRec.Prescreening_Rule_Last_Run__c = System.Now();
                    listLoan.add(loanRec);
                }
                if(!listLoan.isEmpty()){
                    List<Database.SaveResult> listSaveResult = Database.Update(listLoan,false);
                    if(!listSaveResult.isEmpty()){
                        //Add Error when Loan update is failed.
                        for(Database.SaveResult Result : listSaveResult){
                            if(!Result.isSuccess()){
                                throw new LoanServiceException(Label.Prescreening_Insert_Error + ' : ' + String.join(Result.getErrors(),', '));
                            }
                        }
                    }
                }
            }
        }
    }
    
    /************************************************************************
    Method Name: getPrescreeningResultsToInsert
    Author Name: Pranil Thubrikar
    Description: Method which returns Prescreening Records to Insert after Account address is changed.
    Parameters: List <Regulatory_and_Fraud_Indicators__c>listOldResult, List <Regulatory_and_Fraud_Indicators__c>listNewResult
    Returns: List <Regulatory_and_Fraud_Indicators__c>
    *************************************************************************/
    public List<LLC_BI__LLC_LoanDocument__c> getRelatedLoanDocWithoutGUID(List<LLC_BI__Loan__c> listLoan){
        
        List<LLC_BI__LLC_LoanDocument__c> listLoanDoc = new List<LLC_BI__LLC_LoanDocument__c>();
        
        if(listLoan != null && !listLoan.isEmpty()){
            listLoanDoc = [SELECT Id 
                           FROM LLC_BI__LLC_LoanDocument__c
                           WHERE LLC_BI__Loan__c IN : new Map<Id,LLC_BI__Loan__c>(listLoan).keySet()
                             AND LLC_BI__reviewStatus__c = 'Rejected'];
            
            if(!listLoanDoc.isEmpty()){
                for(LLC_BI__LLC_LoanDocument__c loanDocRec : listLoanDoc){
                    loanDocRec.LLC_BI__reviewStatus__c = 'Approved';
                }
            }
        }
        return listLoanDoc;
    }
    
    /************************************************************************
    Method Name: evaluateChecksAndFlags
    Author Name: Pranil Thubrikar
    Description: Method which returns Asynchronous Queue records.
    Parameters: List<LLC_BI__Loan__c> listLoan
    Returns: List<Asynchronous_Queue__c>
    *************************************************************************/
    public List<Asynchronous_Queue__c> evaluateChecksAndFlags(List<LLC_BI__Loan__c> listLoan){

        List<Asynchronous_Queue__c> listAsyncQueue = new List<Asynchronous_Queue__c>();
        List<LLC_BI__Legal_Entities__c> listRelatedEntities = new List<LLC_BI__Legal_Entities__c>();
        
        if(listLoan != null && !listLoan.isEmpty()){
            listRelatedEntities = getRelatedEntityInvolvementDetails(listLoan);
            if(listRelatedEntities != null && !listRelatedEntities.isEmpty()){
                EntityInvolvementService entityInvolvementServiceObj = new EntityInvolvementService();
                //listAsyncQueue = entityInvolvementServiceObj.createAsyncQueueRecs(listRelatedEntities);
                listAsyncQueue = entityInvolvementServiceObj.createAsyncQueueRecords(listRelatedEntities , 'CheckAndFlagsIntegrationService');
            }
        }
        return listAsyncQueue;
    }
    
    /************************************************************************
    Method Name: evaluateCreditBureau
    Author Name: Sumit Sagar
    Description: Method which returns Asynchronous Queue records for Credit Bureau Integration Service.
    Parameters: List<LLC_BI__Loan__c> listLoan
    Returns: List<Asynchronous_Queue__c>
    *************************************************************************/
    public List<Asynchronous_Queue__c> evaluateCreditBureau(List<LLC_BI__Loan__c> listLoan){

        List<Asynchronous_Queue__c> listAsynchronousQueue = new List<Asynchronous_Queue__c>();
        List<LLC_BI__Legal_Entities__c> listRelatedEntities = new List<LLC_BI__Legal_Entities__c>();
        List<LLC_BI__Legal_Entities__c> listFilteredRelatedEntities = new List<LLC_BI__Legal_Entities__c>();
        list<Credit_Result__c> listCreditResult = new list<Credit_Result__c>();
        map<String , Credit_Result__c> mapCreditResultByEntityId = new map<String , Credit_Result__c>();
        
        system.debug('listLoan@@@@'+listLoan);
        if(listLoan != null && !listLoan.isEmpty()){
            listRelatedEntities = getRelatedEntityInvolvementDetails(listLoan);
            System.debug('listRelatedEntities@@@'+listRelatedEntities);
            if(listRelatedEntities != null && !listRelatedEntities.isEmpty()){
                /*
                listCreditResult = [select 
                                  id , Customer_ID__c , Credit_Bureau__c, Date_of_Report__c
                                  from Credit_Result__c
                                  //where Customer_ID__c IN: listRelatedEntities.id
                                  ];
                                                         
                if(!listCreditResult.isEmpty()) {
                    for(Credit_Result__c objCreditResult : listCreditResult) {
                        mapCreditResultByEntityId.put(objCreditResult.Customer_ID__c , objCreditResult);
                    }
                }
                
                // Removing Entities from list for which Credit score has been pulled recently.
                for(LLC_BI__Legal_Entities__c objRelatedEntities : listRelatedEntities) {
                    if(mapCreditResultByEntityId.ContainsKey(objRelatedEntities.id)) {
                        //integer intLatestPull = system.today().daysBetween(mapCreditResultByEntityId.get(objRelatedEntities.id).Date_of_Report__c);
                        if(system.today().daysBetween(mapCreditResultByEntityId.get(objRelatedEntities.id).Date_of_Report__c) > 15) {
                            listFilteredRelatedEntities.add(objRelatedEntities);
                        }
                    }
                    else{
                        listFilteredRelatedEntities.add(objRelatedEntities);
                    }   
                }
                
                //create Async Records for Filtered Entities 
                if(!listFilteredRelatedEntities.isEmpty()) {*/
                    EntityInvolvementService entityInvolvementServiceObj = new EntityInvolvementService();
                    listAsynchronousQueue = entityInvolvementServiceObj.createAsyncQueueRecords(listRelatedEntities , 'CreditBureauIntegrationService');
                    system.debug('listAsynchronousQueue@@##'+listAsynchronousQueue);
                //}       
            }
        }
        return listAsynchronousQueue;
    }
    
    /************************************************************************
    Method Name: getEntityIdWithLoan
    Author Name: Pranil Thubrikar
    Description: Method which returns Map of Entity Involvement ID with Loan ID.
    Parameters: List<LLC_BI__Loan__c> listLoan
    Returns: Map<Id, Id>
    *************************************************************************/
    public Map<Id, Id> getEntityIdWithLoanId(List<LLC_BI__Loan__c>listLoan){
        
        Map<Id, Id> mapEntityIdWithLoan = new Map<Id, Id> ();
        List<LLC_BI__Legal_Entities__c> listRelatedEntities = new List<LLC_BI__Legal_Entities__c>();
        
        if(listLoan != null && !listLoan.isEmpty()){
            listRelatedEntities = getRelatedEntityInvolvementDetails(listLoan);
            if(listRelatedEntities != null && !listRelatedEntities.isEmpty()){
                for(LLC_BI__Legal_Entities__c legalEntity : listRelatedEntities){
                    mapEntityIdWithLoan.put(legalEntity.Id, legalEntity.LLC_BI__Loan__c);
                }
            }
        }
        return mapEntityIdWithLoan;
    }
    
    /************************************************************************
    Method Name: getLoanIdWithProductPackage
    Author Name: Pranil Thubrikar
    Description: Method which returns Map of Loan Id with Product Package Id.
    Parameters: List<Id> listLoanId
    Returns: Map<Id, Id>
    *************************************************************************/
    public Map<Id, Id> getLoanIdWithProductPackage(List<Id>listLoanId){
        
        Map<Id, Id> mapEntityIdWithLoan = new Map<Id, Id> ();
        
        if(listLoanId != null && !listLoanId.isEmpty()){
            for(LLC_BI__Loan__c loanRec : [SELECT Id, LLC_BI__Product_Package__c  FROM LLC_BI__Loan__c WHERE Id IN :listLoanId]){
                mapEntityIdWithLoan.put(loanRec.Id, loanRec.LLC_BI__Product_Package__c);
            }
        }
        return mapEntityIdWithLoan;
    }
    
    // Custom Exception for LoanService class
    public class LoanServiceException extends Exception { }
}