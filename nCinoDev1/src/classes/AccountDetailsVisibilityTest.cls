@isTest(SeeAllData=false)
public class AccountDetailsVisibilityTest{
    
    public testmethod static void testing(){
        Account acct= new Account(name='testAccount');
        
        insert acct;
        
        Profile p=[select id from Profile where name='Standard User'];
        User testUser= new User(Username='testtest@testingtestng.com', LastName='test', Email='test@test.com', Alias='tes', CommunityNickname='tes',ProfileID=p.ID,TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', LanguageLocaleKey='en_US',EmailEncodingKey='ISO-8859-1');
        
        insert testUser;
        
        AccountTeamMember atm= new AccountTeamMember(accountId=acct.id,userId=testUser.id);
        
        insert atm;
        
        AccountDetailsVisibilityUtil.recalculateSharing(acct.id);
         
        Bank_Account_Details__c bad= new Bank_Account_Details__c(client__c=acct.id);
        
        insert bad; 
        
        Restricted_Client_Information__c rci= new Restricted_Client_Information__c(client__c=acct.id);
        
        insert rci;
        
        AccountDetailsVisibilityUtil.recalculateSharingOnSubmit(acct.id);
        // commented by sendeep for test failure on 4/10/2013
        System.AssertEquals([select id from Bank_Account_Details__Share where UserOrGroupID=: testUser.id].isEmpty(),false);
        System.AssertEquals([select id from Restricted_Client_Information__Share where UserOrGroupID=: testUser.id].isEmpty(),false);
        List<Relationship__c> rel_List = new List<Relationship__c>();
        Relationship__c testrelation = new Relationship__c(name='Test relationship');
        insert testrelation;
        rel_List.add(testrelation);
        
        AccountDetailsVisibilityUtil.recalculateUserSharing(rel_List);
        
        delete atm;
        
        AccountDetailsVisibilityUtil.recalculateSharing(acct.id);
        System.AssertEquals([select id from Bank_Account_Details__Share where UserOrGroupID=: testUser.id].isEmpty(),true);
        System.AssertEquals([select id from Restricted_Client_Information__Share where UserOrGroupID=: testUser.id].isEmpty(),true);
        
      /*  
        AccountDetailsVisibilityBatch batch2= new AccountDetailsVisibilityBatch(null);
        AccountDetailsVisibilityBatch batch= new AccountDetailsVisibilityBatch();
        Id ProcessId= Database.ExecuteBatch(batch);
        */
    }
}