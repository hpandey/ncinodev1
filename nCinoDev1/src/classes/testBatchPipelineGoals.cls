/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testBatchPipelineGoals {

    static testMethod void myUnitTest() {
        test.startTest();
        
        date myDate = date.today();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        User testUser = new User(Alias = 'tester', Email='tester@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='TestingA', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, LOB__c='Private Wealth Management',
        TimeZoneSidKey='America/Los_Angeles', UserName='tester@testorg.com');
        insert testUser;
        
        Goal__c testGoal1 = new Goal__c();
        testGoal1.User__c = testUser.Id;
        testGoal1.Production_Total_Goal__c = 1;
        testGoal1.Time_Period__c = 'Monthly';
        testGoal1.Start_Date__c = myDate.addMonths(-1);
        testGoal1.End_Date__c = myDate.addMonths(1);
        insert testGoal1;
        
        Goal__c testGoal2 = new Goal__c();
        testGoal2.User__c = testUser.Id;
        testGoal2.Production_Total_Goal__c = 1;
        testGoal2.Time_Period__c = 'Annual';
        testGoal2.Start_Date__c = myDate.addMonths(-1);
        testGoal2.End_Date__c = myDate.addMonths(2);
        insert testGoal2;
        
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.OwnerId = testUser.Id;
        insert testAcc;
        
        Opportunity testPipeline = new Opportunity();
        testPipeline.CloseDate = myDate.addMonths(1);
        testPipeline.StageName = 'Accepted';
        testPipeline.Probability = 90.0;
        testPipeline.Name = 'Test Pipeline';
        testPipeline.AccountId = testAcc.Id;
        insert testPipeline;
        
        OpportunityTeamMember testOTM = new OpportunityTeamMember();
        testOTM.UserId = testUser.Id;
        testOTM.OpportunityId = testPipeline.Id;
        insert testOTM;

        List<Pipeline_Goal__c> lstAssertPipelineGoals = new List<Pipeline_Goal__c>();
        
        lstAssertPipelineGoals = [select id from Pipeline_Goal__c];
        system.assertEquals(2,lstAssertPipelineGoals.size());
        
        batchPipelineGoals controller = new batchPipelineGoals();
        database.executebatch(controller);
        
        test.stopTest();
        
        lstAssertPipelineGoals = [select id from Pipeline_Goal__c];
        system.assertEquals(2,lstAssertPipelineGoals.size());
    }
}