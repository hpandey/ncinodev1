/************************************************************************************
 Apex Class Name     : LoanTriggerHandler
 Version             : 1.0
 Created Date        : 16th Feb 2016
 Function            : Handler Class for Loan Trigger. This Class will not have any logic and will call methods in Helper Class.
 Author              : Deloitte Consulting
 Modification Log    :
* Developer                             Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar         2/16/2016                Original Version
*************************************************************************************/

public Class LoanTriggerHandler implements ITriggerHandler {
    
    //Create Object for Service Class to call appropriate methods.
    LoanService loanServiceObj = new LoanService();

    //Handler Method for Before Insert.
    public void BeforeInsert(List<sObject> newItems) {
    }
    
    //Handler Method for Before Update.
    public void BeforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
    }
    
    //Handler Method for Before Delete.    
    public void BeforeDelete(Map<Id, sObject> oldItems) {
    }
    
    //Handler method for After Insert.
    public void AfterInsert(Map<Id, sObject> newItems) {
    }

    //Handler method for After Update.    
    public void AfterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {

        List<LLC_BI__Field_Map__c> listFieldMap = new List<LLC_BI__Field_Map__c>(); //Variable to store the field maps created for Loan to Pipeline Sync functionality.
        List<LLC_BI__Loan__c> listLoan = new List<LLC_BI__Loan__c>();
        List<LLC_BI__Loan__c> listFilteredLoan = new List<LLC_BI__Loan__c>();
        List<LLC_BI__Loan__c> listChecksAndFlagsLoan = new List<LLC_BI__Loan__c>();
        List<LLC_BI__Loan__c> listFileNetLoans = new List<LLC_BI__Loan__c>();
        List<LLC_BI__Loan__c> listCreditBureauLoans = new list<LLC_BI__Loan__c>();
        List<Regulatory_and_Fraud_Indicators__c> listResultInsert = new List<Regulatory_and_Fraud_Indicators__c>();
        List<LLC_BI__LLC_LoanDocument__c> listLoanDocToUpdate = new List<LLC_BI__LLC_LoanDocument__c>();
        Map<Id, Id> mapEntityIdWithLoanId = new Map<Id, Id>();
        List<Asynchronous_Queue__c> listAsyncQueueToInsert = new List<Asynchronous_Queue__c>();
        List<Asynchronous_Queue__c> listAsyncQueueCreditBureau = new List<Asynchronous_Queue__c>();
        
        //Get the Field Map records.
        listFieldMap = loanServiceObj.getLoanToPipelineFieldMapRecs();
        
        // variable to store all stages for which prescreening rules need to be processed
        Set<String> setApplicableStages = new Set<String>();

        //Get loan stages for which prescreen rules needs to be executed from custom label
        if(Label.Loan_Stages_Prescreening != null && String.isNotBlank(Label.Loan_Stages_Prescreening)) {
            setApplicableStages.addAll(Label.Loan_Stages_Prescreening.split(';'));
        }
        else{
            String errMsg = 'Custom Label for Loan Stages is Null or Empty. Please contact System Administrator.';
            UtilityClass.throwCustomLabelException(Label.Prescreening_Insert_Error, errMsg);
        }
        
        //Variable to store stages for Checks & Flag Integration.
        Set<String> setCheckAndFlagsStages = new Set<String>();
        
        //Get Loan Stages for Checks & Flags Integration.
        if(Label.Checks_FlagsLoanStages != null && String.isNotBlank(Label.Checks_FlagsLoanStages)){
            setCheckAndFlagsStages.addAll(Label.Checks_FlagsLoanStages.split(';'));
        }
        else{
            String errMsg = 'Custom Label for Loan Stages is Null or Empty. Please contact System Administrator.';
            UtilityClass.throwCustomLabelException(Label.AsyncQueueError, errMsg);
        }
        
        for(LLC_BI__Loan__c newLoan : (List<LLC_BI__Loan__c>)newItems.values()){
            //Filter for Prescreening Results.
            if(setApplicableStages != null && !setApplicableStages.isEmpty() && setApplicableStages.contains(newLoan.LLC_BI__Stage__c) 
                    && newLoan.LLC_BI__Stage__c != ((LLC_BI__Loan__c)oldItems.get(newLoan.Id)).LLC_BI__Stage__c) {

                listLoan.add(newLoan);
            }
            //Filter for Loan To Pipeline Sync.
            if(listFieldMap != null && !listFieldMap.isEmpty()){
                if(newLoan.Is_Converted__c && newLoan.Opportunity__c != null){
                    for(LLC_BI__Field_Map__c fieldMapRec : listFieldMap){
                        //Check to compare new value of field with old value.
                        if(newLoan.get(fieldMapRec.LLC_BI__Orig_Obj_Field__c) != ((LLC_BI__Loan__c)oldItems.get(newLoan.Id)).get(fieldMapRec.LLC_BI__Orig_Obj_Field__c)){
                            listFilteredLoan.add(newLoan);
                            break;
                        }
                    }
                }
            }
            //Filter for Checks & Flags Integration
            if(newLoan.LLC_BI__Stage__c != ((LLC_BI__Loan__c)oldItems.get(newLoan.Id)).LLC_BI__Stage__c && setCheckAndFlagsStages.contains(newLoan.LLC_BI__Stage__c)){
                listChecksAndFlagsLoan.add(newLoan);
            }
            //Filter for FileNet Integration
            /**if(newLoan.LLC_BI__Stage__c == 'Booked'){//To be updated
                listFileNetLoans.add(newLoan);
            }**/

            //Filter Conditions for CreditBureau Integration
            if(newLoan.LLC_BI__Stage__c == Label.Loan_Stage_Application_Processing && newLoan.LLC_BI__Status__c == Label.Loan_Status_Fulfilled &&
               (newLoan.LLC_BI__Stage__c != ((LLC_BI__Loan__c)oldItems.get(newLoan.Id)).LLC_BI__Stage__c || newLoan.LLC_BI__Status__c != ((LLC_BI__Loan__c)oldItems.get(newLoan.Id)).LLC_BI__Status__c) &&
               newLoan.Prescreen_Results_Failure_Count__c == 0) {
                
                System.debug('I am In @@');
                listCreditBureauLoans.add(newLoan);
            }
        }
        //Code for Loan To Pipeline Sync.
        if(!listFilteredLoan.isEmpty()){
            List<Database.SaveResult> listSaveResult = new List<Database.SaveResult>();
            Map<Id,sObject> mapRecsToBeUpdated;
            //Get the Opportunity and Products records to be updated.
            List<sObject> listRecsToBeUpdated = loanServiceObj.loanToPiplineSync(listFilteredLoan);
            if(listRecsToBeUpdated != null){
                mapRecsToBeUpdated = new Map<Id, sObject>(listRecsToBeUpdated);
                //Update Opportunity and Product records.
                if(!mapRecsToBeUpdated.values().isEmpty()){
                    listSaveResult = Database.Update(mapRecsToBeUpdated.values(),false);
                    //Add Error on Loan if there is error updating Opportunity or Products.
                    if(!listSaveResult.isEmpty()){
                        for(Database.SaveResult result : listSaveResult){
                            if(!result.isSuccess()){
                               mapRecsToBeUpdated.get(result.getId()).addError(Label.Loan_Oppty_Sync_Error + ' : '+ UtilityClass.getErrorMessages(result.getErrors())); 
                            }
                        }
                    }
                }
            }
        }
        //Code for Prescreening Result.
        if(!listLoan.isEmpty()){
            //Get new Prescreening Result Records.
            listResultInsert = loanServiceObj.evaluatePrescreenRules(listLoan);
            if(listResultInsert != null && !listResultInsert.isEmpty()){
                //Insert Prescreening Result.
                List<Database.SaveResult> listSaveResult = Database.Insert(listResultInsert,false);
                if(!listSaveResult.isEmpty()){
                    //Add Error on List Records if Prescreening Result records are not created.
                    for(Integer index = 0; index < listResultInsert.size(); index++){
                        if(!listSaveResult[index].isSuccess()){
                            if(listResultInsert[index].Loan__c != null){
                                newItems.get(listResultInsert[index].Loan__c).addError(Label.Prescreening_Insert_Error+ ' : '+UtilityClass.getErrorMessages(listSaveResult[index].getErrors()));
                            }
                        }
                    }
                    utilityClass.processSaveResult(listSaveResult);
                }
            }
        }
        //Code for Checks & Flags Integration
        if(!listChecksAndFlagsLoan.isEmpty()){
            listAsyncQueueToInsert = loanServiceObj.evaluateChecksAndFlags(listChecksAndFlagsLoan);
            if(listAsyncQueueToInsert != null && !listAsyncQueueToInsert.isEmpty()){
                mapEntityIdWithLoanId = loanServiceObj.getEntityIdWithLoanId(listChecksAndFlagsLoan);
                if(mapEntityIdWithLoanId != null && !mapEntityIdWithLoanId.isEmpty()){
                    List<Database.SaveResult> listSaveResult = Database.Insert(listAsyncQueueToInsert,false);
                    if(!listSaveResult.isEmpty()){
                        //Add Error on List Records if Prescreening Result records are not created.
                        for(Integer index = 0; index < listAsyncQueueToInsert.size(); index++){
                            if(!listSaveResult[index].isSuccess()){
                                if(listAsyncQueueToInsert[index].Record_Ids__c != null && mapEntityIdWithLoanId.get(listAsyncQueueToInsert[index].Record_Ids__c) != null){
                                    newItems.get(mapEntityIdWithLoanId.get(listAsyncQueueToInsert[index].Record_Ids__c)).addError(Label.AsyncQueueError +' : '+UtilityClass.getErrorMessages(listSaveResult[index].getErrors()));
                                }
                            }
                        }
                        utilityClass.processSaveResult(listSaveResult);
                    }
                }
            }
        }
        
        //Code for CreditBureau Integration
        if(!listCreditBureauLoans.isEmpty()) {
            system.debug('listCreditBureauLoans@@@@'+listCreditBureauLoans);
            listAsyncQueueCreditBureau = loanServiceObj.evaluateCreditBureau(listCreditBureauLoans);
            system.debug('listAsyncQueueCreditBureau@@@@'+listAsyncQueueCreditBureau);
            if(!listAsyncQueueCreditBureau.isEmpty()) {
                List<Database.SaveResult> listSaveResult = Database.Insert(listAsyncQueueCreditBureau , false);
                system.debug('listSaveResult@@'+listSaveResult);
                utilityClass.processSaveResult(listSaveResult);    
            }
        }

        //Code for FileNet Integration
        /**if(!listFileNetLoans.isEmpty()){
            listLoanDocToUpdate = loanServiceObj.getRelatedLoanDocWithoutGUID(listFileNetLoans);
            
            if(listLoanDocToUpdate != null && !listLoanDocToUpdate.isEmpty()){
                List<Database.SaveResult> listSaveResult; //= Database.Update(listLoanDocToUpdate,false);
                if(!listSaveResult.isEmpty()){
                    for(Integer index = 0; index < listLoanDocToUpdate.size(); index++){
                        if(!listSaveResult[index].isSuccess()){
                            if(listLoanDocToUpdate[index].LLC_BI__Loan__c != null){
                                newItems.get(listLoanDocToUpdate[index].LLC_BI__Loan__c).addError(' Error updating Loan Document Records : '+UtilityClass.getErrorMessages(listSaveResult[index].getErrors()));
                            }
                        }
                    }
                    utilityClass.processSaveResult(listSaveResult);
                }
            }
        }**/
    }

    //Code for

    //Handler method for After Delete    
    public void AfterDelete(Map<Id, sObject> oldItems) {
    }

    //Handler method for After Undelete.
    public void AfterUndelete(Map<Id, sObject> oldItems) {
    }
}