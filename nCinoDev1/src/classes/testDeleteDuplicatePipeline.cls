//------------------------------------------------------------------------------------------------------------------------------------------
//Test Class for deleteDuplicatePipeline class and opportunitytrigger...
//Case 1853 - Remove duplicate pipelines automatically where Stage is 'Closed Lost' and Reason Lost is 'Duplicate Pipeline' 
//and Pipeline Verification is not blank...
//Ephrem Tekle - 10-2015
//------------------------------------------------------------------------------------------------------------------------------------------

@isTest
private class testDeleteDuplicatePipeline {
    static testmethod void testDeleteDuplicate(){
        Test.startTest();
                
        Date oppDate = date.today();
        Date oppCloseDate = oppDate.addDays(2);
        
        Account acc = new Account(Name='Test Account');
        insert acc;
                
        Opportunity opp1 = new Opportunity(Name='Test Opp', closedate=oppCloseDate, stagename='Prospecting', accountid=acc.id);
        insert opp1;
        
        Opportunity opp2 = new Opportunity(Name='Duplicate Opp1', closedate=oppCloseDate, stagename='Closed Lost', Reason_Lost__c = 'Duplicate Pipeline', accountid=acc.id, Pipeline_Verification__c = opp1.Id, Confirm_Duplicate__c = true);
        insert opp2;
        
        Opportunity opp3 = new Opportunity(Name='Duplicate Opp2', closedate=oppCloseDate, stagename='Closed Lost', Reason_Lost__c = 'Duplicate Pipeline', accountid=acc.id, Pipeline_Verification__c = opp1.Id, Confirm_Duplicate__c = true);
        insert opp3;
                    
        List<Opportunity> o1 = new List<Opportunity>();
        system.assertEquals(o1.size(), 0);
        List<Opportunity> o2 = [SELECT id, StageName, Reason_Lost__c, Pipeline_Verification__c from opportunity where StageName = 'Closed Lost' and Reason_Lost__c = 'Duplicate Pipeline' and Pipeline_Verification__c <> null];
        system.assertEquals(o2.size(), 2);
        
        o1.add(o2[0]);
        system.assertEquals(o1.size(), 1);
        delete o1;
        
        Test.stopTest();
    }   
}