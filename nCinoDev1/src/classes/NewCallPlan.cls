public with sharing class NewCallPlan {
     ApexPages.StandardController controller;
     String CallPlanName;
     
     public NewCallPlan(ApexPages.StandardController con){
        controller = con;
     }
     
     public PageReference save() {
        //Attempt to Save Call Plan and catch any validation exceptions 
        try{
            controller.save();
        } catch (DmlException ex) {
            ApexPages.addMessages(ex);
        }
        // Check for exception messages if none build url to direct to record select type page for add new participant 
        if (ApexPages.hasMessages()) {
            return null;
        } else {
             string ObjName = [Select Name from Call_Plan__c where id = :controller.getId()].name;
             CallPlanName = ObjName;        
             PageReference p;
             String s = '/setup/ui/recordtypeselect.jsp?ent=01IG0000002FqKg&retURL=%2F'+controller.getId()+'&save_new_url=%2Fa0B%2Fe%3FCF00NG0000009YY6u%3D'+CallPlanName+'%26CF00NG0000009YY6u_lkid%3D'+controller.getId()+'%26retURL%3D%252F'+controller.getId();
             p = new PageReference(s); 
             return p;
        }
     } 
}