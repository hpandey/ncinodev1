/************************************************************************************
 Apex Class Name     : EntityInvolvementService
 Version             : 1.0
 Created Date        : 01st Mar 2016
 Function            : Helper Class for EntityInvolvementTriggerHandler. This Class will have all the logic.
 Author              : Deloitte Consulting
 Modification Log    :
* Developer                         Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar                 3/01/2016                Original Version
*************************************************************************************/

public Class EntityInvolvementService{

    /************************************************************************
    Method Name: getPrescreenResultsToDelete
    Author Name: Pranil Thubrikar
    Description: Method to delete Prescreening Result Record when Entity Involment is deleted on Loan/Account.
    Parameters: List<LLC_BI__Legal_Entities__c> listOldEntities
    Returns: List<Regulatory_and_Fraud_Indicators__c>
    *************************************************************************/
    public List<Regulatory_and_Fraud_Indicators__c> getPrescreenResultsToDelete(List<LLC_BI__Legal_Entities__c> listOldEntities){
        List<Regulatory_and_Fraud_Indicators__c> listResultDelete = new List<Regulatory_and_Fraud_Indicators__c>();
        Set<Id> setOldEntitiesId = new Set<Id>();
        
        if(!listOldEntities.isEmpty()){
            for(LLC_BI__Legal_Entities__c deletedEntity : listOldEntities){
                setOldEntitiesId.add(deletedEntity.Id);
            }
            if(!setOldEntitiesId.isEmpty()){
                //Query Prescreening Result Records to delete.
                try{
                    listResultDelete = [SELECT Id 
                                        FROM Regulatory_and_Fraud_Indicators__c
                                        WHERE Entity__c IN :setOldEntitiesId];
                }
                catch(QueryException e){
                    utilityClass.throwExceptions(e);
                    throw new EntityInvolvementServiceException(Label.Prescreening_Insert_Error + ' : '+e.getMessage());
                }
            }
        }
        //Return Prescreening Result Records to delete in Handler Class.
        return listResultDelete;
    }
    
    /************************************************************************
    Method Name: evaluatePrescreenRules
    Author Name: Pranil Thubrikar
    Description: Method to create new Prescreening Result Record when Entity Involment is update/created on Loan/Account.
    Parameters: List<LLC_BI__Legal_Entities__c> listOldEntities
    Returns: List<Regulatory_and_Fraud_Indicators__c>
    *************************************************************************/
    public List<Regulatory_and_Fraud_Indicators__c> evaluatePrescreenRules(List<LLC_BI__Legal_Entities__c> listNewEntities){

        Map<LLC_BI__Legal_Entities__c, Id> mapEntityWithLoanId = new Map<LLC_BI__Legal_Entities__c, Id>();
        Map<Id, Account> mapEntityIdWithAccount = new Map<Id, Account>();
        List<Regulatory_and_Fraud_Indicators__c> listNewPrescreeingResults = new List<Regulatory_and_Fraud_Indicators__c>();
        List<Regulatory_and_Fraud_Indicators__c> listTempPrescreeingResults = new List<Regulatory_and_Fraud_Indicators__c>();
        Map<Id, Id> mapLoanIdWithProductPackageId = new Map<Id, Id>();
        List<Id> listLoanIds = new List<Id>();
        
        if(!listNewEntities.isEmpty()){
            AccountService accServiceObj = new AccountService();
            LoanService loanServiceObj = new LoanService();
            
            for(LLC_BI__Legal_Entities__c newEntity : listNewEntities){
                if(newEntity.LLC_BI__Loan__c != null){
                    mapEntityWithLoanId.put(newEntity, newEntity.LLC_BI__Loan__c);
                }
            }
            System.debug('####listNewEntities'+listNewEntities);

            if(!mapEntityWithLoanId.keySet().isEmpty()){
                //Get details of Account related to Entity Involvement Records.
                mapEntityIdWithAccount = getRelatedAccountDetails(new List<LLC_BI__Legal_Entities__c>(mapEntityWithLoanId.keySet()));
                //Get Product Package related to Loan.
                mapLoanIdWithProductPackageId = loanServiceObj.getLoanIdWithProductPackage(mapEntityWithLoanId.values());
                for(LLC_BI__Legal_Entities__c newEntity : mapEntityWithLoanId.keySet()){
                    if(mapEntityIdWithAccount.get(newEntity.Id) != null){
                        listTempPrescreeingResults = accServiceObj.evaluatePrescreenRules(new List<Account>{mapEntityIdWithAccount.get(newEntity.Id)});
                        if(listTempPrescreeingResults!= null){
                            if(!listTempPrescreeingResults.isEmpty()){
                                for(Regulatory_and_Fraud_Indicators__c prescreenResult : listTempPrescreeingResults){
                                    prescreenResult.Entity__c = newEntity.Id;
                                    prescreenResult.Loan__c = mapEntityWithLoanId.get(newEntity);
                                    if(mapLoanIdWithProductPackageId.get(mapEntityWithLoanId.get(newEntity)) != null){
                                        prescreenResult.Product_Package__c = mapLoanIdWithProductPackageId.get(mapEntityWithLoanId.get(newEntity));
                                    }
                                    listNewPrescreeingResults.add(prescreenResult);
                                    listLoanIds.add(mapEntityWithLoanId.get(newEntity));
                                }
                            }
                        }
                    }
                }
                if(!listLoanIds.isEmpty()){
                    loanServiceObj.updatePrescreeningRuleLastRunTime(listLoanIds);
                }
            }
        }
        System.debug('####listNewPrescreeingResults'+listNewPrescreeingResults);
        return listNewPrescreeingResults;
    }
    
    /************************************************************************
    Method Name: getRelatedAccountDetails
    Author Name: Pranil Thubrikar
    Description: Method to related account details by passing Entity Involvement records.
    Parameters: List<LLC_BI__Legal_Entities__c> listOldEntities
    Returns: Map<Id,Account>
    *************************************************************************/
    private Map<Id,Account> getRelatedAccountDetails(List<LLC_BI__Legal_Entities__c> listNewEntities){
        
        Set<Id> setAccountIds = new Set<Id>();
        Map<Id, Account> mapAccounts = new Map<Id, Account>();
        Map<Id, Account> mapEntityIdWithAccount = new Map<Id, Account>();
        
        if(!listNewEntities.isEmpty()){
            for(LLC_BI__Legal_Entities__c newEntity : listNewEntities){
                if(newEntity.LLC_BI__Account__c != null){
                    setAccountIds.add(newEntity.LLC_BI__Account__c);    
                }
            }
            
            if(!setAccountIds.isEmpty()){
                //Query the related Account records.
                try{
                    mapAccounts = new Map<Id, Account>([SELECT Id, Is_Individual__c, OOM_Primary_Flag__c , OOM_Secondary_Flag__c , OOM_Rule_Flag__c,
                                                               OOC_Primary_Flag__c, OOC_Secondary_Flag__c, OOC_Rule_Flag__c,
                                                               Vermont_Primary_Flag__c , Vermont_Secondary_Flag__c , Vermont_Rule_Flag__c,
                                                               BillingCity, BillingCountry, BillingPostalCode, BillingState, BillingStreet,
                                                               ShippingCity, ShippingCountry, ShippingPostalCode, ShippingState, ShippingStreet
                                                        FROM Account 
                                                        WHERE Id IN :setAccountIds]);
                }
                catch(QueryException e){
                    utilityClass.throwExceptions(e);
                    throw new EntityInvolvementServiceException(Label.Prescreening_Insert_Error + ' : '+e.getMessage());
                }
                
                if(!mapAccounts.isEmpty()){
                    for(LLC_BI__Legal_Entities__c newEntity : listNewEntities){
                        if(mapAccounts.keySet().contains(newEntity.LLC_BI__Account__c)){
                            mapEntityIdWithAccount.put(newEntity.Id,mapAccounts.get(newEntity.LLC_BI__Account__c));
                        }
                    }
                }
            }
        }
        return mapEntityIdWithAccount;
    }
    
    /************************************************************************
    Method Name: getRelatedLoanDetails
    Author Name: Pranil Thubrikar
    Description: Method to related Loan details by passing Entity Involvement records.
    Parameters: List<LLC_BI__Legal_Entities__c> listNewEntities
    Returns: Map<Id,LLC_BI_Loan__c> 
    *************************************************************************/
    public Map<Id,LLC_BI__Loan__c> getRelatedLoanDetails(List<LLC_BI__Legal_Entities__c> listNewEntities){
    
        Set<Id> setLoanIds = new Set<Id>();
        List<LLC_BI__Loan__c> listLoans = new List<LLC_BI__Loan__c>();
        Map<Id, LLC_BI__Loan__c> mapEntityIdWithLoan = new Map<Id, LLC_BI__Loan__c>();
        
        if(!listNewEntities.isEmpty()){
            for(LLC_BI__Legal_Entities__c newEntity : listNewEntities){
                if(newEntity.LLC_BI__Loan__c != null){
                    setLoanIds.add(newEntity.LLC_BI__Loan__c);    
                }
            }
            
            if(!setLoanIds.isEmpty()){
                //Query the related Account records.
                try{
                    listLoans = [SELECT Id, LLC_BI__Stage__c
                                    FROM LLC_BI__Loan__c 
                                    WHERE Id IN :setLoanIds];
                }
                catch(QueryException e){
                    utilityClass.throwExceptions(e);
                    throw new EntityInvolvementServiceException(Label.Prescreening_Insert_Error + ' : '+e.getMessage());
                }
                
                if(!listLoans.isEmpty()){
                    for(LLC_BI__Legal_Entities__c newEntity : listNewEntities){
                        for(LLC_BI__Loan__c Loan : listLoans){
                            //Link Entity Involvement record with Account Record.
                            if(newEntity.LLC_BI__Loan__c == Loan.Id){
                                mapEntityIdWithLoan.put(newEntity.Id,Loan);
                            }
                        }
                    }
                }
            }
        }
        return mapEntityIdWithLoan;
    }
    
    /************************************************************************
    Method Name: getRelatedPrescreeningResults
    Author Name: Pranil Thubrikar
    Description: Method to get related Prescreening Result Records by passing Entity Involvement records.
    Parameters: List<LLC_BI__Legal_Entities__c> listEntities
    Returns: List<Regulatory_and_Fraud_Indicators__c>
    *************************************************************************/
    public List<Regulatory_and_Fraud_Indicators__c> getRelatedPrescreeningResults(List<LLC_BI__Legal_Entities__c> listEntities){
        
        List<Regulatory_and_Fraud_Indicators__c> listRelatedResults = new List<Regulatory_and_Fraud_Indicators__c>();
        Map<Id,List<Regulatory_and_Fraud_Indicators__c>>mapAccIdWithListRelatedResults = new Map<Id,List<Regulatory_and_Fraud_Indicators__c>>();
        ID strOOMRecType = Schema.SObjectType.Regulatory_and_Fraud_Indicators__c.getRecordTypeInfosByName().get(Label.Out_of_Market).getRecordTypeId();
        ID strVermontRecType = Schema.SObjectType.Regulatory_and_Fraud_Indicators__c.getRecordTypeInfosByName().get(Label.Vermont).getRecordTypeId();
        Map<Id,LLC_BI__Legal_Entities__c> mapEntities;
        Set<Id> setRecTypeId = new Set<Id>();

        if(listEntities != null && !listEntities.isEmpty()){
            if(strOOMRecType != null && String.isNotBlank(strOOMRecType)){
                setRecTypeId.add(strOOMRecType);
            }
            if(strVermontRecType != null && String.isNotBlank(strVermontRecType)){
                setRecTypeId.add(strVermontRecType);
            }
            mapEntities = new Map<Id,LLC_BI__Legal_Entities__c>(listEntities);
            if(!mapEntities.isEmpty() && !setRecTypeId.isEmpty()){
                try{
                    listRelatedResults = [SELECT Id, Customer__c, Entity__c, Flag_Type__c, Loan__c, Result__c
                                          FROM Regulatory_and_Fraud_Indicators__c
                                          WHERE Entity__c IN :mapEntities.keySet()
                                            AND RecordTypeId IN :setRecTypeId];
                }
                catch(QueryException e){
                    utilityClass.throwExceptions(e);
                    throw new EntityInvolvementServiceException(Label.Prescreening_Insert_Error + ' : '+e.getMessage());
                }
            }
        }
        return listRelatedResults;
    }
    
    /************************************************************************
    Method Name: createAsyncQueueRecs
    Author Name: Pranil Thubrikar
    Description: Method to create Async Queue records for respective entities.
    Parameters: List<LLC_BI__Legal_Entities__c> listNewEntities
    Returns: List<Asynchronous_Queue__c>
    *************************************************************************/
    /*public List<Asynchronous_Queue__c> createAsyncQueueRecs(List<LLC_BI__Legal_Entities__c> listEntities){
        
        List<Asynchronous_Queue__c> listAsyncQueueRecs = new List<Asynchronous_Queue__c>();
        
        if(listEntities != null && !listEntities.isEmpty()){
            for(LLC_BI__Legal_Entities__c legalEntity : listEntities){
                Asynchronous_Queue__c asyncRec = new Asynchronous_Queue__c();
                asyncRec.Record_Ids__c = legalEntity.Id;
                asyncRec.Functional_Class__c = 'CheckAndFlagsIntegrationService';
                asyncRec.Request_Type__c = 'Immediate With CallOut';
                listAsyncQueueRecs.add(asyncRec);
            }
        }
        return listAsyncQueueRecs;
    }*/
    
    /************************************************************************
    Method Name: createAsyncQueueRecords
    Author Name: Sumit Sagar
    Description: Method to create Async Queue records for respective entities.
    Parameters: List<LLC_BI__Legal_Entities__c> listNewEntities
    Returns: List<Asynchronous_Queue__c>
    *************************************************************************/
    public List<Asynchronous_Queue__c> createAsyncQueueRecords(List<LLC_BI__Legal_Entities__c> listEntities , String strFunctionClassName){
        
        List<Asynchronous_Queue__c> listAsynchronousQueue = new List<Asynchronous_Queue__c>();
        
        if(listEntities != null && !listEntities.isEmpty() && !String.isBlank(strFunctionClassName)){
            for(LLC_BI__Legal_Entities__c objlegalEntity : listEntities){
                Asynchronous_Queue__c objAsynchronousQueue = new Asynchronous_Queue__c();
                objAsynchronousQueue.Record_Ids__c = objlegalEntity.Id;
                objAsynchronousQueue.Functional_Class__c = strFunctionClassName;
                objAsynchronousQueue.Request_Type__c = 'Immediate With CallOut';
                listAsynchronousQueue.add(objAsynchronousQueue);
            }
        }
        return listAsynchronousQueue;
    }
    
    /************************************************************************
    Method Name: evaluateAsyncQueueRecs
    Author Name: Pranil Thubrikar
    Description: Method to evaluate Async Queue records for new entities.
    Parameters: List<LLC_BI__Legal_Entities__c> listEntities
    Returns: List<Asynchronous_Queue__c>
    *************************************************************************/
    public List<Asynchronous_Queue__c> evaluateAsyncQueueRecs(List<LLC_BI__Legal_Entities__c> listEntities){
        
        List<LLC_BI__Legal_Entities__c> listNewEntities = new List<LLC_BI__Legal_Entities__c>();
        List<Asynchronous_Queue__c> listAsyncQueueInsert = new List<Asynchronous_Queue__c>();
        Set<String> setCheckAndFlagsStages = new Set<String>();
        
        //Get Loan Stages for Checks & Flags Integration.
        if(Label.Checks_FlagsLoanStages != null && String.isNotBlank(Label.Checks_FlagsLoanStages)){
            setCheckAndFlagsStages.addAll(Label.Checks_FlagsLoanStages.split(';'));
        }
        else{
            String errMsg = 'Custom Label for Loan Stages is Null or Empty. Please contact System Administrator.';
            UtilityClass.throwCustomLabelException(Label.AsyncQueueError, errMsg);
        }
        
        if(!setCheckAndFlagsStages.isEmpty()){
            listNewEntities = [SELECT Id
                               FROM LLC_BI__Legal_Entities__c
                               WHERE Id IN :new Map<Id, LLC_BI__Legal_Entities__c>(listEntities).keySet()
                                 AND LLC_BI__Loan__r.LLC_BI__Stage__c IN :setCheckAndFlagsStages];
                                 
            if(!listNewEntities.isEmpty()){
                listAsyncQueueInsert = createAsyncQueueRecords(listNewEntities , 'CheckAndFlagsIntegrationService');
            }
        }
        return listAsyncQueueInsert;
    }
    
    //Below functionality is not required as of now, hence commented. 16th March 2016 - Pranil
    /************************************************************************
    Method Name: filterRelatedEntities
    Author Name: Pranil Thubrikar
    Description: Method to filter the Entity records for which Prescreening Result are already present.
    Parameters: List<LLC_BI__Legal_Entities__c> listNewEntities
    Returns: List<LLC_BI__Legal_Entities__c>
    *************************************************************************/
    /**public List<LLC_BI__Legal_Entities__c> filterRelatedEntities(List<LLC_BI__Legal_Entities__c> listNewEntities){
        
        Set<Id> setLoanIds = new Set<Id>();
        List<Regulatory_and_Fraud_Indicators__c> listOldResults = new List<Regulatory_and_Fraud_Indicators__c>();
        Map<Id,LLC_BI__Legal_Entities__c> mapNewEntity = new Map<Id,LLC_BI__Legal_Entities__c>();
        
        if(!listNewEntities.isEmpty()){
            mapNewEntity = new map<Id,LLC_BI__Legal_Entities__c>(listNewEntities);
            
            for(LLC_BI__Legal_Entities__c newEntity : listNewEntities){
                if(newEntity.LLC_BI__Loan__c != null){
                    setLoanIds.add(newEntity.LLC_BI__Loan__c);
                }
            }
            if(!setLoanIds.isEmpty()){
                try{
                //Get the existing Prescreening Result records to avoid duplication.
                listOldResults = [SELECT Id, Entity__c 
                                  FROM Regulatory_and_Fraud_Indicators__c 
                                  WHERE Loan__c IN :setLoanIds AND Entity__c != null];
                }
                catch(QueryException e){
                    utilityClass.throwExceptions(e);
                    throw new EntityInvolvementServiceException(Label.Prescreening_Insert_Error + ' : '+e.getMessage());
                }
                if(!listOldResults.isEmpty()){
                    for(Regulatory_and_Fraud_Indicators__c psResult : listOldResults){
                        if(mapNewEntity.keySet().contains(psResult.Entity__c)){
                            mapNewEntity.remove(psResult.Entity__c);
                        }   
                    }
                }
            }
            return mapNewEntity.values();
        }
        return listNewEntities;
    }**/
    
    //Custom Exception for EntityInvolvementService class
    public class EntityInvolvementServiceException extends Exception { }    
}