public class portManTriggerHandler {
    public portManTriggerHandler(){
        //Handler
    }
    public void sharePortfolio(List<Portfolio_Management__c> NewRecords){
        // Create a new list of sharing objects for Job
        List<Portfolio_Management__Share> pmShrs = new List<Portfolio_Management__Share>();
        List<Portfolio_Management__Share> sharesToDelete = new List<Portfolio_Management__Share>();
        
        // Declare variable for participant sharing
        Portfolio_Management__Share pmShr;
        
        for(Portfolio_Management__c pm : NewRecords){
            if (pm.Status__c=='Final'){
                // Instantiate the sharing object
                pmShr = new Portfolio_Management__Share();
                
                // Set the ID of record being shared
                pmShr.ParentId = pm.Id;
                
                // Set the ID of user or group being granted access
                pmShr.UserOrGroupId = pm.Credit_Underwriter__c;
                
                // Set the access level
                pmShr.AccessLevel = 'read';
                
                // Set the Apex sharing reason for call plan participant
                pmShr.RowCause = Schema.Portfolio_Management__Share.RowCause.Portfolio_Final__c;
                
                // Add objects to list for insert
                pmShrs.add(pmShr);
            }else{
                LIST<Portfolio_Management__Share> delShare = [SELECT Id FROM Portfolio_Management__Share 
                                                                    WHERE ParentId = :pm.Id
                                                                    AND RowCause = :Schema.Portfolio_Management__Share.RowCause.Portfolio_Final__c];
                sharesToDelete.addAll(delShare);
            }
        }
        // Insert sharing records and capture save result 
        // The false parameter allows for partial processing if multiple records are passed 
        // into the operation 
        if(!pmShrs.isEmpty()){
            Database.SaveResult[] lsr = Database.insert(pmShrs,false);
        
            // Create counter
            Integer i=0;
            
            // Process the save results
            for(Database.SaveResult sr : lsr){
                if(!sr.isSuccess()){
                    // Get the first save result error
                    Database.Error err = sr.getErrors()[0];
                    
                    // Check if the error is related to a trivial access level
                    // Access levels equal or more permissive than the object's default 
                    // access level are not allowed. 
                    // These sharing records are not required and thus an insert exception is 
                    // acceptable. 
                    if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                                                   &&  err.getMessage().contains('AccessLevel'))){
                        // Throw an error when the error is not related to trivial access level.
                        trigger.newMap.get(pmShrs[i].ParentId).
                          addError(
                           'Unable to grant sharing access due to following exception: '
                           + err.getMessage());
                    }
                }
                i++;
            }
        }
        if(!sharesToDelete.isEmpty()){
            Database.Delete(sharesToDelete, false);
        }
    }
}