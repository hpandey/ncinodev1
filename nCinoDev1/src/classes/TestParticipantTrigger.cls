@isTest
private class TestParticipantTrigger {
    static testMethod void addItems() {
 
        Profile p = [select id from profile where name='Regions Standard User'];
        UserRole r = [SELECT Id, Name from userrole where name = 'Head of BSG'];
        User u1 = new User(alias = 'wTest123', email= 'wxTest123@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id, country='United States',
          timezonesidkey='America/Los_Angeles', username= 'wxTest123@testorg.com', UserRoleId=r.id);
        Insert u1;
        
    System.runAs(u1){
        Account acc = new Account(name='testAccount');
        Insert acc;

        Call_Plan__c cp = new Call_Plan__c(client__c=acc.id);
        Insert cp;

        // add participant
        Participant__c part1 = new Participant__c(Call_Plan__c = cp.id, name__c = u1.id);
        insert part1;

        // query the call plan to find out the total items now
        Call_Plan__c cp1 = [select participants__c from Call_Plan__c where Id = :cp.id];
        // assert that the count is one
        System.assertEquals(1,cp1.Participants__c);
        
        delete part1;
        
        Participant__c part2 = new Participant__c(Call_Plan__c = cp.id, name__c = u1.id);
        insert part2;
        
        try{
            delete cp;
        }catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Please delete participants before deleting Call Plan') ? true : false;
            system.assertEquals(expectedExceptionThrown,true);       
        } 
        
        delete part2;
        
        delete cp;
    }
    }
 
}