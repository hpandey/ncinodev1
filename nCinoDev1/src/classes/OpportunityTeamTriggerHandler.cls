public with sharing class OpportunityTeamTriggerHandler {
    public OpportunityTeamTriggerHandler(){
        //Handler
    }
    public void AfterInsert(List<OpportunityTeamMember> insertRecords){
        //-----------------------------------------------------------------------
        //Goals Opportunity Team Member insert - Jeff Hulslander 10/5/13
        //
        // PMartin Feb7 - only link open pipeline to current goals
        //-----------------------------------------------------------------------
        List <Id> lstUsers = new List<Id>();
        List <Id> lstOpportunities = new List<Id>();
        for(OpportunityTeamMember otm : insertRecords){
            lstUsers.add(otm.UserId);
            lstOpportunities.add(otm.OpportunityId);
        }
        Map<ID,Opportunity> opptyMap = new Map<ID,Opportunity>(); 
        for(Opportunity oppty : [SELECT id, CloseDate, IsClosed From Opportunity WHERE ID IN :lstOpportunities] ) {           
            opptyMap.put(oppty.Id, oppty);         
        }
        List<Goal__c> lstGoals = new List<Goal__c>([select id,Time_Period__c,User__c,Start_Date__c,End_Date__c 
                                                      from Goal__c 
                                                      where User__c in :lstUsers and start_date__c < TODAY]);
        List<Pipeline_Goal__c> newPipelineGoals = new List<Pipeline_Goal__c>();
        for(OpportunityTeamMember otm : insertRecords){
            Opportunity oppty = opptyMap.get(otm.opportunityId);
            for(Goal__c g : lstGoals){
                //For each triggered Opportunity Team Member, find goals that match this user and time period
                if(otm.UserId == g.User__c 
                && (
                (!oppty.IsClosed && (g.End_Date__c >= date.today()))
                || (oppty.CloseDate >= g.Start_Date__c && oppty.CloseDate <= g.End_Date__c))){
                    newPipelineGoals.add(new Pipeline_Goal__c(
                        Goal__c = g.Id,
                        Pipeline__c = otm.OpportunityId,
                        Deal_Credit__c = otm.Deal_Credit__c
                        )
                    );   
                }
            }
        }
        insert newPipelineGoals;
    }
    public void AfterDelete(List<OpportunityTeamMember> deleteRecords){
        //-----------------------------------------------------------------------
        //Goals Opportunity Team Member delete - Jeff Hulslander 10/5/13
        //-----------------------------------------------------------------------
        List <Id> lstOpps = new List<Id>();
        for(OpportunityTeamMember otm : deleteRecords){
            lstOpps.add(otm.OpportunityId);
        }
        List<Pipeline_Goal__c> lstPipeGoals = new List<Pipeline_Goal__c>([select id,Goal__r.User__c,Pipeline__c from Pipeline_Goal__c where Pipeline__c in :lstOpps]);
        List<Pipeline_Goal__c> pipeGoalsToDelete = new List<Pipeline_Goal__c>();
        for(OpportunityTeamMember otm : deleteRecords){
            for(Pipeline_Goal__c plg : lstPipeGoals){
                if(otm.UserId == plg.Goal__r.User__c && otm.OpportunityId == plg.Pipeline__c){
                    pipeGoalsToDelete.add(plg);
                }
            }
        }
        delete pipeGoalsToDelete;
    }
    
    public void RevenueSegment(List<OpportunityTeamMember> OppTeamList){
        boolean GIB;
        boolean CMM;
        boolean RBC;
        boolean SpecInd;
        boolean Corp; 
        boolean updtRevSeg; 
        for (OpportunityTeamMember otm : OppTeamList){
            GIB = false;
            CMM = false;
            RBC = false;
            SpecInd = false;
            Corp = false;           
            updtRevSeg = false;     
            list<OpportunityTeamMember> listOTM = new list<OpportunityTeamMember>();
            listOTM = [SELECT id, User.C_I_Type__c, User.LOB__c, User.Specialized_Industries__c, Revenue_Segment__c From OpportunityTeamMember WHERE  OpportunityId = :otm.OpportunityId];
            for(OpportunityTeamMember otms : listOTM  ){
                system.debug(otms);
                      if (otms.User.C_I_Type__c == 'GIB') {
                             GIB = true;
                      }
                      if (otms.User.C_I_Type__c == 'CMM') {
                             CMM = true;
                      }
                      if (otms.User.LOB__c == 'Regions Business Capital') {
                             RBC = true;
                      }
                      if (otms.User.Specialized_Industries__c == true) {
                             SpecInd =  true;
                      }
                      if (otms.User.C_I_Type__c == 'Corporate') {
                             Corp =  true;
                      }
        
            }
            
            system.debug('GIB ' + GIB);
            system.debug('CMM ' + CMM); 
            system.debug('RBC ' + RBC);
            system.debug('SpecInd ' + SpecInd);
            system.debug('Corp ' + Corp);   
            //system.debug(otm);
            //list<OpportunityTeamMember> updateOTM = new list<OpportunityTeamMember>();
            for(OpportunityTeamMember  otms : listOTM){     
                if (CMM == true){            
                    if (otms.User.C_I_Type__c == 'CMM'){
                        
                        if ((GIB == true) || (RBC == true) || (SpecInd == true)){
                            otms.Revenue_Segment__c = 'Shared';
                            updtRevSeg = true;
                        }
                        if (((GIB == false) && (RBC == false) && (SpecInd == false)) || (Corp == true)){
                            otms.Revenue_Segment__c = 'Core';
                            updtRevSeg = true;
                        }      
                    }
                }
                if (GIB == true){   
                    if (otms.User.C_I_Type__c == 'GIB'){
                        if ((RBC == true) || (SpecInd == true)) {
                            otms.Revenue_Segment__c = 'Shared';
                            updtRevSeg = true;
                        }                       
                        if (((RBC == false) || (SpecInd == false)) || (CMM == true) || (Corp == true)) {
                            otms.Revenue_Segment__c = 'Core';
                            updtRevSeg = true;
                        }      
                    }
                }
                /*if (updtRevSeg == true){
                    
                }*/             
            }
            if (updtRevSeg == true){
                //system.debug(otms);
                update listOTM;
            }

        }
        
        
    }
    public void CoreSegment(List<OpportunityTeamMember> OppTeamList){
        boolean GIB;
        boolean CMM;
        boolean RBC;
        boolean SpecInd; 
        boolean updtCore; 
        string BGType;
        string BGLob;

        //remove duplicat opportunities
        Set<Opportunity> Oppset = new Set<Opportunity>();
        List<Opportunity> OppListReset = new List<Opportunity>();
        
        for (OpportunityTeamMember otmReset : OppTeamList){
            Opportunity opp = new Opportunity(id = otmReset.OpportunityId);
            OppListReset.add(opp);
        }
        Oppset.addAll(OppListReset);
        List<Opportunity> OppList = new List<Opportunity>();
        system.debug('result '+Oppset);
        OppList.AddAll(Oppset);     
        system.debug('OppList '+OppList);
        
        
        //list<Opportunity> updateOpps = new list<Opportunity>();
             
        for (Opportunity opp : OppList){
            GIB = false;
            CMM = false;
            RBC = false;
            SpecInd = false;
            updtCore = false;   
            BGType = '';
            BGLob = ''; 
            set<string> CIType = new set<string>();
            set<string> LOB = new set<string>();
            
            for(OpportunityTeamMember otms : [SELECT id, User.C_I_Type__c, User.LOB__c, User.Specialized_Industries__c From OpportunityTeamMember WHERE  OpportunityId = :opp.Id]  ){
                if (otms.User.LOB__c == 'Commercial and Industrial (C&I)' || otms.User.LOB__c == 'Energy & Natural Resources' || otms.User.LOB__c == 'Healthcare' || otms.User.LOB__c == 'Real Estate Banking' || otms.User.LOB__c == 'Regions Business Capital' || otms.User.LOB__c == 'Restaurant' || otms.User.LOB__c == 'Technology & Defense' || otms.User.LOB__c == 'Transportation' || otms.User.LOB__c == 'Financial Services Group (FSG)'){
                    system.debug(otms);
                    CIType.add(otms.User.C_I_Type__c);
                    LOB.add(otms.User.LOB__c);
                    BGType = BGType + ' ' + otms.User.C_I_Type__c;
                    BGLob = BGLob  + ' ' + otms.User.LOB__c;
                    
                          if (otms.User.C_I_Type__c == 'GIB') {
                                 GIB = true;
                          }
                          if (otms.User.C_I_Type__c == 'CMM') {
                                 CMM = true;
                          }
                          
                          if (otms.User.LOB__c == 'Regions Business Capital') {
                                 RBC = true;
                          }
                          if (otms.User.Specialized_Industries__c == true) {
                                 SpecInd =  true;
                          }
                }
            }
            // ** Core & Core Segment Start **
            if (CIType.contains('CMM') == true || CIType.contains('GIB') == true){
                opp.core__c = 'Commercial';
                updtCore = true;
                if ((CIType.size() == 1) && (CIType.contains('CMM') == true)){  
                    opp.core_segment__c = 'Commercial Middle Market - Core';
                }
                if (CIType.contains('CMM') == true  && SpecInd == true){
                    opp.core_segment__c = 'Commercial Middle Market Shared - Specialized Industries';
                }               
                if (CIType.contains('CMM') == true && RBC == true){  
                    opp.core_segment__c = 'Commercial Middle Market Shared - RBC';
                }
                if (CIType.size() > 1 && CMM == true && GIB == true){
                    opp.core_segment__c = 'Commercial Middle Market Shared - GIB';
                }
                if (CIType.size() == 3 && CMM == true && GIB == true && SpecInd == true){
                    opp.core_segment__c = 'Commercial Middle Market Shared - Specialized Industries';
                }   
                if ((CIType.size() == 1) && (CIType.contains('GIB') == true)){  
                    opp.core_segment__c = 'GIB - Core';
                }
                if (CIType.contains('GIB') == true && SpecInd == true){
                    opp.core_segment__c = 'GIB Shared - Specialized Industries';
                }
                if (CIType.contains('GIB') == true && RBC == true){  
                    opp.core_segment__c = 'GIB Shared - RBC';
                }               
                
            }
            if (CIType.contains('Corporate') == true){
                opp.core__c = 'Corporate';
                updtCore = true;
                if (CIType.size() == 1 && CIType.contains('Corporate') == true){
                    opp.core_segment__c = 'Corporate - Core';
                }
                if (CIType.contains('CMM') == true && CIType.contains('Corporate') == true){
                    opp.core_segment__c = 'Corporate - Core';
                }
                
                //if (BGLob.countMatches('Regions Business Capital') > 1 && BGType.contains('Corporate') == true){  
                if (BGLob.contains('Regions Business Capital') == true && BGType.contains('Corporate') == true){
                    opp.core_segment__c = 'Corporate Shared - RBC';
                }
                //if (BGType.countMatches('Corporate') > 1 && SpecInd == true){  
                if (BGType.contains('Corporate') == true && SpecInd == true){
                    opp.core_segment__c = 'Corporate Shared - Specialized Industries';
                }
            }
            if (LOB.contains('Real Estate Banking') == true){
                opp.core__c = 'Real Estate Banking';
                updtCore = true;    
                if (BGType.contains('Affordable Housing') == true){  
                    opp.core_segment__c = 'Affordable Housing';
                }
                if (BGType.contains('Home Builder Finance') == true){  
                    opp.core_segment__c = 'Home Builder Finance';
                }
                if (BGType.contains('Income Property Finance') == true){  
                    opp.core_segment__c = 'Income Property Finance';
                }
                if (BGType.contains('Local Real Estate') == true){  
                    opp.core_segment__c = 'Local Real Estate';
                }
                if (BGType.contains('Real Estate Corporate Banking') == true){  
                    opp.core_segment__c = 'Real Estate Corporate Banking';
                }
            }
            // ** Core & Core Segment end **

            // ** Core_2__c & Core_2_Segment__c Start **
            if (CIType.contains('GIB') == true){
                opp.Core_2__c = 'GIB';
                updtCore = true;
                if (CIType.size() > 1 && CMM == true && GIB == true){  
                    opp.Core_2_Segment__c = 'GIB - Shared';
                }                   
                if (CIType.size() == 1 &&  GIB == true){  
                    opp.Core_2_Segment__c = 'GIB - Direct';
                }
            }
            if (RBC == true){
                opp.Core_2__c = 'RBC';
                updtCore = true;
                if (LOB.size() > 1 && BGLob.countMatches('Regions Business Capital') == 1){  
                    opp.Core_2_Segment__c = 'RBC - Shared';
                }
                if (LOB.size() == 1){  
                    opp.Core_2_Segment__c = 'RBC - Direct';
                }   
            }           
            
            // ** Core_2__c & Core_2_Segment__c END **
            
            // ** Specialized__c & Specialized_Segment_Logic__c Start **
            if (SpecInd == true){
                opp.Specialized__c = 'Specialized';
                updtCore = true;
                if (BGLob.contains('Energy & Natural Resources') == true && BGLob.contains('Commercial and Industrial (C&I)') == true){  
                    opp.Specialized_Segment_Logic__c = 'Energy - Shared';
                }
                if (BGLob.contains('Energy & Natural Resources') == true && BGLob.contains('Commercial and Industrial (C&I)') == false){  
                    opp.Specialized_Segment_Logic__c = 'Energy - Direct';
                }
                if (BGLob.contains('Healthcare') == true && BGLob.contains('Commercial and Industrial (C&I)') == true){  
                    opp.Specialized_Segment_Logic__c = 'Healthcare - Shared';
                }
                if (BGLob.contains('Healthcare') == true && BGLob.contains('Commercial and Industrial (C&I)') == false){  
                    opp.Specialized_Segment_Logic__c = 'Healthcare - Direct';
                }
                if (BGLob.contains('Technology & Defense') == true && BGLob.contains('Commercial and Industrial (C&I)') == true){  
                    opp.Specialized_Segment_Logic__c = 'Technology & Defense - Shared';
                }
                if (BGLob.contains('Technology & Defense') == true && BGLob.contains('Commercial and Industrial (C&I)') == false){  
                    opp.Specialized_Segment_Logic__c = 'Technology & Defense - Direct';
                }                                   
                if (BGLob.contains('Transportation') == true && BGLob.contains('Commercial and Industrial (C&I)') == true){  
                    opp.Specialized_Segment_Logic__c = 'Transporation - Shared';
                }
                if (BGLob.contains('Transportation') == true && BGLob.contains('Commercial and Industrial (C&I)') == false){  
                    opp.Specialized_Segment_Logic__c = 'Transporation - Direct';
                }               
                if (BGLob.contains('Restaurant') == true && BGLob.contains('Commercial and Industrial (C&I)') == true){  
                    opp.Specialized_Segment_Logic__c = 'Restaurant - Shared';
                }
                if (BGLob.contains('Restaurant') == true && BGLob.contains('Commercial and Industrial (C&I)') == false){  
                    opp.Specialized_Segment_Logic__c = 'Restaurant - Direct';
                }
                if (BGLob.contains('Financial Services Group (FSG)') == true && BGLob.contains('Commercial and Industrial (C&I)') == true){  
                    opp.Specialized_Segment_Logic__c = 'Financial Services Group (FSG) - Shared';
                }
                if (BGLob.contains('Financial Services Group (FSG)') == true && BGLob.contains('Commercial and Industrial (C&I)') == false){  
                    opp.Specialized_Segment_Logic__c = 'Financial Services Group (FSG) - Direct';
                }                               
            }
        
            // ** Specialized__c & Specialized_Segment_Logic__c END **          
            
                
            if (updtCore == true){
                system.debug(opp);
                update opp;
            }   
        }       
                
    } //end of public void CoreSegment
        
}