global class SalesExecutiveBatchScheduler implements Schedulable{
   global void execute(SchedulableContext sc) {
      SalesExecutiveClientTeamBatch SEB = new SalesExecutiveClientTeamBatch(); 
      database.executebatch(SEB,5);
   }
}