/*
Author - Sendeep
Task: T-125307 - Custom clone on pipeline
Date: 05/13/2013
*/

public class createLookbackCTRL {
    public static boolean isLookback=false; // case# 1584...Ephrem 10/2014
    Opportunity opportunityOld = new Opportunity();
    List<OpportunityLineItem> oppLineitemsOld = new List<OpportunityLineItem>();
    List<OpportunityLineItem> oppLineitemsNew = new List<OpportunityLineItem>();
    
    //8-21-13 Modified by Jeff Hulslander - Lookback Update
    List<Funding__c> oppFundingOld = new List<Funding__c>();
    List<Funding__c> oppFundingNew = new List<Funding__c>();
    List<OpportunityTeamMember> oppTeamOld = new List<OpportunityTeamMember>();
    List<OpportunityTeamMember> oppTeamNew = new List<OpportunityTeamMember>();
    //End 8-21-13 Update
    
    // Chris Wilder 12/17/14 new code for combined Approval & Lookback process    
    List<Referral__c> oppReferralOld = new List<Referral__c>();
    List<Referral__c> oppReferralNew = new List<Referral__c>();
    //end 12-17-14 update
    
    
    String oppId = '';
    // url parameters
    string Lookback_PipelineID = '';
    string lookback_pipelineName = '';
    decimal oppAmount;
    public Boolean showError{get;set;}
    
    public createLookbackCTRL(ApexPages.StandardController controller) {        
        oppId = ApexPages.currentPage().getParameters().get('id');
        String qryOpportunity = new selectall('Opportunity').soql + ' where Id= :oppId LIMIT 1';
        opportunityOld = Database.query(qryOpportunity);
        
        Lookback_PipelineID = oppId;
        Lookback_PipelineName = opportunityOld.Name + ' - LB'; 
        // 12-17-14 Changed oppAmount = opportunityOld.Amoun
        oppAmount = opportunityOld.fee_total__c;        
        oppLineitemsOld = [SELECT Id, OpportunityId, SortOrder, PricebookEntryId, Quantity, TotalPrice, Fee_Investment_Management__c, Inv_Mgmt_Fee_Incentive_Pymt__c FROM OpportunityLineItem WHERE opportunityid =:oppId];
        
        //8-21-13 Modified by Jeff Hulslander - Lookback Update
        oppFundingOld = [SELECT Id, Assets__c, Date_Funded__c, Pipeline__c FROM Funding__c 
        WHERE Pipeline__c =:oppId];   
        // 6-19-15 CJW added Incentive_Pay__c for Case 2105 
        oppTeamOld = [SELECT Id, OpportunityId, UserId, TeamMemberRole, OpportunityAccessLevel, 
        Deal_Credit__c, LOB__c, Running_Users_LOB__c, Split_Amount__c, Team_Member_Copy__c, 
        Acceptance_Fee_Split__c, Investment_Management_Fee_Split__c, One_Time_Fee_Split__c, 
        Recurring_Fee_Split__c, Associate_ID__c, C_I__c, Cost_Center__c, Incentive_Pay__c 
        from OpportunityTeamMember WHERE opportunityid =:oppId];                             
        //End 8-21-13 Update
        // Chris Wilder 12/17/14 new code for combined Approval & Lookback process
        oppReferralOld = [SELECT Pipeline__c, External_Referrer__c, Address__c,
        				  Percent_Credit__c, Potential_Award__c, Referral_Cost_Center__c, GL_Code__c,
        				  Payment_Schedule__c, OneView_User__c, Referrer__c, RegionsConnects_Referral__c,
        				  Region__c, Area__c, Referring_LOB__c, Associate_ID__c FROM Referral__c
        				  WHERE Pipeline__c =:oppId];   
    }   

    public pagereference createLookback() { 
            string dateClose = opportunityOld.CloseDate.month()+'/'+ opportunityOld.CloseDate.day()+'/'+opportunityOld.CloseDate.year();            
            Opportunity newRecord;
            if(opportunityOld.StageName == 'Closed Won' && opportunityOld.Incentive_Approval_Acceptable__c) {
            	// Chris Wilder 12/5/14
            	// removed: && String.isBlank(opportunityOld.Incentive_Status__c) from if statement for combined Approval & Lookback process                
                isLookback = true; // case# 1584...Ephrem 10/2014
                newRecord = opportunityOld.clone(false,true);
                newRecord.Fee_Schedule__c = 'Corp Trust Lookback';
                newRecord.lookback__c = TRUE;
                newRecord.Lookback_Pipeline__c = opportunityOld.Id;
                newRecord.StageName = 'Accepted';
                //8-21-13 Modified by Jeff Hulslander - Lookback Update
                newRecord.Schedule1__c = FALSE;
                newRecord.Schedule2__c = FALSE; 
                newRecord.Schedule3__c = TRUE; 
                newRecord.Schedule4__c = FALSE;
                newRecord.New_Recurring_Fee_Sales__c = NULL;
                //newRecord.Annual_Recordkeeping_Fees__c = NULL;
                newRecord.Age_of_Client__c = NULL;
                newRecord.Non_Recurring__c = 'Yes';
                newRecord.Incentive_Type__c = 'One-Time fee for existing account only';
                // Chris Wilder 12/19/14 added Incentive_Status__c
                newRecord.Incentive_Status__c = '';
                newRecord.Other_Fee_Schedule__c = '';
                newRecord.Name = Lookback_PipelineName;
                newRecord.Fee_Payment_Schedule__c = 'One-Time';
                //End 8-21-13 Update
                insert newRecord;
                cloneLineItems(oppLineitemsOld, newRecord);
                //8-21-13 Modified by Jeff Hulslander - Lookback Update
                cloneFundings(oppFundingOld, newRecord);
                clonePipelineTeam(oppTeamOld, newRecord);
                //End 8-21-13 Update
                adjustOriginalPipeline(opportunityOld, newRecord);
                
                //keep the original closedate...Ephrem 04-21-2014
                newRecord.CloseDate = Date.parse(dateClose);
                newRecord.StageName = 'Closed Won';                
                update newRecord;                
                return new PageReference('/'+opportunityOld.ID);
            } else {
                isLookback = false; // case# 1584...Ephrem 10/2014
                showError = TRUE;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Record does not meet lookback criteria'));
                //return null;
                return new Pagereference('/'+opportunityOld.ID);
            }   
    }
    
    public void cloneLineItems(List<OpportunityLineItem> olis, Opportunity oppNew) {
       for( OpportunityLineItem  oli : olis) {
           OpportunityLineItem newoli = oli.clone(false);
           newoli.OpportunityId = oppNew.id;
           newoli.Fee_Acceptance__c = 0;
           newoli.Fee_One_Time__c = 0;
           newoli.Fee_Recurring__c = 0;
           newoli.Fee_Type__c = 'In Advance';
           oppLineItemsNew.add(newoli);
       }
       insert oppLineItemsNew;
    }
    //8-21-13 Modified by Jeff Hulslander - Lookback Update
    public void cloneFundings(List<Funding__c> fundings, Opportunity oppNew) {
       for(Funding__c funding : fundings) {
           Funding__c newfunding = funding.clone(false);
           newfunding.Pipeline__c = oppNew.id;
           oppFundingNew.add(newfunding);
       }
       insert oppFundingNew;
    }
	//12-17-2014 Chris Wilder Approval Lookback
    public void cloneReferrals(List<referral__c> referrals, Opportunity oppNew) {
       for(Referral__c referral : referrals) {
           Referral__c newReferral = referral.clone(false);
           newReferral.Pipeline__c = oppNew.id;
           oppReferralNew.add(newReferral);
       }
       insert oppReferralNew;
    }
    //end 12-17-14
    
    public void clonePipelineTeam(List<OpportunityTeamMember> pipelineTeam, Opportunity oppNew) {
       for(OpportunityTeamMember teamMember: pipelineTeam) {
           OpportunityTeamMember newTeamMember = teamMember.clone(false);
           newTeamMember.OpportunityId = oppNew.id;
           // 6-19-15 CJW added Incentive_Pay__c for Case 2105, change newTeamMember.Incentive_Pay__c = 'No'; to below
           newTeamMember.Incentive_Pay__c = teamMember.Incentive_Pay__c;
           oppTeamNew.add(newTeamMember);
       }
       insert oppTeamNew;
    }
    //End 8-21-13 Update
    
    public void adjustOriginalPipeline(Opportunity oppOld, Opportunity oppNew){
        List<OpportunityLineItem> oppLineitems = new List<OpportunityLineItem>();
        oppOld.Lookback_Pipeline__c = oppNew.id;
        oppOld.CT_fees_Eligible_for_TIPS__c = oppAmount;
        update oppOld;
        for(OpportunityLineItem oppline : [SELECT ID,Fee_Investment_Management__c, Inv_Mgmt_Fee_Incentive_Pymt__c FROM OpportunityLineItem WHERE OpportunityID = :oppOld.id]) {
           OpportunityLineItem lineitem = new OpportunityLineItem(ID=oppline.Id);
           lineitem.Fee_Investment_Management__c = 0;
           lineitem.Inv_Mgmt_Fee_Incentive_Pymt__c = 0; 
           oppLineitems.add(lineitem);                         
        }
        update oppLineitems;
    }    
}