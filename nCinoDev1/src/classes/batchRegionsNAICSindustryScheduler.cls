//-------------------------------------------------------------------------------------
//Case 2313... scheduler for batchRegionsNAICSindustry class...
//Ephrem Tekle 01/2016...
//-------------------------------------------------------------------------------------
global class batchRegionsNAICSindustryScheduler implements Schedulable{
    global void execute(SchedulableContext sc) {
        batchRegionsNAICSindustry BRNI = new batchRegionsNAICSindustry(); 
        database.executebatch(BRNI,5);
    }
}