/************************************************************************************
 Apex Class Name     : LogFrameworkTest
 Version             : 1.0
 Created Date        : 12th Feb 2016
 Function            : Test Class for LogFramework Class.
 Author              : Sumit Sagar
 Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Sumit Sagar               2/12/2016                Original Version
*************************************************************************************/
             
@isTest(seealldata = false)
private class LogFrameworkTest {
    
    
    public static testMethod void testMethod1() {
        
        test.starttest();
        //LogFramework LogFrameworkClassInstance = new LogFramework();
        LogFramework lf = LogFramework.getLogger();
        lf.toString();
        
        lf.createWorkLogs('Test Data');
        lf.commitLogs();
        
        // Creating sample http request
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://api.salesforce.com/foo/bar');
        req.setMethod('GET');
        lf.createWorkLogs(req);
        
        
        // Creating Sample http response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"foo":"bar"}');
        res.setStatusCode(200);
        lf.createWorkLogs(res);

        list<Log__c> listLog = [select id from Log__c Limit 1];
        System.AssertNotEquals(0 , listLog.size());
        //Create Sample Exception
        //try{
            //DMLException e = (DMLException)DMLException.class.newInstance();
            //lf.createExceptionLogs(e);
        //}
        

        test.stoptest();
        
    }
    
}