/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (seeAllData=true)
private class testTaskTrigger {

    static testMethod void myUnitTest() {
        test.startTest();
        //---------------------Test REB Hours Update---------------------
        Taskflow_Hours__c hbfHours = [select Task_Hours__c from Taskflow_Hours__c where Task_Group__c = 'HBF - Pipeline Task' and Task_Type__c = 'ReCAP' limit 1];
        Double hbfPipelineHours = hbfHours.Task_Hours__c;
        
        REB_Task_Hours__c notStarted = [SELECT Percent_Value__c from REB_Task_Hours__c where Task_Area__c = 'Pipeline Task' and Status__c = 'Not Started' limit 1];
        Double notStartedPercent = notStarted.Percent_Value__c;
        Double notStartedHours = hbfPipelineHours*notStartedPercent/100;
        
        REB_Task_Hours__c inProg = [SELECT Percent_Value__c from REB_Task_Hours__c where Task_Area__c = 'Pipeline Task' and Status__c = 'In Progress' limit 1];
        Double inProgPercent = inProg.Percent_Value__c;
        Double inProgHours = hbfPipelineHours*inProgPercent/100;
        
        REB_Task_Hours__c completed = [SELECT Percent_Value__c from REB_Task_Hours__c where Task_Area__c = 'Pipeline Task' and Status__c = 'Completed' limit 1];
        Double completedPercent = completed.Percent_Value__c;
        Double completedHours = hbfPipelineHours*completedPercent/100;
        
        Task testTask = new Task();
        RecordType rebTask = [select id from recordtype where name='REB CU Task' limit 1];
        testTask.RecordTypeId = rebTask.Id;
        testTask.Attorney_Involved__c = 'No';
        testTask.Task_Group__c = 'HBF - Pipeline Task';
        testTask.Task_Type__c = 'ReCAP';
        testTask.Status = 'Not Started';
        insert testTask;
        
        Task assertTask = [select Id,Status,Task_Hours__c from Task where id=:testTask.Id];
        system.assertEquals(notStartedHours,assertTask.Task_Hours__c);
        
        
        assertTask.Status = 'In Progress';
        update assertTask;
        assertTask = [select Id,Status,Task_Hours__c from Task where id=:testTask.Id];
        system.assertEquals(inProgHours,assertTask.Task_Hours__c);
        
        assertTask.Status = 'Completed';
        update assertTask;
        assertTask = [select Id,Status,Task_Hours__c from Task where id=:testTask.Id];
        system.assertEquals(completedHours,assertTask.Task_Hours__c);
        
        assertTask.Status = 'Completed';
        assertTask.Attorney_Involved__c = 'Yes';
        update assertTask;
        assertTask = [select Id,Status,Task_Hours__c from Task where id=:testTask.Id];
        system.assertEquals(completedHours+2,assertTask.Task_Hours__c);
        
        assertTask.Status = 'Unknown Status';
        update assertTask;
        assertTask = [select Id,Status,Task_Hours__c from Task where id=:testTask.Id];
        system.assertEquals(0,assertTask.Task_Hours__c);
        //---------------------Test renewal/financial review completion---------------------
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        insert testAcc;
        
        Bank_Account_Details__c testBA = new Bank_Account_Details__c();
        testBA.Client__c = testAcc.Id;
        testBA.Maturity_Date__c = date.today();
        insert testBA;
        
        Task testTask2 = new Task();
        testTask2.Subject = 'Bank Account up for Renewal';
        testTask2.Task_Type__c = 'Renewal/Financial Review';
        testTask2.Status = 'Not Started';
        testTask2.WhatId = testBA.Id;
        insert testTask2;
        
        testTask2.Status = 'Completed';
        //update testTask2;
        
        List<Opportunity> lstOpps = new List<Opportunity>([Select Id from Opportunity where AccountId = :testAcc.Id]);
        //system.assertEquals(1,lstOpps.size());
        //---------------------Test workflow task insert clone/update---------------------
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        User rmExec = new User(Alias = 'rmexec', Email='rmexec@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='TestingA', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, LOB__c='Treasury Management',
        TimeZoneSidKey='America/Los_Angeles', UserName='rmexec@testorg.com');
        insert rmExec;
        
        User rmManager = new User(Alias = 'rmman', Email='rmman@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='TestingA', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, LOB__c='Treasury Management',
        TimeZoneSidKey='America/Los_Angeles', UserName='rmman@testorg.com',
        ManagerId=rmExec.Id);
        insert rmManager;
        
        User rm = new User(Alias = 'rm', Email='rm@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='TestingA', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, LOB__c='Treasury Management',
        TimeZoneSidKey='America/Los_Angeles', UserName='rm@testorg.com',
        ManagerId=rmManager.Id);
        insert rm;
        
        Task testTask3 = new Task();
        testTask3.Subject = 'Bank Account up for Renewal';
        testTask3.OwnerId = rm.Id;
        //insert testTask3;
        
        Task testTask4 = new Task();
        testTask4.Subject = 'Renewal Closes in 30 Days';
        testTask4.OwnerId = rm.Id;
        //insert testTask4;
        
        Task testTask5 = new Task();
        testTask5.Subject = 'Renewal Closes in 60 Days';
        testTask5.OwnerId = rm.Id;
        //insert testTask5;

        test.stopTest();
    }
}