public with sharing class RelationshipStrategyExtension{
    private final relationship__c rel;
    public User owner {get;set;}
    public List<Restricted_Relationship_Information__c> rri {get;set;}
    public List<Account> accs {get;set;}
    public List<Opportunity> closedOpps {get;set;}
    public List<Opportunity> lostOpps {get;set;}
    public List<Opportunity> currentOpps {get;set;}
    public List<Call_Plan__c> callPlans {get;set;}
    
    //***************************************************************************************************************************
    //***Case 1794 - BB - RSP - Update Relationship Strategy Plan with clients list...                                          
    //***Used existing extension for Relationship__c and extended it for Relationship_Strategy_Plan__c object...               
    //***Newly added code lines #18-20, #23-24, #55-63... [if reverting is ever needed]...                                                        
    //***Ephrem 05-2015...                                                                                          
    //****************************************************************************************************************************
    
    private final Relationship_Strategy_Plan__c rsp;
    public List<Relationship_Strategy_Plan__c> relSP {get;set;} 
    String prefix = Schema.getGlobalDescribe().get('Relationship__c').getDescribe().getKeyPrefix();
    
    public RelationshipStrategyExtension(ApexPages.StandardController controller) {
        try{
            if(((String)controller.getRecord().Id).startsWith(prefix)){
                this.rel = (Relationship__c)controller.getRecord();
                owner = [SELECT Name,LOB__c,C_I_Type__c,Region__c,Area__c 
                            FROM User 
                            WHERE ID=:rel.ownerid LIMIT 1];
                rri = [SELECT Credit_Hold_Limit__c,Credit_Hold_Status__c,Total_Loan_Commitment__c,Loan_Balance__c,
                            Investment_Hold_Limit__c,Investment_Hold_Status__c,X12_month_Average_Deposit__c,
                            Regions_360_Score__c,Trailing_12_month_Credit_Revenue__c,Trailing_12_month_Deposit_Revenue__c,
                            Trailing_12_month_Other_Revenue__c,Total_Trailing_12_Month_Revenue__c,Value_as_of_Date__c,
                            Value_of_Real_Estate_Holdings__c,HBF_Annual_Sales_Volume__c,HBF_Annual_New_Homes__c
                            FROM Restricted_Relationship_Information__c 
                            WHERE Relationship__c=:rel.Id LIMIT 1];
                accs = [SELECT Id,Name,BillingCity,BillingState FROM Account WHERE Relationship__c=:rel.Id];
                closedOpps = [SELECT Id,Name,CloseDate,Credit_Commitment__c,Deposits__c,Amount,
                                    (select PricebookEntry.Product2.Name from opportunitylineitems)
                                    FROM opportunity 
                                    WHERE CloseDate=LAST_N_DAYS:365 and isClosed=TRUE and isWon=TRUE and AccountId in :accs
                                    ORDER BY CloseDate desc];
                lostOpps = [SELECT Id,Name,CloseDate,Credit_Commitment__c,Deposits__c,Amount,Winning_Competitor__r.Name,
                                    (select PricebookEntry.Product2.Name from opportunitylineitems)
                                    FROM opportunity 
                                    WHERE CloseDate=LAST_N_DAYS:365 and isClosed=TRUE and isWon=FALSE and AccountId in :accs
                                    ORDER BY CloseDate desc];
                currentOpps = [SELECT Id,Name,CloseDate,Credit_Commitment__c,Deposits__c,Amount,
                                    Probability,(select PricebookEntry.Product2.Name from opportunitylineitems)
                                    FROM opportunity 
                                    WHERE isClosed=FALSE and AccountId in :accs
                                    ORDER BY CloseDate desc];
                callPlans = [SELECT owner.name,Completed_Date__c,Contact__r.Name,Call_Plan_Type__c,Call_Result__c 
                                    FROM call_plan__c 
                                    WHERE Completed_Date__c=LAST_N_DAYS:180 and Client__c in :accs];
           }else{
                this.rsp = (Relationship_Strategy_Plan__c)controller.getRecord();
                relSP =[SELECT Id, Relationship__c from Relationship_Strategy_Plan__c where Id =:rsp.Id];
                accs = [SELECT Id,Name,Owner.LastName,Segment__c,Purged_Indicator__c FROM Account WHERE Relationship__c=:relSP[0].Relationship__c ORDER BY Name];               
            }
        }catch(System.TypeException e){
             System.debug('The following exception has occurred: ' + e.getMessage());                   
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please contact OneView Support.'));  
        }
    }
}