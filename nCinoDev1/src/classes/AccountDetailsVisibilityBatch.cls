global class AccountDetailsVisibilityBatch implements Database.batchable<sObject>{

   public final String Query;
   global Set<Id> relationsIds ;
   public AccountDetailsVisibilityBatch(String q){
       Query = q;
   }
   public AccountDetailsVisibilityBatch(){
      //Batch all
      // Query='SELECT id, OwnerID, Relationship__c, (Select id,Client__c from restricted_client_information__r), (Select id,Client__c from client_account_details__r), (select id,UserId,AccountAccessLevel,AccountId from AccountTeamMembers) from Account ';
      // modified by sendeep
		Query='SELECT id, OwnerID, Relationship__c  from Account ';      
       
   }

   global Iterable<sObject> start (Database.BatchableContext BC){
      return ((bc==null)?null: Database.query(query));
   }

   public void execute(Database.BatchableContext BC, List<Account> acctList){
        AccountDetailsVisibilityUtil.recalculateSharing(acctList);
   }

   public void finish(Database.BatchableContext BC){
   }
   WebService static void ExecuteFinalizeTemp(){
      //Id batchInstanceId = Database.executeBatch(new UpdateAccountFields(q,e,f,v), 5); 
      Id batchInstanceId = Database.executeBatch(new AccountDetailsVisibilityBatch(), 5);
   }
   // modified by Sendeep
   public static  void startAccountDetailsVisibilityBatch(Set<Id> relationsIds)
   {
   	// get all the clients from the relationship IDs using query 
   		String query ='SELECT id, OwnerID, CreatedByID,Relationship__c from Account where Relationship__c in : relationsIds';
   		AccountDetailsVisibilityBatch visibilityBatch = new AccountDetailsVisibilityBatch(query);
   		visibilityBatch.relationsIds = relationsIds ;
   		Database.executeBatch(visibilityBatch , 50) ;
   }
}