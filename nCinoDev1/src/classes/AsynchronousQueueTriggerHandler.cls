/************************************************************************************
 Apex Class Name     : AsynchronousQueueTriggerHandler
 Version             : 1.0
 Created Date        : 29th Feb 2016
 Function            : Handler Class for Asynchronous Queue Trigger. This Class will not have any logic and will call methods in Helper Class.
 Author              : Sumit Sagar 
 Modification Log    :
* Developer                             Date                   Description
* ----------------------------------------------------------------------------                 
* Sumit Sagar                          2/29/2016                Original Version
*************************************************************************************/
public class AsynchronousQueueTriggerHandler implements ITriggerHandler {

    AsynchronousQueueService objAsyncQueueService = new AsynchronousQueueService();

    //Handler Method for Before Insert.
    public void BeforeInsert(List<sObject> newItems) {
        objAsyncQueueService.ValidateImmediateCallOutProcesses((List<Asynchronous_Queue__c>)newItems);
    }
    
    //Handler Method for Before Update.
    public void BeforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
    }
    
    //Handler Method for Before Delete.    
    public void BeforeDelete(Map<Id, sObject> oldItems) {
    }
    
    //Handler method for After Insert.
    public void AfterInsert(Map<Id, sObject> newItems) {
        objAsyncQueueService.InitiateAsyncProcessMethod(newItems);
    }

    //Handler method for After Update.    
    public void AfterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
        
    }
    
    //Handler method for After Delete    
    public void AfterDelete(Map<Id, sObject> oldItems) {
    }
    
    //Handler method for After Undelete.
    public void AfterUndelete(Map<Id, sObject> oldItems) {
    }
}