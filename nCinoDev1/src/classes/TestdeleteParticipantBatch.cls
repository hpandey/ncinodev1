@isTest
private class TestdeleteParticipantBatch {

    static testMethod void myUnitTest() {
    	
       	Account account = new Account(Name='Test Account');
        insert account;      
            // Access the account that was just created. 
    
        Account insertedAcct = [SELECT Id,Name FROM Account WHERE Name='Test Account'];
            
        
        Contact contact = new Contact();
        contact.FirstName = 'Test';
        contact.LastName = 'ContactA';
        contact.AccountId = insertedAcct.Id;
        insert contact;
		// has required fields
    	Call_Plan__c callplan = new Call_Plan__c(Client__c=account.id, Contact__c=contact.id, Status__c='Planning', Joint_Call2__c='No', Call_Plan_Type__c='Face to Face' );
    	insert callplan;    	
       
       Participant__c[] pl = new List<Participant__c>();
       //for (Integer i=0;i<10;i++) {
            Participant__c p = new Participant__c();
               p.Associate__c='ppp';
               p.RecordTypeId ='012G00000017DgGIAU';
               p.Role__c = 'Area President';
               p.Flag_for_Deletion__c = true;
               p.Call_Plan__c = callplan.id;
           pl.add(p);
       //}
       system.debug(pl);
       insert pl;

       Test.startTest();
       deleteParticipantBatch DPB = new deleteParticipantBatch(); 
       database.executebatch(DPB);
        database.executebatch(DPB);
       Test.stopTest();

       // Verify participants got deleted 
       Integer i = [SELECT COUNT() FROM Participant__c where Call_plan__c = :callplan.id];
       System.assertEquals(i, 0);
       
       
       
    }
}