@isTest (seeAllData=true)
private class testGoalTrigger{
    static testMethod void unitTest() {
        test.startTest();
        
        User testUser = [select id from User where isactive=true limit 1];
        Goal__c testGoal = new Goal__c();
        testGoal.User__c = testUser.Id;
        insert testGoal;
        
        Goal__c assertGoal = [select id, ownerid, user__c from Goal__c where id = :testGoal.Id];
        system.assertEquals(assertGoal.OwnerId,assertGoal.User__c);
        
        test.stopTest();
    } 
}