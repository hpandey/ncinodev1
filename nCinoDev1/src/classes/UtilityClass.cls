/************************************************************************************
 Apex Class Name     : UtilityClass
 Version             : 1.0
 Created Date        : 3rd March 2016
 Function            : Class contains all re-useable components.
 Author              : Deloitte Consulting
 Modification Log    :
* Developer                         Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte Consulting             3/02/2016                Original Version
*************************************************************************************/

public class UtilityClass {
     
    public Static Boolean recursionCheck = true;
    
    public static Map<Schema.DisplayType,String> mapFieldDisplayType_DataType = new Map<Schema.DisplayType,String>{
                                                                Schema.DisplayType.Date => EnvironmentVariables.DATATYPE_DATE,
                                                                Schema.DisplayType.DateTime => EnvironmentVariables.DATATYPE_DATETIME,
                                                                Schema.DisplayType.Integer => EnvironmentVariables.DATATYPE_INTEGER,
                                                                Schema.DisplayType.Picklist => EnvironmentVariables.DATATYPE_PICKLIST,
                                                                Schema.DisplayType.MultiPicklist => EnvironmentVariables.DATATYPE_MULTIPICKLIST,
                                                                Schema.DisplayType.TextArea => EnvironmentVariables.DATATYPE_STRING,
                                                                Schema.DisplayType.String => EnvironmentVariables.DATATYPE_STRING,
                                                                Schema.DisplayType.Boolean => EnvironmentVariables.DATATYPE_BOOLEAN,
                                                                Schema.DisplayType.Currency => EnvironmentVariables.DATATYPE_DOUBLE,
                                                                Schema.DisplayType.Double => EnvironmentVariables.DATATYPE_DOUBLE,
                                                                Schema.DisplayType.Phone => EnvironmentVariables.DATATYPE_PHONE,
                                                                Schema.DisplayType.Email => EnvironmentVariables.DATATYPE_EMAIL,
                                                                Schema.DisplayType.Url => EnvironmentVariables.DATATYPE_EMAIL
                                                                };
                                                                
    public static map<String,String> mapDataType_RCFieldName = new map<String,String>{
                                                                'DATE' => 'Date_Value__c',
                                                                'INTEGER' => 'Number_Value__c',
                                                                'STRING' => 'Text_Value__c',
                                                                'DOUBLE' => 'Number_Value__c',
                                                                'BOOLEAN' => 'Boolean_Value__c',
                                                                'PICKLIST' =>'PICKLIST',
                                                                'DATETIME' => 'Date_Value__c',
                                                                'TEXTAREA' => 'Text_Value__c',
                                                                'MULTIPICKLIST' => 'MULTIPICKLIST',
                                                                'CURRENCY' => 'Currency_Value__c',
                                                                'PERCENT' => 'Percent_Value__c',
                                                                'EMAIL' =>  'Email_Value__c',
                                                                'PHONE' => 'Phone_Value__c',
                                                                'ID' => 'Id',
                                                                'URL' => 'URL_Value__c'
                                                                };
    /************************************************************************
     Method Name: processSaveResult
     Author Name: Deloitte Consulting
     Description: Void method to log errors in SaveResult.
     Parameters: Exception e
     Returns: Void
    *************************************************************************/
    public Static Void processSaveResult(List<Database.SaveResult> results) {
        // Log error
        LogFramework lf = LogFramework.getLogger();
        lf.logSaveResultError(results);
        lf.commitLogs();
    }
    
    /************************************************************************
     Method Name: processDeleteResult
     Author Name: Deloitte Consulting
     Description: Void method to log errors in DeleteResult.
     Parameters: Exception e
     Returns: Void
    *************************************************************************/
    public Static Void processDeleteResult(List<Database.DeleteResult> results) {
        // Log error
        LogFramework lf = LogFramework.getLogger();
        lf.logDeleteResultError(results);
        lf.commitLogs();
    }
    
    
    /************************************************************************
     Method Name: throwExceptions
     Author Name: Deloitte Consulting
     Description: Void method to throw and log exceptions
     Parameters: Exception e
     Returns: Void
    *************************************************************************/
    public Static Void throwExceptions(Exception e) {        
        // Log error
        LogFramework lf = LogFramework.getLogger();
        lf.createExceptionLogs(e);
        lf.commitLogs();
    }
    
    /************************************************************************
     Method Name: getFieldSet
     Author Name: Pranil Thubrikar
     Description: Method to get fieldset for the respective object.
     Parameters: String fsName, String ObjectName
     Returns: Schema.FieldSet
    *************************************************************************/
    public Static Schema.FieldSet getFieldSet(String fsName, String ObjectName){
        
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
        Schema.FieldSet fieldSetObj;
        
        if(GlobalDescribeMap != null){
            Schema.DescribeSObjectResult DescribeSObjectResultObj = GlobalDescribeMap.get(ObjectName).getDescribe();
            fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fsName);
        }

        return fieldSetObj;
    }
    
    /************************************************************************
     Method Name: getErrorMessages
     Author Name: Pranil Thubrikar
     Description: Method to get all Error Messages as a String.
     Parameters: List<Database.Error> listErrors
     Returns: String
    *************************************************************************/
    public Static String getErrorMessages(List<Database.Error> listErrors){
        String strErrMsg = '';
        if(listErrors != null & !listErrors.isEmpty()){
            for(Database.Error error : listErrors){
                strErrMsg += error.getMessage()+'; ';
            }
        }
        return strErrMsg;
    }
    
    /************************************************************************
     Method Name: throwCustomLabelException
     Author Name: Pranil Thubrikar
     Description: Method to throw exception when custom label is null or empty.
     Parameters: List<Database.Error> listErrors
     Returns: String
    *************************************************************************/
    public static void throwCustomLabelException(String errMsg1, string errMsg2){
        String strMsg = '';
        if(errMsg1 != null){
            strMsg += errMsg1;
        }
        if(errMsg2 != null){
            strMsg += (' : '+errMsg2);
        }
        throw new CustomLabelException(strMsg);
    }
    
    // Custom Exception when custom label is null or empty.
    public class CustomLabelException extends Exception { }
    
    /*Method Name    :        getObjectLabelNames
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to get the label names of the object passed in the parameter
     *Parameters     :        String
     *Returns        :        String
    */
    public static String getObjectLabelNames(String strObjectName){
        return Schema.getGlobalDescribe().get(strObjectName).getDescribe().getLabel();
    }//end method
    
    /*Method Name    :        getFieldLabelNames
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to get the label names of the Fields based on the object
     *Parameters     :        String strObjectName , String FieldName
     *Returns        :        String
    */
    public static String getFieldLabelNames(String strObjectName, String strFieldName){
        return Schema.getGlobalDescribe().get(strObjectName).getDescribe().fields.getMap().get(strFieldName).getDescribe().getLabel();
    }//end method
    
    /*Method Name    :        getFieldNames
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to get the Field names based on the object
     *Parameters     :        String strObjectName
     *Returns        :        List<Schema.SObjectField>
    */
    public static List<Schema.SObjectField> getFieldNames(String strObjectName){
        Map <String, Schema.SObjectField> mpFieldMap = Schema.getGlobalDescribe().get(strObjectName).getDescribe().fields.getMap(); 
        List<Schema.SObjectField> lstFieldNames = mpFieldMap.values();
        return lstFieldNames;
    }//end method
    
    /*Method Name    :        getFieldType
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to get the Field type based on the object
     *Parameters     :        String strObjectName , String strFieldName
     *Returns        :        Schema.DisplayType
    */
    public static Schema.DisplayType getFieldType(String strObjectName , String strFieldName){
        return Schema.getGlobalDescribe().get(strObjectName).getDescribe().fields.getMap().get(strFieldName).getDescribe().getType();
    }//end method
    
    /*Method Name    :        getOperatorType
     *Author         :        Deloitte Consulting
     *Description    :        This method is used for getting which operator should be displayed according to field type. 
     *Parameters     :        String strObjectName , String strFieldName
     *Returns        :        Schema.DisplayType
    */
    public static list<String> getOperatorType(string strFieldType) {
        //map<String , list<String>> mapOperatorByFieldType = new map<String , list<String>>(); 
        map<String , list<String>> mapOperatorByFieldType = new map<String , List<String>> {
                        EnvironmentVariables.DATATYPE_DATE =>  new list<String>{
                                                            EnvironmentVariables.OPERTATOR_EQUALSTO , EnvironmentVariables.OPERTATOR_NOTEQUALTO,
                                                            EnvironmentVariables.OPERTATOR_LESSTHAN , EnvironmentVariables.OPERTATOR_GREATERTHAN,
                                                            EnvironmentVariables.OPERTATOR_LESSTHANOREQUALTO , EnvironmentVariables.OPERTATOR_GREATERTHANOREQUALTO
                        },
                        EnvironmentVariables.DATATYPE_DATETIME =>  new list<String>{
                                                            EnvironmentVariables.OPERTATOR_LESSTHAN , EnvironmentVariables.OPERTATOR_GREATERTHAN,
                                                            EnvironmentVariables.OPERTATOR_LESSTHANOREQUALTO , EnvironmentVariables.OPERTATOR_GREATERTHANOREQUALTO
                        },
                        EnvironmentVariables.DATATYPE_INTEGER =>  new list<String>{
                                                            EnvironmentVariables.OPERTATOR_EQUALSTO , EnvironmentVariables.OPERTATOR_NOTEQUALTO,
                                                            EnvironmentVariables.OPERTATOR_LESSTHAN , EnvironmentVariables.OPERTATOR_GREATERTHAN,
                                                            EnvironmentVariables.OPERTATOR_LESSTHANOREQUALTO , EnvironmentVariables.OPERTATOR_GREATERTHANOREQUALTO
                        },
                        EnvironmentVariables.DATATYPE_STRING =>  new list<String>{
                                                            EnvironmentVariables.OPERTATOR_EQUALSTO , EnvironmentVariables.OPERTATOR_NOTEQUALTO,
                                                            EnvironmentVariables.OPERTATOR_CONTAINS , EnvironmentVariables.OPERTATOR_DOESNOTCONTAIN
                        },
                        EnvironmentVariables.DATATYPE_BOOLEAN =>  new list<String>{
                                                            EnvironmentVariables.OPERTATOR_EQUALSTO , EnvironmentVariables.OPERTATOR_NOTEQUALTO
                        },
                        EnvironmentVariables.DATATYPE_DOUBLE =>  new list<String>{
                                                            EnvironmentVariables.OPERTATOR_EQUALSTO , EnvironmentVariables.OPERTATOR_NOTEQUALTO,
                                                            EnvironmentVariables.OPERTATOR_LESSTHAN , EnvironmentVariables.OPERTATOR_GREATERTHAN,
                                                            EnvironmentVariables.OPERTATOR_LESSTHANOREQUALTO , EnvironmentVariables.OPERTATOR_GREATERTHANOREQUALTO
                        },
                        EnvironmentVariables.DATATYPE_PICKLIST =>  new list<String>{
                                                            EnvironmentVariables.OPERTATOR_EQUALSTO , EnvironmentVariables.OPERTATOR_NOTEQUALTO
                                                           
                        },
                        EnvironmentVariables.DATATYPE_MULTIPICKLIST =>  new list<String>{
                                                            EnvironmentVariables.OPERTATOR_EQUALSTO , EnvironmentVariables.OPERTATOR_NOTEQUALTO,
                                                            EnvironmentVariables.OPERTATOR_CONTAINS , EnvironmentVariables.OPERTATOR_DOESNOTCONTAIN
                        },
                        EnvironmentVariables.DATATYPE_PHONE =>  new list<String>{
                                                            EnvironmentVariables.OPERTATOR_EQUALSTO , EnvironmentVariables.OPERTATOR_NOTEQUALTO,
                                                            EnvironmentVariables.OPERTATOR_CONTAINS , EnvironmentVariables.OPERTATOR_DOESNOTCONTAIN
                        },
                        EnvironmentVariables.DATATYPE_EMAIL =>  new list<String>{
                                                            EnvironmentVariables.OPERTATOR_EQUALSTO , EnvironmentVariables.OPERTATOR_NOTEQUALTO,
                                                            EnvironmentVariables.OPERTATOR_CONTAINS , EnvironmentVariables.OPERTATOR_DOESNOTCONTAIN
                        }
                        
        };
        
        if(mapOperatorByFieldType.ContainsKey(strFieldType)) {
            return mapOperatorByFieldType.get(strFieldType);  
        }
        
        return null;
    }//end method
    
    /*Method Name   :         convertStringtoArray
     *Author         :        Deloitte Consulting
     *Description    :        Convert string to a array 
     *Parameters     :        String 
     *Returns        :        String[]
    */
    public static String[] convertStringtoArray(String str){
        String[] returnArray = new String[]{};
        if(str!=null)
            returnArray = str.split(';');
        else
            returnArray = null;
        return returnArray;    
    }//end method
    
     /*Method Name   :        convertArraytoString
     *Author         :        Deloitte Consulting
     *Description    :        Convert Array of String to a semicolon separated values of String to store the input
     *Parameters     :        String[] 
     *Returns        :        String
    */
    public static String convertArraytoString(String[] answers){
        String returnString='';
        for(String str:answers){
            returnString += (returnString==''?'':';')+str;
        }//end for
        return returnString;        
    }//end method
}