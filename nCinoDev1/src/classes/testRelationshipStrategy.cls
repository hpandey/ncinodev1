/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (seeAllData=true)
private class testRelationshipStrategy {

    static testMethod void myUnitTest() {
		Relationship__c testRel = [select id,ownerid from Relationship__c limit 1];		
        Test.SetCurrentPageReference(New PageReference('Page.RelationshipStrategyOverview'));
		System.CurrentPageReference().getParameters().put('id',testRel.Id);
		ApexPages.Standardcontroller sc1 = New ApexPages.StandardController(testRel);
		RelationshipStrategyExtension RSE = new RelationshipStrategyExtension(sc1);
    }
    
    static testMethod void RSPchangesetTest() {
    	Relationship_Strategy_Plan__c testRSP = [SELECT Id, Relationship__c from Relationship_Strategy_Plan__c limit 1];
        List<Account> accs = [SELECT Id,Name,Owner.LastName,Segment__c,Purged_Indicator__c FROM Account WHERE Relationship__c=:testRSP.Relationship__c];
        System.CurrentPageReference().getParameters().put('id',testRSP.Id);
        ApexPages.Standardcontroller sc1 = New ApexPages.StandardController(testRSP);
		RelationshipStrategyExtension RSE = new RelationshipStrategyExtension(sc1);			
    }    
}