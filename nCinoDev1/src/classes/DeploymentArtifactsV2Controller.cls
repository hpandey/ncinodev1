/**************************************************************************************
* Name            : DeploymentArtifactsV2Controller
* Modified        

02/27/11 Svatka -  update export xml format - ApexPage and Trigger were in plural, changed to singular
                  Intialise public variables
 
04/05/2011  Urminder : Added new Data Types (Cutom Label,Custom Tab,Workflow,Custom Objects,
                       Standard Objects) and update Controller 
04/13/2011  Urminder : Added new Data types AnalystSnapshot and ReportType
05/12/2011  Urmidner : Added METADATAAPI_ENDPOINT to get the endpoint of MetaPort
***************************************************************************************/





public with sharing class DeploymentArtifactsV2Controller {

   // 
    public String METADATAAPI_ENDPOINT = 'https://cs11-api.salesforce.com/services/Soap/m/26.0';
    public String dateClause { get; set; }
    public String searchdate{get;set;}
    public List<WrapperApexClass> listWrapCls;
    public List<WrapperApextrigger> listWraptrigger;
    public List<WrapperApexPage> listWrapPages;
    public List<WrapperApexComponent> listWrapComponent;
    public List<WrapperDashbord> listWrapDashbord;
    public List<WrapperWeblink> listWrapWeblink;
    public List<WrapperStaticResource> listWrapStaticResource;
    public List<WrapperReport> listWrapReport;
    public List<WrapperRecordType> listWrapRecordType;
    public List<WrapperScontrol> listWrapScontrol;
    public List<WrapperProfile> listWrapProfile;
    
    public Map<String,list<Wrapper>> mapLayouts;
    public Map<String,list<wrapper>> customFieldMap;
    set<String> setSObj = new set<String>();
    
    //variables used in the page
     
    public DateTime searchdateTime{get;set;}
    public String XMLString{get;set;}
   //this variable is used to check if the date is valid
    public boolean isValidDate{get;set;} 
    public String sObjName{get;set;}
    public Map<string,Wrapper> mapSObjects{get;set;}
    public Map<string,Wrapper> mapStdObj{get;set;}
    public List<Wrapper> listCustomLabel{get;set;}
    public List<Wrapper> listWorkflow{get;set;}
    public List<Wrapper> listCustomTab{get;set;}
    public List<Wrapper> listCstmObj{get;set;}
    public List<Wrapper> listStandardObj{get;set;}
    public List<Wrapper> listAnalyticSnapshot{get;set;}
    public List<Wrapper> listReportType{get;set;}
    /**
     * Constructor
     */ 
    public DeploymentArtifactsV2Controller(){
      customFieldMap = new Map<String,list<Wrapper>>();
      searchdate =Date.today().format(); 
      searchdateTime = Datetime.now().addDays(-1);
      //getting the searchDate and search criteria from url
      if(Apexpages.currentPage().getParameters().get('searchDate') != null ) {
        searchdateTime = date.parse(Apexpages.currentPage().getParameters().get('searchDate'));
       }
       if(Apexpages.currentPage().getParameters().get('dtClause') != null ) {
         dateClause = Apexpages.currentPage().getParameters().get('dtClause');
        } else {
         dateClause = 'CreatedDate';
        }
         
         listWrapCls = new List<WrapperApexClass>();
         listWraptrigger = new List<WrapperApexTrigger>();
         listWrapPages = new List<WrapperApexPage>();
         listWrapComponent = new List<WrapperApexComponent>() ;
         listWrapDashbord = new  List<WrapperDashbord>();
         listWrapWeblink = new List<WrapperWeblink>();
         listWrapStaticResource = new List<WrapperStaticResource>();
         listWrapReport = new List<WrapperReport>();
         listWrapRecordType = new List<WrapperRecordType>();
         listWrapScontrol= new List<WrapperScontrol>();
         listWrapProfile = new List<WrapperProfile>();
         isValidDate =true;
        
         fillData();
    }
    /**
     * In this method we make a list to search the objects or field modified or 
     * created on a particular date
     */ 
    public List<SelectOption> getitems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('CreatedDate','Created Date')); 
        options.add(new SelectOption('LastModifiedDate','Last Modified Date')); 
         
        //dateClause = 'CreatedDate';
        return options; 
    }
    /**
     * In this method we get the list of Apex Class that are modified or Created after the 
     * search date-time
     */ 
    public List<WrapperApexClass> getnewClasses() {
        listWrapCls = new List<WrapperApexClass>();
        String query = 'Select Name, LastModifiedDate, CreatedDate From ApexClass where ';  
        query += dateClause + '>= :searchdateTime order by name asc';
        for(ApexClass cls : Database.query(query)) {
           WrapperApexClass wrap = new WrapperApexClass(cls);
           listWrapCls.add(wrap);
        }
        return listWrapCls;
    }
    /**
     * In this method we get the list of Apex Page that are modified or Created after the 
     * search date-time
     */
    public List<WrapperApexPage> getnewPages() {
        listWrapPages = new List<WrapperApexPage>();
        String query = 'Select Name, LastModifiedDate, CreatedDate, Description From ApexPage where ';  
        query += dateClause + '>= :searchdateTime order by name asc';
        for(ApexPage cls : Database.query(query)) {
            WrapperApexPage wrap = new WrapperApexPage(cls);
            listWrapPages.add(wrap);
        }
        return listWrapPages;
    }
     /**
     * In this method we get the list of Apex Trigger that are modified or Created after the 
     * search date-time
     */
    
    public List<WrapperApexTrigger> getnewTriggers() {
        listWrapTrigger = new List<WrapperApexTrigger>();
        String query = 'Select Name, LastModifiedDate, CreatedDate From ApexTrigger where ';  
        query += dateClause + '>= :searchdateTime order by name asc';
        System.debug('=======date========='+dateClause);
        for(Apextrigger cls : Database.query(query)) {
            WrapperApexTrigger wrap = new WrapperApexTrigger(cls);
            listWrapTrigger.add(wrap);
        }
        return listWrapTrigger;
    }
   /**
     * In this method we get the list of Apex Component that are modified or Created after the 
     * search date-time
     */
    public List<WrapperApexComponent> getnewComponents() {
       listWrapComponent = new List<WrapperApexComponent>() ;
       String query = 'Select Name, LastModifiedDate, CreatedDate,Description From ApexComponent where ';  
       query += dateClause + '>= :searchdateTime order by name asc';              
       for(ApexComponent cls : Database.query(query)) {
            WrapperApexComponent wrap = new WrapperApexComponent(cls);
            listWrapComponent.add(wrap);
        }
        return listWrapComponent;
    }
   /**
     * In this method we get the list of Static Resource that are modified or Created after the 
     * search date-time
     */
    public List<WrapperStaticResource> getnewStaticResources() {
        listWrapStaticResource = new List<WrapperStaticResource>();
        String query = 'Select Name, LastModifiedDate, CreatedDate,Description From StaticResource where ';  
        query += dateClause + '>= :searchdateTime order by name asc';
        for(StaticResource cls : Database.query(query)) {
            WrapperStaticResource wrap = new WrapperStaticResource(cls);
            listWrapStaticResource.add(wrap);
        }               
       
        return listWrapStaticResource;
    } 
    /**
     * In this method we get the list of Weblink that are modified or Created after the 
     * search date-time
     */
    public List<WrapperWeblink> getnewWeblinks() {
         listWrapWeblink = new List<WrapperWeblink>();
        String query = 'Select Name, PageOrSobjectType, LastModifiedDate, CreatedDate, Description From Weblink where ';  
        query += dateClause + '>= :searchdateTime order by name asc';
        for(Weblink cls : Database.query(query)) {
            WrapperWeblink wrap = new WrapperWeblink(cls);
            listWrapWeblink.add(wrap);
        }               
        return listWrapWeblink;
    }
    /**
     * In this method we get the list of Scontrol that are modified or Created after the 
     * search date-time
     */
    public List<WrapperScontrol> getnewScontrols() {
        listWrapScontrol= new List<WrapperScontrol>();
        String query = 'Select Name, LastModifiedDate, CreatedDate,Description From Scontrol where ';  
        query += dateClause + '>= :searchdateTime order by name asc';
        for(Scontrol cls : Database.query(query)) {
            WrapperScontrol wrap = new WrapperScontrol(cls);
            listWrapScontrol.add(wrap);
        }               
        return listWrapScontrol;
    }
    /**
     * In this method we get the list of Dashboard that are modified or Created after the 
     * search date-time
     */
    public List<WrapperDashbord> getnewDashboards() {
        listWrapDashbord = new  List<WrapperDashbord>();
        String query = 'Select title, LastModifiedDate, CreatedDate, Description From Dashboard where ';  
        query += dateClause + '>= :searchdateTime order by title asc';  
        for(Dashboard cls : Database.query(query)) {
            WrapperDashbord wrap = new WrapperDashbord(cls);
            listWrapDashbord.add(wrap);
        }               
        return listWrapDashbord;
    }
    /**
     * In this method we get the list of Report that are modified or Created after the 
     * search date-time
     */
    public List<WrapperReport> getnewReports() {
        listWrapReport = new List<WrapperReport>();
    
        String query = 'Select Name, DeveloperName,LastModifiedDate, CreatedDate,Description From Report where ';  
        query += dateClause + '>= :searchdateTime order by name asc';
        for(Report cls : Database.query(query)) {
            WrapperReport wrap = new WrapperReport(cls);
            listWrapReport.add(wrap);
        }                   
        return listWrapReport;
    }
    /**
     * In this method we get the list of Record Type that are modified or Created after the 
     * search date-time
     */
    public List<WrapperRecordType> getnewRecordTypes() {
             listWrapRecordType = new List<WrapperRecordType>();

        String query = 'Select Name, LastModifiedDate, CreatedDate,Description From RecordType where ';  
        query += dateClause + '>= :searchdateTime order by name asc';
        for(RecordType cls : Database.query(query)) {
            WrapperRecordType wrap = new WrapperRecordType(cls);
            listWrapRecordType.add(wrap);
        }               
        return listWrapRecordType;
    }
    /**
     * In this method we get the list of Profile that are modified or Created after the 
     * search date-time
     */
    public List<WrapperProfile> getnewProfiles() {
                 
         listWrapProfile = new List<WrapperProfile>();
        String query = 'Select Name, LastModifiedDate, CreatedDate,Description From Profile where ';  
       query += dateClause + '>= :searchdateTime order by name asc';  
        for(Profile cls : Database.query(query)) {
            WrapperProfile wrap = new WrapperProfile(cls);
            listWrapProfile.add(wrap);
        }           
        return listWrapProfile;
    }
    /**
     * This method is used to get the list of Apex Class that are selected from the page
     */
    public List<ApexClass> getClasses() {
        List<ApexClass> selectedClasses = new List<ApexClass>();
        System.debug('===newlist============='+listWrapCls);
        for(WrapperApexClass wrap : listWrapCls) {
            if(wrap.isSelected == true) {
                selectedClasses.add(wrap.ac);
            }
        }
        System.debug('===list============='+selectedClasses);
        return selectedClasses;
    }
    /**
     * This method is used to get the list of Apex Trigger that are selected from the page
     */
    public List<ApexTrigger> getTriggers() {
        List<ApexTrigger> selectedTriggers = new List<ApexTrigger>();
        
        for(WrapperApexTrigger wrap : listWrapTrigger) {
            if(wrap.isSelected == true) {
                selectedTriggers.add(wrap.ac);
            }
        }
        
        return selectedTriggers;
    }
    /**
     * This method is used to get the list of Apex Page that are selected from the page
     */
    public List<ApexPage> getPages() {
        List<ApexPage> selectedApexPage = new List<ApexPage>();
        
        for(WrapperApexPage wrap : listWrapPages) {
            if(wrap.isSelected == true) {
                selectedApexPage.add(wrap.ac);
            }
        }
        
        return selectedApexPage;
    }
    /**
     * This method is used to get the list of Component that are selected from the page
     */
    public List<ApexComponent> getComponents() {
        List<ApexComponent> selectedApexComponent = new List<ApexComponent>();
        
        for(WrapperApexComponent wrap : listWrapComponent) {
            if(wrap.isSelected == true) {
                selectedApexComponent.add(wrap.ac);
            }
        }
        
        return selectedApexComponent;
    }
    /**
     * This method is used to get the list of Static Resource that are selected from the page
     */
    public List<StaticResource> getStaticResources() {
        List<StaticResource> selectedStaticResource = new List<StaticResource>();
        
        for(WrapperStaticResource wrap : listWrapStaticResource) {
            if(wrap.isSelected == true) {
                selectedStaticResource.add(wrap.ac);
            }
        }
        
        return selectedStaticResource;
    }
    /**
     * This method is used to get the list of Weblink that are selected from the page
     */
    public List<Weblink> getWeblinks() {
        List<Weblink> selectedWeblink = new List<Weblink>();
        
        for(WrapperWeblink wrap : listWrapWeblink) {
            if(wrap.isSelected == true) {
                selectedWeblink.add(wrap.ac);
            }
        }
        
        return selectedWeblink;
    }
    /**
     * This method is used to get the list of Scontrol that are selected from the page
     */
    public List<Scontrol> getScontrols() {
        List<Scontrol> selectedScontrol = new List<Scontrol>();
        
        for(WrapperScontrol wrap : listWrapScontrol) {
            if(wrap.isSelected == true) {
                selectedScontrol.add(wrap.ac);
            }
        }
        
        return selectedScontrol;
    }
    /**
     * This method is used to get the list of Dashboard that are selected from the page
     */
    public List<Dashboard> getDashboards() {
        List<Dashboard> selectedDashboard = new List<Dashboard>();
        
        for(WrapperDashbord wrap : listWrapDashbord) {
            if(wrap.isSelected == true) {
                selectedDashboard.add(wrap.ac);
            }
        }
        
        return selectedDashboard;
    }
    /**
     * This method is used to get the list of Report that are selected from the page
     */
    public List<Report> getReports() {
        List<Report> selectedReport = new List<Report>();
        
        for(WrapperReport wrap : listWrapReport) {
            if(wrap.isSelected == true) {
                selectedReport.add(wrap.ac);
            }
        }
        
        return selectedReport;
    }
    /**
     * This method is used to get the list of Record Type that are selected from the page
     */
    public List<RecordType> getRecordTypes() {
        List<RecordType> selectedRecordType = new List<RecordType>();
        
        for(WrapperRecordType wrap : listWrapRecordType) {
            if(wrap.isSelected == true) {
                selectedRecordType.add(wrap.ac);
            }
        }
        
        return selectedRecordType;
    }
    /**
     * This method is used to get the list of Profile that are selected from the page
     */
    public List<Profile> getProfiles() {
        List<Profile> selectedProfiles = new List<Profile>();
        
        for(WrapperProfile wrap : listWrapProfile) {
            if(wrap.isSelected == true) {
                selectedProfiles.add(wrap.ac);
            }
        }
        
        return selectedProfiles;
    }
    
   
   /**
     * This method is used to get Fields or Objects that are created or modified after the 
     * search date-time. Two parameters sObjName, for which check fields or Object is  
     * created or last modified later and index is used to assign a number to SObject that 
     * used to relate all the fields and layout to this SObject. First we check for the   
     * SObject if it is created or Last Modified after the date then fetch all fields else 
     * get only those fields that are created or last modified later. 
     */
    
    public void getCustomFields(String sObjName,Integer index) {
      if(sObjName == null) {
        return;
       }
       Wrapper sObj;
      if(mapSObjects.containsKey(sObjName)) {
        sObj =  mapSObjects.get(sObjName); 
       sObj.wrapList = new list<Wrapper>();
        //checking SObject`s  Last Modified date 
       if(( dateClause.equals('LastModifiedDate') && searchdateTime <= sObj.lastModifiedDate) || (dateClause.equals('CreatedDate') && searchdateTime <= sObj.createddate))
       {
         if(customFieldMap.get(sObjName) != null) {
           sObj.wrapList.addAll(customFieldMap.get(sObjName));
           if(sObjName.contains('__c')) {
             listCstmObj.add(sObj);//updating list of Custom Objects    
              }
            }
          if(mapLayouts.get(sObjName) != null) {//adding the layouts to the SObject
            sObj.listLayouts.addAll(mapLayouts.get(sObjName));
            sObj.isLayoutExist = true;  
            } else {
            sObj.isLayoutExist = false;
            }
        sObj.index = index;
        sObj.fieldsAdded = true;
        //checking SObject`s Created Date
        } /*else if(dateClause.equals('CreatedDate') && searchdateTime <= sObj.createddate) {
            if(customFieldMap.get(sObjName) != null) {
              if(sObjName.contains('__c')) {
                listCstmObj.add(mapSObjects.get(sObjName)); 
                }
              mapSObjects.get(sObjName).wrapList.addAll(customFieldMap.get(sObjName));
            }
            if(mapLayouts.get(sObjName) != null) {
              mapSObjects.get(sObjName).listLayouts.addAll(mapLayouts.get(sObjName));
              mapSObjects.get(sObjName).isLayoutExist = true;   
              } else {
              mapSObjects.get(sObjName).isLayoutExist = false;//if not a single layout present for SObject
                }
           mapSObjects.get(sObjName).index= index;
           mapSObjects.get(sObjName).fieldsAdded = true;
           //checking SObject`s Fields last modified date and Created Date  
           } */ 
           
         else if( customFieldMap.get(sObjName) != null) {   
              for(Wrapper field : customFieldMap.get(sObjName)) {
                if((dateClause.equals('LastModifiedDate') && searchdateTime <= field.lastModifiedDate)||(dateClause.equals('CreatedDate') && searchdateTime <= field.createdDate) ){
                    sObj =  mapSObjects.get(sObjName); 
                  sObj.wrapList.add(field);
                  sObj.index= index;
                  if(!setSObj.contains(sObjName) && sObjName.contains('__c')) {
                    setSObj.add(sObjName);
                    listCstmObj.add(sObj);  
                    }else if(!setSObj.contains(sObjName)) {
                       listStandardObj.add(sObj);
                       setSObj.add(sObjName);
                    }
                 } /*else if (dateClause.equals('CreatedDate') && searchdateTime <= field.createdDate) {
                     mapSObjects.get(sObjName).wrapList.add(field);
                     mapSObjects.get(sObjName).index= index;
                     if(!setSObj.contains(sObjName) && sObjName.contains('__c')) {
                       setSObj.add(sObjName);
                       listCstmObj.add(mapSObjects.get(sObjName));  
                      } else if(!setSObj.contains(sObjName)) {
                        listStandardObj.add(mapSObjects.get(sObjName));
                        setSObj.add(sObjName);
                        }
                      }*/
                    }
                  }
                }
              }
  /**
   * This method is used to fetch name, created date and last modified date of Data type
   * passes as parameter.
   */
   public list<Wrapper> fetchData(String types) {
     List<Wrapper> wrapperList = new List<Wrapper>();
     double asOfVersion = 21.0;
    
     soapMetadata.FileProperties[] lstObj;
     soapMetadata.ListMetadataQuery query = new soapMetadata.ListMetadataQuery();
    
         soapMetadata.MetadataPort sm = new soapMetadata.MetadataPort();
         sm.SessionHeader = new soapMetadata.SessionHeader_element();
         sm.SessionHeader.sessionId = userinfo.getsessionid();
         sm.endpoint_x = METADATAAPI_ENDPOINT;
         query.type_x = types;
          if(!test.isRunningTest()) {
           lstObj = sm.listMetadata(new soapMetadata.ListMetadataQuery[] {query}, asOfVersion);
           if (lstObj != null) {
           if(types == 'Layout') { // for layout type update layout Map
             for (soapMetadata.FileProperties n : lstObj) {
                 Wrapper wrap = new Wrapper(n.fullName.split('-')[1],n.createdDate,n.lastModifiedDate);
                 wrap.parentName = n.fullName.split('-')[0];
                 if(mapLayouts.containskey(n.fullName.split('-')[0])) {
                
                     mapLayouts.get(n.fullName.split('-')[0]).add(wrap);
                 } else {
                     list<Wrapper> layoutslist = new list<Wrapper>();
                     layoutslist.add(wrap);
                     mapLayouts.put(n.fullName.split('-')[0],layoutslist);
                   }    
            }
            //return wrapperList;
          }else if(types == 'CustomObject') { // fill the map with Sobject type
             for (soapMetadata.FileProperties n : lstObj) {
               Wrapper wrap = new Wrapper(n.fullName,n.createdDate,n.lastModifiedDate);
               mapSObjects.put(n.fullName,wrap);
              }
            // return wrapperList;  
            }else if(types == 'CustomField') { // fill the custom field map 
               for (soapMetadata.FileProperties n : lstObj) {
                 String sObjParentName = n.fullName.split('\\.')[0];//getting name of SObjet whose field is this
                 String fieldName = n.fullName.split('\\.')[1];//getting Fields name
                 Wrapper cstmFld = new Wrapper(fieldName,  n.createdDate, n.lastModifiedDate );
                 cstmFld.parentName = sObjParentName;
                 if(customFieldMap.containsKey(sObjParentName)) {
                       customFieldMap.get(sObjParentName).add(cstmFld);
                 } else {
                      list<Wrapper> listFields = new list<Wrapper>();
                      listFields.add(cstmFld);
                      customFieldMap.put(sObjParentName,listFields);
                }
              }
             //return wrapperList;
            } else {
             for (soapMetadata.FileProperties n : lstObj) {
               if((dateClause.equals('LastModifiedDate') && searchdateTime <= n.lastModifiedDate) || (dateClause.equals('CreatedDate') && searchdateTime <= n.createdDate)) {
                 wrapper wrap = new wrapper(n.fullName,  n.createdDate, n.lastModifiedDate );
                 wrapperList.add(wrap);
               }
             }
           } 
         }
    }
    return wrapperList;         
   
  }
    /**
    * in this method we get the data in the list of different data types
    */
    public void fillData(){
    //initialising all required  variables 
    
      customFieldMap = new Map<String,list<wrapper>>();
      mapSObjects = new Map<string,Wrapper>();
      mapStdObj = new Map<string,Wrapper>();
      mapLayouts = new Map<String,list<Wrapper>>();
      listCustomLabel = new list<wrapper>();
      listWorkflow = new list<wrapper>();
      listCustomTab = new list<Wrapper>();
      listCstmObj = new List<Wrapper>();
      listStandardObj = new List<Wrapper>();
      listAnalyticSnapshot = new List<Wrapper>();
      listReportType = new List<Wrapper>();
      setSObj = new set<String>();
      Integer counts = 100;//count to use to bind the fields with the object at page
      
      fetchData('Layout');//fill the layout map 
      fetchData('CustomObject');// fill the SObject Map
      fetchData('CustomField');//fill the Custom Fields Map
      for(Wrapper sobjName : mapSObjects.values()) { //getting all custom fields modified/created after search date
        getCustomFields(sobjName.apiName,counts);
        counts++;
       } 
      listCustomLabel = fetchData('CustomLabel');
      listCustomTab = fetchData('CustomTab');
      listWorkflow = fetchData('Workflow');
      listAnalyticSnapshot = fetchData('AnalyticSnapshot');    
      listReportType = fetchData('ReportType');
    }  
    /**
     * Creating the XML for selected items
     */
    public Pagereference fetchXML() {
      XMLString = '<?xml version="1.0" encoding="UTF-8"?>'+
                  '\n<Package xmlns="http://soap.sforce.com/2006/04/metadata">';
      
      if(getClasses().size()>0) {
        XMLString += '\n<types>'; 
        for(ApexClass cls : getClasses()) {
          XMLString += '\n        <members>' + cls.name + '</members>';
         }
        XMLString += ' \n    <name>ApexClass</name>';
        XMLString += ' \n</types>';
      }
     
      if(getPages().size()>0) { 
        XMLString += '\n<types>';  
        for(ApexPage obj : getPages()) {
          XMLString += '\n        <members>' + obj.name + '</members>';
        }
        XMLString += ' \n    <name>ApexPage</name>';
        XMLString += ' \n</types>';
      }
     
     if(getTriggers().size()>0) {
       XMLString += '\n<types>'; 
       for(ApexTrigger obj : getTriggers()) {
         XMLString += '\n        <members>' + obj.name + '</members>';
        }
        XMLString += ' \n    <name>ApexTrigger</name>';
        XMLString += ' \n</types>';
      }
     
     if(getComponents().size()>0) {
       XMLString += '\n<types>';  
       for(ApexComponent obj : getComponents()) {
         XMLString += '\n        <members>' + obj.name + '</members>';
         }
        XMLString += ' \n    <name>ApexComponent</name>';
        XMLString += ' \n</types>';
     }
     
     if(getStaticResources().size()>0) {
       XMLString += '\n<types>';  
       for(StaticResource obj : getStaticResources()) {
          XMLString += '\n        <members>' + obj.name + '</members>';
         }
        XMLString += ' \n    <name>StaticResource</name>';            
        XMLString += ' \n</types>';
     }
     if(getWeblinks().size()>0) {
         XMLString += '\n<types>';  
     
         for(Weblink obj : getWeblinks()) {
            XMLString += '\n        <members>' + obj.PageOrSobjectType + '.' + obj.name + '</members>';
         }
    XMLString += ' \n    <name>Weblink</name>';
    XMLString += ' \n</types>';
     }
     if(getScontrols().size()>0) {
         XMLString += '\n<types>';  
     
         for(Scontrol obj : getScontrols()) {
            XMLString += '\n        <members>' + obj.name + '</members>';
         }
    XMLString += ' \n    <name>Scontrol</name>';
    XMLString += ' \n</types>';
     }
     if(getDashboards().size()>0) {
         XMLString += '\n<types>';  
    
         for(Dashboard obj : getDashboards()) {
            XMLString += '\n        <members>' + obj.title + '</members>';
         }
        XMLString += ' \n    <name>Dashboard</name>';
        XMLString += ' \n</types>';
     }
     if(getReports().size()>0) {
         XMLString += '\n<types>';  
     
         for(Report obj : getReports()) {
            XMLString += '\n        <members>' + obj.DeveloperName + '</members>';
         }
    XMLString += ' \n    <name>Report</name>';
    XMLString += ' \n</types>';
     }
     if(getRecordTypes().size()>0) {
         XMLString += '\n<types>';  
     
         for(RecordType obj : getRecordTypes()) {
            XMLString += '\n        <members>' + obj.name + '</members>';
         }
        XMLString += ' \n    <name>RecordType</name>';
        XMLString += ' \n</types>';
     }
     if(getProfiles().size()>0) {
         XMLString += '\n<types>';  
     
         for(Profile obj : getProfiles()) {
            XMLString += '\n        <members>' + obj.name + '</members>';
         }
        XMLString += ' \n    <name>Profile</name>';
        XMLString += ' \n</types>';
     }
      if(getSelectedObj(listWorkflow).size()>0) {
         XMLString += '\n<types>';  
     
         for(wrapper obj : getSelectedObj(listWorkflow)) {
            XMLString += '\n        <members>' + obj.apiName + '</members>';
         }
        XMLString += ' \n    <name>Workflow</name>';
        XMLString += ' \n</types>';
     }
      if(getselectedObj(listCustomLabel).size()>0) {
        XMLString += '\n<types>';  
        for(wrapper obj : getselectedObj(listCustomLabel)) {
          XMLString += '\n        <members>' + obj.apiName + '</members>';
         }
        XMLString += ' \n    <name>CustomLabel</name>';
        XMLString += ' \n</types>';
     }
     if(getselectedObj(listCustomTab).size()>0) {
         XMLString += '\n<types>';  
         for(Wrapper obj : getselectedObj(listCustomTab)) {
            XMLString += '\n        <members>' + obj.apiName + '</members>';
         }
        XMLString += ' \n    <name>CustomTab</name>';
        XMLString += ' \n</types>';
     }
     if(getselectedObj(listStandardObj).size()>0) {
         XMLString += '\n<types>';  
         for(Wrapper sobj : getselectedObj(listStandardObj)) {
            XMLString += '\n        <members>' + sobj.apiName + '</members>';
         }
        XMLString += ' \n    <name>Object</name>';
        XMLString += ' \n</types>';
     }
     if(getselectedObj(listCstmObj).size()>0) {
         XMLString += '\n<types>';  
         for(Wrapper sobj : getselectedObj(listCstmObj)) {
            XMLString += '\n        <members>' + sobj.apiName + '</members>';
         }
        XMLString += ' \n    <name>CustomObject</name>';
        XMLString += ' \n</types>';
     }
     if(getselectedObjCustomfields().size()>0) {
       XMLString += '\n<types>';  
       for(Wrapper obj : getselectedObjCustomfields()) {
         XMLString += '\n        <members>' + obj.parentName + '.' + obj.apiName + '</members>';
       }
     }
     if(getselectedCustomfields().size()>0) {
       if(!(getselectedObjCustomfields().size()>0)){
         XMLString += '\n<types>';  
        }
       for(Wrapper obj : getselectedCustomfields()) {
         XMLString += '\n        <members>' + obj.parentName + '.' + obj.apiName + '</members>';
        }
        XMLString += ' \n    <name>CustomField</name>';
        XMLString += ' \n</types>';
     }
      if(getselectedlayouts().size()>0) {
         XMLString += '\n<types>';  
         for(string sobj : getselectedlayouts()) {  
            XMLString += '\n        <members>' + sobj + '</members>';
         }
        XMLString += ' \n    <name>Layout</name>';
        XMLString += ' \n</types>';
      }
      
      if(getSelectedObj(listAnalyticSnapshot).size()>0) {
        XMLString += '\n<types>';  
        for(wrapper obj : getSelectedObj(listAnalyticSnapshot)) {
          XMLString += '\n        <members>' + obj.apiName + '</members>';
         }
        XMLString += ' \n    <name>AnalyticSnapshot</name>';
        XMLString += ' \n</types>';
      }
       if(getSelectedObj(listReportType).size()>0) {
        XMLString += '\n<types>';  
        for(wrapper obj : getSelectedObj(listReportType)) {
          XMLString += '\n        <members>' + obj.apiName + '</members>';
         }
        XMLString += ' \n    <name>ReportType</name>';
        XMLString += ' \n</types>';
      }
     
     XMLString += '\n <version>20.0</version>'+
                   '\n </Package>';
     return null;
   }
   
    public PageReference DeploymentArtifactsV2Excel() {
      return Page.DeploymentArtifactsV2Excel;
   }
 // Make the two queries dynamic based on what date is chosen.
 
     public void showArtifacts()
      {
        system.debug('calling1='+searchdate);
        if(searchdate!='' && searchdate != null){
           try{
            searchdateTime = date.parse(searchdate);
            isValidDate = true;
            } catch (Exception e){
              ApexPages.addMessages(e);
              isValidDate = false;
            }
         }
        fillData();
      }
      
      /**
       * Wrapper Classes For All Types of data
       */
      public class WrapperApexClass {
        public ApexClass ac{get;set;}
        public Boolean isSelected{get;set;}
        public WrapperApexClass(ApexClass ac) {
            isSelected = false;
            this.ac = ac;
        }
        
    }
      public class WrapperApexPage {
        public ApexPage ac{get;set;}
        public Boolean isSelected{get;set;}
        public WrapperApexPage(ApexPage ac) {
            isSelected = false;
            this.ac = ac;
        }
        
    }
      public class WrapperApexTrigger {
        public ApexTrigger ac{get;set;}
        public Boolean isSelected{get;set;}
        public WrapperApexTrigger(ApexTrigger ac) {
            isSelected = false;
            this.ac = ac;
        }
        
    }
      public class WrapperApexComponent {
        public ApexComponent ac{get;set;}
        public Boolean isSelected{get;set;}
        public WrapperApexComponent(ApexComponent ac) {
            isSelected = false;
            this.ac = ac;
        }
        
    }
      public class WrapperScontrol {
        public Scontrol ac{get;set;}
        public Boolean isSelected{get;set;}
        public WrapperScontrol(Scontrol ac) {
            isSelected = false;
            this.ac = ac;
        }
        
    }
      public class WrapperProfile{
        public Profile ac{get;set;}
        public Boolean isSelected{get;set;}
        public WrapperProfile(Profile ac) {
            isSelected = false;
            this.ac = ac;
        }
        
    }
      public class WrapperDashbord {
        public Dashboard ac{get;set;}
        public Boolean isSelected{get;set;}
        public WrapperDashbord(Dashboard ac) {
            isSelected = false;
            this.ac = ac;
        }
        
    }
      public class WrapperStaticResource {
        public StaticResource ac{get;set;}
        public Boolean isSelected{get;set;}
        public WrapperStaticResource(StaticResource ac) {
            isSelected = false;
            this.ac = ac;
        }
        
    }
      public class WrapperRecordType {
        public RecordType ac{get;set;}
        public Boolean isSelected{get;set;}
        public WrapperRecordType(RecordType ac) {
            isSelected = false;
            this.ac = ac;
        }
        
    }
      public class WrapperWeblink {
        public Weblink ac{get;set;}
        public Boolean isSelected{get;set;}
        public WrapperWeblink(Weblink ac) {
            isSelected = false;
            this.ac = ac;
        }
        
    }
      public class WrapperReport {
        public Report ac{get;set;}
        public Boolean isSelected{get;set;}
        public WrapperReport(Report ac) {
            isSelected = false;
            this.ac = ac;
        }
        
    }
    
      public class Wrapper {
        public boolean isSelected{get;set;}
        public boolean isLayoutExist{get;set;}//to check if any layout exist
        public boolean fieldsAdded{get;set;}//to check if fields are already added
        
        public String apiName{get;set;}
        public String label{get;set;}
        public String parentName{get;set;}
        public Datetime createdDate{get;set;}
        public Datetime lastModifiedDate{get;set;}
        
        
        public list<String> listCstmFlds{get;set;}
        public list<Wrapper> wrapList{get;set;}
        public list<Wrapper> listLayouts{get;set;}
        public Integer index{get;set;}
        
        public Wrapper(String apiName) {
          this.apiName = apiName;
          this.isSelected = false;
          listCstmFlds = new list<String>();
          listLayouts = new List<Wrapper>();
          wrapList = new list<Wrapper>();
        }
        public Wrapper(String apiName,Datetime createDate,Datetime modDate) {
          this.apiName = apiName;
          this.isSelected = false;
          listCstmFlds = new list<String>();
          listLayouts = new List<Wrapper>();
          wrapList = new list<Wrapper>();
          createdDate = createDate;
          lastModifiedDate=modDate;
        }
     }
      
    
    
    //getting the selected itmes from the page
    
      public List<Wrapper> getselectedObj(List<Wrapper> wrapperList) {
        List<Wrapper> selectedObj = new List<Wrapper>();
        for(Wrapper sobj : wrapperList) {
          if(sobj.isSelected == true) {
           selectedObj.add(sobj);
          }
        }
       return selectedObj;
     }
     public List<Wrapper> getselectedObjCustomfields() {
        List<Wrapper> selectedFlds = new List<Wrapper>();
        for(Wrapper sobj : getselectedObj(listStandardObj)) 
            for(Wrapper wrap : mapSObjects.get(sobj.apiName).wraplist) {
                if(wrap.isSelected == true) {
                    selectedFlds.add(wrap);
                }
            }
        return selectedFlds;
      }
      public List<Wrapper> getselectedCustomfields() {
        List<Wrapper> selectedFlds = new List<Wrapper>();
        for(Wrapper sobj : getselectedObj(listCstmObj)) 
            for(Wrapper wrap : mapSObjects.get(sobj.apiName).wraplist) {
                if(wrap.isSelected == true) {
                    selectedFlds.add(wrap);
                }
             }
        return selectedFlds;
    }
      public List<String> getselectedLayouts() {
        List<String> selectedlayouts = new List<String>();
        for(Wrapper sobj : getselectedObj(listCstmObj)) 
            for(Wrapper wrap : mapSObjects.get(sobj.apiName).listLayouts) {
              if( wrap.isSelected) {
                String layoutName = wrap.parentName + '-' + wrap.apiName;
                selectedlayouts.add(layoutName);
                }
              }
        return selectedlayouts;
        }
    }