/********************************************************
* Name  : VendorDocsControllerExt.cls
* Author    : Jenna Werner
* Desc  : Controller Extension for Visualforce page for 
            tracking vendor documents 
            (TrackingVendorDocuments.page)
* Modification Log:
---------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------
Jenna Werner            01/19/2016      Original Version
********************************************************/

public with sharing class VendorDocsControllerExt {

    // Private variables
    private LLC_BI__Loan__c loan;
       
    // Publically exposed variables
    public List<LLC_BI__LLC_LoanDocument__c> listLoanDocument {get; set;}
    // listLoanDocument is the list of loan documents to be displayed in the Visualforce page
    public List<Vendor_Document_Fields__c> listField {get; set;}
    /* listField is the list of fields in the "Field Name" field on the custom setting -
        the values in this list form the columns of the table in the Visualforce page */
    public List<LLC_BI__LLC_LoanDocument__c> listLoanDocumentClone {get; set;}
    /* listLoanDocumentClone is a deep clone of listLoanDocument, used to compare
    to listLoanDocument and determine which loan document records have been updated */
    public Integer queryLimit;
    // queryLimit is the limit that is set for each query based on the getLimits() method
    public boolean showTable {get; set;}
    /* showTable is a boolean that can be set depending on whether or not there are 
        vendor documents to display in the table. This variable is accessed by the 
        Visualforce page - if true, the table is displayed; if false, the table is
        not displayed (and an informational message is shown to the user) */

    ///Constant variables
    private final static String VENDORDOCUMENT = System.label.Vendor_Document;
    /* VENDORDOCUMENT is a String reference to a custom label determining 
        the category "Vendor Documents" to indicate which types of
        loan documents should be retrieved */
    
    public VendorDocsControllerExt(ApexPages.StandardController stdController) {
        this.loan = (LLC_BI__Loan__c)stdController.getRecord();
        init();
        getListOfDocs();
    }

/* Method Name: getLimits
   Author Name: Jenna Werner
   Description: Method to retrieve limits for queries in other methods
   Parameters: none
   Returns: Integer queryLimit
*/       
    public Integer getLimits(){
        Integer limitDMLRows = Limits.getLimitDMLRows();
        Integer rowsUsedDML = Limits.getDMLRows();
        queryLimit = limitDMLRows - rowsUsedDML;
        return queryLimit;
    }

/* Method Name: throwExceptions
   Author Name: Jenna Werner
   Description: Void method to throw and log exceptions
   Parameters: Exception e, String label
   Returns: void
   */
    public void throwExceptions(Exception e, String label) {
        // Write exception to debug log and Visualforce page
        System.debug('The following exception has occurred: ' 
                        + e.getMessage());
        ApexPages.addMessage(new 
            ApexPages.message(ApexPages.severity.ERROR, label));
        
        // Log error
        LogFramework lf = LogFramework.getLogger();
        lf.createExceptionLogs(e);
        lf.commitLogs();
    }

/* Method Name: init
   Author Name: Jagannath Valaiyapathy
   Description: Method to initiate variables (lists, maps)
   Parameters: none
   Returns: void
*/
    private void init(){
        listField = new List<Vendor_Document_Fields__c>();
        listLoanDocument = new List<LLC_BI__LLC_LoanDocument__c>();
        listLoanDocumentClone = new List<LLC_BI__LLC_LoanDocument__c>();
    }

/* Method Name: getListofDocs
   Author Name: Jenna Werner
   Description: Getter method for list of most recent loan
                    documents for a loan
   Parameters: none
   Returns: void
*/
    public void getListOfDocs() {
        List<LLC_BI__LLC_LoanDocument__c> listLoanDocTemp;
        // listLoanDocTemp stores a temporary, not de-duped list of loan documents
        getTableFields();
        listLoanDocTemp = queryTableData();

        /* If listloanDocTemp is not null or empty, display table and call
            the buildLatestDocs() method */
        if(listLoanDocTemp != null && !listLoanDocTemp.isEmpty()) {
            buildLatestDocs(listLoanDocTemp);    
            showTable = true;
        }     
        // Else, do not display the table, and display an informational message   
        else {
            showTable = false;
            ApexPages.addMessage(new 
                ApexPages.message(ApexPages.severity.Info, 
                    System.Label.Vendor_Doc_Table_Informational_Message));
        }
    }

/* Method Name: getTableFields
   Author Name: Jenna Werner
   Description: Query custom setting to get fields to be used 
                    as columns in Visualforce table
   Parameters: none
   Returns: void
*/
    private void getTableFields() {
        try {
            // Query custom setting for specified fields in ascending order
            getLimits();
            listField = [SELECT 
                            FieldName__c, 
                            IsEditable__c, 
                            Order__c,
                            Name 
                        FROM 
                            Vendor_Document_Fields__c 
                        ORDER BY 
                            Order__c ASC 
                        LIMIT :queryLimit];
        }
        catch (QueryException qe){
            throwExceptions(qe, System.Label.Vendor_Doc_Table_Display_Error);
        }
    }

/* Method Name: queryTableData
   Author Name: Jenna Werner
   Description: Iterate through list result from previous
                    method to use for query of Loan Document
                    and store in map to be used in Visualforce
                    table as columns
   Parameters: none
   Returns: List<LLC_BI__LLC_LoanDocument__c> listLoanDocTemp
*/
    private List<LLC_BI__LLC_LoanDocument__c> queryTableData() {
            String query = 'SELECT ';
            List<LLC_BI__LLC_LoanDocument__c> listLoanDocTemp = new List<LLC_BI__LLC_LoanDocument__c>();
            for (Vendor_Document_Fields__c field : listField) {
                /* Iterate through list of fields in custom setting
                   Add fields to query variable */
                query += field.FieldName__c + ', ';
            }
            // Finish construction of query by adding to query variable
            getLimits();
            query += 'Id, '
                  + 'CreatedDate, '
                  + 'Identifier__c '
                + 'FROM '
                  + 'LLC_BI__LLC_LoanDocument__c '
                + 'WHERE '
                  + 'LLC_BI__Loan__c = \''
                  + loan.id +'\' '
                + 'AND '
                  +' LLC_BI__docType__r.Name = :VENDORDOCUMENT'
                + ' LIMIT :queryLimit';
            System.debug('!@QueryLimit' + queryLimit);
            System.debug('!Query: ' + query);

            try {
                // Store query results in list
                listLoanDocTemp = Database.query(query);
                System.debug('$$$$$$$$+ ' + listLoanDocTemp);
            }
            catch(QueryException qe) {
                throwExceptions(qe, System.Label.Vendor_Doc_Table_Display_Error);
            }
            return listLoanDocTemp;
        }

/* Method Name: buildLatestDocs
   Author Name: Jenna Werner
   Description: Iterate through list of Loan Documents
                    to find most recent versions
   Parameters: List<LLC_BI__LLC_LoanDocument__c> listLoanDocTemp
   Returns: void
*/
    private void buildLatestDocs(List<LLC_BI__LLC_LoanDocument__c> listLoanDocTemp) {
        Map<String, LLC_BI__LLC_LoanDocument__c> mapLoanDoc = new Map<String, LLC_BI__LLC_LoanDocument__c>();
        /* mapLoanDoc is a map containing loan document Identifier__c field values as its key,
            and their loan document sObjects as its value. It ultimately stores the loan documents 
            after they have been de-duped (holding only the most recent version of each) */
        if (listLoanDocTemp != null && !listLoanDocTemp.isempty()) {
            for(LLC_BI__LLC_LoanDocument__c loanDoc : listLoanDocTemp) {
                /* Iterate through query results to find most recent version 
                    of loan document and add Identifier field and 
                    Loan Document to map */
                if(mapLoanDoc.keySet().contains(loanDoc.Identifier__c)) {
                    LLC_BI__LLC_LoanDocument__c tempLoanDoc = new LLC_BI__LLC_LoanDocument__c();
                    tempLoanDoc = mapLoanDoc.get(loanDoc.Identifier__c);
                    if(tempLoanDoc.createdDate < loanDoc.createdDate){
                        mapLoanDoc.put(loanDoc.Identifier__c,loanDoc);
                    }
                }
                else {
                    // Add each loan document and its identifier to map
                    mapLoanDoc.put(loanDoc.Identifier__c,loanDoc);
                }
            }
            if(mapLoanDoc != null && !mapLoanDoc.isempty()){
                /* If map is not null or empty, add values from 
                    map to list*/
                listLoanDocument = mapLoanDoc.values();
                /* Create a deep clone of mapLoanDoc
                    in order to use in save method */
                listLoanDocumentClone = listLoanDocument.deepClone(true,true,true);
            }
        }
    }

/* Method Name: save
   Author Name: Jenna Werner
   Description: Save method for Visualforce page edits; updates
                    loan document records (only ones which have 
                    been edited)
   Parameters: none
   Returns: PageRefernence
*/   
    public PageReference save() {
            List<LLC_BI__LLC_LoanDocument__c> listDocsToUpdate = new List<LLC_BI__LLC_LoanDocument__c>();
            // listDocsToUpdate is a list of loan documents which have been edited, and will ultimately be updated
            Map<Id, LLC_BI__LLC_LoanDocument__c> mapOriginalLoanDoc = new Map<Id, LLC_BI__LLC_LoanDocument__c>();
            /* mapOriginalLoanDoc is a map containing the ID and sObject of the original set of 
                loan documents (before any editing took place) */
            List<Database.SaveResult> result = new List<Database.SaveResult>();
            try {
                Map<Id, LLC_BI__LLC_LoanDocument__c> mapEditedLoanDoc = new Map<Id, LLC_BI__LLC_LoanDocument__c>(listLoanDocument);
                /* mapEditedLoanDoc is a map containing the ID and sObject of the set of loan documents after editing took place - 
                    including both edited and unedited records */
                mapOriginalLoanDoc = new Map<ID, LLC_BI__LLC_LoanDocument__c>(listLoanDocumentClone);
                // result is a list of database Save Results
                    /* Determine which loan documents have been edited,
                        and store them in a new list */
                for (ID loanDocument : mapOriginalLoanDoc.keySet()) {
                    if (mapOriginalLoanDoc.get(loanDocument) != mapEditedLoanDoc.get(loanDocument)) {
                        listDocsToUpdate.add(mapEditedLoanDoc.get(loanDocument));
                    }
                }
                // Update only the loan documents which have been edited
                result = Database.update(listDocsToUpdate, false);
            }
            catch(Exception e) {
                throwExceptions(e, System.Label.Vendor_Doc_Table_Display_Error);
            }
                // Iterate through results to check for errors
            for (Database.SaveResult sr : result) {
                if (!sr.isSuccess()) {
                    for (Database.Error error : sr.getErrors()) {
                        // Write error to debug log and Visualforce page
                        System.debug('The following error has occured.');
                        System.debug(error.getStatusCode() + ': ' 
                                        + error.getMessage());
                        apexPages.addMessage(new 
                            ApexPages.message(ApexPages.severity.ERROR, 
                                System.Label.Vendor_Doc_Table_Display_Error));
                    }
                }
            }
            return null;
    }

/* Method Name: cancel
   Author Name: Jenna Werner
   Description: Cancel edit of Visualforce page
   Parameters: none
   Returns: PageRefernence
*/   
    public PageReference cancel() {
        return null;
    }

/* Method Name: refresh
   Author Name: Jenna Werner
   Description: Refresh (reinitiate) Visualforce page
   Parameters: none
   Returns: PageRefernence
*/   
    public PageReference refresh() {
        init();
        getListOfDocs();
        return null;
    }
}