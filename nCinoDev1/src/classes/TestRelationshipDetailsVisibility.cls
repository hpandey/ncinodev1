@isTest(SeeAllData=false)
public class TestRelationshipDetailsVisibility{
     public testmethod static void testing(){
        
        
        Profile p=[select id from Profile where name='Standard User'];
        User testUser= new User(Username='testtest@testingtestng.com', LastName='test', Email='test@test.com', Alias='tes', CommunityNickname='tes',ProfileID=p.ID,TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', LanguageLocaleKey='en_US',EmailEncodingKey='ISO-8859-1');
        
        insert testUser;
        
        Relationship__c relationship=new Relationship__c(Relationship_ID__c='3456gbjds44');
        
        insert relationship;
        
        Account acct= new Account(name='testAccount',Relationship__c=relationship.id);
        
        insert acct;
        
        AccountTeamMember atm= new AccountTeamMember(accountId=acct.id,userId=testUser.id);
        
        insert atm;
        
        Restricted_Relationship_Information__c rri=new Restricted_Relationship_Information__c(Relationship__c=relationship.id); 
        
        insert rri;
        
        PDM__c pdm= new PDM__c(Relationship__c=relationship.id,Date_Completed__c=Date.today());
        
        insert pdm;
        
        Relationship_Strategy_Plan__c rsp= new Relationship_Strategy_Plan__c(relationship__c=relationship.id);
        
        insert rsp;    
        
        RelationshipDetailsVisibilityUtil.recalculateSharingOnSubmit(relationship.ID);
        
        System.AssertEquals([select id from PDM__Share where UserOrGroupID=: testUser.id].isEmpty(),false);
        System.AssertEquals([select id from Restricted_Relationship_Information__Share where UserOrGroupID=: testUser.id].isEmpty(),false);
        System.AssertEquals([select id from Relationship_Strategy_Plan__Share where UserOrGroupID=: testUser.id].isEmpty(),false);
        
        delete atm;
        
        RelationshipDetailsVisibilityUtil.recalculateSharingOnSubmit(relationship.ID);
        
        System.AssertEquals([select id from PDM__Share where UserOrGroupID=: testUser.id].isEmpty(),true);
        System.AssertEquals([select id from Restricted_Relationship_Information__Share where UserOrGroupID=: testUser.id].isEmpty(),true);
        System.AssertEquals([select id from Relationship_Strategy_Plan__Share where UserOrGroupID=: testUser.id].isEmpty(),true);
        
        RelationshipDetailsVisibilityBatch emptyBatch= new RelationshipDetailsVisibilityBatch(null,null);
        RelationshipDetailsVisibilityBatch batch= new RelationshipDetailsVisibilityBatch();
        Id processID= Database.executeBatch(batch);
    }
}