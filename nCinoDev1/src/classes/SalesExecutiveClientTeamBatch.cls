global class SalesExecutiveClientTeamBatch implements Database.batchable<sObject>, Database.Stateful{
    global final String Query;
       
    global SalesExecutiveClientTeamBatch(){       
       Query = 'Select Id, OwnerId, Owner.C_I_Type__c, Owner.Sales_Executive__c From Account where Owner.C_I_Type__c = \'Corporate\' and Owner.Sales_Executive__c <> null';
    } 
   
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Account> AcctList){
    	//The original query and logic have been optimized to troubleshoot errors generated during batch run... Ephrem 10-2015	
        if(AcctList.size() > 0){        	
        	List<AccountShare> AccntShares = new List<AccountShare>();
        	List<AccountTeamMember> ATMS = new List<AccountTeamMember>();
        	Set<Id> sId = new Set<Id>();
			Set<Id> aId = new Set<Id>();
			List<AccountShare> acSh = new List<AccountShare>();
        	Map<Id,AccountShare> mapShare1 = new Map<Id,AccountShare>();
			Map<Id,AccountShare> mapShare2 = new Map<Id,AccountShare>();
        	
        	for(Account a:AcctList){
        		sId.add(a.Owner.Sales_Executive__c);
				aId.add(a.Id);
        	}
        	integer w = aId.size();
        	integer x = sId.size();
        	
        	acSh = [select AccountId, UserorGroupId FROM AccountShare WHERE  AccountId IN:aId AND userorgroupid IN: sId ];
        	
        	for(AccountShare ash:acSh){
				mapShare1.put(ash.UserorGroupId, ash);
				mapShare2.put(ash.AccountId, ash);
			}
			integer y = mapShare1.size();
			integer z = mapShare2.size();
			
			if(x>y || w>z){
        		for (Account ac: AcctList){
        			if(!mapShare1.containsKey(ac.Owner.Sales_Executive__c) || !mapShare2.containsKey(ac.Id)){
	        		    string userId = ac.Owner.Sales_Executive__c;
	            		string acctId = ac.ID;
	            		string accLvl = 'Edit';
	            		string conLvl = 'Edit';
	            		string caseLvl = 'Edit';
	            		string oppLvl = 'Edit';
	            		AccountShare a = New AccountShare(UserOrGroupId = userId, AccountId=acctId, AccountAccessLevel = accLvl,
	                       ContactAccessLevel = conLvl, CaseAccessLevel = CaseLvl, OpportunityAccessLevel = OppLvl);
	            		AccntShares.add(a);
	             
	            		//Add AccountTeamMemeber Record
	            		string aUserId = ac.Owner.Sales_Executive__c;
	            		string aAcctId = ac.ID;
	            		string teamRole = 'Team';
	            		AccountTeamMember atm = New AccountTeamMember(UserId = aUserId, AccountID = aAcctId, 
	                      TeamMemberRole = teamRole);
	            		ATMS.add(atm);
        			}
				}
        		try{
        			insert AccntShares;	    		    		
	        		insert ATMS;	
	        		AccountDetailsVisibilityUtil.recalculateSharing(AcctList);
            	}catch(DMLException e){
        			System.debug('DMLException:'+e.getMessage());                                         
    			}
			}
    	}        
    }

    global void finish(Database.BatchableContext BC){      
    }  
}