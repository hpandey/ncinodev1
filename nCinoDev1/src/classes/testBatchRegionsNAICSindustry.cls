//-------------------------------------------------------------------------------------------------
//Case 2313... test class for batchRegionsNAICSindustry class... 
//Ephrem Tekle 02/2016...
//-------------------------------------------------------------------------------------------------
@isTest 
private class testBatchRegionsNAICSindustry {
    static testmethod void testRegionsIndustry1(){    	      
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rtId = AccountRecordTypeInfo .get('Client').getRecordTypeId();
        List<Account> lstAcc1 = new List<Account>();
        List<Account> lstAcc2 = new List<Account>();
        //temporarily allows the test class for the job execution to bypass a validation rule and trigger...
        Test.startTest();         
        insert new batchRunFree__c(Is_Batch_Excluded__c=true);
        insert new Trigger_Setting__c(Name='Account',IsActive__c=false);
        //
        Account acc1 = new Account(Name='Test Account1', RecordTypeId=rtId, NAICS_CODE__c='561499');
        lstAcc1.add(acc1);        
        Account acc2 = new Account(Name='Test Account2', RecordTypeId=rtId, NAICS_CODE__c='921110');
        lstAcc1.add(acc2);            
        Account acc3 = new Account(Name='Test Account3', RecordTypeId=rtId, NAICS_CODE__c='999999');
        lstAcc1.add(acc3);                
        Account acc4 = new Account(Name='Test Account4', RecordTypeId=rtId);
        lstAcc1.add(acc4);        
        Account acc5 = new Account(Name='Test Account5', RecordTypeId=rtId, NAICS_CODE__c='811111');
        lstAcc1.add(acc5);        
        Account acc6 = new Account(Name='Test Account6', RecordTypeId=rtId, NAICS_Description__c='Test Description1');
        lstAcc1.add(acc6);        
        Account acc7 = new Account(Name='Test Account7', RecordTypeId=rtId, NAICS_Code__c='', NAICS_Description__c='Test Description2');
        lstAcc1.add(acc7);        
        Account acc8 = new Account(Name='Test Account8', RecordTypeId=rtId, NAICS_CODE__c=null, NAICS_Description__c=null);
        lstAcc1.add(acc8);        
        Account acc9 = new Account(Name='Test Account9', RecordTypeId=rtId, NAICS_CODE__c='561110');
		lstAcc1.add(acc9);		
		Account acc10 = new Account(Name='Test Account10', RecordTypeId=rtId, NAICS_CODE__c='561611');
		lstAcc1.add(acc10);		
		Account acc11 = new Account(Name='Test Account11', RecordTypeId=rtId, NAICS_CODE__c='561910');
		lstAcc1.add(acc11);		
		Account acc12 = new Account(Name='Test Account12', RecordTypeId=rtId, NAICS_CODE__c='561710');
		lstAcc1.add(acc12);		
		Account acc13 = new Account(Name='Test Account13', RecordTypeId=rtId, NAICS_CODE__c='561510');
		lstAcc1.add(acc13);		
		Account acc14 = new Account(Name='Test Account14', RecordTypeId=rtId, NAICS_CODE__c='562111');
		lstAcc1.add(acc14);		
		Account acc15 = new Account(Name='Test Account15', RecordTypeId=rtId, NAICS_CODE__c='811310');
		lstAcc1.add(acc15);		
		Account acc16 = new Account(Name='Test Account16', RecordTypeId=rtId, NAICS_CODE__c='811211');
		lstAcc1.add(acc16);		
		Account acc17 = new Account(Name='Test Account17', RecordTypeId=rtId, NAICS_CODE__c='811411');
		lstAcc1.add(acc17);		
		Account acc18 = new Account(Name='Test Account18', RecordTypeId=rtId, NAICS_CODE__c='112111');
		lstAcc1.add(acc18);				      	
		Account acc19 = new Account(Name='Test Account19', RecordTypeId=rtId, NAICS_CODE__c='111110');
		lstAcc1.add(acc19);		
		Account acc20 = new Account(Name='Test Account20', RecordTypeId=rtId, NAICS_CODE__c='115111');
		lstAcc1.add(acc20);		
		Account acc21 = new Account(Name='Test Account21', RecordTypeId=rtId, NAICS_CODE__c='113110');
		lstAcc1.add(acc21);		
		Account acc22 = new Account(Name='Test Account22', RecordTypeId=rtId, NAICS_CODE__c='611410');
		lstAcc1.add(acc22);		
		Account acc23 = new Account(Name='Test Account23', RecordTypeId=rtId, NAICS_CODE__c='611310');
		lstAcc1.add(acc23);		
		Account acc24 = new Account(Name='Test Account24', RecordTypeId=rtId, NAICS_CODE__c='611710');
		lstAcc1.add(acc24);		
		Account acc25 = new Account(Name='Test Account25', RecordTypeId=rtId, NAICS_CODE__c='611110');
		lstAcc1.add(acc25);		
		Account acc26 = new Account(Name='Test Account26', RecordTypeId=rtId, NAICS_CODE__c='611210');
		lstAcc1.add(acc26);		
		Account acc27 = new Account(Name='Test Account27', RecordTypeId=rtId, NAICS_CODE__c='611610');
		lstAcc1.add(acc27);		
		Account acc28 = new Account(Name='Test Account28', RecordTypeId=rtId, NAICS_CODE__c='611511');
		lstAcc1.add(acc28);		
		Account acc29 = new Account(Name='Test Account29', RecordTypeId=rtId, NAICS_CODE__c='213113');
		lstAcc1.add(acc29);		
		Account acc30 = new Account(Name='Test Account30', RecordTypeId=rtId, NAICS_CODE__c='324122');
		lstAcc1.add(acc30);		
		Account acc31 = new Account(Name='Test Account31', RecordTypeId=rtId, NAICS_CODE__c='221210');
		lstAcc1.add(acc31);		
		Account acc32 = new Account(Name='Test Account32', RecordTypeId=rtId, NAICS_CODE__c='211111');
		lstAcc1.add(acc32);		
		Account acc33 = new Account(Name='Test Account33', RecordTypeId=rtId, NAICS_CODE__c='213111');
		lstAcc1.add(acc33);		
		Account acc34 = new Account(Name='Test Account34', RecordTypeId=rtId, NAICS_CODE__c='324110');
		lstAcc1.add(acc34);		
		Account acc35 = new Account(Name='Test Account35', RecordTypeId=rtId, NAICS_CODE__c='522310');
		lstAcc1.add(acc35);		
		Account acc36 = new Account(Name='Test Account36', RecordTypeId=rtId, NAICS_CODE__c='521110');
		lstAcc1.add(acc36);		
		Account acc37 = new Account(Name='Test Account37', RecordTypeId=rtId, NAICS_CODE__c='522110');
		lstAcc1.add(acc37);		
		Account acc38 = new Account(Name='Test Account38', RecordTypeId=rtId, NAICS_CODE__c='551111');
		lstAcc1.add(acc38);		
		Account acc39 = new Account(Name='Test Account39', RecordTypeId=rtId, NAICS_CODE__c='522220');
		lstAcc1.add(acc39);		
		Account acc40 = new Account(Name='Test Account40', RecordTypeId=rtId, NAICS_CODE__c='523910');
		lstAcc1.add(acc40);		
		Account acc41 = new Account(Name='Test Account41', RecordTypeId=rtId, NAICS_CODE__c='525190');
		lstAcc1.add(acc41);		
		Account acc42 = new Account(Name='Test Account42', RecordTypeId=rtId, NAICS_CODE__c='523110');
		lstAcc1.add(acc42);		
		Account acc43 = new Account(Name='Test Account43', RecordTypeId=rtId, NAICS_CODE__c='532210');
		lstAcc1.add(acc43);		
		Account acc44 = new Account(Name='Test Account44', RecordTypeId=rtId, NAICS_CODE__c='524126');
		lstAcc1.add(acc44);		
		Account acc45 = new Account(Name='Test Account45', RecordTypeId=rtId, NAICS_CODE__c='524113');
		lstAcc1.add(acc45);		
		Account acc46 = new Account(Name='Test Account46', RecordTypeId=rtId, NAICS_CODE__c='524210');
		lstAcc1.add(acc46);		
		Account acc47 = new Account(Name='Test Account47', RecordTypeId=rtId, NAICS_CODE__c='533110');
		lstAcc1.add(acc47);		
		Account acc48 = new Account(Name='Test Account48', RecordTypeId=rtId, NAICS_CODE__c='532299');
		lstAcc1.add(acc48);		
		Account acc49 = new Account(Name='Test Account49', RecordTypeId=rtId, NAICS_CODE__c='525120');
		lstAcc1.add(acc49);		
		Account acc50 = new Account(Name='Test Account50', RecordTypeId=rtId, NAICS_CODE__c='532111');
		lstAcc1.add(acc50);		
		Account acc51 = new Account(Name='Test Account51', RecordTypeId=rtId, NAICS_CODE__c='926110');
		lstAcc1.add(acc51);		
		Account acc52 = new Account(Name='Test Account52', RecordTypeId=rtId, NAICS_CODE__c='924110');
		lstAcc1.add(acc52);		
		Account acc53 = new Account(Name='Test Account53', RecordTypeId=rtId, NAICS_CODE__c='925110');
		lstAcc1.add(acc53);		
		Account acc54 = new Account(Name='Test Account54', RecordTypeId=rtId, NAICS_CODE__c='923110');
		lstAcc1.add(acc54);		
		Account acc55 = new Account(Name='Test Account55', RecordTypeId=rtId, NAICS_CODE__c='922110');
		lstAcc1.add(acc55);		
		Account acc56 = new Account(Name='Test Account56', RecordTypeId=rtId, NAICS_CODE__c='928110');
		lstAcc1.add(acc56);		
		Account acc57 = new Account(Name='Test Account57', RecordTypeId=rtId, NAICS_CODE__c='927110');
		lstAcc1.add(acc57);		
		Account acc58 = new Account(Name='Test Account58', RecordTypeId=rtId, NAICS_CODE__c='623311');
		lstAcc1.add(acc58);		
		Account acc59 = new Account(Name='Test Account59', RecordTypeId=rtId, NAICS_CODE__c='622110');
		lstAcc1.add(acc59);		
		Account acc60 = new Account(Name='Test Account60', RecordTypeId=rtId, NAICS_CODE__c='623110');
		lstAcc1.add(acc60);		
		Account acc61 = new Account(Name='Test Account61', RecordTypeId=rtId, NAICS_CODE__c='623210');
		lstAcc1.add(acc61);		
		Account acc62 = new Account(Name='Test Account62', RecordTypeId=rtId, NAICS_CODE__c='621910');
		lstAcc1.add(acc62);		
		Account acc63 = new Account(Name='Test Account63', RecordTypeId=rtId, NAICS_CODE__c='621991');
		lstAcc1.add(acc63);		
		Account acc64 = new Account(Name='Test Account64', RecordTypeId=rtId, NAICS_CODE__c='424210');
		lstAcc1.add(acc64);		
		Account acc65 = new Account(Name='Test Account65', RecordTypeId=rtId, NAICS_CODE__c='524114');
		lstAcc1.add(acc65);		
		Account acc66 = new Account(Name='Test Account66', RecordTypeId=rtId, NAICS_CODE__c='621610');
		lstAcc1.add(acc66);		
		Account acc67 = new Account(Name='Test Account67', RecordTypeId=rtId, NAICS_CODE__c='621511');
		lstAcc1.add(acc67);		
		Account acc68 = new Account(Name='Test Account68', RecordTypeId=rtId, NAICS_CODE__c='339112');
		lstAcc1.add(acc68);		
		Account acc69 = new Account(Name='Test Account69', RecordTypeId=rtId, NAICS_CODE__c='325411');
		lstAcc1.add(acc69);		
		Account acc70 = new Account(Name='Test Account70', RecordTypeId=rtId, NAICS_CODE__c='621210');
		lstAcc1.add(acc70);		
		Account acc71= new Account(Name='Test Account71', RecordTypeId=rtId, NAICS_CODE__c='621310');
		lstAcc1.add(acc71);		
		Account acc72 = new Account(Name='Test Account72', RecordTypeId=rtId, NAICS_CODE__c='621111');
		lstAcc1.add(acc72);		
		Account acc73 = new Account(Name='Test Account73', RecordTypeId=rtId, NAICS_CODE__c='621410');
		lstAcc1.add(acc73);		
		Account acc74 = new Account(Name='Test Account74', RecordTypeId=rtId, NAICS_CODE__c='515210');
		lstAcc1.add(acc74);		
		Account acc75 = new Account(Name='Test Account75', RecordTypeId=rtId, NAICS_CODE__c='518210');
		lstAcc1.add(acc75);		
		Account acc76 = new Account(Name='Test Account76', RecordTypeId=rtId, NAICS_CODE__c='519110');
		lstAcc1.add(acc76);		
		Account acc77 = new Account(Name='Test Account77', RecordTypeId=rtId, NAICS_CODE__c='512110');
		lstAcc1.add(acc77);		
		Account acc78 = new Account(Name='Test Account78', RecordTypeId=rtId, NAICS_CODE__c='511110');
		lstAcc1.add(acc78);		
		Account acc79 = new Account(Name='Test Account79', RecordTypeId=rtId, NAICS_CODE__c='511191');
		lstAcc1.add(acc79);		
		Account acc80 = new Account(Name='Test Account80', RecordTypeId=rtId, NAICS_CODE__c='517911');
		lstAcc1.add(acc80);		
		Account acc81 = new Account(Name='Test Account81', RecordTypeId=rtId, NAICS_CODE__c='515111');
		lstAcc1.add(acc81);		
		Account acc82 = new Account(Name='Test Account82', RecordTypeId=rtId, NAICS_CODE__c='517410');
		lstAcc1.add(acc82);		
		Account acc83 = new Account(Name='Test Account83', RecordTypeId=rtId, NAICS_CODE__c='512210');
		lstAcc1.add(acc83);		
		Account acc84 = new Account(Name='Test Account84', RecordTypeId=rtId, NAICS_CODE__c='517110');
		lstAcc1.add(acc84);		
		Account acc85 = new Account(Name='Test Account85', RecordTypeId=rtId, NAICS_CODE__c='212210');
		lstAcc1.add(acc85);		
		Account acc86 = new Account(Name='Test Account86', RecordTypeId=rtId, NAICS_CODE__c='325510');
		lstAcc1.add(acc86);		
		Account acc87 = new Account(Name='Test Account87', RecordTypeId=rtId, NAICS_CODE__c='332111');
		lstAcc1.add(acc87);		
		Account acc88 = new Account(Name='Test Account88', RecordTypeId=rtId, NAICS_CODE__c='326111');
		lstAcc1.add(acc88);		
		Account acc89 = new Account(Name='Test Account89', RecordTypeId=rtId, NAICS_CODE__c='331110');
		lstAcc1.add(acc89);		
		Account acc90 = new Account(Name='Test Account90', RecordTypeId=rtId, NAICS_CODE__c='336411');
		lstAcc1.add(acc90);		
		Account acc91 = new Account(Name='Test Account91', RecordTypeId=rtId, NAICS_CODE__c='333111');
		lstAcc1.add(acc91);		
		Account acc92 = new Account(Name='Test Account92', RecordTypeId=rtId, NAICS_CODE__c='336111');
		lstAcc1.add(acc92);		
		Account acc93 = new Account(Name='Test Account93', RecordTypeId=rtId, NAICS_CODE__c='336991');
		lstAcc1.add(acc93);		
		Account acc94 = new Account(Name='Test Account94', RecordTypeId=rtId, NAICS_CODE__c='336510');
		lstAcc1.add(acc94);		
		Account acc95 = new Account(Name='Test Account95', RecordTypeId=rtId, NAICS_CODE__c='623210');
		lstAcc1.add(acc95);
		Account acc96 = new Account(Name='Test Account96', RecordTypeId=rtId, NAICS_CODE__c='327110');
		lstAcc1.add(acc96);		
		Account acc97 = new Account(Name='Test Account97', RecordTypeId=rtId, NAICS_CODE__c='325611');
		lstAcc1.add(acc97);		
		Account acc98 = new Account(Name='Test Account98', RecordTypeId=rtId, NAICS_CODE__c='327910');
		lstAcc1.add(acc98);		
		Account acc99 = new Account(Name='Test Account99', RecordTypeId=rtId, NAICS_CODE__c='332992');
		lstAcc1.add(acc99);		
		Account acc100 = new Account(Name='Test Account100', RecordTypeId=rtId, NAICS_CODE__c='334111');
		lstAcc1.add(acc100); 
		Account acc101 = new Account(Name='Test Account101', RecordTypeId=rtId, NAICS_CODE__c='335110');
		lstAcc1.add(acc101);		 
		Account acc102 = new Account(Name='Test Account102', RecordTypeId=rtId, NAICS_CODE__c='321113');
		lstAcc1.add(acc102);
		Account acc103 = new Account(Name='Test Account103', RecordTypeId=rtId, NAICS_CODE__c='311111');
		lstAcc1.add(acc103);		
		Account acc104 = new Account(Name='Test Account104', RecordTypeId=rtId, NAICS_CODE__c='322110');
		lstAcc1.add(acc104); 		
		Account acc105 = new Account(Name='Test Account105', RecordTypeId=rtId, NAICS_CODE__c='313210');
		lstAcc1.add(acc105); 		
		Account acc106 = new Account(Name='Test Account106', RecordTypeId=rtId, NAICS_CODE__c='337910');
		lstAcc1.add(acc106); 		
		Account acc107 = new Account(Name='Test Account107', RecordTypeId=rtId, NAICS_CODE__c='337920');
		lstAcc1.add(acc107); 		
		Account acc108 = new Account(Name='Test Account108', RecordTypeId=rtId, NAICS_CODE__c='339991');
		lstAcc1.add(acc108); 		
		Account acc109 = new Account(Name='Test Account109', RecordTypeId=rtId, NAICS_CODE__c='339930');
		lstAcc1.add(acc109); 		
		Account acc110 = new Account(Name='Test Account110', RecordTypeId=rtId, NAICS_CODE__c='339910');
		lstAcc1.add(acc110); 		
		Account acc111 = new Account(Name='Test Account111', RecordTypeId=rtId, NAICS_CODE__c='339940');
		lstAcc1.add(acc111); 		
		Account acc112 = new Account(Name='Test Account112', RecordTypeId=rtId, NAICS_CODE__c='339950');
		lstAcc1.add(acc112); 		
		Account acc113 = new Account(Name='Test Account113', RecordTypeId=rtId, NAICS_CODE__c='339920');
		lstAcc1.add(acc113); 		
		Account acc114 = new Account(Name='Test Account114', RecordTypeId=rtId, NAICS_CODE__c='541211');
		lstAcc1.add(acc114); 		
		Account acc115 = new Account(Name='Test Account115', RecordTypeId=rtId, NAICS_CODE__c='541810');
		lstAcc1.add(acc115); 		
		Account acc116 = new Account(Name='Test Account116', RecordTypeId=rtId, NAICS_CODE__c='541310');
		lstAcc1.add(acc116); 		
		Account acc117 = new Account(Name='Test Account117', RecordTypeId=rtId, NAICS_CODE__c='541511');
		lstAcc1.add(acc117); 		
		Account acc118 = new Account(Name='Test Account118', RecordTypeId=rtId, NAICS_CODE__c='541110');
		lstAcc1.add(acc118); 		
		Account acc119 = new Account(Name='Test Account119', RecordTypeId=rtId, NAICS_CODE__c='236115');
		lstAcc1.add(acc119); 		
		Account acc120 = new Account(Name='Test Account120', RecordTypeId=rtId, NAICS_CODE__c='541690');
		lstAcc1.add(acc120); 		
		Account acc121 = new Account(Name='Test Account121', RecordTypeId=rtId, NAICS_CODE__c='236115');
		lstAcc1.add(acc121); 		
		Account acc122 = new Account(Name='Test Account122', RecordTypeId=rtId, NAICS_CODE__c='541620');
		lstAcc1.add(acc122); 		
		Account acc123 = new Account(Name='Test Account123', RecordTypeId=rtId, NAICS_CODE__c='541921');
		lstAcc1.add(acc123); 		
		Account acc124 = new Account(Name='Test Account124', RecordTypeId=rtId, NAICS_CODE__c='541711');
		lstAcc1.add(acc124); 		
		Account acc125 = new Account(Name='Test Account125', RecordTypeId=rtId, NAICS_CODE__c='236115');
		lstAcc1.add(acc125); 		
		Account acc126 = new Account(Name='Test Account126', RecordTypeId=rtId, NAICS_CODE__c='541410');
		lstAcc1.add(acc126); 		
		Account acc127 = new Account(Name='Test Account127', RecordTypeId=rtId, NAICS_CODE__c='541940');
		lstAcc1.add(acc127); 		
		Account acc128 = new Account(Name='Test Account128', RecordTypeId=rtId, NAICS_CODE__c='238990');
		lstAcc1.add(acc128); 		
		Account acc129 = new Account(Name='Test Account129', RecordTypeId=rtId, NAICS_CODE__c='236115');
		lstAcc1.add(acc129); 		
		Account acc130 = new Account(Name='Test Account130', RecordTypeId=rtId, NAICS_CODE__c='236115');
		lstAcc1.add(acc130); 		
		Account acc131 = new Account(Name='Test Account131', RecordTypeId=rtId, NAICS_CODE__c='236115');
		lstAcc1.add(acc131);		
		Account acc132 = new Account(Name='Test Account132', RecordTypeId=rtId, NAICS_CODE__c='236220');
		lstAcc1.add(acc132);		
		Account acc133 = new Account(Name='Test Account133', RecordTypeId=rtId, NAICS_CODE__c='237110');
		lstAcc1.add(acc133);		
		Account acc134 = new Account(Name='Test Account134', RecordTypeId=rtId, NAICS_CODE__c='236210');
		lstAcc1.add(acc134);		
		Account acc135 = new Account(Name='Test Account135', RecordTypeId=rtId, NAICS_CODE__c='237120');
		lstAcc1.add(acc135);		
		Account acc136 = new Account(Name='Test Account136', RecordTypeId=rtId, NAICS_CODE__c='237990');
		lstAcc1.add(acc136);		
		Account acc137 = new Account(Name='Test Account137', RecordTypeId=rtId, NAICS_CODE__c='238320');
		lstAcc1.add(acc137);		
		Account acc138 = new Account(Name='Test Account138', RecordTypeId=rtId, NAICS_CODE__c='238910');
		lstAcc1.add(acc138);		
		Account acc139 = new Account(Name='Test Account139', RecordTypeId=rtId, NAICS_CODE__c='238330');
		lstAcc1.add(acc139);		
		Account acc140 = new Account(Name='Test Account140', RecordTypeId=rtId, NAICS_CODE__c='531210');
		lstAcc1.add(acc140);		
		Account acc141 = new Account(Name='Test Account141', RecordTypeId=rtId, NAICS_CODE__c='531311');
		lstAcc1.add(acc141);		
		Account acc142 = new Account(Name='Test Account142', RecordTypeId=rtId, NAICS_CODE__c='237210');
		lstAcc1.add(acc142);		
		Account acc143 = new Account(Name='Test Account143', RecordTypeId=rtId, NAICS_CODE__c='531130');
		lstAcc1.add(acc143);		
		Account acc144 = new Account(Name='Test Account144', RecordTypeId=rtId, NAICS_CODE__c='531190');
		lstAcc1.add(acc144);		
		Account acc145 = new Account(Name='Test Account145', RecordTypeId=rtId, NAICS_CODE__c='531110');
		lstAcc1.add(acc145);
		Account acc146 = new Account(Name='Test Account146', RecordTypeId=rtId, NAICS_CODE__c='236117');
		lstAcc1.add(acc146);		
		Account acc147 = new Account(Name='Test Account147', RecordTypeId=rtId, NAICS_CODE__c='525110');
		lstAcc1.add(acc147);		
		Account acc148 = new Account(Name='Test Account148', RecordTypeId=rtId, NAICS_CODE__c='813910');
		lstAcc1.add(acc148);		
		Account acc149 = new Account(Name='Test Account149', RecordTypeId=rtId, NAICS_CODE__c='813410');
		lstAcc1.add(acc149);		
		Account acc150 = new Account(Name='Test Account150', RecordTypeId=rtId, NAICS_CODE__c='624221');
		lstAcc1.add(acc150);		
		Account acc151 = new Account(Name='Test Account151', RecordTypeId=rtId, NAICS_CODE__c='814110');
		lstAcc1.add(acc151);		
		Account acc152 = new Account(Name='Test Account152', RecordTypeId=rtId, NAICS_CODE__c='813110');
		lstAcc1.add(acc152);		
		Account acc153 = new Account(Name='Test Account153', RecordTypeId=rtId, NAICS_CODE__c='813311');		 
		lstAcc1.add(acc153);		
		Account acc154 = new Account(Name='Test Account154', RecordTypeId=rtId, NAICS_CODE__c='624110');		
		lstAcc1.add(acc154);		
		Account acc155 = new Account(Name='Test Account155', RecordTypeId=rtId, NAICS_CODE__c='713110');		  
		lstAcc1.add(acc155);
				
		insert lstAcc1;		
		system.assertEquals(lstAcc1.size(), 155);
			
		for(Account a: lstAcc1){
			lstAcc2.add(a);
		}
		
		system.assertEquals(lstAcc2.size(), 155);		         
        System.debug('Account list size is: '+lstAcc2.size());         
                         
       	batchRegionsNAICSindustry NI = new batchRegionsNAICSindustry();
       	QL = NI.start(BC);  
       	NI.execute(BC,lstAcc2);       	                        
        Test.stopTest(); 
    }
}