global class batchPipelineGoals implements Database.Batchable<sObject>
{
    global batchPipelineGoals(){
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id from Pipeline_Goal__c where createddate = last_YEAR or createddate = this_year';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Pipeline_Goal__c> scope)
    {
        delete scope;
        DataBase.emptyRecycleBin(scope);
    }   
    global void finish(Database.BatchableContext BC)
    {
    	batchPipelineGoalsUpdate controller = new batchPipelineGoalsUpdate();
		database.executebatch(controller);
    }
}