public class TaskTriggerHandler {
    public TaskTriggerHandler(){
        //Handler
    }
    public void BeforeSaveREB(List<Task> NewRecords){
        List<Task> RecordsToUpdate = New List<Task>();
        List<String> ttHoursNeeded = new List<String>();
        //Create list of tasks requiring update
        for (Task theTask : NewRecords){
            if(theTask.Record_Type_Name__c=='REB CU Task'){
                RecordsToUpdate.add(theTask);
                ttHoursNeeded.add(theTask.Task_Type__c);
            }
            //auto-populate task closed date...Ephrem 04-2014
            if(theTask.Status == 'Completed' && theTask.Closed_Date__c == null){
                theTask.Closed_Date__c = System.today();                
            }
            else if(theTask.Status !='Completed' && theTask.Closed_Date__c != null){
                theTask.Closed_Date__c = null;                
            }
        }
        //Only process data if we have any records to evaluate and update
        If(!RecordsToUpdate.isEmpty()){
            //Make map of task types against taskflow hours from list of needed values
            List<Taskflow_Hours__c> taskHours = [select Id, Task_Hours__c, Task_Type__c, Task_Group__c from Taskflow_Hours__c where Task_Type__c in :ttHoursNeeded];
            Map<String,Decimal> ttHoursMap = New Map<String,Decimal>();
            for (Taskflow_Hours__c taskHour : taskHours){
                String groupType = (string)taskHour.Task_Group__c+(string)taskHour.Task_Type__c;
                ttHoursMap.put(groupType,taskHour.Task_Hours__c);
            }
            //Query REB Task Hours custom settings
            List<REB_Task_Hours__c> taskHoursPercents = [select Percent_Value__c,Status__c,Task_Area__c from REB_Task_Hours__c];
            //Loop through tasks requiring update
            for (Task thisTask : RecordsToUpdate){
                //Default hours to 0
                thisTask.Task_Hours__c = 0;
                //Loop through custom setting looking for matching Status and Task Area
                for (REB_Task_Hours__c taskHoursPercent : taskHoursPercents){
                    if(thisTask.Task_Area__c==taskHoursPercent.Task_Area__c && thisTask.Status==taskHoursPercent.Status__c){
                        //If matching custom setting is found, look in map of taskflow hours for matching task type/group and compute hours
                        Double attorneyAdjustment = 0.0;
                        if(thisTask.Task_Group__c=='HBF - Pipeline Task' && thisTask.Status=='Completed'){
                            if((thisTask.Task_Type__c=='CAP' || thisTask.Task_Type__c=='ReCAP') && thisTask.Attorney_Involved__c=='Yes'){
                                attorneyAdjustment = 2.0;
                            }
                        }
                        String groupType = (string)thisTask.Task_Group__c+(string)thisTask.Task_Type__c;
                        if(ttHoursMap.get(groupType ) != NULL){
                            thisTask.Task_Hours__c = (taskHoursPercent.Percent_Value__c * ttHoursMap.get(groupType) / 100) + attorneyAdjustment;
                        }
                    }
                }
            }
        }
    }
    public void afterUpdate(Map<Id,Task> OldMap,List<Task> NewRecords){
        List<Task> RecordsToUpdate = New List<Task>();
        List<Id> bankAccountIds = new List<Id>();
        for (Task theTask : NewRecords){
            Task oldTask = OldMap.get(theTask.ID);
            if(theTask.Task_Type__c=='Renewal/Financial Review' && theTask.Status=='Completed' && oldTask.Status!='Completed'){
                RecordsToUpdate.add(theTask);
                bankAccountIds.add(theTask.WhatId);
            }
        }
        Map<Id,Bank_Account_Details__c> bankMap = new Map<Id,Bank_Account_Details__c>([SELECT Id,Name,Earliest_Review_Date__c,Client__c, Source_Account_Number__c FROM Bank_Account_Details__c WHERE Id in :bankAccountIds]);
        List<Opportunity> opps = new List<Opportunity>();
        RecordType oppRecordType = [select id from recordtype where Name='BCB Renewal Pipeline' limit 1];
        for (Task qualTask : RecordsToUpdate){
            Bank_Account_Details__c thisBankAccount = bankMap.get(qualTask.WhatId);
            opps.add(new Opportunity(
                AccountId = thisBankAccount.Client__c,
                // I-79795 append bank account number onto pipeline name
                //Name = '120 Day Renewal Pipeline',
                Name = 'Renewal ' + thisBankAccount.Name,
                StageName = 'Client Notified',
                CloseDate = thisBankAccount.Earliest_Review_Date__c,
                Bank_Account_Details__c = thisBankAccount.Id,
                RecordTypeId = oppRecordType.Id
                )
            );   
        }      
        insert opps;
                   
        PriceBookEntry pbe;
        if(opps.size() > 0){
            pbe = [SELECT Id FROM PriceBookEntry where Name='Credit Renewal' and Pricebook2.IsStandard=false LIMIT 1];
        }
        List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
        for (Opportunity opp : opps){
            oliList.add(new OpportunityLineItem(
                OpportunityId=opp.Id,
                PricebookEntryId=pbe.Id,
                Pricing__c='Renewal',
                Spread_base_points__c=0,
                Term_in_Months_full__c='0',
                Quantity=1,
                TotalPrice=0
                )
            );
        }
        insert oliList;
    }
    // begin modification and enhancement... Ephrem 04-2014 
    public void beforeInsert(List<Task> NewRecords){
        for (Task thisTask : NewRecords){
            if(thisTask.Subject=='Bank Account up for Renewal'){
                thisTask.Task_Group__c = 'Standard';
                thisTask.Task_Type__c = 'Renewal/Financial Review';
                //dynamically retrieves Record Type regardless of environments...
                RecordType[] RecTyp = [Select Id From RecordType Where sObjectType = 'Task' and Name = 'BB 120 Day Renewal'];
                thisTask.RecordTypeId = rectyp[0].id;                                
             }
         }
    }
    //modification and enhancement to send email notification only - not creating task Ephrem 04-2014
    public void emailNotification(List<Task> NewRecords){
        List<Task> tasks30day = New List<Task>();
        List<Task> tasks60day = New List<Task>();
        Datetime activityDate;
        String dueDate;       
        String fullUrl;
        for (Task thisTask : NewRecords){
            if(thisTask.Subject=='Renewal Closes in 30 Days'){ 
                tasks30day.add(thisTask);
            }else if(thisTask.Subject=='Renewal Closes in 60 Days'){
                tasks60day.add(thisTask);
            }
        }
        
        if(tasks30day.size() > 0){
            List<Messaging.SingleEmailMessage> listMail = new List<Messaging.SingleEmailMessage>();
            Boolean donotSend = true;    
            for(Task t : tasks30day){
                String what = t.WhatID;
                String tOwnerId = t.OwnerId;
                activityDate = t.ActivityDate;
                dueDate = activityDate.format('MM/dd/yyyy');
                String[] toAddresses;
                Boolean yesSM = false;
                Boolean yesSE = false;
                User[] SMuser;
                User[] SEuser;
                Opportunity[] opp = [SELECT ID, OwnerId, Name FROM Opportunity WHERE Id = :what];
                User[] tUser = [SELECT ID, Name, Sales_Manager__c, Sales_Executive__c FROM User WHERE Id = :tOwnerId];
                if(tUser[0].Sales_Executive__c != null){ 
                    SEuser = [SELECT ID, Name, Email FROM User WHERE Id = :tUser[0].Sales_Executive__c];
                    yesSE = true;
                }
                if(tUser[0].Sales_Manager__c != null){
                    SMuser = [SELECT ID, Name, Email FROM User WHERE Id = :tUser[0].Sales_Manager__c];
                    yesSM = true;
                }  
  
                if(opp[0].id != null){
                    fullUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+opp[0].id;
                }
                if (fullUrl.startsWith('http:')){
                    fullUrL = fullUrL.replaceFirst('http:', 'https:');
                }
                //Create notification for Sales Manager, if no Sales Manager, create for Sales Executive...
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                if(yesSM){
                    toAddresses = new String[]{SMuser[0].Email};
                    mail.setToAddresses(toAddresses);
                    donotSend = false;
                }else if(yesSE){
                    toAddresses = new String[] {SEuser[0].Email};
                    mail.setToAddresses(toAddresses); 
                    donotSend = false;
                }else{
                    donotSend = true;
                    break;
                }                   
                mail.setSenderDisplayName('RegionsOneView Administrator'); 
                mail.setToAddresses(toAddresses);    // Set the TO addresses
                mail.setSubject(t.Subject);    // Set the subject
                mail.setPlainTextBody('Subject:  '+t.Subject+'<br/>' + 'Pipeline: '+ opp[0].Name + '<br/>'+ 'Pipeline Owner: '+tUser[0].name +'<br/>'+ 'Due Date: '+dueDate +'<br/>'+ 'Priority: '+t.Priority +'<br/><br/>'+ 'Comments: '+t.Description + '<br/><br/>'+'For more details, click the following link:<br/><br/>'+fullUrl);
                mail.setHtmlBody('Subject:  '+t.Subject+'<br/>' + 'Pipeline: '+ opp[0].Name + '<br/>'+ 'Pipeline Owner: '+tUser[0].name +'<br/>'+ 'Due Date: '+dueDate +'<br/>'+ 'Priority: '+t.Priority +'<br/><br/>'+ 'Comments: '+t.Description + '<br/><br/>'+'For more details, click the following link:<br/><br/>'+fullUrl);
                listMail.add(mail);
           }
           if(!donotSend){
               Messaging.sendEmail(listMail);
            }else{}
          }else if(tasks60day.size() > 0){
            List<Messaging.SingleEmailMessage> listMail = new List<Messaging.SingleEmailMessage>();
            Boolean donotSend = true; 
            for(Task t : tasks60day){
                String what;
                what = t.WhatID;
                String tOwnerId;
                tOwnerId = t.OwnerId;
                activityDate = t.ActivityDate;
                dueDate = activityDate.format('MM/dd/yyyy');
                User[] SMuser;
                Boolean yesSM = false;
                Opportunity[] opp = [SELECT ID, Name FROM Opportunity WHERE Id = :what];
                User[] tUser = [SELECT ID, Name, Sales_Manager__c, Sales_Executive__c FROM User WHERE Id = :tOwnerId];                
                if(tUser[0].Sales_Manager__c != null){
                    SMuser = [SELECT ID, Name, Email FROM User WHERE Id = :tUser[0].Sales_Manager__c];
                    yesSM = true;
                }                 
                if(opp[0].id != null){
                    fullUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+opp[0].id;
                }
                if (fullUrl.startsWith('http:')){
                    fullUrL = fullUrL.replaceFirst('http:', 'https:');
                }            
                //Create notification for Sales Manager
                if(yesSM){            
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setSenderDisplayName('RegionsOneView Administrator'); 
                    String[] toAddresses = new String[] {SMuser[0].Email};
                    mail.setToAddresses(toAddresses);    // Set the TO addresses
                    mail.setSubject(t.Subject);    // Set the subject
                    mail.setPlainTextBody('Subject:  '+t.Subject+'<br/>' + 'Pipeline: '+ opp[0].Name + '<br/>'+ 'Pipeline Owner: '+tUser[0].name +'<br/>'+ 'Due Date: '+dueDate +'<br/>'+ 'Priority: '+t.Priority +'<br/><br/>'+ 'Comments: '+t.Description + '<br/><br/>'+'For more details, click the following link:<br/><br/>'+fullUrl);
                    mail.setHtmlBody('Subject:  '+t.Subject+'<br/>' + 'Pipeline: '+ opp[0].Name + '<br/>'+ 'Pipeline Owner: '+tUser[0].name +'<br/>'+ 'Due Date: '+dueDate +'<br/>'+ 'Priority: '+t.Priority +'<br/><br/>'+ 'Comments: '+t.Description + '<br/><br/>'+'For more details, click the following link:<br/><br/>'+fullUrl);
                    listMail.add(mail);
                }
            }
            if(!donotSend){
                Messaging.sendEmail(listMail);
            }else{}
        }
     }
    //end modification and enhancement... 
}