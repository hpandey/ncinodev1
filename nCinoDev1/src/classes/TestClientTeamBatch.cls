@isTest
private class TestClientTeamBatch{
    static testmethod void testBatchClass(){
        Test.startTest();       
        List <UserAccountTeamMember> accList = [select Id, UserId, OwnerId, AccountAccessLevel, TeamMemberRole, ContactAccessLevel, CaseAccessLevel, OpportunityAccessLevel from UserAccountTeamMember where userid not in(select userorgroupid from accountshare) and userid not in (select userorgroupid from relationship__share) limit 1];
        string uatOwnerId = accList[0].OwnerId;
        List<Account> AccountList = new List<Account>();
        Database.QueryLocator QL;
        Database.BatchableContext BC;       
        
        Relationship__c r = new Relationship__c();
        r.Name = 'Scooby Doo Corp';
        r.OwnerId = uatOwnerId;
        insert r;
        
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.OwnerId = uatOwnerId;
        testAcc.Relationship__c = r.Id;
        insert testAcc;        
                    
        AccountList = [Select Id, OwnerID, Relationship__c from Account where OwnerID in (select OwnerID from UserAccountTeamMember)and Relationship__c <> null limit 1];
        System.assertEquals(AccountList.size(),1);
        System.debug('AccountList size is: '+ AccountList.size());  
        
        System.assertEquals(accList.size(),1);
        System.debug('UAT size is: '+ accList.size());
        
        List<Relationship__c> relList = new List<Relationship__c>();       
        Set<Id> relIdList = new Set<Id>();
        
        relIdList.add(AccountList[0].Relationship__c);
        System.assertEquals(relIdList.size(),1);
        
        relList = [select id from Relationship__c where Id IN:relIdList];
        System.assertEquals(relList.size(),1);            

        try{
            ClientTeamBatch cb = new ClientTeamBatch(); 
            QL=cb.start(BC);    
            cb.execute(BC,AccountList);
            cb.finish(BC);
        }catch(exception e){
            System.debug('System-generated error message: '+e.getMessage());            
        }
        Test.stopTest();    
    }    
}