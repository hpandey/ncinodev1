@isTest
public class testCreatelookback
{


     public testmethod static void testing()
    {
             Account acc = new Account(Name='Test Account');
            insert acc;
  
            Opportunity testopty = new Opportunity(Name='Test Opty34', closedate=date.today(), stagename='Prospecting', accountid=acc.id);
            insert testopty;
            //Lookback_Pipeline__c = testopty.ID
            Opportunity opty = new Opportunity(Name='Test Opty',Lookback_Pipeline__c = testopty.ID, Fee_Schedule__c = 'Corp Trust Lookback', closedate=date.today(), Incentive_Status__c = 'Pending Approval' ,stagename='Closed Won', accountid=acc.id);
                         
            insert opty;
              
            Opportunity opty2 = new Opportunity(Name='Test Opty2',Lookback_Pipeline__c = opty.ID, stagename='Prospecting', closedate=date.today(), accountid=acc.id);                   
            insert opty2; 
            
             PageReference lookbckPage = Page.createLookback;
             Test.setCurrentPageReference(lookbckPage); 
                 lookbckPage.getParameters().put('id', opty.id);
                                                    
                ApexPages.standardController controller = new ApexPages.standardController(opty2);
                createLookbackCTRL crLookbckCTRL = new createLookbackCTRL(controller); 
                crLookbckCTRL.createLookback();
                
                
    
    }


}