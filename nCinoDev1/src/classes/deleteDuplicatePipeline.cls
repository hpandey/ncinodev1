//--------------------------------------------------------------------------------------------------------------------------
//Case 1853 - Remove duplicate pipelines automatically where Stage is 'Closed Lost' and Reason Lost is 'Duplicate Pipeline' 
//and Pipeline Verification is not blank...
//Ephrem Tekle - 10-2015
//--------------------------------------------------------------------------------------------------------------------------

global class deleteDuplicatePipeline {
	@future   //used to avoid immediate re-direct to sf standard delete confirmation page
	public static void ProcessDeletion(){
		List<Opportunity> o1 = [SELECT id, StageName, Reason_Lost__c, Pipeline_Verification__c from opportunity where StageName = 'Closed Lost' and Reason_Lost__c = 'Duplicate Pipeline' and Pipeline_Verification__c <> null];
      	List<Opportunity> o2 = new List<Opportunity>();
        
        try{	
        	if(o1.size() > 0){
        		for(integer i=0; i<o1.size(); i++)
        		{
        			o2.add(o1[i]);
        		}
        		delete o2;
        	}        		
        }catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());                              
        }                 
	} 
}