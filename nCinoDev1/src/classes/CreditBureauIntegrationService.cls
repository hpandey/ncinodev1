/**********************************************************************************************
 * Name         : CreditBureauIntegrationService
 * Author       : Deloitte Consulting
 * Description  : This service class implments the logic to generate request body, invoke call out 
 *                and parse the response for Credit Bureau Integration 
 *********************************************************************************************/
 /************************************************************************************
Code Review Comments - Aveleena
1) Add inline comments wherever needed to give an idea of what the line is doing
2) Please create a custom setting that will store Endpoint,Method and timeout
3) Indent the code

**************************************************************************************/
public with sharing class CreditBureauIntegrationService implements OutboundIntegrationInterface{

    String strRequest;
    String strResponse; 
    HttpResponse resposne;
    HTTPFramework objHTTPFramework; 
    
    /***
    * Method name    : CreditBureauIntegrationService
    * Description    : Constructor of class.
    * Return Type    : None
    * Parameter      : None
    */
    public CreditBureauIntegrationService() {
        
        objHTTPFramework    = new HTTPFramework();
        strResponse         = '';
        strRequest          = '';
        resposne            = new HttpResponse();
    }
    
    /***
    * Method name    : getRequestBody
    * Description    : method used for generating request body as a string for API Callout.
    * Return Type    : String
    * Parameter      : sObject sobjRecord 
    */
    
    public String getRequestBody(sObject sobjRecord) {
        String strRequestBody = '';        
        Account objAccount = (Account)sobjRecord;
        
        if(objAccount != Null) {
            try{
                // Generating Json request body.
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                gen.writeStringField('Id', objAccount.id);
                gen.writeStringField('Name', objAccount.Name);
                gen.writeEndObject();

                // Converting Json request body to string
                strRequestBody = gen.getAsString();
            }
            catch(JSONException e) {
                UtilityClass.throwExceptions(e);
            }
        }
        
        strRequest = strRequestBody;
        return strRequestBody;
    }

    /***
    * Method name    : doCallOut
    * Description    : method used for performing API Callouts.
    * Return Type    : HttpResponse
    * Parameter      : None 
    */

    public HttpResponse doCallOut() {
        system.debug('I am In Account'+strRequest);
        // Fetching Access Token
        OauthHandler objOauthHandler = new OauthHandler();
        String strAccessToken = objOauthHandler.getAccessToken();
        
        if(!string.isBlank(strAccessToken)) {
            //creating map of headers
            map<string , string> mapHeaders = new map<string , string>();
            mapHeaders.put('Authorization' , + 'Bearer '+strAccessToken);
            mapHeaders.put('Content-Type' , + 'application/json');
            mapHeaders.put('accept' , + 'application/json');
            system.debug('mapHeaders$$'+mapHeaders);
            
            // Fetching Data of Async Custom Setting.
            Map<String , Async_Process_Log_Setting__c> mapAsyncCustomSetting = Async_Process_Log_Setting__c.getAll();
            system.debug('mapAsyncCustomSetting'+mapAsyncCustomSetting);
            
            if(mapAsyncCustomSetting.ContainsKey(Label.Credit_Bureau_Integration_Service)) {
                Async_Process_Log_Setting__c objAsyncCustomSetting = mapAsyncCustomSetting.get(Label.Credit_Bureau_Integration_Service);
                system.debug('strRequest@$$'+strRequest+objAsyncCustomSetting);
                
                // Initialising Http framework
                objHTTPFramework = new HTTPFramework();
                objHTTPFramework.setMultipleHeaders(mapHeaders);
                objHTTPFramework.setEndpoint(objAsyncCustomSetting.End_Point__c);
                objHTTPFramework.setMethod(objAsyncCustomSetting.Method_Type__c);
                objHTTPFramework.setTimeout(integer.valueof(objAsyncCustomSetting.TimeOut_Duration__c));
                objHTTPFramework.setBody(strRequest);
                
                // Performing API Callout
                resposne = objHTTPFramework.performCallout();
            }   
        }
        return resposne;
    }
    
    /***
    * Method name    : getResponse
    * Description    : method used for generating response body as a string.
    * Return Type    : String
    * Parameter      : None 
    */

    public String getResponse() {
        //String strResponseBody = '';

        // Converting Response body to string.
        strResponse = resposne.getbody();
        return strResponse;
    }
    
    /***
    * Method name    : finish
    * Description    : method used for performing back-update to source object.
    * Return Type    : sObject
    * Parameter      : sObject sobjRecord 
    */

    public sObject finish(sObject sobjRecord) {
        /*system.debug('I am in finish');
        if(sobjRecord != NUll) {
            Account objAccount = (Account)sobjRecord;
            string CreditScore;
            //String requestBodyData = RestContext.request.requestBody.toString();
            //resposne
            
            Map<String, Object> mapRequestData = (Map<String, Object>) JSON.deserializeUntyped(strResponse);
            system.debug('mapRequestData@@'+mapRequestData);
            CreditScore = string.valueof(mapRequestData.get('Credit Score'));
            objAccount.Description = CreditScore;
            return objAccount;
        }*/
        return null;
    }
    
    public void executeIntegration(Asynchronous_Queue__c objAsyncQueueRecord){

        // implement logic to query data from queue record 
        // invoke other methods within this class like getrequest, docallout, parseresponse etc.
    }     
}