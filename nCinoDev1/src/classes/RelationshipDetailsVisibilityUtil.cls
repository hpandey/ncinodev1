global without sharing class RelationshipDetailsVisibilityUtil{
    /******************************************************************************************************************************************
    
    select id, ALL_NEEDED_FIELDS (select id, ALL_NEEDED_FIELDS from accounts__r), (select id, ALL_NEEDED_FIELDS from relationship_strategy_plans__r),(select id, ALL_NEEDED_FIELDS from pdms__r),(select id, ALL_NEEDED_FIELDS from restricted_relationship_information__r) from relationship__c
    
    *************************************************************************************************************************************************/
    private RelationshipDetailsVisibilityUtil(){}
    webservice static void recalculateSharingOnSubmit(Id relationshipId){
        RelationshipDetailsVisibilityUtil.recalculateSharing(relationshipID);}
    public static void recalculateSharing(Id relationshipID){
    	// modified by sendeep
        //List<Relationship__c> relationshipList = [select id,(select id from accounts__r),(select id from relationship_strategy_plans__r),(select id from pdms__r),(select id from restricted_relationship_information__r) from relationship__c where id = : relationshipID];
		List<Relationship__c> relationshipList = [select id, ownerid from relationship__c where id = : relationshipID];
		RelationshipDetailsVisibilityUtil.recalculateSharing(relationshipList);
	}
    public static void recalculateSharing(List<Relationship__c> relationshipList){
        RelationshipDetailsVisibilityUtil util= new RelationshipDetailsVisibilityUtil();
        util.applySharing(relationshipList);
    }
    public void applySharing(List<Relationship__c> relationshipList){
        Map<id,Set<ID>> rIDMbrsUserIDMap= new Map<id,Set<ID>>();
        Map<Id,Map<ID,Set<ID>>> rID2AccountID2MbrsUserIDMap= new Map<Id,Map<ID,Set<ID>>>();
        Map<Id,Map<ID,Map<Id,Relationship_Strategy_Plan__Share>>> rID2rspID2UserID2ShareMap= new Map<Id,Map<ID,Map<Id,Relationship_Strategy_Plan__Share>>>();
        Map<Id,Map<ID,Map<Id,PDM__Share>>> rID2PdmID2UserID2ShareMap= new Map<Id,Map<ID,Map<Id,PDM__Share>>>();
        Map<Id,Map<ID,Map<Id,Restricted_Relationship_Information__Share>>> rID2rriID2UserID2ShareMap= new Map<Id,Map<ID,Map<Id,Restricted_Relationship_Information__Share>>>();
        Set<id> tmpAccountIDSet= new Set<id>();
        Set<id> tmpRspIDSet= new Set<id>();
        Set<id> tmpPdmIDSet= new Set<id>();
        Set<id> tmpRriIDSet= new Set<id>();
        // modifed by Sendeep

 		Map<Id,List<Account>>                                           account_MAP     = new Map<Id,List<Account>>();
        Map<Id,List<Relationship_Strategy_Plan__c>> relationship_strategy_plans_MAP     = new Map<Id,List<Relationship_Strategy_Plan__c>>();        
        Map<Id,List<PDM__c>>                                                PDM_MAP     = new Map<Id,List<PDM__c>>();          
        Map<Id,List<Restricted_Relationship_Information__c>>  res_relation_info_MAP     = new Map<Id,List<Restricted_Relationship_Information__c>>();
                
        for(Relationship__c rel : relationshipList ){
        	account_MAP.put(rel.ID,new List<Account>());
        	relationship_strategy_plans_MAP.put(rel.ID,new List<Relationship_Strategy_Plan__c>());
			PDM_MAP.put(rel.ID,new List<PDM__c>());
        	res_relation_info_MAP.put(rel.ID,new List<Restricted_Relationship_Information__c>());
        }
        
        for(Account acc :[SELECT ID,Relationship__r.ID  FROM Account WHERE  Relationship__r.ID  IN :relationshipList]){           
            account_MAP.get(acc.Relationship__r.ID).add(acc);         
        }        
        for(Relationship_Strategy_Plan__c rel :[SELECT ID,Relationship__r.ID  FROM Relationship_Strategy_Plan__c WHERE  Relationship__r.ID  IN :relationshipList]){
            relationship_strategy_plans_MAP.get(rel.Relationship__r.ID).add(rel);            
        }        
        for(PDM__c oPDM :[SELECT ID,Relationship__r.ID  FROM PDM__c WHERE  Relationship__r.ID  IN :relationshipList]){
             PDM_MAP.get(oPDM.Relationship__r.ID).add(oPDM);            
        }        
        for(Restricted_Relationship_Information__c res :[SELECT ID,Relationship__r.ID  FROM Restricted_Relationship_Information__c WHERE  Relationship__r.ID  IN :relationshipList]){
            res_relation_info_MAP.get(res.Relationship__r.ID).add(res);       
        } 
        
        for(Relationship__c relationship : relationshipList){
            rIDMbrsUserIDMap.put(relationship.ID, new Set<ID>());
            if(account_MAP.get(relationship.id) !=null) // null check  
            	for(Account acct: account_MAP.get(relationship.id)){
                	if(account_MAP.get(relationship.id) !=null){
                    	Map<ID,Set<ID>> tempMap = new Map<ID,Set<ID>>();
                    	tempMap.put(acct.ID, new Set<ID>());                    
                    	if(rID2AccountID2MbrsUserIDMap.keySet().contains(relationship.id)){
                        	rID2AccountID2MbrsUserIDMap.get(relationship.id).put(acct.ID, new Set<ID>());
                    	}else{
                        	rID2AccountID2MbrsUserIDMap.put(relationship.id,new Map<Id,Set<ID>>{acct.ID => new Set<ID>()});
                    	}
                    }
                	tmpAccountIDSet.add(acct.id);
                } 
            if(relationship_strategy_plans_MAP.get(relationship.id) !=null) // null check      
            for(Relationship_Strategy_Plan__c rsp: relationship_strategy_plans_MAP.get(relationship.id)){
                if(relationship_strategy_plans_MAP.get(relationship.id) !=null){
                    if(rID2rspID2UserID2ShareMap.keySet().contains(relationship.id)){
                        rID2rspID2UserID2ShareMap.get(relationship.id).put(rsp.ID ,new Map<Id,Relationship_Strategy_Plan__Share>());
                    }else{
                        rID2rspID2UserID2ShareMap.put(relationship.id,new Map<ID,Map<Id,Relationship_Strategy_Plan__Share>>{rsp.ID => new Map<Id,Relationship_Strategy_Plan__Share>()});
                    }
                } 
                tmpRspIDSet.add(rsp.id);
            }
			if(PDM_MAP.get(relationship.id) !=null) // null check                
            	for(PDM__c pdm: PDM_MAP.get(relationship.id)){
                	if(PDM_MAP.get(relationship.id) !=null){
                    	if(rID2PdmID2UserID2ShareMap.keySet().contains(relationship.id)){
                        	rID2PdmID2UserID2ShareMap.get(relationship.id).put(pdm.ID,new Map<Id,PDM__Share>());
                    	}else{
                        	rID2PdmID2UserID2ShareMap.put(relationship.id,new Map<ID,Map<Id,PDM__Share>>{pdm.ID => new Map<Id,PDM__Share>()});
                        }
                    }
                	tmpPdmIDSet.add(pdm.id);
                }
            if(res_relation_info_MAP.get(relationship.id) !=null) // null check     
            	for(Restricted_Relationship_Information__c rri: res_relation_info_MAP.get(relationship.id)){
                	if( res_relation_info_MAP.get(relationship.id) !=null){
                    	if(rID2rriID2UserID2ShareMap.keySet().contains(relationship.id)){
                    		System.debug('======> If ');
                        	rID2rriID2UserID2ShareMap.get(relationship.id).put(rri.ID , new Map<Id,Restricted_Relationship_Information__Share>());
                    	}else{
                        	rID2rriID2UserID2ShareMap.put(relationship.id,new Map<Id,Map<Id,Restricted_Relationship_Information__Share>>{rri.ID => new Map<Id,Restricted_Relationship_Information__Share>()});
                        }
                    }
                	tmpRriIDSet.add(rri.id);
                }
        }
        system.debug('Executed all the modified logic.');
        // modified by sendeep
        for (AccountTeamMember atm: [select AccountID,UserID,Account.OwnerId,Account.Relationship__c from AccountTeamMember where AccountID in : tmpAccountIDSet AND User.IsActive=true]){
            if(rID2AccountID2MbrsUserIDMap.get(atm.Account.Relationship__c).keySet().contains(atm.AccountID)){
                rID2AccountID2MbrsUserIDMap.get(atm.Account.Relationship__c).get(atm.AccountID).add(atm.userID); 
                rIDMbrsUserIDMap.get(atm.Account.Relationship__c).add(atm.userID);
                //--------------------------------------------------------------------------
                //Case 2280: Include the RM (Client Owner), in the ASR process... 10-2015
                //Added the Account.OwnerId and filter for active users only to the above query on line #109; added owner to the user map below...
                //Ephrem Tekle - 11-2015
                //---------------------------------------------------------------------------
                rID2AccountID2MbrsUserIDMap.get(atm.Account.Relationship__c).get(atm.AccountID).add(atm.Account.OwnerId);
                rIDMbrsUserIDMap.get(atm.Account.Relationship__c).add(atm.Account.OwnerId);
                //--------------------------------------------------------------------------
            }
        }
        for(Relationship_Strategy_Plan__Share share: [select ParentId, UserOrGroupId, Parent.Relationship__c from Relationship_Strategy_Plan__Share where parentID in : tmpRspIDSet and RowCause='ASR_Relationship_Details__c']){
            if(rID2rspID2UserID2ShareMap.get(share.Parent.Relationship__c)!=null){
                rID2rspID2UserID2ShareMap.get(share.Parent.Relationship__c).get(share.ParentID).put(share.UserOrGroupID,share);
                }
        }
        for(PDM__Share share: [select ParentId, UserOrGroupId, Parent.Relationship__c from PDM__Share where parentID in : tmpPdmIDSet and RowCause='ASR_Relationship_Details__c'] ){
            if(rID2PdmID2UserID2ShareMap.get(share.Parent.Relationship__c)!=null){
                rID2PdmID2UserID2ShareMap.get(share.Parent.Relationship__c).get(share.ParentId).put(share.UserOrGroupID,share);
            }
        }
        for(Restricted_Relationship_Information__Share share: [select ParentID, UserOrGroupID, Parent.Relationship__c from Restricted_Relationship_Information__Share where parentID in : tmpRriIDSet and RowCause='ASR_Relationship_Details__c']){
            if(rID2rriID2UserID2ShareMap.get(share.Parent.Relationship__c)!=null){
                rID2rriID2UserID2ShareMap.get(share.Parent.Relationship__c).get(share.ParentId).put(share.UserOrGroupID,share);
            }
        }
        deleteShares(rIDMbrsUserIDMap,rID2AccountID2MbrsUserIDMap,rID2rspID2UserID2ShareMap,rID2PdmID2UserID2ShareMap,rID2rriID2UserID2ShareMap);
        createShares(rID2AccountID2MbrsUserIDMap,rID2rspID2UserID2ShareMap,rID2PdmID2UserID2ShareMap,rID2rriID2UserID2ShareMap);
    }
    public void deleteShares(Map<ID,Set<ID>> mbrsMap,Map<Id,Map<ID,Set<ID>>> acctMap, Map<Id,Map<ID,Map<Id,Relationship_Strategy_Plan__Share>>> rspMap, Map<ID,Map<Id,Map<Id,Pdm__Share>>> pdmMap, Map<ID,Map<Id,Map<Id,Restricted_Relationship_Information__Share>>> rriMap){
        List<Relationship_Strategy_Plan__Share> rspSharesToDelete = new List<Relationship_Strategy_Plan__Share>();
        List<PDM__Share> pdmSharesToDelete = new List<PDM__Share>();
        List<Restricted_Relationship_Information__Share> rriSharesToDelete = new List<Restricted_Relationship_Information__Share>();
        List<Relationship__Share> rSharesToDelete = new List<Relationship__Share>();
        rSharesToDelete = [SELECT Id, ParentId, UserOrGroupId, AccessLevel, RowCause from relationship__share where parentid in : mbrsMap.keySet() and RowCause = 'ASR_Relationship_Details__c'];
        for(Id relationshipId: mbrsMap.keySet()){
            if(rspMap.get(relationshipID)!=null){
                for(ID rspId: rspMap.get(relationshipID).keySet()){
                    for(Id mbrUserID: rspMap.get(relationshipID).get(rspID).keySet()){
                        if(mbrsMap==null || !mbrsMap.get(relationshipID).contains(mbrUserID)){
                            rspSharesToDelete.add(rspMap.get(relationshipID).get(rspID).get(mbrUserID));
                        }
                    }
                }
            }
            if(pdmMap.get(relationshipID)!=null){
                for(ID pdmId: pdmMap.get(relationshipID).keySet()){
                    for(Id mbrUserID: pdmMap.get(relationshipID).get(pdmID).keySet()){
                        if(mbrsMap==null || !mbrsMap.get(relationshipID).contains(mbrUserID)){
                            pdmSharesToDelete.add(pdmMap.get(relationshipID).get(pdmID).get(mbrUserID));
                        }
                    }
                 }
            }
            if(rriMap.get(relationshipID)!=null){//If the Restricted Relationship Information map contains this Relationship_c.id then continue
                for(ID rriId: rriMap.get(relationshipID).keySet()){
                    for(Id mbrUserID: rriMap.get(relationshipID).get(rriID).keySet()){
                        if(mbrsMap==null || !mbrsMap.get(relationshipID).contains(mbrUserID)){
                            rriSharesToDelete.add(rriMap.get(relationshipID).get(rriID).get(mbrUserID));
                        }
                    }
                }
            }
        }
        try{
            if(!rspSharesToDelete.isempty()){
                delete rspSharesToDelete;}
            if(!pdmSharesToDelete.isempty()){
                delete pdmSharesToDelete;}
            if(!rriSharesToDelete.isempty()){
                delete rriSharesToDelete;}
            if(!rSharesToDelete.isempty()){
                delete rSharesToDelete;}
        }catch(DMLException e){
        }
    }
    public void createShares(Map<Id,Map<ID,Set<ID>>> acctMap, Map<Id,Map<ID,Map<Id,Relationship_Strategy_Plan__Share>>> rspMap, Map<ID,Map<Id,Map<Id,Pdm__Share>>> pdmMap, Map<ID,Map<Id,Map<Id,Restricted_Relationship_Information__Share>>> rriMap){
        Set<Relationship_Strategy_Plan__Share> rspSharesToCreateSet = new Set<Relationship_Strategy_Plan__Share>();
        Set<PDM__Share> pdmSharesToCreateSet = new Set<PDM__Share>();
        Set<Restricted_Relationship_Information__Share> rriSharesToCreateSet = new Set<Restricted_Relationship_Information__Share>();
        Set<Relationship__Share> rSharesToCreateSet = new Set<Relationship__Share>();        
        for(Id RelationshipID: acctMap.keySet()){
            if(acctMap.get(relationshipID)!=null){            	
	            for(Id acctID: acctMap.get(relationshipID).keySet()){         	
	                for(ID mbrUserId: acctMap.get(relationshipID).get(acctID)){
	                	//Trying to add the edit rights to those in the Client Team
	                    Relationship__Share rShare = new Relationship__Share(
	                                AccessLevel='Edit', 
	                                RowCause='ASR_Relationship_Details__c',
	                                ParentId=relationshipID,
	                                UserOrGroupId=mbrUserID );
	                    rSharesToCreateSet.add(rShare);   
	                    if(rspMap.get(relationshipID)!=null){
	                        for(ID rspID: rspMap.get(relationshipID).keySet()){
	                            if(rspMap.get(relationshipID).get(rspId)==null || !rspMap.get(relationshipID).get(rspId).keySet().contains(mbrUserID)){
	                                Relationship_Strategy_Plan__Share share= 
	                                new Relationship_Strategy_Plan__Share(
	                                    AccessLevel='Edit', 
	                                    RowCause='ASR_Relationship_Details__c',
	                                    ParentId=rspID,
	                                    UserOrGroupId=mbrUserID );
	                                rspSharesToCreateSet.add(share);
	                            }
	                        }
	                    }
	                    if(pdmMap.get(relationshipID)!=null){
	                        for(ID pdmID: pdmMap.get(relationshipID).keySet()){
	                            if(pdmMap.get(relationshipID).get(pdmId)==null || !pdmMap.get(relationshipID).get(pdmId).keySet().contains(mbrUserID)){
	                                Pdm__Share share= 
	                                new Pdm__Share(
	                                    AccessLevel='Edit', 
	                                    RowCause='ASR_Relationship_Details__c',
	                                    ParentId=pdmID,
	                                    UserOrGroupId=mbrUserID );
	                                pdmSharesToCreateSet.add(share);
	                            }
	                        }
	                    }
	                    if(rriMap.get(relationshipID)!=null){
	                        for(ID rriID: rriMap.get(relationshipID).keySet()){
	                            if(rriMap.get(relationshipID).get(rriId)==null || !rriMap.get(relationshipID).get(rriId).keySet().contains(mbrUserID)){
	                                Restricted_Relationship_Information__Share share= 
	                                new Restricted_Relationship_Information__Share(
	                                    AccessLevel='Edit', 
	                                    RowCause='ASR_Relationship_Details__c',
	                                    ParentId=rriID,
	                                    UserOrGroupId=mbrUserID );
	                                rriSharesToCreateSet.add(share);
	                            }
	                    	}
	                    }
	                }
	             }
           	 }
        }
        List<Relationship_Strategy_Plan__Share> rspSharesToCreate = new List<Relationship_Strategy_Plan__Share>();
        List<PDM__Share> pdmSharesToCreate = new List<PDM__Share>();
        List<Restricted_Relationship_Information__Share> rriSharesToCreate = new List<Restricted_Relationship_Information__Share>();
        List<Relationship__Share> rSharesToCreate = new List<Relationship__Share>();
        rspSharesToCreate.addAll(rspSharesToCreateSet);
        pdmSharesToCreate.addAll(pdmSharesToCreateSet);
        rrisharesToCreate.addAll(rriSharesToCreateSet);
        rSharesToCreate.addAll(rSharesToCreateSet);
        try{
            insert rspSharesToCreate;
            insert pdmSharesToCreate;
            insert rriSharesTocreate;
            insert rSharesToCreate;
        }catch(DMLException e){
        }        
    }
}