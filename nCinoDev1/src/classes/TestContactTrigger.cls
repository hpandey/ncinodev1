//Edited to deploy under the oneview admin

@isTest
private class TestContactTrigger {

    static testMethod void myUnitTest() {
        // Setup test data 

        // This code runs as the system user 

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User a = new User(Alias = 'standta', Email='contactusera@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='TestingA', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='contactusera@testorg.com');

        //System.assert(insertedAcct != null);
        System.runAs(a) {
            System.debug('Current User A: ' + UserInfo.getUserName());
            // The following code runs as user 'u'  
            Account account = new Account(Name='Test Account');
            insert account;      
            // Access the account that was just created. 
    
            Account insertedAcct = [SELECT Id,Name FROM Account WHERE Name='Test Account'];
            
            //Add contact as the owner... This is expected to pass with colors that fly :)
            Contact contact = new Contact();
            contact.FirstName = 'Test';
            contact.LastName = 'ContactA';
            contact.AccountId = insertedAcct.Id;
            try{
                insert contact;
            }catch(Exception e){
                System.debug('Your a big error(A): ' + e);
                //Shouldn't catch any exceptions
            }
            //System.assertEquals(true,okToAdd);
            System.debug('Current User: ' + UserInfo.getUserName());
            //System.debug('Current Profile: ' + UserInfo.getProfileId()); 
        }
        
        User b = new User(Alias = 'standtb', Email='standarduserb@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='TestingB', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduserb@testorg.com');
        System.runAs(b){
            System.debug('Current User B: ' + UserInfo.getUserName());
            Account insertedAcct = [SELECT Id,Name FROM Account WHERE Name='Test Account'];
            
            //Add contact as someone other than the owner... This should fail.
            Contact contact = new Contact();
            contact.FirstName = 'Test';
            contact.LastName = 'ContactB';
            contact.AccountId = insertedAcct.Id;
            try{
                insert contact;
            }catch(Exception e){
                System.debug('Your a big error(B): ' + e.getMessage());
                System.assert(e.getMessage().equals('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, You are not part of the Client team. please contact the RM: []'));
            }
            //System.assertEquals(true,okToAdd);
        }
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User c = new User(Alias = 'admina', Email='adminusera@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='AdminA', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p2.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='adminusera@testorg.com');
        System.runAs(c){
            System.debug('Current User c: ' + UserInfo.getUserName());
            Account account = new Account(Name='Test Account2');
            insert account;      
            Account insertedAcct = [SELECT Id,Name FROM Account WHERE Name='Test Account2'];
            Contact insertedContact = [Select Id, Name, AccountId From Contact Where Name = 'Test ContactA'];
            //Add contact as someone other than the owner... This should fail.
            insertedContact.AccountId = insertedAcct.Id;
            try{
               update insertedContact;
            }catch(Exception e){
                System.debug('Your a big error(B): ' + e.getMessage());

            }
            //System.assertEquals(true,okToAdd);
        }
        
        
    }
}