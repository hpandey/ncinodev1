public with sharing class CoreSegRpt {

    //Query Parameters
    public string StartDate{get;set;}
    public string EndDate{get;set;}
    public string PDFStartDate{get;set;}
    public string PDFEndDate{get;set;}    
    public String ReasonLost{get;set;}
    public String Probability{get;set;}
    public string ReportName{get;set;}
    
    public String ReportView{get;set;}
    public String RptURL{get;set;}
    
    public Boolean FootNote1 {get; set;}
    public Boolean FootNote2 {get; set;}
    
    public Boolean onSearch = false;
    //Fix for repeat firing twice
    public Transient Boolean doRerender = true;
 
    public CoreSegRpt() {
        if(System.currentPageReference().getParameters().get('ReportView') == null){
            ReportView = '1';
        } else {
            ReportView = System.currentPageReference().getParameters().get('ReportView');
            onSearch = true;           
        }
        ReportName = System.currentPageReference().getParameters().get('ReportName');
        if(System.currentPageReference().getParameters().get('StartDate') != null){
            StartDate = System.currentPageReference().getParameters().get('StartDate'); 
            if(startdate.contains('-')){
                list<string> STdatepart = startdate.split('-');
                PDFstartdate = STdatepart[1]+'/'+STdatepart[2]+'/'+STdatepart[0];                          
            } else {
                PDFStartDate = StartDate;
            }                    
        }
        if(System.currentPageReference().getParameters().get('EndDate') != null){
            EndDate = System.currentPageReference().getParameters().get('EndDate'); 
            if(enddate.contains('-')){
                list<string> EDdatepart = enddate.split('-');
                PDFenddate = EDdatepart[1]+'/'+EDdatepart[2]+'/'+EDdatepart[0];
            } else {
                PDFEndDate = EndDate;
            }                   
        }
        if(System.currentPageReference().getParameters().get('Probability') != null){
            Probability = System.currentPageReference().getParameters().get('Probability');
        }
        if(System.currentPageReference().getParameters().get('ReasonLost') != null){
            ReasonLost = System.currentPageReference().getParameters().get('ReasonLost');
        }       

        RptURL = buildURL(); 
        
        ShowFootNote();
        
    }
    
    public PageReference View() {
        this.doRerender = true;  //fix for repeat firing twice on rerender
        onSearch = true;
        RptURL = buildURL();
        return null;
    }
    
    public PageReference ReportSelected() {
        RptURL = buildURL(); 
        ShowFootNote();
        this.doRerender = true; //fix for repeat firing twice on rerender
        //doSearch=false;
        startdate=null;
        enddate=null;
        ReasonLost=null;
        Probability=null;
        onSearch = false;
        return null;
    }
    
    public string buildURL() {
        system.debug('url start date ' + startdate);
        string URL = '/apex/CoreSegRptPdf?ReportView='+ReportView;
        if(StartDate!=null){
            URL = URL + '&StartDate='+StartDate;
        }
        if(EndDate!=null){
            URL = URL + '&EndDate='+EndDate;
        }
        if(ReasonLost!=null){
            URL = URL + '&ReasonLost='+ReasonLost;
        }
        if(Probability!=null){
            URL = URL + '&Probability='+Probability;
        }           
        if(ReportView=='1') {URL = URL + '&ReportName=Corporate Banking Group %2D Closed Business';}
        if(ReportView=='2') {URL = URL + '&ReportName=Corporate Banking Group %2D Open Pipeline';}
        if(ReportView=='3') {URL = URL + '&ReportName=Corporate Banking Group %2D Lost Business %2D YTD(Reason Lost Analysis)';}
        if(ReportView=='4') {URL = URL + '&ReportName=Corporate Banking Group %2D Lost Business %2D YTD(Winning Competitor)';}  
        return URL;
    }
    
    public void ShowFootNote() {
        if (ReportView == '1' || ReportView == '2') { FootNote1 = true; FootNote2 = false; }
        if (ReportView == '3' || ReportView == '4') { FootNote2 = true; FootNote1 = false; }
    }

    public Component.Apex.PageBlockSection getSection(){
        string startDateLabel = 'Close Date From:';
        string endDateLabel = 'Close Date to:';
        
        if(ReportView == '1') {
            startDateLabel = 'Start Date:';
            endDateLabel = 'End Date:';
        }
        Component.Apex.PageBlockSection pbs= new Component.Apex.PageBlockSection(columns=1);
        pbs.id='pbs1';
        Component.Apex.PageBlockSectionItem pbsi = new Component.Apex.PageBlockSectionItem();
        Component.Apex.Inputtext StartDateInput = new Component.Apex.Inputtext(id = 'StartDate', label=startDateLabel,onfocus = 'DatePicker.pickDate(false, this , false);');
        StartDateInput.expressions.value = '{!StartDate}';
        pbs.childComponents.add(StartDateInput);             
                    
        Component.Apex.PageBlockSectionItem pbsi2 = new Component.Apex.PageBlockSectionItem();
        Component.Apex.Inputtext EndDateInput = new Component.Apex.Inputtext(id = 'EndDate', label=endDateLabel,onfocus = 'DatePicker.pickDate(false, this , false);');
        EndDateInput.expressions.value = '{!EndDate}';
        pbs.childComponents.add(EndDateInput);
        
        if(ReportView == '2'){
            Component.Apex.selectList ProbabilityPL = new Component.Apex.selectList(Label = 'Probability => :', id = 'ProbabilityPL', multiselect = false, size=1);
            ProbabilityPL.expressions.value = '{!Probability}';
            Component.Apex.selectOption PLOptions1 = new Component.Apex.SelectOption(itemValue = '25',itemLabel = '25');
            ProbabilityPL.childComponents.add(PLOptions1);
            Component.Apex.selectOption PLOptions2 = new Component.Apex.SelectOption(itemValue = '50',itemLabel = '50');
            ProbabilityPL.childComponents.add(PLOptions2);
            Component.Apex.selectOption PLOptions3 = new Component.Apex.SelectOption(itemValue = '75',itemLabel = '75');
            ProbabilityPL.childComponents.add(PLOptions3);                                
            Component.Apex.selectOption PLOptions4 = new Component.Apex.SelectOption(itemValue = '90',itemLabel = '90');
            ProbabilityPL.childComponents.add(PLOptions4);           
            pbs.childComponents.add(ProbabilityPL);
        } 

        if(ReportView == '3' || ReportView == '4'){                        
            Component.Apex.selectList ReasonLostPL = new Component.Apex.selectList(Label = 'Reason Lost:', id = 'ReasonLostPL', multiselect = true);
            ReasonLostPL.expressions.value = '{!ReasonLost}';
            Component.Apex.selectOption RLOptions1 = new Component.Apex.SelectOption(itemValue = 'Declined by bank',itemLabel = 'Declined by bank');
            ReasonLostPL.childComponents.add(RLOptions1);
            Component.Apex.selectOption RLOptions2 = new Component.Apex.SelectOption(itemValue = 'Declined by Prospect Client',itemLabel = 'Declined by Prospect Client');
            ReasonLostPL.childComponents.add(RLOptions2);
            Component.Apex.selectOption RLOptions3 = new Component.Apex.SelectOption(itemValue = 'Uncontrollable Loss',itemLabel = 'Uncontrollable Loss');
            ReasonLostPL.childComponents.add(RLOptions3);                                
            pbs.childComponents.add(ReasonLostPL);
        }   
        return pbs;
    }
    

    public class Cell {
        String value;
        String bgcolor ='#ffffff';
        String fntcolor ='#000000'; 
        string align = 'center';
        string indent = '0px';
        string width = '0px';
        string colspan = '1';
        string BottomBorder = 'none'; // border-bottom: 1px solid black
        public Cell(String value, String bgcolor, String fntcolor, String align, String indent, String width, String Colspan, String BottomBorder) {
            this.value = value;
            this.bgcolor = bgcolor;
            this.fntcolor = fntcolor;
            this.align = align;
            this.indent = indent;
            this.width = width;
            this.colspan = colspan;
            this.BottomBorder = BottomBorder;
        }
        public Cell(String value) {
            this.value = value;
        }
        public String getValue() {
            return this.value;
        }
        public String getBgColor() {
            return this.bgcolor;
        }
        public String getFntColor() {
            return this.fntcolor;
        }        
        public String getAlign() {
            return this.align;
        }                
        public String getIndent() {
            return this.indent;
        }                
        public String getWidth() {
            return this.width;
        }
        public String getColspan() {
            return this.colspan;
        }
        public String getBottomBorder() {
            return this.BottomBorder;
        }                                                               

    }
    
    public List<List<Cell>> getReportRows() {
        
        system.debug('getReportRows ' +reportView);
        List<List<Cell>> trows= new List<List<Cell>>();
        integer i = 0;
        integer header = 0;
        integer firstpass = 0;
        integer GrandTotals;
        string totals;
        List<string> ReportList = new List<string>();
        List<string> LeftHeader = new List<string>();
        if (this.doRerender!=null){
            if(ReportView == '1'){
                ReportList.add('Core_Corporate_Bank_Closed_Prev_Yr');
                ReportList.add('Core_2_Corporate_Bank_Closed_Prev_Yr');  
                ReportList.add('Specialized_Corp_Bank_Closed_Prev_Yr'); 
                totals = 'Corporate_Bank_Total_Closed_Prev_Yr'; 
            }
            if(ReportView == '2') {
                ReportList.add('Core_Pipeline_90_Day_75');
                ReportList.add('Core_2_Pipeline_90_Day');  
                ReportList.add('Specialized_Pipeline_90_Day_75');
                totals = 'Corporate_Total_Pipeline_90_Day_75';  
            }   
            If (ReportView == '3'){
                ReportList.add('Corp_Bank_Lost_Current_YTD');
                leftHeader.add('Lost Detail');
                ReportList.add('Corp_Bank_Lost_Detail_Current_YTD');
                leftHeader.add('Reason Lost Detail');
                GrandTotals = 2;        
            }
            If (ReportView == '4'){
                ReportList.add('Corporate_Bank_Winning_Competitor_YTD');
                leftHeader.add('Reason Lost Detail');
                GrandTotals = 1;
            }       
             
            for (string RL: ReportList){
                List <Report> ReportListID = [SELECT Id,DeveloperName, Format FROM Report where DeveloperName = :RL];
                String ReportId = (String) ReportListID.get(0).get('Id');
                String ReportFormat =  (String) ReportListID.get(0).get('Format');
                    Reports.ReportDescribeResult describe = Reports.ReportManager.describeReport(reportId);
                    Reports.ReportMetadata reportMd = describe.getReportMetadata();
                    Reports.standardDateFilter Datefilter = reportMd.getstandardDateFilter();
                
                    if(onSearch == true){
                        if(startdate.contains('/')){
                            list<string> STdatepart = startdate.split('/');
                            startdate = STdatepart[2]+'-'+STdatepart[0]+'-'+STdatepart[1];                          
                        }

                        Datefilter.setstartDate(startDate);
                        if(enddate.contains('/')){
                            list<string> EDdatepart = enddate.split('/');
                            enddate = EDdatepart[2]+'-'+EDdatepart[0]+'-'+EDdatepart[1];
                        }
                        Datefilter.setendDate(endDate);
                        
                        Datefilter.setdurationValue('CUSTOM');
                    } 
                    if (startdate == null){
                        If (reportview == '2'){
                            startdate = date.today().addDays(-30).format();
                            
                        } else {
                            startdate = date.valueOf(Datefilter.getstartDate()).format();                           
                        }

                    }
                    if(endDate == null){
                        If (reportview == '2'){
                            enddate = date.today().addDays(90).format();
                        } else {
                            enddate = date.valueOf(Datefilter.getendDate()).format();
                        }                       
                    }                   

                        for(Reports.ReportFilter filter: reportMd.getReportFilters()){
                            if (filter.getcolumn() == 'Opportunity.Reason_Lost__c') {
                                if(ReasonLost != null){
                                    filter.setValue(ReasonLost.remove('[').remove(']'));
                                } else {
                                    ReasonLost = filter.getValue();
                                }       
                            }
                            system.debug('filter.getcolumn() '+filter.getcolumn());
                            system.debug('filter.getcolumn() '+filter.getvalue());                          
                            if (filter.getcolumn() == 'Opportunity.Probability')    {
                                if(Probability != null){
                                    filter.setValue(Probability);
                                } else {
                                    Probability = filter.getValue();
                                }       
                            }           
                                        
                        }

                If(ReportFormat == 'Summary'){
                    Reports.reportResults results = Reports.ReportManager.runReport(ReportId,reportMd);
    
                    if (header == 0){ 
                        List<Cell> headerth = new List<Cell>();
                        headerth.add(new Cell('','#548235','#ffffff','center', '0px', '170px', '1', 'none'));
                        for(String column:results.getReportMetadata().getdetailColumns()) {
                            if (results.getReportExtendedMetadata().getdetailColumnInfo().get(column).getLabel() != 'Pipeline Name'){
                                headerth.add(new Cell(results.getReportExtendedMetadata().getdetailColumnInfo().get(column).getLabel(),'#548235','#ffffff','center', '0px', '25px', '1', 'none'));
                            }
                        }
                        trows.add(headerth);
                    }
                    //colors - #ffffff = white, #A9D08E = light green, black = #000000
                    //cell - value, bgcolor, fntcolor, align, indent, width, colspan, 
                    for(Reports.GroupingValue grouping: results.getGroupingsDown().getGroupings()) {
                        //Main Grouping Label & Values
                        List<Cell> grouprow = new List<Cell>();
                        grouprow.add(new Cell(grouping.getLabel(),'#A9D08E','#ffffff','left', '0px', '170px', '1', 'none'));
                        Reports.ReportFact fact =  results.getFactMap().get(grouping.getKey()+'!T');
                        i = 0;
                        //Main Grouping Values          
                        for(Reports.SummaryValue agg: fact.getAggregates()) {
                            i++;
                            //only get 8 columns
                            if(i <= 8){
                                grouprow.add(new Cell(agg.getLabel().substringbefore('.'),'#A9D08E','#ffffff','right', '0px', '25px', '1', 'none'));
                            }
                        }
                        trows.add(grouprow);
                        //Subgrouping Label & Values
                        for(Reports.GroupingValue subgrouping:grouping.getGroupings()) {
                            List<Cell> subgrouprow = new List<Cell>();
                            //subgrouprow.add(new Cell('')); //blank to the left
                            subgrouprow.add(new Cell(subgrouping.getLabel(),'#ffffff','#000000','left', '10px', '170px', '1', 'none'));
                            fact =  results.getFactMap().get(subgrouping.getKey()+'!T');
                            i = 0;
                            for(Reports.SummaryValue agg: fact.getAggregates()) {
                                i++;
                                //only get 8 columns
                                if(i <= 8){
                                    subgrouprow.add(new Cell(agg.getLabel().substringbefore('.'),'#ffffff','#000000','right', '0px', '25px', '1', 'none'));
                                }
                            }
                         trows.add(subgrouprow);
                        }
                    }
                    header++;
                }
                If (ReportFormat == 'Matrix'){
                    Reports.reportResults results = Reports.ReportManager.runReport(ReportId,reportMD);
                    if (header == 0){ 
                        List<Cell> headerth1 = new List<Cell>();
                        headerth1.add(new Cell('Sum of Credit Commitment Production','#60943c','#ffffff','center', '0px', '', '1', 'none'));
                        headerth1.add(new Cell('Corporate Bank Segment','#60943c','#ffffff','center', '0px', '', '7', 'none'));
                        trows.add(headerth1);
                    }
                        List<Cell> headerth = new List<Cell>();
                        headerth.add(new Cell(leftHeader.get(header),'#548235','#ffffff','center', '0px', '170px', '1', 'none'));
                        for(Reports.GroupingValue groupingA: results.getGroupingsAcross().getGroupings()){
                            headerth.add(new Cell(groupinga.getLabel(),'#548235','#ffffff','center', '0px', '25px', '1', 'none'));
                        }
                        headerth.add(new Cell('Grand Total','#548235','#ffffff','center', '0px', '25px', '1', 'none'));                 
                        trows.add(headerth);                
                    header++;
                    //colors - #ffffff = white, #A9D08E = light green, black = #000000
                    //cell - value, bgcolor, fntcolor, align, indent, width
                    for(Reports.GroupingValue grouping: results.getGroupingsDown().getGroupings()) {
                        //Main Grouping Label & Values
                        List<Cell> grouprow = new List<Cell>();
                        grouprow.add(new Cell(grouping.getLabel(),'#A9D08E','#000000','left', '0px', '170px', '1', '.5px solid green'));
                        for(Reports.GroupingValue groupingA: results.getGroupingsAcross().getGroupings()){
                             Reports.ReportFact fact =  results.getFactMap().get(grouping.getKey()+'!'+groupingA.getKey());
                             Reports.SummaryValue Value = fact.getAggregates()[0];
                             grouprow.add(new Cell(value.getLabel().substringbefore('.'),'#ffffff','#000000','right', '0px', '25px', '1', '.5px solid Green'));
                        }
                        //Grand Totals Down
                        Reports.ReportFact fact =  results.getFactMap().get(grouping.getKey()+'!T');
                        Reports.SummaryValue Value = fact.getAggregates()[0];
                        grouprow.add(new Cell(value.getLabel().substringbefore('.'),'#A9D08E','#000000','right', '0px', '25px', '1', '.5px solid green'));
                        trows.add(grouprow);
                    }
                    //Grand Totals Across
                    if (GrandTotals == header) {
                        List<Cell> grouprow = new List<Cell>();
                        grouprow.add(new Cell('Grand Total','#548235','#ffffff','right', '0px', '170px', '1', 'none')); 
                        for(Reports.GroupingValue groupingA: results.getGroupingsAcross().getGroupings()){
                            Reports.ReportFact fact =  results.getFactMap().get('T!'+groupingA.getKey());
                            Reports.SummaryValue Value = fact.getAggregates()[0];
                            grouprow.add(new Cell(value.getLabel().substringbefore('.'),'#548235','#ffffff','right', '0px', '25px', '1', 'none'));
                        }
                            Reports.ReportFact fact =  results.getFactMap().get('T!T');
                            Reports.SummaryValue Value = fact.getAggregates()[0];
                            grouprow.add(new Cell(value.getLabel().substringbefore('.'),'#548235','#ffffff','right', '0px', '25px', '1', 'none'));
                            trows.add(grouprow);    
                    }           
                } //end of matrix
            }
            if(totals != null){
                //colors - #ffffff = white, #A9D08E = light green, black = #000000
                //cell - value, bgcolor, fntcolor, align, indent, width
                // grandtotals - this  cannot be included in the loop above
                List <Report> GTReportList = [SELECT Id,DeveloperName FROM Report where DeveloperName = :totals];
                String GTReportId = (String)GTReportList.get(0).get('Id');
                Reports.ReportDescribeResult describe = Reports.ReportManager.describeReport(GTReportId);
                Reports.ReportMetadata reportMd = describe.getReportMetadata();
                Reports.standardDateFilter Datefilter = reportMd.getstandardDateFilter();
                if(onSearch == true){
                    if(startdate.contains('/')){
                    list<string> STdatepart = startdate.split('/');
                    startdate = STdatepart[2]+'-'+STdatepart[0]+'-'+STdatepart[1];
                    }
                    Datefilter.setstartDate(startDate);
                    if(enddate.contains('/')){
                    list<string> EDdatepart = enddate.split('/');
                    enddate = EDdatepart[2]+'-'+EDdatepart[0]+'-'+EDdatepart[1];
                    }
                    Datefilter.setendDate(endDate);
                                            
                    Datefilter.setdurationValue('CUSTOM');
                }
                for(Reports.ReportFilter filter: reportMd.getReportFilters()){
                    if (filter.getcolumn() == 'Opportunity.Probability')    {
                        if(Probability != null){
                            filter.setValue(Probability);
                        } else {
                            Probability = filter.getValue();
                        }       
                    }           
                                
                }                 
                Reports.reportResults GTresults = Reports.ReportManager.runReport(GTReportId,reportMd);
                    List<Cell> grouprow = new List<Cell>();
                    grouprow.add(new Cell('Total','#A9D08E','#ffffff','left', '0px', '170px', '1', 'none'));
                    Reports.ReportFact fact =  GTresults.getFactMap().get('T!T');
                    i = 0;
                    for(Reports.SummaryValue agg: fact.getAggregates()) {
                        i++;
                        //only get 8 columns
                        if(i <= 8){
                            grouprow.add(new Cell(agg.getLabel().substringbefore('.'),'#A9D08E','#ffffff','right', '0px', '25px', '1', 'none'));
                        }
                    }
                    trows.add(grouprow);
            }
        }//if doredender
        RptURL = buildURL();
        return trows;
    }//end of class
    
}