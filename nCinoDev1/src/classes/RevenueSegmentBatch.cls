global class RevenueSegmentBatch implements Database.batchable<sObject> {
	global final String Query;
	global boolean GIB;
    global boolean CMM;
    global boolean RBC;
    global boolean SpecInd;
    global boolean Corp; 
    global boolean updtRevSeg; 
	
	
	global RevenueSegmentBatch() {
	   	
      
      //Query = 'Select Id, OpportunityId, User.C_I_Type__c, User.LOB__c,  Revenue_Segment__c from OpportunityTeamMember where OpportunityId in (select id from opportunity where (RecordType.Name = \'TM Open Pipeline\' or RecordType.Name = \'TM Closed Pipeline\' or RecordType.Name = \'Open Pipeline\' or RecordType.Name = \'Closed Pipeline\') and (CloseDate = this_year or CloseDate = Last_year))';
      Query = 'Select Id, OpportunityId, User.C_I_Type__c, User.LOB__c, User.Specialized_Industries__c,  Revenue_Segment__c from OpportunityTeamMember';
	} 	
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<OpportunityTeamMember> OppTeamList){
		
		for (OpportunityTeamMember otm : OppTeamList){
		    GIB = false;
		    CMM = false;
		    RBC = false;
		    SpecInd = false;
		    Corp = false;		    
			updtRevSeg = false;		
			for(OpportunityTeamMember otms : [SELECT id, User.C_I_Type__c, User.LOB__c, User.Specialized_Industries__c, Revenue_Segment__c From OpportunityTeamMember WHERE  OpportunityId = :otm.OpportunityId]  ){
				system.debug(otms);
		              if (otms.User.C_I_Type__c == 'GIB') {
		                     GIB = true;
		              }
		              if (otms.User.C_I_Type__c == 'CMM') {
		                     CMM = true;
		              }
		              if (otms.User.LOB__c == 'Regions Business Capital') {
		                     RBC = true;
		              }
		              if (otms.User.Specialized_Industries__c == true) {
		                     SpecInd =  true;
		              }
		              if (otms.User.C_I_Type__c == 'Corporate') {
		                     Corp =  true;
		              }
		
			}
			if (CMM == true){			 
				if (otm.User.C_I_Type__c == 'CMM'){
	           		
	              	if ((GIB == true) || (RBC == true) || (SpecInd == true)){
	                	otm.Revenue_Segment__c = 'Shared';
	                    updtRevSeg = true;
	              	}
	              	if (((GIB == false) && (RBC == false) && (SpecInd == false)) || (Corp == true)){
	                	otm.Revenue_Segment__c = 'Core';
	                    updtRevSeg = true;
	              	}      
	   			}
			}
			if (GIB == true){	
	   			if (otm.User.C_I_Type__c == 'GIB'){
	              	if (RBC == true || SpecInd == true) {
	                    otm.Revenue_Segment__c = 'Shared';
	                    updtRevSeg = true;
	              	}      	              	
	              	if ((RBC == false || SpecInd == false) || (CMM == true) || (Corp == true)) {
	                    otm.Revenue_Segment__c = 'Core';
	                    updtRevSeg = true;
	              	}      
	   			}
			}
   			if (updtRevSeg == true){
   				system.debug(otm);
   				update otm;
   			}

		}
	}

	global void finish(Database.BatchableContext BC){
		
	}	

}