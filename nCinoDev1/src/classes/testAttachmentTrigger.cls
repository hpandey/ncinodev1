/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testAttachmentTrigger {

    static testMethod void myUnitTest() {
    	           Account account = new Account(Name='Test Account');
            insert account;      
            // Access the account that was just created. 
    
            Account insertedAcct = [SELECT Id,Name FROM Account WHERE Name='Test Account'];
   		Blob b = Blob.valueOf('111224422424242424');
		Attachment attach1= new Attachment();
		attach1.ParentId = insertedAcct.id;
		attach1.Name = 'Test Attachment for Parent';
		attach1.Body = b;
		
		insert attach1;          
    }
}