/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
global class TestWebServiceActivities implements WebServiceMock {
   //public GetActivities_Response.ArrayOfEventDetailObject getEventsResult;
   //public GetActivities_Response.EventDetailObject[] EventDetailObject;
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
		//GetActivities_Stub.getEvents_element request_x = new GetActivities_Stub.getEvents_element();
       //AccountBalance.FetchSalesForceAccountProfileResponse_element respElement = new AccountBalance.FetchSalesForceAccountProfileResponse_element();
        System.debug('We are in TestWebserviceActivites');
        GetActivities_Stub.getEvents_element request_x = new GetActivities_Stub.getEvents_element();
        GetActivities_Stub.getEventsResponse_element response_x;
      //AccountBalanceArrays.KeyValueOfstringstring_element[] keyValue= new AccountBalanceArrays.KeyValueOfstringstring_element[1];
		GetActivities_Response.EventDetailObject[] keyValue = new GetActivities_Response.EventDetailObject[1];
		//{new AccountBalanceArrays.KeyValueOfstringstring_element()};
		keyValue[0]=new GetActivities_Response.EventDetailObject();
		keyValue[0].EventDescription = 'Account Information Maintenance';
		keyValue[0].SourceDescription = 'Sales and Service';
		keyValue[0].AccountNumber = '0000000000109555307';
		keyValue[0].ProductCode = '';
		keyValue[0].RCIFID = '00001235356472000';
		keyValue[0].AssocUserBranchNumber = 112;
		keyValue[0].AssocUserBankNumber = 1;
		keyValue[0].AssocUserName = 'CHERYL KRUSE';
		keyValue[0].AssocUserID = 'U472LLN';
		keyValue[0].EventGuid = '3e61a6b6-ff06-4b7a-a36b-6c9455cef4fd';
		keyValue[0].CreatedDate = datetime.now();
		System.debug('keyValue in TestWebserviceActivites: ' + keyValue);
		//public GetActivities_Response.EventDetailObject[] EventDetailObject;
		GetActivities_Response.ArrayOfEventDetailObject res = new GetActivities_Response.ArrayOfEventDetailObject();
		//AccountBalanceArrays.ArrayOfKeyValueOfstringstring keyValueString= new AccountBalanceArrays.ArrayOfKeyValueOfstringstring();
		//keyValueString.KeyValueOfstringstring=keyValue;
   		res.EventDetailObject = keyValue;
		//respElement.FetchSalesForceAccountProfileResult = keyValueString;
       	//request_x = keyValue;//.getEventsResult
       	System.debug('keyValue in TestWebserviceActivites: ' + keyValue);
       	System.debug('res in TestWebserviceActivites: ' + res);
		response.put('response_x', res); 
   }
}