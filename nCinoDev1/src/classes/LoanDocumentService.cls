/************************************************************************************
 Apex Class Name     : LoanDocumentService
 Version             : 1.0
 Created Date        : 16th Feb 2016
 Function            : Helper Class for LoanDocumentTriggerHandler. This Class will have all the logic.
 Author              : Deloitte Consulting
 Modification Log    :
* Developer                         Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar                 3/31/2016                Original Version
*************************************************************************************/

public Class LoanDocumentService {

    /************************************************************************
    Method Name: getAsyncEntryForApprovedLoanDoc
    Author Name: Pranil Thubrikar
    Description: 
    Parameters: List<LLC_BI__LLC_LoanDocument__c> listLoanDoc
    Returns: List<Asynchronous_Queue__c>
    *************************************************************************/
    public List<Asynchronous_Queue__c> getAsyncEntryForApprovedLoanDoc(List<LLC_BI__LLC_LoanDocument__c> listLoanDoc) {
        
        List<Asynchronous_Queue__c> listAsyncQueue = new List<Asynchronous_Queue__c>();
        
        if(listLoanDoc != null && !listLoanDoc.isEmpty()){
            for(LLC_BI__LLC_LoanDocument__c loanDocRec : listLoanDoc){
                Asynchronous_Queue__c newAsyncQueueRec = new Asynchronous_Queue__c();
                //To be updated with more details.
                newAsyncQueueRec.Record_Ids__c = loanDocRec.Id;
                newAsyncQueueRec.Request_Type__c = 'Immediate With CallOut';
                listAsyncQueue.add(newAsyncQueueRec);
            }
        }
        return listAsyncQueue;
    }
    
    /************************************************************************
    Method Name: getAsyncEntryForUpdatedLoanDoc
    Author Name: Pranil Thubrikar
    Description: 
    Parameters: List<LLC_BI__LLC_LoanDocument__c> listLoanDoc
    Returns: List<Asynchronous_Queue__c>
    *************************************************************************/
    public List<Asynchronous_Queue__c> getAsyncEntryForUpdatedLoanDoc(List<LLC_BI__LLC_LoanDocument__c> listLoanDoc){
        
        List<Asynchronous_Queue__c> listAsyncQueue = new List<Asynchronous_Queue__c>();
        
        if(listLoanDoc != null && !listLoanDoc.isEmpty()){
            for(LLC_BI__LLC_LoanDocument__c loanDocRec : listLoanDoc){
                Asynchronous_Queue__c newAsyncQueueRec = new Asynchronous_Queue__c();
                //To be updated with more details.
                newAsyncQueueRec.Request_Type__c = 'Immediate With CallOut';
                listAsyncQueue.add(newAsyncQueueRec);
            }
        }
        return listAsyncQueue;
    }
}