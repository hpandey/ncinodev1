/************************************************************************************
 Apex Class Name     : EntityInvolvementTriggerHandler
 Version             : 1.0
 Created Date        : 1st Mar 2016
 Function            : Handler Class for Entity Involvement Trigger. This Class will not have any logic and will call methods in Helper Class.
 Author              : Deloitte Consulting
 Modification Log    :
* Developer                         Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar                3/01/2016                Original Version
*************************************************************************************/

public Class EntityInvolvementTriggerHandler implements ITriggerHandler {
    
    //Create Object for Service Class to call appropriate methods.
    EntityInvolvementService entityInvolvementServiceObj = new EntityInvolvementService();
    
    //Handler Method for Before Insert.
    public void BeforeInsert(List<sObject> newItems) {
    }
    
    //Handler Method for Before Update.
    public void BeforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
    }
    
    //Handler Method for Before Delete.    
    public void BeforeDelete(Map<Id, sObject> oldItems) {
        List<Regulatory_and_Fraud_Indicators__c> listResultDelete = new List<Regulatory_and_Fraud_Indicators__c>();
        
        //Get Prescreening Result records to delete.
        listResultDelete = entityInvolvementServiceObj.getPrescreenResultsToDelete((list<LLC_BI__Legal_Entities__c>)oldItems.values());
        
        //Delete Prescreening Result Records.
        if(listResultDelete != null && !listResultDelete.isEmpty()){
            List<Database.DeleteResult> listDeleteResult = Database.Delete(listResultDelete,false);
            if(!listDeleteResult.isEmpty()){
                //Add Error on Entity Involvement Records if Prescreening Result records are not created.
                for(Integer index = 0; index < listResultDelete.size(); index++){
                    if(!listDeleteResult[index].isSuccess()){
                        if(listResultDelete[index].Entity__c != null){
                            oldItems.get(listResultDelete[index].Entity__c).addError(Label.Prescreening_Insert_Error+ ' : '+UtilityClass.getErrorMessages(listDeleteResult[index].getErrors()));
                        }
                    }
                }
                utilityClass.processDeleteResult(listDeleteResult);
            }
        }
    }
    
    //Handler method for After Insert.
    public void AfterInsert(Map<Id, sObject> newItems) {

        List<LLC_BI__Legal_Entities__c> listFilterEntity = new List<LLC_BI__Legal_Entities__c>();
        List<Regulatory_and_Fraud_Indicators__c> listResultInsert = new List<Regulatory_and_Fraud_Indicators__c>();
        List<Regulatory_and_Fraud_Indicators__c> listFinalResultInsert = new List<Regulatory_and_Fraud_Indicators__c>();
        Map<Id, LLC_BI__Loan__c> mapEntityIdWithLoan = new Map<Id, LLC_BI__Loan__c>();
        List<Asynchronous_Queue__c> listAsyncQueueInsert = new List<Asynchronous_Queue__c>();
        
        //Code to Insert Asynchronous Queue Records for Checks & Flags Integration.
        listAsyncQueueInsert = entityInvolvementServiceObj.evaluateAsyncQueueRecs((List<LLC_BI__Legal_Entities__c>)newItems.values());
        if(listAsyncQueueInsert != null && !listAsyncQueueInsert.isEmpty()){
            List<Database.SaveResult> listSaveResult = Database.Insert(listAsyncQueueInsert,false);
            if(!listSaveResult.isEmpty()){
                //Add Error on Entity Involvement Records if Async Queue records are not created.
                for(Integer index = 0; index < listAsyncQueueInsert.size(); index++){
                    if(!listSaveResult[index].isSuccess()){
                        if(listAsyncQueueInsert[index].Record_Ids__c != null){
                            newItems.get(listAsyncQueueInsert[index].Record_Ids__c).addError(Label.AsyncQueueError+ ' : '+UtilityClass.getErrorMessages(listSaveResult[index].getErrors()));
                        }
                    }
                }
                utilityClass.processSaveResult(listSaveResult);
            }
        }
        
        //Code for Prescreening Result.
        listFilterEntity = getRelevantEntities(newItems, null);
        
        if(listFilterEntity != null && !listFilterEntity.isEmpty()){
            //Get Prescreening Result Records to insert.
            listResultInsert = entityInvolvementServiceObj.evaluatePrescreenRules(listFilterEntity);
            
            //Insert Prescreening Result Records.
            if(listResultInsert != null && !listResultInsert.isEmpty()){
                for(Regulatory_and_Fraud_Indicators__c prescreenRec : listResultInsert){
                    if((prescreenRec.Flag_Type__c == Label.Vermont || prescreenRec.Flag_Type__c == Label.Out_of_Market) && prescreenRec.Result__c == Label.Failed){
                        listFinalResultInsert.add(prescreenRec);
                    }
                }
                if(!listFinalResultInsert.isEmpty()){
                    List<Database.SaveResult> listSaveResult = Database.Insert(listFinalResultInsert,false);
                    if(!listSaveResult.isEmpty()){
                        //Add Error on Entity Involvement Records if Prescreening Result records are not created.
                        for(Integer index = 0; index < listFinalResultInsert.size(); index++){
                            if(!listSaveResult[index].isSuccess()){
                                if(listFinalResultInsert[index].Entity__c != null){
                                    newItems.get(listFinalResultInsert[index].Entity__c).addError(Label.Prescreening_Insert_Error+ ' : '+UtilityClass.getErrorMessages(listSaveResult[index].getErrors()));
                                }
                            }
                        }
                        utilityClass.processSaveResult(listSaveResult);
                    }
                }
            }
        }
    }

    //Handler method for After Update.    
    public void AfterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {

        List<LLC_BI__Legal_Entities__c> listNewEntities = new List<LLC_BI__Legal_Entities__c>();
        List<Regulatory_and_Fraud_Indicators__c> listResultDelete = new List<Regulatory_and_Fraud_Indicators__c>();
        List<Regulatory_and_Fraud_Indicators__c> listResultInsert = new List<Regulatory_and_Fraud_Indicators__c>();
        List<Regulatory_and_Fraud_Indicators__c> listFinalResultInsert = new List<Regulatory_and_Fraud_Indicators__c>();
        
        listNewEntities = getRelevantEntities(newItems, oldItems);

        if(listNewEntities != null && !listNewEntities.isEmpty()){
            //Get Prescreening Result Records to delete.
            listResultDelete = entityInvolvementServiceObj.getPrescreenResultsToDelete(listNewEntities);
            
            //Delete Prescreening Result Records.
            if(listResultDelete != null && !listResultDelete.isEmpty()){
                List<Database.DeleteResult> listDeleteResult = Database.Delete(listResultDelete,false);
                if(!listDeleteResult.isEmpty()){
                    //Add Error on Entity Involvement Records if Prescreening Result records are not created.
                    for(Integer index = 0; index < listResultDelete.size(); index++){
                        if(!listDeleteResult[index].isSuccess()){
                            if(listResultDelete[index].Entity__c != null){
                                oldItems.get(listResultDelete[index].Entity__c).addError(Label.Prescreening_Insert_Error+ ' : '+UtilityClass.getErrorMessages(listDeleteResult[index].getErrors()));
                            }
                        }
                    }
                    utilityClass.processDeleteResult(listDeleteResult);
                }
            }
            
            //Get Prescreening Result Records to insert.
            listResultInsert = entityInvolvementServiceObj.evaluatePrescreenRules(listNewEntities);
            
            //Insert Prescreening Result Records.
            if(listResultInsert != null && !listResultInsert.isEmpty()){
                //Check to insert only Failed Prescreening Results.
                for(Regulatory_and_Fraud_Indicators__c presceeningResult : listResultInsert){
                    if(presceeningResult.Flag_Type__c == Label.Out_of_Market && presceeningResult.Result__c == Label.Failed){
                        listFinalResultInsert.add(presceeningResult);
                    }
                    else if(presceeningResult.Flag_Type__c == Label.Vermont && presceeningResult.Result__c == Label.Failed){
                        listFinalResultInsert.add(presceeningResult);
                    }
                }
                if(!listFinalResultInsert.isEmpty()){
                    List<Database.SaveResult> listSaveResult = Database.Insert(listFinalResultInsert,false);
                    if(!listSaveResult.isEmpty()){
                        //Add Error on Entity Involvement Records if Prescreening Result records are not created.
                        for(Integer index = 0; index < listFinalResultInsert.size(); index++){
                            if(!listSaveResult[index].isSuccess()){
                                if(listFinalResultInsert[index].Entity__c != null){
                                    newItems.get(listFinalResultInsert[index].Entity__c).addError(Label.Prescreening_Insert_Error+ ' : '+UtilityClass.getErrorMessages(listSaveResult[index].getErrors()));
                                }
                            }
                        }
                        utilityClass.processSaveResult(listSaveResult);
                    }
                }
            }
        }
    }

    //Handler method for After Delete    
    public void AfterDelete(Map<Id, sObject> oldItems) {
    }

    //Handler method for After Undelete.
    public void AfterUndelete(Map<Id, sObject> newItems) {
        //invoke the AfterInsert logic here
        AfterInsert(newItems);
    }
    
    /************************************************************************
    Method Name: getRelevantEntities
    Author Name: Pranil Thubrikar
    Description: Method to filter the relevent Entity Involvement records and return it.
    Parameters: Map<Id, sObject> newItems, Map<Id, sObject> oldItems
    Returns: List<LLC_BI__Legal_Entities__c>
    *************************************************************************/
    private List<LLC_BI__Legal_Entities__c> getRelevantEntities(Map<Id, sObject> newItems, Map<Id, sObject> oldItems){
        
        Set<String> setApplicableStages = new Set<String>();
        List<LLC_BI__Legal_Entities__c> listNewEntities = new List<LLC_BI__Legal_Entities__c>();
        Map<Id, LLC_BI__Loan__c> mapEntityIdWithLoan = new Map<Id, LLC_BI__Loan__c>();
        
        if(Label.Loan_Stages_Prescreening!=null && !String.isBlank(Label.Loan_Stages_Prescreening) && Label.Loan_Stages_Prescreening.contains(';')) {
            setApplicableStages.addAll(Label.Loan_Stages_Prescreening.split(';'));
        }
        else{
            for(LLC_BI__Legal_Entities__c legalEntity : (List<LLC_BI__Legal_Entities__c>)newItems.values()){
                legalEntity.addError(Label.Prescreening_Insert_Error + ' : Custom Label for Loan Stages is Null or Empty.');
            }
        }
        if(setApplicableStages != null && !setApplicableStages.isEmpty() && newItems != null){
            //Get Loans related with New Entity Involvement.
            mapEntityIdWithLoan = entityInvolvementServiceObj.getRelatedLoanDetails((List<LLC_BI__Legal_Entities__c>)newItems.values());
            //Filter Entity Involvement Records if the Client is changed.
            for(LLC_BI__Legal_Entities__c legalEntity : (List<LLC_BI__Legal_Entities__c>)newItems.values()){
                if(legalEntity.LLC_BI__Loan__c != null){
                    if(setApplicableStages.contains(mapEntityIdWithLoan.get(legalEntity.Id).LLC_BI__Stage__c)){    
                        if(OldItems != null && legalEntity.LLC_BI__Account__c != ((LLC_BI__Legal_Entities__c)oldItems.get(legalEntity.Id)).LLC_BI__Account__c){
                            listNewEntities.add(legalEntity);
                        }
                        else{
                            listNewEntities.add(legalEntity);
                        }
                    }
                }
            }
        }
        return listNewEntities;
    }
}