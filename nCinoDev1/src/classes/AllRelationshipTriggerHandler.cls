public class AllRelationshipTriggerHandler {
    public AllRelationshipTriggerHandler(){
        //Handler
    }
    //Description   Resource    Path    Location    Type
    public void OnAfterPDMInsert(List<PDM__c> insertRecords){
        //System.debug('You just created a new PDM about to recalculate your sharing ' + insertRecords[0].Relationship__c);
        /*********************************************************************************************************************************************************
        This trigger will only add the Client Team members to the new PDM__c when one is added by a team member or the owner. It won't run the
        Recalculate sharing for all clients or the relationship attached to the client.
        **********************************************************************************************************************************************************/
        List<PDM__Share> newShares = new List<PDM__Share>();
        List<Relationship__Share> relationshipShares = [Select id,ParentId, RowCause, UserOrGroupId from Relationship__Share where ParentId =: insertRecords[0].Relationship__c];
        for (Relationship__Share rs: relationshipShares){
            if(rs.UserOrGroupId != Userinfo.getUserId()){//Had to exclude the owner of the Call_Plan because it doesn't want the User's ID in the table more than once.
                PDM__Share curShare = new PDM__Share(ParentId=insertRecords[0].id, RowCause='ASR_Relationship_Details__c', UserOrGroupId=rs.UserOrGroupId, AccessLevel='Edit');
                newShares.add(curShare);
            }
        }
        insert newShares;
    }
    //Relationship Strategy Plans
    public void OnAfterRSPInsert(List<Relationship_Strategy_Plan__c> insertRecords){ 
        //System.debug('You just created a new Relationship Strategy Plan about to recalculate your sharing ' + insertRecords[0].Relationship__c);
        /*********************************************************************************************************************************************************
        This trigger will only add the Client Team members to the new Relationship_Strategy_Plan__c when one is added by a team member or the owner. It won't run the
        Recalculate sharing for all clients or the relationship attached to the client.
        **********************************************************************************************************************************************************/
        List<Relationship_Strategy_Plan__Share> newShares = new List<Relationship_Strategy_Plan__Share>();
        List<Relationship__Share> relationshipShares = [Select id,ParentId, RowCause, UserOrGroupId from Relationship__Share where ParentId =: insertRecords[0].Relationship__c];
        for (Relationship__Share rs: relationshipShares){
            if(rs.UserOrGroupId != Userinfo.getUserId()){//Had to exclude the owner of the Call_Plan because it doesn't want the User's ID in the table more than once.
                Relationship_Strategy_Plan__Share curShare = new Relationship_Strategy_Plan__Share(ParentId=insertRecords[0].id, RowCause='ASR_Relationship_Details__c', UserOrGroupId=rs.UserOrGroupId, AccessLevel='Edit');
                newShares.add(curShare);
            }
        }
        insert newShares;
    }
    public void OnAfterCallPlanInsert(List<Call_Plan__c> insertRecords){
        System.debug('You just created a new Call Plan about to recalculate your sharing ' + insertRecords[0].Client__c);
        /*********************************************************************************************************************************************************
        This trigger will only add the Client Team members to the new Call Plan when one is added by a team member or the owner. It won't run the
        Recalculate sharing for all clients or the relationship attached to the client.
        **********************************************************************************************************************************************************/
        List<Call_Plan__Share> newShares = new List<Call_Plan__Share>();
        List<AccountShare> clientShares = [Select id,AccountId, RowCause, UserOrGroupId from AccountShare where AccountId =: insertRecords[0].Client__c];
        for (AccountShare cs: clientShares){
            if(cs.UserOrGroupId != Userinfo.getUserId()){//Had to exclude the owner of the Call_Plan because it doesn't want the User's ID in the table more than once.
                Call_Plan__Share curShare = new Call_Plan__Share(ParentId=insertRecords[0].id, RowCause='ASR_Parent_Account_Team_Member__c', UserOrGroupId=cs.UserOrGroupId, AccessLevel='Edit');
                system.debug(curShare); 
                newShares.add(curShare);
            }
        }

        insert newShares;

    }
    
    
/*********************************************************************************************************************************************************
Nightly batch code only to be used under the Regions Migration profile
**********************************************************************************************************************************************************/
    public void OnBeforeNightlyClientUpdate(List<Account> updateRecords){
        List<Sharing_Batch__c> newSharesToBatch = new List<Sharing_Batch__c>();
        List<Sharing_Batch__c> currentRelationshipsToBatch =  [Select Relationship__c From Sharing_Batch__c where batch_changed__c =null];
        Set<Id> currentRelationshipIDsToBatch = new Set<Id>();
        for (Sharing_Batch__c toShare : currentRelationshipsToBatch){
            currentRelationshipIDsToBatch.add(toShare.Relationship__c);
        }
        set<ID> ownerIDs = new set<ID>();
        
        for(Account acc : updateRecords) {
            ownerIDs.add(acc.ownerID);
        }
        
        Map<Id,user> userMap = new Map<Id,User>([SELECT ID, Profile.Name FROM User WHERE ID IN: ownerIDs]);
        
        for (Account recordID : updateRecords){
            // bypass record owned by Regions Migration profile
            if (userMap.get(recordId.ownerID).profile.name == 'Regions Migration') {
                continue;
            }
            if (!currentRelationshipIDsToBatch.contains(recordID.Relationship__c)){
                Sharing_Batch__c share= new Sharing_Batch__c(
                Relationship__c = recordID.Relationship__c,
                Reason__c = 'Owner Change - Account');
                newSharesToBatch.add(share);
                currentRelationshipIDsToBatch.add(recordID.Relationship__c);
            }
        }
        if (newSharesToBatch.size() > 0){
            try{
                insert newSharesToBatch;
            } catch (DMLException f) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error adding to Batch Share table'));
            }
        }
    }
    public void OnBeforeNightlyRelationshipUpdate(List<Relationship__c> updateRecords){
        //Need to see if the owner changed - already done in trigger
        List<Sharing_Batch__c> newSharesToBatch = new List<Sharing_Batch__c>();
        List<Sharing_Batch__c> currentRelationshipsToBatch =  [Select Relationship__c From Sharing_Batch__c where batch_changed__c=null];
        Set<Id> currentRelationshipIDsToBatch = new Set<Id>();
        for (Sharing_Batch__c toShare : currentRelationshipsToBatch){
            currentRelationshipIDsToBatch.add(toShare.Relationship__c);
        }
        set<ID> ownerIDs = new  set<ID>();
        for(Relationship__c relation : updateRecords) {
                ownerIDs.add(relation.ownerID);
        }
        Map<Id,user> userMap = new Map<Id,User>([SELECT ID, Profile.Name FROM User WHERE ID IN: ownerIDs]);
        
        for (Relationship__c recordID : updateRecords){
            // bypass record owned by Regions Migration profile
            if (userMap.get(recordId.ownerID).profile.name == 'Regions Migration') {
                continue;
            }
            if (!currentRelationshipIDsToBatch.contains(recordID.Id)){
                Sharing_Batch__c share= new Sharing_Batch__c(
                Relationship__c = recordID.Id,
                Reason__c = 'Owner Change - Relationship');
                newSharesToBatch.add(share);
                currentRelationshipIDsToBatch.add(recordID.Id);
            }
        }
        if (newSharesToBatch.size() > 0){
            try{
                insert newSharesToBatch;
            } catch (DMLException f) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error adding to Batch Share table'));
            }
        }
    }
    public void OnAfterNightlyClientInsert(List<Account> insertRecords){
        List<Sharing_Batch__c> newSharesToBatch = new List<Sharing_Batch__c>();
        List<Sharing_Batch__c> currentRelationshipsToBatch =  [Select Relationship__c From Sharing_Batch__c where batch_changed__c=null];
        Set<Id> currentRelationshipIDsToBatch = new Set<Id>();
        for (Sharing_Batch__c toShare : currentRelationshipsToBatch){
            currentRelationshipIDsToBatch.add(toShare.Relationship__c);
        }
        set<ID> ownerIDs = new set<Id>();
        for(Account relation : insertRecords) {
                ownerIDs.add(relation.ownerID);
        }
        Map<Id,user> userMap = new Map<Id,User>([SELECT ID, Profile.Name FROM User WHERE ID IN: ownerIDs]);
        
        for (Account recordID : insertRecords){
            // bypass record owned by Regions Migration profile
            if (userMap.get(recordId.ownerID).profile.name == 'Regions Migration') {               
                continue;
            }
            if (!currentRelationshipIDsToBatch.contains(recordID.Relationship__c)){
                Sharing_Batch__c share= new Sharing_Batch__c(
                Relationship__c = recordID.Relationship__c,
                Reason__c = 'New - Account');
                newSharesToBatch.add(share);
                currentRelationshipIDsToBatch.add(recordID.Relationship__c);
            }
        }
        if (newSharesToBatch.size() > 0){
            try{
                insert newSharesToBatch;
            } catch (DMLException f) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error adding to Batch Share table'));
            }
        }
    }
    public void OnAfterNightlyRelationshipInsert(List<Relationship__c> insertRecords){
        //Need to add the items to the nightly batch table.
        List<Sharing_Batch__c> newSharesToBatch = new List<Sharing_Batch__c>();
        List<Sharing_Batch__c> currentRelationshipsToBatch =  [Select Relationship__c From Sharing_Batch__c where batch_changed__c=null];
        Set<Id> currentRelationshipIDsToBatch = new Set<Id>();
        for (Sharing_Batch__c toShare : currentRelationshipsToBatch){
            currentRelationshipIDsToBatch.add(toShare.Relationship__c);
        }
        set<ID> ownerIDs = new set<Id>();
        for(Relationship__c relation : insertRecords) {
                ownerIDs.add(relation.ownerID);
        }
        Map<Id,user> userMap = new Map<Id,User>([SELECT ID, Profile.Name FROM User WHERE ID IN: ownerIDs]);
        
        for (Relationship__c recordID : insertRecords){
            // bypass record owned by Regions Migration profile
            if (userMap.get(recordId.ownerID).profile.name == 'Regions Migration') {               
                continue;
            }
            if (!currentRelationshipIDsToBatch.contains(recordID.Id)){
                //Add all of these to the table if they don't already exist
                Sharing_Batch__c share= new Sharing_Batch__c(
                Relationship__c = recordID.Id,
                Reason__c = 'New - Relationship');
                newSharesToBatch.add(share);
                currentRelationshipIDsToBatch.add(recordID.Id);
            }
        }
        if (newSharesToBatch.size() > 0){
            try{
                insert newSharesToBatch;
            } catch (DMLException f) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error adding to Batch Share table'));
            }
        }
    }

    public void OnAfterNightlyBankAccountDetailsInsert(List<Bank_Account_Details__c> insertRecords){
        List<Sharing_Batch__c> newSharesToBatch = new List<Sharing_Batch__c>();
        List<Sharing_Batch__c> currentRelationshipsToBatch =  [Select Relationship__c From Sharing_Batch__c where batch_changed__c=null];
        Set<Id> currentRelationshipIDsToBatch = new Set<Id>();
        for (Sharing_Batch__c toShare : currentRelationshipsToBatch){
            currentRelationshipIDsToBatch.add(toShare.Relationship__c);
        }
    
       set<ID> ownerIDs = new set<ID>();       
       for(Bank_Account_Details__c relation : insertRecords) {
                ownerIDs.add(relation.ownerID);
        }
        Map<Id,user> userMap = new Map<Id,User>([SELECT ID, Profile.Name FROM User WHERE ID IN: ownerIDs]);
                
        //Need to add the items to the nightly batch table.
        for (Bank_Account_Details__c recordID : insertRecords){
            // bypass record owned by Regions Migration profile
            if (userMap.get(recordId.ownerID).profile.name == 'Regions Migration') {
                continue;
            }
            if (!currentRelationshipIDsToBatch.contains(recordID.Relationship__c)){
                //Add all of these to the table if they don't already exist
                Sharing_Batch__c share= new Sharing_Batch__c(
                Relationship__c = recordID.Relationship__c,
                Reason__c = 'New - Bank Account Detail');
                newSharesToBatch.add(share);
                currentRelationshipIDsToBatch.add(recordID.Relationship__c);
            }
        }
        if (newSharesToBatch.size() > 0){
            try{
                insert newSharesToBatch;
            } catch (DMLException f) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error adding to Batch Share table'));
            }
        }
    }
}