//Edited to deploy under the oneview admin

@isTest (seeAllData=true)
private class testPerformanceAssessmentTrigger{
    static testMethod void PAtest() {
        test.startTest();
         
        // added due to deployment validation failure...to run the test as system administrator... Ephrem 04-21-2015
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator' limit 1]; 
        User u = new User(Alias = 'sysadmin', Email='sysadmin@regions.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='sysadminuser@regions.com');            
        //
        System.runAs(u){
            Performance_Assessment__c testPA =[select id, ownerid, createdbyid, credit_underwriter__c, assessment_status__c from Performance_Assessment__c limit 1]; 
            testPA.assessment_status__c = 'Final';
            update testPA;
            if (testPA.Assessment_Status__c == 'Final'){
                testPA.OwnerId = testPA.Credit_Underwriter__c;                          
            }
            Performance_Assessment__c  assertPA; 
            
            assertPA = [select id, ownerid, Credit_Underwriter__c, CreatedById, assessment_status__c from Performance_Assessment__c where id = :testPA.id];
            system.assertEquals(assertPA.assessment_status__c,'Final'); 
    
            //testPA.Assessment_Status__c = 'Draft';
            //update testPA;      
            if (testPA.Assessment_Status__c == 'Draft'){
                    testPA.OwnerId = testPA.createdbyid;            
            }        
            assertPA = [select id, ownerid, Credit_Underwriter__c, CreatedById, assessment_status__c from Performance_Assessment__c where id = :testPA.id];
            //system.assertEquals(assertPA.assessment_status__c,'Draft');
        } 
        test.stopTest();
    } 
}