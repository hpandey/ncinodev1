global class ScheduleEmailNotification Implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        SendEmailNotification();
    }
    public void SendEmailNotification(){
        date d = date.today().addDays(-1);
        List<Task> tasks7day = [SELECT Id, Subject, Status, CreatedDate, ActivityDate, Priority, OwnerId, WhatId  from task WHERE Status = 'Not Started' AND Subject='Bank Account up for Renewal' AND ActivityDate = :d];
        //new functionality to generate email notification to the Business Banker's Sales Manager if the Renewal process hasn't started in 7 days...Ephrem 05-2014 
        if(tasks7day.size() > 0){        
            List<Messaging.SingleEmailMessage> listMail = new List<Messaging.SingleEmailMessage>();
            Boolean donotSend = true;        
            for(Task t : tasks7day){
                String acctId = t.WhatId;
                String taskId = t.Id;
                String tOwnerId = t.OwnerId;
                Datetime activityDate = t.ActivityDate;
                String dueDate = activityDate.format('MM/dd/yyyy');       
                String fullUrl;
                String[] toAddresses;
                Boolean yesSM = false;
                Boolean yesSE = false; 
                String subject = 'Bank Account up for Renewal';                
                String description = 'The above task has not been dispositioned in the allowed time and is now past due. Please follow up with the Business Banker to expedite the 120 Day Renewal process.';
                Bank_Account_Details__c[] acc = [SELECT ID, Name FROM Bank_Account_Details__c WHERE Id = :acctId];
                User[] tUser = [SELECT ID, Name, Sales_Manager__c, Sales_Executive__c FROM User WHERE Id = :tOwnerId];
                User[] SMuser;
                User[] SEuser; 
                if(tUser[0].Sales_Manager__c != null){
                    SMuser = [SELECT ID, Name, Email FROM User WHERE Id = :tUser[0].Sales_Manager__c];
                    yesSM = true;
                }
                if(tUser[0].Sales_Executive__c != null){    
                    SEuser = [SELECT ID, Name, Email FROM User WHERE Id = :tUser[0].Sales_Executive__c];
                    yesSE = true;
                }                          
                if(taskId != null){
                    fullUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+taskId; 
                }
                if (fullUrl.startsWith('http:')){
                    fullUrL = fullUrL.replaceFirst('http:', 'https:');
                }
                //Create notification for Sales Manager, if no Sales Manager, create for Sales Executive...
                Messaging.SingleEmailMessage mailMessage = new Messaging.SingleEmailMessage();
                if(yesSM){
                    toAddresses = new String[]{SMuser[0].Email};
                    mailMessage.setToAddresses(toAddresses);
                    donotSend = false;
                }else if(yesSE){
                    toAddresses = new String[] {SEuser[0].Email};
                    mailMessage.setToAddresses(toAddresses); 
                    donotSend = false;
                }else{
                    donotSend = true;
                    break;
                }
                mailMessage.setSenderDisplayName('RegionsOneView Administrator'); 
                mailMessage.setSubject(subject);    
                mailMessage.setPlainTextBody('Subject:  '+subject+'<br/>' + 'Bank Account: '+ acc[0].Name + '<br/>'+ 'Business Banker: '+tUser[0].name +'<br/>'+ 'Task Due Date: '+dueDate +'<br/><br/>'+ 'Comments: '+description + '<br/><br/>'+'For more details, click the following link:<br/><br/>'+fullUrl);
                mailMessage.setHtmlBody('Subject:  '+subject+'<br/>' + 'Bank Account: '+ acc[0].Name + '<br/>'+ 'Account Owner: '+tUser[0].name +'<br/>' +'Task Due Date: '+dueDate+'<br/><br/>'+ 'Comments: '+description +'<br/><br/>'+'For more details, click the following link:<br/><br/>'+fullUrl);
                listMail.add(mailMessage);                             
            }
            if(!donotSend){
                    Messaging.sendEmail(listMail);
            }else{}
        }
    }
}