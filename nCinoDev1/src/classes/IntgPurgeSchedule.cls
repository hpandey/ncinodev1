global class IntgPurgeSchedule implements Schedulable{
   global void execute(SchedulableContext sc) {
      IntgPurgeBatch b = new IntgPurgeBatch(); 
      database.executebatch(b);
   }
}