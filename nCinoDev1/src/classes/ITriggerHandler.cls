/************************************************************************************
 Apex Class Name     : ITriggerHandler
 Version             : 1.0
 Created Date        : 26th Jan 2016
 Function            : Interface methods to be used in all Triggers
 Author              : Jag Valaiyapathy (jvalaiyapathy@deloitte.com)
 Credits             : Chris Aldridge (Original Author)
 Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------------                 
* Jag Valaiyapathy          1/26/2016                Original Version
*************************************************************************************/

public Interface ITriggerHandler{

    void BeforeInsert(List<sObject> newItems);
    void BeforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems);
    void BeforeDelete(Map<Id, sObject> oldItems);

    void AfterInsert(Map<Id, sObject> newItems);
    void AfterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems);
    void AfterDelete(Map<Id, sObject> oldItems);

    void AfterUndelete(Map<Id, sObject> oldItems);
}