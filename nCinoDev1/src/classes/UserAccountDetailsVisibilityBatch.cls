global class UserAccountDetailsVisibilityBatch implements Database.batchable<sObject>{

   public final String Query;
   
   public UserAccountDetailsVisibilityBatch(String q, List<Relationship__c> rlist){
       Query= q;
   }
   public UserAccountDetailsVisibilityBatch(ID currentOwnerID){
     //  Query = 'SELECT id, OwnerID ,(Select Id from Accounts__r),(Select Id from Relationship_Strategy_Plans__r),(Select Id from PDMs__r),(select Id from Restricted_Relationship_Information__r) from Relationship__c where OwnerId =' + '\'' + currentOwnerID+ '\''; 
        Query = 'SELECT id, OwnerID  from Relationship__c where OwnerId =' + '\'' + currentOwnerID+ '\'';     
   }

   global Database.QueryLocator start (Database.BatchableContext BC){
        return Database.getQueryLocator(query);
   }

   public void execute(Database.BatchableContext BC, List<Relationship__c> relationshipList){
        System.debug('We are about to start the batch for ... ' + relationshipList);
        AccountDetailsVisibilityUtil.recalculateUserSharing(relationshipList);
   }

   public void finish(Database.BatchableContext BC){
   }
   
   WebService static void ExecuteFinalizeTemp(ID currentOwnerID){
    System.debug('This is your current ownerID' + currentOwnerID);
      Id batchInstanceId = Database.executeBatch(new UserAccountDetailsVisibilityBatch(currentOwnerID), 20);
   }
}