public with sharing class goalTriggerHandler {
    public goalTriggerHandler(){
        //Handler
    }
    public void updateGoalOwner(List<Goal__c> newRecords){
        Set<Id> userIds = new Set<Id> ();
        for (Goal__c g : newRecords){
            userIds.add(g.User__c);
        }
        Map<ID,User> userMap = new Map<ID,User>(); 
        for(User usr : [SELECT id, isActive, ManagerId From User WHERE ID IN :userIds] ) {           
            userMap.put(usr.Id, usr);         
        }
    
        for(Goal__c thisGoal : newRecords){
            User thisUser = userMap.get(thisGoal.User__c);
            if (thisUser.IsActive) {
                System.debug('Active user ' + thisUser.Id);
                thisGoal.OwnerId = thisUser.Id;
            } else {
                System.debug('Inactive user ' + thisUser.Id + ' with managerid ' + thisUser.ManagerId);
                If (thisUser.ManagerId != null){
                    thisGoal.OwnerId = thisUser.ManagerId;
                }
            }
        }
    }
}