@isTest(SeeAllData=true)
private class TestoppProductSelectionCon3{

static testMethod void testLineItem(){

List <Trigger_Setting__c> ts = [Select Id, Name, isActive__c from Trigger_Setting__c Where Name='Opportunity'];
if (ts.size()>0){
    ts[0].isActive__c=false;
    update ts[0];
}
else {
    Trigger_Setting__c ts1 = new Trigger_Setting__c();
    ts1.Name='Opportunity';
    ts1.isActive__c=false;
    insert ts1;
}

Test.startTest();

Account acc = new Account(Name = 'Test Account');
insert acc;
Opportunity testOpty= new Opportunity(Name='Test Opty', CloseDate=date.today(), StageName = 'Prospecting', Account=acc);
insert testOpty;

Product2 prod = new Product2(Name = 'Test Product', Family='Capital Markets');
Pricebook2 pbook = new Pricebook2(Name = 'Test Pricebook', isActive=true);
insert prod;
insert pbook;
Pricebook2 sbook = [Select Id,Name from pricebook2 where isStandard=true];

PriceBookEntry sbe = new PriceBookEntry ( Pricebook2Id=sbook.id, Product2Id=prod.id,UnitPrice=1, IsActive=true);
insert sbe;
PriceBookEntry pbe = new PriceBookEntry ( Pricebook2Id=pbook.id, Product2Id=prod.id,UnitPrice=20, IsActive=true);
insert pbe;

OpportunityLineItem oppLine = new OpportunityLineItem( PricebookEntryId=pbe.Id, OpportunityId=testOpty.Id, 
                                                       Estimated_Revenue__c=20, 
                                                       unitPrice=1, quantity=1);
insert oppLine;
Product_Layout__c pl1 = new Product_Layout__c (Name='Loan', Display__c='Input', Field__c='UnitPrice', Sequence__c=1 );
Product_Layout__c pl2 = new Product_Layout__c (Name='Loan', Display__c='Output', Field__c='TotalPrice', Sequence__c=2 );
insert pl1;
insert pl2;

PriceBookEntry dbe = [select Id, Name,Product2.Family, Pricebook2Id, Product2Id, UnitPrice, IsActive
                      from PricebookEntry where Id = : pbe.Id];

ApexPages.StandardController sc = new ApexPages.StandardController(testOpty);

// create an instance of the controller
oppProductSelectionCon3 PageCon = new oppProductSelectionCon3(sc);

oppProductSelectionCon3.wrapProduct wrap = new oppProductSelectionCon3.wrapProduct();
wrap.priceBookProd=dbe;
wrap.oppLineItem=oppLine;

PageCon.wrapProductList.add(wrap);
PageCon.prodId = prod.Id;

//Test converage for the visualforce page
PageReference pageRef = Page.oppOverride3;
pageRef.getParameters().put('id', String.valueOf(Opportunity.Id));
Test.setCurrentPageReference(pageRef);

PageCon.getSection();
PageCon.saveProducts();
PageCon.quickSave();
PageCon.selectProduct();
PageCon.removeProduct();

Test.stopTest();
}

static testMethod void testLineItem2(){

Trigger_Setting__c ts = [Select Id, Name, isActive__c from Trigger_Setting__c Where Name='Opportunity'];
ts.isActive__c=false;
update ts;

Test.startTest();

Account acc = new Account(Name = 'Test Account');
insert acc;
Opportunity testOpty= new Opportunity(Name='Test Opty', CloseDate=date.today(), StageName = 'Prospecting', Account=acc);
insert testOpty;

Product2 prod = new Product2(Name = 'Test Product', Family='Capital Markets');
Pricebook2 pbook = new Pricebook2(Name = 'Test Pricebook', isActive=true);
insert prod;
insert pbook;
Pricebook2 sbook = [Select Id,Name from pricebook2 where isStandard=true];

PriceBookEntry sbe = new PriceBookEntry ( Pricebook2Id=sbook.id, Product2Id=prod.id,UnitPrice=1, IsActive=true);
insert sbe;
PriceBookEntry pbe = new PriceBookEntry ( Pricebook2Id=pbook.id, Product2Id=prod.id,UnitPrice=20, IsActive=true);
insert pbe;

Product_Layout__c pl1 = new Product_Layout__c (Name='Capital Markets', Display__c='Input', Field__c='UnitPrice', Sequence__c=1 );
Product_Layout__c pl2 = new Product_Layout__c (Name='Capital Markets', Display__c='Output', Field__c='TotalPrice', Sequence__c=2 );
insert pl1;
insert pl2;

OpportunityLineItem oppLine = new OpportunityLineItem( PricebookEntryId=pbe.Id, OpportunityId=testOpty.Id, 
                                                       Estimated_Revenue__c=20,
                                                       unitPrice=1, quantity=1);
insert oppLine;

ApexPages.StandardController sc = new ApexPages.StandardController(testOpty);

// create an instance of the controller
oppProductSelectionCon3 PageCon = new oppProductSelectionCon3(sc);

oppProductSelectionCon3.wrapProduct wrap = new oppProductSelectionCon3.wrapProduct();
wrap.priceBookProd=pbe;
wrap.oppLineItem=oppLine;

PageCon.wrapProductList.add(wrap);
PageCon.prodId = prod.Id;
PageCon.isMultiSelect=true;

//Test converage for the visualforce page
PageReference pageRef = Page.oppOverride3;
pageRef.getParameters().put('id', String.valueOf(Opportunity.Id));
Test.setCurrentPageReference(pageRef);

PageCon.getSection();
PageCon.selectProduct();
PageCon.cancel();
PageCon.removeProduct();

Test.stopTest();
}

static testMethod void testBlankOpty(){

Trigger_Setting__c ts = [Select Id, Name, isActive__c from Trigger_Setting__c Where Name='Opportunity'];
ts.isActive__c=false;
update ts;

Test.startTest();

Account acc = new Account(Name = 'Test Account');
insert acc;
Opportunity testOpty= new Opportunity(Name='Test Opty', CloseDate=date.today(), StageName = 'Prospecting', Account=acc);
insert testOpty;
Product2 prod = new Product2(Name = 'Test Product', Family='Capital Markets');
Pricebook2 pbook = new Pricebook2(Name = 'Test Pricebook', isActive=true);
insert prod;
insert pbook;
Pricebook2 sbook = [Select Id,Name from pricebook2 where isStandard=true];

PriceBookEntry sbe = new PriceBookEntry ( Pricebook2Id=sbook.id, Product2Id=prod.id,UnitPrice=1, IsActive=true);
insert sbe;
PriceBookEntry pbe = new PriceBookEntry ( Pricebook2Id=pbook.id, Product2Id=prod.id,UnitPrice=20, IsActive=true);
insert pbe;


ApexPages.StandardController sc = new ApexPages.StandardController(testOpty);

// create an instance of the controller
oppProductSelectionCon3 PageCon = new oppProductSelectionCon3(sc);

//Test converage for the visualforce page
PageReference pageRef = Page.oppOverride3;
pageRef.getParameters().put('id', String.valueOf(Opportunity.Id));
Test.setCurrentPageReference(pageRef);

PageCon.getSection();
oppProductSelectionCon3.returnMatchingRec('h','Name',pbook.id);
PageCon.quickSave();
PageCon.saveProducts();
PageCon.cancel();
PageCon.selectProduct();
PageCon.removeProduct();

Test.stopTest();
}

}