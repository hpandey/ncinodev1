/************************************************************************************
 Apex Class Name     : ExpressionEvaluator
 Version             : 1.0
 Created Date        : 4th March 2016
 Function            : Class implements infix to postfix algorimth and logic to evaluate a postfix expression
 Author              : Deloitte Consulting (Fayas Mansoor) 
*************************************************************************************/

public with sharing class ExpressionEvaluator {

	final static String STR_AND_OPERATOR 	= '*';
	final static String STR_OR_OPERATOR 	= '+';
	final static String STR_EXCLAMATION 	= '!';
	final static String STR_OPENING_PARANTHESIS 	= '(';
	final static String STR_CLOSING_PARANTHESIS 	= ')';
	final static Set<String> SET_OPERATORS = new Set<String>{ 	STR_AND_OPERATOR, 
																STR_OR_OPERATOR,
																STR_EXCLAMATION,
																STR_OPENING_PARANTHESIS,
																STR_CLOSING_PARANTHESIS
															};	

    List<String> listSymbolStack = new List<String>(); 
    
    private void push(String strNew){
    	
    	if(listSymbolStack==null){
    		listSymbolStack = new List<String>(); 
    	}

		listSymbolStack.add(strNew);	
    }

    private String pop(){
		String strLastElement ='';
    	
    	if(listSymbolStack!=null && !listSymbolStack.isEmpty()){
    		strLastElement = listSymbolStack.get(listSymbolStack.size()-1);
    		listSymbolStack.remove(listSymbolStack.size()-1);
    	}

    	return strLastElement;
    }

    private Integer getWeightage(String symbol){
       
    	return Integer.valueOf((symbol==STR_AND_OPERATOR)?'10'
    										:(symbol==STR_OR_OPERATOR)?'5':'1');
    }
    
    private Boolean isOperator(String strCharacter){

      return (SET_OPERATORS.contains(strCharacter)?true:false);
      
    }

    private List<String> convertInfixToPostFix(String strInfixExpression){
    	    
        List<String> listOutputStack = new List<String>(); 
        String str_Character;
        

        strInfixExpression = strInfixExpression.toLowerCase();
        strInfixExpression = strInfixExpression.replaceAll('and', '*');
        strInfixExpression = strInfixExpression.replaceAll('or', '+');
        
        listSymbolStack.add('!');
         
         
        for(Integer index=0;index<strInfixExpression.length();index++) {

			str_Character = (!String.isEmpty(strInfixExpression) && !String.isBlank(strInfixExpression)) 
									? strInfixExpression.substring(index, index+1):'';

			if(str_Character.trim()=='') {
				continue;
			}
			else {
			    
			    if(!isOperator(str_Character)) {
			       listOutputStack.add(str_Character); 
			    }

			    else {
			       
			       	if(str_Character=='('){

						push(str_Character);  
			       	}

			       	else if(str_Character==')') {

			           while(listSymbolStack.get(listSymbolStack.size()-1)!='(') 
			           { 
			               listOutputStack.add(pop()); 
			           }                    
			           pop(); 
			       	}

			       	else { 
			           	
			           	if(getWeightage(str_Character)>getWeightage(listSymbolStack.get(listSymbolStack.size()-1))) { 
			               push(str_Character); 
			           	}

			           	else { 
			               while(getWeightage(str_Character)<=getWeightage(listSymbolStack.get(listSymbolStack.size()-1))) { 
			                   listOutputStack.add(pop()); 
			               } 
			               push(str_Character); 
			           	}
			       	}
		    	}
			}

         
         }
         
         while(listSymbolStack.get(listSymbolStack.size()-1)!='!') { 
            listOutputStack.add(pop()); 
         } 
         
         return listOutputStack;
    }

   	private Boolean evaluatePostfixExpression(List<String> listPostfixExpression){
   		
   		Boolean boolResult = false;
   		
   		listSymbolStack.clear();
   		
   		for(String strExpressionElement : listPostfixExpression) {
   			 
			if(!isOperator(strExpressionElement)) {
   				push(strExpressionElement);
   			}

   			else {
   			 	
   			 	Integer intOperand_1 = Integer.valueOf(pop());
   			 	Integer intOperand_2 = Integer.valueOf(pop());
   			 	Integer intResult = 0;
   			 	
   			 	intResult = (strExpressionElement == STR_AND_OPERATOR)? intOperand_1*intOperand_2 : intOperand_1+intOperand_2;
   			 	intResult = intResult>1 ? 1 : intResult; 

   			 	push(String.valueOf(intResult));
   		 	}
   			 	
   		}  			 
   		
   		boolResult = Integer.valueOf(pop())==1?true:false;
   		
   		return boolResult;   		
   	}


   	public Boolean evaluationExpression(String strCriteriaLogic, Map<String, Boolean> mapCriteriaResult){

   		Boolean boolResult = false;

   		for(String str_Iterator : mapCriteriaResult.keySet()){
            System.debug('iterator is '+str_Iterator);
			strCriteriaLogic = strCriteriaLogic.trim();
   			strCriteriaLogic = strCriteriaLogic.replaceAll(' ','');
   			strCriteriaLogic = strCriteriaLogic.replace(str_Iterator, String.valueOf(mapCriteriaResult.get(str_Iterator)?1:0));
   		}

   		boolResult = evaluatePostfixExpression(convertInfixToPostFix(strCriteriaLogic));

   		return boolResult;
   	}

    
}