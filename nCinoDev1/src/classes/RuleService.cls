/******************************************************************************************************************
* Name            :  RuleService
* Author          :  Deloitte Consulting
* Description     :  This is a service class that fetches rules, rule criteria and fields to query based on rule criteria
*******************************************************************************************************************/
public with sharing class RuleService {

  /**** Class properties  ****/

  // parent query to get all rules and associated fields
  private String strRuleQuery = 'SELECT Id, Name,Criteria_Logic__c, Rule_Type__c, Object_Name__c {0} '
                                    +'FROM Rule__c WHERE Active__c = true' 
                                        +' AND Object_Name__c = {1}'
                                        +' AND Rule_Type__c = {2} {3}' ;
    
    // child query for all rule criteria related to rules 
    private String strRuleChildQuery_RuleCriteria =  '(SELECT Id,Criterion_Number__c, Criteria_Sequence__c,Currency_Value__c,Date_Value__c,'
                                                            +'Field_Name__c,Number__c,Operator__c,'
                                                            +'Text_Value__c,Rule__c, Field_Type__c,Name,Rule__r.Name '
                                                        +'FROM Rule_Criteria__r)';

    // stores all rules along with rule criteria 
    private List<Rule__c>   listActiveRulesWithCriteria;
    // stores all fields on sobject that needs to be queried
    private Set<String>   setFieldsToQuery;
    // defines the sobject for which this serivce provides support
    private String      strRelatedsObject;
    // defines the filter criteria for type of rules this service supports
    private String      strRuleType;
    // option member that adds filter filter on rules by explicitly mentioning rule names
    private Set<String>   setRuleNames;
    // stores all rule criteria related to rules 
    private List<Rule_Criterion__c> listAllRuleCriteria; 


    /**** End Class Properties ****/

    // Constructor
    public RuleService(String strObjectName, String strRuleType, Set<String> setRuleNames){

      if(strObjectName==null || String.isBlank(strObjectName) 
          || strRuleType==null || String.isBlank(strRuleType)) { 

        throw new RuleException('Invalid Parameters strObjectName  : '+strObjectName+ ' strRuleType   : ' 
            +strRuleType + ' setRuleNames  : '+setRuleNames);
      }
      else if(!Schema.getGlobalDescribe().containskey(strObjectName)){

        throw new RuleException('Invalid sObject Name --> '+strObjectName);
      }

      else {

        this.strRelatedsObject  = strObjectName;
        this.strRuleType    = strRuleType;
        this.setRuleNames     = setRuleNames;

        listActiveRulesWithCriteria = findActiveRulesWithCriteria(); // find all the active rules with criteria
        listAllRuleCriteria = findAllRuleCriteria(); // get all the rule criteria
        setFieldsToQuery = findFieldsToQuery();
        
      }

    }

    /*** Getter Methods ***/
    public List<Rule_Criterion__c> getlistAllRuleCriteria() {
      return listAllRuleCriteria;
    }

    public List<Rule__c> getlistActiveRulesWithCriteria() {
      return listActiveRulesWithCriteria;
    }
    
    public Set<String> getsetFieldsToQuery() {
      return setFieldsToQuery;
    }

    public String getstrRelatedsObject() {
      return strRelatedsObject;
    }

    public String getstrRuleType() {
      return strRuleType;
    }

    public Set<String> getsetRuleNames() {
      return setRuleNames;
    }
    /**** End of Getter Methods ****/

    /*Method Name    :  findActiveRulesWithCriteria
     *Description    :  Returns  all active rules that are related to 
                        a sobject and a rule type (and rule names) along with
                        rule criteria
     *Parameters     :  --none--
     *Returns        :  List<Rule__c>
    */
    private List<Rule__c> findActiveRulesWithCriteria(){

      List<Rule__c> listRules = new List<Rule__c>();
      List<String> listQueryReplacementStrings = new List<String>();
      String strWhereClauseForRuleName = ' AND name IN :setRuleNames';
      
      listQueryReplacementStrings.add(', '+strRuleChildQuery_RuleCriteria);
      listQueryReplacementStrings.add('\''+strRelatedsObject+'\'');
      listQueryReplacementStrings.add('\''+strRuleType+'\'');
      
      if(setRuleNames!=null && !setRuleNames.isEmpty()) { // for multiple rule names
        listQueryReplacementStrings.add(strWhereClauseForRuleName);
      }
      else {
        listQueryReplacementStrings.add('');
      }//end if else

      strRuleQuery = String.format(strRuleQuery, listQueryReplacementStrings);//generate dynamic query for Rule and Rule criteria object
      
      try {
        listRules = database.query(strRuleQuery); // fire the query
      }catch(QueryException ex) {
         utilityClass.throwExceptions(ex);
      }//end try catch

      return listRules;
    }//end method

    
    /*Method Name    :  findAllRuleCriteria
     *Description    :  Return a list of all rule criteria related to 
                        all rules prepared in method findActiveRulesWithCriteria
     *Parameters     :  --none--
     *Returns        :  List<Rule_Criterion__c>
    */
    private List<Rule_Criterion__c> findAllRuleCriteria() {
        List<Rule_Criterion__c> listRuleCriteria = new List<Rule_Criterion__c>();
        for(Rule__c objRule_iterator : listActiveRulesWithCriteria) { // iterate over the rule criteria records
          if(objRule_iterator.Rule_Criteria__r!=null && !objRule_iterator.Rule_Criteria__r.isEmpty()) {
              listRuleCriteria.addAll(objRule_iterator.Rule_Criteria__r);
          }//end if
        }//end for
    return listRuleCriteria;
  }//end method 

  /*Method Name    :  findFieldsToQuery
     *Description    :  Return a set of fields related to all rule criteria records
     *Parameters     :  --none--
     *Returns        :  Set<String>
    */
  private Set<String> findFieldsToQuery() { 

    Set<String> setFieldNames = new Set<String>();
      for(Rule_Criterion__c objRC_iterator : listAllRuleCriteria) {
          setFieldNames.add(objRC_iterator.Field_Name__c.trim());
        }
        return setFieldNames;
    }
    
    /*Method Name    :  queryRuleActions
     *Description    :  This method queries the RuleAction object based on the Rule Id
     *Parameters     :  mapRuleId_mapsObjectId_Result
     *Returns        :  List<Rule_Action__c>
    */
    public List<Rule_Action__c> queryRuleActions(Map<Id,Map<Id,boolean>> mapRuleId_mapsObjectId_Result,Set<Id> setRuleIds){
        
        List<Rule_Action__c> lstActions;
        try{
            if(setRuleIds!=null && !setRuleIds.isEmpty())
            {
               lstActions = [Select Id,Action_Type__c,Object_Name__c,Rule__c,Rule__r.Id,
                             (Select id,Boolean_Value__c,Currency_Value__c,Date_Value__c,Field_Name__c,
                             Number_Value__c,Text_Value__c,Rule_Action__r.Object_Name__c from Records_data__r) 
                             from Rule_Action__c where Rule__c IN: setRuleIds];    
            }//end if
        }catch(QueryException query){
            System.debug('Error is '+query.getMessage());
        }
        return lstActions;
    }//end method

    // Exception Class for Rule Service
    public class RuleException extends Exception { }
}