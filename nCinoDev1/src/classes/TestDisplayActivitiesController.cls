@isTest(SeeAllData=true)
public class TestDisplayActivitiesController {
  
    static testMethod void testConnectionSuccessNonNullRCIF(){
            Account testClient = [Select Id,RCIF__c From Account LIMIT 1];
    
            ApexPages.StandardController sc = new ApexPages.StandardController(testClient);
            
            // create an instance of the controller
            
            DisplayActivitiesController PageCon = new DisplayActivitiesController(sc);
            
            //Test converage for the visualforce page
            System.debug('Test coverage for the visualforce page...');
            PageReference pageRef = Page.DisplayActivities;
            pageRef.getParameters().put('id', String.valueOf(testClient.Id));
            
            Test.setCurrentPageReference(pageRef);
            Test.setMock(WebServiceMock.class, new WebServiceActMock());
                PageCon.listActivities();
    }
    
    static testMethod void testConnectionSuccessNullRCIF(){
            Account testClient = [Select Id,RCIF__c From Account LIMIT 1];
            
            ApexPages.StandardController sc = new ApexPages.StandardController(testClient);
            
            // create an instance of the controller
            
            DisplayActivitiesController PageCon = new DisplayActivitiesController(sc);
            
            //Test converage for the visualforce page
            System.debug('Test coverage for the visualforce page...');
            PageReference pageRef = Page.DisplayActivities;
            pageRef.getParameters().put('id', String.valueOf(testClient.Id));
            
            Test.setCurrentPageReference(pageRef);
            Test.setMock(WebServiceMock.class, new WebServiceNullMock());
                PageCon.listActivities();
    }
    
    static testMethod void testCalloutException(){

            Account testClient=new Account(Name='Test Client', RCIF__c='1234');
            insert testClient;

            ApexPages.StandardController sc = new ApexPages.StandardController(testClient);
            
            // create an instance of the controller
            
            DisplayActivitiesController PageCon = new DisplayActivitiesController(sc);
            
            //Test converage for the visualforce page
            System.debug('Test coverage for the visualforce page...');
            PageReference pageRef = Page.DisplayActivities;
            pageRef.getParameters().put('id', String.valueOf(testClient.Id));
            
            Test.setCurrentPageReference(pageRef);
            Test.setMock(WebServiceMock.class, new WebServiceActMock());
                PageCon.listActivities();

    }
    
    static testMethod void testGenericException(){
            Account testClient=new Account(Name='Test Client',RCIF__c='1234');
            insert testClient;

            ApexPages.StandardController sc = new ApexPages.StandardController(testClient);
            
            // create an instance of the controller
            
            DisplayActivitiesController PageCon = new DisplayActivitiesController(sc);
            
            //Test converage for the visualforce page
            PageReference pageRef = Page.DisplayActivities;
            pageRef.getParameters().put('id', String.valueOf(testClient.Id));
            
            Test.setCurrentPageReference(pageRef);

                PageCon.listActivities();
    
    }
    
    static testMethod void testNullRCIF(){
            Account testClient=new Account(Name='Test Client');
            insert testClient;

            ApexPages.StandardController sc = new ApexPages.StandardController(testClient);
            
            // create an instance of the controller
            
            DisplayActivitiesController PageCon = new DisplayActivitiesController(sc);
            
            //Test converage for the visualforce page
            PageReference pageRef = Page.DisplayActivities;
            pageRef.getParameters().put('id', String.valueOf(testClient.Id));
            
            Test.setCurrentPageReference(pageRef);

                PageCon.listActivities();
    
    }
}