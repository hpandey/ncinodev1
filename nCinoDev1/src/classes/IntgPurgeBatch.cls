Global Class IntgPurgeBatch implements
          Database.Batchable<SObject>, Database.Stateful
{    
String processName;
Integer duration;
DateTime endDate;

global IntgPurgeBatch()
{
    processName    = 'IntgPurgeBatch Job';
    
    //Integration_Error__c is a custom setting which defines the number of days to retain the error records before deletion.
    duration = -1 * (Integer)Integration_Error__c.getValues('Keep').Duration__c;
    endDate = System.Now().addDays(duration);
}

global Database.queryLocator 
                    start(Database.BatchableContext ctx){
        return Database.getQueryLocator([Select Id from IntegrationError__c where CreatedDate <= :endDate]);
    }


global void execute(Database.BatchableContext ctx, List<Sobject>
                        batch)
{

List<IntegrationError__c> errorBatch=(List<IntegrationError__c>)batch;
delete errorBatch;

}

global void finish(Database.BatchableContext ctx)
{
  
}

}