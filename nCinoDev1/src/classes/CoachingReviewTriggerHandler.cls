public class CoachingReviewTriggerHandler {
    public static boolean firstRun = true; 
    public CoachingReviewTriggerHandler(){
        //Handler
    }
    
    public void shareReview(List<Coaching_Review__c> NewRecords){
        // Create a new list of sharing objects for Job
        List<Coaching_Review__Share> crShrs = new List<Coaching_Review__Share>();
        List<Coaching_Review__Share> sharesToDelete = new List<Coaching_Review__Share>();
        
        // Declare variable for review sharing
        Coaching_Review__Share crShr;
        
        for(Coaching_Review__c cr : NewRecords){
            if (cr.Status__c != 'Draft'){
                // Instantiate the sharing object
                crShr = new Coaching_Review__Share();
                
                // Set the ID of record being shared
                crShr.ParentId = cr.Id;
                
                // Set the ID of user or group being granted access
                crShr.UserOrGroupId = cr.Relationship_Manager__c;
                
                // Set the access level
                crShr.AccessLevel = 'read';
                
                // Set the Apex sharing reason for call plan participant
                crShr.RowCause = Schema.Coaching_Review__Share.RowCause.Coaching_Review__c;
                
                // Add objects to list for insert
                crShrs.add(crShr);
            }else{
                LIST<Coaching_Review__Share> delShare = [SELECT Id FROM Coaching_Review__Share 
                                                                    WHERE ParentId = :cr.Id
                                                                    AND RowCause = :Schema.Coaching_Review__Share.RowCause.Coaching_Review__c];
                sharesToDelete.addAll(delShare);
            }
        }
        // Insert sharing records and capture save result 
        // The false parameter allows for partial processing if multiple records are passed 
        // into the operation 
        if(!crShrs.isEmpty()){
            Database.SaveResult[] lsr = Database.insert(crShrs,false);
        
            // Create counter
            Integer i=0;
            
            // Process the save results
            for(Database.SaveResult sr : lsr){
                if(!sr.isSuccess()){
                    // Get the first save result error
                    Database.Error err = sr.getErrors()[0];
                    
                    // Check if the error is related to a trivial access level
                    // Access levels equal or more permissive than the object's default 
                    // access level are not allowed. 
                    // These sharing records are not required and thus an insert exception is 
                    // acceptable. 
                    if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                                                   &&  err.getMessage().contains('AccessLevel'))){
                        // Throw an error when the error is not related to trivial access level.
                        trigger.newMap.get(crShrs[i].ParentId).
                          addError(
                           'Unable to grant sharing access due to following exception: ' + err.getMessage());
                    }
                }
                i++;
            }
        }
        if(!sharesToDelete.isEmpty()){
            Database.Delete(sharesToDelete, false);
        }
    }
}