public with sharing class OpportunityTriggerHandler {
    public boolean okToAdd {get; set;}
    public string usrProfileName {get; set;}
    public OpportunityTriggerHandler(){
        //Handler
    }
    // ensure only client team members can create opportunity
    public void OnBeforeInsert(List<Opportunity> insertRecords){
        okToAdd = false;
        List<AccountShare> AccountShares = [SELECT AccountAccessLevel, ContactAccessLevel, RowCause, UserOrGroupId FROM AccountShare where AccountId = :insertRecords[0].AccountId];
        Id currentuser = UserInfo.getUserID();
        // if user is sysadmin, allow pipeline entry...also added OneView Audit Profile and TM Auditor...Case 1710...Ephrem 02-2015,
        usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name;
        if (usrProfileName == 'System Administrator' || usrProfileName == 'OneView Audit Profile' || usrProfileName == 'TM Auditor') {
            okToAdd = true;
        } else {
            for (Integer i = 0; i <AccountShares.size(); i++){
                if (AccountShares[i].UserOrGroupId == currentuser){
                    okToAdd = true;
                }
            }
        }
        if (!okToAdd){ 
            insertRecords[0].addError('You are not part of the Client team. please contact the RM');
        }
    }
        
    // put the client owner onto the opportuntiy deal team
    public void OnAfterInsert (List<Opportunity> insertRecords){
        List<Account> client = [Select ownerid from Account where id = :insertRecords[0].AccountId];
        Id oppId = insertRecords[0].id;
        // I-85055 do not copy client owner automatically for lookback
        If (!insertRecords[0].lookback__c) {
            OpportunityTeamMember otm = new OpportunityTeamMember(OpportunityId=oppId,userId=client[0].OwnerId,TeamMemberRole='Client Owner',Deal_Credit__c=100, Incentive_Pay__c = 'No');
            insert otm;
        }
    }
    
    //--------------------------------------------------------------------------------------------------------------------------------------------
    //Require at least one RM Credit Only checked if Closed Won pipeline Stage is selected for C&I group...
    //Ephrem Tekle 03/14/2014
    //--------------------------------------------------------------------------------------------------------------------------------------------
    public void OnUpdate(List<Opportunity> updateRecords){
        string ErrMsg = '';
        integer RMcounter = 0;
        boolean yesClosedWon = true;        
        
        usrProfileName = [select u.Profile.Name from User u where u.id = :Userinfo.getUserId()].Profile.Name; //Ephrem...04-03-2015            
        List<OpportunityTeamMember> oppTM = [SELECT lob__c, user.c_i_type__c, RM_Credit_Only__c, opportunity.StageName, user.Name from opportunityteammember where opportunityid = :updateRecords[0].Id AND lob__c = 'Commercial and Industrial (C&I)'];
        for (OpportunityTeamMember otm : oppTM) {
            if(otm.opportunity.StageName == 'Closed Won')
            {
                if(otm.RM_Credit_Only__c == true){RMcounter++;}   
            }else{
                yesClosedWon = false;
            }            
        }   
         
        if(yesClosedWon){
            try{
                if(oppTM.size() > 1 && usrProfileName != 'System Administrator'){ //modified... Ephrem 04/03/2015  
                    if(RMCounter == 0){                   
                       ErrMsg = 'If more than one Commercial user is added, one must have RM Credit Only field checked. See RM Credit Only help text for details.';
                       updateRecords[0].addError(ErrMsg);
                    }else if(RMCounter > 1){     //Case 00001339...Ephrem 06-2014
                       ErrMsg='The RM Credit Only field can only be checked for one user.';
                       updateRecords[0].addError(ErrMsg); 
                    }
                    else{}   
                }else if(oppTM.size() == 1 && usrProfileName != 'System Administrator'){ //modified... Ephrem 04/03/2015  
                    if(RMCounter == 1){                   
                       ErrMsg = 'The RM Credit Only field cannot be checked if there is only one Commercial user on the pipeline team.';
                       updateRecords[0].addError(ErrMsg);
                    }
                    else{}  
               }
               else {}
            }catch(DmlException e) {
                   System.debug('The following exception has occurred: ' + e.getMessage());
                   ErrMsg = 'A system error has occured. Please contact OneView Support.';                    
            }    
        }
        
        //---------------------------------------------------------------------------------------------------------
        //Require a team member if the pipeline stage is Closed Won or Closed Lost...
        //The only exception is Record Type of Lost Loan Business... 
        //Case 1527/8... Ephrem Tekle 08-2014 
        //---------------------------------------------------------------------------------------------------------
        Opportunity[] opp = [SELECT id, StageName, RecordTypeId from opportunity where id = :updateRecords[0].Id limit 1];
        OpportunityTeamMember[] TMopp = [SELECT lob__c, opportunity.StageName from opportunityteammember where opportunityid = :updateRecords[0].Id];
        RecordType oppRecordType = [select Id, Name from recordtype where Name='Lost Existing Client Loan' limit 1];
        //added for case #1446...Ephrem...06-2015
        OpportunityLineItem[] oli = [Select id, opportunityid, Product_Name__c From OpportunityLineItem where opportunityid = :updateRecords[0].Id];        
        //       
        try{
            if(opp[0].RecordTypeId != oppRecordType.Id)
            {  
                if(TMopp.size() == 0 && (opp[0].StageName == 'Closed Won' || opp[0].StageName == 'Closed Lost')){
                    if((usrProfileName == 'System Administrator') || (usrProfileName ==  'Regions Power User') || (usrProfileName ==  'PWM Power User') 
                      || (usrProfileName == 'CT Power User') || (usrProfileName == 'Incentive Administrator')){
                    }else{
                        ErrMsg= 'Please add a Pipeline Team member before closing the Pipeline.';
                        updateRecords[0].addError(ErrMsg);               
                    }
                }
            }else{}
            
            //-----------------------------------------------------------------------------------------------------------
            //Disallow a user to move a pipeline to "Closed Won" status when TM Opportunity is the product type
            //iterates through multiple products of a TM pipeline... 
            //Case 1446... Ephrem 06-2015
            //------------------------------------------------------------------------------------------------------------
            if(oli.size() > 0){
                for(Integer i=0; i<oli.size(); i++){
                    if(opp[0].StageName == 'Closed Won' && oli[i].Product_Name__c == 'TM Opportunity'){
                        ErrMsg = 'TM Opportunity is not a valid TM product. Please select the actual product type sold to the client.';
                        updateRecords[0].addError(ErrMsg);
                    }
                }     
            }   
        }catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            ErrMsg = 'A system error has occured. Please contact OneView Support.';
            updateRecords[0].addError(ErrMsg);                    
        }         
    }
}