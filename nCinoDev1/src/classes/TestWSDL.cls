@isTest
private class TestWSDL {

  static testMethod void myUnitTest() {
    // Setup test data 
    RegionsConnectivity__c cs = new RegionsConnectivity__c();
    cs.Name = 'AccountBalance_WS';
    cs.RegionsURL__c='https://sfdc.regionstest.com/SalesForce/SalesForceAccountProfile.svc';
    cs.UserID__c='SalesForce001';
    cs.Password__c='D0g5h1tTac0';
    cs.ApplicationCode__c='DA';
    cs.OperatorID__c='MQCSMDN';
    cs.MessageForError__c='Please contact customer service at (888) 888-8888.';
    cs.MessageForRejection__c='Please contact customer service at (888) 888-8888.';
    insert cs;
    
    RegionsConnectivity__c cs2 = new RegionsConnectivity__c();
    cs2.Name = 'Activities_WS';
    cs2.RegionsURL__c='https://sfdc.regionstest.com/SalesForce/SalesForceAccountProfile.svc';
    cs2.UserID__c='SalesForce001';
    cs2.Password__c='D0g5h1tTac0';
    cs2.ApplicationCode__c='DA';
    cs2.OperatorID__c='MQCSMDN';
    cs2.MessageForError__c='Please contact customer service at (888) 888-8888.';
    cs2.MessageForRejection__c='Please contact customer service at (888) 888-8888.';
    insert cs2;
    
    AccountBalance a= new AccountBalance();
    AccountBalance.BasicHttpBinding_ISalesForceAccountProfile b = new AccountBalance.BasicHttpBinding_ISalesForceAccountProfile();
    AccountBalance.FetchSalesForceAccountProfile_element c = new AccountBalance.FetchSalesForceAccountProfile_element();
    AccountBalance.FetchSalesForceAccountProfileResponse_element d = new AccountBalance.FetchSalesForceAccountProfileResponse_element();
    
    AccountBalanceArrays e = new AccountBalanceArrays();
    AccountBalanceArrays.KeyValueOfstringstring_element f = new AccountBalanceArrays.KeyValueOfstringstring_element();
    AccountBalanceArrays.ArrayOfKeyValueOfstringstring g = new AccountBalanceArrays.ArrayOfKeyValueOfstringstring();
    
    AccountBalanceImports h = new AccountBalanceImports();
    
    GetActivities_Response i = new GetActivities_Response();
    GetActivities_Response.ArrayOfEventDetailObject j = new GetActivities_Response.ArrayOfEventDetailObject();
    GetActivities_Response.EventDetailObject k = new GetActivities_Response.EventDetailObject();
    
    GetActivities_Stub l = new GetActivities_Stub();
    GetActivities_Stub.getEvents_element m = new GetActivities_Stub.getEvents_element();
    GetActivities_Stub.BasicHttpBinding_IOrbit360EventsSF n = new GetActivities_Stub.BasicHttpBinding_IOrbit360EventsSF();
    GetActivities_Stub.getEventsResponse_element o = new GetActivities_Stub.getEventsResponse_element();
    
    }
}