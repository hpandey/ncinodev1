/**********************************************************************************************
 * Name         : AsynchronousQueueService
 * Author       : Deloitte Consulting
 * Description  : Helper Class for AsynchronousProcessLogTriggerHandler. This Class will have all the logic.
 *********************************************************************************************/

public with sharing class AsynchronousQueueService {

    /***
    * Method name    : InitiateAsyncProcessMethod
    * Description    : Method used for initiate Async process.
    * Return Type    : void
    * Parameter      : Map<Id,sObject> newItems
    */

    public void initiateAsyncProcessMethod(Map<Id, sObject> newItems) {

        Set<Id> setAllRecordIds                             = new Set<Id>();
        List<Asynchronous_Queue__c> lstAsyncQueueToUpdated  = new List<Asynchronous_Queue__c>();
        Id jobID;
        
        for(Asynchronous_Queue__c objAsyncQueueIterator : (List<Asynchronous_Queue__c>) newItems.values()) {

            if(objAsyncQueueIterator.isImmediate__c) {

                try{
                    
                    jobID = System.enqueueJob( new AsyncJobExecution(objAsyncQueueIterator));
                    lstAsyncQueueToUpdated.add(new Asynchronous_Queue__c(
                                                        id = objAsyncQueueIterator.id,
                                                        Job_Id__c = jobID,
                                                        Enqueued_Date_Time__c = system.now()
                                                        ));
                }
                
                catch(AsyncException ex){

                    lstAsyncQueueToUpdated.add(new Asynchronous_Queue__c(
                                                    id = objAsyncQueueIterator.id,
                                                    Request_Status__c = Label.Request_Type_Completed_with_Errors,
                                                    Error_Message__c = Label.Error_Message_Queue_Job + ex.getMessage(),
                                                    Error_Type__c = ex.getTypeName()
                                                    ));
                }
            
            }
    
            if(!lstAsyncQueueToUpdated.isEmpty()){

                List<Database.SaveResult> listSaveResult = Database.update(lstAsyncQueueToUpdated, false);
                // Its unlikely to cause error as there are no validations on the object or after update trigger logic. 
                UtilityClass.processSaveResult(listSaveResult); 
            }      

        }

        
    }


    public void validateImmediateCallOutProcesses(List<Asynchronous_Queue__c> listNewAsyncQueueRecords){
        
        Integer int_ImmediateCounter = 0;

        for(Asynchronous_Queue__c objAsynchQueue_Iterator : listNewAsyncQueueRecords) {

            if(objAsynchQueue_Iterator.isImmediate__c) {

                int_ImmediateCounter++;

                if(int_ImmediateCounter > EnvironmentVariables.QueueJobTransactionLimit) {

                    objAsynchQueue_Iterator.Request_Type__c = objAsynchQueue_Iterator.isCallout__c 
                                                                ? Label.Request_Type_Batch_With_Callout 
                                                                    : Label.Request_Type_Batch_No_Callout;
                }
            }
        }
    }
}