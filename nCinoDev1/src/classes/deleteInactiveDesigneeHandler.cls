//------------------------------------------------------------------------------------------------------------------------------------------
//Case 2266 - removes a user from the Designee object when a user's record is modified to an Inactive status...
//This class is instantiated by the InactiveDesigneeTrigger after an update of a user record...
//The class utilizes the @future method to prevent Apex errors since it involves 2 DML operations...
//The trigger calling this class fires whenever any user's status is modified to an inactive user...
//It checks if an inactive user is in a Designee object and if found, it will be removed from Designee object...
//Ephrem Tekle - 08-2015 
//------------------------------------------------------------------------------------------------------------------------------------------

global class deleteInactiveDesigneeHandler {
    @future
    public static void processInactiveDesignee()
    { 
        List<ID>InactiveRecordIds = new List<ID>();
        List <User> inactiveU = [SELECT Id FROM User WHERE isActive = false];  
        for(User u: inactiveU){
            InactiveRecordIds.add(u.Id);
        }
        
        List<Designee__c> dUser = new List<Designee__c>();  
        List<Designee__c> dList = [SELECT Id, OwnerId, Designee__c from designee__c WHERE Designee__c in : InactiveRecordIds OR Designee__c = null];
        if(dList.size() > 0){
            for(integer i=0; i<dList.size(); i++){
                dUser.add(dList[i]);
            }
            try{
                delete dUser;
            }catch(DMLException e){
                System.debug('DML Exception: '+e.getMessage());
            }
        }else{}
    }
}