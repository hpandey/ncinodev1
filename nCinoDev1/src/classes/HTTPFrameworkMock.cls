/************************************************************************************
 Apex Class Name     : HTTPFrameworkMock
 Version             : 1.0
 Created Date        : 12th Feb 2016
 Function            : Mock Callout used in Test Class for HTTPFramework.
 Author              : Pranil Thubrikar 
 Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar         2/12/2016                Original Version
*************************************************************************************/
@isTest
public Class HTTPFrameworkMock implements HttpCalloutMock{
    
    public boolean mockException;
    
    public HTTPFrameworkMock(Boolean isException){
        if(isException)
            mockException = true;
        else if(!isException)
            mockException = false;
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        if(!mockException){
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"study":{"mockKey1":"mockValue1","mockKey2":"mockValue1","mockKey3":"mockValue1"}}');
            res.setStatusCode(200);
            return res;
        }
        else if(mockException){
            CalloutException e = (CalloutException)CalloutException.class.newInstance();
            e.setMessage('Callout Exception');
            throw e;
        }
        return null;
    }
}