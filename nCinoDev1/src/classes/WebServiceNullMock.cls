@isTest
global class WebServiceNullMock implements WebServiceMock {global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           
    GetActivities_Stub.getEventsResponse_element respElement =  
        new GetActivities_Stub.getEventsResponse_element();
    
    GetActivities_Response.EventDetailObject[] eventDetail = null;
    
    GetActivities_Response.ArrayOfEventDetailObject arrayEvent = new GetActivities_Response.ArrayOfEventDetailObject();
    arrayEvent.EventDetailObject = eventDetail;
    
    respElement.getEventsResult=arrayEvent;
      
    response.put('response_x', respElement);        
        
   }
}