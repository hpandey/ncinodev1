/**********************************************************************************************
 * Name         : AsynchronousQueueBatchClass
 * Author       : Deloitte Consulting
 * Description  : Batch class for invoking apex job for Async Process log records .
 *********************************************************************************************/
 
/************************************************************************************
Code Review Comments - Aveleena
1) Use the commenting format for Class and Method as mentioned in the coding standards
2) Add Try catch at line 30 
New Comments
3) What is the purpose of mapOfJobIdById?  -- Removed
4) Please add inline comments whereever needed
5) What has been assigned to objSobject? as i see line 80 commented -- This will be part of back update.
6) I think we can avoid creating lstAsynchronousQueueToBeUpdated List and use lstAsynchronousQueue for update -- Done
7) Add try catch around Query and DML statements -- Handled through utility class method.
**************************************************************************************/

global class AsynchronousQueueBatchClass implements Database.Batchable<sObject>, Database.AllowsCallouts {

    List<Asynchronous_Queue__c> listAllAsynchronousQueue;
    LogFramework objLogger;
    /***
    * Method name    : start
    * Description    : Gathers requied records for batch.
    * Return Type    : Database.QueryLocator
    * Parameter      : Database.BatchableContext BC
    */

    global Database.QueryLocator start(Database.BatchableContext BC){
        String strFailStatus = Label.Status_Complete_Fail;
        String strCompleteStatus = Label.Status_Complete_Success;
        String strQuery = 'select id , Completed_Date_Time__c , Enqueued_Date_Time__c , Error_Message__c , Error_Type__c ,'+
                                ' Functional_Class__c , isCallout__c , isImmediate__c , Job_Id__c , Record_Ids__c , Request_Status__c ,'+
                                ' Request_Type__c from Asynchronous_Queue__c where (isImmediate__c = false AND'+
                                ' Request_Status__c !=: strCompleteStatus) OR Request_Status__c =:strFailStatus';
        listAllAsynchronousQueue = new List<Asynchronous_Queue__c>();
        objLogger = LogFramework.getLogger();
        system.debug('strQuery@@'+strQuery);
        return Database.getQueryLocator(strQuery);
    }
    
    /***
    * Method name    : execute
    * Description    : Contains the logic for batch class.
    * Return Type    : void
    * Parameter      : Database.BatchableContext BC, List<Asynchronous_Queue__c> lstAsynchronousQueue
    */

    global void execute(Database.BatchableContext BC, List<Asynchronous_Queue__c> lstAsynchronousQueue) {
        
        system.debug('lstAsynchronousQueue@@'+lstAsynchronousQueue);
        objLogger = LogFramework.getLogger();
        id jobID = BC.getJobId();
        Map<String,Set<Id>> mapFunctionalClassName_sObjectIds = new Map<String,Set<Id>>();

        for(Asynchronous_Queue__c objAsyncProcess : lstAsynchronousQueue) {
            Set<Id> setsObjectIds = mapFunctionalClassName_sObjectIds.containsKey(objAsyncProcess.Functional_Class__c)
                                    ? mapFunctionalClassName_sObjectIds.get(objAsyncProcess.Functional_Class__c)
                                    : new Set<Id>();
            if(String.isNotBlank(objAsyncProcess.Record_Ids__c)){
                setsObjectIds.addAll((List<Id>)objAsyncProcess.Record_Ids__c.split(';'));
            }
            mapFunctionalClassName_sObjectIds.put(objAsyncProcess.Functional_Class__c,setsObjectIds);
        }
        
        for(Asynchronous_Queue__c objAsyncProcess : lstAsynchronousQueue) {
            HttpResponse response;
            sObject objSobject;
            Type objType = Type.forName(objAsyncProcess.Functional_Class__c);
            OutboundIntegrationInterface objOutboundIntg = (OutboundIntegrationInterface)objType.newInstance();
            objOutboundIntg.executeIntegration(objAsyncProcess);
            response = objOutboundIntg.getHTTPResponse();
            if(response.getStatusCode() == environmentVariables.STATUS_200 || response.getStatusCode() == environmentVariables.STATUS_201) {
                objAsyncProcess.Request_Status__c = Label.Status_Complete_Success;
            }
            else {
                objAsyncProcess.Request_Status__c = Label.Status_Complete_Fail;
            }
            //Storing Completed time of API Callout.
            objAsyncProcess.Completed_Date_Time__c = system.now();
            objAsyncProcess.Job_Id__c = jobID;
        }

        update lstAsynchronousQueue;
        // create / update response into objects 
        objLogger.commitLogs();
    }
    
    /***
    * Method name    : finish
    * Description    : Contains action needs to perform after completion of batch.
    * Return Type    : void
    * Parameter      : Database.BatchableContext BC
    */

    global void finish(Database.BatchableContext BC){
        
        
        
    }
}