@isTest(SeeAllData=true)
private class TestCoreSegRPT {

  static testMethod void myUnitTest1() {
    
		CoreSegRpt PageCon = new CoreSegRpt();
		PageReference pageRef = Page.CoreSegRpt;
		Test.setCurrentPageReference(pageRef);
		pageRef.getParameters().put('reportview', '2');
		PageCon.View();

		pageCon.onSearch = false;
		pageCon.ReportView = '1';
		PageCon.getReportRows();
		pageCon.onSearch = false;
		pageCon.ReportView = '2';
		PageCon.getReportRows();  
		pageCon.onSearch = false;
		pageCon.ReportView = '3';
		PageCon.getReportRows(); 
		pageCon.onSearch = false;
		pageCon.ReportView = '4';
		PageCon.getReportRows();  
 
	    	
	    	
    }
    static testMethod void myUnitTest2() {

		CoreSegRpt PageCon = new CoreSegRpt();
		PageReference pageRef = Page.CoreSegRpt;
		Test.setCurrentPageReference(pageRef);
		pageRef.getParameters().put('reportview', '3');
		PageCon.View();
		
		pageRef.getParameters().put('reportview', '3');
		PageCon.ReportSelected();
		pageRef.getParameters().put('reportview', '3');
		pageRef.getParameters().put('StartDate', '1/1/2015');				
		pageRef.getParameters().put('EndDate', '12/31/2015');		
		pageRef.getParameters().put('ReasonLost', 'Declined by bank');		
		PageCon.View();
		
		pageCon.onSearch = true;
		pageCon.ReportView = '1';
		pageCon.StartDate = '1/1/2014';
		pageCon.EndDate = '12/31/2014';
		PageCon.getReportRows();
		pageCon.buildURL();
		
		pageCon.onSearch = true;
		pageCon.ReportView = '2';
		pageCon.StartDate = '5/24/2015';
		pageCon.EndDate = '9/21/2015';
		pageCon.Probability = '25';
		PageCon.getReportRows();
						
		pageCon.onSearch = true;
		pageCon.ReportView = '3';
		pageCon.StartDate = '1/1/2015';
		pageCon.EndDate = '12/31/2015';
		pageCon.ReasonLost = 'Declined by bank';
		PageCon.getReportRows();
		pageCon.buildURL();
		
		pageCon.onSearch = true;
		pageCon.ReportView = '4';
		pageCon.StartDate = '1/1/2015';
		pageCon.EndDate = '12/31/2015';
		pageCon.ReasonLost = 'Declined by bank';
		PageCon.getReportRows();
		pageCon.buildURL();		

		
		pageCon.ReportView = '2';
		pageCon.StartDate = '5/24/2015';
		pageCon.EndDate = '9/21/2015';
		pageCon.Probability = '25';
		pageCon.buildURL();  	
    }
    static testMethod void myUnitTest3() {

		CoreSegRpt PageCon = new CoreSegRpt();
		PageReference pageRef = Page.CoreSegRptPDF;
		pageRef.getParameters().put('reportview', '2');
		pageRef.getParameters().put('StartDate', '5/24/2015');
		pageRef.getParameters().put('EndDate', '9/21/2015');
		pageRef.getParameters().put('Probability', '25');
		Test.setCurrentPageReference(pageRef);
        /*pageRef.getParameters().put('reportview', '2');
		pageCon.ReportView = '1';
		pageCon.StartDate = '5/24/2015';
		pageCon.EndDate = '9/21/2015';
		//pageCon.Probability = '25';
		Component.Apex.PageBlockSection pblk = PageCon.getSection();
		System.assert(pblk!=null);
		//trows[] report = PageCon.getReportRows();*/
		
    }   
        
}