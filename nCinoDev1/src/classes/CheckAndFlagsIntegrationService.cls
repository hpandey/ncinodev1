/**********************************************************************************************
 * Name         : CheckAndFlagsIntegrationService
 * Author       : Deloitte Consulting
 * Description  : This service class implments the logic to generate request body, invoke call out 
 *                and parse the response for Checks and Flags Integration 
 *********************************************************************************************/

public with sharing class CheckAndFlagsIntegrationService implements OutboundIntegrationInterface{

    String strRequest;
    String strResponse; 
    HttpResponse resposne;
    HTTPFramework objHTTPFramework; 
    
    /***
    * Method name    : CreditBureauIntegrationService
    * Description    : Constructor of class.
    * Return Type    : None
    * Parameter      : None
    */
    public CheckAndFlagsIntegrationService() {
        
        objHTTPFramework    = new HTTPFramework();
        resposne            = new HttpResponse();
        strResponse         = '';
        strRequest          = '';
    }
    
    public void executeIntegration(Asynchronous_Queue__c objAsyncQueueRecord){
        
        Asynchronous_Queue__c asyncQueueRecord_toUpdate = new Asynchronous_Queue__c(id = objAsyncQueueRecord.id);
        strRequest = getRequestBody(objAsyncQueueRecord.Record_Ids__c);



        try{
            system.debug('#### Is Batch? --> ' + system.isBatch());
            objHTTPFramework.setEndpoint('http://fake-response.appspot.com/?sleep=16');
            objHTTPFramework.setMethod('GET');
            objHTTPFramework.setTimeout(100000);
            //objHTTPFramework.enableLogging();
            objHTTPFramework.performCallout();
            asyncQueueRecord_toUpdate.Request_Status__c = 'Completed - Success';
        }
        catch(exception ex){
            asyncQueueRecord_toUpdate.Request_Status__c = 'Completed - Fail';   
            asyncQueueRecord_toUpdate.Error_Type__c = 'System Exception : '+ ex.getTypeName();
            asyncQueueRecord_toUpdate.Error_Message__c = ex.getMessage();
            if(system.isBatch()){
                throw ex;
            }
            else {
                LogFramework.getLogger().createExceptionLogs(ex);
            }
            
        }

        finally {
            if(!System.isBatch()){
                if(asyncQueueRecord_toUpdate!=null)
                    update asyncQueueRecord_toUpdate;
                LogFramework.getLogger().commitLogs();
            }
        }

        
        // implement logic to query data from queue record 
        // invoke other methods within this class like getrequest, docallout, parseresponse etc.
    }

    /***
    * Method name    : getRequestBody
    * Description    : method used for generating request body as a string for API Callout.
    * Return Type    : String
    * Parameter      : sObject sobjRecord 
    */
    
    public String getRequestBody(String strIds) {
        String strRequestBody = '';        
        Set<String> set_AccountIds = new Set<String>();
        List<Account> list_AccountRecords = new List<Account>();

        if(String.isBlank(strIds)){
            CheckAndFlagsIntegrationServiceException objException = new CheckAndFlagsIntegrationServiceException('Invalid queue entry - there is no'
                                                                                                                    + ' sobject record associated to'
                                                                                                                    + ' process the request ');
            throw objException;
        }
        
        else {
            set_AccountIds.addAll(strIds.split(','));
            if(!set_AccountIds.isEmpty()) {
                list_AccountRecords = fetchAccountData(set_AccountIds);
                strRequestBody = generateRequestFrom_sobjectRecords(list_AccountRecords);
            }
        }

        return strRequestBody;
    }

    
    public List<Account> fetchAccountData(Set<String> set_AccountIds) {
        
        // not handling exception here, but client invoking the this service will be responsible for handling the exception
        List<Account> list_AccountRecord = [SELECT id FROM Account WHERE Id in :set_AccountIds];

        return list_AccountRecord;
    }


    public String generateRequestFrom_sobjectRecords(List<Account> listAccountRecords){
        String strRequestBody = '';

        // logic to generate JSON body 


        return strRequestBody;

    }

    /***
    * Method name    : doCallOut
    * Description    : method used for performing API Callouts.
    * Return Type    : HttpResponse
    * Parameter      : None 
    */
    public HttpResponse doCallOut() {
        
        //performCallout
        system.debug('I am In Loan'+strRequest);
        // Fetching Data of Async Custom Setting.
        Map<String , Async_Process_Log_Setting__c> mapAsyncCustomSetting = Async_Process_Log_Setting__c.getAll();
        system.debug('mapAsyncCustomSetting'+mapAsyncCustomSetting);
        if(mapAsyncCustomSetting.ContainsKey(Label.CheckAndFlags_Integration_Service)) {
            Async_Process_Log_Setting__c objAsyncCustomSetting = mapAsyncCustomSetting.get(Label.CheckAndFlags_Integration_Service);
            system.debug('strRequest$$'+strRequest+objAsyncCustomSetting);
            // Initialising Http framework
            objHTTPFramework = new HTTPFramework();
            objHTTPFramework.setEndpoint(objAsyncCustomSetting.End_Point__c);
            objHTTPFramework.setMethod(objAsyncCustomSetting.Method_Type__c);
            objHTTPFramework.setTimeout(integer.valueof(objAsyncCustomSetting.TimeOut_Duration__c));
            objHTTPFramework.setBody(strRequest);
            
            // Performing API Callout
            resposne = objHTTPFramework.performCallout();
            system.debug('resposne$$'+resposne);
        }
        return resposne;
    }
    
    /***
    * Method name    : getResponse
    * Description    : method used for generating response body as a string.
    * Return Type    : String
    * Parameter      : None 
    */

    public String getResponse() {
        //String strResponseBody = '';

        // Converting Response body to string.
        strResponse = resposne.getbody();
        return strResponse;
    }
    
    /***
    * Method name    : finish
    * Description    : method used for performing back-update to source object.
    * Return Type    : sObject
    * Parameter      : sObject sobjRecord
     */

    public sObject finish() {
        /*system.debug('I am in finish');
        if(sobjRecord != NUll) {
            Account objAccount = (Account)sobjRecord;
            string CreditScore;
            //String requestBodyData = RestContext.request.requestBody.toString();
            //resposne
            
            Map<String, Object> mapRequestData = (Map<String, Object>) JSON.deserializeUntyped(strResponse);
            system.debug('mapRequestData@@'+mapRequestData);
            CreditScore = string.valueof(mapRequestData.get('Credit Score'));
            objAccount.Description = CreditScore;
            return objAccount;
        }*/

        return null;
    }

    

    public HttpResponse getHTTPResponse(){
        return objHTTPFramework.getResponse();
    }
    public HttpRequest getHTTPRequest(){
        return objHTTPFramework.getRequest();
    }  

    public class CheckAndFlagsIntegrationServiceException extends Exception { }  
}