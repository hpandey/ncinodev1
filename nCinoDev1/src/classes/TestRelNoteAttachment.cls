/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRelNoteAttachment {

    static testMethod void myUnitTest() {

    
            Relationship__c Relationship = new Relationship__c(Name='Test Relationship');
            insert Relationship;      
            // Access the account that was just created. 
    
            Relationship__c insertedRel = [SELECT Id,Name FROM Relationship__c WHERE Name='Test Relationship'];
            
        //Create Notes
		note n = new note();
		n.parentId=insertedRel.id; 
		n.body='inserting note from code'; // body of the note. this should be populated from the notes field that you have shown in the screenshot
		n.title='New Note from code'; // the title of the note
		n.isPrivate=false;  // if private it will be visible only for the owner and any user with modify all permission
		insert n; //insert the record          

        //Create Notes
		note n1 = new note();
		n1.parentId=insertedRel.id; 
		n1.body='inserting note from code'; // body of the note. this should be populated from the notes field that you have shown in the screenshot
		n1.title='New Note from code'; // the title of the note
		n1.isPrivate=false;  // if private it will be visible only for the owner and any user with modify all permission
		insert n1; //insert the record          

        //Create Notes
		note n2 = new note();
		n2.parentId=insertedRel.id; 
		n2.body='inserting note from code'; // body of the note. this should be populated from the notes field that you have shown in the screenshot
		n2.title='New Note from code'; // the title of the note
		n2.isPrivate=false;  // if private it will be visible only for the owner and any user with modify all permission
		insert n2; //insert the record          

        //Create Notes
		note n3 = new note();
		n3.parentId=insertedRel.id; 
		n3.body='inserting note from code'; // body of the note. this should be populated from the notes field that you have shown in the screenshot
		n3.title='New Note from code'; // the title of the note
		n3.isPrivate=false;  // if private it will be visible only for the owner and any user with modify all permission
		insert n3; //insert the record          

        //Create Notes
		note n4 = new note();
		n4.parentId=insertedRel.id; 
		n4.body='inserting note from code'; // body of the note. this should be populated from the notes field that you have shown in the screenshot
		n4.title='New Note from code'; // the title of the note
		n4.isPrivate=false;  // if private it will be visible only for the owner and any user with modify all permission
		insert n4; //insert the record          
                        
        //create attachment 
		
		Blob b = Blob.valueOf('111224422424242424');
		Attachment attach1= new Attachment();
		attach1.ParentId = insertedRel.id;
		attach1.Name = 'Test Attachment for Parent';
		attach1.Body = b;
		
		insert attach1;          
	
    PageReference pageRef = Page.RelNoteAttachment;
    PageRef.getParameters().put('id',insertedRel.id);
      

    Test.setCurrentPage(pageRef);
    ApexPages.standardController controller = new ApexPages.standardController(insertedRel);
    boolean isNote = true;
   	//ApexPages.currentPage( ).getParameters().put('DelItemType',i);
    //ApexPages.currentPage( ).getParameters().put('DelItemID',n.id);
    RelNoteAttachment pag = new RelNoteAttachment(controller);
    
    pag.Next();
    pag.Previous();
    pag.DelItemID=n.id;
    pag.DelItemType=true;
    pag.DeleteNote();
    
    pag.DelItemID=attach1.id;
    pag.DelItemType=false;
    pag.DeleteNote();    
              
   
       
    }
}