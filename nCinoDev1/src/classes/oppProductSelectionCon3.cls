global class oppProductSelectionCon3 {
        public String searchStr{get;set;}
        public Integer prodIndex{get;set;}
        public List<wrapProduct> wrapProductList{get;set;}
        public Boolean isAlreadySelected{get;set;}
        public Boolean isNotfirstLoad{get;set;}
        public Boolean isException{get;set;}
        public string exMsg{get;set;}
        public Opportunity opp{get;set;}
        public string errorMsg{get;set;}
        public string prodId{get;set;}
        public string searchFilter{get;set;}
        public string pricebook2id{get;set;}
        public Boolean isEmpty{get;set;}
        public User currentUser{get;set;}
        public Boolean isLOB{get;set;}
        public Boolean firstSave{get;set;}
        public Boolean isMultiSelect{get;set;}
        public List<String> productNameList;
        Map<Id,PricebookEntry> masterProductMap;
        String retUrl;
        Id oppId;
        
        public oppProductSelectionCon3(ApexPages.StandardController controller) {
                errorMsg = 'ok';
                exMsg='';
                isException = false;
                isEmpty=true;
                isNotfirstLoad=false;
                firstSave=false;
                isMultiSelect=true;
                isAlreadySelected = false;
                prodIndex = null;
                wrapProductList = new List<wrapProduct>();
                productNameList = new List<String>();
                oppId = controller.getId();
                // Since User is arriving at this page once Pipeline is saved, should be returned to the pipeline detail page
                //retUrL = ApexPages.currentPage().getParameters().get('retUrl');
                retUrL = controller.getId();
                opp = [select pricebook2id,Name, Owner.LOB__c from Opportunity where Id=:oppId]; //Added Owner.LOB__c for the purpose of matching it with pricebook name... Ephrem 05-2015
                currentUser = [select Id, LOB__c, profile.name from User where ID=:UserInfo.getUserId()];
                isLOB = true;
                //also added OneView Audit Profile and TM Auditor to have access to all pricebooks...Case 1710...Ephrem 02-2015
                
                //***********************************************************************************************************
                //*****Case 1843... Allow System Admins and Auditors to add any products to any pricebooks... 
                //*****if pricebook name matches the pipeline owner's business group, get the pricebook entry id
                //*****from the corresponding pricebook;otherwise pull the id from the Standard Price Book...
                //***** Ephrem 05-2015... code lines 51-59
                //************************************************************************************************************
                if (currentUser.Profile.Name == 'System Administrator' || currentUser.Profile.Name == 'OneView Audit Profile' || currentUser.Profile.Name == 'TM Auditor') {                   
                   List<priceBook2> pb = [Select Id, Name from PriceBook2 Where Name=:opp.Owner.LOB__c];
                   if(pb.size() > 0){
                       if(pb[0].name == opp.Owner.LOB__c){
                           pricebook2id=pb[0].Id;
                       }
                   }
                   else{
                        PriceBook2 priceBook = [Select Id, Name from PriceBook2 Where Name='Standard Price Book'];
                        pricebook2id=priceBook.Id;
                   }
                }else{
                   if (opp.pricebook2id!=null){
                           pricebook2id =opp.pricebook2id;
                   }else {
                       if (currentUser.LOB__c!=null) { 
                           PriceBook2 priceBook = [Select Id, Name from PriceBook2 Where Name=:currentUser.LOB__c];
                           pricebook2id=priceBook.Id;
                       } else{
                           isLOB=false;
                           PriceBook2 priceBook = [Select Id, Name from PriceBook2 Where Name='Empty'];
                           pricebook2id=priceBook.Id;
                       }
                   } 
                }
                // ensure only active products within pricebook are returned
                masterProductMap=new Map<Id,PricebookEntry>([Select Id, UseStandardPrice, UnitPrice,Product2.Family, ProductCode, Product2Id,Product2.description, Pricebook2Id, Name,Pricebook2.name, IsActive From PricebookEntry where isActive = True and Pricebook2id =:pricebook2id]);
        }

        //Method to display dynamic content on screen
        public Component.Apex.PageBlock getSection(){
            Map<string, Schema.SObjectField> FieldMap;
            FieldMap = Schema.SObjectType.OpportunityLineItem.fields.getMap();
            Set<string> FieldSet = FieldMap.keySet();
            List<string> FieldList = new List<string>();
            FieldList.addAll(FieldSet);
            FieldList.sort();
            boolean addHelp = true;
              
            Component.Apex.PageBlock pb= new Component.Apex.PageBlock();
            pb.id='pb2';
            
            Component.Apex.PageBlockSection defaultPbs = new Component.Apex.PageBlockSection();
            defaultPbs.title='No Products Added. Please add products from find products tab';
            defaultPbs.rendered=isEmpty;
            defaultPbs.collapsible=false;
            
            pb.childComponents.add(defaultPbs);

            if (wrapProductList.isEmpty()==false){                  
            for (integer i=0; i<wrapProductList.size();i++){
                addHelp = True;
                List<Product_Layout__c> productLayoutList = 
                    [select Id, Name, Field__c, Sequence__c, Display__c, Custom_Label_Text__c, URL__C 
                    from Product_Layout__c where Name=:wrapProductList[i].priceBookProd.Product2.Family and Sequence__c>0
                    order by Sequence__c ASC];
                    
                Component.Apex.outputPanel div = new Component.Apex.outputPanel();
                div.styleClass='ui-widget-content';
                
                Component.Apex.outputPanel span = new Component.Apex.outputPanel();
                span.styleClass='ui-icon ui-icon-cart';
                span.style='display:inline-block;';
                
                div.childComponents.add(span);
                
                Component.Apex.PageBlockSection pbs = new Component.Apex.PageBlockSection(columns = 2);
                pbs.title=wrapProductList[i].priceBookProd.Name;
                    
                for(Product_Layout__c productLayout : productLayoutList ){
                  Component.Apex.PageBlockSectionItem pbsi = new Component.Apex.PageBlockSectionItem();
                  Schema.DescribeFieldResult field = (FieldMap.get(productLayout.Field__c)).getDescribe();
                                    
                  if (field.isAccessible() && productLayout.Display__c=='Output'){                     
                      Component.Apex.OutputText lblText = new Component.Apex.OutputText();
                      if (productLayout.Custom_Label_Text__c==NULL){
                          lblText.value = field.getLabel();
                      }else{
                          lblText.value = productLayout.Custom_Label_Text__c;
                      }
                  
                      Component.Apex.OutputLabel lblValue = new Component.Apex.OutputLabel();
                     // lblValue.value = wrapProductList[i].oppLineItem.get(productLayout.Field__c);
                      lblValue.style = 'font-weight:Bold;color:black;';
                     
                      if (field.getType().name()=='Currency') {
                        List<String> args = new String[]{'0','number','###,###,##0.00'};
                        System.debug('==>'+wrapProductList[i].oppLineItem.get(productLayout.Field__c));
                        Decimal d = (Decimal) wrapProductList[i].oppLineItem.get(productLayout.Field__c);
                        if (d==null)
                            lblValue.value = wrapProductList[i].oppLineItem.get(productLayout.Field__c);
                        else
                            lblValue.value =  '$' + String.format(d.format(), args);
                      }
                      else {
                        lblValue.value = wrapProductList[i].oppLineItem.get(productLayout.Field__c);
                      }
                      
                      pbsi.childComponents.add(lblText);
                      pbsi.childComponents.add(lblValue);                       
                  }
                  else if (field.isAccessible() && field.isUpdateable() && productLayout.Display__c=='Input'){
                      Component.Apex.InputField input = new Component.Apex.InputField();  
                      input.expressions.value = '{!wrapProductList['+i+']'+'.oppLineItem.'+productLayout.Field__c +'}';  
                      input.id = field.getName();  
                      
                      Component.Apex.OutputLabel inputLabel = new Component.Apex.OutputLabel();  
                      if (productLayout.Custom_Label_Text__c==NULL){
                          inputLabel.value = field.getLabel();
                      }else{
                          inputLabel.value = productLayout.Custom_Label_Text__c;
                      } 
                      inputLabel.for = field.getName();  
                      
                      pbsi.childComponents.add(inputLabel);
                      pbsi.childComponents.add(input);                       
                  } 
                  else if(field.isAccessible() && productLayout.Display__c=='Link'){
                      addHelp = false;
                      Component.Apex.OutputLink Link = new Component.Apex.OutputLink();
                      Link.value = 'javascript:void(0)';
                      system.debug('url name ** '+productLayout.URL__c);
                      Link.onclick = 'window.open("'+productLayout.URL__c+'","","resizable=yes","width=500,height=500")';
                      
                      Link.title = 'Click Here';
                      
                      Component.Apex.outputText txt = new Component.Apex.outputText();
                      txt.value = 'Click Here';
                      link.childComponents.add(txt);

                      Component.Apex.OutputLabel LinkLabel = new Component.Apex.OutputLabel();  
                      if (productLayout.Custom_Label_Text__c==NULL){
                          LinkLabel.value = field.getLabel();
                      }else{
                          LinkLabel.value = productLayout.Custom_Label_Text__c;
                      } 
                      LinkLabel.for = field.getName();
                  
                      pbsi.childComponents.add(LinkLabel);
                      pbsi.childComponents.add(Link);                           
                  } 
                  if(addHelp == True){
                    pbsi.helpText = field.getInlineHelpText();
                  } 
                  pbs.childComponents.add(pbsi);
                  div.childComponents.add(pbs);
                } 
                pb.childComponents.add(div);               
              }
            }

            return pb;
        }
        
        //Method to select product from auto suggest
        public void selectProduct(){           
                isAlreadySelected = false;
                
                Map <String, MultiProduct__c> custMap = new Map<String, MultiProduct__c>();  
                custMap = MultiProduct__c.getAll();             
                if (custMap.containsKey(currentUser.LOB__c)==false && isEmpty==false){
                    isMultiSelect=false;
                }

                PricebookEntry prod= new PricebookEntry();
                //retrieve the product from the master map
                prod = masterProductMap.get(prodId);
                if(prod !=null && (!prodAlreadySelected(prod.Id)) && isMultiSelect==true ){
                        wrapProduct wrap = new wrapProduct();
                        wrap.priceBookProd = prod;
                        wrap.oppLineItem.unitPrice = prod.unitPrice;
                        wrapProductList.add(wrap);
                        productNameList.add(prod.Name);

                }

                searchStr='';
                prodId = null;
                isEmpty = wrapProductList.isEmpty();
                
        }
        //method to remove product on onClick event
        public void removeProduct(){
                isAlreadySelected = false;
                if(prodIndex!=null && prodIndex > -1 && prodIndex<=wrapProductList.size() && (!wrapProductList.isEmpty())){
                        if (wrapProductList.get(prodIndex).oppLineItem.Id<>null)
                            delete wrapProductList.get(prodIndex).oppLineItem;
                        productNameList.remove(prodIndex);
                        wrapProductList.remove(prodIndex);
                }
                //check if any product is selected or not
                isEmpty = wrapProductList.isEmpty();
                if (isEmpty==true)
                    isMultiSelect=true;
                prodIndex=null;

        }
        //method to save products
        public Pagereference saveProducts(){
                isNotfirstLoad = true;
                errorMsg = 'ok';
                exMsg='';
                integer i=0;
                //Check if any product is selected
                if(wrapProductList.isEmpty()){
                        //errorMsg = '<b>No Product Selected!</b><br/> Please select a product first.';
                        isEmpty = true;
                        return null;
                }

                Boolean skip=false;
                String searchStr='';
                String errorString='';
                List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
                for(wrapProduct prod: wrapProductList){
                        prod.oppLineItem.quantity=1;
                        //check for mandatory fields
                        if(prod.oppLineItem.quantity==null ||prod.oppLineItem.quantity==0){
                                errorMsg ='<b>Required field missing or not of required type.<br/>Please check following products</b><br/>';
                                errorMsg=errorMsg+i+'.'+prod.priceBookProd.Name+'<br/><b>&nbsp;&nbsp;Field:</b>'
                                                +((prod.oppLineItem.quantity==null ||prod.oppLineItem.quantity==0)?'Quantity<br/>':'')
                                                +(prod.oppLineItem.unitPrice==null?'Sales Price<br/>':'');
                                skip=true;
                                                                
                        }
                        if(!skip){
                                if (prod.oppLineItem.PricebookEntryId==null){
                                prod.oppLineItem.PricebookEntryId=prod.priceBookProd.Id;
                                prod.oppLineItem.OpportunityId=oppid;                                
                                }
                                // prod.oppLineItem.TotalPrice = prod.oppLineItem.unitPrice *  prod.oppLineItem.Quantity;
                                oppLineItemList.add( prod.oppLineItem);                                
                        }
                }
                //if mandatory fields are present insert 
                if(!skip){
                    List<Database.upsertResult> results = Database.upsert(oppLineItemList, false);                    
                    for (Database.upsertResult result : results){
                      if (result.isSuccess()==false){
                          isException=true;
                          for (Database.error errorResult : result.getErrors()){                              
                              exMsg= exMsg + errorResult.getMessage() + '<br/>';
                          }                          
                          System.debug('Upsert Error#1 ======> '+exMsg);                      
                      } else 
                          return new Pagereference('/'+retUrl);
                    }
                }
                return null;
        }
        //method to quickSave products
        public void quickSave(){
                isNotfirstLoad = true;
                errorMsg = 'ok';
                exMsg='';
                integer i=0;
                //Check if any product is selected
                if(wrapProductList.isEmpty()){
                        //errorMsg = '<b>No Product Selected!</b><br/> Please select a product first.';
                        isEmpty = true;
                        return;
                }

                Boolean skip=false;
                String searchStr='';
                String errorString='';
                errorMsg ='<b>Required field missing or not of required type.<br/>Please check following products</b><br/>';
                List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
                for(wrapProduct prod: wrapProductList){
                        prod.oppLineItem.quantity=1;
                        //check for mandatory fields
                        if(prod.oppLineItem.unitPrice==null){
                                errorMsg=errorMsg+i+'.'+prod.priceBookProd.Name+'<br/><b>&nbsp;&nbsp;Field:</b>'
                                                +((prod.oppLineItem.quantity==null ||prod.oppLineItem.quantity==0)?'Quantity<br/>':'')
                                                +(prod.oppLineItem.unitPrice==null?'Sales Price<br/>':'');
                                skip=true;
                               
                        }
                        if(!skip){                                
                                if (prod.oppLineItem.PricebookEntryId==null){
                                prod.oppLineItem.PricebookEntryId=prod.priceBookProd.Id;                                
                                prod.oppLineItem.OpportunityId=oppid;                                
                                }
                                // prod.oppLineItem.TotalPrice = prod.oppLineItem.unitPrice *  prod.oppLineItem.Quantity;
                                oppLineItemList.add( prod.oppLineItem);
                        }
                }
                //if mandatory fields are present insert 
                if(!skip){
                            
                   List<Database.upsertResult> results = Database.upsert(oppLineItemList, false);
                    
                    for (Database.upsertResult result : results){
                      if (result.isSuccess()==false){
                          isException=true;
                          for (Database.error errorResult : result.getErrors()){                              
                              exMsg= exMsg + errorResult.getMessage() + '<br/>';
                          }
                          System.debug('Upsert Error#2 ======> '+exMsg);                      
                      }
                      else {
                        firstSave=true;
                        errorMsg = 'ok';
                        //return new Pagereference('/'+retUrl);
                        Map<String,OpportunityLineItem> oppItemMap=new Map<String,OpportunityLineItem>();
                        List<wrapProduct> newWrapProductList= new List<wrapProduct>();
                        //Update the select to add any new product field to be shown
                        
                        String qryOppLineItem = new selectall('OpportunityLineItem').soql + ' where OpportunityId=:oppid and product_name__c in :productNameList';
                        List<OpportunityLineItem> lineItemList = Database.query(qryOppLineItem);
                        
                        for (OpportunityLineItem item : lineItemList){
                            oppItemMap.put(item.PricebookEntryId,item);
                        }    
                            
                        for (integer j=0; j< wrapProductList.size();j++){                            
                            wrapProductList[j].oppLineItem=oppItemMap.get(wrapProductList[j].priceBookProd.id);                        
                        }   
                    }
                }

        }
       }
        
        
        //cancel method
        public Pagereference cancel(){
                List<OpportunityLineItem> deleteList = new List<OpportunityLineItem>();
                for (wrapProduct prod : wrapProductList){
                    if (prod.oppLineItem.Id <> null )
                        deleteList.add(prod.oppLineItem);                       
                }
                delete deleteList;
                return new Pagereference('/'+retUrl);
        }
        //the wrapper class
        public class wrapProduct{
                public PricebookEntry priceBookProd{get;set;}
                public OpportunityLineItem oppLineItem{get;set;} 

                public wrapProduct(){
                        priceBookProd = new PricebookEntry ();
                        oppLineItem = new OpportunityLineItem ();
                }
        }
        
        //Method to populate auto suggest field
        @RemoteAction
        global static list<PriceBookEntry> returnMatchingRec(String searchStr,String field2Search,String pricebook2id){
                String query;
                if(pricebook2id =='' || pricebook2id ==null){
                        query='Select Id, UseStandardPrice, UnitPrice,Product2.Family, ProductCode, Product2Id,Product2.description, Pricebook2Id, Name,Pricebook2.name, IsActive From PricebookEntry where isActive = True and '+field2Search+ ' Like '+'\'%'+searchStr+'%\' and Pricebook2.isStandard=true';
                } else{
                        query='Select Id, UseStandardPrice, UnitPrice,Product2.Family, ProductCode, Product2Id,Product2.description, Pricebook2Id, Name,Pricebook2.name, IsActive From PricebookEntry where isActive = True and '+field2Search+ ' Like '+'\'%'+searchStr+'%\' and pricebook2id ='+'\''+pricebook2id+'\' ';
                }

                list<PriceBookEntry> result = Database.query(query);
                return result ;
        }
        //method to check whether the product is already added or not
        private Boolean prodAlreadySelected(String Id2Chk){
                for(wrapProduct wrap:wrapProductList){
                        if(wrap.priceBookProd.Id== Id2Chk){
                                isAlreadySelected = true;
                                return isAlreadySelected;
                        }
                }
                isAlreadySelected = false;        
                return isAlreadySelected ;
        }
}