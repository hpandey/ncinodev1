/**********************************************************************************************
 * Name         : RuleDefinitionControllerExt
 * Author       : Deloitte Consulting
 * Description  : This is a controller class which will include all the business logic 
                                for RuleDefinition Page.
 *********************************************************************************************/

public with sharing class RuleDefinitionControllerExt {
    
    public String strRecordId;
    public Integer intRowAddedToWrapper; // To track how many rows has been added to wrapper class.
    public String strSelectedObject {get;set;}
    public String strSelectedOldObject;
    public String errorMessage;
    
    public Rule__c objRule {get;set;}
    
    public List<Rule_Criterion__c> listRuleCriterionToBeInserted;
    public List<Rule_Criterion__c> listRuleCriterionToBeDeleted;
    public List<SelectOption> lstObjectsOption {get;set;}
    public List<SelectOption> lstFieldsOption {get;set;}
    public List<SelectOption> lstOperatorType {get;set;}
    public list<RuleCriteriaWrapper> lstRuleCriteriaWrapper {get;set;}
    
    public map<String , String > mapDataTypeByFieldName {get;set;}                                                 
    
    /***
    * Method name    : RuleDefinitionControllerExt
    * Description    : Constructor of class.
    * Return Type    : None
    * Parameter      : None
    */

    public RuleDefinitionControllerExt(ApexPages.StandardController controller) {
        
        strRecordId = ApexPages.currentPage().getParameters().get('id');
        // Initiates all the collections
        init();
        
        //Creating Default Rule Criteria Table
        populateObjectList();
        populateFieldList(); 
        
        if(String.isBlank(strRecordId)) {
            addNewRow();
        }
        else {
            loadSavedData();
        }
        strSelectedOldObject = strSelectedObject;
    }
    
     /*Method Name    :        init
      *Author         :        Deloitte Consulting
      *Description    :        This method is used to instatiate the collections 
      *Parameters     :        None
      *Returns        :        Void
    */
    // code review done
    private void init(){
        //instantiate objects
        objRule = new Rule__c();
        
        //instantiate lists
        lstObjectsOption = new list<SelectOption>();
        lstFieldsOption = new list<SelectOption>();
        lstOperatorType = new list<SelectOption>();
        ListRuleCriterionToBeInserted = new List<Rule_Criterion__c>();
        listRuleCriterionToBeDeleted = new list<Rule_Criterion__c>();
        lstRuleCriteriaWrapper = new list<RuleCriteriaWrapper>();
        mapDataTypeByFieldName = new map<String , String>();
        
        intRowAddedToWrapper = lstRuleCriteriaWrapper.size();
    }
    
    /***
    * Method name    : loadSavedData
    * Description    : load saved data of Rule object and Rule Criteria object in case of Edit.
    * Return Type    : Void
    * Parameter      : None
    */
    //code review done
    public void loadSavedData() {
        if(!String.isBlank(strRecordId)) {
        
            //Initiate Select list for picklist value field
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('' , '--None--'));
            
            // Fetch list of rule criteria in case of rule edit
            list<Rule_Criterion__c> lstCriteria = [select
                                                    Rule__r.Criteria_Logic__c , Rule__r.Object_Name__c , Rule__r.Rule_Type__c,
                                                    id, Boolean_Value__c , Criteria_Sequence__c , Currency_Value__c , Date_Value__c , Email_Value__c ,
                                                    Field_Name__c , Field_Type__c , Number__c , Percent_Value__c , Operator__c , Phone_Value__c ,
                                                    Rule__c , Text_Value__c ,URL_Value__c
                                                    From Rule_Criterion__c
                                                    Where Rule__c =: strRecordId
                                                  ];
            if(lstCriteria != Null && !lstCriteria.isEmpty()) {
               
                for(Rule_Criterion__c objCriteria : lstCriteria) {
                    objRule.id = objCriteria.Rule__c;
                    objRule.Criteria_Logic__c = objCriteria.Rule__r.Criteria_Logic__c;
                    objRule.Object_Name__c = objCriteria.Rule__r.Object_Name__c;
                    objRule.Rule_Type__c = objCriteria.Rule__r.Rule_Type__c;
                    strSelectedObject = objCriteria.Rule__r.Object_Name__c;
                    
                    list<SelectOption> lstOperatorsOptions = getOperatorType(objCriteria.Field_Name__c);
                    system.debug('lstOperatorsOptions@@@@@@@'+lstOperatorsOptions);
                    for(SelectOption objOperatorsOptions : lstOperatorsOptions) {
                        if(objOperatorsOptions.getValue() == objCriteria.Operator__c) {
                            objOperatorsOptions.setValue(objCriteria.Operator__c);
                        }
                    }
                    system.debug('lstOperatorsOptions@@@@@@@1'+lstOperatorsOptions);
                    
                    // instantiate Wrapper
                    system.debug('objCriteria@@'+objCriteria);
                    
                    //Initiate list for multiSelect list for wrapper
                    list<String> listMultiSelectedOptions = new list<String>();
                    
                    RuleCriteriaWrapper objWrapper = new RuleCriteriaWrapper(objCriteria.Criteria_Sequence__c , objCriteria.Field_Name__c , objCriteria.Operator__c , lstOperatorsOptions , objCriteria , options , listMultiSelectedOptions);
                    
                    // Fetching Picklist Values in case of Picklist Field
                    objWrapper = populatePicklistOption(strSelectedObject, objCriteria.Field_Name__c , objWrapper);
                    
                    system.debug('objWrapper@@@@@@@@@@'+objWrapper.strOperator);
                    lstRuleCriteriaWrapper.add(objWrapper);
                    system.debug('lstRuleCriteriaWrapper@@'+lstRuleCriteriaWrapper);
                    
                    // Updating the sequence of wrapper list
                    lstRuleCriteriaWrapper.sort();
                }
                // Initiate field list 
                populateFieldList();
            }
        }
    }
    
    /***
    * Method name    : populateObjectList
    * Description    : method used for fetching list of objects from custom setting and displaying as picklist.
    * Return Type    : List<SelectOption>
    * Parameter      : None
    */
    //code review done
    public void populateObjectList() {
        //Accessing Object List from Custom Setting
        Map<String, Rule_definition_Custom_Setting__c> mapRuleDefinition = Rule_definition_Custom_Setting__c.getAll();
        
        lstObjectsOption.add(new SelectOption('' , '--None--'));
        for(Rule_definition_Custom_Setting__c objRuleDefinition : mapRuleDefinition.values()) {
            lstObjectsOption.add(new SelectOption(objRuleDefinition.API_Name__c , objRuleDefinition.Name));
        }
    }
    
    /***
    * Method name    : populateFieldList
    * Description    : method used for fetching list of fields from selected object and displaying as picklist.
    * Return Type    : List<SelectOption>
    * Parameter      : None
    */
    //code review done
    public void populateFieldList() {
        //Reset Wrapper List when object changes
        if(!String.isBlank(strSelectedOldObject)) { 
            if(String.isBlank(strRecordId)) {
                lstRuleCriteriaWrapper.clear(); //Clearing List in case of New Rule Creation when selected Obect Changes.    
            }
            else {
                deleteRelatedRuleCriteria(); // Delete all rule criteria in case of edit when selected object changes.
            }
                                            
            addNewRow(); // Adding Default Row
        }
        
        lstFieldsOption.clear(); //Reset Select Option List
        lstFieldsOption.add(new SelectOption('' , '--None--'));
        
        if(!string.isBlank(strSelectedObject)) {
            
            // Fetching fields of selected object.
            Map<String, Schema.SObjectField> mapSobjectFields = Schema.getGlobalDescribe().get(strSelectedObject).getDescribe().fields.getMap();
            for(Schema.SObjectField objectFields : mapSobjectFields.values()) {
                Schema.DescribeFieldResult fieldDescribe = objectFields.getDescribe();
                //check to exclude system, read-only & relationship fields
                if(fieldDescribe.isUpdateable() && !fieldDescribe.isAutoNumber() && fieldDescribe.getRelationshipName()==null) {
                    lstFieldsOption.add(new SelectOption(string.valueof(objectFields), UtilityClass.getFieldLabelNames(strSelectedObject , string.valueof(objectFields))));
                } 
            }    
        }

        //Sorting the option list
        SelectOptionSorter.doSort(lstFieldsOption , SelectOptionSorter.FieldToSort.Label);

        // fetching default operators
        getOperatorType('');

        //setting Old Value for Object
        strSelectedOldObject = strSelectedObject;
    }
    
    /***
    * Method name    : addNewRow
    * Description    : method used for creating new row
    * Return Type    : void
    * Parameter      : None
    */
    
    public void addNewRow() {
        Rule_Criterion__c objRuleCriteria = new Rule_Criterion__c();
        list<SelectOption> lstOperatorsOptions = getOperatorType('');
        
        //Initiate Select list for picklist value field
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('' , '--None--'));
        
        //Initiate list for multiSelect list
        list<String> listMultiSelectedOptions = new list<String>();
        
        // instantiate Wrapper
        RuleCriteriaWrapper objWrapper = new RuleCriteriaWrapper('' , '' , '' , lstOperatorsOptions , objRuleCriteria , options , listMultiSelectedOptions);
        lstRuleCriteriaWrapper.add(objWrapper);

        // Updating the sequence of wrapper list
        updateWrapperListSequence();
    }
    
    /***
    * Method name    : updateCurrentRow
    * Description    : method used for updating Operators according to field selected .
    * Return Type    : void
    * Parameter      : None
    */
 
    public void updateCurrentRow() {
        // getting which row number whhich have to update
        integer intRowNumber = (integer.valueof(ApexPages.currentPage().getParameters().get('rowNumber')) - 1);
        RuleCriteriaWrapper objWrapper = lstRuleCriteriaWrapper.get(intRowNumber);
        objWrapper.lstOperators = getOperatorType(objWrapper.objRuleCriteriaWrap.Field_Name__c);
        //Fetching Picklist Values in case of picklist field 
        system.debug('$$$$$$$$$$$$$$$objWrapper'+objWrapper);
        objWrapper = populatePicklistOption(strSelectedObject, objWrapper.objRuleCriteriaWrap.Field_Name__c , objWrapper);
        
        lstRuleCriteriaWrapper.set(intRowNumber , objWrapper);
        
    }
    
    /***
    * Method name    : deleteRow
    * Description    : method used for deleting current row
    * Return Type    : void
    * Parameter      : None
    */
    //code review done
    public void deleteRow() {
        integer intRowNumber = (integer.valueof(ApexPages.currentPage().getParameters().get('rowNumberToDelete')) - 1);
        Rule_Criterion__c objRuleCriteria = lstRuleCriteriaWrapper.get(intRowNumber).objRuleCriteriaWrap;
        if(objRuleCriteria.id != Null) {
            listRuleCriterionToBeDeleted.add(objRuleCriteria);
        }

        lstRuleCriteriaWrapper.remove(intRowNumber);

        // Updating the sequence of wrapper list
        updateWrapperListSequence();
    }
    
    /***
    * Method name    : deleteRelatedRuleCriteria
    * Description    : method used for deleting all related rule criteria when selected object changes.
    * Return Type    : void
    * Parameter      : None
    */
    public void deleteRelatedRuleCriteria() {
        if(!lstRuleCriteriaWrapper.isEmpty()) {
            for(RuleCriteriaWrapper objRuleCriteriaWrapper : lstRuleCriteriaWrapper ) {
                if(objRuleCriteriaWrapper.objRuleCriteriaWrap.id != Null) {
                    listRuleCriterionToBeDeleted.add(objRuleCriteriaWrapper.objRuleCriteriaWrap);
                }
            }
            lstRuleCriteriaWrapper.clear(); // Clearing wrapper list after deleting all rule criteria.
        }
    }
    /***
    * Method name    : getOperatorType
    * Description    : method used for fetching list of operators according selected field
    * Return Type    : List<SelectOption>
    * Parameter      : string strSelectedField
    */
    //code review done
    public List<SelectOption> getOperatorType(string strSelectedField) {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('' , '--None--'));
        list<string> lstOperators = new list<String>();
        
        system.debug('strSelectedObject@@'+strSelectedObject);
        if(!string.isBlank(strSelectedObject)) {
            if(!String.isBlank(strSelectedField)) {
                string strFieldType;
                Schema.DisplayType fieldSchemaDisplayType = UtilityClass.getFieldType(strSelectedObject , strSelectedField);
                                
                // updating mapDataTypeByFieldName
                if(UtilityClass.mapDataType_RCFieldName.ContainsKey(string.valueof(fieldSchemaDisplayType))) {
                    mapDataTypeByFieldName.put(strSelectedField , UtilityClass.mapDataType_RCFieldName.get(string.valueof(fieldSchemaDisplayType)));
                }
                system.debug('@@@@####@@fieldSchemaDisplayType'+fieldSchemaDisplayType);
                system.debug('@@@@####@@UtilityClass.mapDataType_RCFieldName'+UtilityClass.mapDataType_RCFieldName);
                system.debug('@@@@####@@mapDataTypeByFieldName'+mapDataTypeByFieldName);
                
                if(UtilityClass.mapFieldDisplayType_DataType.ContainsKey(fieldSchemaDisplayType)) {
                    strFieldType = UtilityClass.mapFieldDisplayType_DataType.get(fieldSchemaDisplayType);
                    system.debug('strFieldType@@'+strFieldType);
                    lstOperators = UtilityClass.getOperatorType(strFieldType);
                    
                }
            }
            
            if(lstOperators != Null && !lstOperators.isEmpty()) {
                for(String objOperators : lstOperators) {
                   options.add(new SelectOption(objOperators , objOperators)); 
                   system.debug('$$$$$$$$$$$'+objOperators);
                }
                
                /*options.add(new SelectOption('Equal To' , 'Equals To'));
                options.add(new SelectOption('Not Equal To' , 'Not Equal To'));*/
            }    
        }
        return options;
    }
    
    /***
    * Method name    : saveData
    * Description    : method used for creating Rules and Rule Criteria
    * Return Type    : pageReference
    * Parameter      : None
    */
    //code review done
    public pageReference saveData() {

        errorMessage = Null; // Reset Error Message
        System.savePoint objSavePoint = Database.setSavePoint();
        PageReference objPageRef = null;
        if(!String.isBlank(strSelectedObject) && !string.isBlank(objRule.Criteria_Logic__c) && !string.isBlank(objRule.Rule_Type__c) ) {
            objRule.Object_Name__c = strSelectedObject;
            try {
                upsert objRule;
                system.debug('I am In try'+objRule);
            }
            catch(DmlException e) {
                system.debug('I am in Catch'+e);
                errorMessage =  e.getMessage();
                displayErrorMessage();
                Database.rollback(objSavePoint);
            }
        }
        else {
            errorMessage = 'Please Fill all fields related to Rule Definition';
            displayErrorMessage();
        }
        
        if(!lstRuleCriteriaWrapper.isEmpty() && objRule.id != Null) {
            
            listRuleCriterionToBeInserted.clear(); // Reset List Of Rule Criteria
            validateData();
            system.debug('errorMessage@@##'+errorMessage);
            if(String.isBlank(errorMessage) && !listRuleCriterionToBeInserted.isEmpty()) {
                try{
                    system.debug('listRuleCriterionToBeInserted@@@@'+listRuleCriterionToBeInserted);
                    Upsert listRuleCriterionToBeInserted;
                }
                catch(DmlException e) {
                    errorMessage =  e.getMessage();
                    displayErrorMessage();
                    Database.rollback(objSavePoint);
                }
            }
            else {
                displayErrorMessage(); 
            }

            //Deleting Rule Criteria
            if(!listRuleCriterionToBeDeleted.isEmpty()) {
                try {
                    delete listRuleCriterionToBeDeleted;
                }
                catch(DmlException e) {
                    errorMessage =  e.getMessage();
                    displayErrorMessage();
                    Database.rollback(objSavePoint);
                }
            }
        }
        system.debug('errorMessage@@'+errorMessage);
        if(String.isBlank(errorMessage)) {
            system.debug('I am In');
            objPageRef = new PageReference('/'+objRule.id);
            objPageRef.setRedirect(true);
        }
        return objPageRef;
    }
    
    /***
    * Method name    : validateData
    * Description    : method used to Validate data before Save.
    * Return Type    : None
    * Parameter      : Void
    */
    public void validateData() {
        if(!lstRuleCriteriaWrapper.isEmpty()) {
            set<String> setUniqueValues = new set<String>();
            for(RuleCriteriaWrapper objRuleCriteriaWrapper : lstRuleCriteriaWrapper) {
                if(objRuleCriteriaWrapper.objRuleCriteriaWrap.Field_Name__c != Null && objRuleCriteriaWrapper.objRuleCriteriaWrap.Operator__c != Null && 
                    (objRuleCriteriaWrapper.objRuleCriteriaWrap.Text_Value__c != Null || objRuleCriteriaWrapper.objRuleCriteriaWrap.Number__c != Null || 
                    objRuleCriteriaWrapper.objRuleCriteriaWrap.Date_Value__c != Null || objRuleCriteriaWrapper.objRuleCriteriaWrap.Currency_Value__c != Null ||
                    objRuleCriteriaWrapper.objRuleCriteriaWrap.URL_Value__c != Null || objRuleCriteriaWrapper.objRuleCriteriaWrap.Percent_Value__c != Null ||
                    objRuleCriteriaWrapper.objRuleCriteriaWrap.Phone_Value__c != Null || objRuleCriteriaWrapper.objRuleCriteriaWrap.Email_Value__c != Null ||
                    objRuleCriteriaWrapper.objRuleCriteriaWrap.Boolean_Value__c == true) ) {
                    
                    string strUnique = objRuleCriteriaWrapper.objRuleCriteriaWrap.Field_Name__c + objRuleCriteriaWrapper.objRuleCriteriaWrap.Operator__c+
                                        objRuleCriteriaWrapper.objRuleCriteriaWrap.Text_Value__c + objRuleCriteriaWrapper.objRuleCriteriaWrap.Number__c +
                                        objRuleCriteriaWrapper.objRuleCriteriaWrap.Date_Value__c + objRuleCriteriaWrapper.objRuleCriteriaWrap.Currency_Value__c+
                                        objRuleCriteriaWrapper.objRuleCriteriaWrap.Boolean_Value__c + objRuleCriteriaWrapper.objRuleCriteriaWrap.Percent_Value__c +
                                        objRuleCriteriaWrapper.objRuleCriteriaWrap.Phone_Value__c + objRuleCriteriaWrapper.objRuleCriteriaWrap.Email_Value__c +
                                        objRuleCriteriaWrapper.objRuleCriteriaWrap.URL_Value__c ;
                    system.debug('strUnique$$$$'+strUnique);
                    if(setUniqueValues.contains(strUnique)) {
                        errorMessage = 'Duplicate Records Found';
                    }
                    else {
                        Rule_Criterion__c objRuleCriterion = new Rule_Criterion__c();
                        objRuleCriterion = objRuleCriteriaWrapper.objRuleCriteriaWrap;
                        objRuleCriterion.Criteria_Sequence__c = objRuleCriteriaWrapper.strCriteriaNumber;
                        if(string.isBlank(strRecordId) || objRuleCriterion.id == Null) {
                            objRuleCriterion.Rule__c = objRule.id;
                        }
                        if(!objRuleCriteriaWrapper.listSelectedMultiPicklistOptions.isEmpty()) {
                            objRuleCriterion.Text_Value__c = utilityClass.convertArraytoString(objRuleCriteriaWrapper.listSelectedMultiPicklistOptions);   
                        }
                        listRuleCriterionToBeInserted.add(objRuleCriterion);
                        setUniqueValues.add(strUnique);
                    }
                }
                else {
                    errorMessage = 'Please Fill all fields related to Rule Criteria'; 
                }   
            }   
        }
    }
    
    /***
    * Method name    : displayErrorMessage
    * Description    : method used to display error messages.
    * Return Type    : None
    * Parameter      : Void
    */
    public void displayErrorMessage() {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Fatal, errorMessage));
    }
    
    /***
    * Method name    : convertToAlphabet
    * Description    : method used for converting Numeric characters to Alphabet Characters
    * Return Type    : String
    * Parameter      : integer intNumber
    */
    
    public string convertToAlphabet(integer intNumber) {
        String strConvertedAlphabet;
        map<String , Numeric_Alphabet_Mapping__c> mapNumericAlphabetMapping = Numeric_Alphabet_Mapping__c.getall(); // fetching data from custom setting
        if(mapNumericAlphabetMapping.ContainsKey(String.valueOf(intNumber))) {
            Numeric_Alphabet_Mapping__c objMapping = mapNumericAlphabetMapping.get(String.valueOf(intNumber));
            strConvertedAlphabet = objMapping.Alphabet_Value__c;
        }
        return strConvertedAlphabet;
    }
    
    /***
    * Method name    : updateWrapperListSequence
    * Description    : method used for updating sequence of wrapper list.
    * Return Type    : void
    * Parameter      : list<RuleCriteriaWrapper> lstCritWrapper
    */
    
    public void updateWrapperListSequence() {
        
        if(lstRuleCriteriaWrapper != Null && !lstRuleCriteriaWrapper.isEmpty()) {
            integer intCounter = 0; 
            for(RuleCriteriaWrapper objCritWrapper : lstRuleCriteriaWrapper) {
                intCounter++;
                objCritWrapper.strCriteriaNumber = convertToAlphabet(intCounter);
                intRowAddedToWrapper = intCounter;
            }
        }
    }
    
    /***
    * Method name    : populatePicklistOption
    * Description    : method used for populating picklist option if selected field is picklist type.
    * Return Type    : List<SelectOption>
    * Parameter      : string strSelectedField
    */
    
    public RuleCriteriaWrapper populatePicklistOption(string strObjName , String strFieldName , RuleCriteriaWrapper objWrapper) {
        list<selectOption> options = new list<selectOption>();
        options.add(new SelectOption('' , '--None--'));
        //List to store selected option in case of multi picklist.
        list<String> listSelectedOption = new list<String>();
        if(objWrapper.objRuleCriteriaWrap.Text_Value__c != Null) {
            listSelectedOption = objWrapper.objRuleCriteriaWrap.Text_Value__c.split(',');
        }
        system.debug('listSelectedOption'+listSelectedOption); 
        if(!String.isBlank(strObjName) && !String.isBlank(strFieldName)) {
            string strFieldType = string.valueof(UtilityClass.getFieldType(strObjName , strFieldName));
            if(strFieldType=='PICKLIST' || strFieldType=='MULTIPICKLIST') {
                List<Schema.PicklistEntry> lstPicklistEntry = Schema.getGlobalDescribe().get(strObjName).getDescribe().fields.getMap().get(strFieldName).getDescribe().getPicklistValues();
                //lstPicklistEntry.sort();
                for(Schema.PicklistEntry objPicklistEntry : lstPicklistEntry) {
                    options.add(new SelectOption(string.valueOf(objPicklistEntry.getLabel()) , string.valueOf(objPicklistEntry.getValue())));
                }
                if(!options.isEmpty()) {
                    options.sort();
                    objWrapper.listPicklistOptions = options;
                }
                if(strFieldType=='MULTIPICKLIST' && objWrapper.objRuleCriteriaWrap.Text_Value__c != Null) {
                    objWrapper.listSelectedMultiPicklistOptions = UtilityClass.convertStringtoArray(objWrapper.objRuleCriteriaWrap.Text_Value__c);
                }  
            }
        }
        return objWrapper;
    }
    
    /**********************************************************************************************
     * Name         : RuleCriteriaWrapper
     * Author       : Deloitte Consulting
     * Description  : This is a Wrapper class which is used for displaying data on visualforce page.
     *********************************************************************************************/
 
    public class RuleCriteriaWrapper implements Comparable {
        public string strCriteriaNumber {get;set;}
        public string strFieldName {get;set;}
        public string strOperator {get;set;}
        public list<SelectOption> lstOperators {get;set;}
        public Rule_Criterion__c objRuleCriteriaWrap {get;set;}
        public list<SelectOption> listPicklistOptions {get;set;}
        public list<String> listSelectedMultiPicklistOptions {get;set;}
        
        /***
        * Method name    : RuleCriteriaWrapper
        * Description    : Constructor of wrapper class
        * Return Type    : None
        * Parameter      : String selectedFieldName , String slectedOperator , list<SelectOption> lstOperatorOption , Rule_Criterion__c selectedRuleCriteria
        */
        public RuleCriteriaWrapper(String selectedCriteria, String selectedFieldName , String slectedOperator , list<SelectOption> lstOperatorOption , Rule_Criterion__c selectedRuleCriteria , list<SelectOption> lstPicklistOptions , list<String> lstSelectedMultiPicklistOptions) {
            strCriteriaNumber = selectedCriteria;
            strFieldName = selectedFieldName;
            lstOperators = lstOperatorOption;
            strOperator = slectedOperator; 
            objRuleCriteriaWrap = selectedRuleCriteria;
            listPicklistOptions = lstPicklistOptions;
            listSelectedMultiPicklistOptions = lstSelectedMultiPicklistOptions;
        }
        
        /***
        * Method name    : compareTo
        * Description    : Method used for making sort function available for wrapper list by implenting Comparable.
        * Return Type    : Integer
        * Parameter      : Object compareTo
        */
        
        public Integer compareTo(Object compareTo) 
        {
            RuleCriteriaWrapper RuleCriteriaWrapper = (RuleCriteriaWrapper) compareTo;
            if (strCriteriaNumber == RuleCriteriaWrapper.strCriteriaNumber) return 0;
            if (strCriteriaNumber > RuleCriteriaWrapper.strCriteriaNumber) return 1;
            return -1;        
        }
    }
}