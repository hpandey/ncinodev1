global without sharing class AccountDetailsVisibilityUtil{
    /******************************************************
    query to run to get accounts
    SELECT id, (Select id,ALL_NEEDED_FIELDS from restricted_client_information__r), (Select id,ALL_NEEDED_FIELDS from client_account_details__r), (select id,ALL_NEEDED_FIELDS from AccountTeamMembers) from Account where ... .... .....
    ********************************************************/
    
    private AccountDetailsVisibilityUtil(){
    
    }

    /***************************************************************************************************** 
    This webservice method will be called on a click of a button, allowing the sharing model to be executed 
    
    quick javascript to call this webservice from a button click:
    
    {!REQUIRESCRIPT("/soap/ajax/10.0/connection.js")}
    {!REQUIRESCRIPT("/soap/ajax/10.0/apex.js")}
    sforce.apex.execute("AccountDetailsVisibilityUtil","recalculateSharingOnSubmit", {accountId:"{!Account.Id}"});
    ************************************************************************************************/
    webservice static void recalculateSharingOnSubmit(Id accountId){
        AccountDetailsVisibilityUtil.recalculateSharing(accountID);
    }
    
    public static void recalculateSharing(Id accountID){
    
        /*****************************************************************************
        The following query will return the list of accounts and lists of related child records within the list of account;
        in this case, the child objects are restricted client information, client account details and AccountTeamMembers
        ******************************************************************************/
        Set<Id> relationShips = new Set<Id>();
        
        //Get the relationshipID for the current account.
        List<Account> curAccount = [Select id, OwnerID, CreatedByID,relationship__c from Account Where id =: accountID];
        //Pmartin - just call the recalculate sharing with this list and avoid calling Batch
        AccountDetailsVisibilityUtil.recalculateSharing(curAccount);
        if(curAccount != null && curAccount.size() > 0 && curAccount[0].Relationship__c != null) {
            relationShips.add(curAccount[0].Relationship__c);
        }
        RelationshipDetailsVisibilityUtil.recalculateSharing(curAccount[0].Relationship__c);
    }
   
   public static void recalculateUserSharing(List<Relationship__c> relationshipList){
    
        /*****************************************************************************
        The following query will return the list of accounts and lists of related child records within the list of account;
        in this case, the child objects are restricted client information, client account details and AccountTeamMembers
        ******************************************************************************/
        List<Account> accList = new List<Account>();
        List<Id> relIdList = new List<Id>();
        
        for (Relationship__c rel: relationshipList){
            relIdList.add(rel.Id);
        }
        
       /* modified by Sendeep 
         Modified Inner SOQL to Simple SOQL and Splitted the logic using for loops.
         separately Queried   restricted_client_information__c  , client_account_details__c, Call_Plan__C, AccountTeamMembers
         in Applysharing method line 123
       */
               
    /*    accList = [SELECT id, OwnerID, CreatedByID,Relationship__c                   
                    from Account 
                    where Relationship__c in :relIdList]; */
                 
                      
        accList = [SELECT id, OwnerID, CreatedByID,Relationship__c,
                    (Select id,Client__c from Restricted_Client_Information__r), 
                    (Select id,Client__c from Client_Account_Details__r), 
                    (Select id,Client__c from Call_Plan__r), 
                    (select id,UserId,AccountAccessLevel,AccountId from AccountTeamMembers) 
                    from Account 
                    where Relationship__c in :relIdList];
                  
          AccountDetailsVisibilityUtil.recalculateSharing(accList);
          RelationshipDetailsVisibilityUtil.recalculateSharing(relationshipList);
    }
    
    public static void recalculateSharing(List<Account> acctList){
        AccountDetailsVisibilityUtil util= new AccountDetailsVisibilityUtil();
        util.applySharing(acctList);
    }
    
    
    /*********************************************************************************************************************************
    The sole purpose of this code is to reorganize the records returned by the query into maps, making deletion and creation of Share records much easier 
    **********************************************************************************************************************************/
   
    public void applySharing(List<Account> acctList){
        
        /******************************************************************************************************************************************************
        The first map is equivalent to : {accountId => {bank_account_details__c.id => {userID => Bank_account_details__Share }}}
        The second map is equivalent to : {accountId => {bank_account_details__c.id => {userID => Restricted_client_information__Share }}}
        The third map is equivalent to : {accountId => {AccountTeamMember.UserId}}
        
        With this picture, we proceed this way:
             for each accountId key from the first map and for each userID key and Bank_account_details__share associated with it,
             if the userID is not in the value Set associated with the same accountID taken from the third map, the Bank_account_details__share should be deleted
             Conversly, for each accountID from the third map and for each userID from the value Set associated to that accountID , if the UserID is not in the userID keySet of the accountID entry taken from the first map, then
             a Bank_account_details__share has to be created (same should be done with the second map)
             These functions are implemented by the deleteShares and createShares methods
        ********************************************************************************************************************************************************/
        
        Map<Id,Map<Id,Map<Id,Bank_Account_Details__Share>>> acctID2badID2UserId2ShareMap = new Map<Id,Map<Id,Map<Id,Bank_Account_Details__Share>>>();
        Map<Id,Map<Id,Map<Id,Restricted_Client_Information__Share>>> acctID2rciID2UserId2ShareMap = new Map<Id,Map<Id,Map<Id,Restricted_Client_Information__Share>>>();   
        Map<Id,Map<Id,Map<Id,Call_Plan__Share>>> acctID2callplanID2UserId2ShareMap = new Map<Id,Map<Id,Map<Id,Call_Plan__Share>>>();
        Map<ID,Set<ID>> acctId2MbrsIDMap= new Map<ID,Set<Id>>();
        Set<ID> OwnersGroupMembersUserIDSet= new Set<ID>();
        Set<ID> acctOwnersUserIDSet= new Set<ID>();
        
        /*
        Temp lists for holding the records which need to be updated.
        */

        List<Restricted_Client_Information__c> tempRciList= new List<Restricted_Client_Information__c>();
        List<Bank_Account_Details__c> tempBadList= new List<Bank_Account_Details__c>();
        List<Relationship__c> RelationshipList = new List<Relationship__c>();
        List<Call_Plan__c> tempCpList = new List<Call_Plan__c>();
        
       // Modifed by sendeep 
       // Map with - Account Ids with List of records[]
              
        Map<Id,List<Restricted_Client_Information__c>> res_client_info_MAP = new Map<Id,List<Restricted_Client_Information__c>>();
        Map<Id,List<Bank_Account_Details__c>> Bank_Account_Details_MAP     = new Map<Id,List<Bank_Account_Details__c>>();        
        Map<Id,List<Call_Plan__c>> Call_Plans_MAP                          = new Map<Id,List<Call_Plan__c>>();          
        Map<Id,List<AccountTeamMember>> AccountTeamMembers_MAP             = new Map<Id,List<AccountTeamMember>>();        
        
                
        for(account oacc : acctList)
        {
            res_client_info_MAP.put(oacc.ID,new List<Restricted_Client_Information__c>()) ;
            Bank_Account_Details_MAP.put(oacc.Id,new List<Bank_Account_Details__c>());            
            Call_Plans_MAP.put(oacc.Id,new List<Call_Plan__c>());            
            AccountTeamMembers_MAP.put(oacc.Id,new List<AccountTeamMember>());            
        }
        
        for(Restricted_Client_Information__c rec :[SELECT id,Client__r.ID,Client__c  From Restricted_Client_Information__c WHERE  Client__r.ID  IN :acctList] )        
        {           
            res_client_info_MAP.get(rec.Client__r.ID).add(rec);         
        }        
        for(Bank_Account_Details__c bankAcc :[SELECT id,Client__r.ID,Client__c  From Bank_Account_Details__c WHERE  Client__r.ID  IN :acctList] )        
        {
            Bank_Account_Details_MAP.get(bankAcc.Client__r.ID).add(bankAcc);            
        }        
        for(Call_Plan__c callplan :[SELECT id,Client__r.ID,Client__c From Call_Plan__c  WHERE  Client__r.ID  IN :acctList] )        
        {
            Call_Plans_MAP.get(callplan.Client__r.ID).add(callplan);            
        }        
        for(AccountTeamMember accTeamMember :[SELECT id,UserId,AccountAccessLevel,AccountId FROM AccountTeamMember WHERE  AccountId IN :acctList] )        
        {
            AccountTeamMembers_MAP.get(accTeamMember.AccountId).add(accTeamMember);         
        }       
        
              
         
        //List of accounts under rel.id from -->>> "for (Relationship__c rel: relationshipList)"
        for(Account acc: acctList){
            acctOwnersUserIDSet.add(acc.OwnerId);

            if(res_client_info_MAP.get(acc.id) !=null) // null check                       
                tempRciList.addAll(res_client_info_MAP.get(acc.id));
            if(Bank_Account_Details_MAP.get(acc.id) !=null) // null check                                        
                tempBadList.addAll(Bank_Account_Details_MAP.get(acc.ID));
            if(Call_Plans_MAP.get(acc.id) !=null)  // null check                                  
                tempCpList.addAll(Call_Plans_MAP.get(acc.ID));
            
            acctId2MbrsIDMap.put(acc.Id,new Set<ID>{acc.OwnerID});
            if( AccountTeamMembers_MAP.get(acc.ID) != null) // null check
            for(AccountTeamMember atm: AccountTeamMembers_MAP.get(acc.ID)){
                acctId2MbrsIDMap.get(acc.Id).add(atm.UserId);
            }
            acctID2badID2UserId2ShareMap.put(acc.ID,new Map<Id,Map<Id,Bank_Account_Details__Share>>());
            acctID2rciID2UserId2ShareMap.put(acc.ID,new Map<Id,Map<Id,Restricted_Client_Information__Share>>());
            acctID2callplanID2UserId2ShareMap.put(acc.ID,new Map<Id,Map<Id,Call_Plan__Share>>());
            
            //for each Restricted Client Information record, put that record into the map under Account.id->Restricted_Client_Information__c.id
            if(res_client_info_MAP.get(acc.id) !=null)  // null check            
            for(Restricted_Client_Information__c rci : res_client_info_MAP.get(acc.id)){
                acctID2rciID2UserId2ShareMap.get(acc.ID).put(rci.ID, new Map<Id,Restricted_Client_Information__Share>());   
            }
            //for each Bank Account Details record, put that record into the map under Account.id->Bank_Account_Details__c.id
            if(Bank_Account_Details_MAP.get(acc.id) !=null)  // null check            
            for(Bank_Account_Details__c bad : Bank_Account_Details_MAP.get(acc.id)){
                acctID2badID2UserId2ShareMap.get(acc.ID).put(bad.ID, new Map<Id,Bank_Account_Details__Share>());
            }
            //for each Call Plan record, put that record into the map under Account.id->Call_Plan__c.id
            if(Call_Plans_MAP.get(acc.id) !=null) // null check           
            for(Call_Plan__c cp : Call_Plans_MAP.get(acc.id)){
                acctID2callplanID2UserId2ShareMap.get(acc.ID).put(cp.ID, new Map<Id,Call_Plan__Share>());
            }
        // modifed by sendeep 
        system.debug('***CROSSED modifeid logic in AccountDetailsVisibilityUtil CLass ***');             
        }
        /*
        For each current Restricted Client Information with the RowCause of ASR_Parent_Account_Team_Member__c populate the Restricted Client Information. This gives us
        a map of current shares to be removed so that the new members map from the Client Team can be added.
        */
        for(Restricted_Client_Information__Share rciShare : 
            [select id,ParentId,Parent.Client__c,UserOrGroupId 
                from Restricted_Client_Information__Share 
                    where ParentId in :temprciList and RowCause='ASR_Parent_Account_Team_Member__c']){
                    
            acctID2rciID2UserId2ShareMap.get(rciShare.Parent.client__c).get(rciShare.ParentId).put(rciShare.UserOrGroupId,rciShare);
        } 
         /*
        For each current Bank Account Detail with the RowCause of ASR_Parent_Account_Team_Member__c populate the Bank Account Details map. This gives us
        a map of current shares to be removed so that the new members map from the Client Team can be added.
        */
        for(Bank_Account_Details__Share badShare : 
            [select id,ParentId,Parent.Client__c,UserOrGroupId 
                from Bank_Account_Details__Share 
                    where ParentId in :tempbadList and RowCause='ASR_Parent_Account_Team_Member__c']){
                    
            acctID2badID2UserId2ShareMap.get(badShare.Parent.client__c).get(badShare.ParentId).put(badShare.UserOrGroupId,badShare);
        }
        /*
        For each current Call Plan with the RowCause of ASR_Parent_Account_Team_Member__c populate the Call Plan map. This gives us
        a map of current shares to be removed so that the new members map from the Client Team can be added.
        */
        for(Call_Plan__Share cpShare : 
            [select id,ParentId,Parent.Client__c,UserOrGroupId 
                from Call_Plan__Share 
                    where ParentId in :tempCpList and RowCause='ASR_Parent_Account_Team_Member__c']){
                    
            acctID2callplanID2UserId2ShareMap.get(cpShare.Parent.client__c).get(cpShare.ParentId).put(cpShare.UserOrGroupId,cpShare);
        }
        
        //Clean up all the previous shares
        deleteShares(acctID2callplanID2UserId2ShareMap,acctID2badID2UserId2ShareMap,acctID2rciID2UserId2ShareMap,acctId2MbrsIDMap);
        //Add the current shares into the table
        createShares(acctID2callplanID2UserId2ShareMap,acctID2badID2UserId2ShareMap,acctID2rciID2UserId2ShareMap,acctId2MbrsIDMap);
        //Refresh the sharing to the Relaitonship and all its children
        //    RelationshipDetailsVisibilityUtil.recalculateSharing(acctList[0].Relationship__c);
    }
    
    public void createShares(Map<Id,Map<Id,Map<Id,Call_Plan__Share>>> cpMap,Map<Id,Map<Id,Map<Id,Bank_Account_Details__Share>>> badMap, Map<Id,Map<Id,Map<Id,Restricted_Client_Information__Share>>> rciMap, Map<ID,Set<ID>> mbrsMap){
        /*
        Create lists to hold the shares to be created
        */
        List<Bank_Account_Details__Share> badSharesToCreate= new List<Bank_Account_Details__Share>();
        List<Restricted_Client_Information__Share> rciSharesToCreate= new List<Restricted_Client_Information__Share>();
        List<Call_Plan__Share> cpSharesToCreate= new List<Call_Plan__Share>();
        
        for(Id acctID : mbrsMap.keySet()){
            if(mbrsMap.get(acctID)!=null){
            for(Id userID : mbrsMap.get(acctID)){
                //If there are Bank Account Details in the map, populate the temporary Bank Account Details List with the shares from the Client Team using the RowCause of ASR_Parent_Account_Team_Member__c
                if(badMap.get(acctID)!=null){
                    for(Id badID: badMap.get(acctID).keySet()){
                        if(badMap.get(acctID).get(badID)== null || badMap.get(acctID).get(badID).get(userID)==null){
                            Bank_Account_Details__Share share= 
                            new Bank_Account_Details__Share(
                                AccessLevel='Edit', 
                                RowCause='ASR_Parent_Account_Team_Member__c',
                                ParentId=badID,
                                UserOrGroupId=userID );
                            badSharesToCreate.add(share); 
                        }
                    }
                }
                //If there are Restricted Client Information in the map, populate the temporary Restricted Client Information List with the shares from the Client Team using the RowCause of ASR_Parent_Account_Team_Member__c
                if(rciMap.get(acctID)!=null){
                    for(Id rciID: rciMap.get(acctID).keySet()){
                        if(rciMap.get(acctID).get(rciID)==null || rciMap.get(acctID).get(rciID).get(userID)==null){
                            Restricted_Client_Information__Share share= 
                            new Restricted_Client_Information__Share(
                                AccessLevel='Edit', 
                                RowCause='ASR_Parent_Account_Team_Member__c',
                                ParentId=rciID,
                                UserOrGroupId=userID );
                            rciSharesToCreate.add(share); 
                        }
                    }
                }
                //If there are Call Plan in the map, populate the temporary Call Plan List with the shares from the Client Team using the RowCause of ASR_Parent_Account_Team_Member__c
                if(cpMap.get(acctID)!=null){
                    for(Id cpID: cpMap.get(acctID).keySet()){
                        if(cpMap.get(acctID).get(cpID)==null || cpMap.get(acctID).get(cpID).get(userID)==null){
                            Call_Plan__Share share= 
                            new Call_Plan__Share(
                                AccessLevel='Edit', 
                                RowCause='ASR_Parent_Account_Team_Member__c',
                                ParentId=cpID,
                                UserOrGroupId=userID );
                            cpSharesToCreate.add(share); 
                        }
                    }
                }
            }
            }
        }
        System.debug('badSharesToCreate:'+badSharesToCreate.size());
        System.debug('rciSharesToCreate:'+rciSharesToCreate.size());
        System.debug('cpSharesToCreate:'+cpSharesToCreate.size());
        try{
            insert badSharesToCreate;
            insert rciSharesToCreate;
            insert cpSharesToCreate;
        }catch(DMLException e){
             System.debug('DMLException:'+e.getMessage());
        //This catch is here so that we fail softly. We shouldn't have many instances of failure on insert and it is next to impossible to reproduce. Adding code here will cause test coverage issues.
        }
    }
    //We need to look for a way to delete a single user
    public void deleteShares(Map<Id,Map<Id,Map<Id,Call_Plan__Share>>> cpMap,Map<Id,Map<Id,Map<Id,Bank_Account_Details__Share>>> badMap, Map<Id,Map<Id,Map<Id,Restricted_Client_Information__Share>>> rciMap, Map<ID,Set<ID>> mbrsMap){
        /*
        Temporary lists to hold the shares to delete
        */
        List<Bank_Account_Details__Share> badSharesToDelete= new List<Bank_Account_Details__Share>();
        List<Restricted_Client_Information__Share> rciSharesToDelete= new List<Restricted_Client_Information__Share>();
        List<Call_Plan__Share> cpSharesToDelete= new List<Call_Plan__Share>();
        
        //Build the list of Call Plan Shares to delete.
        for(Id acctID: cpMap.keySet()){
            for(Id cpID: cpMap.get(acctID).keySet()){
               for(Id userID: cpMap.get(acctID).get(cpID).keySet()){
                   if(mbrsMap.get(acctID)==null || !mbrsMap.get(acctID).contains(userID)){
                       cpSharesToDelete.add(cpMap.get(acctID).get(cpID).get(userID));
                   }
               } 
            }
        }       
        //Build the list of Bank Account Detail Shares to delete
        for(Id acctID: badMap.keySet()){
            for(Id badID: badMap.get(acctID).keySet()){
               for(Id userID: badMap.get(acctID).get(badID).keySet()){
                   if(mbrsMap.get(acctID)==null || !mbrsMap.get(acctID).contains(userID)){
                       badSharesToDelete.add(badMap.get(acctID).get(badID).get(userID));
                   }
               } 
            }
        }
        //Build the list of 
        for(Id acctID: rciMap.keySet()){
            for(Id rciID: rciMap.get(acctID).keySet()){
               for(Id userID: rciMap.get(acctID).get(rciID).keySet()){
                   if(mbrsMap.get(acctID)==null || !mbrsMap.get(acctID).contains(userID)){
                       rciSharesToDelete.add(rciMap.get(acctID).get(rciID).get(userID));
                   }
               } 
            }
        }
         System.debug('badSharesToDelete:'+badSharesToDelete.size());
        System.debug('rciSharesToDelete:'+rciSharesToDelete.size());
        System.debug('cpSharesToDelete:'+cpSharesToDelete.size());
        try{
            delete badSharesToDelete;
            delete rciSharesToDelete;
            delete cpSharesToDelete;
        }catch(DMLException e){
             System.debug('DMLException:'+e.getMessage());
        //This catch is here so that we fail softly. We shouldn't have many instances of failure on insert and it is next to impossible to reproduce. Adding code here will cause test coverage issues.
        }
    }
    
}