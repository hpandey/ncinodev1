/************************************************************************************
 Apex Class Name     : RequestGeneratorAsyncProcess
 Version             : 1.0
 Created Date        : 4rd March 2016
 Function            : Generate Request body according to Object and Integartion System .
 Author              : Sumit Sagar 
 Modification Log    :
* Developer                         Date                   Description
* ----------------------------------------------------------------------------                 
* Sumit Sagar                     3/4/2016                Original Version
*************************************************************************************/
public class RequestGeneratorAsyncProcess {
    
    
    /************************************************************************
    Method Name: accountRequestGenerate
    Author Name: Sumit Sagar
    Description: Generate http request body for Account Object.
    Parameters: Account objAccount 
    Returns: String
    *************************************************************************/
    public string accountRequestGenerate(Account objAccount) {
        String strRequestBody;
        if(objAccount != Null) {
            JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                    gen.writeStringField('Id', objAccount.id);
                    gen.writeStringField('Name', objAccount.Name);
                gen.writeEndObject();

            strRequestBody = gen.getAsString();
        }
        return strRequestBody;
        
        /*httpRequest request = new httpRequest();
        string objAPIName = objAccount.id.getSObjectType().getDescribe().getName();
        Async_Process_Log_Setting__c objAsyncProcessLogSetting;
        
        if(mapAsyncProcessCustomSetting.ContainsKey(objAPIName)) {
            objAsyncProcessLogSetting = mapAsyncProcessCustomSetting.get(objAPIName);
            request.
            
        }
        return request;*/
    }
}