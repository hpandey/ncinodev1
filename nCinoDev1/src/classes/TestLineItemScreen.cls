@isTest(SeeAllData=true)
private class TestLineItemScreen{

static testMethod void testCase(){
Test.startTest();


List <Trigger_Setting__c> ts = [Select Id, Name, isActive__c from Trigger_Setting__c Where Name='Opportunity'];
if (ts.size()>0){
    ts[0].isActive__c=false;
    update ts[0];
}
else {
    Trigger_Setting__c ts1 = new Trigger_Setting__c();
    ts1.Name='Opportunity';
    ts1.isActive__c=false;
    insert ts1;
}

Account acc = new Account(Name = 'Test Account');
insert acc;
Opportunity testOpty= new Opportunity(Name='Test Opty', CloseDate=date.today(), StageName = 'Prospecting', Account=acc);
insert testOpty;

Product2 prod = new Product2(Name = 'Test Product', Family='Capital Markets');
Pricebook2 pbook = new Pricebook2(Name = 'Test Pricebook', isActive=true);
insert prod;
insert pbook;
Pricebook2 sbook = [Select Id,Name from pricebook2 where isStandard=true];

PriceBookEntry sbe = new PriceBookEntry ( Pricebook2Id=sbook.id, Product2Id=prod.id,UnitPrice=1, IsActive=true);
insert sbe;
PriceBookEntry pbe = new PriceBookEntry ( Pricebook2Id=pbook.id, Product2Id=prod.id,UnitPrice=20, IsActive=true);
insert pbe;

OpportunityLineItem oppLine = new OpportunityLineItem( PricebookEntryId=pbe.Id, OpportunityId=testOpty.Id, 
                                                      Estimated_Revenue__c=20,
                                                      Quantity=1, unitPrice=1);
insert oppLine;
ApexPages.StandardController sc = new ApexPages.StandardController(oppLine);

// create an instance of the controller
LineItemController PageCon = new LineItemController(sc);

//Test converage for the visualforce page
PageReference pageRef = Page.LineItemScreen;
pageRef.getParameters().put('id', String.valueOf(OpportunityLineItem.Id));
Test.setCurrentPageReference(pageRef);

PageCon.getSection();
PageCon.quickSave();
PageCon.saveProducts();
PageCon.cancel();

Test.stopTest();
}
}