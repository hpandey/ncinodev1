/**********************************************************************************************
 * Name         : OutboundIntegrationInterface
 * Author       : Deloitte Consulting
 * Description  : This is an interface for all outbound integration service classes 
 *********************************************************************************************/

public interface OutboundIntegrationInterface {

    String getRequestBody(String strIds); // method to prepare requestbody
    HttpResponse doCallOut();   // method to make call out to external system
    String getResponse(); // method to process response from external system and return result
    sObject finish(); //method to backUpdate in source Object. 
    void executeIntegration(Asynchronous_Queue__c objAsyncQueueRecord);
    HttpResponse getHTTPResponse();
    HttpRequest getHTTPRequest();
}