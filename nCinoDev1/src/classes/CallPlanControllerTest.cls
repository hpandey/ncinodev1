@isTest
public class CallPlanControllerTest {
    
    static testmethod void testSelectOptions() {
        CallPlanController cpc = new CallPlanController();
        System.Assert(cpc.userRoles.size() > 0);
        System.Assert(cpc.contactRoles.size() > 0);
        System.Assert(cpc.callPlanTypeOptions.size() > 0);
        System.Assert(cpc.callPlanCategoryOptions.size() > 0);
        System.Assert(cpc.callPlanStatusOptions.size() > 0);
        System.Assert(cpc.callPlanPrimaryRiskOptions.size() > 0);
        System.Assert(cpc.callPlanSecondaryRiskOptions.size() > 0);
        System.Assert(cpc.callPlanJointCallOptions.size() > 0);
        System.Assert(cpc.callPlanEmergingRiskIdentifiedOptions.size() > 0);
    }
    
    static testmethod void testUsers() {
        setupUser();
        CallPlanController cpc = new CallPlanController();
        System.Assert(!String.isBlank(cpc.taskRecordType));
        System.Assert(cpc.isAdmin || !cpc.isAdmin);
    }
    
    static testmethod void populateClient() {
        Account client = new Account(Name='TestClient');
        insert client;
        
        CallPlanController cpc = new CallPlanController('', client.id,'','');
        System.assertNotEquals(cpc.SFUrl + '/' + client.Id, cpc.referrerUrl);
    }
    
    static testmethod void populateContact() {
        Account client = new Account(Name='TestClient');
        insert client;

        Contact con = new Contact(FirstName='Test',LastName='User',Email='testuser@salesforce.com',AccountId=client.id);
        insert con;
        
        CallPlanController cpc = new CallPlanController('', '', con.id,'');
        System.assertNotEquals(cpc.SFUrl + '/' + con.Id, cpc.referrerUrl);
    }
    
    private static Call_Plan__c setupCallPlan() {
        Account client = new Account(Name='TestClient');
        insert client;

        Contact con = new Contact(FirstName='Test',LastName='User',Email='testuser@salesforce.com',AccountId=client.id);
        insert con;
        
        CallPlanController.CallPlanUI cpUI = new CallPlanController.CallPlanUI();
        cpUI.Call_Plan_Type = 'Face to Face';
        cpUI.Emerging_Risk = 'No';
        cpUI.Client = client.Id;
        cpUI.Contact = con.Id;
        cpUI.Joint_Call2 = 'No';
        cpUI.Status = 'Planning';
        cpUI.Target_Date = '11/1/2014';
        cpUI.Meeting_Category = '';
        cpUI.Completed_Date = '';
        cpUI.Objectives = '';
        cpUI.Primary_Emerging_Risk = '';
        cpUI.Secondary_E = '';
        cpUI.Describe_Emerging_Risk = '';
        cpUI.Risk = '';
        cpUI.Other_Risk_Factors_Discussed = '';
        cpUI.Call_Result = '';
        cpUI.Next_Steps = '';
        
        Call_Plan__c cp = CallPlanController.savePlan(cpUI);
        System.Assert(!string.isBlank(cp.Id));
        System.Assert(!string.isBlank(cp.Name));
        
        cpUI.id = cp.Id;
        Call_Plan__c cp2 = CallPlanController.savePlan(cpUI);
        System.assertEquals(cp.Id, cp2.Id);
        
        return cp;
    }
    
    static testmethod void callPlanOperations() {
        
        Account acct= new Account(name='testAccount');
        insert acct;
        
        Profile p = [select id from Profile where name='Standard User'];
        User testUser= new User(Username='testtest@testingtestng.com', LastName='test', Email='test@test.com', Alias='tes', CommunityNickname='tes',ProfileID=p.ID,TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', LanguageLocaleKey='en_US',EmailEncodingKey='ISO-8859-1');
        
        insert testUser;
        
        Call_Plan__c cp = setupCallPlan();
        List<CallPlanController.UserUI> users = new List<CallPlanController.UserUI>();
        CallPlanController.UserUI user = new CallPlanController.UserUI();
        user.email = 'test@testing.com';
        user.name = 'Test Test';
        user.userid = testUser.Id;
        users.add(user);
        
        List<Database.UpsertResult> results = CallPlanController.saveNotify(cp.Id, users);
        System.Assert(!string.isBlank(results[0].id));
        user.id = results[0].id;
        
        List<Database.UpsertResult> results2 = CallPlanController.saveNotify(cp.Id, users);
        System.assertEquals(user.id, results2[0].id);
        
        CallPlanController cpc = new CallPlanController(cp.Id, '', '', 'edit');
        cpc.getClientContacts();
        
        User testUser2= new User(Username='newtest007@newtest.com', LastName='newtest007', Email='newtest007@newtest.com', Alias='test007', CommunityNickname='newtest',ProfileID=p.ID,TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', LanguageLocaleKey='en_US',EmailEncodingKey='ISO-8859-1');
        
        insert testUser2;
        
        AccountTeamMember atm= new AccountTeamMember(accountId=cpc.accountId,userId=testUser2.id);
        insert atm;
        cpc.getClientTeamMembers();
        
        Contact con = new Contact(FirstName='Test',LastName='User',Email='testuser@salesforce.com',AccountId=cpc.accountId);
        insert con;
        
        List<CallPlanController.ParticipantUI> parts = new List<CallPlanController.ParticipantUI>();
        CallPlanController.ParticipantUI part = new CallPlanController.ParticipantUI();
        part.callPlanId = cp.Id;
        part.email = 'test@test.com';
        part.name = 'Testy McTesterson';
        part.notify = true;
        part.type = 'Contact';
        part.userid = con.id;
        part.businessSegment = false;
        parts.add(part);
        
        part = new CallPlanController.ParticipantUI();
        part.callPlanId = cp.Id;
        part.email = 'test@test.com';
        part.name = 'Testy McTesterson';
        part.notify = true;
        part.type = 'OneView';
        part.userid = testUser.id;
        part.businessSegment = false;
        parts.add(part);
        
        part = new CallPlanController.ParticipantUI();
        part.callPlanId = cp.Id;
        part.email = 'test@test.com';
        part.name = 'Testy McTesterson';
        part.notify = true;
        part.businessSegment = false;
        part.type = 'Non OneView';
        parts.add(part);
        
        List<CallPlanController.ParticipantUI> resultParts = CallPlanController.addParticipants(parts);
        CallPlanController.addNotify(parts);
        //CallPlanController.updateRoles(resultParts);
        CallPlanController.removeParticipant(resultParts.get(1).Id, testUser.Id);
            
        cpc.getParticipants();
    }
    
    private static void setupUser() {
        Account acct= new Account(name='testAccount');
        insert acct;
        
        Profile p = [select id from Profile where name='Standard User'];
        User testUser= new User(Username='testtest@testingtestng.com', LastName='test', Email='test@test.com', Alias='tes', CommunityNickname='tes',ProfileID=p.ID,TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US', LanguageLocaleKey='en_US',EmailEncodingKey='ISO-8859-1');
        
        insert testUser;
        
        AccountTeamMember atm= new AccountTeamMember(accountId=acct.id,userId=testUser.id);
        insert atm;
        
        Contact con = new Contact(FirstName='Test',LastName='User',Email='testuser@salesforce.com',AccountId=acct.id);
        insert con;
    }
}