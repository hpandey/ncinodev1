public Class CreditRetrievalPageExt{
    
    public String strAccId;
    public Account accRec{get;set;}
    public List<LLC_BI__Loan__c> listLoan;
    public List<LLC_BI__Legal_Entities__c> listEntities;
    public List<Regulatory_and_Fraud_Indicators__c> listPrescreeningResults{get;set;}
    public Boolean psrErrorFlag{get;set;}
    public Boolean callOutSuccess{get;set;}
    public List<loanDetailsWrapper> loanDetailsWrap{get;set;}
    public String strCurrentBureau{get;set;}
    public String strPin{get;set;}
    public List<SelectOption> bureauOptions{get;set;}
    public String response{get;set;}
    private String returnedContinuationId;
    
    
    public CreditRetrievalPageExt(ApexPages.StandardController controller){
        strAccId = ApexPages.currentPage().getParameters().get('Id');
        psrErrorFlag = false;
        callOutSuccess = false;
        if(strAccId != null && strAccId != ''){    
            listLoan = new List<LLC_BI__Loan__c>();
            listEntities = new List<LLC_BI__Legal_Entities__c>();
            getBureauOptions();
            init(strAccId);
        }
    }
    
    public void init(String strAccId){
        
        List<String> listResult = new List<String>();
        ID strOOMRecType = Schema.SObjectType.Regulatory_and_Fraud_Indicators__c.getRecordTypeInfosByName().get(Label.Out_of_Market).getRecordTypeId();
        ID strVermontRecType = Schema.SObjectType.Regulatory_and_Fraud_Indicators__c.getRecordTypeInfosByName().get(Label.Vermont).getRecordTypeId();
        List<Id> listRecType = new List<Id>();
        
        if(strAccId != null && strAccId != ''){
            accRec = [SELECT Id, Name
                      FROM Account 
                      WHERE Id = :strAccId];
            system.debug('##### AccRec : '+accRec);
            //Add appropriate result in list
            listResult.add(Label.Passed);
            listResult.add(Label.Rejected);
            //Add Record Type in List
            listRecType.add(strOOMRecType);
            listRecType.add(strVermontRecType);
            if(accRec != null){
                listEntities = [SELECT Id, LLC_BI__Borrower_Type__c, LLC_BI__Loan__r.Name, LLC_BI__Loan__r.LLC_BI__Stage__c, Name, 
                                       (SELECT Id, Entity__r.Name, Flag_Type__c, Name, Result__c, Loan__r.Name
                                        FROM Prescreening_Results__r
                                        WHERE Result__c NOT IN :listResult
                                          AND RecordTypeId IN :listRecType) 
                                FROM LLC_BI__Legal_Entities__c
                                WHERE LLC_BI__Account__c = :strAccId];
                                
                if(!listEntities.isEmpty()){
                    listPrescreeningResults = new List<Regulatory_and_Fraud_Indicators__c>();
                    loanDetailsWrap = new List<loanDetailsWrapper>();
                    for(LLC_BI__Legal_Entities__c entityRec : listEntities){
                        if(!entityRec.Prescreening_Results__r.isEmpty()){
                            listPrescreeningResults.addAll(entityRec.Prescreening_Results__r);
                        }
                        loanDetailsWrap.add(new loanDetailsWrapper(false,entityRec));
                    }
                    system.debug('##### listPrescreeningResults : '+listPrescreeningResults);
                    if(listPrescreeningResults != null && !listPrescreeningResults.isEmpty()){
                        system.debug('##### In if PSR');
                        psrErrorFlag = true;
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Credit report cannot be pulled because one or more flags are open.'));
                    }
                    else{
                        //get the existing credit report.
                        //getExistingCreditReport();
                    }
                }
            }
        }
    }
    
    /**public PageReference retrieveCredit(){
        //To add web service callout from continuation class.
        callOutSuccess = true;
        psrErrorFlag = false;
        for(loanDetailsWrapper wrap : loanDetailsWrap){
            if(wrap.isSelected){
                System.debug('#####wrap'+wrap);
                //Do Callout.
                doCallout();
            }
        }
        
        return null;
    }**/
    
    public void getBureauOptions(){
        bureauOptions = new List<SelectOption>();
        for(Integer i=1;i<5;i++){
            bureauOptions.add(new SelectOption('Test '+String.valueOf(i), 'Test '+String.valueOf(i)));
        }
    }
    
    public void getExistingCreditReport(){
    //Code to be added to get the credit report which is less than 15 days old.
    }
    
    public object doCallout(){
        callOutSuccess = true;
        psrErrorFlag = false;

        Continuation con = new Continuation(10);
        con.continuationMethod='processResponse';
        
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint('http://www.mocky.io/v2/570c9f9f1100007727d17ee5');
        system.debug('##### req :'+req);
        returnedContinuationId = con.addHttpRequest(req);
        
        return con;
    }
    
    public object processResponse(){

        HttpResponse httpRes = Continuation.getResponse(returnedContinuationId);
        response = httpRes.getBody();
        system.debug('#####response : '+response);
        return null;
    }
    
    public Class loanDetailsWrapper{
        public Boolean isSelected{get;set;}
        public LLC_BI__Legal_Entities__c entityRec{get;set;}
        
        public loanDetailsWrapper(Boolean isSelected, LLC_BI__Legal_Entities__c entityRec){
            this.isSelected = isSelected;
            this.entityRec = entityRec;
        }
    }
}