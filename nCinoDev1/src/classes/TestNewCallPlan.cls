/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestNewCallPlan {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
    PageReference pageRef = Page.NewCallPlan;
    Test.setCurrentPage(pageRef);     
	
            Account account = new Account(Name='Test Account');
            insert account;      
            // Access the account that was just created. 
    
            Account insertedAcct = [SELECT Id,Name FROM Account WHERE Name='Test Account'];
            
            //Add contact as the owner... This is expected to pass with colors that fly :)
            Contact contact = new Contact();
            contact.FirstName = 'Test';
            contact.LastName = 'ContactA';
            contact.AccountId = insertedAcct.Id;
            insert contact;
	// has required fields
    Call_Plan__c callplan = new Call_Plan__c(Client__c=account.id, Contact__c=contact.id, Status__c='Planning', Joint_Call2__c='No', Call_Plan_Type__c='Face to Face' );
    insert callplan;
        
    ApexPages.standardController controller = new ApexPages.standardController(callplan);
             
    NewCallPlan pag = new NewCallPlan(controller); 
              
    pag.save();
    // doesn't have all required fields.
    Call_Plan__c callplan2 = new Call_Plan__c(Client__c=account.id, Contact__c=contact.id, Status__c='Planning', Joint_Call2__c='No', Call_Plan_Type__c='Face to Face' );
    insert callplan2;
        
    ApexPages.standardController controller2 = new ApexPages.standardController(callplan2);
             
    NewCallPlan pag2 = new NewCallPlan(controller2); 
              
    try{
         pag2.save();
    }catch(Exception e){
          System.debug('Your a big error(B): ' + e.getMessage());
          System.assert(e.getMessage().contains('Completed Date: If you are completing a Call Plan, the Completed Date must filled in and not after today')); 
    }
       
    }
}