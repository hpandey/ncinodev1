/************************************************************************************
 Apex Class Name     : HTTPFrameworkTest
 Version             : 1.0
 Created Date        : 12th Feb 2016
 Function            : Test Class for HTTPFramework Class.
 Author              : Pranil Thubrikar 
 Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar         2/12/2016                Original Version
*************************************************************************************/


@isTest(seealldata = false)
public Class HTTPFrameworkTest{
    
    public static testMethod void testMethod1(){
        
        //Setting up the variables.
        String mockEndPoint = 'https://www.mockEndPoint.com';
        String mockMethod = 'GET';
        Integer mockTimeOut = 400;
        Map<String,String> mockHeaderMap = new Map<String,String>{'key1'=>'value1','key2'=>'value2'};
        String mockBody = 'Mock Body';
        Boolean mockCompression = true;
        String mockClientCertificateName = 'Client Certificate Name';
        
        //Setting the Mock Response for Test Class
        Test.setMock(HttpCalloutMock.class, new HTTPFrameworkMock(false));
        
        Test.startTest();//Start Test
        
        //Object for HTTPFramework
        HTTPFramework classObj = new HTTPFramework();
        
        classObj.setEndpoint(mockEndPoint);
        classObj.setMethod(mockMethod);
        classObj.setTimeout(mockTimeOut);
        classObj.setHeader(mockHeaderMap.values()[0],mockHeaderMap.values()[1]);
        classObj.setMultipleHeaders(mockHeaderMap);
        classObj.setBody(mockBody);
        classObj.setCompression(mockCompression);
        //classObj.setClientCertificateName(mockClientCertificateName);
        classObj.disableLogging = false;

        HttpResponse response_1 = classObj.performCallout();
        System.Assert(response_1 != null);
        
        //Setting the Mock Exception for Test Class.
        try{
            Test.setMock(HttpCalloutMock.class, new HTTPFrameworkMock(true));
            HTTPFramework newClassObj = new HTTPFramework();
            HttpResponse response_2 = newClassObj.performCallout();
            System.Assert(response_2 == null);
        }
        catch(exception e){}
        
        Test.stopTest();//Stop Test
    }
}