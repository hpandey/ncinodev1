public class continuationClassHandler {

    // Unique label corresponding to the continuation
    public String requestLabel;
    // Endpoint of long-running service
    private static final String LONG_RUNNING_SERVICE_URL = 'http://www.mocky.io/v2/5185415ba171ea3a00704eed';
    // Result of callout
    public String result {get;set;}
    
    
    public void hitMultipleRequest() {
        for(integer i = 0; i < 10 ; i++) {
            startRequest();
        }
    }
    
    /*Action Method*/
    public object startRequest() {
        system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
        // Create continuation with a timeout
        Continuation con = new Continuation(10);
        
        // Set callback method
        con.continuationMethod='processResponse';
        
        // Create callout request
        HttpRequest req = new HttpRequest();
        req.setMethod('Post');
        req.setEndpoint(LONG_RUNNING_SERVICE_URL);
        req.setbody('Killer Frost');
        
        // Add callout request to continuation
        this.requestLabel = con.addHttpRequest(req);
        /*Http http = new Http();
        
        HttpResponse res = new HttpResponse();
        res = http.send(req);*/
        
        system.debug('$$$$$$$$$$$$$$$$$$$$$$$$');
        system.debug('########################'+requestLabel);
        // Return the continuation
        return con; 
    }
    
    /*CallBack Method*/
    public object processResponse() {
        
        // Get the response by using the unique label
        system.debug('&&&&&&&&&&&&&&&&&&&&&&&');
        HttpResponse response = Continuation.getResponse(this.requestLabel);
        
        // Set the result variable that is displayed on the Visualforce page
                this.result = this.result + response.getBody() + response.getstatusCode();

        
        // Return null to re-render the original Visualforce page
        return null;
    
    }
}