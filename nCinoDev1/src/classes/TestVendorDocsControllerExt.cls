@isTest
/********************************************************
* Name  : TestVendorDocsControllerExt.cls
* Author    : Jenna Werner
* Desc  : Test class for controller Extension
            (VendorDocsControllerExt.cls)
* Modification Log:
---------------------------------------------------------
Developer               Date                Description
---------------------------------------------------------
Jenna Werner            02/29/2016      Original Version
********************************************************/
private class TestVendorDocsControllerExt {

        static LLC_BI__Loan__c testLoan1 = new LLC_BI__Loan__c();
        static {
            testLoan1 = TestDataFactory.createLoan();
        }

/* Method Name: testButtonActions_positive
   Author Name: Jenna Werner
   Description: Tests positive scenario that the
                    three buttons (save, cancel, 
                    and refresh) all work
   Parameters: none
*/       
    @isTest static void testButtonActions_positive() {

        Test.startTest();

        ApexPages.StandardController stdLoan = new ApexPages.StandardController(testLoan1);
        VendorDocsControllerExt controllerExtInstance = new VendorDocsControllerExt(stdLoan);
        Test.setCurrentPage(Page.TrackingVendorDocuments);
        
        controllerExtInstance.save();
        controllerExtInstance.cancel();
        controllerExtInstance.refresh();

        Test.stopTest();
    }

/* Method Name: testCreateLoanDocs_positive
   Author Name: Jenna Werner
   Description: Tests positive scenario that a
                    list of loan documents
                    is created and presented
                    in the Visualforce page
   Parameters: none
*/  
    @isTest static void testCreateLoanDocs_postive() {

        TestDataFactory.createVendorDocColumns();       

        List<LLC_BI__LLC_LoanDocument__c> listTestLoanDocuments = new List<LLC_BI__LLC_LoanDocument__c>();
        listTestLoanDocuments = TestDataFactory.createLoanDocuments(testLoan1, 10);

        Test.startTest();

        ApexPages.StandardController stdLoan = new ApexPages.StandardController(testLoan1);
        VendorDocsControllerExt controllerExtInstance = new VendorDocsControllerExt(stdLoan);
        Test.setCurrentPage(Page.TrackingVendorDocuments);
        
        controllerExtInstance.save();

        Test.stopTest();
        
        System.assert(controllerExtInstance.listLoanDocument.size() == 10);

        for (Integer i = 0; i < controllerExtInstance.listLoanDocument.size(); i++) {
            System.assertEquals(controllerExtInstance.listLoanDocument.get(i).Name, listTestLoanDocuments.get(i).Name);
        }
    }

/* Method Name: testCreateAndUpdateLoanDocs_positive
   Author Name: Jenna Werner
   Description: Tests positive scenario that a
                    list of loan documents
                    is created and presented
                    in the Visualforce page,
                    and that updates to them
                    will save appropriately
   Parameters: none
*/  
    @isTest static void testCreateAndUpdateLoanDocs_positive() {

        TestDataFactory.createVendorDocColumns();       

        List<LLC_BI__LLC_LoanDocument__c> listTestLoanDocuments = new List<LLC_BI__LLC_LoanDocument__c>();
        listTestLoanDocuments = TestDataFactory.createLoanDocuments(testLoan1, 10);

        Test.startTest();

        ApexPages.StandardController stdLoan = new ApexPages.StandardController(testLoan1);
        VendorDocsControllerExt controllerExtInstance = new VendorDocsControllerExt(stdLoan);
        Test.setCurrentPage(Page.TrackingVendorDocuments);
        
        controllerExtInstance.save();
        
        // Update the date of the first loan document in the list to be the
        // day after the original date, and save the loan document
        Date initialDate;
        initialDate = controllerExtInstance.listLoanDocument[0].Date_Received__c;
        controllerExtInstance.listLoanDocument[0].Date_Received__c = initialDate + 1;
        controllerExtInstance.save();

        Test.stopTest();

        System.assertEquals(controllerExtInstance.listLoanDocument[0].Date_Received__c, (initialDate + 1), 'not equal');
    }

/* Method Name: partialErrorOnUpdate_negative
   Author Name: Jenna Werner
   Description: Tests positive scenario that the
                    three buttons (save, cancel, 
                    and refresh) all work
   Parameters: none
*/  
    @isTest static void partialErrorOnUpdate_negative() {

        TestDataFactory.createVendorDocColumns();       

        List<LLC_BI__LLC_LoanDocument__c> listTestLoanDocuments = new List<LLC_BI__LLC_LoanDocument__c>();
        listTestLoanDocuments = TestDataFactory.createLoanDocuments(testLoan1, 10);
        Test.startTest();

        ApexPages.StandardController stdLoan = new ApexPages.StandardController(testLoan1);
        VendorDocsControllerExt controllerExtInstance = new VendorDocsControllerExt(stdLoan);
        Test.setCurrentPage(Page.TrackingVendorDocuments);
        delete testLoan1;
        controllerExtInstance.listLoanDocument[0].Date_Received__c = controllerExtInstance.listLoanDocument[0].Date_Received__c + 1;
        controllerExtInstance.save();
        Test.stopTest();
        System.assert(ApexPages.getMessages().size()>0);
        System.assert(ApexPages.getMessages()[0].getDetail().contains(System.Label.Vendor_Doc_Table_Display_Error));
    }


/* Method Name: testDuplicateLoanDocs_positive
   Author Name: Jenna Werner
   Description: Tests positive scenario that the
                    three buttons (save, cancel, 
                    and refresh) all work
   Parameters: none
*/  
    @isTest static void testDuplicateLoanDocs_positive() {

        TestDataFactory.createVendorDocColumns();       

        List<LLC_BI__LLC_LoanDocument__c> listTestLoanDocuments = new List<LLC_BI__LLC_LoanDocument__c>();
        listTestLoanDocuments = TestDataFactory.createLoanDocumentsWithCreatedDatesSet(testLoan1, 5);

        Test.startTest();

        ApexPages.StandardController stdLoan = new ApexPages.StandardController(testLoan1);
        VendorDocsControllerExt controllerExtInstance = new VendorDocsControllerExt(stdLoan);
        Test.setCurrentPage(Page.TrackingVendorDocuments);
        

        controllerExtInstance.save();

        Test.stopTest();
        
        System.assert(controllerExtInstance.listLoanDocument.size() == 1);
        System.assertEquals(controllerExtInstance.listLoanDocument[0].ID, listTestLoanDocuments[4].ID, 'not equal');
    }

/* Method Name: testInvalidQueryCustomSetting_negative
   Author Name: Jenna Werner
   Description: Tests negative scenario that custom
                    setting contains field names
                    that are not fields on Loan
                    Document object
   Parameters: none
*/  
    @isTest static void testInvalidQueryCustomSetting_negative() {
    
        TestDataFactory.createVendorDocColumns_throwException();  

        List<LLC_BI__LLC_LoanDocument__c> listTestLoanDocuments = new List<LLC_BI__LLC_LoanDocument__c>();
        listTestLoanDocuments = TestDataFactory.createLoanDocumentsWithoutDates(testLoan1, 10);

        Test.startTest();

        ApexPages.StandardController stdLoan = new ApexPages.StandardController(testLoan1);
        VendorDocsControllerExt controllerExtInstance = new VendorDocsControllerExt(stdLoan);
        Test.setCurrentPage(Page.TrackingVendorDocuments);
        
        controllerExtInstance.save();

        Test.stopTest();
        
        //System.assert(ApexPages.getMessages()[0].getDetail().contains(System.Label.Vendor_Doc_Table_Display_Error));
    }

	/*Method Name: testLoanDocsWithoutIds_negative
	   Author Name: Jenna Werner
	   Description: Tests negative scenario that
	                loan documents are created
	                without IDs
	   Parameters: none
	*/  
    @isTest static void testLoanDocsWithoutIds_negative() {

        TestDataFactory.createVendorDocColumns();       

        List<LLC_BI__LLC_LoanDocument__c> listTestLoanDocuments = new List<LLC_BI__LLC_LoanDocument__c>();
        listTestLoanDocuments = TestDataFactory.createLoanDocumentsWithoutIDs(testLoan1, 10);

        Test.startTest();

        ApexPages.StandardController stdLoan = new ApexPages.StandardController(testLoan1);
        VendorDocsControllerExt controllerExtInstance = new VendorDocsControllerExt(stdLoan);
        Test.setCurrentPage(Page.TrackingVendorDocuments);
        
        controllerExtInstance.save();

        Test.stopTest();
        
        System.assert(controllerExtInstance.listLoanDocument.size() == 0);
    }
    
/* Method Name: testExceptionDuringUpdate_negative
   Author Name: Jenna Werner
   Description: 
   Parameters: none
*/  
    @isTest static void testExceptionDuringUpdate_negative() {
        
        TestDataFactory.createVendorDocColumns();  

        List<LLC_BI__LLC_LoanDocument__c> listTestLoanDocuments = new List<LLC_BI__LLC_LoanDocument__c>();
        listTestLoanDocuments = TestDataFactory.createLoanDocuments(testLoan1, 10);

        Test.startTest();

        ApexPages.StandardController stdLoan = new ApexPages.StandardController(testLoan1);
        VendorDocsControllerExt controllerExtInstance = new VendorDocsControllerExt(stdLoan);
        Test.setCurrentPage(Page.TrackingVendorDocuments);
        
        controllerExtInstance.save();
        
        controllerExtInstance.listLoanDocument = null;
        
        controllerExtInstance.save();

        Test.stopTest();
        
        System.assert(ApexPages.getMessages()[0].getDetail().contains(System.Label.Vendor_Doc_Table_Display_Error));
    }
}