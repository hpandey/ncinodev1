//Generated by wsdl2apex

public class AccountBalance {
    public class BasicHttpBinding_ISalesForceAccountProfile {
        //public String endpoint_x = 'https://sfdc.regionstest.com/SalesForce/SalesForceAccountProfile.svc';
        public String endpoint_x = RegionsConnectivity__c.getInstance('AccountBalance_WS').RegionsURL__c;
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://tempuri.org/', 'AccountBalance', 'http://schemas.microsoft.com/2003/10/Serialization/Arrays', 'AccountBalanceArrays', 'http://tempuri.org/Imports', 'AccountBalanceImports'};
        public AccountBalanceArrays.ArrayOfKeyValueOfstringstring FetchSalesForceAccountProfile(String UserID,String Password,String AccountNumber,String ApplicationCode,String OperatorID,String CustomerBankNumber) {
            AccountBalance.FetchSalesForceAccountProfile_element request_x = new AccountBalance.FetchSalesForceAccountProfile_element();
            AccountBalance.FetchSalesForceAccountProfileResponse_element response_x;
            request_x.UserID = UserID;
            request_x.Password = Password;
            request_x.AccountNumber = AccountNumber;
            request_x.ApplicationCode = ApplicationCode;
            request_x.OperatorID = OperatorID;
            request_x.CustomerBankNumber = CustomerBankNumber;
            Map<String, AccountBalance.FetchSalesForceAccountProfileResponse_element> response_map_x = new Map<String, AccountBalance.FetchSalesForceAccountProfileResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://tempuri.org/ISalesForceAccountProfile/FetchSalesForceAccountProfile',
              'http://tempuri.org/',
              'FetchSalesForceAccountProfile',
              'http://tempuri.org/',
              'FetchSalesForceAccountProfileResponse',
              'AccountBalance.FetchSalesForceAccountProfileResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.FetchSalesForceAccountProfileResult;
        }
    }
    public class FetchSalesForceAccountProfile_element {
        public String UserID;
        public String Password;
        public String AccountNumber;
        public String ApplicationCode;
        public String OperatorID;
        public String CustomerBankNumber;
        private String[] UserID_type_info = new String[]{'UserID','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] Password_type_info = new String[]{'Password','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] AccountNumber_type_info = new String[]{'AccountNumber','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] ApplicationCode_type_info = new String[]{'ApplicationCode','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] OperatorID_type_info = new String[]{'OperatorID','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] CustomerBankNumber_type_info = new String[]{'CustomerBankNumber','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'UserID','Password','AccountNumber','ApplicationCode','OperatorID','CustomerBankNumber'};
    }
    public class FetchSalesForceAccountProfileResponse_element {
        public AccountBalanceArrays.ArrayOfKeyValueOfstringstring FetchSalesForceAccountProfileResult;
        private String[] FetchSalesForceAccountProfileResult_type_info = new String[]{'FetchSalesForceAccountProfileResult','http://schemas.microsoft.com/2003/10/Serialization/Arrays','ArrayOfKeyValueOfstringstring','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'FetchSalesForceAccountProfileResult'};
    }
}