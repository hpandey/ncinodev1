/************************************************************************************
 Apex Class Name     : AccountService
 Version             : 1.0
 Created Date        : 08th Mar 2016
 Function            : Service Class for Account object.
 Author              : Deloitte Consulting
 Modification Log    :
* Developer                         Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar                 03/08/2016                Original Version
*************************************************************************************/

public Class AccountService{
    
    /************************************************************************
    Method Name: evaluatePrescreenRules
    Author Name: Pranil Thubrikar
    Description: Method to create Prescreening Result Records by getting data from Account record.
    Parameters: List<Account> listAccount
    Returns: List<Regulatory_and_Fraud_Indicators__c>
    *************************************************************************/
    public List<Regulatory_and_Fraud_Indicators__c> evaluatePrescreenRules(List<Account> listAccount){
        
        List<Regulatory_and_Fraud_Indicators__c> listResultInsert = new List<Regulatory_and_Fraud_Indicators__c>();
        
        //Get Record Type Id's.
        ID strOOMRecType = Schema.SObjectType.Regulatory_and_Fraud_Indicators__c.getRecordTypeInfosByName().get(Label.Out_of_Market).getRecordTypeId();
        ID strOOCRecType = Schema.SObjectType.Regulatory_and_Fraud_Indicators__c.getRecordTypeInfosByName().get(Label.Out_of_Country).getRecordTypeId();
        ID strVermontRecType = Schema.SObjectType.Regulatory_and_Fraud_Indicators__c.getRecordTypeInfosByName().get(Label.Vermont).getRecordTypeId();
        
        if(listAccount!=null && !listAccount.isEmpty()){
          for(Account acc : listAccount){
                //Create Prescreening Result Records for Out of Market.
                Regulatory_and_Fraud_Indicators__c pr_OOM = new Regulatory_and_Fraud_Indicators__c();
                pr_OOM.RecordTypeId = strOOMRecType;
                pr_OOM.Flag_Type__c = Label.Out_of_Market;
                if(!acc.OOM_Rule_Flag__c){
                    pr_OOM.Result__c = Label.Failed;
                }
                else{
                    pr_OOM.Result__c = Label.Passed;
                }
                pr_OOM = updatePrescreeningAddress(pr_OOM, acc);
                if(pr_OOM != null){
                    listResultInsert.add(pr_OOM);
                }
                
                //Create Prescreening Result Records for Out of Country.
                Regulatory_and_Fraud_Indicators__c pr_OOC = new Regulatory_and_Fraud_Indicators__c();
                pr_OOC.RecordTypeId = strOOCRecType;
                pr_OOC.Flag_Type__c = Label.Out_of_Country;
                if(!acc.OOC_Rule_Flag__c){
                    pr_OOC.Result__c = Label.Failed;
                }
                else{
                    pr_OOC.Result__c = Label.Passed;
                }
                pr_OOC = updatePrescreeningAddress(pr_OOC, acc);
                if(pr_OOC != null){
                    listResultInsert.add(pr_OOC);
                }
                
                //Create Prescreening Result for Vermont Rule.
                if(acc.Is_Individual__c){
                    Regulatory_and_Fraud_Indicators__c pr_vermont = new Regulatory_and_Fraud_Indicators__c();
                    pr_vermont.RecordTypeId = strVermontRecType;
                    pr_vermont.Flag_Type__c = Label.Vermont;
                    if(!acc.Vermont_Rule_Flag__c){
                        pr_vermont.Result__c = Label.Failed;
                    }
                    else{
                        pr_vermont.Result__c = Label.Passed;
                    }
                    pr_vermont = updatePrescreeningAddress(pr_vermont, acc);
                    if(pr_vermont != null){
                        listResultInsert.add(pr_vermont);
                    }
                }
            }
        }
        return listResultInsert;
    } 
    
    /************************************************************************
    Method Name: getRelatedEntitieswithActiveLoan
    Author Name: Pranil Thubrikar
    Description: Method to get related Entity Involvement Records by passing Account record.
    Parameters: List<Account> listAccount
    Returns: List<LLC_BI__Legal_Entities__c>
    *************************************************************************/
    public List<LLC_BI__Legal_Entities__c> getRelatedEntitieswithActiveLoan(List<Account> listAccount){
        
        List<LLC_BI__Legal_Entities__c> listEntity = new List<LLC_BI__Legal_Entities__c>();
        Set<Id> setAccIds = new Set<Id>();
        // variable to store all stages for which prescreening rules need to be processed
        List<String> listApplicableStages = new List<String>();
        if(Label.Loan_Stages_Prescreening != null && !String.isBlank(Label.Loan_Stages_Prescreening)) {
            listApplicableStages = Label.Loan_Stages_Prescreening.split(';');
        }
        else{
            String errMsg = 'Custom Label for Loan Stages is Null or Empty. Please contact System Administrator.';
            UtilityClass.throwCustomLabelException(Label.Prescreening_Insert_Error, errMsg);
        }
        if(listAccount != null && !listAccount.isEmpty() && !listApplicableStages.isEmpty()){
            setAccIds = new Map<Id,Account>(listAccount).keySet();
            if(!setAccIds.isEmpty()){
                try{
                    listEntity = [SELECT Id, LLC_BI__Loan__c, LLC_BI__Account__c 
                                  FROM LLC_BI__Legal_Entities__c 
                                  WHERE LLC_BI__Account__c IN :setAccIds 
                                    AND LLC_BI__Loan__r.LLC_BI__Stage__c IN :listApplicableStages];
                }
                catch(QueryException e){
                    utilityClass.throwExceptions(e);
                    throw new AccServiceException(Label.Prescreening_Insert_Error + ' : '+e.getMessage());
                }
            }
        }
        return listEntity;
    }
    
    /************************************************************************
    Method Name: updatePrescreeningAddress
    Author Name: Pranil Thubrikar
    Description: Method to update the address in new Prescreening records.
    Parameters: Regulatory_and_Fraud_Indicators__c psrRec, Account accRec
    Returns: Regulatory_and_Fraud_Indicators__c
    *************************************************************************/
    private Regulatory_and_Fraud_Indicators__c updatePrescreeningAddress(Regulatory_and_Fraud_Indicators__c psrRec, Account accRec){
        
        if(psrRec != null && accRec != null){
            psrRec.Customer__c = accRec.Id;
            psrRec.Address__c = '';
            psrRec.Secondary_Address__c = '';
            
            //Populate Primary Address in Prescreening Result records.
            if((psrRec.Flag_Type__c == Label.Out_of_Country && !accRec.OOC_Primary_Flag__c) || (psrRec.Flag_Type__c == Label.Out_Of_Market && !accRec.OOM_Primary_Flag__c) || (psrRec.Flag_Type__c == Label.Vermont && !accRec.Vermont_Primary_Flag__c)){
                if(String.isNotBlank(accRec.BillingStreet) && accRec.BillingStreet != null) {
                    psrRec.Address__c = accRec.BillingStreet+',';
                }
                if(String.isnotBlank(accRec.BillingCity) && accRec.BillingCity != null) {
                    psrRec.Address__c += accRec.BillingCity+',';
                }
                if(String.isnotBlank(accRec.BillingState) && accRec.BillingState != null) {
                    psrRec.Address__c += accRec.BillingState+',';
                }
                if(String.isnotBlank(accRec.BillingPostalCode) && accRec.BillingPostalCode != null) {
                    psrRec.Address__c += accRec.BillingPostalCode+',';
                }
                if(String.isnotBlank(accRec.BillingCountry) && accRec.BillingCountry != null) {
                    psrRec.Address__c += accRec.BillingCountry;
                }
                else{ 
                    psrRec.Address__c = psrRec.Address__c.removeEnd(',');
                }
            }
            
            //Populate Secondary Address in Prescreening Result records.
            if((psrRec.Flag_Type__c == Label.Out_of_Country && !accRec.OOC_Secondary_Flag__c) || (psrRec.Flag_Type__c == Label.Out_Of_Market && !accRec.OOM_Secondary_Flag__c) || (psrRec.Flag_Type__c == Label.Vermont && !accRec.Vermont_Secondary_Flag__c)){
                if(String.isNotBlank(accRec.ShippingStreet) && accRec.ShippingStreet != null) {
                    psrRec.Secondary_Address__c = accRec.ShippingStreet+',';
                }
                if(String.isnotBlank(accRec.ShippingCity) && accRec.ShippingCity != null) {
                    psrRec.Secondary_Address__c += accRec.ShippingCity+',';
                }
                if(String.isnotBlank(accRec.ShippingState) && accRec.ShippingState != null) {
                    psrRec.Secondary_Address__c += accRec.ShippingState+',';
                }
                if(String.isnotBlank(accRec.ShippingPostalCode) && accRec.ShippingPostalCode != null) {
                    psrRec.Secondary_Address__c += accRec.ShippingPostalCode+',';
                }
                if(String.isnotBlank(accRec.ShippingCountry) && accRec.ShippingCountry != null) {
                    psrRec.Secondary_Address__c += accRec.ShippingCountry;
                }
                else{ 
                    psrRec.Secondary_Address__c = psrRec.Secondary_Address__c.removeEnd(',');
                }
            }
        }
        return psrRec;
    }
    
    
    /************************************************************************
    Method Name: handleAccountAddressChange
    Author Name: Pranil Thubrikar
    Description: Method to Handle Account Address change and to create Prescreening Result records.
    Parameters: Regulatory_and_Fraud_Indicators__c psrRec, Account accRec
    Returns: Map<String, List<Regulatory_and_Fraud_Indicators__c>>
    *************************************************************************/
    public Map<String, List<Regulatory_and_Fraud_Indicators__c>> handleAccountAddressChange(List<Account> listAccount){
        
        List<LLC_BI__Legal_Entities__c> listRelatedEntity = new List<LLC_BI__Legal_Entities__c>();
        List<Regulatory_and_Fraud_Indicators__c> listOldResult = new List<Regulatory_and_Fraud_Indicators__c>();
        List<Regulatory_and_Fraud_Indicators__c> listNewResult = new List<Regulatory_and_Fraud_Indicators__c>();
        List<Regulatory_and_Fraud_Indicators__c> listResultDelete = new List<Regulatory_and_Fraud_Indicators__c>();
        List<Regulatory_and_Fraud_Indicators__c> listResultInsert = new List<Regulatory_and_Fraud_Indicators__c>();
        Map<String, List<Regulatory_and_Fraud_Indicators__c>> mapResultDML = new Map<String, List<Regulatory_and_Fraud_Indicators__c>>();
            
            if(listAccount != null && !listAccount.isEmpty()){
                
                listRelatedEntity = getRelatedEntitieswithActiveLoan(listAccount);
                
                if(listRelatedEntity != null && !listRelatedEntity.isEmpty()){
                    //Create Object of the Entity Service Class
                    EntityInvolvementService entityInvolvementServiceObj = new EntityInvolvementService();
                    //Create Object of the Loan Service Class
                    LoanService loanServiceObj = new LoanService();
                    
                    //Get existing Prescreening Result Records.
                    listOldResult = entityInvolvementServiceObj.getRelatedPrescreeningResults(listRelatedEntity);
    
                    //Evaluate New Prescreening Result Records.
                    listNewResult = entityInvolvementServiceObj.evaluatePrescreenRules(listRelatedEntity);
    
                //Get Prescreening Results records to delete & insert.
                    //Get results to delete.
                    if(listOldResult != null && !listOldResult.isEmpty()){
                        listResultDelete = loanServiceObj.getPrescreeningResultsToDelete(listOldResult);
                        if(listResultDelete != null && !listResultDelete.isEmpty()){
                            mapResultDML.put('Delete',listResultDelete);
                        }
                    }
                    
                    //Get results to insert.
                    if(listNewResult != null && !listNewResult.isEmpty()){
                        listResultInsert = loanServiceObj.getPrescreeningResultsToInsert(listNewResult);
                        if(listResultInsert != null && !listResultInsert.isEmpty()){
                            mapResultDML.put('Insert',listResultInsert);
                        }
                    }
                }
            }
            
        return mapResultDML;
    }
    
    // Custom Exception for AccountService class
    public class AccServiceException extends Exception { }
}