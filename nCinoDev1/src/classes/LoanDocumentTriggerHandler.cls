/***********************************************************************************
 Apex Class Name     : LoanDocumentTriggerHandler
 Version             : 1.0
 Created Date        : 31st March 2016
 Function            : Handler Class for Loan Document Trigger. This Class will not have any logic and will call methods in Helper Class.
 Author              : Deloitte Consulting
 Modification Log    :
* Developer                             Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar         3/31/2016                Original Version
*************************************************************************************/

public Class LoanDocumentTriggerHandler implements ITriggerHandler {
    
    //Create Object for Service Class to call appropriate methods.
    LoanDocumentService loanDocServiceObj = new LoanDocumentService();

    //Handler Method for Before Insert.
    public void BeforeInsert(List<sObject> newItems) {
    }
    
    //Handler Method for Before Update.
    public void BeforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
    }
    
    //Handler Method for Before Delete.    
    public void BeforeDelete(Map<Id, sObject> oldItems) {
    }
    
    //Handler method for After Insert.
    public void AfterInsert(Map<Id, sObject> newItems) {
    
        List<LLC_BI__LLC_LoanDocument__c> listFilteredLoanDoc = new List<LLC_BI__LLC_LoanDocument__c>();
        List<Asynchronous_Queue__c> listAsyncQueueToInsert = new List<Asynchronous_Queue__c>();
        
        for(LLC_BI__LLC_LoanDocument__c loanDocRec : (List< LLC_BI__LLC_LoanDocument__c >)newItems.values()){
            if(loanDocRec.LLC_BI__reviewStatus__c == 'Approved' && loanDocRec.LLC_BI__Has_File__c){//Condition to be finalized.
                listFilteredLoanDoc.add(loanDocRec);
            }
        }
        if(!listFilteredLoanDoc.isEmpty()){
            listAsyncQueueToInsert = loanDocServiceObj.getAsyncEntryForApprovedLoanDoc(listFilteredLoanDoc);
            if(listAsyncQueueToInsert != null && !listAsyncQueueToInsert.isEmpty()){
                List<Database.SaveResult> listSaveResult; //= Database.Insert(listAsyncQueueToInsert,false);
                if(!listSaveResult.isEmpty()){
                    //Add Error on List Records if Prescreening Result records are not created.
                    for(Integer index = 0; index < listAsyncQueueToInsert.size(); index++){
                        if(!listSaveResult[index].isSuccess()){
                            if(listAsyncQueueToInsert[index].Record_Ids__c != null){
                                newItems.get(listAsyncQueueToInsert[index].Record_Ids__c).addError('Error creating Async recs : '+UtilityClass.getErrorMessages(listSaveResult[index].getErrors()));
                            }
                        }
                    }
                    utilityClass.processSaveResult(listSaveResult);
                }
            }
        }
    }

    //Handler method for After Update.    
    public void AfterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems){
    
        Map<Id, LLC_BI__LLC_LoanDocument__c> mapApprovedLoanDoc = new Map<Id, LLC_BI__LLC_LoanDocument__c>();
        List<LLC_BI__LLC_LoanDocument__c> listRejectedLoanDoc = new List<LLC_BI__LLC_LoanDocument__c>();
        List<Asynchronous_Queue__c> listAsyncQueueToInsert = new List<Asynchronous_Queue__c>();
        
        for(LLC_BI__LLC_LoanDocument__c loanDocRec : (List< LLC_BI__LLC_LoanDocument__c >)newItems.values()){
            if(loanDocRec.LLC_BI__reviewStatus__c == 'Approved' && loanDocRec.LLC_BI__Has_File__c){//Condition to be finalized.
                if(!mapApprovedLoanDoc.keySet().contains(loanDocRec.Id)){
                    mapApprovedLoanDoc.put(loanDocRec.Id, loanDocRec);
                }
            }
            if(loanDocRec.NDOC__isTrash__c){
                //Add rec to list.
            }
            if(loanDocRec.LLC_BI__reviewStatus__c == 'Rejected' && ((LLC_BI__LLC_LoanDocument__c)oldItems.get(loanDocRec.Id)).LLC_BI__reviewStatus__c == 'Approved'){
                //listRejectedLoanDoc.add(loanDocRec);
            }
        }
        
        if(!mapApprovedLoanDoc.isEmpty()){
            listAsyncQueueToInsert = loanDocServiceObj.getAsyncEntryForApprovedLoanDoc(mapApprovedLoanDoc.values());
            if(listAsyncQueueToInsert != null && !listAsyncQueueToInsert.isEmpty()){
                List<Database.SaveResult> listSaveResult; //= Database.Insert(listAsyncQueueToInsert,false);
                if(!listSaveResult.isEmpty()){
                    //Add Error on List Records if Prescreening Result records are not created.
                    for(Integer index = 0; index < listAsyncQueueToInsert.size(); index++){
                        if(!listSaveResult[index].isSuccess()){
                            if(listAsyncQueueToInsert[index].Record_Ids__c != null){
                                newItems.get(listAsyncQueueToInsert[index].Record_Ids__c).addError('Error creating Async recs : '+UtilityClass.getErrorMessages(listSaveResult[index].getErrors()));
                            }
                        }
                    }
                    utilityClass.processSaveResult(listSaveResult);
                }
            }
        }
    }

    //Handler method for After Delete    
    public void AfterDelete(Map<Id, sObject> oldItems) {
    }

    //Handler method for After Undelete.
    public void AfterUndelete(Map<Id, sObject> oldItems) {
    }
}