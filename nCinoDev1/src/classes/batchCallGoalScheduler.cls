global class batchCallGoalScheduler implements Schedulable{
   global void execute(SchedulableContext sc) {
      batchCallGoals bCG = new batchCallGoals(); 
      database.executebatch(bCG,200);
   }
}