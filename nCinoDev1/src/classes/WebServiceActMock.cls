@isTest
global class WebServiceActMock implements WebServiceMock {
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           
    GetActivities_Stub.getEventsResponse_element respElement =  
        new GetActivities_Stub.getEventsResponse_element();
    
    GetActivities_Response.EventDetailObject[] eventDetail = new GetActivities_Response.EventDetailObject[1];
    eventDetail[0]= new GetActivities_Response.EventDetailObject();
    
    eventDetail[0].AccountNumber = '0000000000109555307';
    eventDetail[0].AssocUserBankNumber = 1;
    eventDetail[0].AssocUserBranchNumber = 112;
    eventDetail[0].AssocUserID = 'U472LLN';
    eventDetail[0].AssocUserName = 'CHERYL KRUSE';
    eventDetail[0].EventDescription = 'Account Information Maintenance';
    eventDetail[0].EventGuid = '3e61a6b6-ff06-4b7a-a36b-6c9455cef4fd' ;
    eventDetail[0].ProductCode = '';
    eventDetail[0].RCIFID = '00001235356472000';
    eventDetail[0].SourceDescription = 'Sales and Service';
    
    
    GetActivities_Response.ArrayOfEventDetailObject arrayEvent = new GetActivities_Response.ArrayOfEventDetailObject();
    arrayEvent.EventDetailObject = eventDetail;
    
    respElement.getEventsResult=arrayEvent;
      
    response.put('response_x', respElement);        
        
   }
}