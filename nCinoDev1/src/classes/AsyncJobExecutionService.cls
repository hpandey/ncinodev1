/**********************************************************************************************
 * Name         : AsyncJobExecution
 * Author       : Deloitte Consulting
 * Description  : Class which contains logic of Apex Queueable jobs.
 *********************************************************************************************/
/***********************************************************************************************************************************
Code Review Comments - Aveleena
1) As mentioned by Fayas check for null first and then isEmpty at line 29,44 -- At line 29 Done , At 44 map is already initialised.
2) Can we check first if mapRecordIdsByObjectAPIName contains Account & Loan and then only call getAccountData() & getLoanData() accordingly? -- Done
3) Add with sharing keyword to the class -- Done
4) Create a seperate method to split the Ids,insted of having it in 2 places -- Done
************************************************************************************************************************************/
public with sharing class AsyncJobExecutionService {

    map<id , sObject> mapSobjectById = new map<id , sObject>();

    /***
    * Method name    : getSobjectData
    * Description    : Method for querying requied sObject data for API callout.
    * Return Type    : map<Id , sObject>
    * Parameter      : set<String> setSobjectRecordIds
    */

    public map<Id , sObject> getSobjectData(set<String> setSobjectRecordIds) {
        map<string , string> mapRecordIdsByObjectAPIName = new map<string , string>();
        //map<id , sObject> mapSobjectById = new map<id , sObject>();
        
        system.debug('setSobjectRecordIds@@'+setSobjectRecordIds);
        if(setSobjectRecordIds != Null && !setSobjectRecordIds.isEmpty()) {
            for(string objIds : setSobjectRecordIds) {
                string objAPIName = ID.valueof(objIds).getSObjectType().getDescribe().getName();
                if(mapRecordIdsByObjectAPIName.containsKey(objAPIName)) {
                    string strTempId = mapRecordIdsByObjectAPIName.get(objAPIName);
                    strTempId += environmentVariables.COMMA+objIds;
                    mapRecordIdsByObjectAPIName.put(objAPIName , strTempId);
                }
                else{
                    mapRecordIdsByObjectAPIName.put(objAPIName , objIds);
                }
            }
        }
        system.debug('mapRecordIdsByObjectAPIName@@'+mapRecordIdsByObjectAPIName);
        
        if(!mapRecordIdsByObjectAPIName.isEmpty()) {
        	if(mapRecordIdsByObjectAPIName.ContainsKey(Label.Account)) {
        		getAccountData(mapRecordIdsByObjectAPIName);	
        	}
        	if(mapRecordIdsByObjectAPIName.ContainsKey(Label.Loan_API_Name)) {
        		getLoanData(mapRecordIdsByObjectAPIName);	
        	}
        }
        system.debug('mapSobjectById@@'+mapSobjectById); 
        return mapSobjectById;
    }

    /***
    * Method name    : getAccountData
    * Description    : Method for querying Account Data
    * Return Type    : void
    * Parameter      : map<string , string> mapRecordIdsByObjectAPIName
    */

    public void getAccountData(map<string , string> mapRecordIdsByObjectAPIName) {
        list<Account> lstAccount = new list<Account>();
        if(mapRecordIdsByObjectAPIName.ContainsKey(Label.Account)) {
            list<Id> lstAccountIds =  getListOfRecordIds(mapRecordIdsByObjectAPIName.get(Label.Account));
            if(!lstAccountIds.isEmpty()) {
                try {
                    lstAccount = [select
                                  id , name ,Description 
                                  from Account 
                                  where id IN:lstAccountIds];
                }
                Catch(QueryException e) {
                    UtilityClass.throwExceptions(e);
                }
                if(!lstAccount.isEmpty()) {
                    for(Account objAccount : lstAccount) {
                        mapSobjectById.put(objAccount.id , objAccount);
                    }
                }
            }
        }
        system.debug('mapSobjectById##'+mapSobjectById); 
    }

    /***
    * Method name    : getLoanData
    * Description    : Method for querying Loan Data
    * Return Type    : void
    * Parameter      : map<string , string> mapRecordIdsByObjectAPIName
    */

    public void getLoanData(map<string , string> mapRecordIdsByObjectAPIName) {
        list<LLC_BI__Loan__c> lstLoan = new list<LLC_BI__Loan__c>();
        if(mapRecordIdsByObjectAPIName.ContainsKey(Label.Loan_API_Name)) {
        	list<Id> lstLoanIds =  getListOfRecordIds(mapRecordIdsByObjectAPIName.get(Label.Loan_API_Name));
            if(!lstLoanIds.isEmpty()) {
                try {
                    lstLoan = [select
                               id , name 
                               from LLC_BI__Loan__c 
                               where id IN:lstLoanIds];
                }
                Catch(QueryException e) {
                    UtilityClass.throwExceptions(e);
                }
                if(!lstLoan.isEmpty()) {
                    for(LLC_BI__Loan__c objLoan : lstLoan) {
                        mapSobjectById.put(objLoan.id , objLoan);
                    }
                }
            }
        }
        system.debug('mapSobjectById$$'+mapSobjectById); 
    }
    
    /***
    * Method name    : getListOfRecordIds
    * Description    : Method for creating a list of Ids from string which contains Comma separated Ids. 
    * Return Type    : list<Id>
    * Parameter      : String strRecordsIds
    */
    public list<Id> getListOfRecordIds(string strRecordsIds) {
    	list<Id> lstRecordIds = new list<Id>();
    	if(!string.isBlank(strRecordsIds)) {
    		lstRecordIds = strRecordsIds.split(EnvironmentVariables.COMMA);
    	}
    	return lstRecordIds;
    }
}