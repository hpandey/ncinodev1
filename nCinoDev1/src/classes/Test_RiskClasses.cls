/*
@author - Sendeep
Descrption   Test methods
Testmethod Triggers covered - 
    Trigger - restClientBeforeInsertBeforeUpdate
*/
@isTest
 public class Test_RiskClasses 
{

    static  testmethod void test1()
    {
        Profile p = [select id from profile where name='System Administrator'];
       // UserRole r = new UserRole(Name='Dummy Role');
        
       // insert r;
        User testUser = new User(alias = 'testuser', email= 'testuser986@testorg.com',
              emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
              localesidkey='en_US', profileid = p.Id, country='United States',Associate_ID__c='TEST67',
              timezonesidkey='America/Los_Angeles', username= 'testuser987' + '@testorg.com');
            
         insert testUser;
         
         Id riskAssementRecordTypeId = Schema.SObjectType.Risk_Assessment_Review__c.getRecordTypeInfosByName().get('Risk Assessment').getRecordTypeId();
         Id riskReviewRecordTypeId = Schema.SObjectType.Risk_Assessment_Review__c.getRecordTypeInfosByName().get('Conduct Review').getRecordTypeId();
           
                
                                
        system.runAs(testUser)
        {       
                Relationship__c relationship = new Relationship__c(Relationship_ID__c='3456gbjds0984');
                    insert relationship;
                Account acct= new Account(OwnerId = testuser.ID ,name='testAccount986',Relationship__c=relationship.id,Associate_ID__c='TEST67');               
                    insert acct;
                Restricted_Client_Information__c test_resClientInfo = new Restricted_Client_Information__c(client__c=acct.id);
                    insert test_resClientInfo;
                Risk_Assessment_Review__c riskAssesment = new Risk_Assessment_Review__c(Restricted_Client_Information__c = test_resClientInfo.ID,
                                                                                         recordTypeID = riskAssementRecordTypeId
                                                                                         );
                                                                                         
                    insert riskAssesment;
                                
                
                   // riskRTypeselection Code coverage STARTED
                              
                PageReference editRiskPage = Page.RiskRtype;
                Test.setCurrentPageReference(editRiskPage); 
                
                // record type risk assesment               
                editRiskPage.getParameters().put('RecordType', riskAssementRecordTypeId);                                   
                ApexPages.standardController controller1 = new ApexPages.standardController(riskAssesment);
                riskRTypeselection RiskRtypeCTRL1 = new riskRTypeselection(controller1); 
                RiskRtypeCTRL1.ChagneURL();
                RiskRtypeCTRL1.gotoEdit();
                
                // record type risk review              
                editRiskPage.getParameters().put('RecordType', riskReviewRecordTypeId);                                 
                ApexPages.standardController controller2 = new ApexPages.standardController(riskAssesment);
                riskRTypeselection RiskRtypeCTRL2 = new riskRTypeselection(controller2); 
                RiskRtypeCTRL2.ChagneURL();
                RiskRtypeCTRL2.gotoEdit();
                
                editRiskPage.getParameters().put('RecordType', null);               
                ApexPages.standardController controller3 = new ApexPages.standardController(riskAssesment);
                riskRTypeselection RiskRtypeCTRL3 = new riskRTypeselection(controller3);                
                RiskRtypeCTRL3.gotoEdit();
                
                riskAssesment.recordtypeID = riskReviewRecordTypeId;                
                ApexPages.standardController controller4 = new ApexPages.standardController(riskAssesment);
                riskRTypeselection RiskRtypeCTRL4 = new riskRTypeselection(controller4);                
                RiskRtypeCTRL4.gotoEdit();  
                
                //    riskRTypeselection Code coverage ENDED
                
                // new instance 
                Risk_Assessment_Review__c riskAssesment99 = new Risk_Assessment_Review__c();
                PageReference RiskAssessmentPageNew = Page.RiskAssessment;
                Test.setCurrentPageReference(RiskAssessmentPageNew);                
                RiskAssessmentPageNew.getParameters().put('retURL', test_resClientInfo.ID);
                RiskAssessmentPageNew.getParameters().put('RecordType', riskAssementRecordTypeId);  
                    
                ApexPages.standardController ctrlRiskAsses99 = new ApexPages.standardController(riskAssesment99);
                riskassessmentExtn  riskassessmentExtnCtrl99 = new riskassessmentExtn(ctrlRiskAsses99);
                riskassessmentExtn.isApexTest = true;
                riskassessmentExtnCtrl99.save();
                    
                PageReference RiskAssessmentPage = Page.RiskAssessment;
                Test.setCurrentPageReference(RiskAssessmentPage);
                // record type risk Assesment
                
                RiskAssessmentPage.getParameters().put('RecordType', riskAssementRecordTypeId);                                 
                ApexPages.standardController ctrlRiskAsses = new ApexPages.standardController(riskAssesment);
                riskassessmentExtn  riskassessmentExtnCtrl = new riskassessmentExtn(ctrlRiskAsses);
                riskassessmentExtn.isApexTest = true;
                
                riskassessmentExtnCtrl.renderDateaction();
                riskassessmentExtnCtrl.getrQSTypes();
                riskassessmentExtnCtrl.getrQFTypes();
                riskassessmentExtnCtrl.getrQ1Types();
                riskassessmentExtnCtrl.getrQ3Types();
                riskassessmentExtnCtrl.getrQ4Types();
                riskassessmentExtnCtrl.getrQ5Types();
                //riskassessmentExtnCtrl.getrQ6Types();
                riskassessmentExtnCtrl.getQ7Types();
                riskassessmentExtnCtrl.getQ6signedTypes();
                riskassessmentExtnCtrl.getQ6outgoingTypes();
                riskassessmentExtnCtrl.getQ6incomingTypes();
                riskassessmentExtnCtrl.getQ6Types();
                riskassessmentExtnCtrl.getQ5Types();
                riskassessmentExtnCtrl.getQ4Types();
                riskassessmentExtnCtrl.getQ3Types();
                riskassessmentExtnCtrl.getQ3ATypes();
                riskassessmentExtnCtrl.getQ2Types();
                riskassessmentExtnCtrl.getQ1Types();                
                
                
                riskassessmentExtnCtrl.assignDefaultValues();
                riskassessmentExtnCtrl.ChangeQ7();
                riskassessmentExtnCtrl.ChangeQ6();
                riskassessmentExtnCtrl.ChangeQ5();
                riskassessmentExtnCtrl.ChangeQ4();
                riskassessmentExtnCtrl.ChangeQ3();      
                riskassessmentExtnCtrl.ChangeQ2();
                riskassessmentExtnCtrl.ChangeQ1();      
                riskassessmentExtnCtrl.ChangeQ6Travel();
                riskassessmentExtnCtrl.addRiskreviewTrans();
                
                riskassessmentExtnCtrl.rChangeQ8();
                riskassessmentExtnCtrl.rChangeQ7();
                riskassessmentExtnCtrl.rChangeQ6();
                riskassessmentExtnCtrl.rChangeQ5();
                riskassessmentExtnCtrl.rChangeQ4();
                riskassessmentExtnCtrl.rChangeQ3();
                riskassessmentExtnCtrl.pageValidations();

            // for record type conduct review
            
                RiskAssessmentPage.getParameters().put('RecordType', riskReviewRecordTypeId);                                   
                ApexPages.standardController ctrlRiskREview = new ApexPages.standardController(riskAssesment);
                riskassessmentExtn  riskassessmentExtnCtrl2 = new riskassessmentExtn(ctrlRiskREview);
                riskassessmentExtn.isApexTest = true;
                
                riskassessmentExtnCtrl2.renderDateaction();
                riskassessmentExtnCtrl2.getrQSTypes();
                riskassessmentExtnCtrl2.getrQFTypes();
                riskassessmentExtnCtrl2.getrQ1Types();
                riskassessmentExtnCtrl2.getrQ3Types();
                riskassessmentExtnCtrl2.getrQ4Types();
                riskassessmentExtnCtrl2.getrQ5Types();
                //riskassessmentExtnCtrl2.getrQ6Types();
                riskassessmentExtnCtrl2.getQ7Types();
                riskassessmentExtnCtrl2.getQ6signedTypes();
                riskassessmentExtnCtrl2.getQ6outgoingTypes();
                riskassessmentExtnCtrl2.getQ6incomingTypes();
                riskassessmentExtnCtrl2.getQ6Types();
                riskassessmentExtnCtrl2.getQ5Types();
                riskassessmentExtnCtrl2.getQ4Types();
                riskassessmentExtnCtrl2.getQ3Types();
                riskassessmentExtnCtrl2.getQ2Types();
                riskassessmentExtnCtrl2.getQ1Types();               
                
                
                riskassessmentExtnCtrl2.assignDefaultValues();
                riskassessmentExtnCtrl2.ChangeQ7();
                riskassessmentExtnCtrl2.ChangeQ6();
                riskassessmentExtnCtrl2.ChangeQ5();
                riskassessmentExtnCtrl2.ChangeQ4();
                riskassessmentExtnCtrl2.ChangeQ3();     
                riskassessmentExtnCtrl2.ChangeQ2();
                riskassessmentExtnCtrl2.ChangeQ1();     
                riskassessmentExtnCtrl2.ChangeQ6Travel();
                riskassessmentExtnCtrl2.addRiskreviewTrans();
                
                riskassessmentExtnCtrl2.rChangeQ8();
                riskassessmentExtnCtrl2.rChangeQ7();
                riskassessmentExtnCtrl2.rChangeQ6();
                riskassessmentExtnCtrl2.rChangeQ5();
                riskassessmentExtnCtrl2.rChangeQ4();
                riskassessmentExtnCtrl2.rChangeQ3();
                riskassessmentExtnCtrl2.pageValidations();
                
                riskassessmentExtnCtrl2.save();    
                    
               riskAssesment.Assessment_Q1__c = 'Yes';
               riskAssesment.Assessment_Q1_Countries__c = null;
               riskAssesment.Assessment_Q2__c = null;
               riskAssesment.Assessment_Q3__c = 'Yes';
               riskAssesment.Assessment_Q3A__c = null;
               riskAssesment.Assessment_Q3A_Comment__c = null;
               riskAssesment.Assessment_Q3B__c = null;
               riskAssesment.Assessment_Q4__c = 'yes';
               riskAssesment.Assessment_Q4_Comment__c = null;
               riskAssesment.Assessment_Q5__c = 'Yes';
               riskAssesment.Assessment_Q5_Comment__c = null;
               riskAssesment.Assessment_Q6__c = 'Yes';
               riskAssesment.Travelers_check_Money_order__c  = false;
               riskAssesment.International_Wires__c = false;
               riskAssesment.Domestic_Wires__c  = false;
               riskAssesment.Assessment_Q7__c = 'yes';
               riskAssesment.Assessment_Q7_Comment__c = null;
                riskAssesment.recordtypeId = riskAssementRecordTypeId;
                update riskAssesment;
                
                riskAssesment.Review_Q3B__c  = null;
                riskAssesment.Review_Q4__c = null;
                riskAssesment.Review_Q5__c = null;
                riskAssesment.Review_Q6__c = null;
                riskAssesment.Review_Q7__c = null;
                riskAssesment.Review_Q8__c = null;
                update riskAssesment;
                           
            // negative test case
            // record type risk assesment
                RiskAssessmentPage.getParameters().put('RecordType', riskAssementRecordTypeId);                                 
                ApexPages.standardController ctrlRiskAssesnegR = new ApexPages.standardController(riskAssesment);
                riskassessmentExtn  riskassessmentExtnCtrlnegR = new riskassessmentExtn(ctrlRiskAssesnegR);
                riskassessmentExtn.isApexTest = true;   
                riskassessmentExtnCtrlnegR.save();
                riskassessmentExtnCtrlnegR.assignDefaultValues();
                
                riskAssesment.Review_Q3__c = 'Yes';
                riskAssesment.Review_Q3_Comment__c = null;
                riskAssesment.Review_Q3B__c  = 'No';
                riskAssesment.Review_Q4__c = 'Yes'; 
                riskAssesment.Review_Q5__c = 'Yes';
                riskAssesment.Review_Q5_Comment__c = null;
                riskAssesment.Review_Q6__c = 'Yes';
                riskAssesment.Review_Q6_Comment__c  = null;
                riskAssesment.Review_Q7__c = 'Yes';
                riskAssesment.Review_Q7_Comment__c  = null;
                riskAssesment.Review_Q8__c = 'Yes';
                riskAssesment.Review_Q8_Comment__c  = null;
                riskAssesment.recordtypeId = riskReviewRecordTypeId;
                update riskAssesment;
                
                
                
            // record type conduct review
                RiskAssessmentPage.getParameters().put('RecordType', riskReviewRecordTypeId);                                   
                ApexPages.standardController ctrlRiskAssesnegC = new ApexPages.standardController(riskAssesment);
                riskassessmentExtn  riskassessmentExtnCtrlnegc = new riskassessmentExtn(ctrlRiskAssesnegC);
                riskassessmentExtn.isApexTest = true;   
                riskassessmentExtnCtrlnegC.save();  
                riskassessmentExtnCtrlnegC.assignDefaultValues();       
            
            // test rest trigger -restClientBeforeInsertBeforeUpdate 
            List<Restricted_Client_Information__c> restClients = new List<Restricted_Client_Information__c>();
            
            Restricted_Client_Information__c resClient1 = new Restricted_Client_Information__c(client__c=acct.id);
                        resClient1.Date_Approved__c  =  system.today();
                        resClient1.Risk_Rating_Override__c = 'High';
                        resClient1.Existing_Client__c= 'Yes';
                        restClients.add(resClient1);
            Restricted_Client_Information__c resClient2 = new Restricted_Client_Information__c(client__c=acct.id);
                        resClient2.Date_Approved__c  =  system.today();
                        resClient2.Risk_Rating_Override__c = 'Medium';
                        resClient2.Existing_Client__c= 'Yes';
                        resClient2.Monitoring_For_Suspicious_Activity__c= true;
                        resClient2.Date_Last_Review_Approved__c = system.today(); 
                        restClients.add(resClient2);                        
            Restricted_Client_Information__c resClient3 = new Restricted_Client_Information__c(client__c=acct.id);
                        resClient3.Date_Approved__c  =  system.today();
                        resClient3.Risk_Rating_Override__c = 'Low';
                        resClient3.Existing_Client__c= 'Yes';    
                        resClient3.Monitoring_For_Suspicious_Activity__c= true;
                        resClient3.Date_Approved__c = system.today();                   
                        restClients.add(resClient3);
                        
            Restricted_Client_Information__c resClient4 = new Restricted_Client_Information__c(client__c=acct.id);
                        resClient4.Date_Approved__c  =  system.today();
                        //resClient4.Risk_Rating_Level__c  = 'High';
                        resClient4.Existing_Client__c= 'Yes';
                        restClients.add(resClient4);
            Restricted_Client_Information__c resClient5 = new Restricted_Client_Information__c(client__c=acct.id);
                        resClient5.Date_Approved__c  =  system.today();
                        //resClient5.Risk_Rating_Level__c  = 'Medium';
                        resClient5.Existing_Client__c= 'Yes';
                        resClient5.Monitoring_For_Suspicious_Activity__c= true;
                        resClient5.Date_Last_Review_Approved__c = system.today(); 
                        restClients.add(resClient5);                        
            Restricted_Client_Information__c resClient6 = new Restricted_Client_Information__c(client__c=acct.id);
                        resClient6.Date_Approved__c  =  system.today();
                        //resClient6.Risk_Rating_Level__c  = 'Low';
                        resClient6.Existing_Client__c= 'Yes';    
                        resClient6.Monitoring_For_Suspicious_Activity__c= true;
                        resClient6.Date_Approved__c = system.today();                   
                        restClients.add(resClient6);
                        
                    insert restClients;
                    List<Restricted_Client_Information__c> restClientsUpdate = new List<Restricted_Client_Information__c>();
                    
                    for(Restricted_Client_Information__c rs : restClients)
                    {
                        if(rs.Risk_Rating_Level__c == 'Low')
                        {                       
                                //rs.Risk_Rating_Level__c = 'Medium'; 
                        }
                        else if(rs.Risk_Rating_Level__c == 'Medium')
                        {                       
                                //rs.Risk_Rating_Level__c = 'High';
                        }           
                        else if (rs.Risk_Rating_Level__c == 'High')
                        {                       
                               // rs.Risk_Rating_Level__c = 'Low';
                        } 
                                
                        if(rs.Risk_Rating_Override__c == 'Low') 
                        {                   
                                rs.Risk_Rating_Override__c = 'Medium';  
                        }       
                        else if(rs.Risk_Rating_Override__c == 'Medium')
                        {                       
                                rs.Risk_Rating_Override__c = 'High';
                        }           
                        else if(rs.Risk_Rating_Override__c == 'High')
                        {                       
                                rs.Risk_Rating_Override__c = 'Low';
                        }       
                        restClientsUpdate.add(rs);                              
                    }
                    update restClientsUpdate;
                    system.debug('*****insertED'+restClients.size());
        // for trigger - sendemailtemplte on Bank_Account_Details__c object                 
        List<Bank_Account_Details__c> bankAccounts = new List<Bank_Account_Details__c>();
            Bank_Account_Details__c bankAccount = new Bank_Account_Details__c(Name='test Bank name',Client__c = acct.ID,Account_Officer_Code__c=acct.Associate_ID__C );
            bankAccount.Closed_Indicator__c = 'Yes';
            bankAccount.Product_ID__c = 'TESTCODE'; 
                bankAccounts.add(bankaccount);
                insert bankAccounts;
            bankAccount.Closed_Indicator__c = 'Yes';
            bankAccount.Product_ID__c = 'TESTCODE'; 
            bankAccount.Account_Officer_Code__c = '';           
                update bankAccounts;
            system.debug('*****bankaccount******'+bankaccount);
            
                                                        
                       
        }            
            
    }
    
}