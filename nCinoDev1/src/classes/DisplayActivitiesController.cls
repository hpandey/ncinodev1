public with sharing class DisplayActivitiesController {
	public GetActivities_Response.ArrayOfEventDetailObject res {get; set;}
	//public GetActivities_Response.EventDetailObject[] records {get; set;}
	public List<DataTableWrapper> myCollection {get; set;}
	public Account account { get; set; }
	public string errorMessage {get; set;}
    public string errorStyle {get; set;}
    public string MessageForError {get; set;}
    public string MessageForRejection {get; set;}
    public string MessageForTimeout {get; set;}
    
	public void listActivities() {
		MessageForError = RegionsConnectivity__c.getInstance('Activities_WS').MessageForError__c;
        MessageForRejection = RegionsConnectivity__c.getInstance('Activities_WS').MessageForRejection__c;
        MessageForTimeout = RegionsConnectivity__c.getInstance('Activities_WS').MessageForTimeoutError__c;
        Decimal Timeout = RegionsConnectivity__c.getInstance('Activities_WS').Timeout__c;
		errorStyle = 'display:none;';
		List<Account> RCIFs = [SELECT RCIF__c FROM Account WHERE Id = :account.Id];
		String RCIF = RCIFs[0].RCIF__c;
		System.debug('Your RCIF: ' + RCIF);
		if (RCIF == null){
			//Display message to user that the recent activities can't be displayed.
			System.debug('Empty RCIF: ' + RCIF);
			errorStyle = 'display:block;';
			errorMessage = 'The RCIF field is blank on this record. Please add the appropriate value and try again.';
		}else{
			myCollection = new List<DataTableWrapper>();
			try{
				System.debug('Doing the basic http binding call...');
				GetActivities_Stub.BasicHttpBinding_IOrbit360EventsSF act= new GetActivities_Stub.BasicHttpBinding_IOrbit360EventsSF();
				System.debug('Made it out of the http binding call...');
				act.timeout_x = Timeout.intValue();//10000;
				System.debug('ACT: ' + act);
				System.debug('About to get res...');
				//The next line is causing the errors in my test covereage not sure why...
				res = act.getEvents('RCIFID',RCIF);
				System.debug('Got res...' + res);
				//records = res.EventDetailObject;
				//System.debug('EventDetailObject: ' + res.EventDetailObject);//.EventDetailObject[0]
				if (res.EventDetailObject != null){
					for (Integer i = 0; i < res.EventDetailObject.size(); i++){
						myCollection.add(new DataTableWrapper(res.EventDetailObject[i].EventDescription, res.EventDetailObject[i].SourceDescription, 
						res.EventDetailObject[i].AccountNumber, res.EventDetailObject[i].ProductCode, res.EventDetailObject[i].RCIFID, 
						res.EventDetailObject[i].AssocUserBranchNumber,res.EventDetailObject[i].AssocUserBankNumber, res.EventDetailObject[i].AssocUserName, 
						res.EventDetailObject[i].AssocUserID, res.EventDetailObject[i].EventGuid, res.EventDetailObject[i].CreatedDate, i,
						res.EventDetailObject[i].Title, res.EventDetailObject[i].Description));
					}
				}else{
					//Display message to user that the recent activities can't be displayed.
					IntegrationError__c errorRecord = new IntegrationError__c();
		            errorRecord.Error__c = 'The Activities Web Service returned a null response.';
		            errorRecord.Error_Description__c = 'The Activities Web Service returned a null response.';
		            errorRecord.Error_Type__c = 'Exception';
		            errorRecord.Identifier_1__c = RCIF;
		            errorRecord.Integration__c = 'AccountActivities';
		            errorRecord.Last_Occurrence__c = datetime.now();
		            errorRecord.Status__c = 'New';
		            errorRecord.Status_Notes__c = 'New & Still Open';
		            errorRecord.UserEncounteredError__c = UserInfo.getUserID();
		            
		            try {
		                insert errorRecord; // inserts the new record into the database
		            }catch(DmlException f) {
		            	System.debug('DML insert Error: ' + f);
		                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating new error log'));
		            }catch(Exception f){
		            	System.debug('DML insert Error: ' + f);
		                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating new error log'));
		            }
					System.debug('Empty RCIF: ' + RCIF);
					errorStyle = 'display:block;';
					errorMessage = 'The RCIF for this client could not be found in our system. Please add the appropriate value and try again, or contact your administrator for further assistance.';			
				}
			}catch(CalloutException e){
	            IntegrationError__c errorRecord = new IntegrationError__c();

	            errorRecord.Identifier_1__c = RCIF;
	            errorRecord.Integration__c = 'AccountActivities';
	            errorRecord.Last_Occurrence__c = datetime.now();
	            errorRecord.Status__c = 'New';
	            errorRecord.Status_Notes__c = 'New & Still Open';
	            errorRecord.UserEncounteredError__c = UserInfo.getUserID();
	            if (e.getMessage().contains('Read timed out')){
	            	errorMessage = MessageForTimeout;
	            	errorRecord.Error__c = 'Failed to connect to the Get Activities web service due to a long running web service call.';
		            errorRecord.Error_Description__c = 'The user was unable to connect to the web service within the timeout set in the system. ' + e;
		            errorRecord.Error_Type__c = 'Timeout';
	            }else{
		            errorMessage = 'You have encountered a connection error. ' + MessageForError;
		           	errorRecord.Error__c = 'Failed to connect to the Get Activities web service.';
	            	errorRecord.Error_Description__c = 'The user was unable to connect to the web service. ' + e;
	            	errorRecord.Error_Type__c = 'Exception';
	            }
	            errorStyle = 'display:block;';
	            try {
	                insert errorRecord; // inserts the new record into the database
	            }catch(DmlException f) {
	            	System.debug('DML insert Error: ' + f);
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating new error log'));
	            }catch(Exception f){
	            	System.debug('DML insert Error: ' + f);
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating new error log'));
	            }
	            
        	}catch(Exception e){
	            IntegrationError__c errorRecord = new IntegrationError__c();
				
	            errorRecord.Error__c = 'Unknown error while connecting to the Get Activities web service.';
	            errorRecord.Error_Description__c = 'The user was unable to connect to the web service. ' + e;
	            errorRecord.Error_Type__c = 'Exception';
	            errorRecord.Identifier_1__c = RCIF;
	            errorRecord.Integration__c = 'AccountActivities';
	            errorRecord.Last_Occurrence__c = datetime.now();
	            errorRecord.Status__c = 'New';
	            errorRecord.Status_Notes__c = 'New & Still Open';
	            errorRecord.UserEncounteredError__c = UserInfo.getUserID();
	            try {
	                insert errorRecord; // inserts the new record into the database
	            }catch(DmlException f) {
	            	System.debug('DML insert Error: ' + f);
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating new error log'));
	            }catch(Exception f){
	            	System.debug('DML insert Error: ' + f);
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating new error log'));
	            }
				errorStyle = 'display:block;';
				errorMessage = 'An unknown error has prevented activities from being delivered to the page.';
				System.debug('Big Error: ' + e);
			}
		}
		
		//System.debug('Here it is2: ' + records[0].CreatedDate);
		//System.debug('Here it is: ' + myCollection);
		//CreatedDate = records[0].CreatedDate;
		// Data is no longer in the map at this point. Not sure what is wrong. This is returning NULL
		//System.debug('Here it is2: ' + lmap.get(0).get('AssocUserID'));
	}

	// inner class
	class DataTableWrapper { 
		public Integer counter { get; set; }
		public string EventGuid {get; set;}
		public datetime CreatedDate {get; set;}
		public string AssocUserID {get; set;}
		public string AssocUserName {get; set;}
		public integer AssocBankNumber {get; set;}
		public integer AssocBranchNumber {get; set;}
		public string RCIFID {get; set;}
		public string ProductCode {get; set;}
		public string AccountNumber {get; set;}
		public string SourceDescription {get; set;}
		public string EventDescription {get; set;}
		public string Title {get; set;}
		public string Description {get; set;}
		public DataTableWrapper(String EventDescription, String SourceDescription, String AccountNumber, String ProductCode, 
				String RCIFID, Integer AssocBranchNumber, Integer AssocBankNumber, string AssocUserName, string AssocUserID, 
				String EventGuid, datetime CreatedDate, Integer counter, String Title, String Description) {
		    this.EventDescription = EventDescription;
		    this.SourceDescription = SourceDescription;
		    this.AccountNumber = AccountNumber;
		    this.ProductCode = ProductCode;
		    this.RCIFID = RCIFID;
		    this.AssocBranchNumber = AssocBranchNumber;
		    this.AssocBankNumber = AssocBankNumber;
		    this.AssocUserName = AssocUserName;
		    this.AssocUserID = AssocUserID;
		    this.EventGuid = EventGuid;
		    this.CreatedDate = CreatedDate;
		    this.counter = counter;
		    this.Title = Title;
		    this.Description = Description;
		  }
	 
	}
    public DisplayActivitiesController(ApexPages.StandardController stdController) {
    	this.account =  (Account) stdController.getRecord();
    }

}