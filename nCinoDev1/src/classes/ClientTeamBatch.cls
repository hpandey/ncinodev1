global class ClientTeamBatch implements Database.batchable<sObject>, Database.Stateful{
    global final String Query;    
   
    global ClientTeamBatch(){
        //Added Relationship__c and filter to retrieve active owners only... case 1821... Ephrem 11-2015        
        Query = 'Select Id, OwnerID, Relationship__c from Account where OwnerID in (select OwnerID from UserAccountTeamMember) and Owner.isActive = true';
    } 
   
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Account> AccountList){
        List<UserAccountTeamMember> accList = new List<UserAccountTeamMember>();
        List<AccountShare> AccntShares = new List<AccountShare>();
        List<AccountTeamMember> ATMS = new List<AccountTeamMember>();
        List<Account> AcctList = new List<Account>();
        //Case 1821 - create list of relationship records and recalculate relationship sharing; also added Relationship__c to the main query... Ephrem 11-2015
        //begin..
        List<Relationship__c> relList = new List<Relationship__c>();      
        List<UserAccountTeamMember> rua = new List<UserAccountTeamMember>();
        Set<Id> relIdList = new Set<Id>();
                
         for (Account r : AccountList){
         	accList = [select Id, UserId, OwnerId, AccountAccessLevel, TeamMemberRole, ContactAccessLevel, CaseAccessLevel, OpportunityAccessLevel from UserAccountTeamMember where OwnerID = :r.OwnerID and UserId not in (Select UserOrGroupID from AccountShare where AccountID = :r.id)];
            //
            if(r.Relationship__c <> null){
            	rua = [select Id, UserId, OwnerId from UserAccountTeamMember where OwnerID = :r.OwnerID AND UserId not in (Select UserOrGroupID from relationship__share WHERE ParentId =: r.Relationship__c)];
            }
            if(rua.size() > 0){
            	relIdList.add(r.Relationship__c);
            }            	
        	//end part 1
            if (accList.size() > 0){
                for ( UserAccountTeamMember u: accList){
                        AccountShare a = New AccountShare();
                        AccountTeamMember atm = New AccountTeamMember();
                        // Add AccountShare Record
                        a.UserOrGroupId  = u.UserID;
                        a.AccountId = r.ID;
                        a.AccountAccessLevel = u.AccountAccessLevel;
                        a.ContactAccessLevel = u.ContactAccessLevel;
                        a.CaseAccessLevel = u.CaseAccessLevel;
                        a.OpportunityAccessLevel = u.OpportunityAccessLevel;
                        AccntShares.add(a);
                        //Add AccountTeamMemeber Record
                        atm.UserId = u.UserID;
                        atm.AccountID = r.ID;
                        atm.TeamMemberRole = u.TeamMemberRole;
                        ATMS.add(atm);
                }
                AcctList.add(r);                
            }                                       
        }       
        if (accntShares.size() > 0){
            try{
                insert AccntShares;
                insert ATMS;
                AccountDetailsVisibilityUtil.recalculateSharing(AcctList);          
            }catch(DMLException e){
                System.debug('DMLException:'+e.getMessage());               
            }   
        }
        
        //Case 1821...
        //begin part 2...        
        if(relIdList.size() > 0){        
        	try{
        		relList = [select id from Relationship__c where Id IN:relIdList];        		
        		RelationshipDetailsVisibilityUtil.recalculateSharing(relList);
        	}catch(DMLException e){
            	System.debug('DMLException:'+e.getMessage());               
        	}  
        } // end part 2
    }

    global void finish(Database.BatchableContext BC){
      AsyncApexJob aaj = [select Id, ApexClassId, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors, CompletedDate, MethodName, ExtendedStatus from AsyncApexJob where Id=:BC.getJobId()];        
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      if (aaj.NumberofErrors>0){
      	//Send Mail on failure if specified in the custom setting.
        string toaddress = 'chris.wilder@regions.com';
        mail.setToAddresses(new String[] { 'chris.wilder@regions.com' });
        mail.setSubject('FAILED : ClientTeamBatch Job Run');  
        mail.setPlainTextBody('The batch Apex job processed ' + aaj.TotalJobItems + ' batches with '+ aaj.NumberOfErrors + ' failures. ExtendedStatus: ' + aaj.ExtendedStatus);  
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
      }       
    }   
}