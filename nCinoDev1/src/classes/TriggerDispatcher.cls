/************************************************************************************
 Apex Class Name     : TriggerDispatcher
 Version             : 1.0
 Created Date        : 26th Jan 2016
 Function            : Dispatcher method used to call methods based on Trigger Context
 Author              : Jag Valaiyapathy (jvalaiyapathy@deloitte.com)
 Credits             : Chris Aldridge (Original Author)
 Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------------                 
* Jag Valaiyapathy          1/26/2016                Original Version
*************************************************************************************/

public class TriggerDispatcher{

    /*
        NOTE: Call this static method from Trigger and pass instance of TriggerHandler which implements ITriggerInterface
    */
    public static void Run(ITriggerHandler handler){

        //Before Trigger Logic
        if (Trigger.IsBefore){

            if(Trigger.IsInsert){
                handler.BeforeInsert(trigger.new);
            }

            if(Trigger.IsUpdate){
                handler.BeforeUpdate(trigger.newMap, trigger.oldMap);
            }

            if(Trigger.IsDelete){
                handler.BeforeDelete(trigger.oldMap);
            }
        }

        //After Trigger Logic
        if (Trigger.IsAfter){

            if(Trigger.IsInsert){
                handler.AfterInsert(Trigger.newMap);
            }

            if(Trigger.IsUpdate){
                handler.AfterUpdate(Trigger.newMap, Trigger.oldMap);
            }

            if(Trigger.IsDelete){
                handler.AfterDelete(Trigger.oldMap);
            }

            if(Trigger.IsUndelete){
                handler.AfterUndelete(Trigger.newMap);
            }
        }
    }
}