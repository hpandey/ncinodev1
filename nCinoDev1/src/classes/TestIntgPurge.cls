@isTest
Class TestIntgPurge
{
    Static testmethod void IntgPurgeScheduleTest()
    {    
      //Create custom setting entry
      Integration_Error__c c = new Integration_Error__c ();
      c.Name='Keep';
      c.Duration__c=0;
      insert c;
      
      //Create a sample integration record
      IntegrationError__c a = new IntegrationError__c();
      a.Error__c='Test Error';
      insert a;
    
      Test.startTest();
   // Check initial order history record count should be 2   
      List<IntegrationError__c> before = [Select Id from IntegrationError__c where Error__c='Test Error'];
      System.assertEquals(1,before.size());
      
   // Schedule the test job 
      String jobId = System.schedule('testSchedule',
      '0 0 0 3 9 ? 2022', 
         new IntgPurgeSchedule());
   // Get the information from the CronTrigger API object 
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
   // Verify the expressions are the same 
      System.assertEquals('0 0 0 3 9 ? 2022', 
         ct.CronExpression);
   // Verify the job has not run 
      System.assertEquals(0, ct.TimesTriggered);
   // Verify the next time the job will run 
    
      System.assertEquals('2022-09-03 00:00:00', 
         String.valueOf(ct.NextFireTime));
         
      IntgPurgeBatch b = new IntgPurgeBatch(); 
      database.executebatch(b);

      Test.stopTest();
       
      List<IntegrationError__c> after = [Select Id from IntegrationError__c where Error__c='Test Error'];
      System.assertEquals(0,after.size());
    }
}