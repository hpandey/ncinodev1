public class updateLookback
{

	public static boolean isreccursive = false;
	public static boolean isreccursiveLineitem = false;	
	
	
	
	public static void updateOpportunites ( map<Id,ID> opplookbackmap)
	{
			
		List<Opportunity> updateOpp = new list<Opportunity>();	
    		for(Opportunity oppLK : [SELECT Id,Lookback_Pipeline__c FROM opportunity WHERE Lookback_Pipeline__c IN: opplookbackmap.keyset() order by createdDate limit 1])
    		{    			    		
    				Opportunity lookbckOpp = new Opportunity(ID=oppLK.Lookback_Pipeline__c);
               	 	lookbckOpp.Lookback_Pipeline__c = opplookbackmap.get(oppLK.Lookback_Pipeline__c);                
              	 	 updateOpp.add(lookbckOpp);	    				
    		}
    		if(updateOpp.size() > 0)
    		{	    			  			  
    			update updateOpp;    			    			
    		}
		
	}
}