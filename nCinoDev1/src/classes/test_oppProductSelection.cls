@isTest(SeeAllData=true)
private class test_oppProductSelection {

    static testMethod void oppProductSelectionTest() {
        PageReference  pageRef = Page.oppOverride3;
        
        List <Trigger_Setting__c> ts = [Select Id, Name, isActive__c from Trigger_Setting__c Where Name='Opportunity'];
        if (ts.size()>0){
            ts[0].isActive__c=false;
            update ts[0];
        }
        else {
            Trigger_Setting__c ts1 = new Trigger_Setting__c();
            ts1.Name='Opportunity';
            ts1.isActive__c=false;
            insert ts1;
        }
       
        Product2 prodObj = new Product2(Name='TestProduct',ProductCode='TestCode');
        insert prodObj;
        
        Account acc = new Account(Name='Test Account');
        insert acc;
        
        PriceBook2 priceBkObj = [select Id from Pricebook2 where isStandard=true limit 1];
        PricebookEntry priceBookEntryObj = new PricebookEntry (Product2Id=prodObj.Id,Pricebook2Id=priceBkObj.Id,UnitPrice=100,isActive=true);
        insert priceBookEntryObj;
        Opportunity oppObj = new Opportunity(Name='Test',StageName='Prospecting', Account=acc,
                                             CloseDate=System.Today().addDays(1),Pricebook2Id=priceBkObj.Id);
        insert oppObj;
        
        test.startTest();
        ApexPages.StandardController mc = new ApexPages.StandardController(oppObj);
        oppProductSelectionCon3 controller = new oppProductSelectionCon3(mc);
        //testing the autosuggest method
        list<PriceBookEntry> result = oppProductSelectionCon3.returnMatchingRec('TestProduct','Name',priceBkObj.Id);
        System.assertEquals('TestProduct',result[0].Name);
        //Selecting a product
        controller.prodId = priceBookEntryObj.Id;
        controller.selectProduct();
        //checking if the selected product is added
        System.assertEquals(controller.wrapProductList[0].priceBookProd.Id, priceBookEntryObj.Id);
        //Looping inside already selected
        controller.selectProduct();
        //removing products
        controller.prodIndex = 0;
        controller.removeProduct();
        //Checking that no product is selected
        System.assertEquals(0,controller.wrapProductList.size());
        //Again selecting product
        controller.prodId = prodObj.Id;
        controller.selectProduct();
        controller.saveProducts();
        //selecting product to save
        controller.prodId = priceBookEntryObj.Id;
        controller.selectProduct();
        //save product w/o mandatory fields
         controller.saveProducts();
         //Saving
         controller.wrapProductList[0].oppLineItem.quantity = 1;
         controller.saveProducts();
        
        test.stopTest();
    }
}