/************************************************************************************
 Apex Class Name     : FileNet_Doc_Search_Cont
 Version             : 1.0
 Created Date        : 16th Feb 2016
 Function            : controller class for visualforce page FileNet_Doc_Search_Page .
 Author              : Deloitte Consulting
 Modification Log    :
* Developer                         Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar                 2/22/2016                Original Version
* Sumit sagar
*************************************************************************************/

/******* Code Review Comments

1.  Variable naming convention is not as per our standards document. 
    e.g. all string variable should be prefiled with str
2.  Line # 46 - Validate if fieldsetmap includes configuration you are looking for - d
    before access the field - will cause null pointer
3.  I think we went over board with using final string variables for query string. 
    Below is a good pattern I have used in the past 
    String 1 --> SELECT {0} FROM {1}
    String 2 --> WHERE {2} 
    String 3 --> LIMIT {3}

    String.format(String 1 + String 2 + String 3, String[]);
    String [] --> will contain list of fields to query, 
                object from which fields needs to be queried
                condition limiting the result result
                Limit condition (use limit class to retrieve the number)
4.  String.Join is a powerful method to construct fields to query from list/array of string 
5.  Database.query is missing exception handling - done
6.  whats the purpose of loop in lines 104 through 106 - fixed that
7.  getFieldSet is missing validations that map keyset contains what is being retrieved using get().
    Also, it may make sense to move this method to a untility class to be reused. - Is there an existing class which we can use?

8.  importDoc(havent review this method in detail) logic should ideally be moved to a service class that works with DocumentManager functionality 
    and may make sense to be broken down --> Need not spend time on this now but we need to update this.
9.  Exceptions are being swallowed with out informing the user with any error messages - done
*/

public With Sharing Class FileNet_Doc_Search_Cont{
    
    public Id idRecord {get;set;}
    public String strObjAPIName;
    public String strFieldSetName;
    public Static Schema.FieldSet objFieldSet;
    public List<Schema.FieldSetMember> listFieldSetMem {get;set;}
    public Transient sObject sObjectRecord {get;set;}
    public List<docSearchResultWrapper> listDocWrap{get;set;}
    public Boolean boolShowResults{get;set;}
    
    //Constructor
    public FileNet_Doc_Search_Cont(){
        //Get record Id from Parameter in URL
        idRecord = ApexPages.currentPage().getParameters().get('Id');
        if(idRecord != null){
            //Determine if object is Account or Loan.
            strObjAPIName = idRecord.getSObjectType().getDescribe().getName();
            if(strObjAPIName != null){
                //Get fieldSet name from Custom Setting
                if(FieldSetMap__c.getValues(strObjAPIName) != null){
                    strFieldSetName = FieldSetMap__c.getValues(strObjAPIName).FieldSet_Name__c;
                }
                objFieldSet = UtilityClass.getFieldSet(strFieldSetName, strObjAPIName);
                if(objFieldSet != null){
                    listDocWrap = new List<docSearchResultWrapper>();
                    //Call method to get the Loan or Account Data.
                    displayRecData(objFieldSet);
                    boolShowResults = true;
                }
            }
        }
    }
    
    /************************************************************************
     Method Name: displayRecData
     Author Name: Sumit Sagar/Pranil Thubrikar
     Description: Method for displaying information of record according to record Id of current page and fields mentioned in FieldSetMap__c custom setting.
     Parameters: Schema.FieldSet
     Returns: void
    *************************************************************************/
    public void displayRecData(Schema.FieldSet recFieldSet){
        
        String strQuery;
        listFieldSetMem = recFieldSet.getFields();
        List<sObject> lstSobject = new List<sObject>();
        List<String> listFieldName = new List<String>();
        List<LLC_BI__LLC_LoanDocument__c> listRelatedLoanDoc = new List<LLC_BI__LLC_LoanDocument__c>();
        List<LLC_BI__AccountDocument__c> listRelatedAccDoc = new List<LLC_BI__AccountDocument__c>();
        Set<String> setDocsPresentId = new Set<String>();
         
        system.debug('@@@@@@@@'+recFieldSet+'$$$$$$$$'+listFieldSetMem);
        
        //Create a Dynamic Query.
        String strQuery_Basic = 'SELECT {0} FROM {1}';
        String strQuery_Where = ' WHERE {2}';
        String strQuery_Limit = ' LIMIT {3}';

        strQuery = strQuery_Basic + strQuery_Where + strQuery_Limit;

        for(Schema.FieldSetMember objfieldSetMember : listFieldSetMem) {
            listFieldName.add(objfieldSetMember.getFieldPath());
        }
        //Create final dynamic query string.
        strQuery = String.format(strQuery, new list<String>{String.join(listFieldName,','),'LLC_BI__Loan__c', 'Id= :idRecord','1'});

        //Store the queried data in List.
        try{
            lstSobject = Database.Query(strQuery);
        }
        catch(DMLException e){
            utilityClass.throwExceptions(e);
        }
        
        if(!lstSobject.isEmpty()){
            sObjectRecord = lstSobject[0];
        }
        
        //Check Logic to see if Document already exists in Salesforce. Will be updated when we have more data on Webservice.
        try{
            if(strObjAPIName == Label.Loan_API_Name){
                listRelatedLoanDoc = [SELECT Id, Unique_Document_Id__c 
                                      FROM LLC_BI__LLC_LoanDocument__c 
                                      WHERE LLC_BI__Loan__c = :idRecord];
                                      
                if(!listRelatedLoanDoc.isEmpty()){
                    for(LLC_BI__LLC_LoanDocument__c loan : listRelatedLoanDoc){
                        if(loan.Unique_Document_Id__c != null){
                            setDocsPresentId.add(loan.Unique_Document_Id__c);
                        }
                    }
                }
            }
            else if(strObjAPIName == Label.Account){
                listRelatedAccDoc = [SELECT Id, Unique_Document_Id__c 
                                     FROM LLC_BI__AccountDocument__c 
                                     WHERE LLC_BI__Account__c = :idRecord];
                
                if(!listRelatedAccDoc.isEmpty()){
                    for(LLC_BI__AccountDocument__c acc : listRelatedAccDoc){
                        if(acc.Unique_Document_Id__c != null){
                            setDocsPresentId.add(acc.Unique_Document_Id__c);
                        }
                    }
                }
            }
        }
        catch(QueryException e){
            utilityClass.throwExceptions(e);
        }
        
        for(Integer i = 1; i< 1000; i++){ // This if for displaying test data , will be removed when filenet integration gets finalized.
            Boolean isDocPresent = false;
            if(math.mod(i,3) == 0)//Will be replaced with the Set to check if doc is present.
                isDocPresent = true;

            listDocWrap.add(new docSearchResultWrapper(false, isDocPresent, 'docName '+i, 'docType'+i, (System.Today()-i).format(), 'docFileType '+i, 'docId '+i));
        }
    }
    /************************************************************************
     Method Name: importDoc
     Author Name: Pranil Thubrikar
     Description: Method called when User clicks Import button on VF Page. Has logic to insert Document and its subsequent records.
     Parameters: none
     Returns: pageReference
    *************************************************************************/
    public pageReference importDoc(){
        Map<Id,Attachment> mapParentIdAttachment = new Map<Id,Attachment>();
        List<LLC_BI__Document_Store__c> listDocStore = new List<LLC_BI__Document_Store__c>();
        List<LLC_BI__Document_Store_Index__c>listDocStoreIndex = new List<LLC_BI__Document_Store_Index__c>();
        List<LLC_BI__LLC_LoanDocument__c> loanDocInsertList = new List<LLC_BI__LLC_LoanDocument__c>();
        List<LLC_BI__AccountDocument__c> accDocInsertList = new List<LLC_BI__AccountDocument__c>();
        
        //Create Document Store records from selected wrapper.
        if(!listDocWrap.isEmpty()){
            for(docSearchResultWrapper docRec : listDocWrap){
                if(docRec.isSelected){
                    LLC_BI__Document_Store__c docStore = new LLC_BI__Document_Store__c();
                    docStore.LLC_BI__Type__c = 'Salesforce Attachment';
                    listDocStore.add(docStore);
                }
            }
        }
        try{
            //Insert Document Store.
            if(!listDocStore.isEmpty()){
                insert listDocStore;
            }
        }
        catch(DMLException e){
            utilityClass.throwExceptions(e);
        }
        
        //Create Document Store Index and Attachments when Document Store is created.
        if(!listDocStore.isEmpty()){
            for(LLC_BI__Document_Store__c docStore : listDocStore){
                LLC_BI__Document_Store_Index__c docStoreIndex = new LLC_BI__Document_Store_Index__c();
                docStoreIndex.LLC_BI__Document_Store__c = docStore.Id;
                listDocStoreIndex.add(docStoreIndex);
                
                Attachment newAttach = new Attachment();
                //This code will be updated after main integration part is done.
                newAttach.parentId = docStore.Id;
                mapParentIdAttachment.put(docStore.Id,newAttach);
            }
            try{
                //Insert Document Store Index and Attachment records.
                //system.debug('--docStoreIndexAttachmentMap1--'+docStoreIndexAttachmentMap.keySet());
                if(!listDocStoreIndex.isEmpty()){
                    insert listDocStoreIndex;
                }
                //system.debug('--docStoreIndexAttachmentMap2--'+docStoreIndexAttachmentMap.keySet());
                if(!mapParentIdAttachment.values().isEmpty()){
                    insert mapParentIdAttachment.values();
                }
            }
            catch(DMLException e){
                utilityClass.throwExceptions(e);
            }
            
        }
        
        //Create Account Document or Loan Document records.
        if(!listDocStoreIndex.isEmpty()){
            Map<String,Id> docManMap = new Map<String,Id>();
            for(LLC_BI__DocManager__c docMan : [SELECT Id, Name FROM LLC_BI__DocManager__c LIMIT 2]){
                docManMap.put(docMan.Name,docMan.Id);
            }
            for(LLC_BI__Document_Store_Index__c docStoreIndex : listDocStoreIndex){
                if(mapParentIdAttachment.get(docStoreIndex.LLC_BI__Document_Store__c) != null){
                    //Code for creating Loan Document.
                    if(strObjAPIName == Label.Loan_API_Name){
                        LLC_BI__LLC_LoanDocument__c loanDocRec = new LLC_BI__LLC_LoanDocument__c();
                        loanDocRec.LLC_BI__Loan__c = idRecord;
                        loanDocRec.Name = mapParentIdAttachment.get(docStoreIndex.LLC_BI__Document_Store__c).Name;
                        loanDocRec.LLC_BI__Document_Store_Index__c = docStoreIndex.Id;
                        loanDocRec.LLC_BI__attachmentId__c = String.valueOf(mapParentIdAttachment.get(docStoreIndex.LLC_BI__Document_Store__c).Id);
                        loanDocRec.LLC_BI__docManager__c = docManMap.get(Label.LLC_Loan);
                        loanDocRec.NDOC__fileName__c = mapParentIdAttachment.get(docStoreIndex.LLC_BI__Document_Store__c).Name;
                        loanDocRec.LLC_BI__fileSize__c = mapParentIdAttachment.get(docStoreIndex.LLC_BI__Document_Store__c).BodyLength;
                        loanDocRec.LLC_BI__mimeType__c = mapParentIdAttachment.get(docStoreIndex.LLC_BI__Document_Store__c).Name.substringAfterLast('.');
                        loanDocInsertList.add(loanDocRec);
                    }
                    //Code for creating Account Document.
                    else if(strObjAPIName == Label.Account){                    
                        LLC_BI__AccountDocument__c accDocRec = new LLC_BI__AccountDocument__c();
                        accDocRec.LLC_BI__Account__c = idRecord;
                        accDocRec.Name = mapParentIdAttachment.get(docStoreIndex.LLC_BI__Document_Store__c).Name;
                        accDocRec.LLC_BI__Document_Store_Index__c = docStoreIndex.Id;
                        accDocRec.LLC_BI__attachmentId__c = String.valueOf(mapParentIdAttachment.get(docStoreIndex.LLC_BI__Document_Store__c).Id);
                        accDocRec.LLC_BI__docManager__c = docManMap.get(Label.Account);
                        accDocRec.NDOC__fileName__c = mapParentIdAttachment.get(docStoreIndex.LLC_BI__Document_Store__c).Name;
                        accDocRec.LLC_BI__fileSize__c = mapParentIdAttachment.get(docStoreIndex.LLC_BI__Document_Store__c).BodyLength;
                        accDocRec.LLC_BI__mimeType__c = mapParentIdAttachment.get(docStoreIndex.LLC_BI__Document_Store__c).Name.substringAfterLast('.');
                        accDocInsertList.add(accDocRec);
                    }
                }
            }
            try{
                //Insert Loan Document.
                if(!loanDocInsertList.isEmpty()){
                    insert loanDocInsertList;
                }
                //Insert Account Document.
                if(!accDocInsertList.isEmpty()){
                    insert accDocInsertList;
                }
            }
            catch(DMLException e){
                utilityClass.throwExceptions(e);
            }
        }
        return null;
    }
    
    /************************************************************************
     Class Name: docSearchResultWrapper
     Author Name: Sumit Sagar/Pranil Thubrikar
     Description: wrapper class used for displaying data after API callout from fileNet
    *************************************************************************/
    public Class docSearchResultWrapper{
        public Boolean isSelected{get;set;}
        public Boolean isPresent{get;set;}
        public String docName{get;set;}
        public String docType{get;set;}
        public String docDateRecieved{get;set;}
        public String docFileType{get;set;}
        public String docId{get;set;}
        
        //Constructor for Wrapper.
        public docSearchResultWrapper(Boolean isSelected, Boolean isPresent, String docName, String docType, String docDateRecieved, String docFileType, String docId){
            this.isSelected = isSelected;
            this.isPresent = isPresent;
            this.docName = docName;
            this.docType = docType;
            this.docDateRecieved = docDateRecieved;
            this.docFileType = docFileType;
            this.docId = docId;
        }
    }
}