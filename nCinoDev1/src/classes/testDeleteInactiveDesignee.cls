//------------------------------------------------------------------------------------------------------------------------------------------
//Test Class for inactiveDesigneeTrigger and deleteInactiveDesigneeHandler class...
//Case 2266 - remove a user from the Designee object when moved to an Inactive status...
//Ephrem Tekle 08-2015
//------------------------------------------------------------------------------------------------------------------------------------------

@isTest (SeeAllData = true)
private class testDeleteInactiveDesignee {
    static testmethod void testDeleteDesignee(){
        Test.startTest();
        List<ID> x = new List<ID>();
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        
        System.runAs (thisUser) {
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User testUser1 = new User( Alias='tester1', Email='t1@testorg.com', 
            EmailEncodingKey='UTF-8', FirstName = 'Bob', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Chicago', UserName='bt@testorg.com');
            insert testUser1;
            
            User testUser2 = new User( Alias='tester2', Email='t2@testorg.com', 
            EmailEncodingKey='UTF-8', FirstName = 'John', LastName='Test', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Chicago', UserName='jt@testorg.com');
            insert testUser2;
        
            List<Designee__c> dUser1 = new List<Designee__c>();        
            Designee__c w = new Designee__c(Designee__c = testUser1.Id);            
            dUser1.add(w);
            insert dUser1;
            
            List<Designee__c> dUser2 = new List<Designee__c>();        
            Designee__c q = new Designee__c(Designee__c = testUser2.Id);            
            dUser2.add(q);
            insert dUser2;             
             
            testUser1.isActive = false;
            update testUser1;
            
            testUser2.isActive = false;
            update testUser2;
            
            List<User> y = [SELECT Id from User where id =:testUser1.Id];
            x.add(y[0].Id);
            
            List<Designee__c> d = new List<Designee__c>();
            List<Designee__c> dList = [SELECT Id, OwnerId, Designee__c from designee__c WHERE Designee__c in:x];
            d.add(dList[0]);                    
            system.assertEquals(d.size(), 1);
            delete d;
            Test.stopTest();
        }
    }
}