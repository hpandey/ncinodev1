/*
Author - Peter Martin Appirio
Custom Clone of Coaching Reviews that also clones unfinished Coaching Areas
Date: 02/06/2014
*/

public class cloneCoachingReviewCTRL {
    Coaching_Review__c reviewOLD = new Coaching_Review__c();
    List<Coaching_Area__c> reviewAreasOld = new List<Coaching_Area__c>();
    List<Coaching_Area__c> reviewAreasNew = new List<Coaching_Area__c>();
    String reviewId = '';
    String originalReviewId = '';
    public Boolean showError{get;set;}
    
    public cloneCoachingReviewCTRL(ApexPages.StandardController controller) {
        reviewId = ApexPages.currentPage().getParameters().get('id');
        String qryCoaching_Review = new selectall('Coaching_Review__c').soql + ' where Id= :reviewId LIMIT 1';
        reviewOld = Database.query(qryCoaching_Review);
        originalReviewID = reviewId;
        // only clone Coaching Areas that have no results
        reviewAreasOld = [SELECT Id, Area_of_Development__c, Focus_Area__c, Comments__c, Next_Steps__c 
                            FROM Coaching_Area__c 
                            WHERE Coaching_Review__c =:reviewId
                            and Completed__c = FALSE];
    }   

    public pagereference cloneReview() { 
            Coaching_Review__c newRecord;
            newRecord = reviewOld.clone(false,true);
            newRecord.Status__c = 'Draft';
            newRecord.Coaching_Date__c = reviewOld.Coaching_Date__c.addMonths(1);
            if (reviewOld.Reminder_Date__c <> null) {
                newRecord.Reminder_Date__c = reviewOld.Reminder_Date__c.addMonths(1);
            }
            newRecord.Prior_Review__c = originalReviewId;
            insert newRecord;
            cloneAreas(reviewAreasOld, newRecord);
            reviewOld.Next_Review__c = newRecord.id;
            update reviewOld;
            return new PageReference('/'+newRecord.id);
    }
    
    public void cloneAreas(List<Coaching_Area__c> areas, Coaching_Review__c reviewNew) {
       for( Coaching_Area__c  area : areas) {
           Coaching_Area__c newarea = area.clone(false);
           newarea.Coaching_Review__c = reviewNew.id;
           reviewAreasNew.add(newarea);
       }
       insert reviewAreasNew;
    }
    
}