/******************************************************************************************************************
* Name            :             FillLoanQuestionnaireControllerExt
* Author          :             Deloitte Consulting
* Description     :             This is a controller class which will include all the business logic 
                                to get the set of questions for a particular loan type.
*******************************************************************************************************************/

/*
* do we need the variable operation, can we just not use the list size()
*
*/
Public with sharing class FillLoanQuestionnaireControllerExt{
    private ID loanId{set;get;}
    private String loanType{set;get;}//loanType to query the appropriate Questions
    public String operation{set;get;}//decides whether INSERT or UPDATE operation
    public String parentQuestionID{set;get;}
    public String parentAnswerValue{set;get;}
    
    //Lists
    transient List<Questionnaire__c> lstQuestionnaire{set;get;}
    transient List<Questionnaire__c> lstDependentQuestionnaire{set;get;}
    private List<Loan_Questionnaire__c> lstLoanQuestionnaire{get;set;}
    transient List<Loan_Questionnaire__c> lstQuestionnaireTBI{set;get;}//List to be inserted
    transient List<Loan_Questionnaire__c> lstQuestionnaireTBU{set;get;}//List to be updated
    public List<QuestionnaireWrapper> lstQuestionnaireWrapper{set;get;}
 //Code Review comments - I think the wrapper only needs to be non-transient. Rest all can be made transient [Comments] I had tried this earlier myself, but making them transient resulted in null pointer exceptions.
    //Maps
    private map<id,String> mapQsnIDQsnType{get;set;}
    public map<id,List<SelectOption>> mapQsnIdAnswers{set;get;}
    private map<String,Integer> mapIndentation{get;set;}
    private map<String,List<Questionnaire__c>> mapDependentQuestion {get;set;}    
    
    /*Method Name    :        FillLoanQuestionnaireControllerExt
     *Author         :        Deloitte Consulting
     *Description    :        This is a constructor which gets the loan id and type.
                              It initiates all the collections and gets the Questionniare
                              records and loan Questionnaire records. 
    */
    public FillLoanQuestionnaireControllerExt(){
        loanId = ApexPages.currentPage().getParameters().get('id');           // Get the loan ID from the URL
        loanType = ApexPages.currentPage().getParameters().get('type');      // Get the loan type from the URL
        init();                                                             // Initiates all the collections
        //If the Questionnaire was filled in before for this loan, fetch the answers selected last time from the LoanQuestionnaire__c junction object
        getListofLoanQuestionnaire();                                      
        //If the Questionnaire is being filled in for the first time, Query the appropriate Questions from the Questionnaire__c object
        if(lstLoanQuestionnaire.size()==0){
            buildListofLoanQuestionnaire();
        }//end if
        
    }//end of constructor
    
    /*Method Name    :        init
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to instatiate the collections 
     *Parameters     :        None
     *Returns        :        Void
    */
    private void init(){
        //instantiate lists
        lstLoanQuestionnaire = new List<Loan_Questionnaire__c>();
        lstQuestionnaire = new List<Questionnaire__c>();
        lstDependentQuestionnaire = new List<Questionnaire__c>();
        lstQuestionnaireWrapper = new List<QuestionnaireWrapper>();
        
        //instantiate maps
        mapQsnIDQsnType = new map<id,String>();
        mapQsnIdAnswers = new map<id,List<SelectOption>>();
        mapIndentation = new map<String,Integer>();
        mapDependentQuestion = new map<String,List<Questionnaire__c>>();
    }//end method
    
    /*Method Name    :        getListofLoanQuestionnaire
     *Author         :        Deloitte Consulting
     *Description    :        This method is called only if the laon record has existing loan Questionniare records
                              associated with it.
     *Parameters     :        None
     *Returns        :        Void
    */
    public void getListofLoanQuestionnaire(){
        //SOQL to get the loan Questionnaire records.
        lstLoanQuestionnaire = [SELECT
                                    Loan__c,
                                    Questionnaire__c,
                                    Answer__c,
                                    Questionnaire__r.Id,
                                    Questionnaire__r.Question__c,
                                    Questionnaire__r.Type__c,
                                    Questionnaire__r.Required__c,
                                    Questionnaire__r.Answers__c,
                                    Questionnaire__r.Order__c,
                                    Questionnaire__r.Parent_Question__c,
                                    Questionnaire__r.Parent_Answer__c,
                                    Questionnaire__r.Description__c
                                FROM
                                    Loan_Questionnaire__c
                                WHERE
                                    Loan__c = :loanId
                                ORDER BY
                                    Questionnaire__r.Order__c];
        if(lstLoanQuestionnaire.size()>0){ //Null check
            operation = 'UPDATE';
            getQuestionnaireWrapperRecords(lstLoanQuestionnaire); // send the list of Loan Questionnaire records and get the list of Questionnaire Wrapper
        }//end if                          
    }//end method
    
    /*Method Name    :        buildListofLoanQuestionnaire
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to construct a list of QuestionnaireWrapper based on the records returned from the query.
     *Parameters     :        None
     *Returns        :        Void
    */
    public void buildListofLoanQuestionnaire(){
        //Query to retrive the Questionnaire records based on the loan type.
        lstQuestionnaire = [SELECT
                                  Loan_Type__c,
                                  Question__c,
                                  Type__c,
                                  Required__c,
                                  Parent_Question__c,
                                  Parent_Answer__c,
                                  Answers__c,
                                  Order__c,
                                  Description__c 
                            FROM
                                  Questionnaire__c
                            WHERE
                                  Loan_Type__c = :loanType
                            ORDER BY
                                  Order__c];
         //Build the QuestionnaireWrapper list from the queried list of Questionnaires based on parent questions and child questions 
         System.debug('type is @@@@@@'+loanType);
         if(lstQuestionnaire.size()>0){ // Null check to see if the query has returned records
             System.debug('lstQuestionnaire is '+lstQuestionnaire.size());
             operation = 'NEW';
             //lstQuestionnaireWrapper = new QuestionnaireWrapper [lstQuestionnaire.size()];
             getQuestionnaireWrapperRecords(lstQuestionnaire); // send the list of Questionnaire records and get the list of Questionnaire Wrapper
         }//end if    
         
    }//end method

    /*Method Name    :        buildUtilityMaps
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to generate the options for the relevant questions
     *Parameters     :        List<QuestionnaireWrapper>
     *Returns        :        Void
    */
    public void buildUtilityMaps(List<QuestionnaireWrapper> lstQuestionnaireWrpr){
        if(lstQuestionnaireWrpr.size()>0){ // Null check
            for(QuestionnaireWrapper qrwpr:lstQuestionnaireWrpr){
                mapQsnIDQsnType.put(qrwpr.qn.id,qrwpr.qn.Type__c);
                if(qrwpr.qn.Answers__c!=null && qrwpr.qn.Answers__c!=''){
                    List<SelectOption> Options = new List<SelectOption>();
                    for(String str:qrwpr.qn.Answers__c.split(';')){
                        Options.add(new SelectOption(str,str));
                    }//end for
                    mapQsnIdAnswers.put(qrwpr.qn.id,Options);
                }//end if
            }//end for
        }//end if
    }//end method
    
    /*Method Name    :        buildDependentQuestionnaireMap
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to get the child questions based on the answers selected by the user on the page
     *Parameters     :        None
     *Returns        :        Void
    */
    public void buildDependentQuestionnaireMap(){
        Integer CurrIndx=0,var=0;
        //The answer selected for the parent question
        for(integer i=0;i<lstQuestionnaireWrapper.size();i++){
            if(lstQuestionnaireWrapper.get(i).qn.id == parentQuestionID){
                parentAnswerValue = lstQuestionnaireWrapper.get(i).Answer; // getting the parent answer
                CurrIndx = i; // getting the index of the parent question
            }//end if
        }//end for
        
        //The new index where the dependent question will be added on the list
        CurrIndx+=1; 
  //Code review comments - can we move the indentation logic completely to separate method.  [Comments] I have made the changes but doing this will add 2 for loops. Please refer to indentatiobValue method     
        if(mapDependentQuestion.containskey(parentQuestionID+'-'+parentAnswerValue)){
            lstDependentQuestionnaire = mapDependentQuestion.get(parentQuestionID+'-'+parentAnswerValue);                                  
            //Iterate through the list of dependent questions queried above add them next to the parent question index
            if(lstDependentQuestionnaire.size()>0){
                for(Questionnaire__c qr:lstDependentQuestionnaire){
                   QuestionnaireWrapper QuestionWrapper = new QuestionnaireWrapper();
                   QuestionWrapper.qn = qr;
                   if(lstQuestionnaireWrapper.size()==CurrIndx){ // to avoid null exception if size of the list is less and we need to add new values at a specific position
                       lstQuestionnaireWrapper.add(QuestionWrapper);  
                  }else
                       lstQuestionnaireWrapper.add(CurrIndx,QuestionWrapper);
                }//end for
                if(lstQuestionnaireWrapper.size()>0){ // null check
                    indentationValue(lstQuestionnaireWrapper);
                    buildUtilityMaps(lstQuestionnaireWrapper); 
                }     
            }//end if
        }//end if
        
        //remove the previously added dependent questions as the answer changes
        for(var=0;var<lstQuestionnaireWrapper.size();var++){
            if(lstQuestionnaireWrapper.get(var).qn.Parent_Question__c==parentQuestionID 
                && 
                lstQuestionnaireWrapper.get(var).qn.Parent_Answer__c!=parentAnswerValue){
                    lstQuestionnaireWrapper.remove(var);
                }//end if
        }//end for
    }//end method
    
    /*Method Name    :        indentationValue
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to add indentation value to each question
     *Parameters     :        List<QuestionnaireWrapper>
     *Returns        :        Void
    */
   public void indentationValue(List<QuestionnaireWrapper> lstQuestionnaireWrapper){
        for(QuestionnaireWrapper q:lstQuestionnaireWrapper){ // traverse throught each record
            if(q.qn.Parent_Question__c!=null && mapIndentation.containskey(q.qn.Parent_Question__c)){// add indentation only for child records
                String value='';
                for(integer i=0;i<mapIndentation.get(q.qn.Parent_Question__c)+4;i++){// dynamically add number of '-'
                    value+='-';    
                }//end for
                q.indentation = value;
                mapIndentation.put(q.qn.id,mapIndentation.get(q.qn.Parent_Question__c)+3);    
            }//end if    
        }//end for
    }//end method
    
    
    /*Method Name    :        save
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to associate the relevant questions to the Loan record based on the user's selection
     *Parameters     :        None
     *Returns        :        PageReference
    */
    public PageReference save(){
        List<Loan_Questionnaire__c> lstQuestionnaire = new List<Loan_Questionnaire__c>();
        lstQuestionnaire = checkValidationBeforeSave(lstQuestionnaireWrapper);
        if(lstQuestionnaire != Null && !lstQuestionnaire.isEmpty()) {
            try{
                upsert lstQuestionnaire;
            }
            catch(DMLException Dml){ // specific DML exception
                System.debug('Exception is '+Dml.getMessage());
                utilityClass.throwExceptions(Dml);
                displayMessage(Dml);
                return null;
            }//end of try catch block
        }
        else {
        return null;
        }
        
        return new PageReference('/'+loanId); // return page reference
    }//end method
    
    /*Method Name    :        checkValidationBeforeSave
     *Author         :        Deloitte Consulting
     *Description    :        This method is used for checking validation before save.
     *Parameters     :        list<QuestionnaireWrapper> lstQuestionnaireWrapper
     *Returns        :        List<Loan_Questionnaire__c>
    */
    public List<Loan_Questionnaire__c> checkValidationBeforeSave(list<QuestionnaireWrapper> lstQuestionnaireWrapper) {
        lstQuestionnaireTBI = new List<Loan_Questionnaire__c>();
        lstQuestionnaireTBU = new List<Loan_Questionnaire__c>();
        List<Loan_Questionnaire__c> lstQuestionnaire = new List<Loan_Questionnaire__c>();
        Boolean ErrorFlag = false;
        String ErrorMessage;
        try{
            for(QuestionnaireWrapper qrwpr:lstQuestionnaireWrapper){
                Loan_Questionnaire__c LoanQuestion = new Loan_Questionnaire__c();
                LoanQuestion.Questionnaire__c = qrwpr.qn.id;
                LoanQuestion.Loan__c = loanId;
                LoanQuestion.id = qrwpr.loanQuestionnaireId;
                if(qrwpr.qn.Required__c == true && String.isBlank(qrwpr.Answer) && qrwpr.qn.Type__c != 'Multi-Select'){// Validation checks
                    qrwpr.errorMessage = 'Required value missing';
                    ErrorMessage = 'Required value missing';
                    ErrorFlag = true;
                }//end if
                else if(qrwpr.qn.Required__c == true && String.isNotBlank(qrwpr.Answer) && qrwpr.qn.Type__c != 'Multi-Select'){// Validation checks
                    qrwpr.errorMessage = '';
                    LoanQuestion.Answer__c = qrwpr.Answer;
                }//end else if
                else if(qrwpr.qn.Required__c == true && qrwpr.qn.Type__c == 'Multi-Select' && qrwpr.Answers==null){// Validation checks
                    qrwpr.errorMessage = 'Required value missing';
                    ErrorMessage = 'Required value missing';
                    ErrorFlag = true;
                }//end else if
                else if(qrwpr.qn.Required__c == true && qrwpr.qn.Type__c == 'Multi-Select' && qrwpr.Answers!=null){// Validation checks
                    qrwpr.errorMessage = '';   
                    LoanQuestion.Answer__c = convertArraytoString(qrwpr.Answers); 
                }//end else if
                else{ // insert the questions
                     if(qrwpr.qn.Type__c == 'Multi-Select')
                        LoanQuestion.Answer__c = convertArraytoString(qrwpr.Answers);
                    else
                        LoanQuestion.Answer__c = qrwpr.Answer;
                }//end else
                    if(LoanQuestion.id == null)
                        lstQuestionnaireTBI.add(LoanQuestion);
                    else
                        lstQuestionnaireTBU.add(LoanQuestion);
            }//end for
            System.debug('Error flag is '+ErrorFlag);
            if(ErrorFlag == true) {
                return null;
            }
            else{
                System.debug('inside else');
                if(lstQuestionnaireTBI.size()>0)//null check
                    lstQuestionnaire.addAll(lstQuestionnaireTBI);
                if(lstQuestionnaireTBU.size()>0)//null check
                    lstQuestionnaire.addAll(lstQuestionnaireTBU);
                if(!lstQuestionnaire.isEmpty()) {
                    return lstQuestionnaire;
                }    
            }//end else
        }catch(DMLException Dml){ // specific DML exception
            System.debug('Exception is '+Dml.getMessage());
            utilityClass.throwExceptions(Dml);
            displayMessage(Dml);
            return null;
        }//end of try catch block
        return null;
    }

    /*Method Name    :        Cancel
     *Author         :        Deloitte Consulting
     *Description    :        This method is called when user clicks on cancel button on the page
     *Parameters     :        None
     *Returns        :        PageReference
    */
    public PageReference Cancel(){
        return new PageReference('/'+loanId);
    }
    
    /*Method Name    :        convertArraytoString
     *Author         :        Deloitte Consulting
     *Description    :        Convert Array of String to a semicolon separated values of String to store the input for SelectCheckboxes field 
     *Parameters     :        String[] answers
     *Returns        :        String
    */
    private String convertArraytoString(String[] answers){
        String returnString='';
        for(String str:answers){
            returnString += (returnString==''?'':';')+str;
        }//end for
        return returnString;        
    }//end method
    
    /*Method Name    :        convertStringtoArray
     *Author         :        Deloitte Consulting
     *Description    :        Convert a semicolon separated values of String to Array of String to display the options of SelectCheckboxes
     *Parameters     :        String[] answers
     *Returns        :        String
    */    
    private String[] convertStringtoArray(String str){
        String[] returnArray = new String[]{};
        if(str!=null)
            returnArray = str.split(';');
        else
            returnArray = null;
        return returnArray;    
    }//end method
    
    /*Method Name    :        deleteOldRecords
     *Author         :        Deloitte Consulting
     *Description    :        Delete old loan Questionnaire records and fecth the new Questions
     *Parameters     :        None
     *Returns        :        pageReference 
    */
    public void deleteOldRecords(){
        System.debug('entered');
        if(operation == 'UPDATE' && lstLoanQuestionnaire.size()>0){ // this will run only for updated records
            try{
                delete lstLoanQuestionnaire; // delete the loan Questionnaire records
            }catch(DMLException Dml){ // specific DML Excecptions
                System.debug('Exception is '+Dml.getMessage());
                utilityClass.throwExceptions(Dml); 
                displayMessage(Dml);   
            }
        }//end if
        lstLoanQuestionnaire = new List<Loan_Questionnaire__c>();
        lstQuestionnaireWrapper = new List<QuestionnaireWrapper>();
        System.debug('lstLoanQuestionnaire size '+lstLoanQuestionnaire.size() +' lstQuestionnaireWrapper '+lstQuestionnaireWrapper.size());
        buildListofLoanQuestionnaire(); // Fetch the Questions based on loan type
        //return null;
    }//end method
    
    /*Method Name    :        displayMessage
     *Author         :        Deloitte Consulting
     *Description    :        Method to display error messages on the page.
     *Parameters     :        Exception 
     *Returns        :        Void
    */
    public void displayMessage(Exception e){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
        ApexPages.addMessage(myMsg);    
    }//end method
    
    /*Method Name    :        getQuestionnaireWrapperRecords
     *Author         :        Deloitte Consulting
     *Description    :        Method to convert Loan Questionnaire records to Questionnaire Wrapper
     *Parameters     :        List<Loan_Questionnaire__c>
     *Returns        :        Void
    */
    public void getQuestionnaireWrapperRecords(List<Loan_Questionnaire__c> lstLoanQuestionnaire)
    {
        for(Loan_Questionnaire__c lq:lstLoanQuestionnaire){// pull the relevant information from loan questionnaire object and add it to the questionnaire records.
            Questionnaire__c Questionnaire = new Questionnaire__c(); 
            Questionnaire.id = lq.Questionnaire__c;
            Questionnaire.Question__c = lq.Questionnaire__r.Question__c;
            Questionnaire.Type__c = lq.Questionnaire__r.Type__c; 
            Questionnaire.Description__c = lq.Questionnaire__r.Description__c;
            Questionnaire.Answers__c = lq.Questionnaire__r.Answers__c;
            Questionnaire.Parent_Question__c = lq.Questionnaire__r.Parent_Question__c;
            
            //Generate the QuestionnaireWrapper 
            QuestionnaireWrapper QuestionWrapper = new QuestionnaireWrapper();
            QuestionWrapper.qn = Questionnaire;
            if(Questionnaire.Type__c=='Multi-Select')
                QuestionWrapper.Answers = convertStringtoArray(lq.Answer__c);
            QuestionWrapper.Answer = lq.Answer__c;
            QuestionWrapper.loanQuestionnaireId = lq.id;
            
            if(lq.Questionnaire__r.Parent_Question__c==null){
                mapIndentation.put(lq.Questionnaire__r.id,0);
            }
            
            //Add the wrapper to the list
            lstQuestionnaireWrapper.add(QuestionWrapper);
        }//end for
        if(lstQuestionnaireWrapper.size()>0){ //Null Check
            indentationValue(lstQuestionnaireWrapper);
            buildUtilityMaps(lstQuestionnaireWrapper);
        }//end if
    }//end method
    
    /*Method Name    :        getQuestionnaireWrapperRecords
     *Author         :        Deloitte Consulting
     *Description    :        Method to convert Questionnaire records to Questionnaire Wrapper
     *Parameters     :        List<Questionnaire__c>
     *Returns        :        Void
    */
    public void getQuestionnaireWrapperRecords(List<Questionnaire__c> lstQuestionnaire)
    {
        for(Questionnaire__c qr:lstQuestionnaire){
             QuestionnaireWrapper QuestionWrapper = new QuestionnaireWrapper();
             if(qr.Parent_Question__c==null){//get only the first level Questions
                 QuestionWrapper.qn = qr;
                 mapIndentation.put(qr.id,0);
                 lstQuestionnaireWrapper.add(QuestionWrapper);
             }//end if
             if(qr.Parent_Question__c!=null){//get only the dependent questions and add it to a map
                 list<Questionnaire__c> ChildQuestions = new List<Questionnaire__c>();
                 if(!mapDependentQuestion.containskey(qr.Parent_Question__c+'-'+qr.Parent_Answer__c)){// check to see if the map does not have the key before adding
                     ChildQuestions.add(qr);
                     mapDependentQuestion.put(qr.Parent_Question__c+'-'+qr.Parent_Answer__c,ChildQuestions);
                 }else if(mapDependentQuestion.containskey(qr.Parent_Question__c+'-'+qr.Parent_Answer__c)){ // check to see if the map has the key, if yes update the map accordingly
                     ChildQuestions =  mapDependentQuestion.get(qr.Parent_Question__c+'-'+qr.Parent_Answer__c);
                     ChildQuestions.add(qr); 
                     mapDependentQuestion.put(qr.Parent_Question__c+'-'+qr.Parent_Answer__c,ChildQuestions);  
                 }//end else if
             }//end if
         }//end for 
         if(lstQuestionnaireWrapper.size()>0) // Null check before calling the method
             buildUtilityMaps(lstQuestionnaireWrapper);   
    }//end method
    
    //Wrapper Class Definition
    public class QuestionnaireWrapper{
        public Id loanQuestionnaireId{set;get;}//required for update operation
        public Questionnaire__c qn{set;get;}//question object
        public String Answer{set;get;}//response to the question
        public String errorMessage{set;get;}//error message if any after validation
        public List<String> Answers{set;get;}{Answers = new List<String>();}//for answers of type list i.e. Multi-Select
        public String indentation {get;set;}
    } 
}