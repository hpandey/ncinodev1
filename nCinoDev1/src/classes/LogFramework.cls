/************************************************************************************
 Apex Class Name     : Log Framework
 Version             : 1.1
 Created Date        : 5th Jan 2016
 Function            : Custom class to manage exception/log related activities
 Author              : Jag Valaiyapathy (jvalaiyapathy@deloitte.com)
 Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Jag Valaiyapathy          1/5/2016                Original Version
* Jag Valaiyapathy          1/6/2016                Made improvements based on feedback from Ryan
* Jag Valaiyapathy          1/11/2016               Removing Static Classes and Inner Classes based 
                                                    on Ryan's and Faya's Feedback
*************************************************************************************/
public class LogFramework {
    
    public static final String EXCEPTION_LOG = 'ExceptionLog';
    public static final String WORK_LOG = 'WorkLog';
    public static final String HTTPREQUEST_LOG = 'HTTPRequestLog';
    public static final String HTTPRESPONSE_LOG = 'HTTPResponseLog';
    
    private List<Log__c> logListToBeSaved {get; set;}
    private List<Log__c> committedLogs    {get; set;}

    //Singleton Pattern is implemented to reduce instance of LogFramework Class.
    private static LogFramework singleLogFramework { 
        get {
            if (singleLogFramework == null) {
                singleLogFramework = new LogFramework();
            }
            return singleLogFramework;
        }
        set;
    }
    
    //Private Constructor
    private LogFramework() {
        logListToBeSaved = new List<Log__c>();
        committedLogs = new List<Log__c>();
    }

    /*
        PUBLICALLY EXPOSED METHODS
    */ 
    public static LogFramework getLogger() {
        return singleLogFramework;
    }
    
    public override String toString() {
        Map<String,List<Log__c>> logsToString = new Map<String,List<Log__c>>{
            'newLogs' => logListToBeSaved,
            'committedLogs' => committedLogs
        };
        return JSON.serialize(logsToString);
    }
    
    public void logSaveResultError(List<Database.SaveResult> results){
        formatLogInfo(results);
    }

    public void logDeleteResultError(List<Database.DeleteResult> results){
        formatLogInfo(results);
    }
    
    //This method can accept Strings.
    public void createWorkLogs(String s){
        createWorkLogs(new List<String>{s});
    }
    
    //This method can accept HTTPRequest Object.
    public void createWorkLogs(System.HTTPRequest req){
        createWorkLogs(new List<HTTPRequest>{req});
    }
    
    //This method can accept HTTPResponse Object.
    public void createWorkLogs(System.HTTPResponse res){
        createWorkLogs(new List<System.HTTPResponse>{res});
    }
    
    /*OVERLOADED METHODS*/
    public void createExceptionLogs(List<System.Exception> eList){
        formatLogInfo(eList);
    }

    public void createExceptionLogs(System.Exception eInst){
        formatLogInfo(eInst);
    }
    
    public void createWorkLogs(List<String> sList){
        formatWorkLogInfo(sList);
    }
    
    public void createWorkLogs(List<System.HTTPRequest> HTTPreqList){
        formatHTTPReqLogInfo(HTTPreqList);
    }
    
    public void createWorkLogs(List<System.HTTPResponse> HTTPresList){
        formatHTTPResLogInfo(HTTPresList);
    }
    
    //CommitLogs
    public void commitLogs(){
        
        if(!logListToBeSaved.isEmpty()){
            //Setting DML Options
            Database.DMLOptions dmlOpt = new Database.DMLOptions();
            dmlOpt.AllowFieldTruncation = true;
            dmlOpt.OptAllOrNone = false;
            
            Database.SaveResult[] svResultList = Database.insert(logListToBeSaved, dmlOpt); 
        
            for(Database.SaveResult svr : svResultList){
                if(svr.isSuccess()){
                    //All Good. Do nothing.
                }else{
                    //Custom Exception Save Logic failed.
                    for(Database.Error err : svr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that are affected in this error: ' + err.getFields());
                    }
                }
            }
            committedLogs.addAll(logListToBeSaved);
            logListToBeSaved.clear();
        }
    }
    
    /*PRIVATE METHODS*/
    private void formatLogInfo(List<System.Exception> eList){
        for(System.Exception eInst : eList){
            Log__c logInst = getLogRecordFromException(eInst);
            logListToBeSaved.add(logInst);
        }
    }

    private void formatLogInfo(System.Exception eList){
        
        Log__c logInst = getLogRecordFromException(eList);
        logListToBeSaved.add(logInst);
        
    }
    
    private void formatLogInfo(List<Database.SaveResult> results){
        for(Database.SaveResult result : results){
             if (!result.isSuccess()){
                Log__c logInst = new Log__c();
                for(Database.Error error : result.getErrors()){
                    logInst.Exception_Message__c += error.getMessage()+'/; ';
                    logInst.Exception_Type__c += error.getStatusCode()+'/; ';
                    for(String fieldNames : error.getFields()){
                        logInst.Field_Names__c += fieldNames+'/; ';
                    }
                }
                if(result.getId() != null){
                    logInst.Record_ID__c = result.getId(); 
                }
                logListToBeSaved.add(logInst);
            }
        }
    }
    
    private void formatLogInfo(List<Database.DeleteResult> results){
        for(Database.DeleteResult result : results){
             if (!result.isSuccess()){
                Log__c logInst = new Log__c();
                for(Database.Error error : result.getErrors()){
                    logInst.Exception_Message__c += error.getMessage()+'/; ';
                    logInst.Exception_Type__c += error.getStatusCode()+'/; ';
                    for(String fieldNames : error.getFields()){
                        logInst.Field_Names__c += fieldNames+'/; ';
                    }
                }
                if(result.getId() != null){
                    logInst.Record_ID__c = result.getId(); 
                }
                logListToBeSaved.add(logInst);
            }
        }
    }
    
    private void formatHTTPReqLogInfo(List<System.HTTPRequest> HTTPReqList){
        
        for(HTTPRequest req: HTTPReqList){

            Log__c logInst              = new Log__c();
            logInst.Log_Type__c         = LogFramework.HTTPREQUEST_LOG;
            logInst.HTTP_Body__c        = req.getBody();
            logInst.HTTP_EndPoint__c    = req.getEndpoint();
            logInst.HTTP_Method__c      = req.getMethod();
            
            logListToBeSaved.add(logInst);
        }
        
    }
    
    private void formatHTTPResLogInfo(List<System.HTTPResponse> HTTPResList){
        
        for(HTTPResponse res: HTTPResList){

            Log__c logInst              = new Log__c();
            logInst.Log_Type__c         = LogFramework.HTTPRESPONSE_LOG;
            logInst.HTTP_Body__c        = res.getBody();
            logInst.HTTP_Status__c      = res.getStatus();
            logInst.HTTP_Status_Code__c = res.getStatusCode();
            
            //Retrieve Header Keys
            List<String> headerkeys     = res.getHeaderKeys();
            Map<String, String> headers = new Map<String, String>();//Store header Key/Value information

            if(headerkeys!=null && !headerkeys.isEmpty()) {
                
                for(String s: headerkeys) {
                    if(s!=null && res.getHeader(s)!=null) {
                        headers.put(s, res.getHeader(s));
                    }
                }
            }
            
            logListToBeSaved.add(logInst);
        }
    }
    
    private void formatWorkLogInfo(List<String> sList){
        for(String s: sList){
            Log__c logInst = new Log__c();
            
            //Detailed Log Specific fields
            logInst.Developer_Comments__c = s;
            logInst.Log_Type__c = LogFramework.WORK_LOG;
            
            logListToBeSaved.add(logInst);
        }
    }

    private Log__c getLogRecordFromException(System.Exception eInst){

        Log__c logInst = new Log__c();
        logInst.Line_Number__c = eInst.getLineNumber(); //Line Number
        logInst.Exception_Type__c = eInst.getTypeName(); //Exception Type
        logInst.Exception_Message__c = eInst.getMessage(); //Exception Message
        logInst.Stack_Trace__c = eInst.getStackTraceString(); //Stack Trace
        logInst.Log_Type__c = LogFramework.EXCEPTION_LOG; //Log Type
        logInst.sObject_Output__c = String.Valueof(eInst); //Raw Stringified output of object
        
        //Capturing additional Infomation and taking advantage of additional methods exposed by Salesforce
        If(eInst.getTypeName() == 'System.DmlException'){
            for(Integer i = 0; i < eInst.getNumDML(); i++){
                logInst.DML_Field_Names__c  += i + ': ' + eInst.getDmlFieldNames(i) + '\n';
                logInst.DML_Message__c += i + ': ' + eInst.getDmlMessage(i) + '\n';
                logInst.DML_ID__c += i + ': ' + eInst.getDmlId(i) + '\n';
            }
        }
        return logInst;
    }
}