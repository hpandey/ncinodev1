//Edited to deploy under the oneview admin

public with sharing class CallPlanEmailAccountTeamTriggerHandler {
    public void SendAccountTeamEmails(List<Call_Plan__c> objcallplan){
            //helper class for bulk messages
            List<Call_Plan__c> callplan = new List<Call_Plan__c>();
            for( Call_Plan__c obj : objcallplan )
            {
                callplan.add(obj);
            }
            if(callplan.size() > 0){
                if(callplan[0].Status__c == 'Completed' && callplan[0].Changes_Business_or_Financial_Profile__c == 'Yes'){
                    integer cntr = 0;
                    String EmailFailure;
                    
                    //query on AccountTeamMember object for those people who are associated with the account
                    List<AccountTeamMember> TeamMembers = [select userid from AccountTeamMember where AccountId = :callplan[0].Client__c];
                
                    //query on template object
                    //EmailTemplate et=[Select id,body,subject from EmailTemplate where name=:'Emerging Risk Factor Identified'];
                    List<Account> ClientName = [Select Name from Account where Id=:callplan[0].Client__c];
      
                   //query on template object
                    //EmailTemplate et=[Select id,body,subject from EmailTemplate where name=:'Emerging Risk Factor Identified'];
                    List<Account> ClientOwner = [SELECT Owner_ID__c FROM Account where id=:callplan[0].Client__c];
                     
                    //list of emails
                    List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
                    String CPWebAddress = URL.getSalesforceBaseUrl().toExternalForm()+'/'+callplan[0].Id;
                    
                    //loop
                    //Send email to the client owner
                    for(Account Owner : ClientOwner){
                        //system.debug('Client Owner ID = ' + Owner.Owner_ID__c);                        
                        //check for Account
                        if(Owner.Owner_ID__c != null){
    
                            //initiallize messaging method
                            Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
        
                            //set object Id
                            singleMail.setTargetObjectId(Owner.Owner_ID__c);
                            //system.debug('MemberId = ' + Member.UserId);                        
                        
                            //flag to false to stop inserting activity history
                            singleMail.setSaveAsActivity(false);
                            singleMail.setSubject('Emerging Risk Factor Identified - Please Review - '+ClientName[0].Name);
                            singleMail.setPlainTextBody('As a member of the client team for '+ClientName[0].Name+', you are receiving this email '+
                                                        'as a notification that an emerging risk factor has been identified on a call plan. Please '+
                                                        'click on the link below to view the emerging risk factor information on the Call Plan.' + 
                                                        '\n\n'+'Reminder - call plans may contain sensitive information and in cases of emerging ' +
                                                        'risks, the information should be held strictly confidential. Should you have any questions ' +
                                                        'about the emerging risks, please contact the associate that entered the information in the ' +
                                                        'call plan.'+'\n\n'+'This emerging risk identification email can also be forwarded to other ' +
                                                        'members of either the First or Second Lines of Defense as needed.'+'\n\n\n'+CPWebAddress+'\n\n'+
                                                        'OneView Administrator');
                            //add mail
                            emails.add(singleMail);
                            cntr++;
                        }
                    }
                    
                    //loop
                    for(AccountTeamMember Member : TeamMembers){
            
                        //check for Account
                        if(Member.Id != null){

                            //initiallize messaging method
                            Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
        
                            //set object Id
                            singleMail.setTargetObjectId(Member.UserId);
                            //system.debug('MemberId = ' + Member.UserId);                        
                        
                            //flag to false to stop inserting activity history
                            singleMail.setSaveAsActivity(false);
                            singleMail.setSubject('Emerging Risk Factor Identified - Please Review - '+ClientName[0].Name);
                            singleMail.setPlainTextBody('As a member of the client team for '+ClientName[0].Name+', you are receiving this email '+
                                                        'as a notification that an emerging risk factor has been identified on a call plan. Please '+
                                                        'click on the link below to view the emerging risk factor information on the Call Plan.' + 
                                                        '\n\n'+'Reminder - call plans may contain sensitive information and in cases of emerging ' +
                                                        'risks, the information should be held strictly confidential. Should you have any questions ' +
                                                        'about the emerging risks, please contact the associate that entered the information in the ' +
                                                        'call plan.'+'\n\n'+'This emerging risk identification email can also be forwarded to other ' +
                                                        'members of either the First or Second Lines of Defense as needed.'+'\n\n\n'+CPWebAddress+'\n\n'+
                                                        'OneView Administrator');
                            //add mail
                            emails.add(singleMail);
                            cntr++;
                            
                            if(cntr==10){
                                try {
                                    Messaging.sendEmail(emails);
                                    //system.debug('CNTR reached 10');
                                }
                                catch(System.EmailException emlEx) {
                                    EmailFailure = 'Email Failed: ' + emlEx;
                                    //system.debug('EXCEPTION CAUGHT' + emlEx);
                                }
                                emails = new List<Messaging.SingleEmailMessage>();
                                //system.debug('NEW LIST CREATED');
                                cntr=0;
                                //system.debug('CNTR RESET');
                            }
                        }
                    }
                    
                    //send mail
                    Messaging.sendEmail(emails);
                    //system.debug('FINAL EMAILS SENT');
                }
            }
    }
}