@isTest
private class TestScheduleEmailNotification {
   // CRON expression: midnight on March 15.   
   // Because this is a test, job executes   
   // immediately after Test.stopTest().   
   public static String CRON_EXP = '0 0 0 15 3 ? 2022';
   static testmethod void test() {
      Test.startTest();
      // Schedule the test job
      String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new ScheduleEmailNotification());         
      // Get the information from the CronTrigger API object
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
      // Verify the expressions are the same
      System.assertEquals(CRON_EXP, 
         ct.CronExpression);
      // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);
      // Verify the next time the job will run
      System.assertEquals('2022-03-15 00:00:00', 
         String.valueOf(ct.NextFireTime));
      // Verify the scheduled job hasn't run yet.
      
      SendMail();
      Test.stopTest(); 
   }
   static testmethod void sendMail() { 
       Account testAcc = new Account();
       testAcc.Name = 'Test Account';
       insert testAcc;
       
       Bank_Account_Details__c testBA = new Bank_Account_Details__c();
       testBA.Client__c = testAcc.Id;
       testBA.Name = 'Scooby Doo';
       testBA.Maturity_Date__c = date.today();
       insert testBA;
       
       Task testTask = new Task();
       testTask.Subject = 'Bank Account up for Renewal';
       testTask.Task_Type__c = 'Renewal/Financial Review';
       testTask.Status = 'Not Started';
       testTask.WhatId = testBA.Id;
       testTask.ActivityDate= date.today().addDays(-1);
       insert testTask;
       
       List<Task> tasks7day = [SELECT Id, Subject, Status, CreatedDate, ActivityDate, OwnerId, WhatId  from task WHERE Status = 'Not Started' AND Subject='Bank Account up for Renewal'];
       System.assertEquals(tasks7day.size(), 1);

       if(tasks7day.size() > 0){        
           List<Messaging.SingleEmailMessage> listMail = new List<Messaging.SingleEmailMessage>();
           Boolean donotSend = true;        
           for(Task t : tasks7day){
               String acctId = t.WhatId;
               String taskId = t.Id;
               String tOwnerId = t.OwnerId;
               Datetime activityDate = t.ActivityDate;
               String dueDate = activityDate.format('MM/dd/yyyy');       
               String fullUrl;
               String[] toAddresses;
               Boolean yesSM = false;
               Boolean yesSE = false;
               String subject = 'Bank Account up for Renewal';                
               String description = 'Past Due Notification Test.';
               Bank_Account_Details__c[] acc = [SELECT ID, Name FROM Bank_Account_Details__c WHERE Id = :acctId];
               User[] tUser = [SELECT ID, Name, Sales_Manager__c, Sales_Executive__c FROM User Where Id = :tOwnerId];
               tUser[0].Sales_Manager__c = '005G0000002kaJRIAY';
               tUser[0].Sales_Executive__c = '005G0000002kaJRIAY';
               Update tUser; 
               User[] SMuser; 
               User[] SEuser; 
                             
               if(tUser[0].Sales_Manager__c != null){
                    SMuser = [SELECT ID, Name, Email FROM User WHERE Id = :tUser[0].Sales_Manager__c];
                    yesSM = true;
                }
                if(tUser[0].Sales_Executive__c != null){    
                    SEuser = [SELECT ID, Name, Email FROM User WHERE Id = :tUser[0].Sales_Executive__c];
                    yesSE = true;
                }                
                                            
               if(taskId != null){
                   fullUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+taskId; 
               }
               if (fullUrl.startsWith('http:')){
                   fullUrL = fullUrL.replaceFirst('http:', 'https:');
               }
               Messaging.SingleEmailMessage mailMessage = new Messaging.SingleEmailMessage();
               if(yesSM){
                    toAddresses = new String[]{SMuser[0].Email};
                    mailMessage.setToAddresses(toAddresses);
                    donotSend = false;
                }else if(yesSE){
                    toAddresses = new String[] {SEuser[0].Email};
                    mailMessage.setToAddresses(toAddresses); 
                    donotSend = false;
                }else{
                    donotSend = true;
                    break;
                }
               //String[] toAddresses = new String[] {'me@email1.com','you@email2.com'};                
               //mailMessage.setToAddresses(toAddresses);
               mailMessage.setSenderDisplayName('RegionsOneView Administrator'); 
               mailMessage.setSubject(subject);    
               mailMessage.setPlainTextBody('Subject:  '+subject+'<br/>' + 'Bank Account: '+ acc[0].Name + '<br/>'+ 'Business Banker: Test Banker'+'<br/>'+ 'Task Due Date: '+dueDate +'<br/><br/>'+ 'Comments: '+description + '<br/><br/>'+'For more details, click the following link:<br/><br/>'+fullUrl);
               mailMessage.setHtmlBody('Subject:  '+subject+'<br/>' + 'Bank Account: '+ acc[0].Name + '<br/>'+ 'Account Owner: Test Owner'+'<br/>' +'Task Due Date: '+dueDate+'<br/><br/>'+ 'Comments: '+description +'<br/><br/>'+'For more details, click the following link:<br/><br/>'+fullUrl);
               listMail.add(mailMessage);                             
            }
            if(!donotSend){
                Messaging.sendEmail(listMail);
                System.assertEquals(listMail.size(), 1);
            }else{}
       }
   }  
}