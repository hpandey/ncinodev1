/*
    Things to test
    - Recalculate Client(Account) Sharing & Recalculate Relationship Sharing
    - Recalcualte Client + Relationship from User Default Team Change
    - Triggers for
        > New PDM
        > New Call Plans
        > New Relationship Strategy Plan
        
    Need to create a function that does the following
    1. Creates a standard user (To test recalculates for owners of Relationship & Client records and sub records)
        a. TstGuy1 (Original owner of records to be created)
        b. TstGuy2 (To give us someone to transfer ownership to)
        c. TstGuy1a (Just to have someone to add on the client team for TstGuy1)
        d. TstGuy1b (Just to have someone to add on the client team for TstGuy1)
        e. TstGuy1c (Just to have someone to add on the client team for TstGuy1)
        f. TstGuy2a (Just to have someone to add on the client team for TstGuy2)
        g. TstGuy2b (Just to have someone to add on the client team for TstGuy2)
        h. TstGuy2c (Just to have someone to add on the client team for TstGuy2)
    2. Creates an admin user (To test recalculates without failures)
    3. Creates a data migration user (Test triggers for adding Relationships to the Nightly Share Table) >> TODO
    4. Create records
        a. Relationship as user from STEP #1a
        b. Client as user from STEP #1a
        c. PDM, Relationship Strategy Plan and Call Plan as STEP #1a (Trigger Test)
        d. Bank Account Details and Restricted Client Information
    5. Run the Recalculate Sharing code as the user from STEP #1
        5a. Delete a user from the share and update sharing. >>> FOUND A BUG!
    6. Change the ownership on the Relationship and Client from STEP #4 as the user from STEP #3 (This will populate the Nightly Share Table with a record)
    7. Run the NightlyBatch as the user from STEP #2
    8. Insert a new Relationship with all sub records as the Data Migration user to fire the Nightly Share Table triggers.
*/
@isTest
private class TestNightlyShareBatch {
    static User testUser;
    static User testSysAdminUser;
    static User TstGuy1;
    static User TstGuy2;
    static List<User> TstGuy1Team;
    static List<User> TstGuy2Team;
    static List<User> testUserList;
    static Relationship__c relationship;
    static Relationship__c relationship2;
    static Account acct;
    static List<AccountTeamMember> atmList;
    static Call_Plan__c ccp;
    static Bank_Account_Details__c bad;
    static Restricted_Client_Information__c rci;
    static Restricted_Relationship_Information__c rri;
    static PDM__c pdm;
    static Relationship_Strategy_Plan__c rsp;
    
    static {    
        Set<string> testUsers = new Set<string>{'TstGuy1','TstGuy1a','TstGuy1b','TstGuy1c','TstGuy2','TstGuy2a','TstGuy2b','TstGuy2c'};
        Set<string> TstGuy1Defaultgroup = new Set<string>{'TstGuy1a','TstGuy1b','TstGuy1c'};
        Set<string> TstGuy2Defaultgroup = new Set<string>{'TstGuy2a','TstGuy2b','TstGuy2c'};
        testUserList = new List<User>();
        TstGuy1Team = new List<User>();
        TstGuy2Team = new List<User>();
        Profile p = [select id from profile where name='Regions Standard User'];
        UserRole r = new UserRole(Name='Dummy Role');
        insert r;
        //[SELECT Id, Name from userrole where name = 'MA-Missouri Iowa West Kentucky-RM'];
        for (string user:testUsers){
            testUser = new User(alias = user, email= user + '@testorg.com',
              emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
              localesidkey='en_US', profileid = p.Id, country='United States',
              timezonesidkey='America/Los_Angeles', username= user + '@testorg.com', UserRoleId=r.id);
         
            insert testUser;
            testUserList.add(testUser);
            if (testUser.Alias == 'TstGuy1'){
                TstGuy1 = testUser;
            }else if(testUser.Alias == 'TstGuy2'){
                TstGuy2 = testUser;
            }else if(TstGuy1Defaultgroup.Contains(testUser.Alias)){
                TstGuy1Team.add(testUser);
            }else if(TstGuy2Defaultgroup.Contains(testUser.Alias)){
                TstGuy2Team.add(testUser);
            }
        }
        System.debug('TstGuy1Team >> ' + TstGuy1Team);
        System.debug('TstGuy1 >> ' + TstGuy1);
        
        testUserList = [Select id from User Where alias in :testUsers];
        Profile p2 = [select id from profile where name='System Administrator'];
            testSysAdminUser = new User(alias = 'SysAdmin', email= 'SysAdmin@dummy.com',
              emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
              localesidkey='en_US', profileid = p2.Id, country='United States',
              timezonesidkey='America/Los_Angeles', username= 'SysAdmin@dummy.com');
         
        insert testSysAdminUser;
        System.runAs(TstGuy1) {
            relationship = new Relationship__c(Relationship_ID__c='3456gbjds44');
            insert relationship;
            
            relationship2 = new Relationship__c(Relationship_ID__c='3456gbjds45');
            insert relationship2;
            
            acct= new Account(name='testAccount',Relationship__c=relationship.id);
            insert acct;
            
            atmList = new List<AccountTeamMember>();
            for (User TeamMember : TstGuy1Team){
                AccountTeamMember atm = new AccountTeamMember(AccountId=acct.id,userId=TeamMember.Id);
                atmList.add(atm);
            }
            insert atmList;
            
            ccp = new Call_Plan__c(client__c=acct.id);//client call plan
            insert ccp;
            
            bad= new Bank_Account_Details__c(client__c=acct.id);
            insert bad; 
        
            rci= new Restricted_Client_Information__c(client__c=acct.id);
            insert rci;
            
            rri = new Restricted_Relationship_Information__c(Relationship__c=relationship.id); 
            insert rri;
            
            pdm = new PDM__c(Relationship__c=relationship.id,Date_Completed__c=Date.today()); 
            insert pdm;
            
            rsp = new Relationship_Strategy_Plan__c(relationship__c=relationship.id);
            insert rsp; 
        }
    }

    
    
    static testMethod void runNightlyBatchAsAdmin() {
        Test.startTest();
        // Schedule the test job 
        String jobId = System.schedule('testSchedule','0 0 0 3 9 ? 2022', new NightlyBatchScheduler());
       // Get the information from the CronTrigger API object 
       CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered,NextFireTime
         FROM CronTrigger WHERE id = :jobId];
       // Verify the expressions are the same 
       System.assertEquals('0 0 0 3 9 ? 2022', ct.CronExpression);
      // Verify the job has not run 
       System.assertEquals(0, ct.TimesTriggered);
      // Verify the next time the job will run 
    
      System.assertEquals('2022-09-03 00:00:00', 
         String.valueOf(ct.NextFireTime));
         
      NightlyShareBatch b = new NightlyShareBatch(); 
      database.executebatch(b);
        
        Test.stopTest();
    }
       
    static testMethod void userDefaultTeamUpdate(){
        System.runAs(TstGuy1) {
            //DML not allowed on UserAccountTeamMember table.
            /*UserAccountTeamMember newTeamMember = new UserAccountTeamMember();
            newTeamMember = new UserAccountTeamMember(ownerid=TstGuy1.Id, UserId=TstGuy2Team[0].id, AccountAccessLevel='Edit');
            insert newTeamMember;*/
            
            AccountTeamMember atmUser = new AccountTeamMember(AccountId=acct.id,userId=TstGuy2Team[0].id);
            insert atmUser;
            //TstGuy2Team[0].id hasn't been added yet so it should return empty
            System.AssertEquals([select id from Bank_Account_Details__Share where UserOrGroupID=: TstGuy2Team[0].id].isEmpty(),true);
            System.AssertEquals([select id from Restricted_Client_Information__Share where UserOrGroupID=: TstGuy2Team[0].id].isEmpty(),true);
            System.AssertEquals([select id from Relationship__Share where UserOrGroupID=: TstGuy1Team[0].id and parentid =: pdm.id and IsDeleted=false].isEmpty(),true);
            System.AssertEquals([select id from PDM__Share where UserOrGroupID=: TstGuy1Team[0].id and parentid =: pdm.id and IsDeleted=false and RowCause='ASR_Relationship_Details__c'].isEmpty(),true);
            System.AssertEquals([select id from Restricted_Relationship_Information__Share where UserOrGroupID=: TstGuy1Team[0].id and RowCause='ASR_Relationship_Details__c'].isEmpty(),true);
            System.AssertEquals([select id from Relationship_Strategy_Plan__Share where UserOrGroupID=: TstGuy1Team[0].id and RowCause='ASR_Relationship_Details__c'].isEmpty(),true);      
            
            UserAccountDetailsVisibilityBatch.ExecuteFinalizeTemp(TstGuy1.Id);
            //Relationship__c Query =[SELECT id, OwnerID from Relationship__c where OwnerId =: TstGuy1.Id];
            //UserAccountDetailsVisibilityBatch.start(Query);
            UserAccountDetailsVisibilityBatch emptyBatch= new UserAccountDetailsVisibilityBatch(null,null);
            UserAccountDetailsVisibilityBatch batch= new UserAccountDetailsVisibilityBatch(TstGuy1.Id);
            Id processID= Database.executeBatch(batch);
            //TstGuy2Team[0].id has just been added so it should not return empty
            /*
            System.AssertEquals([select id from Bank_Account_Details__Share where UserOrGroupID=: TstGuy2Team[0].id].isEmpty(),false);
            System.AssertEquals([select id from Restricted_Client_Information__Share where UserOrGroupID=: TstGuy2Team[0].id].isEmpty(),false);
            System.AssertEquals([select id from Relationship__Share where UserOrGroupID=: TstGuy1Team[0].id and parentid =: pdm.id and IsDeleted=false].isEmpty(),false);
            System.AssertEquals([select id from PDM__Share where UserOrGroupID=: TstGuy1Team[0].id and parentid =: pdm.id and IsDeleted=false and RowCause='ASR_Relationship_Details__c'].isEmpty(),false);
            System.AssertEquals([select id from Restricted_Relationship_Information__Share where UserOrGroupID=: TstGuy1Team[0].id and RowCause='ASR_Relationship_Details__c'].isEmpty(),false);
            System.AssertEquals([select id from Relationship_Strategy_Plan__Share where UserOrGroupID=: TstGuy1Team[0].id and RowCause='ASR_Relationship_Details__c'].isEmpty(),false);
            */
        }
    }
}