public class scorecardHandler {
    public scorecardHandler(){
        //Handler
    }
    public void shareScorecard(List<Scorecard__c> NewRecords){
        // Create a new list of sharing objects for Job
        List<Scorecard__Share> scShrs = new List<Scorecard__Share>();
        List<Scorecard__Share> sharesToDelete = new List<Scorecard__Share>();
        
        // Declare variable for participant sharing
        Scorecard__Share scShr;
        
        for(Scorecard__c sc : NewRecords){
            if (sc.Scorecard_Status__c=='Final'){
                // Instantiate the sharing object
                scShr = new Scorecard__Share();
                
                // Set the ID of record being shared
                scShr.ParentId = sc.Id;
                
                // Set the ID of user or group being granted access
                scShr.UserOrGroupId = sc.Credit_Underwriter__c;
                
                // Set the access level
                scShr.AccessLevel = 'read';
                
                // Set the Apex sharing reason for call plan participant
                scShr.RowCause = Schema.Scorecard__Share.RowCause.Scorecard_Final__c;
                
                // Add objects to list for insert
                scShrs.add(scShr);
            }else{
                LIST<Scorecard__Share> delShare = [SELECT Id FROM Scorecard__Share
                                                                    WHERE ParentId = :sc.Id
                                                                    AND RowCause = :Schema.Scorecard__Share.RowCause.Scorecard_Final__c];
                sharesToDelete.addAll(delShare);
            }
        }
        // Insert sharing records and capture save result 
        // The false parameter allows for partial processing if multiple records are passed 
        // into the operation 
        if(!scShrs.isEmpty()){
            Database.SaveResult[] lsr = Database.insert(scShrs,false);
        
            // Create counter
            Integer i=0;
            
            // Process the save results
            for(Database.SaveResult sr : lsr){
                if(!sr.isSuccess()){
                    // Get the first save result error
                    Database.Error err = sr.getErrors()[0];
                    
                    // Check if the error is related to a trivial access level
                    // Access levels equal or more permissive than the object's default 
                    // access level are not allowed. 
                    // These sharing records are not required and thus an insert exception is 
                    // acceptable. 
                    if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                                                   &&  err.getMessage().contains('AccessLevel'))){
                        // Throw an error when the error is not related to trivial access level.
                        trigger.newMap.get(scShrs[i].ParentId).
                          addError(
                           'Unable to grant sharing access due to following exception: '
                           + err.getMessage());
                    }
                }
                i++;
            }
        }
        if(!sharesToDelete.isEmpty()){
            Database.Delete(sharesToDelete, false);
        }
    }
}