global class NightlyShareBatch implements Database.batchable<sObject>, Database.Stateful{
    global final String Query;
    global List<ID> relId;
   
    global NightlyShareBatch(String q){
       Query= q;
       relId = new List<ID>();
    }
    global NightlyShareBatch(){
       Query = 'SELECT id, OwnerID  from Relationship__c where Id in (Select Relationship__c From Sharing_Batch__c where batch_changed__c=null)';
       relId = new List<ID>();
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Relationship__c> relationshipList){
        AccountDetailsVisibilityUtil.recalculateUserSharing(relationshipList);
        for (Relationship__c r : relationshipList){
            relId.add(r.Id);
        }
    }

    global void finish(Database.BatchableContext BC){
      AsyncApexJob aaj = [select Id, ApexClassId, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors, 
        CompletedDate, MethodName, ExtendedStatus 
        from AsyncApexJob where Id=:BC.getJobId()];
        
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
         
      //update records in sharing_batch__c table with timestamp
      List<sharing_batch__c> sbList = [SELECT id, Batch_Changed__c from Sharing_Batch__c where batch_changed__c = null];
      for (integer i=0; i<sbList.size(); i++) {
            sbList[i].Batch_Changed__c=date.today();
      } 
      update sbList;

      if (aaj.NumberofErrors==0){
        
        //Send mail on success if specified in the custom setting 
         if (AdminEmail__c.getValues('NightlyShare')!=null &&
                 AdminEmail__c.getValues('NightlyShare').MailOnSuccess__c==true ){
             
           String[] toAddresses = new String[] {AdminEmail__c.getValues('NightlyShare').Email__c};  
           mail.setToAddresses(toAddresses);
           mail.setSubject('SUCCESS : NightlyShareBatch Job Run');  
           mail.setPlainTextBody('The batch Apex job processed ' + aaj.TotalJobItems +  
          ' batches successfully.');  
      
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
        }    
      }
      //Send Mail on failure if specified in the custom setting.
      else if (   aaj.NumberofErrors>0 &&
                  AdminEmail__c.getValues('NightlyShare')!=null && 
                  AdminEmail__c.getValues('NightlyShare').MailOnFailure__c==true ) {
           
           String[] toAddresses = new String[] {AdminEmail__c.getValues('NightlyShare').Email__c};  
           mail.setToAddresses(toAddresses);
           mail.setSubject('FAILED : NightlyShareBatch Job Run');  
           mail.setPlainTextBody('The batch Apex job processed ' + aaj.TotalJobItems +  
          ' batches with '+ aaj.NumberOfErrors + ' failures. ExtendedStatus: ' + aaj.ExtendedStatus);  
       
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
      }
      
      //Delete Sharing_Batch__c records which are processed and older than 7 Days          
        List<Sharing_Batch__c> delSB = [SELECT id from Sharing_Batch__c where Batch_Changed__c<: date.today().addDays(-7)];
      delete delSB;
       
    }
   
    WebService static void ExecuteFinalizeTemp(){
      Id batchInstanceId = Database.executeBatch(new NightlyShareBatch(), 50);
    }
}