/************************************************************************************
 Apex Class Name     : LoanServiceTest
 Version             : 1.0
 Created Date        : 18th Feb 2016
 Function            : Test Class for LoanService
 Author              : Deloitte 
 Modification Log    :
* Developer                         Date                   Description
* ----------------------------------------------------------------------------                 
* Pranil Thubrikar                 2/18/2016                Original Version
*************************************************************************************/

@isTest(SeeAllData=false)
public class LoanServiceTest{
    
    /************************************************************************
    Method Name: setTestData
    Author Name: Deloitte
    Description: Method to set up the Test data for the test methods.
    Parameters: none
    Returns: void
    *************************************************************************/
    @testSetup
    static void setupTestData(){
        //Insert Custom Setting.
        Trigger_Setting__c loanEntry = new Trigger_Setting__c(Name='LLC_BI__Loan__c',isActive__c =true);
        if(loanEntry != null){
            insert loanEntry;
        }
        
        //Insert Field Map
        LLC_BI__Field_Map__c setupfMap = new LLC_BI__Field_Map__c(Name = 'LoanToPipelineSync', LLC_BI__Orig_Obj_Field__c = 'Name', LLC_BI__Target_Obj_Field__c = 'Name');
        if(setupfMap != null){
            insert setupfMap;
        }
        
        //Insert Pipeline. Record created from TestDataFactory class.
        List<Opportunity> setupTestPipeline = TestDataFactory.createPipeline(1);
        
        //Insert Loan. Record created from TestDataFactory class.
        LLC_BI__Loan__c setupTestLoan = TestDataFactory.createLoanWithoutID();
        setupTestLoan.Is_Converted__c  = true;
        if(setupTestLoan != null){
            insert setupTestLoan;
        }
    }
    
    /************************************************************************
    Method Name: loanToPiplineSyncMethodTest_positive
    Author Name: Deloitte
    Description: Test Method to positive test 'loanToPiplineSyncMethod' Method in LoanService Class.
    Parameters: none
    Returns: void
    *************************************************************************/
    public testMethod static void loanToPiplineSyncMethodTest_positive(){
        
        Test.startTest();
            List<LLC_BI__Loan__c> testLoan = [SELECT Id, Name FROM LLC_BI__Loan__c LIMIT 5];
            
            List<Opportunity> testPipeline = [SELECT Id, LLC_BI__Loan__c FROM Opportunity LIMIT 5];
            testPipeline[0].LLC_BI__Loan__c = testLoan[0].Id;
            if(testPipeline != null){
                update testPipeline;
            }
            
            //Update Loan fields according to field Map.
            testLoan[0].Name = 'Test Loan New';
            if(testLoan != null){
                update testLoan;
            }
            
            //Querying the updated Opportunity Record.
            List<Opportunity> newOpp = [SELECT Id, Name FROM Opportunity LIMIT 1];
            
            //Assert the Name for previously queried Opportunity Record with Loan Record. 
            System.assertEquals(newOpp[0].Name, testLoan[0].Name);
        
        Test.stopTest();
    }
    
    /************************************************************************
    Method Name: loanToPiplineSyncMethodTest_negative
    Author Name: Deloitte
    Description: Test Method to negative test 'loanToPiplineSyncMethod' Method in LoanService Class.
    Parameters: none
    Returns: void
    *************************************************************************/
    public testMethod static void loanToPiplineSyncMethodTest_negative(){
        
        Test.startTest();
           
            //Changing Target Object Field to Integer Type field so that exception is caught in main Method.
            List<LLC_BI__Field_Map__c> fMap = [SELECT Id, LLC_BI__Target_Obj_Field__c FROM LLC_BI__Field_Map__c LIMIT 5];
            fMap[0].LLC_BI__Target_Obj_Field__c = 'AccountId';
            if(fMap != null){
                update fMap;
            }
            
            List<LLC_BI__Loan__c> testLoan = [SELECT Id, Name FROM LLC_BI__Loan__c LIMIT 5];
            
            List<Opportunity> testPipeline = [SELECT Id, LLC_BI__Loan__c FROM Opportunity LIMIT 5];
            testPipeline[0].LLC_BI__Loan__c = testLoan[0].Id;
            if(testPipeline != null){
                update testPipeline;
            }
            
            //Updating Loan fields according to field Map. This condition will not update the Opportunity and error will be thrown.
            testLoan[0].Name = 'Test Loan New - 1';
            if(testLoan != null){
                update testLoan;
            }
                
        Test.stopTest();
    }
}