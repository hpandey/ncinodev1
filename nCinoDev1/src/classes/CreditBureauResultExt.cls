public Class CreditBureauResultExt{
    
    public String strAccId;
    public String strCreditResId{get;set;}
    public Credit_Result__c selectedResult{get;set;}
    public Account accRec{get;set;}
    public Boolean boolStep1{get;set;}
    public Boolean boolStep2{get;set;}
    public Boolean boolStep3{get;set;}
    public Boolean boolOpenRFI{get;set;}
    public Boolean boolNoOpenRFI{get;set;}
    public Boolean boolLoanLinkMsg{get;set;}
    public Boolean boolSelectCBR{get;set;}
    public Boolean boolSelectLoan{get;set;}
    public List<LLC_BI__Loan__c> listRelatedLoan{get;set;}
    public List<LoanWrapper> listLoanWrap{get;set;}
    public List<Credit_Result__c> listCreditResult{get;set;}
    public List<Regulatory_and_Fraud_Indicators__c> listOpenRFI{get;set;}
    public List<String> listResult = new List<String>();
    public String strCurrentBureau{get;set;}
    public List<SelectOption> bureauOptions{get;set;}
    public String strCurrentRationale{get;set;}
    public List<SelectOption> rationaleOptions{get;set;}
    public String strPin{get;set;}
    public String response{get;set;}
    private String returnedContinuationId;
    
    public CreditBureauResultExt(ApexPages.StandardController controller){
        
        strAccId = ApexPages.currentPage().getParameters().get('Id');
        boolStep1 = false;
        boolStep2 = false;
        boolStep3 = false;
        boolOpenRFI = false;
        boolLoanLinkMsg = false;
        boolSelectCBR = false;
        boolSelectLoan = false;
        if(strAccId != null && strAccId != ''){
            
            //Add appropriate result in list
            listResult.add(Label.Passed);
            listResult.add(Label.Rejected);
            
            getSelectOptions();
            
            init();
        }
    }
    
    public void getSelectOptions(){
        bureauOptions = new List<SelectOption>();
        rationaleOptions = new List<SelectOption>();
        for(Integer i=1;i<5;i++){
            bureauOptions.add(new SelectOption('Bureau '+String.valueOf(i), 'Bureau '+String.valueOf(i)));
            rationaleOptions.add(new SelectOption('Rationale '+String.valueOf(i), 'Rationale '+String.valueOf(i)));
        }
    }
    
    public void init(){
        
        if(strAccId != null && strAccId != ''){
            
            
            
            accRec = [SELECT Id,
                             (SELECT Id, Credit_Bureau__c, Date_of_Report__c, Error_Details__c , Score__c
                              FROM Credit_Result__r
                              WHERE Date_of_Report__c >:(System.Today()-15)),
                             (SELECT Id, Entity__r.Name, Flag_Type__c, Name, Result__c, Loan__r.Name
                              FROM Prescreening_Results__r
                              WHERE Result__c NOT IN :listResult)
                      FROM Account
                      WHERE Id = :strAccId];
            
            if(accRec != null){
                if(!accRec.Credit_Result__r.isEmpty()){
                    system.debug('####accRec loans : '+accRec.Loans__r);
                    listCreditResult = new List<Credit_Result__c>();
                    listCreditResult.addAll(accRec.Credit_Result__r);
                    boolStep1 = true;
                }
            }
        }
    }
    
    public pagereference getCreditResId(){
        strCreditResId = ApexPages.currentPage().getParameters().get('creditResId');
        system.debug('#####strCreditResId : '+strCreditResId);
        return null;
    }
    
    public void linkToLoan(){
    system.debug('#####strCreditResId : '+strCreditResId);
        if(strCreditResId == null){
             boolSelectCBR = true;
        }
        else if(strCreditResId != null){
            selectedResult =  new Map<Id, Credit_Result__c>(listCreditResult).get(strCreditResId);
            system.debug('#####accRec.Loans__r : '+accRec.Loans__r);
            listRelatedLoan = new List<LLC_BI__Loan__c>();
            
            listRelatedLoan = [SELECT Id, Name, LLC_BI__Stage__c
                               FROM LLC_BI__Loan__c
                               WHERE LLC_BI__Account__c = :strAccId];
            if(!listRelatedLoan.isEmpty()){
                listLoanWrap = new List<LoanWrapper>();
                for(LLC_BI__Loan__c loanRec : listRelatedLoan){
                    listLoanWrap.add(new LoanWrapper(false, loanRec));
                }
            system.debug('#####listLoanWrap : '+listLoanWrap);
            }
            
            boolStep1 = false;
            boolStep2 = true;
            boolStep3 = false;
        }
    }
    
    public void linkResultToLoan(){
        
        List<LLC_BI__Loan__c> listLoanToLink = new List<LLC_BI__Loan__c>();
        
        //Logic to be added to link Result with Loan.
        for(LoanWrapper wrap : listLoanWrap){
            if(wrap.isSelected){
                listLoanToLink.add(wrap.loanRec);
            }
        }
        
        if(listLoanToLink != null && !listLoanToLink.isEmpty()){
            //update loan.
            boolLoanLinkMsg = true;
            boolSelectLoan = false;
        }
        else{
            boolSelectLoan = true;
            boolLoanLinkMsg = false;
        }
    }
    
    public void retrieveNew(){
        
        if(accRec != null){
            if(!accRec.Prescreening_Results__r.isEmpty()){
                listOpenRFI = new List<Regulatory_and_Fraud_Indicators__c>();
                listOpenRFI.addAll(accRec.Prescreening_Results__r);
            }
            
            if(listOpenRFI != null && !listOpenRFI.isEmpty()){
                boolOpenRFI = true;
                //boolNoOpenRFI = false;
            }
            /**else{
                boolOpenRFI = false;
                boolNoOpenRFI = true;
            }**/
        }
        
        boolStep1 = false;
        boolStep2 = false;
        boolStep3 = true;
    }
    
    public void refreshRFI(){
        
        listOpenRFI = new List<Regulatory_and_Fraud_Indicators__c>();
        listOpenRFI = [SELECT Id, Entity__r.Name, Flag_Type__c, Name, Result__c, Loan__r.Name
                       FROM Regulatory_and_Fraud_Indicators__c
                       WHERE Customer__c = :strAccId
                         AND Result__c NOT IN :listResult];
    
        if(listOpenRFI != null && !listOpenRFI.isEmpty()){
            boolOpenRFI = true;
        }
        else{
            boolOpenRFI = false;
        }
    }
    
    public object creditResultCallout(){
        
        Continuation con = new Continuation(10);
        con.continuationMethod='processResponse';
        
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint('http://www.mocky.io/v2/570c9f9f1100007727d17ee5');
        system.debug('##### req :'+req);
        returnedContinuationId = con.addHttpRequest(req);
        
        return con;
    }
    
    public object processResponse(){

        HttpResponse httpRes = Continuation.getResponse(returnedContinuationId);
        response = httpRes.getBody();
        system.debug('#####response : '+response);
        return null;
    }
    
    public Class LoanWrapper{
        
        public Boolean isSelected{get;set;}
        public LLC_BI__Loan__c loanRec{get;set;}
        
        public LoanWrapper(Boolean isSelected, LLC_BI__Loan__c loanRec){
            this.isSelected = isSelected;
            this.loanRec = loanRec;
        }
    }
}