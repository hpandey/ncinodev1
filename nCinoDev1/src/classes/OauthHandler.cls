/**********************************************************************************************
 * Name         : OauthHandler
 * Author       : Deloitte Consulting
 * Description  : This class implements the logic for fetching access token from dev org. 
 *********************************************************************************************/
global class OauthHandler {
    
    /***
    * Method name    : getAccessToken
    * Description    : method used for getting access token.
    * Return Type    : String
    * Parameter      : None 
    */
    public String getAccessToken() {
        String strClientId = '3MVG9ZL0ppGP5UrBamT9HyYBtRY1A3Py2EZZ8wassqV_1w.1K0hJ1b7tPhBU5l8IsCRZMmvMeYQMTwEQby95f';
        String strClientSecret = '4968351306906540745';
        String strUsername= 'sumsagar@deloitte.com';
        String strPassword = '4rfvVFR$'+'3a5eaVzdGBmDxEVBRO1axzs7';
        
        String reqbody = 'grant_type=password&client_id='+strClientId+'&client_secret='+strClientSecret+'&username='+strUsername+'&password='+strPassword;
    
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setBody(reqbody);
        req.setMethod('POST');
        req.setEndpoint('https://login.salesforce.com/services/oauth2/token');
        
        HttpResponse res = h.send(req);

        OAuth2 objAuthenticationInfo = (OAuth2)JSON.deserialize(res.getbody(), OAuth2.class);
        
        system.debug('objAuthenticationInfo@@'+objAuthenticationInfo);
        return objAuthenticationInfo.Access_token;
    }
    
    /**********************************************************************************************
     * Name         : OAuth2
     * Author       : Deloitte Consulting
     * Description  : This class used as a wrapper for storing OAuth details.
     *********************************************************************************************/
    public class OAuth2{
        public String Id{get;set;}
        public String Issued_at{get;set;}
        public String Instance_url{get;set;}
        public String Signature{get;set;}
        public String Access_token{get;set;}    
    }
}