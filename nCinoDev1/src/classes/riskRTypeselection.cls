/*
Author - Ssandeep
Descrption - Record Type Redirect
*/public class riskRTypeselection 
{

     private ApexPages.StandardController controller;
     public String retURL {get; set;}
     public String saveNewURL {get; set;}
     public String rType {get; set;}
     public String cancelURL {get; set;}
     public String ent {get; set;}
     public String confirmationToken {get; set;}
     public String riskId;
     public Risk_Assessment_Review__c risk;
     public string riskType;
    public riskRTypeselection(ApexPages.StandardController controller) 
    {
    	system.debug('in edit risk page'+ApexPages.currentPage().getParameters().get('RecordType'));
          this.controller = controller;
          riskId = controller.getId();
          risk = (Risk_Assessment_Review__c)controller.getRecord();
          riskType = risk.RecordTypeId;
          retURL = ApexPages.currentPage().getParameters().get('retURL');
          rType = ApexPages.currentPage().getParameters().get('RecordType');
          cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
          ent = ApexPages.currentPage().getParameters().get('ent');
          confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
          saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
    }
    
    public PageReference ChagneURL()
    {
PageReference returnURL;
        // Redirect if Record Type corresponds to custom VisualForce page
        Id riskAssementRecordTypeId = Schema.SObjectType.Risk_Assessment_Review__c.getRecordTypeInfosByName().get('Risk Assessment').getRecordTypeId();
        Id riskReviewRecordTypeId = Schema.SObjectType.Risk_Assessment_Review__c.getRecordTypeInfosByName().get('Conduct Review').getRecordTypeId();
                
        
        
        if(rType == riskAssementRecordTypeId ) 
        {
            returnURL = new PageReference('/apex/RiskAssessment?RecordType='+riskAssementRecordTypeId );
        }
        else if(rType == riskReviewRecordTypeId  )
        {
            returnURL = new PageReference('/apex/RiskReview?RecordType='+riskAssementRecordTypeId );
        }
        else 
        {
            returnURL = new PageReference('/001/e');
        }
        returnURL.getParameters().put('retURL', retURL);
        returnURL.getParameters().put('RecordType', rType);
        returnURL.getParameters().put('cancelURL', cancelURL);
        returnURL.getParameters().put('ent', ent);
        returnURL.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
        returnURL.getParameters().put('save_new_url', saveNewURL);
        returnURL.getParameters().put('nooverride', '1');
        returnURL.setRedirect(true);
        return returnURL; 
    }
    
    public PageReference gotoEdit()
    {
    PageReference returnURL;
        // Redirect if Record Type corresponds to custom VisualForce page
        String riskAssementRecordTypeId = Schema.SObjectType.Risk_Assessment_Review__c.getRecordTypeInfosByName().get('Risk Assessment').getRecordTypeId();
        String riskReviewRecordTypeId = Schema.SObjectType.Risk_Assessment_Review__c.getRecordTypeInfosByName().get('Conduct Review').getRecordTypeId();
                
        system.debug('RiskAssesmentRecord'+riskAssementRecordTypeId );
        system.debug('riskReviewRecordTypeId '+riskReviewRecordTypeId );
        system.debug('riskRecordTypeId '+riskType );
        system.debug('rType '+rType );
        if(rType != null) // on record type chagne from view page
        {
        		if(riskAssementRecordTypeId.contains(rType))
       	 			returnURL = new PageReference('/apex/RiskAssessment?id='+riskId+'&RecordType='+rType);
       	        if(riskReviewRecordTypeId.contains(rType) )
       	 		    returnURL = new PageReference('/apex/RiskReview?id='+riskId+'&RecordType='+rType);       	 				
        }
        else if(risk.recordTypeID != null && riskAssementRecordTypeId.contains(risk.recordTypeID)){
            returnURL = new PageReference('/apex/RiskAssessment?id='+riskId+'&RecordType='+riskAssementRecordTypeId);
        }
        else if(risk.recordTypeID != null && riskReviewRecordTypeId.contains(risk.recordTypeID) )
        {
            returnURL = new PageReference('/apex/RiskReview?id='+riskId+'&RecordType='+riskReviewRecordTypeId);
        }
        else if(riskType == riskAssementRecordTypeId ) 
        {
            returnURL = new PageReference('/apex/RiskAssessment?id='+riskId  );
        }
        else if(riskType == riskReviewRecordTypeId )
        {
            returnURL = new PageReference('/apex/RiskReview?id='+riskId  );
        }
        return returnURL ;
    }

}