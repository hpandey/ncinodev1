public with sharing class CoachingAreaListExtension {

    public List<Coaching_Area__c> areas { get; set; }
    public String searchString { get; set; }
    private String reviewId;

    public CoachingAreaListExtension(ApexPages.StandardController controller){
        searchString = '';
        reviewId = controller.getId();
        doQuery();
    }

    public void save() {
        Update areas;
    }

    public void doQuery() {
        String queryString = '';
        if (searchString != null) {
            queryString = '%' + searchString + '%';
        }
        areas = [SELECT id, Name, Area_of_Development__c, Focus_Area__c, Comments__c, Next_Steps__c, Results__c
                     FROM coaching_area__c
                    WHERE Coaching_Review__c = :reviewId];        
    }

}