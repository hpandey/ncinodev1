global class NightlyBatchScheduler implements Schedulable{
   global void execute(SchedulableContext sc) {
      NightlyShareBatch b = new NightlyShareBatch(); 
      database.executebatch(b,5);
   }
}