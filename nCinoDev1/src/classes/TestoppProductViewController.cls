@isTest
private class TestoppProductViewController {
	
	@isTest public static void testGetInvolvedLoans() {
        Account testAccount = createAccount();
        Opportunity testOpportunity = createOpportunity(testAccount.Id);
        LLC_BI__Loan__c testLoan = createLoan();

        ApexPages.StandardController stdController = new ApexPages.StandardController(testOpportunity);
        OppProductViewController pv = new OppProductViewController(stdController);

        Test.startTest();

        LLC_BI__Legal_Entities__c testEntityNoLoan = new LLC_BI__Legal_Entities__c(
            Name = 'EntityWithLoan',
            LLC_BI__Account__c = testAccount.Id,
            LLC_BI__Loan__C = testLoan.Id);
        insert testEntityNoLoan;

        Test.stopTest();
    }
    
    @isTest
    public static void testGetInvolvedDeposits() {
        Account testAccount = createAccount();
        Opportunity testOpportunity = createOpportunity(testAccount.Id);
        LLC_BI__Deposit__c testDeposit = createDeposit();

        ApexPages.StandardController stdController = new ApexPages.StandardController(testOpportunity);
        OppProductViewController pv = new OppProductViewController(stdController);

        Test.startTest();

        LLC_BI__Legal_Entities__c testEntityNoDeposit = new LLC_BI__Legal_Entities__c(
            Name = 'EntityNoDeposit',
            LLC_BI__Account__c = testAccount.Id,
            LLC_BI__Deposit__c = testDeposit.Id);
        insert testEntityNoDeposit;

        Test.stopTest();
    }

    private static Account createAccount(){
        Account testAccount = new Account(
            Name = 'TestAccount');
        insert testAccount;
        return testAccount;
    }

    private static Opportunity createOpportunity(Id theaccountId) {
    	Opportunity testOpportunity = new Opportunity(
    		Name = 'TestOpportunity',
    		AccountId = theaccountId,
    		CloseDate = system.today(),
    		StageName = 'Qualification');
    	insert testOpportunity;
    	return testOpportunity;
    }

    private static LLC_BI__Loan__c createLoan(){
        LLC_BI__Loan__c testLoan = new LLC_BI__Loan__c(
            Name = 'TestLoan');
        insert testLoan;
        return testLoan;
    }
    private static LLC_BI__Deposit__c createDeposit(){
        LLC_BI__Deposit__c testDeposit = new LLC_BI__Deposit__c(
            Name = 'TestDeposit');
        insert testDeposit;
        return testDeposit;
    }
	
}