/******************************************************************************************************************
* Name            :             CreateRuleActions_Controller
* Author          :             Deloitte Consulting
* Description     :             This is a controller class which will include all the logic 
                                create Rule Action & Record Data
*******************************************************************************************************************/
public with sharing class CreateRuleActions_Controller {

    private String strParentObjectName;
    private Id ruleId;
    private Id recordId;
    public Rule_Action__c objRuleAction {get;set;}
   
    public Integer rowToRemove {get;set;}
    
    public List<RecordDataWrapper> lstRecordData {get;set;}
    public List<SelectOption> object_Options {get;set;} // select options having object names
    public List<SelectOption> field_Options {get;set;} // select options having field names
    private List<Record_data__c> lstRecordDataToDelete = new List<Record_data__c>();
    
    private map<String,List<SelectOption>> mapPickListValues; // stores the picklist values if the field type is picklist
    private map<String,String> mapFieldName_FieldType ; // stores the API Name of the field and the field to be displayed with it. E.g Address => Text_Value__c
    public map<String,String> mapFieldName_Type{get;set;} // map having the field name and its data type
    private map<String,String> mapDataType_RCFieldName = new map<String,String>{
                                                                'DATE' => 'Date_Value__c',
                                                                'INTEGER' => 'Number_Value__c',
                                                                'STRING' => 'Text_Value__c',
                                                                'DOUBLE' => 'Currency_Value__c',
                                                                'BOOLEAN' => 'Boolean_Value__c',
                                                                'PICKLIST' =>'PICKLIST',
                                                                'DATETIME' => 'Date_Value__c',
                                                                'TEXTAREA' => 'Text_Value__c',
                                                                'MULTIPICKLIST' => 'MULTIPICKLIST',
                                                                'CURRENCY' => 'Number_Value__c',
                                                                'PERCENT' => 'Percent_Value__c',
                                                                'EMAIL' =>  'Email_Value__c',
                                                                'PHONE' => 'Phone_Value__c',
                                                                'URL' => 'URL_Value__c',
                                                                'REFERENCE' => 'Text_Value__c'
                                                                };
    
    /*Method Name    :        CreateRuleActions_Controller
     *Author         :        Deloitte Consulting
     *Description    :        This is a constructor
    */
    public CreateRuleActions_Controller(ApexPages.StandardController controller) {
         strParentObjectName = ApexPages.currentPage().getParameters().get('objectName'); // get object name from the URL
         ruleId = ApexPages.currentPage().getParameters().get('ruleId'); // get rule Id from the URL
         recordId = ApexPages.currentPage().getParameters().get('id');
         System.debug('@@@@@@@ '+recordId);
         init(); // initialize collections
         if(recordId!=null) // checking if the edit button is clicked
            queryExistingData(); // load the existing data
         
    }//end constructor
    
    /*Method Name    :        init
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to instatiate the collections 
     *Parameters     :        None
     *Returns        :        Void
    */
    private void init(){
        objRuleAction = new Rule_Action__c();
        lstRecordData = new List<RecordDataWrapper>();
        mapFieldName_FieldType = new map<String,String>();
//Code review comment - As I understand, recordId will be null for new pages. But why do we need to call the addNewRow() method         
        if(recordId==null)
            addNewRow(); // add new row in the table only in case of new records
    }//end method
    
    /*Method Name    :        queryExistingData
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to load all the existing records in case edit button is clicked 
     *Parameters     :        None
     *Returns        :        Void
    */
    private void queryExistingData(){
        List<Record_data__c> lstExistingRecordData = [Select Id,Boolean_Value__c,Currency_Value__c,Date_Value__c,Email_Value__c,
                                                     Field_Name__c,Number_Value__c,Percent_Value__c,Phone_Value__c,Rule_Action__c,
                                                     Text_Value__c,URL_Value__c,Rule_Action__r.Action_Type__c,Rule_Action__r.Object_Name__c,Rule_Action__r.Rule__c,
                                                     Rule_Action__r.Rule__r.Object_Name__c,Rule_Action__r.Rule__r.Id
                                                     from Record_data__c
                                                     where Rule_Action__r.Id=:recordId];        
        assignValues(lstExistingRecordData);
    }//end method
    
    /*Method Name    :        assignValues
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to assign values to the wrapper and rule action, so that the existing records display on the page
     *Parameters     :        None
     *Returns        :        Void
    */
    private void assignValues(List<Record_data__c> lstRecordData){
        if(lstRecordData!=null){
            //assign values to the record data object
            for(Record_data__c recordData_iterator : lstRecordData){
                objRuleAction.Action_Type__c = recordData_iterator.Rule_Action__r.Action_Type__c;
                objRuleAction.Object_Name__c = recordData_iterator.Rule_Action__r.Object_Name__c;// get the label name of the object
//Code Review Comment - do we need the following 2 lines here? they will be overridden in every iteration of the loop
                strParentObjectName = recordData_iterator.Rule_Action__r.Rule__r.Object_Name__c;
                ruleId = recordData_iterator.Rule_Action__r.Rule__r.Id;
            }//end for
            displayRelatedObject(); // pre load the related objects
            getRelatedFields(); // pre load the related fields
            generateWrapperData(lstRecordData); // pre load the table on the page
            getFieldType(); // get the field type to display the values on the page
        }//end if
        
    }//end method
    
    /*Method Name    :        displayRelatedObject
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to get the related objects based on the URL parameter and on the Action type
                              selected by the user on the page.
     *Parameters     :        None
     *Returns        :        Void
    */
    public void displayRelatedObject(){
        object_Options = new List<SelectOption>();
//Code Review Comment: Please add null checker for System.Label.UpdateRecord if the value is null or blank, we need to add Apex Page message                
        if(System.label.UpdateRecord!=null && !String.isBlank(System.label.UpdateRecord) && System.label.CreateChildRecord!=null && !String.isBlank(System.label.CreateChildRecord)){//check if the labels are blank or null
            if(objRuleAction.Action_Type__c==System.label.UpdateRecord){ // display only one option
                object_Options.add(new SelectOption(strParentObjectName,UtilityClass.getObjectLabelNames(strParentObjectName)));
                objRuleAction.Object_Name__c =  strParentObjectName;
                getRelatedFields(); // call method immediately to get the fields
            }else if(objRuleAction.Action_Type__c==System.label.CreateChildRecord){ //check if create child record is selected
                object_Options.add(new SelectOption('','--None--'));
                getChildRelationships(); // get the child objects
            }//end if else
        }else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'An unexpected error has occured. Please contact your System Administrator'));      
        }//end if else
    }//end method
    
    /*Method Name    :        getChildRelationships
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to get the child relationships based on the object name
     *Parameters     :        String
     *Returns        :        Void
    */
    private void getChildRelationships (){
//Code Review Comment - Please correct the spelling of mpExcludedObjects
        map<String, Exclude_Objects__c> mapExcludedObjects = Exclude_Objects__c.getAll(); // get the list of excluded objects from the custom setting
        List<Schema.ChildRelationship> lstChildRelationships = Schema.getGlobalDescribe().get(strParentObjectName).getDescribe().getChildRelationships();  // schema methods to get relationships
        if(lstChildRelationships!=null && !lstChildRelationships.isEmpty()){ // null check
            for(Schema.ChildRelationship childRelationship_iterator: lstChildRelationships){ // get all the child objects based on parent objects name
               String strChild = String.valueof(childRelationship_iterator.getChildSObject());
               System.debug('values are :  '+strChild);        
               if(mapExcludedObjects!=null && !mapExcludedObjects.containskey(strChild)) // exclude unnecessary object names
                   object_Options.add(new SelectOption(strChild,UtilityClass.getObjectLabelNames(strChild)+'--'+childRelationship_iterator.getField()));   // get label names  
            }//end for
        }//end if
        if(object_Options!=null && !object_Options.isEmpty())
            SelectOptionSorter.doSort(object_Options , SelectOptionSorter.FieldToSort.Label); // sorting options
    }//end method
    
    /*Method Name    :        getRelatedFields
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to get the Field Names based on the object name
     *Parameters     :        None
     *Returns        :        Void
    */
    public void getRelatedFields(){
        field_Options = new List<SelectOption>();
        field_Options.add(new SelectOption('','--None--'));
        if(objRuleAction.Object_Name__c!=null){
            if(lstRecordData.size()>0){ // wipe out the table if a different object is selected
                checkRecordsToDelete();// delete the records before clearing the records from the table
                lstRecordData = new List<RecordDataWrapper>(); // clear the list
                addNewRow(); // add a new row to the list
            }//end if
            List<Schema.SObjectField> lstFieldNames = UtilityClass.getFieldNames(objRuleAction.Object_Name__c); // utility method to get the field names
            if(lstFieldNames!=null && !lstFieldNames.isEmpty()){ 
                getRelevantFields(lstFieldNames); // method to get the field names by excluding unwanted fields    
            }//end if
        }//end if
    }//end method
    
    /*Method Name    :        checkRecordsToDelete
     *Author         :        Deloitte Consulting
     *Description    :        This method is used check if the records are already inserted, if yes then they will be added to the list of records to be deleted
     *Parameters     :        None
     *Returns        :        Void
    */
//Code review comment - Didn't quite understand the purpose of this method.
    public void checkRecordsToDelete(){
        for(RecordDataWrapper wrapper : lstRecordData){
            if(wrapper.recordData.Id!=null)// check if the record exists
                lstRecordDataToDelete.add(wrapper.recordData); // add to the list of deleted records   
        }//end for
    }//end method
    
    /*Method Name    :        getRelevantFields
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to get the relevant Fields
     *Parameters     :        List<Schema.SObjectField>
     *Returns        :        Void
    */
    public void getRelevantFields(List<Schema.SObjectField>lstFieldNames){
        map<String,List<Schema.PicklistEntry>> mapField_Picklist = new map<String,List<Schema.PicklistEntry>>();// map for picklist fields
        for(Schema.SObjectField iterator_Field : lstFieldNames){
            Schema.DescribeFieldResult fieldDescribe = iterator_Field.getDescribe();
            //check to exclude system, formula, read-only & relationship fields
            if(fieldDescribe.isUpdateable() && !fieldDescribe.isAutoNumber() && !fieldDescribe.isCalculated() && fieldDescribe.getRelationshipName()==null){ 
                String strFieldType = String.valueof(fieldDescribe.getType());
                String strFieldValue = String.valueOf(iterator_Field);
                String strFieldLabel = UtilityClass.getFieldLabelNames(objRuleAction.Object_Name__c,strFieldValue); // utility method to get fields API names
                System.debug('Type is ' + strFieldType +' Name is '+strFieldLabel );
//Code Review comment - The name of the map mapFieldName_FieldResult is confusing in the current context                
                mapFieldName_FieldType.put(strFieldValue,strFieldType); // create a map of Field API Name and its data type
                field_Options.add(new SelectOption(strFieldValue,strFieldLabel));
                if(strFieldType=='PICKLIST' || strFieldType=='MULTIPICKLIST'){ // add into map if field type is either picklist or multiselect picklist
                   mapField_Picklist.put(strFieldValue,fieldDescribe.getPicklistValues());    
                } 
            }  
        }//end for 
        if(mapField_Picklist.size()>0){
            mapPickListValues = new map<String,List<SelectOption>>();
//Code Review comment -  mapField_Picklist key is a Field as per line 210. Why is the variable name strObjectName in the line below.          
            for(String strFieldName: mapField_Picklist.keyset()){ // get the picklist values for each picklist field and add it in a map
                List<SelectOption> lstOptions = new List<SelectOption>();
                for(Schema.PicklistEntry picklistvalues: mapField_Picklist.get(strFieldName)){    
                        lstOptions.add(new SelectOption(picklistvalues.getLabel(),picklistvalues.getValue()));
                        mapPickListValues.put(strFieldName,lstOptions);   
                }//end for 
            }//end for  
        }//end if
        if(field_Options!=null && !field_Options.isEmpty())
            SelectOptionSorter.doSort(field_Options , SelectOptionSorter.FieldToSort.Label); // sorting fields
     }//end method
     
    /*Method Name    :        getFieldType
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to get the Field Types based on users selection
     *Parameters     :        None
     *Returns        :        Void
    */
    public void getFieldType(){
        mapFieldName_Type = new map<String,String>();
        for(RecordDataWrapper iterator_Record:lstRecordData){ // iterate over the table
            String strDataType;
            if(iterator_Record.recordData.Field_Name__c!=null){
                strDataType = mapDataType_RCFieldName.get(mapFieldName_FieldType.get(iterator_Record.recordData.Field_Name__c));
                mapFieldName_Type.put(iterator_Record.recordData.Field_Name__c,strDataType); // get the name of the field to display
            }//end if 
            if(mapPickListValues.containskey(iterator_Record.recordData.Field_Name__c)){ // if map contains a picklist field then get the picklist values
               iterator_Record.field_PickListOptions =  mapPickListValues.get(iterator_Record.recordData.Field_Name__c);  
            }//end if
            System.debug('strDataType is  '+strDataType);
            if(strDataType=='Boolean_Value__c'){ // to indentiy boolean fields
                iterator_Record.isBooleanField = true;
            }//end if
            if(strDataType =='MULTIPICKLIST' && recordId!=null){// assing multiselect values in case of edit
                iterator_Record.lstMultiselectValues = UtilityClass.convertStringtoArray(iterator_Record.recordData.Text_Value__c); 
            }//end if
        }//end for
    }//end method
    
    /*Method Name    :        addNewRow
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to add a new row to the Record data table on the page
     *Parameters     :        None
     *Returns        :        Void
    */
    public void addNewRow(){
        RecordDataWrapper row_RecordData=new RecordDataWrapper();
        Record_data__c newRec = new Record_data__c();
        row_RecordData.recordData = newRec; // assign record data to the wrapper
        lstRecordData.add(row_RecordData);    
    }//end method
    
    /*Method Name    :        removeRow
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to remove row from the Record data table on the page
     *Parameters     :        None
     *Returns        :        Void
    */
    public void removeRow(){
        RecordDataWrapper deldata = lstRecordData.get(rowToRemove-1);
        if(deldata.recordData.Id !=null){ // create a list of records to ve deleted
            lstRecordDataToDelete.add(deldata.recordData);        
        }//end if 
        lstRecordData.remove(rowToRemove-1); // based on the index remove the row
    }//end method
    
    /*Method Name    :        saveAll
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to save/update/delete Rule Action and Record Data records
     *Parameters     :        None
     *Returns        :        PageReference
    */
    public pageReference saveAll(){
        pageReference pg;
        boolean validationResult = validateData(); // check for validations
        Savepoint sp;
        if(validationResult==false){
            try{
                List<Record_data__c> lstRecordData_ToInsert_Update = new List<Record_data__c>(); // create list to insert
                sp = Database.setSavepoint(); // set save point
                if(recordId==null){// check for new records
                    objRuleAction.Rule__c = ruleId; // assignment of rule id is needed only in case of a new record
                }else{ // check for existing records
                    objRuleAction.id = recordId;  
                }//end if else
                if(objRuleAction!=null)
                    upsert objRuleAction; // upsert rule Action
                if(objRuleAction.id !=null){ // insert/update record data only when the rule action record is saved successfully.
                    lstRecordData_ToInsert_Update= getRecordDataList(lstRecordData); // call method to get the list of record data to insert
                    if(lstRecordData_ToInsert_Update.size()>0) // insert/update record data 
                        upsert lstRecordData_ToInsert_Update;
                }else{ // if insert/update fails
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'An unexpected error has occured. Please contact your System Administrator'));  
                    return null;
                }//end if else
                if(lstRecordDataToDelete!=null && lstRecordDataToDelete.size()>0){ // check for deleting records
                    delete lstRecordDataToDelete;
                }//end if
                pg = new pageReference('/'+ruleId);  
                return pg; 
            }catch (DMLException DML){
                Database.rollback(sp);// rollback in case of any errors
                utilityClass.throwExceptions(Dml);
                displayErrorMessage(DML);
                return null;
            }//end try catch 
        }else{
            return null;
        }// end if else
    }//end method
    
    /*Method Name    :        cancel
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to redirect the users to the rule page when cancel button is clicked
     *Parameters     :        None
     *Returns        :        PageReference
    */
    public pageReference cancel(){
        return new pageReference('/'+ruleId);  
    }//end method
    
    /*Method Name    :        getRecordDataList
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to convert the wrapper data to list of Record data 
     *Parameters     :        List<RecordDataWrapper>
     *Returns        :        List<Record_data__c>
    */
    public List<Record_data__c> getRecordDataList(List<RecordDataWrapper> lstWrapper){
        List<Record_data__c> lstRecord = new List<Record_data__c>();
        for(RecordDataWrapper iteratorRecordData : lstWrapper){
            Record_data__c rec = new Record_data__c();
            rec = iteratorRecordData.recordData;
            if(rec.Rule_Action__c==null)// do not assign rule action value in case of update
                rec.Rule_Action__c =  objRuleAction.id; // Assign the rule action Id to every record data.    
            lstRecord.add(rec);
        }//end for 
        return lstRecord;
    }//end method
    
    /*Method Name    :        generateWrapperData
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to convert the  list of Record data to list of wrapper data  
     *Parameters     :        List<RecordDataWrapper>
     *Returns        :        List<Record_data__c>
    */
    public void generateWrapperData(List<Record_data__c> lstRecords){
        for(Record_data__c iteratorRecordData : lstRecords){
            RecordDataWrapper recWrapper = new RecordDataWrapper();
            recWrapper.recordData = iteratorRecordData;
            //recWrapper.field_PickListOptions = 
            lstRecordData.add(recWrapper);
        }//end for 
    }//end method
    
    /*Method Name    :        validateData
     *Author         :        Deloitte Consulting
     *Description    :        This method is used to Validate data before insterting
     *Parameters     :        None
     *Returns        :        Boolean
    */
    public boolean validateData(){
        boolean isError=false;
        Set<String> setFieldNames = new Set <String>(); // This set is used to store the unique field names
        if(objRuleAction.Object_Name__c==null || objRuleAction.Object_Name__c==''){ // Check if the Related object is entered
            isError = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a Related Object'));
        }//end if
        if(lstRecordData.size()==0){
            isError = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please add atleast one record data'));    
        }
        for(RecordDataWrapper iteratorRecordData : lstRecordData){
            if(!iteratorRecordData.lstMultiselectValues.isEmpty()) //assign value to the multi select field
                 iteratorRecordData.recordData.Text_Value__c = utilityClass.convertArraytoString(iteratorRecordData.lstMultiselectValues); 
           if(iteratorRecordData.recordData.Field_Name__c==null){ // check to see if any field name is not entered
                isError = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Field Name cannot be blank. Please select a value'));
            }//end if
            if(iteratorRecordData.recordData.Currency_Value__c==null && iteratorRecordData.recordData.Date_Value__c==null &&
            iteratorRecordData.recordData.Number_Value__c==null && iteratorRecordData.recordData.Text_Value__c==null && iteratorRecordData.isBooleanField == null){ // check to see if the value is entered
                isError = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Value cannot be blank. Please select a value'));    
            }
            if(setFieldNames.contains(iteratorRecordData.recordData.Field_Name__c)){ // check to see if one field name is selected more than once
                isError = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,+iteratorRecordData.recordData.Field_Name__c+' is selected more than once.'));     
            }else{
                setFieldNames.add(iteratorRecordData.recordData.Field_Name__c);
            }
        }//end for
        return isError;
    }//end method
     
     /*Method Name    :       displayErrorMessage
     *Author         :        Deloitte Consulting
     *Description    :        Method to display error messages on the page.
     *Parameters     :        Exception 
     *Returns        :        Void
    */
    public void displayErrorMessage(Exception e){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
        ApexPages.addMessage(myMsg);    
    }//end method
    
    /*Method Name    :        displayErrorMessage
     *Author         :        Deloitte Consulting
     *Description    :        Method to display error messages on the page.
     *Parameters     :        List<Database.Error> 
     *Returns        :        Void
    */
    public void displayErrorMessage(List<Database.Error> lstErrors){
        for(Database.Error err : lstErrors) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,err.getMessage());
            ApexPages.addMessage(myMsg);                    
        }//end for
    }//end method
    
     // wrapper class defination
     public class RecordDataWrapper{
         public Record_data__c recordData {get;set;}
         public List<SelectOption> field_PickListOptions {get;set;}
         public boolean isBooleanField {get;set;}
         public List<String> lstMultiselectValues{set;get;}{lstMultiselectValues = new List<String>();}
     }//end wrapper class

}//end class