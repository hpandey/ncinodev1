global class CoreRevenueSegmentBatch implements Database.batchable<sObject>{

	global final String Query;
	global boolean GIB;
    global boolean CMM;
    global boolean RBC;
    global boolean SpecInd; 
    global boolean updtCore; 
	global string BGType;
    global string BGLob;
	
	
	global CoreRevenueSegmentBatch() {
	   	
      //Query = 'select id, Core__c, Core_2__c, Core_2_Segment__c, Core_Segment__c, Specialized__c, Specialized_Segment_Logic__c from opportunity where (RecordType.Name = \'TM Open Pipeline\' or RecordType.Name = \'TM Closed Pipeline\' or RecordType.Name = \'Open Pipeline\' or RecordType.Name = \'Closed Pipeline\' or RecordType.Name = \'Forecast Pipeline\' or RecordType.Name = \'REB Closed Pipeline\' or RecordType.Name = \'REB Open Pipeline\' ) and (CloseDate = this_year or CloseDate = Last_year))';	 	
      Query = 'select id, Core__c, Core_2__c, Core_2_Segment__c, Core_Segment__c, Specialized__c, Specialized_Segment_Logic__c from opportunity';
	} 	
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
	}
	 
	global void execute(Database.BatchableContext BC, List<Opportunity> OppList){
		
		for (Opportunity opp : OppList){
		    GIB = false;
		    CMM = false;
		    RBC = false;
		    SpecInd = false;
			updtCore = false;	
			BGType = '';
			BGLob = '';	
			set<string> CIType = new set<string>();
			set<string> LOB = new set<string>();
			
			for(OpportunityTeamMember otms : [SELECT id, User.C_I_Type__c, User.LOB__c, User.Specialized_Industries__c From OpportunityTeamMember WHERE  OpportunityId = :opp.Id]  ){
				if (otms.User.LOB__c == 'Commercial and Industrial (C&I)' || otms.User.LOB__c == 'Energy & Natural Resources' || otms.User.LOB__c == 'Healthcare' || otms.User.LOB__c == 'Real Estate Banking' || otms.User.LOB__c == 'Regions Business Capital' || otms.User.LOB__c == 'Restaurant' || otms.User.LOB__c == 'Technology & Defense' || otms.User.LOB__c == 'Transportation' || otms.User.LOB__c == 'Financial Services Group (FSG)'){
					system.debug(otms);
					CIType.add(otms.User.C_I_Type__c);
					LOB.add(otms.User.LOB__c);
					BGType = BGType + ' ' + otms.User.C_I_Type__c;
					BGLob = BGLob  + ' ' + otms.User.LOB__c;
					
			              if (otms.User.C_I_Type__c == 'GIB') {
			                     GIB = true;
			              }
			              if (otms.User.C_I_Type__c == 'CMM') {
			                     CMM = true;
			              }
			              
			              if (otms.User.LOB__c == 'Regions Business Capital') {
			                     RBC = true;
			              }
			              if (otms.User.Specialized_Industries__c == true) {
			                     SpecInd =  true;
			              }
				}
			}
			// ** Core & Core Segment Start **
			if (CIType.contains('CMM') == true || CIType.contains('GIB') == true){
				opp.core__c = 'Commercial';
				updtCore = true;
				if ((CIType.size() == 1) && (CIType.contains('CMM') == true)){  
					opp.core_segment__c = 'Commercial Middle Market - Core';
				}
				if (CIType.contains('CMM') == true  && SpecInd == true){
					opp.core_segment__c = 'Commercial Middle Market Shared - Specialized Industries';
				}				
				if (CIType.contains('CMM') == true && RBC == true){  
					opp.core_segment__c = 'Commercial Middle Market Shared - RBC';
				}
				if (CIType.size() > 1 && CMM == true && GIB == true){
					opp.core_segment__c = 'Commercial Middle Market Shared - GIB';
				}
				if (CIType.size() == 3 && CMM == true && GIB == true && SpecInd == true){
					opp.core_segment__c = 'Commercial Middle Market Shared - Specialized Industries';
				}	
				if ((CIType.size() == 1) && (CIType.contains('GIB') == true)){  
					opp.core_segment__c = 'GIB - Core';
				}
				if (CIType.contains('GIB') == true && SpecInd == true){
					opp.core_segment__c = 'GIB Shared - Specialized Industries';
				}
				if (CIType.contains('GIB') == true && RBC == true){  
					opp.core_segment__c = 'GIB Shared - RBC';
				}				
				
			}
			if (CIType.contains('Corporate') == true){
				opp.core__c = 'Corporate';
				updtCore = true;
				if (CIType.size() == 1 && CIType.contains('Corporate') == true){
					opp.core_segment__c = 'Corporate - Core';
				}
				//if (BGLob.countMatches('Regions Business Capital') > 1 && BGType.contains('Corporate') == true){  
				if (BGLob.contains('Regions Business Capital') == true && BGType.contains('Corporate') == true){
					opp.core_segment__c = 'Corporate Shared - RBC';
				}
				//if (BGType.countMatches('Corporate') > 1 && SpecInd == true){  
				if (BGType.contains('Corporate') == true && SpecInd == true){
					opp.core_segment__c = 'Corporate Shared - Specialized Industries';
				}
			}
			if (LOB.contains('Real Estate Banking') == true){
				opp.core__c = 'Real Estate Banking';
				updtCore = true;	
				if (BGType.contains('Affordable Housing') == true){  
					opp.core_segment__c = 'Affordable Housing';
				}
				if (BGType.contains('Home Builder Finance') == true){  
					opp.core_segment__c = 'Home Builder Finance';
				}
				if (BGType.contains('Income Property Finance') == true){  
					opp.core_segment__c = 'Income Property Finance';
				}
				if (BGType.contains('Local Real Estate') == true){  
					opp.core_segment__c = 'Local Real Estate';
				}
				if (BGType.contains('Real Estate Corporate Banking') == true){  
					opp.core_segment__c = 'Real Estate Corporate Banking';
				}
			}
			// ** Core & Core Segment end **

			// ** Core_2__c & Core_2_Segment__c Start **
			if (CIType.contains('GIB') == true){
				opp.Core_2__c = 'GIB';
				updtCore = true;
				if (CIType.size() > 1 && CMM == true && GIB == true){  
					opp.Core_2_Segment__c = 'GIB - Shared';
				}					
				if (CIType.size() == 1 &&  GIB == true){  
					opp.Core_2_Segment__c = 'GIB - Direct';
				}
			}
			if (RBC == true){
				opp.Core_2__c = 'RBC';
				updtCore = true;
				if (LOB.size() > 1 && BGLob.countMatches('Regions Business Capital') == 1){  
					opp.Core_2_Segment__c = 'RBC - Shared';
				}
				if (LOB.size() == 1){  
					opp.Core_2_Segment__c = 'RBC - Direct';
				}	
			}			
			
			// ** Core_2__c & Core_2_Segment__c END **
			
			// ** Specialized__c & Specialized_Segment_Logic__c Start **
			if (SpecInd == true){
				opp.Specialized__c = 'Specialized';
				updtCore = true;
				if (BGLob.contains('Energy & Natural Resources') == true && BGLob.contains('Commercial and Industrial (C&I)') == true){  
					opp.Specialized_Segment_Logic__c = 'Energy - Shared';
				}
				if (BGLob.contains('Energy & Natural Resources') == true && BGLob.contains('Commercial and Industrial (C&I)') == false){  
					opp.Specialized_Segment_Logic__c = 'Energy - Direct';
				}
				if (BGLob.contains('Healthcare') == true && BGLob.contains('Commercial and Industrial (C&I)') == true){  
					opp.Specialized_Segment_Logic__c = 'Healthcare - Shared';
				}
				if (BGLob.contains('Healthcare') == true && BGLob.contains('Commercial and Industrial (C&I)') == false){  
					opp.Specialized_Segment_Logic__c = 'Healthcare - Direct';
				}
				if (BGLob.contains('Technology & Defense') == true && BGLob.contains('Commercial and Industrial (C&I)') == true){  
					opp.Specialized_Segment_Logic__c = 'Technology & Defense - Shared';
				}
				if (BGLob.contains('Technology & Defense') == true && BGLob.contains('Commercial and Industrial (C&I)') == false){  
					opp.Specialized_Segment_Logic__c = 'Technology & Defense - Direct';
				}									
				if (BGLob.contains('Transportation') == true && BGLob.contains('Commercial and Industrial (C&I)') == true){  
					opp.Specialized_Segment_Logic__c = 'Transporation - Shared';
				}
				if (BGLob.contains('Transportation') == true && BGLob.contains('Commercial and Industrial (C&I)') == false){  
					opp.Specialized_Segment_Logic__c = 'Transporation - Direct';
				}				
				if (BGLob.contains('Restaurant') == true && BGLob.contains('Commercial and Industrial (C&I)') == true){  
					opp.Specialized_Segment_Logic__c = 'Restaurant - Shared';
				}
				if (BGLob.contains('Restaurant') == true && BGLob.contains('Commercial and Industrial (C&I)') == false){  
					opp.Specialized_Segment_Logic__c = 'Restaurant - Direct';
				}
				if (BGLob.contains('Financial Services Group (FSG)') == true && BGLob.contains('Commercial and Industrial (C&I)') == true){  
					opp.Specialized_Segment_Logic__c = 'Financial Services Group (FSG) - Shared';
				}
				if (BGLob.contains('Financial Services Group (FSG)') == true && BGLob.contains('Commercial and Industrial (C&I)') == false){  
					opp.Specialized_Segment_Logic__c = 'Financial Services Group (FSG) - Direct';
				}								
			}
		
		    // ** Specialized__c & Specialized_Segment_Logic__c END **			
			
				
   			if (updtCore == true){
   				system.debug(opp);
   				update opp;
   			}			
			
		}
	}

	global void finish(Database.BatchableContext BC){
		
	}	
}