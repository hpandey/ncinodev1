@isTest(SeeAllData=true)
private class TestClientTeamRequest {
    static testMethod void unitTest() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standta', Email='contactusera@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='TestingA', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, LOB__c='Treasury Management',
        TimeZoneSidKey='America/Los_Angeles', UserName='contactusera@testorg.com');
        
        System.runAs(u){
            Account acc = new Account(Name='Test Account');
            insert acc;
            
            clientTeamRequest.sendEmail(acc.Id,'contactusera@testorg.com');
        }
    }
}