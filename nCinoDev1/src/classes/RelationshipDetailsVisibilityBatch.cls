public class RelationshipDetailsVisibilityBatch implements Database.Batchable<sObject>{
   public final String Query;
   
   public RelationshipDetailsVisibilityBatch(String q,List<Relationship__c> rlist){
       Query= q;
   }
   public RelationshipDetailsVisibilityBatch(){

      /*Query='select id,'+ 
           '(select id from accounts__r),'+ 
           '(select id from relationship_strategy_plans__r),'+
           '(select id from pdms__r),'+
           '(select id from restricted_relationship_information__r)'+ 
           'from relationship__c';
       */
 	Query='select id from relationship__c';
                       
   }

   public Database.QueryLocator start(Database.BatchableContext BC){
      return ((bc==null)?null: Database.getQueryLocator(query));
   }

   public void execute(Database.BatchableContext BC, List<Relationship__c> relationshipList){
        RelationshipDetailsVisibilityUtil.recalculateSharing(RelationshipList);
   }

   public void finish(Database.BatchableContext BC){
   }
}