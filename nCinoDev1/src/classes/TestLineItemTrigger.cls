@isTest(SeeAllData=true)
private class TestLineItemTrigger {

    static testMethod void unitTest() {
        // Setup test data 

        // This code runs as the system user 

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standta', Email='contactusera@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='TestingA', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, LOB__c='Treasury Management',
        TimeZoneSidKey='America/Los_Angeles', UserName='contactusera@testorg.com');
        
        System.runAs(u){
            Account acc = new Account(Name='Test Account');
            insert acc;
            
            Product2 prod = new Product2(Name = 'Test Product', Family='Capital Markets');
            Product2 prod2 = new Product2(Name = 'Test Product2', Family='Capital Markets');
            Pricebook2 pbook = new Pricebook2(Name = 'Test Pricebook', isActive=true);
            insert prod;
            insert prod2;
            insert pbook;
            
            Pricebook2 sbook = [Select Id,Name from pricebook2 where isStandard=true];

            PriceBookEntry sbe = new PriceBookEntry ( Pricebook2Id=sbook.id, Product2Id=prod.id,UnitPrice=1, IsActive=true);
            insert sbe;
            PriceBookEntry sbe2 = new PriceBookEntry ( Pricebook2Id=sbook.id, Product2Id=prod2.id,UnitPrice=1, IsActive=true);
            insert sbe2;

            PriceBookEntry pbe = new PriceBookEntry ( Pricebook2Id=pbook.id, Product2Id=prod.id,UnitPrice=20, IsActive=true);
            insert pbe;
            
            PriceBookEntry pbe2 = new PriceBookEntry ( Pricebook2Id=pbook.id, Product2Id=prod2.id,UnitPrice=20, IsActive=true);
            insert pbe2;
            
            Opportunity opty = new Opportunity(Name='Test Opty', AccountId=acc.id, CloseDate=date.today(), StageName = 'Prospecting');
            insert opty;
            //Opportunity opty2 = new Opportunity(Name='Test Opty', AccountId=acc.id, CloseDate=date.today(), StageName = 'Prospecting');
            //insert opty2;
            
            
            OpportunityLineItem litm = new OpportunityLineItem(PricebookEntryId=pbe.Id,
                                            OpportunityId=opty.id, Estimated_Revenue__c=20,
                                                      Quantity=1, unitPrice=1);
            insert litm;
            //opty2.Lookback_Pipeline__c = opty.ID;
            //opty2.Lookback__c =true;
            //update opty2;
            try {
            OpportunityLineItem litm2 = new OpportunityLineItem(PricebookEntryId=pbe2.Id,
                                            OpportunityId=opty.id, Estimated_Revenue__c=20,
                                                      Quantity=1, unitPrice=1);
            insert litm2;
            }
            catch (DMLException e){
                System.assertEquals('Can only add Multiple Line Items for Treasury Management Products',e.getDmlMessage(0));
            }
        }
    }
}