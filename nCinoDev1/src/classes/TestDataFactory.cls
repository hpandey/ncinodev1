@isTest
public with sharing class TestDataFactory {

    public static List<Vendor_Document_Fields__c> createVendorDocColumns() {
        Vendor_Document_Fields__c loanDocumentDate = new Vendor_Document_Fields__c();
        Vendor_Document_Fields__c loanDocumentDateReceived = new Vendor_Document_Fields__c();
        Vendor_Document_Fields__c loanDocumentFileName = new Vendor_Document_Fields__c();
        Vendor_Document_Fields__c loanDocumentName = new Vendor_Document_Fields__c();
        List <Vendor_Document_Fields__c> listFields = new List<Vendor_Document_Fields__c>();
        loanDocumentDate.Name = 'Loan Document Date';
        loanDocumentDate.FieldName__c = 'LLC_BI__date__c';
        loanDocumentDate.IsEditable__c = false;
        loanDocumentDate.Order__c = 30;
        listFields.add(loanDocumentDate);
        loanDocumentDateReceived.Name = 'Loan Document Date Received';
        loanDocumentDateReceived.FieldName__c = 'Date_Received__c';
        loanDocumentDateReceived.IsEditable__c = true;
        loanDocumentDateReceived.Order__c = 40;
        listFields.add(loanDocumentDateReceived);
        loanDocumentFileName.Name = 'Loan Document File Name';
        loanDocumentFileName.FieldName__c = 'NDOC__fileName__c';
        loanDocumentFileName.IsEditable__c = false;
        loanDocumentFileName.Order__c = 20;
        listFields.add(loanDocumentFileName);
        loanDocumentName.Name = 'Loan Document Name';
        loanDocumentName.FieldName__c = 'Name';
        loanDocumentName.IsEditable__c = false;
        loanDocumentName.Order__c = 10;
        listFields.add(loanDocumentName);
        insert listFields;
        return listFields;
    }
    
    public static List<Vendor_Document_Fields__c> createVendorDocColumns_throwException() {
        Vendor_Document_Fields__c loanDocumentDate = new Vendor_Document_Fields__c();
        Vendor_Document_Fields__c loanDocumentDateReceived = new Vendor_Document_Fields__c();
        Vendor_Document_Fields__c loanDocumentFileName = new Vendor_Document_Fields__c();
        Vendor_Document_Fields__c loanDocumentName = new Vendor_Document_Fields__c();
        List <Vendor_Document_Fields__c> listFields = new List<Vendor_Document_Fields__c>();
        loanDocumentDate.Name = 'Loan Document Date';
        loanDocumentDate.FieldName__c = 'xyz';
        loanDocumentDate.IsEditable__c = false;
        loanDocumentDate.Order__c = 30;
        listFields.add(loanDocumentDate);
        loanDocumentDateReceived.Name = 'Loan Document Date Received';
        loanDocumentDateReceived.FieldName__c = 'xyz';
        loanDocumentDateReceived.IsEditable__c = true;
        loanDocumentDateReceived.Order__c = 40;
        listFields.add(loanDocumentDateReceived);
        loanDocumentFileName.Name = 'Loan Document File Name';
        loanDocumentFileName.FieldName__c = 'xyz';
        loanDocumentFileName.IsEditable__c = false;
        loanDocumentFileName.Order__c = 20;
        listFields.add(loanDocumentFileName);
        loanDocumentName.Name = 'Loan Document Name';
        loanDocumentName.FieldName__c = 'xyz';
        loanDocumentName.IsEditable__c = false;
        loanDocumentName.Order__c = 10;
        listFields.add(loanDocumentName);
        insert listFields;
        return listFields;
    }

    public static LLC_BI__Loan__c createLoan() {
        // Insert a new relationship to which the new loan will be associated
        Account testRelationship = new Account();
        testRelationship.Name = 'Test Relationship';
        insert testRelationship;

        // Create custom setting for loan creation trigger and set to false
        Trigger_Setting__c loanEntry = new Trigger_Setting__c(Name='LLC_BI__Loan__c',isActive__c =false);
        insert loanEntry;

        // Insert a new loan
        LLC_BI__Loan__c testLoan = new LLC_BI__Loan__c();
        testLoan.Name = 'Test Loan ' + testRelationship.Name;
        testLoan.Test_Loan__c = 'Yes - This is a Test';
        testLoan.LLC_BI__Account__c = testRelationship.ID;
        testLoan.LLC_BI__Loan_Officer__c = UserInfo.getUserId();
        testLoan.LLC_BI__Amount__c = 100;
        testLoan.LLC_BI__Product__c = 'Line of Credit';
        testLoan.LLC_BI__Product_Line__c = 'Commercial';
        testLoan.LLC_BI__Product_Type__c= 'Non-Real Estate';
        insert testLoan;

        return testLoan;
    }

    public static LLC_BI__Loan__c createLoanWithoutID() {
        // Insert a new relationship to which the new loan will be associated
        Account testRelationship = new Account();
        testRelationship.Name = 'Test Relationship';
        insert testRelationship;

        // Insert a new loan
        LLC_BI__Loan__c testLoan = new LLC_BI__Loan__c();
        testLoan.Name = 'Test Loan ' + testRelationship.Name;
        testLoan.Test_Loan__c = 'Yes - This is a Test';
        testLoan.LLC_BI__Account__c = testRelationship.ID;
        testLoan.LLC_BI__Loan_Officer__c = UserInfo.getUserId();
        testLoan.LLC_BI__Amount__c = 100;
        testLoan.LLC_BI__Product__c = 'Line of Credit';
        testLoan.LLC_BI__Product_Line__c = 'Commercial';
        testLoan.LLC_BI__Product_Type__c= 'Non-Real Estate';

        // Do not insert loan, so that it cannot be assigned an ID - for use to make update fail
        return testLoan;
    }

    public static List<LLC_BI__LLC_LoanDocument__c> createLoanDocuments(LLC_BI__Loan__c testLoan, Integer numDocs) {

        // Insert loan document tab, category, and placeholder
        LLC_BI__DocTab__c testDocTab = new LLC_BI__DocTab__c();
        testDocTab.Name = 'Loan File';
        insert testDocTab;
        LLC_BI__DocType__c testDocType = new LLC_BI__DocType__c();
        testDocType.Name = 'Vendor Document';
        testDocType.LLC_BI__docTab__c = testDocTab.ID;
        insert testDocType;
        LLC_BI__DocClass__c testDocClass = new LLC_BI__DocClass__c();
        testDocClass.Name = 'Vendor Doc 1';
        testDocClass.LLC_BI__DocType__c = testDocType.ID;
        insert testDocClass;

        // Insert loan documents associated to the loan - the amount is specified in the parameter numDocs
        List<LLC_BI__LLC_LoanDocument__c> listLoanDocs = new List<LLC_BI__LLC_LoanDocument__c>();
        for (Integer c = 0; c < numDocs; c++) {
            LLC_BI__LLC_LoanDocument__c testLoanDoc = new LLC_BI__LLC_LoanDocument__c();
            testLoanDoc.Name = 'Vendor Doc ' + c;
            testLoanDoc.NDOC__fileName__c = 'xyz';
            testLoanDoc.LLC_BI__date__c = date.today();
            testLoanDoc.Date_Received__c = date.today();
            testLoanDoc.LLC_BI__Loan__c = testLoan.ID;
            testLoanDoc.LLC_BI__docType__c = testDocType.ID;
            testLoanDoc.LLC_BI__docClass__c = testDocClass.ID;
            testLoanDoc.LLC_BI__docTab__c = testDocTab.ID;
            listLoanDocs.add(testLoanDoc);
        }
        insert listLoanDocs;

    return listLoanDocs;
    }
    
    public static List<LLC_BI__LLC_LoanDocument__c> createLoanDocumentsWithoutDates(LLC_BI__Loan__c testLoan, Integer numDocs) {

        // Insert loan document tab, category, and placeholder
        LLC_BI__DocTab__c testDocTab = new LLC_BI__DocTab__c();
        testDocTab.Name = 'Loan File';
        insert testDocTab;
        LLC_BI__DocType__c testDocType = new LLC_BI__DocType__c();
        testDocType.Name = 'Vendor Document';
        testDocType.LLC_BI__docTab__c = testDocTab.ID;
        insert testDocType;
        LLC_BI__DocClass__c testDocClass = new LLC_BI__DocClass__c();
        testDocClass.Name = 'Vendor Doc 1';
        testDocClass.LLC_BI__DocType__c = testDocType.ID;
        insert testDocClass;

        // Insert loan documents associated to the loan - the amount is specified in the parameter numDocs
        List<LLC_BI__LLC_LoanDocument__c> listLoanDocs = new List<LLC_BI__LLC_LoanDocument__c>();
        for (Integer c = 0; c < numDocs; c++) {
            LLC_BI__LLC_LoanDocument__c testLoanDoc = new LLC_BI__LLC_LoanDocument__c();
            testLoanDoc.Name = 'Vendor Doc ' + c;
            testLoanDoc.NDOC__fileName__c = 'xyz';
            testLoanDoc.Date_Received__c = date.today();
            testLoanDoc.LLC_BI__Loan__c = testLoan.ID;
            testLoanDoc.LLC_BI__docType__c = testDocType.ID;
            testLoanDoc.LLC_BI__docClass__c = testDocClass.ID;
            testLoanDoc.LLC_BI__docTab__c = testDocTab.ID;
            listLoanDocs.add(testLoanDoc);
        }
        insert listLoanDocs;

    return listLoanDocs;
    }

    public static List<LLC_BI__LLC_LoanDocument__c> createLoanDocumentsWithOneDuplicate(LLC_BI__Loan__c testLoan, Integer numDocs) {

        // Insert loan document tab, category, and placeholder
        LLC_BI__DocTab__c testDocTab = new LLC_BI__DocTab__c();
        testDocTab.Name = 'Loan File';
        insert testDocTab;
        LLC_BI__DocType__c testDocType = new LLC_BI__DocType__c();
        testDocType.Name = 'Vendor Document';
        testDocType.LLC_BI__docTab__c = testDocTab.ID;
        insert testDocType;
        LLC_BI__DocClass__c testDocClass = new LLC_BI__DocClass__c();
        testDocClass.Name = 'Vendor Doc 1';
        testDocClass.LLC_BI__DocType__c = testDocType.ID;
        insert testDocClass;

        // Insert loan documents associated to the loan - the amount is specified in the parameter numDocs
        List<LLC_BI__LLC_LoanDocument__c> listLoanDocs = new List<LLC_BI__LLC_LoanDocument__c>();
        for (Integer c = 0; c < numDocs; c++) {
            LLC_BI__LLC_LoanDocument__c testLoanDoc = new LLC_BI__LLC_LoanDocument__c();
            testLoanDoc.Name = 'Vendor Doc ' + c;
            testLoanDoc.NDOC__fileName__c = 'xyz';
            testLoanDoc.LLC_BI__date__c = date.today();
            testLoanDoc.Date_Received__c = date.today();
            testLoanDoc.LLC_BI__Loan__c = testLoan.ID;
            testLoanDoc.LLC_BI__docType__c = testDocType.ID;
            testLoanDoc.LLC_BI__docClass__c = testDocClass.ID;
            testLoanDoc.LLC_BI__docTab__c = testDocTab.ID;
            listLoanDocs.add(testLoanDoc);
        }
        insert listLoanDocs;

        listLoanDocs.add(listLoanDocs[0].clone());

    return listLoanDocs;
    }

    public static List<LLC_BI__LLC_LoanDocument__c> editOneLoanDocument(LLC_BI__Loan__c testLoan) {
        List <LLC_BI__LLC_LoanDocument__c> listLoanDocuments = [SELECT Id FROM LLC_BI__LLC_LoanDocument__c WHERE 
                                            LLC_BI__Loan__c = :testLoan.ID];
        
        // Insert loan document tab, category, and placeholder
        LLC_BI__DocTab__c testDocTab = new LLC_BI__DocTab__c();
        testDocTab.Name = 'Loan File';
        insert testDocTab;
        LLC_BI__DocType__c testDocType = new LLC_BI__DocType__c();
        testDocType.Name = 'Vendor Document';
        testDocType.LLC_BI__docTab__c = testDocTab.ID;
        insert testDocType;
        LLC_BI__DocClass__c testDocClass = new LLC_BI__DocClass__c();
        testDocClass.Name = 'Vendor Doc 1';
        testDocClass.LLC_BI__DocType__c = testDocType.ID;
        insert testDocClass;

        listLoanDocuments[0].Name = null;
        listLoanDocuments[0].NDOC__fileName__c = 'xyz';
        listLoanDocuments[0].LLC_BI__date__c = date.today();
        listLoanDocuments[0].Date_Received__c = date.today();
        listLoanDocuments[0].LLC_BI__Loan__c = null;
        listLoanDocuments[0].LLC_BI__docType__c = testDocType.ID;
        listLoanDocuments[0].LLC_BI__docClass__c = testDocClass.ID;
        listLoanDocuments[0].LLC_BI__docTab__c = testDocTab.ID;
        update listLoanDocuments[0];
        return listLoanDocuments;
    }

    public static List<LLC_BI__LLC_LoanDocument__c> createLoanDocumentsWithoutIDs(LLC_BI__Loan__c testLoan, Integer numDocs) {

        // Insert loan document tab, category, and placeholder
        LLC_BI__DocTab__c testDocTab = new LLC_BI__DocTab__c();
        testDocTab.Name = 'Loan File';
        insert testDocTab;
        LLC_BI__DocType__c testDocType = new LLC_BI__DocType__c();
        testDocType.Name = 'Vendor Document';
        testDocType.LLC_BI__docTab__c = testDocTab.ID;
        insert testDocType;
        LLC_BI__DocClass__c testDocClass = new LLC_BI__DocClass__c();
        testDocClass.Name = 'Vendor Doc 1';
        testDocClass.LLC_BI__DocType__c = testDocType.ID;
        insert testDocClass;

        // Insert loan documents associated to the loan - the amount is specified in the parameter numDocs
        List<LLC_BI__LLC_LoanDocument__c> listLoanDocs = new List<LLC_BI__LLC_LoanDocument__c>();
        for (Integer c = 0; c < numDocs; c++) {
            LLC_BI__LLC_LoanDocument__c testLoanDoc = new LLC_BI__LLC_LoanDocument__c();
            testLoanDoc.Name = 'Vendor Doc ' + c;
            testLoanDoc.NDOC__fileName__c = 'xyz';
            testLoanDoc.LLC_BI__date__c = date.today();
            testLoanDoc.Date_Received__c = date.today();
            testLoanDoc.LLC_BI__Loan__c = testLoan.ID;
            testLoanDoc.LLC_BI__docType__c = testDocType.ID;
            testLoanDoc.LLC_BI__docClass__c = testDocClass.ID;
            testLoanDoc.LLC_BI__docTab__c = testDocTab.ID;
            listLoanDocs.add(testLoanDoc);
        }

    return listLoanDocs;
    }

    public static List<LLC_BI__LLC_LoanDocument__c> createLoanDocumentsWithCreatedDatesSet(LLC_BI__Loan__c testLoan, Integer numDocs) {

        // Insert loan document tab, category, and placeholder
        LLC_BI__DocTab__c testDocTab = new LLC_BI__DocTab__c();
        testDocTab.Name = 'Loan File';
        insert testDocTab;
        LLC_BI__DocType__c testDocType = new LLC_BI__DocType__c();
        testDocType.Name = 'Vendor Document';
        testDocType.LLC_BI__docTab__c = testDocTab.ID;
        insert testDocType;
        LLC_BI__DocClass__c testDocClass = new LLC_BI__DocClass__c();
        testDocClass.Name = 'Vendor Doc 1';
        testDocClass.LLC_BI__DocType__c = testDocType.ID;
        insert testDocClass;

        String name = 'Name';

        // Insert loan documents associated to the loan - the amount is specified in the parameter numDocs
        List<LLC_BI__LLC_LoanDocument__c> listLoanDocs = new List<LLC_BI__LLC_LoanDocument__c>();
        for (Integer c = 0; c < numDocs; c++) {
            LLC_BI__LLC_LoanDocument__c testLoanDoc = new LLC_BI__LLC_LoanDocument__c();
            testLoanDoc.Name = name;
            testLoanDoc.NDOC__fileName__c = 'xyz';
            testLoanDoc.LLC_BI__date__c = date.today();
            testLoanDoc.Date_Received__c = date.today();
            testLoanDoc.LLC_BI__Loan__c = testLoan.ID;
            testLoanDoc.LLC_BI__docType__c = testDocType.ID;
            testLoanDoc.LLC_BI__docClass__c = testDocClass.ID;
            testLoanDoc.LLC_BI__docTab__c = testDocTab.ID;
            listLoanDocs.add(testLoanDoc);
        }
        insert listLoanDocs;

        for (Integer b = 0; b < listLoanDocs.size(); b++) {
            LLC_BI__LLC_LoanDocument__c loanDoc = new LLC_BI__LLC_LoanDocument__c();
            loanDoc = listLoanDocs[b];
            Test.setCreatedDate(loanDoc.ID, DateTime.now()+b);
        }

        upsert listLoanDocs;
        


    return listLoanDocs;
    }
    
    public static List<LLC_BI__Field_Map__c> createFieldMap(Integer noOfRecs){
        
        List<LLC_BI__Field_Map__c> fmList = new List<LLC_BI__Field_Map__c>();
        
        //Create Field Map records.
        for(Integer i = 0; i < noOfRecs; i++){
            LLC_BI__Field_Map__c fieldMap = new LLC_BI__Field_Map__c();
            fieldMap.Name = 'LoanToPipelineSync';
            fieldMap.LLC_BI__Orig_Obj__c = 'LLC_BI__Loan__c';
            fieldMap.LLC_BI__Orig_Obj_Field__c = 'Name';
            fieldMap.LLC_BI__Target_Obj__c = 'Opportunity';
            fieldMap.LLC_BI__Target_Obj_Field__c = 'Name';
            
            fmList.add(fieldMap);
        }
        insert fmList;
        return fmList;
    }
    
    public static List<Opportunity> createPipeline(Integer noOfRecs){
        
        List<Opportunity> oppList = new List<Opportunity>();
        
        // Insert a new relationship to which the new Pipelines will be associated
        Account testRelationship = new Account();
        testRelationship.Name = 'Test Relationship';
        insert testRelationship;
        
        //Insert new Pipelines
        for(Integer i = 0; i < noOfRecs; i++){
            Opportunity opp = new Opportunity();
            opp.Name = 'Test Pipeline - '+i;
            opp.AccountId = testRelationship.Id;
            opp.CloseDate = System.Today()+30;
            opp.StageName = 'Lead Generation';

            oppList.add(opp);
        }
        insert oppList;
        return oppList;
    }
    
    public static List<Questionnaire__c> createQuestionnaireRecords(){
        Questionnaire__c parentQuestion = new Questionnaire__c();
        parentQuestion.Question__c = 'Select Question 1';
        parentQuestion.Type__c ='Single-Select';
        parentQuestion.Answers__c='Bad;Fair;Good;Excellent';
        parentQuestion.Loan_Type__c='Small business';
        parentQuestion.order__c=9;
        parentQuestion.Required__c=false;
        insert parentQuestion;
        
        list<Questionnaire__c> lstQuestions = new list<Questionnaire__c>();
        for(integer i=0;i<10;i++)
        {
            Questionnaire__c childQuestion = new Questionnaire__c();
            childQuestion.Question__c = 'Select Question '+i;
            if(i==1 || i==4)
                childQuestion.Type__c ='Multi-Select';
            else 
                childQuestion.Type__c ='Text';
            childQuestion.Loan_Type__c='Small business';
            childQuestion.Required__c=false;
            if(i==8){
                childQuestion.Parent_Question__c=parentQuestion.id;
                childQuestion.Parent_Answer__c='Fair';
                childQuestion.order__c=10;
            }
            if(i==9){
                childQuestion.Parent_Question__c=parentQuestion.id;
                childQuestion.Parent_Answer__c='Fair';
                childQuestion.order__c=11;
            }else
                childQuestion.order__c=i;
            lstQuestions.add(childQuestion);
        }
        insert lstQuestions;
        
        
        lstQuestions.add(parentQuestion);
        //lstQuestions.add(childQuestion);
        
        return lstQuestions;
    }
    
    public static List<Loan_Questionnaire__c> createLoanQuestionnaireRecords(List<Questionnaire__c> lstQuestionnaire,LLC_BI__Loan__c loan){
        List<Loan_Questionnaire__c> lstLoanQuestionnaire = new List<Loan_Questionnaire__c>(); 
        for(Questionnaire__c quest : lstQuestionnaire)
        {
            Loan_Questionnaire__c q = new Loan_Questionnaire__c();
            q.Answer__c = 'Yes';
            q.Loan__c =  loan.Id;
            q.Questionnaire__c = quest.Id; 
            lstLoanQuestionnaire.add(q);  
        }//end for
        if(lstLoanQuestionnaire.size()>0)
        {
            insert lstLoanQuestionnaire;    
        }
        return lstLoanQuestionnaire;    
    }//end method
    
    public static LLC_BI__Loan__c getLoanRecords(String LoanType){
        LLC_BI__Loan__c loan = [Select id,LLC_BI__CRA_Type_Code__c from LLC_BI__Loan__c where LLC_BI__CRA_Type_Code__c=:LoanType];
        return loan;
    }//end method
    
    public static List<Questionnaire__c> getQuestionnaireRecords(String LoanType){
        List<Questionnaire__c> lstquestion = [select Question__c, Type__c, Answers__c, Loan_Type__c, order__c,
                                          Required__c, Parent_Question__c,Parent_Answer__c from Questionnaire__c
                                          where Loan_Type__c=:LoanType order by order__c];
        return lstquestion;
    }//end method

}