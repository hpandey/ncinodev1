global class batchCallGoalsUpdate implements Database.Batchable<sObject>
{
    List<Goal__c> goals {get; set;}
    global batchCallGoalsUpdate(){
        goals = new List<Goal__c>([SELECT Id, User__c, Start_Date__c, End_Date__c, Time_Period__c from Goal__c where Is_Call_Goal__c=TRUE and Start_Date__c < TODAY and End_Date__c = this_year]);
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Call_Plan__r.Id, Name__c,Call_Plan__r.Completed_Date__c, Call_Plan__r.Status__c FROM Participant__c';
                 // 20140129 - pmartin - query taking too long
                 //           'WHERE Call_Plan__r.Completed_Date__c <> null and Name__c <> null';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Participant__c> scope){
        List<Call_Goal__c> newCallGoals = New List<Call_Goal__c>();
        for(Participant__c p : scope){
                for(Goal__c g : goals){
                //For each triggered Participant, find goals that match this user and time period
                    if(p.Name__c == g.User__c 
                    && p.Call_Plan__r.Completed_Date__c >= g.Start_Date__c 
                    && p.Call_Plan__r.Completed_Date__c <= g.End_Date__c){
                        newCallGoals.add(new Call_Goal__c(
                            Goal__c = g.Id,
                            Call_Plan__c = p.Call_Plan__c
                            )
                        );   
                    }
                }
        }
        insert newCallGoals;
    }   

    global void finish(Database.BatchableContext BC){
    }
}