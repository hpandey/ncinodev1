<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Used to store the Credit Bureau results for entities involved in the Loan</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Credit Result</relationshipLabel>
        <relationshipName>Credit_Result</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Credit_Bureau__c</fullName>
        <description>Name of the Credit Bureau form where the report was pulled</description>
        <externalId>false</externalId>
        <label>Credit Bureau</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Customer_ID__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Customer ID of the Entity involved</description>
        <externalId>false</externalId>
        <label>Customer ID</label>
        <referenceTo>LLC_BI__Legal_Entities__c</referenceTo>
        <relationshipLabel>Credit Result</relationshipLabel>
        <relationshipName>Credit_Result</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Date_of_Report__c</fullName>
        <description>Date on which the report was pulled</description>
        <externalId>false</externalId>
        <label>Date of Report</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Error_Details__c</fullName>
        <description>Error message from integration</description>
        <externalId>false</externalId>
        <label>Error Details</label>
        <length>1000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Loan_ID__c</fullName>
        <externalId>false</externalId>
        <label>Loan ID</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Score_Factor_Detail__c</fullName>
        <externalId>false</externalId>
        <label>Score Factor Detail</label>
        <length>1000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Score__c</fullName>
        <description>Credit score returned from the Credit Bureau</description>
        <externalId>false</externalId>
        <label>Score</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Credit Result</label>
    <nameField>
        <displayFormat>CR-{00000000}</displayFormat>
        <label>Credit Result Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Credit Result</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
