We have recently received inquiries regarding the Dyre malware which typically targets customers of well-known financial institutions and how it may now also be targeting Salesforce users (known as OneView for Regions).  The good news for Regions is that we manage our instance of Salesforce by leveraging two of the recommended layers of control which are listed below to prevent this exposure.

•	Activate IP Range Restrictions to allow users to access salesforce.com only from your corporate network or VPN
•	Leverage SAML authentication capabilities to require that all authentication attempts be sourced from your network.

To clarify, Regions only allows our associates to access Salesforce through the single sign-on process within our corporate network or remote VPN.  This means that the Dyre malware would not be able to capture Salesforce login credentials from our user base.  

The recent L@R articles below talk about security issues such as phishing, social engineering, and malware, including best practices to combat these issues. Using these best practices along with the layered security controls Regions has put in place help reduce the possibility of the exploitation the SFDC alert discusses but it is always good to reinforce security best practices. 

http://lifeatregions.rgbk.com/Home/Message/SocialEngineeringII.rf
http://lifeatregions.rgbk.com/Technology/Message/avoidviruses.rf